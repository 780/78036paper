+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : U.S. Fish and Wildlife Service - Midwest Region
Photo URL    : https://www.flickr.com/photos/usfwsmidwest/8636166904/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 2 11:43:07 GMT+0200 2013
Upload Date  : Tue Apr 9 23:57:44 GMT+0200 2013
Views        : 6,236
Comments     : 1


+---------+
|  TITLE  |
+---------+
Little brown bat on left, Indiana bat on right


+---------------+
|  DESCRIPTION  |
+---------------+
Adam Mann


+--------+
|  TAGS  |
+--------+
endangered "Endangered Species Act" "Endangered Species" "Indiana bat" Bat Indiana IN Bats Mammal Mammals Midwest "Creative Commons" USFWS "U.S. Fish and Wildlife Service" 