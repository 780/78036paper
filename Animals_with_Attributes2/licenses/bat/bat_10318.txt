+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Nyanatusita
Photo URL    : https://commons.wikimedia.org/wiki/File:Hipposideros_lankadiva_Kelaart%27s_leaf-nosed_bat_1.jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution 3.0 Unported (//creativecommons.org/licenses/by/3.0/deed.en)
Date         : 2010-03-03
