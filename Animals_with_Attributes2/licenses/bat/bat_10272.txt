+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Mdk572
Photo URL    : https://commons.wikimedia.org/wiki/File:Little_Red_Flying_Foxes.jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution-Share Alike 3.0 Unported (//creativecommons.org/licenses/by-sa/3.0/deed.en)
Date         : 2010-02-25
