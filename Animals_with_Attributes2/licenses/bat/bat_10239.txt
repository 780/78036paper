+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Krynak Tim, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/bats-pictures/tri-colored-bat-pipistrellus-subflavus-hibernating-in-an-abandoned-limestone-mine
License      : public domain (CC0)
Uploaded     : 2015-01-02 09:13:01

+--------+
|  TAGS  |
+--------+
colored, bat, pipistrellus, subflavus, hibernating, abandoned, limestone, mine