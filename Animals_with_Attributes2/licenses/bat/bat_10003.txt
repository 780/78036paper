+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : anoldent
Photo URL    : https://www.flickr.com/photos/anoldent/552487646/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 14 12:22:14 GMT+0200 2007
Upload Date  : Fri Jun 15 18:21:14 GMT+0200 2007
Views        : 1,785
Comments     : 0


+---------+
|  TITLE  |
+---------+
1998 Fruit Bat


+---------------+
|  DESCRIPTION  |
+---------------+
This one had been disturbed from its sleep in a tree.


+--------+
|  TAGS  |
+--------+
bat 