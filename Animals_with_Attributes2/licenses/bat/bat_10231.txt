+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Stihler Craig, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/bats-pictures/hibernating-virginia-big-eared-bats-in-cave
License      : public domain (CC0)
Uploaded     : 2016-06-07 09:02:03

+--------+
|  TAGS  |
+--------+
hibernating, Virginia, big, eared, bats, cave