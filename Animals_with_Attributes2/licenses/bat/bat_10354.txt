+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Jim Conrad
Photo URL    : https://commons.wikimedia.org/wiki/File:Eptesicus_fuscus-teeth-and-ears.jpg
License      : public domain
Date         : 2008-07-28
