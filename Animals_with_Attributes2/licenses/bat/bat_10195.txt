+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Vijayanarasimha
Photo URL    : https://pixabay.com/en/tree-bats-dharwad-bat-eared-india-300855/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : March 29, 2014
Uploaded     : March 31, 2014

+--------+
|  TAGS  |
+--------+
Tree, Bats, Dharwad, Bat-Eared, India, Mamma, Fly