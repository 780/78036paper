+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USDAgov
Photo URL    : https://www.flickr.com/photos/usdagov/10537766716/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 28 15:32:45 GMT+0100 2013
Upload Date  : Mon Oct 28 16:42:07 GMT+0100 2013
Views        : 4,980
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bat Week


+---------------+
|  DESCRIPTION  |
+---------------+
Indiana bats, such as this one, are part of a monitoring program on the Monongahela National Forest in West Virginia. The bats are fitted with a radio transmitter and tracked to roosting locations throughout the life of the transmitter. (U.S. Forest Service)


+--------+
|  TAGS  |
+--------+
bats FS pollinators Farmers Texas "White-Nose Syndrome" Canada "U.S. Fish and Wildlife Service" BatsLIVE! Conservation Forestry "healthy forests" 