+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Furryscaly
Photo URL    : https://www.flickr.com/photos/furryscalyman/569023964/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 17 11:36:24 GMT+0200 2007
Upload Date  : Tue Jun 19 14:01:59 GMT+0200 2007
Geotag Info  : Latitude:39.108081, Longitude:-76.762279
Views        : 10,024
Comments     : 2


+---------+
|  TITLE  |
+---------+
Big Brown Bat


+---------------+
|  DESCRIPTION  |
+---------------+
She's the same species as the other bats I catch in Maryland, which are big brown bats, <i>Eptesicus fuscus</i>.  I caught this one in the hallway.  She's not baring her teeth at me, she's echolocating.  I call her &quot;Bird bat&quot;.


+--------+
|  TAGS  |
+--------+
"Eptesicus fuscus fuscus" "eastern big brown bat" "Bird Bat" flash countertop echolocating echolocation netted captured teeth scary creepy face head ears tragus close-up "beady eyes" "Anne Arundel County" Maryland MD "bat removal" "animal rehabilitation" "animal rescue" wildlife "wild animal" "urban wildlife" Animalia animal taxonomy:kingdom=Animalia Chordata taxonomy:phylum=Chordata Mammalia mammal hairy furry taxonomy:class=Mammalia Chiroptera bat "flying mammal" winged taxonomy:order=Chiroptera Microchiroptera microbat "true bat" insectivore insectivorous "small mammal" Vespertilionoidea Vespertilionidae "evening bats" "vesper bats" "common bats" fuzzy taxonomy:family=Vespertilionidae Eptesicus "house bats" taxonomy:genus=Eptesicus "taxonomy:binomial=Eptesicus fuscus" "big brown bat" brown "taxonomy:trinomial=Eptesicus fuscus fuscus" "taxonomy:common=big brown bat" 