+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Froschauer Ann, USFWS
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/bats-pictures/bats-flying-sunset
License      : public domain (CC0)
Uploaded     : 2016-06-07 08:57:38

+--------+
|  TAGS  |
+--------+
bats, flying, sunset
