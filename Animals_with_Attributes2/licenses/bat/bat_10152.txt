+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Gilles San Martin
Photo URL    : https://www.flickr.com/photos/sanmartin/4482514125/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 2 17:29:54 GMT+0100 2010
Upload Date  : Fri Apr 2 03:35:07 GMT+0200 2010
Views        : 3,290
Comments     : 0


+---------+
|  TITLE  |
+---------+
Myotis mystacinus sl


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
gtplecotus vertebrata bat Chimay chiroptera vespertillonidae "whiskered bat" "Myotis mystacinus - brandtii" hibernating hibernation Belgium 