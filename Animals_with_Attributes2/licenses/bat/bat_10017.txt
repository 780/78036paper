+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/13361050093/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 23 10:36:02 GMT+0100 2014
Upload Date  : Sun Mar 23 20:02:05 GMT+0100 2014
Geotag Info  : Latitude:42.462697, Longitude:-71.093636
Views        : 993
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bat Hanging Out


+---------------+
|  DESCRIPTION  |
+---------------+
Had to crank the ISO to get this but I think it was worth it.


+--------+
|  TAGS  |
+--------+
stone zoo bat cave hanging closeup 