+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stanzebla
Photo URL    : https://www.flickr.com/photos/stanzebla/16839250906/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Mar 17 12:51:45 GMT+0100 2015
Upload Date  : Thu Mar 19 20:51:39 GMT+0100 2015
Geotag Info  : Latitude:49.185350, Longitude:0.605406
Views        : 2,622
Comments     : 19


+---------+
|  TITLE  |
+---------+
Bat from the castle cellar


+---------------+
|  DESCRIPTION  |
+---------------+
Must be the local vampire lord. The cute little bat got stuck in sticky plastic film. I removed the plastic carefully. Hope nothing was damaged. Gave the little cute little thing some water, because I hought it might need some. Unsure if it was drinking though. Brought it in a WWII bunker afterwards. That's where all our wild, lost animals get a temporary home.


+--------+
|  TAGS  |
+--------+
bats fledermaus fledermäuse chauve-souris 