+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : seagul
Photo URL    : https://pixabay.com/en/stalactites-hibernated-bat-cave-323562/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : Nov. 21, 2010
Uploaded     : April 14, 2014

+--------+
|  TAGS  |
+--------+
Stalactites, Hibernated Bat, Cave, Caves, Bat, Cavern