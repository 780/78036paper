+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jo Naylor
Photo URL    : https://www.flickr.com/photos/pandora_6666/3615393930/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 10 18:12:43 GMT+0200 2009
Upload Date  : Thu Jun 11 01:12:43 GMT+0200 2009
Views        : 315
Comments     : 0


+---------+
|  TITLE  |
+---------+
a bat - we found a bat


+---------------+
|  DESCRIPTION  |
+---------------+
yes, this is a bat!! It was laying on the ground at my dad's house - I almost stepped on it in fact until he started screaching at me


+--------+
|  TAGS  |
+--------+
bat animal night fangs cool 