+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Watson Lake
Photo URL    : https://www.flickr.com/photos/yukonlife/16497719246/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 22 14:04:56 GMT+0200 2014
Upload Date  : Sat Feb 14 04:00:32 GMT+0100 2015
Views        : 292
Comments     : 0


+---------+
|  TITLE  |
+---------+
Brown Bat


+---------------+
|  DESCRIPTION  |
+---------------+
A brown bat at the Watson Lake airport terminal entrance.


+--------+
|  TAGS  |
+--------+
"Watson Lake" Yukon bat "brown bat" 