+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Rohit.chakravarty
Photo URL    : https://commons.wikimedia.org/wiki/File:Fulvous_Roundleaf_Bat_(Hipposideros_fulvus).jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution-Share Alike 3.0 Unported (//creativecommons.org/licenses/by-sa/3.0/deed.en)
Date         : 2012-04-18
