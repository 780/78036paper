+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nicolas.boullosa
Photo URL    : https://www.flickr.com/photos/faircompanies/2225909936/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jan 7 20:41:47 GMT+0100 2008
Upload Date  : Mon Jan 28 11:49:36 GMT+0100 2008
Geotag Info  : Latitude:-37.650461, Longitude:145.532627
Views        : 345
Comments     : 0


+---------+
|  TITLE  |
+---------+
bats


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"healesville animal sancturary" melbourne australia aussie koala kangaroo fitzroy hipster "inés boullosa" "kirsten dirksen" "katrina ellis" 