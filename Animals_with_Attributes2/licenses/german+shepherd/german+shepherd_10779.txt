+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : haroldmeerveld
Photo URL    : https://www.flickr.com/photos/haroldmeerveld/14877819005/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 29 15:44:39 GMT+0200 2014
Upload Date  : Sun Aug 10 18:54:06 GMT+0200 2014
Views        : 6,958
Comments     : 4


+---------+
|  TITLE  |
+---------+
Sable German Shepherd


+---------------+
|  DESCRIPTION  |
+---------------+
Moja Westerveldse bos.


+--------+
|  TAGS  |
+--------+
"german shepherd" Dogs dog 