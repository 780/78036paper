+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jenniffer Baltzell
Photo URL    : https://www.flickr.com/photos/jjfbaltzell/14657874618/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 26 10:58:53 GMT+0100 2014
Upload Date  : Wed Aug 6 16:58:46 GMT+0200 2014
Views        : 108
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otto, ~22.5 weeks


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Otto GSD "German Shepherd Dog" 