+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : handicappedpets1
Photo URL    : https://www.flickr.com/photos/handicappedpets/5730841543/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 20 17:36:35 GMT+0200 2009
Upload Date  : Tue May 17 21:49:53 GMT+0200 2011
Views        : 921
Comments     : 0


+---------+
|  TITLE  |
+---------+
German Shepherd Dog Wheelchair


+---------------+
|  DESCRIPTION  |
+---------------+
Dog Wheelchair for a Degenerative Myelopathy dog.


+--------+
|  TAGS  |
+--------+
(no tags)