+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rennett Stowe
Photo URL    : https://www.flickr.com/photos/tomsaint/2518870233/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 24 15:03:52 GMT+0200 2008
Upload Date  : Sun May 25 00:03:52 GMT+0200 2008
Views        : 23,584
Comments     : 4


+---------+
|  TITLE  |
+---------+
German Shepherd


+---------------+
|  DESCRIPTION  |
+---------------+
My sweet dog.


+--------+
|  TAGS  |
+--------+
"German Shepherd" shepherd dog dogs hound husky pooch Alsatian "husky mix' German" "mix hound hound" "dog white" black "dog my" "pooch sweet" "good dog" 'beautiful "a dog" "lovely dog" "our dog" "family dog" "family's dog" wolf "German Shepherds" "black nose" "white ears" "seal pup" "seal pup eyes" puppy "puppy eyes" "dog eyes" "cute eyes" cute cuddly "cuddly dog" "german shephard" "german sheperd" "german shepard" "california dog" ivy "brown eyes" "white and black coat" "my sweet dog" "sassy hound" "come pet me" "pet me" 