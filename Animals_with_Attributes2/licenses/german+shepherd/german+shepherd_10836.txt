+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aegidian
Photo URL    : https://www.flickr.com/photos/aegidian/10341227713/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 18 07:03:02 GMT+0200 2013
Upload Date  : Fri Oct 18 08:56:40 GMT+0200 2013
Geotag Info  : Latitude:51.562927, Longitude:0.015172
Views        : 579
Comments     : 0


+---------+
|  TITLE  |
+---------+
Silhouetted Bessy in the morning


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
flickriosapp:filter=NoFilter uploaded:by=flickr_mobile Bessy GSD "German Shepherd" dog sunrise Leytonstone 