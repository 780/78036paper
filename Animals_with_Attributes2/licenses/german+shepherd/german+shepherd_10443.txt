+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MarilynJane
Photo URL    : https://www.flickr.com/photos/marilynjane/3817350252/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 12 07:32:18 GMT+0200 2009
Upload Date  : Thu Aug 13 10:01:12 GMT+0200 2009
Views        : 868
Comments     : 4


+---------+
|  TITLE  |
+---------+
Hambledon  Hill


+---------------+
|  DESCRIPTION  |
+---------------+
Kim watching the early mist  clearing the Blackmore Vale


+--------+
|  TAGS  |
+--------+
"Hambledon Hill" "Blackmore Vale" "Child Okeford" Kim GSD "German Shepherd" 