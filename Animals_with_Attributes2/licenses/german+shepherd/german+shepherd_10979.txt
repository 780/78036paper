+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Soren Wolf
Photo URL    : https://www.flickr.com/photos/sorenwolf/24184527192/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 6 15:44:12 GMT+0100 2016
Upload Date  : Sun Jan 10 14:58:59 GMT+0100 2016
Views        : 141
Comments     : 0


+---------+
|  TITLE  |
+---------+
Vuko


+---------------+
|  DESCRIPTION  |
+---------------+
A walk in the snow


+--------+
|  TAGS  |
+--------+
dog german shepherd winter now play outdoor 