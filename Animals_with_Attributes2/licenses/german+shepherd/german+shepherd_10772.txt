+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hardwarehank
Photo URL    : https://www.flickr.com/photos/ralree/5724768847/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 15 18:50:40 GMT+0200 2011
Upload Date  : Mon May 16 06:10:09 GMT+0200 2011
Views        : 291
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ruby Ruby Ruby Ruby!


+---------------+
|  DESCRIPTION  |
+---------------+
Having some fun with my Tamron 28-75 f/2.8.


+--------+
|  TAGS  |
+--------+
ruby gsd "german shepherd" dog 