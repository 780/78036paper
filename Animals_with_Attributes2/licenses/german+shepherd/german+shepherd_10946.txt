+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aegidian
Photo URL    : https://www.flickr.com/photos/aegidian/7017591041/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 26 11:34:10 GMT+0200 2012
Upload Date  : Mon Mar 26 15:51:40 GMT+0200 2012
Geotag Info  : Latitude:51.567617, Longitude:0.050511
Views        : 543
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dog gsd "german shepherd" 