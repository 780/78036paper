+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jacilluch
Photo URL    : https://www.flickr.com/photos/70626035@N00/8636393946/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 28 20:02:13 GMT+0100 2013
Upload Date  : Wed Apr 10 01:42:13 GMT+0200 2013
Views        : 7,258
Comments     : 135


+---------+
|  TITLE  |
+---------+
Diplotaxis erucoides y mi ena ..... por Teruel


+---------------+
|  DESCRIPTION  |
+---------------+
Mi pastora esta muy viejecita, pero esta guapa entre flores.

De unas  hierbas muy abundantes durante el otoño y el invierno en los campos de cultivo, aunque pueden estar en flor en cualquier época del año. Germina rápidamente después de las primeras lluvias, y en pocas semanas florece y cubre de blanco los campos.


+--------+
|  TAGS  |
+--------+
ena perro dog "pastor aleman" germanshepherddog GSD spain españa I "german shepherd" 狗兒 "pastori tedeschi" gos can planta plant silvestre blanco white Diplotaxis erucoides hembra 