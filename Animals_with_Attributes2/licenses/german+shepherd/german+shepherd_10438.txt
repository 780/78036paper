+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : localpups
Photo URL    : https://www.flickr.com/photos/133374862@N02/20474454906/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 6 17:17:10 GMT+0200 2012
Upload Date  : Wed Aug 12 02:37:26 GMT+0200 2015
Views        : 492
Comments     : 0


+---------+
|  TITLE  |
+---------+
Guard dog outside


+---------------+
|  DESCRIPTION  |
+---------------+
Please use attribution and give attribution link to: <a href="http://www.localpuppybreeders.com" rel="nofollow">www.localpuppybreeders.com</a>


+--------+
|  TAGS  |
+--------+
animal breed dog friend german guard nature outdoor police shepherd puppy 