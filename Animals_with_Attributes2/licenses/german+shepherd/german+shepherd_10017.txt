+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eblaser
Photo URL    : https://www.flickr.com/photos/evanblaser/6299543988/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 17 05:47:38 GMT+0200 2011
Upload Date  : Mon Oct 31 18:39:34 GMT+0100 2011
Geotag Info  : Latitude:36.240933, Longitude:-121.819581
Views        : 2,613
Comments     : 1


+---------+
|  TITLE  |
+---------+
289 [yawn]


+---------------+
|  DESCRIPTION  |
+---------------+
This is Baloo. His mother is a  German shepherd, his father an apparently very manly Welsh corgi.


+--------+
|  TAGS  |
+--------+
corgi german shepherd mix puppy yawn 