+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aegidian
Photo URL    : https://www.flickr.com/photos/aegidian/8857428274/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 27 10:56:20 GMT+0200 2013
Upload Date  : Mon May 27 20:16:00 GMT+0200 2013
Geotag Info  : Latitude:51.564145, Longitude:0.044749
Views        : 223
Comments     : 0


+---------+
|  TITLE  |
+---------+
"Can we go now?"


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
bessy dog dogshow "German Shepherd" GSD 