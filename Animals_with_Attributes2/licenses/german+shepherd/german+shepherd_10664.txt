+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : melalouise
Photo URL    : https://www.flickr.com/photos/melalouise/2353518602/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 22 15:51:22 GMT+0100 2008
Upload Date  : Sun Mar 23 02:04:50 GMT+0100 2008
Views        : 748
Comments     : 0


+---------+
|  TITLE  |
+---------+
Nanuk


+---------------+
|  DESCRIPTION  |
+---------------+
My sister's new puppy - German Shepherd x Rottweiler


+--------+
|  TAGS  |
+--------+
puppy dog 