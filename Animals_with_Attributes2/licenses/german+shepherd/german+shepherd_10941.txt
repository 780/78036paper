+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aegidian
Photo URL    : https://www.flickr.com/photos/aegidian/6306298626/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 2 12:02:32 GMT+0100 2011
Upload Date  : Wed Nov 2 16:20:45 GMT+0100 2011
Geotag Info  : Latitude:51.566333, Longitude:0.026000
Views        : 385
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Autumn "German Shepherd Dog" GSD leaves London Max 