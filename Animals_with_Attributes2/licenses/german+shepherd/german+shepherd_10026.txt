+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : localpups
Photo URL    : https://www.flickr.com/photos/133374862@N02/20490497462/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 23 11:13:43 GMT+0100 2013
Upload Date  : Wed Aug 12 01:25:28 GMT+0200 2015
Views        : 389
Comments     : 0


+---------+
|  TITLE  |
+---------+
3051-german-shepherd-dog


+---------------+
|  DESCRIPTION  |
+---------------+
Please use attribution and give attribution link to: <a href="http://www.localpuppybreeders.com" rel="nofollow">www.localpuppybreeders.com</a>


+--------+
|  TAGS  |
+--------+
dog puppy 