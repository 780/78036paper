+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lilli2de
Photo URL    : https://www.flickr.com/photos/seven_of9/6818893654/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 26 15:16:19 GMT+0100 2012
Upload Date  : Thu Mar 8 20:53:18 GMT+0100 2012
Views        : 1,681
Comments     : 7


+---------+
|  TITLE  |
+---------+
And where is the weekend?


+---------------+
|  DESCRIPTION  |
+---------------+
Wo bleibt denn jetzt das Wochenende?


+--------+
|  TAGS  |
+--------+
26Febr2012 "Belgischer Schäferhund" "Belgian Shepherd Dog" "my mothers dog Theo" "meiner Mutters Hund Theo" 