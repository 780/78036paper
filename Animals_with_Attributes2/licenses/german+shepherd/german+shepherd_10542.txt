+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tedkerwin
Photo URL    : https://www.flickr.com/photos/tedkerwin/2467294933/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 5 10:48:06 GMT+0200 2008
Upload Date  : Mon May 5 16:48:06 GMT+0200 2008
Geotag Info  : Latitude:40.639065, Longitude:-74.384994
Views        : 341
Comments     : 0


+---------+
|  TITLE  |
+---------+
Keely Fanwood


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
gsd "german shepherd" dog 