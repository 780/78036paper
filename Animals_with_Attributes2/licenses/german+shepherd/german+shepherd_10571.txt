+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MarilynJane
Photo URL    : https://www.flickr.com/photos/marilynjane/2341503862/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 17 11:41:01 GMT+0100 2008
Upload Date  : Mon Mar 17 21:00:40 GMT+0100 2008
Views        : 2,634
Comments     : 2


+---------+
|  TITLE  |
+---------+
Kim


+---------------+
|  DESCRIPTION  |
+---------------+
Our new edition to the family, now at 7 weeks old and will be with us at Easter. Kim is a full coated German Shepherd. It was difficult to take a photo as she wouldn't stay still!


+--------+
|  TAGS  |
+--------+
"German Shepherd Puppy" Dog Pet Animal Kim 