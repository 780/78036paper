+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Bob Haarmans
Photo URL    : https://www.flickr.com/photos/rhaarmans/7279653454/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 26 15:15:23 GMT+0200 2012
Upload Date  : Sun May 27 16:43:29 GMT+0200 2012
Views        : 2,972
Comments     : 0


+---------+
|  TITLE  |
+---------+
St. James Farm Family Day 2012


+---------------+
|  DESCRIPTION  |
+---------------+
German Shepherd Search and Rescue Dog Association of Illinois


+--------+
|  TAGS  |
+--------+
Outdoor St. James Farm "St. James Farm" Warrenville DuPage Forest Preserve Family Day 2012 "DuPage Forest Preserve" German Shepherd Search Rescue Dog Association Illinois "German Shepherd Search and Rescue Dog Association" "German Shepherd" 