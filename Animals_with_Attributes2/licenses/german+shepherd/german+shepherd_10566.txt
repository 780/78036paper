+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MarilynJane
Photo URL    : https://www.flickr.com/photos/marilynjane/2538427107/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 31 15:01:33 GMT+0200 2008
Upload Date  : Sat May 31 19:03:11 GMT+0200 2008
Views        : 1,868
Comments     : 8


+---------+
|  TITLE  |
+---------+
Kim at 16 weeks


+---------------+
|  DESCRIPTION  |
+---------------+
New teeth coming through at the moment!


+--------+
|  TAGS  |
+--------+
Kim GSD "German Shepherd" Dog Pet Animal 