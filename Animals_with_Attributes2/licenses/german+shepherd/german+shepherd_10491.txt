+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tedkerwin
Photo URL    : https://www.flickr.com/photos/tedkerwin/2406087396/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 4 08:18:38 GMT+0200 2008
Upload Date  : Fri Apr 11 19:43:31 GMT+0200 2008
Geotag Info  : Latitude:40.194216, Longitude:-74.026222
Views        : 350
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_0012


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
gsd "german shepherd" dog 