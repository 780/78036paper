+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : diveofficer
Photo URL    : https://www.flickr.com/photos/diveofficer/2153742560/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 6 15:03:16 GMT+0200 2005
Upload Date  : Tue Jan 1 05:05:35 GMT+0100 2008
Geotag Info  : Latitude:30.264034, Longitude:-89.791431
Views        : 538
Comments     : 2


+---------+
|  TITLE  |
+---------+
Brisk in Slidell


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animal dog slidell katrina hurricane Brisk GHSD SAR "German Shepherd" 