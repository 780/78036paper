+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nick Mitha
Photo URL    : https://www.flickr.com/photos/nickmitha/3224130414/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 25 01:31:29 GMT+0100 2009
Upload Date  : Sun Jan 25 01:31:29 GMT+0100 2009
Views        : 4,892
Comments     : 15


+---------+
|  TITLE  |
+---------+
Do I have to wear this?


+---------------+
|  DESCRIPTION  |
+---------------+
© 2009 Nick Mitha Photography. All rights reserved.

Kelly has to wear this to keep her from agitating a skin rash by licking it.

&quot;Creative Commons&quot; granted only to Petresolution.
Info for <a href="http://www.flickr.com/groups/petresolution/">www.flickr.com/groups/petresolution/</a> 

Name of pet: Kelly
Breed of pet: Deutscher Schäferhund (German Shepard Dog)
age of pet: 8 years old
Special Interests of Pet: Playing, Tracking, more playing, games involving searching.
How you and your pet got together: Adopted her from someone else, who had no time for her.
Is your pet keeping his or her resolution? No, thats why she has to wear the cone ;)


+--------+
|  TAGS  |
+--------+
GSD German Shepard Dog Cone Minolta AF 28-100mm D Sad Unhappy PetResolutions Deutscher Schäferhund petresolution AmazonCaresContest 