+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jeffgunn
Photo URL    : https://www.flickr.com/photos/jeffgunn/4256789776/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 7 18:59:56 GMT+0100 2010
Upload Date  : Fri Jan 8 14:47:42 GMT+0100 2010
Geotag Info  : Latitude:33.736135, Longitude:-84.381684
Views        : 1,392
Comments     : 1


+---------+
|  TITLE  |
+---------+
Aayla


+---------------+
|  DESCRIPTION  |
+---------------+
Phoenix Park II


+--------+
|  TAGS  |
+--------+
atlanta snow dogs "german shepherd" black summerhill "phoenix park" 