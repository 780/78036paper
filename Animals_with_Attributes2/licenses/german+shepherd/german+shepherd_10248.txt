+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : EpicFireworks
Photo URL    : https://www.flickr.com/photos/epicfireworks/2824476049/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 24 23:21:57 GMT+0100 2007
Upload Date  : Wed Sep 3 17:22:37 GMT+0200 2008
Geotag Info  : Latitude:51.525833, Longitude:-0.230712
Views        : 1,815
Comments     : 0


+---------+
|  TITLE  |
+---------+
Epic Fireworks - Credit Control


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
firework pyro bruce dog "german shepherd" "credit control" "epic fireworks" 