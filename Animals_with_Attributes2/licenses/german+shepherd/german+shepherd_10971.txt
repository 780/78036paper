+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aegidian
Photo URL    : https://www.flickr.com/photos/aegidian/14731755410/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 14 18:06:47 GMT+0200 2014
Upload Date  : Thu Aug 14 19:52:01 GMT+0200 2014
Geotag Info  : Latitude:51.565155, Longitude:0.019766
Views        : 282
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Bessy GSD "German Shepherd" puppies dogs puppy dog 