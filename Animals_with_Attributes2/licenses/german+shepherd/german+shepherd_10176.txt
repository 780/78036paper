+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MarilynJane
Photo URL    : https://www.flickr.com/photos/marilynjane/4398892252/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 1 08:14:10 GMT+0100 2010
Upload Date  : Mon Mar 1 18:00:13 GMT+0100 2010
Views        : 613
Comments     : 2


+---------+
|  TITLE  |
+---------+
Do you want ice with it?


+---------------+
|  DESCRIPTION  |
+---------------+
Kim just loves being in anything wet or muddy even if it's frozen!


+--------+
|  TAGS  |
+--------+
Kim GSD "German Shepherd dog" Dog Pet animal 