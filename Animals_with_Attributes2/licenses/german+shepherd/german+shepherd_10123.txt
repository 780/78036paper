+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tobyotter
Photo URL    : https://www.flickr.com/photos/78428166@N00/5705419798/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 9 18:02:06 GMT+0200 2011
Upload Date  : Tue May 10 01:52:25 GMT+0200 2011
Views        : 2,572
Comments     : 1


+---------+
|  TITLE  |
+---------+
Bud


+---------------+
|  DESCRIPTION  |
+---------------+
A neighbor's German Shepherd - he is really big and his owner is a tiny woman but he is a good guard and companion for her.


+--------+
|  TAGS  |
+--------+
dog k9 canine perro pet Bud "German Shepherd" somebodyelsesdog 