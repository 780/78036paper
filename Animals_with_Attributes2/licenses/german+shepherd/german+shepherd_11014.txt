+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rennett Stowe
Photo URL    : https://www.flickr.com/photos/tomsaint/3117105850/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 17 16:30:24 GMT+0100 2008
Upload Date  : Thu Dec 18 00:45:56 GMT+0100 2008
Views        : 9,463
Comments     : 3


+---------+
|  TITLE  |
+---------+
Wolf Mix


+---------------+
|  DESCRIPTION  |
+---------------+
The Wolf Dog playing with a ball in the snow


+--------+
|  TAGS  |
+--------+
"german shepherd" "wolf hybrid" "german shepherd wolf mix" "playing with a ball" "playing in the snow" "snow day" "no school" "playing with my ball" "beautiful german shepherd" "snow flakes" "large snow flakes" "fighting with a ball" "pretty dog" "beautiful dog" "mans' best friend" "dog that loves snow" 