+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aegidian
Photo URL    : https://www.flickr.com/photos/aegidian/5796041629/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 3 12:30:06 GMT+0200 2011
Upload Date  : Sat Jun 4 15:11:09 GMT+0200 2011
Geotag Info  : Latitude:51.569167, Longitude:0.041500
Views        : 1,946
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Alsatian dog "German Shepherd" Max 