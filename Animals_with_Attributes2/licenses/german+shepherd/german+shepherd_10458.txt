+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daniel.baker
Photo URL    : https://www.flickr.com/photos/theadventuresofdan/3168848412/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 4 18:12:19 GMT+0100 2009
Upload Date  : Mon Jan 5 01:12:19 GMT+0100 2009
Views        : 158
Comments     : 0


+---------+
|  TITLE  |
+---------+
Honey enjoys the flowers


+---------------+
|  DESCRIPTION  |
+---------------+
From the archives. She looks like a puppet.


+--------+
|  TAGS  |
+--------+
"pitbull/german shepherd" 