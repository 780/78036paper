+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : maduko
Photo URL    : https://www.flickr.com/photos/maduko/8691197106/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 28 07:00:27 GMT+0200 2013
Upload Date  : Mon Apr 29 02:01:20 GMT+0200 2013
Geotag Info  : Latitude:35.473147, Longitude:-97.516429
Views        : 450
Comments     : 0


+---------+
|  TITLE  |
+---------+
2013-okc-03


+---------------+
|  DESCRIPTION  |
+---------------+
Security was visible everywhere- and the red chew toy was probably the most secure thing for miles.


+--------+
|  TAGS  |
+--------+
"Oklahoma City Memorial Marathon" marathon "Oklahoma City" police "german shepherd" dog 