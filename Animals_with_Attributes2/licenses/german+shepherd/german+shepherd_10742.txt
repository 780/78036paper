+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael Hart Photography
Photo URL    : https://www.flickr.com/photos/9778871@N08/12279741195/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 2 18:47:34 GMT+0100 2014
Upload Date  : Mon Feb 3 03:03:53 GMT+0100 2014
Views        : 1,643
Comments     : 0


+---------+
|  TITLE  |
+---------+
Police Dog Demonstration


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Police Dog" "German Shepherd" Schutzhund 