+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jenniffer Baltzell
Photo URL    : https://www.flickr.com/photos/jjfbaltzell/14657934789/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 1 12:10:15 GMT+0200 2014
Upload Date  : Wed Aug 6 17:11:33 GMT+0200 2014
Views        : 290
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otto, 9 days shy of one year old


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Otto GSD "German Shepherd Dog" 