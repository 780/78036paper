+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mcunnelly
Photo URL    : https://www.flickr.com/photos/cunnelly/14472205500/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 15 06:33:16 GMT+0200 2014
Upload Date  : Tue Jul 15 07:42:54 GMT+0200 2014
Views        : 1,655
Comments     : 20


+---------+
|  TITLE  |
+---------+
German Shepherd


+---------------+
|  DESCRIPTION  |
+---------------+
Waiting to go downstairs for breakfast


+--------+
|  TAGS  |
+--------+
german shepherd dog gsd dogs pet canon 600d 