+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mrRobot
Photo URL    : https://www.flickr.com/photos/tjarda/2333442307/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 14 20:00:02 GMT+0100 2008
Upload Date  : Sat Mar 15 02:17:11 GMT+0100 2008
Views        : 599
Comments     : 0


+---------+
|  TITLE  |
+---------+
the grand stand


+---------------+
|  DESCRIPTION  |
+---------------+
a fortuitous alignment


+--------+
|  TAGS  |
+--------+
dog "german shepherd" 