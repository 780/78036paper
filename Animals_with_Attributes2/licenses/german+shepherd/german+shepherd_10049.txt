+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/14681284654/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 31 17:08:29 GMT+0200 2014
Upload Date  : Fri Jul 18 15:27:08 GMT+0200 2014
Views        : 738
Comments     : 0


+---------+
|  TITLE  |
+---------+
Action


+---------------+
|  DESCRIPTION  |
+---------------+
Mai 2014
Canon EOS 60D
EF 85mm f/1.8 USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hunde dogs dog puppies puppy welpe welpen deutscher schäferhund german shepherd ddr linie line 