+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ☺ Lee J Haywood
Photo URL    : https://www.flickr.com/photos/leehaywood/4809279603/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 11 17:31:01 GMT+0200 2010
Upload Date  : Mon Jul 19 21:45:16 GMT+0200 2010
Views        : 1,686
Comments     : 2


+---------+
|  TITLE  |
+---------+
Sleepy


+---------------+
|  DESCRIPTION  |
+---------------+
This lovely dog didn't even raise its head as our large group passed by.

This image has been used in 4 articles...
• <a href="http://www.choosing-a-dog-made-easy.com/german-shepherd-dog.html" rel="nofollow">www.choosing-a-dog-made-easy.com/german-shepherd-dog.html</a>
• <a href="http://keyw.com/10-most-popular-dog-breeds-2/" rel="nofollow">keyw.com/10-most-popular-dog-breeds-2/</a>
• <a href="http://www.annarbor.com/pets/dogs-behavior-genetic-impulses-canines-dna-influence/" rel="nofollow">www.annarbor.com/pets/dogs-behavior-genetic-impulses-cani...</a>
• <a href="http://www.blogher.com/snippets/re-homing-beloved-pet" rel="nofollow">www.blogher.com/snippets/re-homing-beloved-pet</a>


+--------+
|  TAGS  |
+--------+
"German Shepherd" dog Alsatian UsedBySomeone 