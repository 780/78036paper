+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aegidian
Photo URL    : https://www.flickr.com/photos/aegidian/9116629214/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 23 12:33:07 GMT+0200 2013
Upload Date  : Sun Jun 23 14:40:51 GMT+0200 2013
Geotag Info  : Latitude:51.580333, Longitude:0.015666
Views        : 417
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bessy's got a brand new bag


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Bessy dog GSD "German Shepherd" backpack ezydog 