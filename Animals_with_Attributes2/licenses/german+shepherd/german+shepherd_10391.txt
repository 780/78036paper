+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : VirtualWolf
Photo URL    : https://www.flickr.com/photos/virtualwolf/18142442183/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 13 11:39:59 GMT+0200 2015
Upload Date  : Sat Jun 13 11:42:55 GMT+0200 2015
Geotag Info  : Latitude:-33.809099, Longitude:150.764023
Views        : 1,569
Comments     : 0


+---------+
|  TITLE  |
+---------+
German shepherd


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Events Animal Techniques 2015 "Fujifilm X100S" Equipment Dog Domesticated Bokeh "Dogs on Show" "German shepherd" 