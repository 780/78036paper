+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : andy_carter
Photo URL    : https://www.flickr.com/photos/salsaboy/2479976779/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 12 14:41:43 GMT+0200 2008
Upload Date  : Sat May 10 17:08:47 GMT+0200 2008
Views        : 233
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe x2


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Africa giraffe two "south Africa" safari animals bush nature 