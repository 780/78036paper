+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : wolfsavard
Photo URL    : https://www.flickr.com/photos/wolfsavard/3875017189/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 25 15:48:26 GMT+0200 2009
Upload Date  : Mon Aug 31 22:07:18 GMT+0200 2009
Geotag Info  : Latitude:34.147804, Longitude:-118.290331
Views        : 202
Comments     : 0


+---------+
|  TITLE  |
+---------+
giraffes


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"los angeles zoo" "griffith park" giraffe 