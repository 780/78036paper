+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bobosh_t
Photo URL    : https://www.flickr.com/photos/frted/6760558045/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 24 23:29:39 GMT+0100 2012
Upload Date  : Wed Jan 25 16:31:39 GMT+0100 2012
Geotag Info  : Latitude:33.098875, Longitude:-116.997485
Views        : 115
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_0566


+---------------+
|  DESCRIPTION  |
+---------------+
Giraffe


+--------+
|  TAGS  |
+--------+
"San Diego Zoo Safari Park" animals mammals Giraffe Gifaffes 