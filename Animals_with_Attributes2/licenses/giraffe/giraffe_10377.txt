+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : shannonkringen
Photo URL    : https://www.flickr.com/photos/shannonkringen/1682775031/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Oct 9 11:03:52 GMT+0200 2007
Upload Date  : Mon Oct 22 05:48:20 GMT+0200 2007
Views        : 428
Comments     : 0


+---------+
|  TITLE  |
+---------+
giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo animals nature mammals "new jersey" "cape main" "shannon kringen" "goddess kring" synchronicity synkringnicity serendipity magic peace love joy art shannon kringen "multi media" bliss passion spark imagination goddess godus "the goddesskring" "the goddess kringen" shannonnicolekringen "art for sale" "buy photo prints" "buy art" kring kringgoddess explore www.shannonkringen.com shannonkringen.com artist 