+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/4512124481/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 10 12:26:51 GMT+0200 2010
Upload Date  : Mon Apr 12 01:00:23 GMT+0200 2010
Geotag Info  : Latitude:42.064268, Longitude:-71.586366
Views        : 1,047
Comments     : 0


+---------+
|  TITLE  |
+---------+
Why did the Giraffe play basketball?


+---------------+
|  DESCRIPTION  |
+---------------+
because it was a good dribbler! (look closely)


+--------+
|  TAGS  |
+--------+
southwick zoo giraffe drink dribble 