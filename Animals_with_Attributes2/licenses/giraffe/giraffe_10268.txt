+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lee P Yang
Photo URL    : https://www.flickr.com/photos/hd7/9661329374/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 12 15:20:55 GMT+0200 2012
Upload Date  : Tue Sep 3 05:17:50 GMT+0200 2013
Views        : 260
Comments     : 0


+---------+
|  TITLE  |
+---------+
giraffe headshot


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
giraffe neck head top zoo cpl polarizer 200mm canon 70-200 