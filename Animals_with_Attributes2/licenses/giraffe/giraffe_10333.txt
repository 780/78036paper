+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ruth and Dave
Photo URL    : https://www.flickr.com/photos/ruthanddave/53085277/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 16 14:45:43 GMT+0200 2005
Upload Date  : Sun Oct 16 21:26:18 GMT+0200 2005
Geotag Info  : Latitude:51.853488, Longitude:-0.546398
Views        : 660
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
giraffe whipsnade zoos 