+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : meaduva
Photo URL    : https://www.flickr.com/photos/meaduva/29732445/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 15 14:54:12 GMT+0200 2005
Upload Date  : Sat Jul 30 20:33:04 GMT+0200 2005
Views        : 56
Comments     : 0


+---------+
|  TITLE  |
+---------+
giraffes


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo "July 15, 2005" giraffe 