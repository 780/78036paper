+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ChrisHConnelly
Photo URL    : https://www.flickr.com/photos/c_conn/6003755992/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 15 21:32:21 GMT+0200 2011
Upload Date  : Wed Aug 3 01:51:19 GMT+0200 2011
Views        : 222
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
As opposed to another animal of course


+--------+
|  TAGS  |
+--------+
"Pittsburgh Zoo" animals zoo Pittsburgh summer giraffe 