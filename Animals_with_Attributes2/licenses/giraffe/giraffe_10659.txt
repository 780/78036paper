+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andreas Buthmann
Photo URL    : https://www.flickr.com/photos/andreas_buthmann/9053511846/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 25 11:56:48 GMT+0200 2013
Upload Date  : Sat Jun 15 23:08:57 GMT+0200 2013
Views        : 37
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"ZOOM Erlebniswelt" 