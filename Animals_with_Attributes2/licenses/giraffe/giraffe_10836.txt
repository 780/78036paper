+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Elecé
Photo URL    : https://www.flickr.com/photos/luisdcarbia/5237939187/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 6 17:37:53 GMT+0100 2010
Upload Date  : Mon Dec 6 17:37:53 GMT+0100 2010
Views        : 67
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffes


+---------------+
|  DESCRIPTION  |
+---------------+
Giraffes (Giraffa camelopardis) dining at Murchison National Park, Uganda.


+--------+
|  TAGS  |
+--------+
(no tags)