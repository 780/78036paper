+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : AZAdam
Photo URL    : https://www.flickr.com/photos/azadam/86749557/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 14 02:20:31 GMT+0100 2006
Upload Date  : Sun Jan 15 08:01:59 GMT+0100 2006
Views        : 369
Comments     : 0


+---------+
|  TITLE  |
+---------+
Reticulated Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo "phoenix zoo" animal animals giraffe "Reticulated Giraffe" takenbyjeni 