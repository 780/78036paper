+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : spamdangler
Photo URL    : https://www.flickr.com/photos/justcallmehillsy/5050859106/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 27 07:17:43 GMT+0200 2010
Upload Date  : Mon Oct 4 13:20:03 GMT+0200 2010
Geotag Info  : Latitude:-34.194197, Longitude:21.637058
Views        : 1,443
Comments     : 0


+---------+
|  TITLE  |
+---------+
Young Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Garden Route Game Park" giraffe 