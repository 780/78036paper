+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Stuart.Bassil
Photo URL    : https://www.flickr.com/photos/93014478@N00/3358896379/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 23 15:50:02 GMT+0200 2008
Upload Date  : Mon Mar 16 11:46:13 GMT+0100 2009
Views        : 544
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"kruger national park" giraffe 