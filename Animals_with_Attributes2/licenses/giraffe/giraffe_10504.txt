+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Bohemianism
Photo URL    : https://www.flickr.com/photos/bohemianism/4250130217/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 23 10:23:07 GMT+0200 2009
Upload Date  : Wed Jan 6 12:25:25 GMT+0100 2010
Geotag Info  : Latitude:28.226629, Longitude:-80.713977
Views        : 661
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Giraffe at the Brevard Zoo in Melbourne, FL.

The sun shining through the branches gives this giraffe a look of almost otherworldy divinity, as if the heavens themselves are shining directly down upon them.


+--------+
|  TAGS  |
+--------+
giraffe brevard brevardzoo melbourne "melbourne fl" "melbourne florida" fl florida zoo animal 