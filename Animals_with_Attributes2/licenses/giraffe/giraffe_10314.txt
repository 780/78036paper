+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Steve Slater (used to be Wildlife Encounters)
Photo URL    : https://www.flickr.com/photos/wildlife_encounters/13868741275/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 13 12:09:51 GMT+0100 2013
Upload Date  : Tue Apr 15 13:19:12 GMT+0200 2014
Views        : 1,114
Comments     : 0


+---------+
|  TITLE  |
+---------+
giraffe standing tall


+---------------+
|  DESCRIPTION  |
+---------------+
giraffe standing tall


+--------+
|  TAGS  |
+--------+
"South Africa" giraffe mammals "mammals of africa" nature wildlife 