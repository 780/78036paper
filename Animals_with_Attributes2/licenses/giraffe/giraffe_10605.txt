+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Matt Biddulph
Photo URL    : https://www.flickr.com/photos/mbiddulph/7093114187/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 16 08:20:22 GMT+0200 2012
Upload Date  : Thu Apr 19 13:34:21 GMT+0200 2012
Views        : 884
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"masai mara" kenya safari giraffe 