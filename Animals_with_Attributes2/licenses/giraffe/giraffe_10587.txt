+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Carsten aus Bonn
Photo URL    : https://www.flickr.com/photos/zickzangel/6309907081/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Oct 12 11:48:29 GMT+0200 2011
Upload Date  : Thu Nov 3 22:20:51 GMT+0100 2011
Views        : 122
Comments     : 1


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"south africa" 