+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ben124.
Photo URL    : https://www.flickr.com/photos/ben124/5272655650/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 7 11:57:06 GMT+0200 2009
Upload Date  : Sun Dec 19 01:04:43 GMT+0100 2010
Views        : 2,771
Comments     : 8


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at Khristiansand Zoo (Dyreparken) in June 2009.


+--------+
|  TAGS  |
+--------+
animal giraffe portrait face eyes ears 