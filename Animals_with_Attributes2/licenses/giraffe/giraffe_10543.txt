+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pyntofmyld
Photo URL    : https://www.flickr.com/photos/pyntofmyld/5068163939/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 10 12:54:18 GMT+0200 2010
Upload Date  : Sun Oct 10 20:29:25 GMT+0200 2010
Geotag Info  : Latitude:52.442972, Longitude:1.023572
Views        : 132
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo "banham zoo" 