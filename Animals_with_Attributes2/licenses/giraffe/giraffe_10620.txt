+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Shehan Obeysekera
Photo URL    : https://www.flickr.com/photos/mshehan/7920221928/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 3 14:51:02 GMT+0200 2012
Upload Date  : Mon Sep 3 12:13:13 GMT+0200 2012
Views        : 66
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
At the Dehiwala Zoo, Sri Lanka


+--------+
|  TAGS  |
+--------+
(no tags)