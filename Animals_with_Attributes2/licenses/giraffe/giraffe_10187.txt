+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : 4nitsirk
Photo URL    : https://www.flickr.com/photos/4nitsirk/5226059566/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 1 18:13:29 GMT+0100 2010
Upload Date  : Thu Dec 2 10:02:08 GMT+0100 2010
Geotag Info  : Latitude:-41.322359, Longitude:174.784358
Views        : 497
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe (1)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
giraffe zoodoo wellington zoo 