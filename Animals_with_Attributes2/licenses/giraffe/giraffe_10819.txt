+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mmmavocado
Photo URL    : https://www.flickr.com/photos/mmmavocado/3837920154/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 7 10:16:32 GMT+0200 2009
Upload Date  : Wed Aug 19 22:03:36 GMT+0200 2009
Views        : 74
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffes


+---------------+
|  DESCRIPTION  |
+---------------+
Sabi Sands area, Kruger.


+--------+
|  TAGS  |
+--------+
"South Africa" Africa 