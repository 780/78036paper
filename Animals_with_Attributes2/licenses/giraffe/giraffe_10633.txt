+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kathrin_mezger
Photo URL    : https://www.flickr.com/photos/26094756@N04/8235744818/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 27 12:28:54 GMT+0200 2009
Upload Date  : Sat Dec 1 19:21:34 GMT+0100 2012
Views        : 40
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Wilhelma Stuttgart zoo Tierpark zoologischer Garten Germany Deutschland Tiere Animals 