+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brainstorm1984
Photo URL    : https://www.flickr.com/photos/brainstorm1984/8208707681/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Nov 15 07:24:48 GMT+0100 2012
Upload Date  : Thu Nov 22 22:35:26 GMT+0100 2012
Geotag Info  : Latitude:-28.107493, Longitude:32.073812
Views        : 181
Comments     : 1


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Safari Südafrika "South Africa" Giraffe Hluhluwe-Umfolozi Wildlife "Giraffa camelopardalis" Imfolozi-Hluhluwe KwaZulu-Natal Hluhluwe 