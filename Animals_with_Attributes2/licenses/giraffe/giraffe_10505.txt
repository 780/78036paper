+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : phalinn
Photo URL    : https://www.flickr.com/photos/phalinn/4257083036/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 1 14:04:04 GMT+0100 2010
Upload Date  : Fri Jan 8 17:09:43 GMT+0100 2010
Geotag Info  : Latitude:3.207832, Longitude:101.756486
Views        : 1,286
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Zirafah


+--------+
|  TAGS  |
+--------+
animals haiwan binatang flora fauna wildlife nature zoo negara malaysia kl kuala lumpur selangor asia photography phalinn canon eos 400d gambar bird burung giraffe zirafah deer pigs boar elephants puma cats crocs 