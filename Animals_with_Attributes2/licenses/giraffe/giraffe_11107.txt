+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Just chaos
Photo URL    : https://www.flickr.com/photos/7326810@N08/3327620504/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 3 14:08:01 GMT+0100 2008
Upload Date  : Wed Mar 4 04:29:23 GMT+0100 2009
Views        : 53
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rothschild giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Giraffa camelopardalis rothschildi


+--------+
|  TAGS  |
+--------+
(no tags)