+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : oldandsolo
Photo URL    : https://www.flickr.com/photos/shankaronline/7513600584/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 1 00:00:00 GMT+0100 2010
Upload Date  : Fri Jul 6 11:43:07 GMT+0200 2012
Views        : 1,078
Comments     : 0


+---------+
|  TITLE  |
+---------+
Masai Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Dial G for Giraffe. Close up of a giraffe browsing the tender top shoots of a tree. He was in a ditch beside the road, so the top shoots were not too high from our level. Masai Mara Game Reserve, Kenya, E. Africa,Nov. 2010.


+--------+
|  TAGS  |
+--------+
Kenya "East Africa" "African wildlife" "wildlife photography" "Masai Mara" "Masai Mara game reserve" "Masai Mara National Park" ungulates giraffe "Masai Giraffe" 