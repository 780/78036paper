+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Snake3yes
Photo URL    : https://www.flickr.com/photos/snake3yes/229891787/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 15 15:21:46 GMT+0200 2006
Upload Date  : Thu Aug 31 10:27:03 GMT+0200 2006
Views        : 806
Comments     : 2


+---------+
|  TITLE  |
+---------+
Giraffe Herd


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
giraffe africa kenya wildlife samburu 