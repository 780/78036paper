+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Terrence Franck
Photo URL    : https://www.flickr.com/photos/terrencefranck/5328378578/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 4 03:10:14 GMT+0200 2008
Upload Date  : Wed Jan 5 22:34:52 GMT+0100 2011
Geotag Info  : Latitude:-23.915470, Longitude:31.470539
Views        : 339
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kruger National Park


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
africa "kruger national park" giraffe 