+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ckramer
Photo URL    : https://www.flickr.com/photos/ckramer/8211984841/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 23 08:20:40 GMT+0100 2012
Upload Date  : Sat Nov 24 04:39:58 GMT+0100 2012
Views        : 81
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Seen on Kilimanjaro Safari at Disney's Animal Kingdom in Walt Disney World


+--------+
|  TAGS  |
+--------+
(no tags)