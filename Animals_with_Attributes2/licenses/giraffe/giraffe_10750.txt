+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : _somaholiday
Photo URL    : https://www.flickr.com/photos/75872360@N08/14625664916/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 20 12:00:04 GMT+0100 2013
Upload Date  : Mon Jul 14 03:27:54 GMT+0200 2014
Geotag Info  : Latitude:32.738771, Longitude:-96.816029
Views        : 136
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe, Dallas Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Giraffe, Dallas Zoo


+--------+
|  TAGS  |
+--------+
animals zoo ungulates 