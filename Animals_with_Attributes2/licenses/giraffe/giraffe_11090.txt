+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tiswango
Photo URL    : https://www.flickr.com/photos/tiswango/459318502/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 14 14:23:21 GMT+0200 2007
Upload Date  : Sun Apr 15 04:23:03 GMT+0200 2007
Geotag Info  : Latitude:25.604427, Longitude:-80.400701
Views        : 405
Comments     : 0


+---------+
|  TITLE  |
+---------+
Reticulated Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Metro Zoo" Giraffe Zoo Animals "Family Fun" Miami Florida "Miami Metro Zoo" 