+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : TexasDarkHorse
Photo URL    : https://www.flickr.com/photos/utahdarkhorse/3473222107/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 24 13:32:25 GMT+0200 2009
Upload Date  : Sat Apr 25 19:44:12 GMT+0200 2009
Geotag Info  : Latitude:29.709226, Longitude:-95.396347
Views        : 46
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Kim &amp; I visited the Houston Zoo on her birthday.


+--------+
|  TAGS  |
+--------+
Houston Zoo 