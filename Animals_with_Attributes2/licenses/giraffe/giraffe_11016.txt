+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Marcel Oosterwijk
Photo URL    : https://www.flickr.com/photos/wackelijmrooster/5014508606/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 5 17:31:15 GMT+0200 2010
Upload Date  : Wed Sep 22 12:31:35 GMT+0200 2010
Geotag Info  : Latitude:-1.485047, Longitude:35.173416
Views        : 535
Comments     : 0


+---------+
|  TITLE  |
+---------+
Masai Mara - Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2010 Africa Afrika Kenia Kenya Tanzania wildlife safari tarangire "lake manyara" ngorongoro ngoron goro crater krater dieren beesten animals wild savanne prey 