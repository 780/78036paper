+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : osseous
Photo URL    : https://www.flickr.com/photos/osseous/5953426576/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 2 15:52:26 GMT+0200 2011
Upload Date  : Tue Jul 19 06:35:11 GMT+0200 2011
Views        : 159
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zoo Miami July 2, 2011


+---------------+
|  DESCRIPTION  |
+---------------+
A day at the zoo.


+--------+
|  TAGS  |
+--------+
"2011 july" miami zoo giraffe 