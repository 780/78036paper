+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rosshuggett
Photo URL    : https://www.flickr.com/photos/rosshuggett/8076531229/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 17 13:32:39 GMT+0200 2012
Upload Date  : Thu Oct 11 12:00:26 GMT+0200 2012
Geotag Info  : Latitude:-18.790201, Longitude:24.646300
Views        : 262
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
botswana chobe giraffe national park 