+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Derek Keats
Photo URL    : https://www.flickr.com/photos/dkeats/4193405362/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 17 14:33:40 GMT+0100 2009
Upload Date  : Thu Dec 17 19:46:04 GMT+0100 2009
Views        : 79
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Visit to lion park, Johannesburg, December 17 2009


+--------+
|  TAGS  |
+--------+
lionpark 