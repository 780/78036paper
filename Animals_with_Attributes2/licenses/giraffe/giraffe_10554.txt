+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ThomasKohler
Photo URL    : https://www.flickr.com/photos/mecklenburg/5390850299/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 19 12:36:41 GMT+0100 2011
Upload Date  : Wed Jan 26 23:03:01 GMT+0100 2011
Geotag Info  : Latitude:53.606206, Longitude:11.440930
Views        : 528
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Zoo Schwerin


+--------+
|  TAGS  |
+--------+
zoo tiergarten tierpark schwerin sn zoologisch zoologischer garten Camelopardalis Giraffa afrika africa giraffe tier animal 