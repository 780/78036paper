+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Svadilfari
Photo URL    : https://www.flickr.com/photos/22280677@N07/2699934902/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 14 22:11:06 GMT+0200 2008
Upload Date  : Fri Jul 25 00:41:18 GMT+0200 2008
Views        : 420
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
A giraffe at the Roger Williams Park Zoo in Providence, Rhode Island.


+--------+
|  TAGS  |
+--------+
"Providence Rhode Island" "Providence RI" Providence "Rhode Island" RI "Roger Williams Park Zoo" "Roger Williams Zoo" "Roger Williams" Zoo Animal Giraffe "Stone Wall" "New England" 