+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brixendk
Photo URL    : https://www.flickr.com/photos/brixendk/8397976639/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 29 09:27:45 GMT+0100 2012
Upload Date  : Sun Jan 20 17:28:42 GMT+0100 2013
Views        : 173
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Tanzania Katavi Animal 