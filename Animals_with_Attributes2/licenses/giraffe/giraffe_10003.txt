+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/8094467364/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Sep 27 14:15:05 GMT+0200 2012
Upload Date  : Tue Oct 16 20:01:16 GMT+0200 2012
Views        : 227
Comments     : 0


+---------+
|  TITLE  |
+---------+
Twycross Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Giraffes


+--------+
|  TAGS  |
+--------+
"Twycross Zoo" Giraffes 