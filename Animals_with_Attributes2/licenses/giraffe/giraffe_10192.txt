+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : NH53
Photo URL    : https://www.flickr.com/photos/nh53/5144133538/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 28 07:48:58 GMT+0100 2006
Upload Date  : Wed Nov 3 22:34:51 GMT+0100 2010
Geotag Info  : Latitude:-1.628503, Longitude:34.342746
Views        : 3,704
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe (Serengeti)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
giraffe serengeti 