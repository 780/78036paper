+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neil T
Photo URL    : https://www.flickr.com/photos/neilt/14267283414/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 19 12:17:04 GMT+0200 2014
Upload Date  : Sun May 25 20:08:09 GMT+0200 2014
Geotag Info  : Latitude:53.354111, Longitude:-6.305551
Views        : 1,224
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffes


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dublinzoo zoo giraffe giraffes 