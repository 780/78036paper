+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mister-E
Photo URL    : https://www.flickr.com/photos/mister-e/2392594047/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 30 16:38:10 GMT+0100 2008
Upload Date  : Sun Apr 6 20:15:11 GMT+0200 2008
Geotag Info  : Latitude:-24.073497, Longitude:31.689851
Views        : 373
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Kruger "Kruger National Park" "South Africa" "game reserve" giraffe 