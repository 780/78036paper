+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : amitp
Photo URL    : https://www.flickr.com/photos/amitp/12387564974/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 5 15:17:07 GMT+0100 2014
Upload Date  : Sat Feb 8 17:13:01 GMT+0100 2014
Views        : 2,935
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe contemplating


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
creatures giraffe safariwest e-m1 