+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : joshkehn
Photo URL    : https://www.flickr.com/photos/joshkehn/9368424783/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 18 16:49:09 GMT+0200 2013
Upload Date  : Fri Jul 26 15:16:26 GMT+0200 2013
Views        : 312
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
africa kenya travel 