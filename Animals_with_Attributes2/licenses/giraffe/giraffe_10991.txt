+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/8428020041/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 12 16:29:11 GMT+0100 2013
Upload Date  : Wed Jan 30 04:51:18 GMT+0100 2013
Geotag Info  : Latitude:25.609216, Longitude:-80.403034
Views        : 842
Comments     : 0


+---------+
|  TITLE  |
+---------+
Little Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoomiami zoo miami florida reticulated giraffe juvenile 