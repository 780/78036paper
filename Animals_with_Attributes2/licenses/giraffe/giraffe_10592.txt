+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : llee_wu
Photo URL    : https://www.flickr.com/photos/13523064@N03/6580131975/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 27 12:28:46 GMT+0100 2010
Upload Date  : Tue Dec 27 08:38:37 GMT+0100 2011
Views        : 60
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2010 Chimelong safari park animal 