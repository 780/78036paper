+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8324904654/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 8 12:05:53 GMT+0200 2012
Upload Date  : Sun Dec 30 06:29:01 GMT+0100 2012
Geotag Info  : Latitude:47.223575, Longitude:8.821763
Views        : 2,090
Comments     : 4


+---------+
|  TITLE  |
+---------+
Long neck giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Nothing else as the portrait of a giraffe...


+--------+
|  TAGS  |
+--------+
portrait face head neck kinderzoo zoo knie rapperswil switzerland nikon d4 mfcc 