+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Vanessa Pike-Russell
Photo URL    : https://www.flickr.com/photos/lilcrabbygal/376528044/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Oct 17 11:58:01 GMT+0200 2006
Upload Date  : Thu Feb 1 16:07:34 GMT+0100 2007
Views        : 1,301
Comments     : 0


+---------+
|  TITLE  |
+---------+
False depth of field on giraffe photo


+---------------+
|  DESCRIPTION  |
+---------------+
2006_1017_115801_giraffe_tr2


+--------+
|  TAGS  |
+--------+
sydney tarongazoo animal telephoto oob background photoshopped fujifinepix s5600 giraffe tall 