+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mountain/\Ash
Photo URL    : https://www.flickr.com/photos/mountainash/2622729788/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 24 07:33:50 GMT+0200 2008
Upload Date  : Mon Jun 30 00:06:27 GMT+0200 2008
Geotag Info  : Latitude:-32.271530, Longitude:148.585453
Views        : 54
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
westernplainszoo zoo dubbo holiday 