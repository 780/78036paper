+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Becker1999
Photo URL    : https://www.flickr.com/photos/becker271/2908029344/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 28 15:43:51 GMT+0200 2008
Upload Date  : Thu Oct 2 20:25:41 GMT+0200 2008
Views        : 113
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
&quot;The Wilds&quot; Photo Camp 2008
Cumberland Ohio (9/26-9/28)


+--------+
|  TAGS  |
+--------+
"Paul's Pic" 2008 "wilds photo camp" "the wilds" photography camp Ohio "conservation center" Giraffe 