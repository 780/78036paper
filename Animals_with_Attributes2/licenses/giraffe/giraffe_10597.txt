+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike's Birds
Photo URL    : https://www.flickr.com/photos/pazzani/6608144559/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 10 01:47:06 GMT+0100 2011
Upload Date  : Sat Dec 31 20:14:44 GMT+0100 2011
Geotag Info  : Latitude:-24.796187, Longitude:31.498266
Views        : 96
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)