+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jdeeringdavis
Photo URL    : https://www.flickr.com/photos/hayesandjenn/3893418534/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 5 15:51:44 GMT+0200 2008
Upload Date  : Sun Sep 6 18:11:26 GMT+0200 2009
Views        : 92
Comments     : 0


+---------+
|  TITLE  |
+---------+
giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
texas wildlife "natural bridge wildlife ranch" zebra animals "hill country" 