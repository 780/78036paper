+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sdhansay
Photo URL    : https://www.flickr.com/photos/sdhansay/5422656464/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 29 14:10:11 GMT+0100 2011
Upload Date  : Sun Feb 6 20:33:53 GMT+0100 2011
Views        : 596
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra and Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
If you would like to use the picture please provide a link back to <a href="http://www.picturesquetraveller.com" rel="nofollow">www.picturesquetraveller.com</a>, if possible


+--------+
|  TAGS  |
+--------+
Zebra Giraffe picturesquetraveller 