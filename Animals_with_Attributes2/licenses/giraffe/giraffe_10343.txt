+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mister-E
Photo URL    : https://www.flickr.com/photos/mister-e/213518240/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 1 00:00:00 GMT+0100 2003
Upload Date  : Sun Aug 13 00:48:51 GMT+0200 2006
Geotag Info  : Latitude:-24.242268, Longitude:31.749114
Views        : 103
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Kruger National Park" Kruger "South Africa" 