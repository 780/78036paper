+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : oldandsolo
Photo URL    : https://www.flickr.com/photos/shankaronline/7513261430/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 1 00:00:00 GMT+0100 2010
Upload Date  : Fri Jul 6 10:57:39 GMT+0200 2012
Views        : 3,623
Comments     : 0


+---------+
|  TITLE  |
+---------+
Masai Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
A closer look at the Masai Giraffe before he moves in towards the bush. (Oct/ Nov 2010)


+--------+
|  TAGS  |
+--------+
Kenya "East Africa" "African wildlife" "wildlife photography" "Masai Mara" "Masai Mara game reserve" "Masai Mara National Park" ungulates giraffe "Masai Giraffe" 