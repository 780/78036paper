+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Stephen Kruso
Photo URL    : https://www.flickr.com/photos/is0crazy/11813044335/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 3 13:27:24 GMT+0100 2014
Upload Date  : Tue Jan 7 07:08:18 GMT+0100 2014
Views        : 931
Comments     : 0


+---------+
|  TITLE  |
+---------+
Baby Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"San Diego Zoo" giraffe baby Bahati newborn mama kiss animals 