+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : John5199
Photo URL    : https://www.flickr.com/photos/jonnyb558/7006253476/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 5 12:18:13 GMT+0200 2012
Upload Date  : Mon May 7 15:13:46 GMT+0200 2012
Geotag Info  : Latitude:51.799379, Longitude:-1.641769
Views        : 102
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at Cotswold Wildlife Park, May 2012


+--------+
|  TAGS  |
+--------+
D3000 "cotswold wildlife park" "nikon 70-300" wildlife burford oxfordshire nikon 