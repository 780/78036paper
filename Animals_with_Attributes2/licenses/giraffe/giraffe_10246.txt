+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : puliarf
Photo URL    : https://www.flickr.com/photos/puliarfanita/3776383684/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 30 14:37:38 GMT+0200 2009
Upload Date  : Sat Aug 1 00:02:02 GMT+0200 2009
Views        : 3,470
Comments     : 2


+---------+
|  TITLE  |
+---------+
Giraffe Duo


+---------------+
|  DESCRIPTION  |
+---------------+
Posing nicely, 2 giraffes (Giraffa camelopardalis). The giraffe is an African even-toed ungulate mammal , the tallest of all land-living animal species seen here at the Santa Barbara Zoo <a href="http://www.santabarbarazoo.org/default.asp" rel="nofollow">www.santabarbarazoo.org/default.asp</a>


+--------+
|  TAGS  |
+--------+
"santa barbara" "santa barbara zoo" Lion "bald eagle" condor "california condor" macaw meerkat giraffe nikon d700 FX 