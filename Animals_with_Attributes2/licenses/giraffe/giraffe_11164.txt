+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KierenAris
Photo URL    : https://www.flickr.com/photos/mopuss/4712670940/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 14 11:48:59 GMT+0200 2010
Upload Date  : Fri Jun 18 21:56:36 GMT+0200 2010
Geotag Info  : Latitude:51.535017, Longitude:-0.153830
Views        : 59
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"London Zoo" Giraffe 