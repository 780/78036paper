+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mariahagglof
Photo URL    : https://www.flickr.com/photos/mariahagglof/8523181919/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 1 11:00:08 GMT+0100 2013
Upload Date  : Sun Mar 3 11:36:08 GMT+0100 2013
Views        : 111
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe Center, Nairobi


+---------------+
|  DESCRIPTION  |
+---------------+
Rothschild giraffe


+--------+
|  TAGS  |
+--------+
(no tags)