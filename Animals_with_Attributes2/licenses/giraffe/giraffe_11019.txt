+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : K_Dafalias
Photo URL    : https://www.flickr.com/photos/72906133@N00/6584365865/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 28 00:04:20 GMT+0100 2011
Upload Date  : Wed Dec 28 00:04:20 GMT+0100 2011
Views        : 134
Comments     : 0


+---------+
|  TITLE  |
+---------+
Massai-Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
The Massai Giraffe is somewhat darker in color


+--------+
|  TAGS  |
+--------+
(no tags)