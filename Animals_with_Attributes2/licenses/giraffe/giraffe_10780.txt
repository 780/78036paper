+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : heatherlyone
Photo URL    : https://www.flickr.com/photos/heatherlyone/1167262083/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 19 03:44:06 GMT+0200 2007
Upload Date  : Thu Dec 8 10:32:13 GMT+0100 2005
Geotag Info  : Latitude:-1.109549, Longitude:35.573730
Views        : 118
Comments     : 0


+---------+
|  TITLE  |
+---------+
giraffes 3


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Africa "East Africa" giraffes "masai mara" head "close up" 