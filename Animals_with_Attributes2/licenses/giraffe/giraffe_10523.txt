+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jim, the Photographer
Photo URL    : https://www.flickr.com/photos/jcapaldi/4749969701/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 27 15:06:38 GMT+0200 2010
Upload Date  : Thu Jul 1 01:46:54 GMT+0200 2010
Geotag Info  : Latitude:39.952270, Longitude:-75.162368
Views        : 81
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Philadelphia Zoo" zoo 