+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nzgabriel
Photo URL    : https://www.flickr.com/photos/nzgabriel/8520983928/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 2 10:16:51 GMT+0100 2013
Upload Date  : Sat Mar 2 09:17:52 GMT+0100 2013
Views        : 3,065
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffes


+---------------+
|  DESCRIPTION  |
+---------------+
Giraffes at Wellington Zoo, New Zealand.


+--------+
|  TAGS  |
+--------+
wellington zoo wellingtonzoo animal animals newzealand giraffe giraffes 