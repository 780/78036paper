+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : edans
Photo URL    : https://www.flickr.com/photos/edans/677720300/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 30 17:22:03 GMT+0200 2007
Upload Date  : Sun Jul 1 02:59:10 GMT+0200 2007
Views        : 551
Comments     : 1


+---------+
|  TITLE  |
+---------+
Jirafa - IV


+---------------+
|  DESCRIPTION  |
+---------------+
Dos jirafos en una actitud de lo más íntima...


+--------+
|  TAGS  |
+--------+
zoo animals animales fauna jirafa giraffe 