+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Stephen Kelly Photography
Photo URL    : https://www.flickr.com/photos/skellysf/8536655701/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 1 13:12:12 GMT+0100 2013
Upload Date  : Thu Mar 7 20:34:26 GMT+0100 2013
Views        : 43
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)