+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/1172798751/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 1 16:28:05 GMT+0200 2005
Upload Date  : Sun Aug 19 20:30:28 GMT+0200 2007
Geotag Info  : Latitude:-1.535155, Longitude:35.469360
Views        : 82,826
Comments     : 44


+---------+
|  TITLE  |
+---------+
Walking giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
Giraffe walking in the plains of the Masai Mara.


+--------+
|  TAGS  |
+--------+
kenya safari giraffe walking plains "masai mara" tree grass ABigFave Ysplix PhotoFaceOffWinner NaturesElegantShots PFOGold 