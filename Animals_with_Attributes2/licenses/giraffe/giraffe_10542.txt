+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pyntofmyld
Photo URL    : https://www.flickr.com/photos/pyntofmyld/5068161627/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 10 12:52:55 GMT+0200 2010
Upload Date  : Sun Oct 10 20:28:46 GMT+0200 2010
Geotag Info  : Latitude:52.442969, Longitude:1.023574
Views        : 109
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo "banham zoo" 