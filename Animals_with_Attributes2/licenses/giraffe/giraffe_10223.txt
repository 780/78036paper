+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Shayan (USA)
Photo URL    : https://www.flickr.com/photos/ssanyal/2168498088/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 5 01:21:34 GMT+0100 2008
Upload Date  : Sat Jan 5 10:21:34 GMT+0100 2008
Views        : 583
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe at Taronga Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Taken in 2000 with my ol' DSC 2.1 Megapixel. Did some selective leveling in Photoshop.


+--------+
|  TAGS  |
+--------+
giraffe sydney taronga zoo "taronga zoo" opera "opera house" "sydney opera" "circular quay" 