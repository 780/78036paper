+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gisela gerson lohman-braun
Photo URL    : https://www.flickr.com/photos/giselaglb/15397896398/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 30 13:09:02 GMT+0200 2014
Upload Date  : Mon Oct 20 17:36:25 GMT+0200 2014
Views        : 196
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)