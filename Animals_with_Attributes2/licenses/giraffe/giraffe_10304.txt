+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Elliot Moore
Photo URL    : https://www.flickr.com/photos/elliotmoore/155910763/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 28 15:33:19 GMT+0200 2006
Upload Date  : Tue May 30 00:26:11 GMT+0200 2006
Views        : 65
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe pose


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Animals Whipsnade zoo 