+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Chris Makarsky
Photo URL    : https://www.flickr.com/photos/cmak/6615267831/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 1 00:00:00 GMT+0100 2011
Upload Date  : Sun Jan 1 23:10:45 GMT+0100 2012
Geotag Info  : Latitude:-0.667404, Longitude:35.639648
Views        : 373
Comments     : 0


+---------+
|  TITLE  |
+---------+
giraffes


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
RAW africa safari kenya giraffe 