+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flowcomm
Photo URL    : https://www.flickr.com/photos/flowcomm/2769167702/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 15 07:44:51 GMT+0200 2008
Upload Date  : Sat Aug 16 23:11:33 GMT+0200 2008
Views        : 1,290
Comments     : 1


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
giraffe nature africa wildlife 