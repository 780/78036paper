+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Just chaos
Photo URL    : https://www.flickr.com/photos/7326810@N08/2315194559/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 6 11:50:38 GMT+0100 2008
Upload Date  : Fri Mar 7 04:18:20 GMT+0100 2008
Views        : 25
Comments     : 0


+---------+
|  TITLE  |
+---------+
Reticulated Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
SanFranciscozoo zoo 