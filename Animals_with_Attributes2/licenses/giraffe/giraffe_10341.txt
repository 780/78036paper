+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ForsterFoto
Photo URL    : https://www.flickr.com/photos/forsterfoto/168970617/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Feb 16 11:38:00 GMT+0100 2005
Upload Date  : Sat Jun 17 18:39:19 GMT+0200 2006
Views        : 424
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giraffe


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Masai Mara Kenya Giraffe Safari 