+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Matt Biddulph
Photo URL    : https://www.flickr.com/photos/mbiddulph/7093173681/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 17 09:14:01 GMT+0200 2012
Upload Date  : Thu Apr 19 14:10:10 GMT+0200 2012
Views        : 448
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sitting giraffes


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"masai mara" kenya safari giraffe 