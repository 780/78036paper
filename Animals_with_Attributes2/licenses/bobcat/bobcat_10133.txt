+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/25933056592/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 27 15:21:59 GMT+0100 2016
Upload Date  : Fri Mar 25 14:00:11 GMT+0100 2016
Geotag Info  : Latitude:47.050651, Longitude:8.554229
Views        : 1,193
Comments     : 0


+---------+
|  TITLE  |
+---------+
A very pretty lynx posing!


+---------------+
|  DESCRIPTION  |
+---------------+
Last picture of this series: a beautiful lynx portrait for you!


+--------+
|  TAGS  |
+--------+
pretty beautiful portrait face calm watching attentive lynx wild cat european feline goldau "arth goldau" tierpark zoo switzerland nikon d4 