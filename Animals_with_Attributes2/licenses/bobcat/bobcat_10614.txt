+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Luke Hayfield Photography
Photo URL    : https://www.flickr.com/photos/altoexyl/3363856714/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Mar 17 15:32:04 GMT+0100 2009
Upload Date  : Tue Mar 17 21:42:04 GMT+0100 2009
Views        : 181
Comments     : 1


+---------+
|  TITLE  |
+---------+
The Lynx Effect... You're Doing it Wrong


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at Dudley Zoological Gardens


+--------+
|  TAGS  |
+--------+
Dudley Zoo Lynx Effect "70-300mm f/4-5.6G" 