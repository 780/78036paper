+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/15254045155/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 6 16:17:52 GMT+0200 2014
Upload Date  : Tue Sep 16 05:12:43 GMT+0200 2014
Geotag Info  : Latitude:42.462697, Longitude:-71.093636
Views        : 735
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lynx Kitten Teething on Branch


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
stone zoo massachusetts cat canada lynx kitten 