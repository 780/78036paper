+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Der Stefan
Photo URL    : https://www.flickr.com/photos/derstefan/4578801471/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 8 14:57:05 GMT+0100 2009
Upload Date  : Tue May 4 22:05:28 GMT+0200 2010
Views        : 214
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lynx


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo osnabrück animals luchs lynx 