+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Luke Hayfield Photography
Photo URL    : https://www.flickr.com/photos/altoexyl/3363855566/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Mar 17 11:59:25 GMT+0100 2009
Upload Date  : Tue Mar 17 21:41:36 GMT+0100 2009
Views        : 170
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dudley Zoo, Lynx


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Dudley Zoo Lynx "70-300mm f/4-5.6G" 