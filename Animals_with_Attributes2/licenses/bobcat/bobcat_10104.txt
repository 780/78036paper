+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/15879261981/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 28 18:41:36 GMT+0200 2014
Upload Date  : Wed Nov 26 06:00:30 GMT+0100 2014
Geotag Info  : Latitude:46.565115, Longitude:6.773371
Views        : 4,002
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lynx looking at the side


+---------------+
|  DESCRIPTION  |
+---------------+
Portrait of the male lynx looking at the side.


+--------+
|  TAGS  |
+--------+
profile portrait face looking side wild cat lynx male servion lausanne zoo switzerland nikon d4 