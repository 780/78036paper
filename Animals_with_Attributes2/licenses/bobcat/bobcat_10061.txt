+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8925670630/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 24 13:40:38 GMT+0100 2013
Upload Date  : Sun Jun 2 17:57:55 GMT+0200 2013
Geotag Info  : Latitude:47.291907, Longitude:8.529338
Views        : 4,379
Comments     : 4


+---------+
|  TITLE  |
+---------+
Lynx getting down the tree


+---------------+
|  DESCRIPTION  |
+---------------+
Funny picture of our male lynx getting down the tree in a quite acrobatic position!


+--------+
|  TAGS  |
+--------+
walking "getting down" acrobatic tree dangerous "upside down" lynx wild cat male feline zoo tierpark langenberg wildpark zürich switzerland nikon d4 