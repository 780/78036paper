+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8573582858/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 10 14:09:54 GMT+0100 2012
Upload Date  : Wed Mar 20 01:00:32 GMT+0100 2013
Geotag Info  : Latitude:47.050651, Longitude:8.554229
Views        : 8,839
Comments     : 99


+---------+
|  TITLE  |
+---------+
Profile of a lynx licking his nose


+---------------+
|  DESCRIPTION  |
+---------------+
This time the male lynx (you can recognize him because he has white eartufts) seen from profile.


+--------+
|  TAGS  |
+--------+
male profile portriat face ears eartufts licking nose tongue lynx wild cat feline tierpark arth goldau zoo switzerland nikon d4 