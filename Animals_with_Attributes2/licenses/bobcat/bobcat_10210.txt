+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neeta Lind
Photo URL    : https://www.flickr.com/photos/neeta_lind/7976413642/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 8 15:03:29 GMT+0200 2012
Upload Date  : Tue Sep 11 16:22:09 GMT+0200 2012
Views        : 177
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bobcat by Sally Cat


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
CA "Marin Headlands" bobcat 