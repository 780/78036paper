+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Thanks for 1.5 Million Views!!
Photo URL    : https://www.flickr.com/photos/chad_sparkes/21395636416/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 16 11:11:09 GMT+0200 2015
Upload Date  : Mon Sep 14 23:57:01 GMT+0200 2015
Views        : 2,832
Comments     : 21


+---------+
|  TITLE  |
+---------+
Sad Eyes


+---------------+
|  DESCRIPTION  |
+---------------+
Shot this bobcat laying on top of a barrel at Gatorland Orlando Florida during a rain storm.                    
<a href="https://www.facebook.com/ChadSparkesPhotography/" rel="nofollow">www.facebook.com/ChadSparkesPhotography/</a>


+--------+
|  TAGS  |
+--------+
bobcat animal wildlife nature chadsparkes canonpowershotsx160IS centralflorida ChadSparkesPhotography OrlandoFlorida Orlando Gatorland GatorlandOrlandoFlorida 