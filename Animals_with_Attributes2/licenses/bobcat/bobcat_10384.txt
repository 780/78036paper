+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Emmy_Animals
Photo URL    : https://www.flickr.com/photos/44711529@N04/5057144981/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 26 18:06:15 GMT+0200 2010
Upload Date  : Wed Oct 6 19:14:56 GMT+0200 2010
Geotag Info  : Latitude:51.744518, Longitude:-0.061269
Views        : 1,134
Comments     : 1


+---------+
|  TITLE  |
+---------+
Eurasian Lynx


+---------------+
|  DESCRIPTION  |
+---------------+
Eurasian Lynx at Paradise Wildlife Park, UK

The largest of the lynx family, the Eurasian lynx has powerful legs, with slightly longer hindlimbs adapted for springing. In common with other lynx species, the large ears are adorned with conspicuous black tufts, and the long cheek hair hangs down to form a facial ruff, appearing almost mane-like in winter.


+--------+
|  TAGS  |
+--------+
animals captive animal captivity cat eurasian feline paradisewildlifepark nature lynx mammal 