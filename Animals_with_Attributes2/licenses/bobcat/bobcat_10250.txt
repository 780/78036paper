+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Just chaos
Photo URL    : https://www.flickr.com/photos/7326810@N08/2146603431/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 28 11:39:38 GMT+0100 2007
Upload Date  : Sat Dec 29 22:20:13 GMT+0100 2007
Views        : 362
Comments     : 1


+---------+
|  TITLE  |
+---------+
Bobcat


+---------------+
|  DESCRIPTION  |
+---------------+
Lynx rufus


+--------+
|  TAGS  |
+--------+
Animalia Chordata Mammalia Carnivora Felidae "Lynx rufus" feline wildcat cat bobcat animal mammal zoo 