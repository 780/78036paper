+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8543125831/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 3 15:48:51 GMT+0100 2012
Upload Date  : Sun Mar 10 06:00:18 GMT+0100 2013
Geotag Info  : Latitude:47.105828, Longitude:6.822745
Views        : 3,420
Comments     : 3


+---------+
|  TITLE  |
+---------+
Sitting and posing female lynx


+---------------+
|  DESCRIPTION  |
+---------------+
Another picture of the lynx, this time sitting...


+--------+
|  TAGS  |
+--------+
sitting posing grass lynx big wild cat female "bois du petit château" "animal park" zoo la-chaux-de-fonds neuchâtel switzerland nikon d4 