+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ~Ealasaid~
Photo URL    : https://www.flickr.com/photos/fredrte/5421650506/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 22 16:28:03 GMT+0100 2011
Upload Date  : Sun Feb 6 14:49:57 GMT+0100 2011
Geotag Info  : Latitude:28.228406, Longitude:-80.714921
Views        : 44
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_0411


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Brevard Zoo" bobcat 