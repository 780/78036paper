+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/13577516543/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 30 15:29:04 GMT+0100 2013
Upload Date  : Wed Apr 2 11:00:05 GMT+0200 2014
Geotag Info  : Latitude:47.050651, Longitude:8.554229
Views        : 8,935
Comments     : 4


+---------+
|  TITLE  |
+---------+
Walking with paw on the front


+---------------+
|  DESCRIPTION  |
+---------------+
And one more picture of the female lynx walking in the snow...


+--------+
|  TAGS  |
+--------+
approaching pacing walking coming portrait female wolf canine canid brown dog winter "tierpark goldau" tierpark zoo arth switzerland nikon d4 