+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ahisgett
Photo URL    : https://www.flickr.com/photos/hisgett/8489714266/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 19 14:33:52 GMT+0100 2013
Upload Date  : Tue Feb 19 17:07:49 GMT+0100 2013
Geotag Info  : Latitude:52.449821, Longitude:-1.910258
Views        : 2,169
Comments     : 1


+---------+
|  TITLE  |
+---------+
Lynx 2


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Lynx 