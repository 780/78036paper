+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Becker1999
Photo URL    : https://www.flickr.com/photos/becker271/2955468015/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 19 08:42:46 GMT+0200 2008
Upload Date  : Mon Oct 20 00:02:46 GMT+0200 2008
Views        : 357
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bobcat


+---------------+
|  DESCRIPTION  |
+---------------+
Columbus Zoo Boo at the Zoo (10/18/08)


+--------+
|  TAGS  |
+--------+
"Paul's pic" 2008 cilumbus zoo "boo at the zoo" bobcat canon rebel xti 70-300 