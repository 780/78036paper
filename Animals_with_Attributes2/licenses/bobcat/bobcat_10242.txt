+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Hawk eyez
Photo URL    : https://www.flickr.com/photos/9209526@N04/722067294/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 4 06:23:56 GMT+0200 2007
Upload Date  : Thu Jul 5 04:46:38 GMT+0200 2007
Views        : 29
Comments     : 0


+---------+
|  TITLE  |
+---------+
bobcat


+---------------+
|  DESCRIPTION  |
+---------------+
On the morning of July 4, near Granite Reef Dam, Arizona.  We saw this bobcat two days ago in the same park.


+--------+
|  TAGS  |
+--------+
(no tags)