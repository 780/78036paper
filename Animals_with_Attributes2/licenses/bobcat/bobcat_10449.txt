+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ahisgett
Photo URL    : https://www.flickr.com/photos/hisgett/8148826401/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 2 14:55:32 GMT+0100 2012
Upload Date  : Fri Nov 2 22:43:09 GMT+0100 2012
Geotag Info  : Latitude:52.449821, Longitude:-1.910258
Views        : 348
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lynx 4


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Lynx Big Cat 