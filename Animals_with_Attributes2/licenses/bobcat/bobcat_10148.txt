+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cvalette
Photo URL    : https://www.flickr.com/photos/cvalette/14632808930/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 31 18:42:40 GMT+0200 2014
Upload Date  : Sun Aug 3 19:30:46 GMT+0200 2014
Views        : 207
Comments     : 0


+---------+
|  TITLE  |
+---------+
3b_05_0361Lynx


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Zoo "Zoo de Vincennes" "Parc animalier" Lynx 