+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/14870899212/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 9 18:03:14 GMT+0200 2014
Upload Date  : Sun Aug 10 00:49:10 GMT+0200 2014
Geotag Info  : Latitude:42.462697, Longitude:-71.093636
Views        : 2,486
Comments     : 1


+---------+
|  TITLE  |
+---------+
Canada Lynx Kitten Climbing


+---------------+
|  DESCRIPTION  |
+---------------+
Big paws and black pointy ears


+--------+
|  TAGS  |
+--------+
stone zoo cat canada lynx kitten climbing tree cute aww 