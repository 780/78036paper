+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : P. León
Photo URL    : https://www.flickr.com/photos/26524778@N02/8263170487/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 2 17:51:53 GMT+0100 2012
Upload Date  : Tue Dec 11 13:05:34 GMT+0100 2012
Views        : 614
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lynx


+---------------+
|  DESCRIPTION  |
+---------------+
Photographed in Cabarceno (Spain), the Eurasian lynx (Lynx lynx) is a medium-sized cat native to European and Siberian forests, South Asia and East Asia. It is also known as the European lynx, common lynx, the northern lynx, and the Siberian or Russian lynx. While its conservation status has been classified as &quot;Least Concern&quot;, populations of Eurasian lynx have been reduced or extirpated from western Europe, where it is now being reintroduced.

Fotografiado en Cabárceno, el lince boreal, europeo, eurasiático o común (Lynx lynx) es una especie de mamífero carnívoro de la familia Felidae. Es el representante más común y conocido del género Lynx. Es un felino de tamaño medio, predador nativo de los bosques europeos y siberianos.


+--------+
|  TAGS  |
+--------+
(no tags)