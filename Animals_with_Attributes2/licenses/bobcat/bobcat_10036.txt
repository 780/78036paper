+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8542817995/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 3 16:00:00 GMT+0100 2012
Upload Date  : Sun Mar 10 03:20:31 GMT+0100 2013
Geotag Info  : Latitude:47.105828, Longitude:6.822745
Views        : 4,034
Comments     : 4


+---------+
|  TITLE  |
+---------+
Lynx in the tree


+---------------+
|  DESCRIPTION  |
+---------------+
A nice picture of the female lynx in the tree, I like it!


+--------+
|  TAGS  |
+--------+
tree perched menacing beautiful lynx big wild cat female "bois du petit château" "animal park" zoo la-chaux-de-fonds neuchâtel switzerland nikon d4 