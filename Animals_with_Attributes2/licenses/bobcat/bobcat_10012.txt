+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/5728291644/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 24 17:35:59 GMT+0100 2011
Upload Date  : Tue May 17 00:33:29 GMT+0200 2011
Geotag Info  : Latitude:50.117523, Longitude:14.405221
Views        : 17,052
Comments     : 126


+---------+
|  TITLE  |
+---------+
Portrait of a caracal


+---------------+
|  DESCRIPTION  |
+---------------+
Believe it or not, it's the first caracal picture I post here on Flickr! :p

I like the portrait, sad that the mesh is visible in the background.

More caracal pictures later!


+--------+
|  TAGS  |
+--------+
caracal big wild cat lynx ears portrait face female cute zoo prague czech republik nikon d700 