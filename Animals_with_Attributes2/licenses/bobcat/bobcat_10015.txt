+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/5904190803/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 16 18:13:48 GMT+0200 2011
Upload Date  : Tue Jul 5 12:57:48 GMT+0200 2011
Geotag Info  : Latitude:47.050651, Longitude:8.554229
Views        : 3,928
Comments     : 5


+---------+
|  TITLE  |
+---------+
Lynx in the setting sun


+---------------+
|  DESCRIPTION  |
+---------------+
Between other photos, here is one of the male lynx of the Tierpark Arth Goldau in central Switzerland!


+--------+
|  TAGS  |
+--------+
european brown lynx feline portrait face sunny setting orange beautiful perched tierpark arth goldau switzerland nikon d700 