+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sarcroth
Photo URL    : https://www.flickr.com/photos/24737767@N00/822688120/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 15 09:53:26 GMT+0200 2007
Upload Date  : Mon Jul 16 00:17:09 GMT+0200 2007
Views        : 232
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bobcat II


+---------------+
|  DESCRIPTION  |
+---------------+
A Bobcat at the Rio Grande Zoo.


+--------+
|  TAGS  |
+--------+
bobcat zoo animal 