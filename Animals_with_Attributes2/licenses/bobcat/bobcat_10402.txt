+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mellow cat
Photo URL    : https://www.flickr.com/photos/bgwashburn/25089765213/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 11 18:22:43 GMT+0100 2016
Upload Date  : Sat Mar 12 05:34:18 GMT+0100 2016
Views        : 30
Comments     : 0


+---------+
|  TITLE  |
+---------+
Good cat


+---------------+
|  DESCRIPTION  |
+---------------+
We looked for Bobcat all day, and that was the last wild friend we found.


+--------+
|  TAGS  |
+--------+
bobcat 