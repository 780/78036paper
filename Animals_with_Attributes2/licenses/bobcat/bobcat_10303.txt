+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : U.S. Fish and Wildlife Service - Midwest Region
Photo URL    : https://www.flickr.com/photos/usfwsmidwest/6835377741/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 7 06:48:21 GMT+0100 2012
Upload Date  : Tue Feb 7 13:48:21 GMT+0100 2012
Views        : 617
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bobcat


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Bobcat Bobcats "National Wildlife Refuge" NWR Refuge "Creative Commons" USFWS "U.S. Fish and Wildlife Service" Midwest 