+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : fontplaydotcom
Photo URL    : https://www.flickr.com/photos/fontplaydotcom/7570674756/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 12 12:41:02 GMT+0200 2012
Upload Date  : Sun Jul 15 00:15:04 GMT+0200 2012
Views        : 566
Comments     : 1


+---------+
|  TITLE  |
+---------+
fpx051312-08


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://www.fontplay.com/freephotos/index.htm" rel="nofollow">www.fontplay.com/freephotos/index.htm</a> - You may use these photos for any artistic purpose, personal or commercial. My sets: <a href="http://www.flickr.com/photos/fontplaydotcom/sets/">www.flickr.com/photos/fontplaydotcom/sets/</a> - Get more photos, and conditions of use at <a href="http://www.fontplay.com/freephotos/index.htm" rel="nofollow">www.fontplay.com/freephotos/index.htm</a>


+--------+
|  TAGS  |
+--------+
bobcat 