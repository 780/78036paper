+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jinxmcc
Photo URL    : https://www.flickr.com/photos/64443083@N00/8579582362/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 20 19:10:38 GMT+0100 2013
Upload Date  : Fri Mar 22 05:58:58 GMT+0100 2013
Views        : 311
Comments     : 10


+---------+
|  TITLE  |
+---------+
Bobcat:  "Is that a gopher?"


+---------------+
|  DESCRIPTION  |
+---------------+
Biggest excitement of our day came at dinnertime, when Paul glanced out at the meadow and exclaimed &quot;Bobcat!&quot;  I hurried off for my camera, and the bobcat came up fairly close to the house (there is some digital zoom in this photo, though) and posed for us for 5 minutes or so,  I got quite a few pics through the window, not Great photos but pleasing to me, and I'll post most of them!

(#2 chronologically)


+--------+
|  TAGS  |
+--------+
bobcat feline "wild cat" Edgewood MendocinoCoast NorthernCalifornia 