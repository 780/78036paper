+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : B A Bowen Photography
Photo URL    : https://www.flickr.com/photos/riverbk/4003374186/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 19 01:26:23 GMT+0100 2007
Upload Date  : Mon Oct 12 03:21:23 GMT+0200 2009
Views        : 236
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lynx


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Dauset Trails Nature Center families and nature wildlife" Lynx 