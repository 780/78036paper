+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/15250932421/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 6 16:10:02 GMT+0200 2014
Upload Date  : Tue Sep 16 05:10:18 GMT+0200 2014
Geotag Info  : Latitude:42.462697, Longitude:-71.093636
Views        : 1,553
Comments     : 0


+---------+
|  TITLE  |
+---------+
Canada Lynx Mom and Kitten


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
stone zoo massachusetts cat canada lynx kitten 