+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tylerkaraszewski
Photo URL    : https://www.flickr.com/photos/tylerkaraszewski/6518181929/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 15 15:31:39 GMT+0100 2011
Upload Date  : Fri Dec 16 00:20:59 GMT+0100 2011
Views        : 651
Comments     : 1


+---------+
|  TITLE  |
+---------+
Bobcat!


+---------------+
|  DESCRIPTION  |
+---------------+
Look who I saw walking through the backyard. This may be the photo that convinces me to buy a 7D.


+--------+
|  TAGS  |
+--------+
bobcat 