+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lotzman Katzman
Photo URL    : https://www.flickr.com/photos/lotzman/25780828560/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 12 14:53:42 GMT+0100 2016
Upload Date  : Sat Mar 26 22:48:35 GMT+0100 2016
Views        : 81
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lynx


+---------------+
|  DESCRIPTION  |
+---------------+
Cosley Zoo Mar'16


+--------+
|  TAGS  |
+--------+
"Cosley Zoo" Zoo 2016 