+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/15261442004/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 28 18:20:42 GMT+0200 2014
Upload Date  : Wed Nov 26 15:00:20 GMT+0100 2014
Geotag Info  : Latitude:46.565115, Longitude:6.773371
Views        : 5,606
Comments     : 1


+---------+
|  TITLE  |
+---------+
Lynx walking on the log


+---------------+
|  DESCRIPTION  |
+---------------+
I also like this lynx picture, he was licking his nose!


+--------+
|  TAGS  |
+--------+
walking log wood portrait licking nose wild cat lynx male servion lausanne zoo switzerland nikon d4 