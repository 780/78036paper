+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DenaliNPS
Photo URL    : https://www.flickr.com/photos/denalinps/5302398562/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 17 23:52:21 GMT+0200 2009
Upload Date  : Wed Dec 29 05:05:09 GMT+0100 2010
Views        : 957
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lynx


+---------------+
|  DESCRIPTION  |
+---------------+
(NPS Photo/Neil Blake)

Check out the official Denali Facebook, Twitter and YouTube pages:

Like us on Facebook: <a href="http://www.facebook.com/DenaliNPS" rel="nofollow">www.facebook.com/DenaliNPS</a>

Follow us on Twitter: <a href="http://www.twitter.com/DenaliNPS" rel="nofollow">www.twitter.com/DenaliNPS</a>

Denali YouTube Channel: <a href="http://www.youtube.com/denalinps" rel="nofollow">www.youtube.com/denalinps</a>


+--------+
|  TAGS  |
+--------+
(no tags)