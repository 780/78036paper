+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : LandBetweenTheLakesKYTN
Photo URL    : https://www.flickr.com/photos/lblkytn/8742252900/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 18 23:12:33 GMT+0100 2008
Upload Date  : Wed May 15 20:45:55 GMT+0200 2013
Views        : 25
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bobcat, Nature Station, Carrie Szwed


+---------------+
|  DESCRIPTION  |
+---------------+
Mammals at our near Woodlands Nature Station.


+--------+
|  TAGS  |
+--------+
LandBetweenTheLakesKYTN "Land Between The Lakes" "US Forest Service" nature "environmental education" "Kentucky Lake" "Lake Barkley" history camping birding biking hiking hunting Kentucky Tennessee "public lands" "scenic driving" "wildlife watching" 