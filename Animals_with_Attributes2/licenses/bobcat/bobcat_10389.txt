+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Airwolfhound
Photo URL    : https://www.flickr.com/photos/24874528@N04/10774402114/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 9 14:56:38 GMT+0100 2013
Upload Date  : Sun Nov 10 10:58:12 GMT+0100 2013
Views        : 4,516
Comments     : 3


+---------+
|  TITLE  |
+---------+
European Lynx - Whipsnade Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Lynx "Whipsnade Zoo" 