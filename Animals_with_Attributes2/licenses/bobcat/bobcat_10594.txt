+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jitze
Photo URL    : https://www.flickr.com/photos/jitze1942/3355251163/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 14 18:14:17 GMT+0100 2009
Upload Date  : Sun Mar 15 09:15:34 GMT+0100 2009
Views        : 377
Comments     : 0


+---------+
|  TITLE  |
+---------+
Siberian Lynx (Juvenile)


+---------------+
|  DESCRIPTION  |
+---------------+
A light snack would not go amiss around now


+--------+
|  TAGS  |
+--------+
"Siberian Lynx" 