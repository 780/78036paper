+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 19, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8575079928/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 10 13:54:49 GMT+0100 2012
Upload Date  : Wed Mar 20 15:00:24 GMT+0100 2013
Geotag Info  : Latitude:47.050651, Longitude:8.554229
Views        : 3,288
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lynx looking backwards


+---------------+
|  DESCRIPTION  |
+---------------+
The female lynx seen from below, with the gray sky as background...


+--------+
|  TAGS  |
+--------+
backlight "gray sky" sky "looking back" female beautiful lynx wild cat feline tierpark arth goldau zoo switzerland nikon d4 