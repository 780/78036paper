+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/5189739872/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Sep 16 11:15:49 GMT+0200 2010
Upload Date  : Fri Nov 19 15:01:26 GMT+0100 2010
Geotag Info  : Latitude:57.037414, Longitude:9.898660
Views        : 4,081
Comments     : 5


+---------+
|  TITLE  |
+---------+
Young chimpanzee on a ladder


+---------------+
|  DESCRIPTION  |
+---------------+
Second chimpanzee picture taken in the Aalborg zoo in Denmark, I like this picture! :)


+--------+
|  TAGS  |
+--------+
chimp chimpanzee primate monkey ape log wood ladder rope bokeh hand young zoo aalborg holidays nikon d300 