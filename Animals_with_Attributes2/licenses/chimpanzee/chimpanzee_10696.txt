+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/7133640491/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 24 14:16:19 GMT+0100 2012
Upload Date  : Wed May 2 01:08:58 GMT+0200 2012
Geotag Info  : Latitude:47.420073, Longitude:9.285378
Views        : 5,438
Comments     : 3


+---------+
|  TITLE  |
+---------+
Young chimp with a male


+---------------+
|  DESCRIPTION  |
+---------------+
A young chimpanzee with an adult. This adult was a male, not a female like one could think... Maybe it was the father!


+--------+
|  TAGS  |
+--------+
chimp chimpanzee primate monkey young male adult sitting grass cuddling cute adorable "walter zoo" zoo gossau switzerland nikon d700 