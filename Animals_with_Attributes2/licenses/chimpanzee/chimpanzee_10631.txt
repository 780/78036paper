+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lola's Big Adventure!
Photo URL    : https://www.flickr.com/photos/kokalola/8373525531/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 23 03:28:23 GMT+0100 2012
Upload Date  : Sat Jan 12 22:28:22 GMT+0100 2013
Views        : 129
Comments     : 0


+---------+
|  TITLE  |
+---------+
Taronga Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Australia "New South Wales" "Taronga Zoo" Sydney chimpanzee 