+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pelican
Photo URL    : https://www.flickr.com/photos/pelican/3592142998/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 2 13:07:08 GMT+0200 2008
Upload Date  : Wed Jun 3 12:37:47 GMT+0200 2009
Views        : 520
Comments     : 0


+---------+
|  TITLE  |
+---------+
Oji zoo, Kobe, Japan


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
動物園／水族館 k100d "sigma 70-300mm F4-5.6" oji kobe zoo "oji zoo" chimpanzee 