+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/4000834268/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 23 14:24:18 GMT+0200 2009
Upload Date  : Sun Oct 11 14:54:32 GMT+0200 2009
Geotag Info  : Latitude:47.420073, Longitude:9.285378
Views        : 6,301
Comments     : 20


+---------+
|  TITLE  |
+---------+
Exploring his territory


+---------------+
|  DESCRIPTION  |
+---------------+
The same young chimpanzee boy that was featured in the previous picture, walking in his big enclosure. I like the fact that the tree on the top offers a nice frame to the picture.

Picture taken in the Walter zoo Gossau in Switzerland.


+--------+
|  TAGS  |
+--------+
chimpanzee ape monkey primate young baby male walking cute grass zoo walter gossau switzerland nikon d300 chimp VosPlusBellesPhotos 