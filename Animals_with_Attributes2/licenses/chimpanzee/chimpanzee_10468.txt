+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Abi Skipp
Photo URL    : https://www.flickr.com/photos/9557815@N05/14132651929/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 4 15:43:20 GMT+0200 2014
Upload Date  : Sun Jun 1 13:51:30 GMT+0200 2014
Views        : 159
Comments     : 0


+---------+
|  TITLE  |
+---------+
Frankfurt Zoo, 4th May 2014


+---------------+
|  DESCRIPTION  |
+---------------+
Chimpanzee


+--------+
|  TAGS  |
+--------+
Frankfurt zoo Germany chimpanzee 