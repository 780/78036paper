+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nilsrinaldi
Photo URL    : https://www.flickr.com/photos/nilsrinaldi/5157809799/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Oct 6 11:32:56 GMT+0200 2010
Upload Date  : Mon Nov 8 16:10:53 GMT+0100 2010
Views        : 4,597
Comments     : 2


+---------+
|  TITLE  |
+---------+
Chimpanzee thinking


+---------------+
|  DESCRIPTION  |
+---------------+
Chimpanzee thinking, Mahale Mountains National Park, Lake Tanganyika, Tanzania


+--------+
|  TAGS  |
+--------+
chimpanzee mahale tanzania 