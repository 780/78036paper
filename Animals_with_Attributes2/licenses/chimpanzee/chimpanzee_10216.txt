+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/9399913391/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 14 11:04:33 GMT+0200 2013
Upload Date  : Tue Jul 30 17:50:29 GMT+0200 2013
Geotag Info  : Latitude:53.230161, Longitude:-2.889211
Views        : 9,684
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chimpanzee sitting in the sun


+---------------+
|  DESCRIPTION  |
+---------------+
This time a chimpanzee outside in the sun! They have a quite big enclosure!


+--------+
|  TAGS  |
+--------+
sitting resting sun portrait old chimpanzee chimp ape primate monkey chester zoo uk "united kingdom" england nikon d4 