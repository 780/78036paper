+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8249809393/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 18 13:55:02 GMT+0200 2012
Upload Date  : Thu Dec 6 20:59:45 GMT+0100 2012
Geotag Info  : Latitude:47.547277, Longitude:7.578163
Views        : 7,389
Comments     : 4


+---------+
|  TITLE  |
+---------+
Cute young chimpanzee


+---------------+
|  DESCRIPTION  |
+---------------+
Another shot of the cute young chimpanzee you've already seen, this time without disturbing reflection! ;)


+--------+
|  TAGS  |
+--------+
chimp champanzee primate monkey young small baby sitting posing cute basel zoo zolli switzerland nikon d4 