+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/15268399712/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 29 14:06:37 GMT+0200 2014
Upload Date  : Wed Sep 17 18:00:05 GMT+0200 2014
Geotag Info  : Latitude:47.420073, Longitude:9.285378
Views        : 7,692
Comments     : 3


+---------+
|  TITLE  |
+---------+
Old chimpanzee sitting and posing


+---------------+
|  DESCRIPTION  |
+---------------+
This one was looking quite serious while watching me...


+--------+
|  TAGS  |
+--------+
old serious watching sitting.scratching portrait face chimpanzee chimp primate ape mammal tiger amur siberian big wild cat "walter zoo" gossau "st gallen" switzerland nikon d4 