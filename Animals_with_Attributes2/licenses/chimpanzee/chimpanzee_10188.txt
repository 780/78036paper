+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tiswango
Photo URL    : https://www.flickr.com/photos/tiswango/459276648/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 14 13:50:48 GMT+0200 2007
Upload Date  : Sun Apr 15 03:40:06 GMT+0200 2007
Geotag Info  : Latitude:25.604427, Longitude:-80.400701
Views        : 3,492
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chimpanzee Clapping


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Metro Zoo" Chimpanzee Zoo Animals "Family Fun" Miami Florida "Miami Metro Zoo" 