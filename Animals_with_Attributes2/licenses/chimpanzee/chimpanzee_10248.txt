+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rennett Stowe
Photo URL    : https://www.flickr.com/photos/tomsaint/3847028787/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 22 09:59:05 GMT+0200 2009
Upload Date  : Sun Aug 23 07:50:35 GMT+0200 2009
Views        : 3,460
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chimpanzee


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Chimpanzee monkey chimp "i wonder" "what's that?" "chimpanzee in rocks" "chimp rocks" chimps "hairy chimp" stubborn pensive "hand on face" determined "stubborn individual" "photo of  a chimp" "photo of a chimpanzee" "photograph of a chimp" "photograph of a chimpanzee" "picture of a chimp" "picture of a chimpanzee" "photograph chimpanzee" "pic chimp" "pic chimpanzee" "pic of a chimp" "pic of a chimpanzee" 