+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : TheBusyBrain
Photo URL    : https://www.flickr.com/photos/thebusybrain/3284021566/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 15 15:16:27 GMT+0100 2009
Upload Date  : Mon Feb 16 06:15:18 GMT+0100 2009
Views        : 1,676
Comments     : 1


+---------+
|  TITLE  |
+---------+
Chimp2


+---------------+
|  DESCRIPTION  |
+---------------+
Want to use this photo in a publication? Go ahead!  But please <a href="http://thebusybrain.com/photodo" rel="nofollow">Read This</a>!


+--------+
|  TAGS  |
+--------+
Lowry Park Zoo "Lowry Park Zoo" "Tampa, FL" Tampa Florida Animal Chimp Chimpanzee Bark Wood 