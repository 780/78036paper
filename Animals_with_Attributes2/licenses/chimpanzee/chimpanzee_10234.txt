+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Just chaos
Photo URL    : https://www.flickr.com/photos/7326810@N08/2315193921/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 6 11:38:53 GMT+0100 2008
Upload Date  : Fri Mar 7 04:18:00 GMT+0100 2008
Views        : 191
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chimpanzee


+---------------+
|  DESCRIPTION  |
+---------------+
Pan troglodytes


+--------+
|  TAGS  |
+--------+
SanFranciscozoo zoo Animals Animal Mammal primate Pan troglodytes Animalia Chordata Mammalia Primates Hominidae Homininae Hominini Panina 