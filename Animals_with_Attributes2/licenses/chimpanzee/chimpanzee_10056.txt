+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/6270783422/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 31 15:21:24 GMT+0200 2011
Upload Date  : Sun Oct 23 01:01:36 GMT+0200 2011
Geotag Info  : Latitude:47.420073, Longitude:9.285378
Views        : 8,475
Comments     : 5


+---------+
|  TITLE  |
+---------+
Baby on the back of mommy


+---------------+
|  DESCRIPTION  |
+---------------+
A young chimpanzee on the back of his mother (?), cute scene taken at the Walter Zoo in Switzerland.


+--------+
|  TAGS  |
+--------+
baby young back mother tired cute sunny primate chimpanzee ape monkey walter zoo gossau switzerland nikon d700 