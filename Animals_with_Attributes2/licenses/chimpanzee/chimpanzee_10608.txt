+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brainstorm1984
Photo URL    : https://www.flickr.com/photos/brainstorm1984/14831779409/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 14 10:31:41 GMT+0200 2014
Upload Date  : Sun Aug 24 14:09:47 GMT+0200 2014
Geotag Info  : Latitude:52.749252, Longitude:9.618616
Views        : 1,391
Comments     : 0


+---------+
|  TITLE  |
+---------+
Schimpanse / Chimpanzee


+---------------+
|  DESCRIPTION  |
+---------------+
Ein Schimpanse im Seregeti-Park Hodenhagen.

A Chimpanzee in the &quot;Serengeti-Park Hodenhagen&quot;.


+--------+
|  TAGS  |
+--------+
Chimpanzee Schimpanse Hodenhagen Pan Serengeti-Park "Pan troglodytes" "Common Chimpanzee" "Gemeiner Schimpanse" "Gewöhnlicher Schimpanse" 