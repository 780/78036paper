+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dakiny
Photo URL    : https://www.flickr.com/photos/dakiny/7710877916/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 2 15:37:25 GMT+0200 2012
Upload Date  : Sat Aug 4 18:15:35 GMT+0200 2012
Geotag Info  : Latitude:35.649084, Longitude:139.402213
Views        : 98
Comments     : 0


+---------+
|  TITLE  |
+---------+
チンパンジー (Chimpanzee)


+---------------+
|  DESCRIPTION  |
+---------------+
2012年8月2日木曜日、多摩動物公園にて、Nikon D5100 + AF-S DX VR Zoom Nikkor ED 55-200mm F4-5.6Gで撮影。


+--------+
|  TAGS  |
+--------+
Zoo Tama-Zoological-Park Tama-Zoo 多摩動物園 動物園 