+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rod Waddington
Photo URL    : https://www.flickr.com/photos/rod_waddington/15059473018/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 12 15:20:21 GMT+0200 2014
Upload Date  : Mon Sep 15 12:34:05 GMT+0200 2014
Views        : 2,849
Comments     : 0


+---------+
|  TITLE  |
+---------+
Thinking,Chimpanzee, Uganda


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
africa uganda kibale forrest forest chimpanzee chimp primate wild animal 