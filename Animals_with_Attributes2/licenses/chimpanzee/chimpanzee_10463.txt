+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Derek Keats
Photo URL    : https://www.flickr.com/photos/dkeats/3308522716/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 22 11:07:16 GMT+0100 2009
Upload Date  : Wed Feb 25 05:42:15 GMT+0100 2009
Views        : 1,567
Comments     : 4


+---------+
|  TITLE  |
+---------+
Female chimpanzee with baby


+---------------+
|  DESCRIPTION  |
+---------------+
Female chimpanzee with baby


+--------+
|  TAGS  |
+--------+
mamal ape chimpanze juvenile 