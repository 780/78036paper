+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/6978600218/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 24 11:55:58 GMT+0100 2012
Upload Date  : Sun Apr 29 17:08:46 GMT+0200 2012
Geotag Info  : Latitude:47.420073, Longitude:9.285378
Views        : 7,108
Comments     : 2


+---------+
|  TITLE  |
+---------+
Young chimp playing with the water


+---------------+
|  DESCRIPTION  |
+---------------+
One of the young chimps discovered how to play with the water supply of the little stream they have: like kids do, he was putting a water in the pipe so that the water spills all around!


+--------+
|  TAGS  |
+--------+
chimp chimpanzee young ape primate monkey play water pipe spilling drops playing fun amazing grass "walter zoo" zoo gossau switzerland nikon d700 