+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : russellstreet
Photo URL    : https://www.flickr.com/photos/russellstreet/4386412893/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 28 13:16:13 GMT+0100 2008
Upload Date  : Thu Feb 25 10:52:34 GMT+0100 2010
Geotag Info  : Latitude:-33.842723, Longitude:151.239825
Views        : 352
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chimpanzee


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Animal Australia Chimpanzee "Pan troglodytes" Sydney "Taronga Zoo" "New South Wales" AU 