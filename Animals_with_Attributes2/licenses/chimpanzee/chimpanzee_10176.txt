+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Matt Brittaine
Photo URL    : https://www.flickr.com/photos/mattbrittaine/16548618500/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 7 13:24:28 GMT+0100 2015
Upload Date  : Fri Mar 6 21:47:10 GMT+0100 2015
Views        : 884
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chimpanzee at Barcelona Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
SONY DSC


+--------+
|  TAGS  |
+--------+
"matt brittaine" chimp chimpanzee barcelona zoo spain 