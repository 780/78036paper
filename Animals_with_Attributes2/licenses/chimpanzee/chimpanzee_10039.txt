+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Oddernod
Photo URL    : https://www.flickr.com/photos/oddernod/10636092473/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 20 14:43:59 GMT+0200 2013
Upload Date  : Sun Nov 3 01:28:54 GMT+0100 2013
Views        : 791
Comments     : 0


+---------+
|  TITLE  |
+---------+
A-Punk


+---------------+
|  DESCRIPTION  |
+---------------+
A calm moment before the baby chimp storm starts again.


+--------+
|  TAGS  |
+--------+
Chimpanzee Location "Los Angeles" "Los Angeles Zoo" animal outdoor zoo 