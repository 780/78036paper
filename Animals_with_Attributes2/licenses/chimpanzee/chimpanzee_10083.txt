+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/8197995800/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 3 12:33:44 GMT+0100 2012
Upload Date  : Sun Nov 18 22:43:16 GMT+0100 2012
Views        : 269
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Chimpanzees


+--------+
|  TAGS  |
+--------+
Chimpanzees "Chester Zoo" 