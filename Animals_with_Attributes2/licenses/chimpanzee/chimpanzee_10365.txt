+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : City of Albuquerque
Photo URL    : https://www.flickr.com/photos/cityofalbuquerque/697273810/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 2 10:31:29 GMT+0200 2007
Upload Date  : Mon Jul 2 19:31:29 GMT+0200 2007
Views        : 3,559
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chimpanzees


+---------------+
|  DESCRIPTION  |
+---------------+
Chimps at the Rio Grande Zoo in Albuquerque.


+--------+
|  TAGS  |
+--------+
Zoo "Rio Grande Zoo" BioPark Chimps Chimpanzees Cartload dpcman 