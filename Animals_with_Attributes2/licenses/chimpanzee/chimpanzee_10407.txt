+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Oddernod
Photo URL    : https://www.flickr.com/photos/oddernod/16817416935/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 2 15:23:55 GMT+0100 2013
Upload Date  : Sun Mar 15 03:05:05 GMT+0100 2015
Views        : 469
Comments     : 0


+---------+
|  TITLE  |
+---------+
Continuous Thunder


+---------------+
|  DESCRIPTION  |
+---------------+
Lip swagger. Lip smacker.


+--------+
|  TAGS  |
+--------+
Chimpanzee Location "Los Angeles" "Los Angeles Zoo" animal outdoor 