+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : conor.mcdermottroe
Photo URL    : https://www.flickr.com/photos/91242583@N03/12280483035/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 2 01:45:58 GMT+0100 2014
Upload Date  : Mon Feb 3 04:13:42 GMT+0100 2014
Geotag Info  : Latitude:-33.843811, Longitude:151.241751
Views        : 66
Comments     : 0


+---------+
|  TITLE  |
+---------+
A chimpanzee watching me take photos


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
taronga zoo sydney 