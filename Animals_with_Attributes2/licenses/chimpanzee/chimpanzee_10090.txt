+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/9487097198/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 22 11:33:59 GMT+0200 2013
Upload Date  : Sun Aug 11 15:51:42 GMT+0200 2013
Views        : 271
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Chimpanzees


+--------+
|  TAGS  |
+--------+
Chimpanzees "Chester Zoo" 