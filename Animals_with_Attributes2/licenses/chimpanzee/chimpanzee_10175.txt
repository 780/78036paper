+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : dougbelshaw
Photo URL    : https://www.flickr.com/photos/dougbelshaw/4469480718/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 17 12:58:11 GMT+0100 2010
Upload Date  : Sun Mar 28 10:27:45 GMT+0200 2010
Views        : 855
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chimpanzee at Al Ain Zoo, UAE


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
UAE chimpanzee zoo chimp "Al Ain" 