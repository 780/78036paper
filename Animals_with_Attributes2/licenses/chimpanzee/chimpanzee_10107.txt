+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/22098824911/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 17 14:26:05 GMT+0200 2015
Upload Date  : Sun Oct 11 13:06:23 GMT+0200 2015
Views        : 71
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Chimpanzee


+--------+
|  TAGS  |
+--------+
"Chester Zoo" Chimpanzees 