+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/12243909664/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 17 11:37:00 GMT+0200 2013
Upload Date  : Sat Feb 1 01:58:29 GMT+0100 2014
Views        : 166
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Chimpanzees


+--------+
|  TAGS  |
+--------+
"Chester Zoo" Chimpanzees 