+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/7625527564/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 16 15:51:41 GMT+0200 2012
Upload Date  : Mon Jul 23 01:00:28 GMT+0200 2012
Geotag Info  : Latitude:51.340719, Longitude:6.602504
Views        : 11,401
Comments     : 4


+---------+
|  TITLE  |
+---------+
Male chimpanzee with open mouth


+---------------+
|  DESCRIPTION  |
+---------------+
I think he was yawning! ;)
Taken at the Krefeld zoo.


+--------+
|  TAGS  |
+--------+
chimpanzee chimp monkey ape primate male "open mouth" yawning penis testicles sitting zoo krefeld germany nikon d700 