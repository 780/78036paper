+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : generalising
Photo URL    : https://www.flickr.com/photos/shimgray/2797259892/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 24 10:50:34 GMT+0200 2008
Upload Date  : Mon Aug 25 19:47:21 GMT+0200 2008
Views        : 287
Comments     : 0


+---------+
|  TITLE  |
+---------+
p1210420


+---------------+
|  DESCRIPTION  |
+---------------+
Chimpanzee (Pan troglodytes) at Whipsnade Zoo


+--------+
|  TAGS  |
+--------+
chimp chimpanzee "pan trogdolytes" 