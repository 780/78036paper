+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/7123407493/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 24 14:06:07 GMT+0100 2012
Upload Date  : Sun Apr 29 06:38:42 GMT+0200 2012
Geotag Info  : Latitude:47.420073, Longitude:9.285378
Views        : 10,519
Comments     : 5


+---------+
|  TITLE  |
+---------+
Young but not so cute chimpanzee


+---------------+
|  DESCRIPTION  |
+---------------+
This is Malik, the young chimpanzee who has not a lot of hairs. Sadly his mom pulls out his hairs... Poor one...


+--------+
|  TAGS  |
+--------+
primate ape monkey chimpanzee chimp young sitting posing naked male stone grass ugly "walter zoo" zoo gossau switzerland nikon d700 