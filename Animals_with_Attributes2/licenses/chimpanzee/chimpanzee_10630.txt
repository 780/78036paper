+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Newtown grafitti
Photo URL    : https://www.flickr.com/photos/newtown_grafitti/9222354058/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 6 11:20:12 GMT+0200 2013
Upload Date  : Sat Jul 6 10:37:32 GMT+0200 2013
Views        : 71
Comments     : 0


+---------+
|  TITLE  |
+---------+
Taronga Park Zoo, VII


+---------------+
|  DESCRIPTION  |
+---------------+
Chimpanzees


+--------+
|  TAGS  |
+--------+
Mosman Sydney 