+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : infomatique
Photo URL    : https://www.flickr.com/photos/infomatique/282845269/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 29 14:02:13 GMT+0100 2006
Upload Date  : Mon Oct 30 00:14:38 GMT+0100 2006
Views        : 804
Comments     : 0


+---------+
|  TITLE  |
+---------+
DUBLIN ZOO


+---------------+
|  DESCRIPTION  |
+---------------+
Darwin's theory of evolution (published in 1859) spurred scientific interest in chimpanzees, as in much of life science, leading eventually to numerous studies of the animals in the wild and captivity. The observers of chimpanzees at the time were mainly interested in behaviour as it related to that of humans. This was less strictly and disinterestedly scientific than it might sound, with much attention being focused on whether or not the animals had traits that could be considered 'good'; the intelligence of chimpanzees was often significantly exaggerated. At one point there was even a scheme drawn up to domesticate chimpanzees in order to have them perform various menial tasks (i.e. factory work)[citation needed]. By the end of the 1800s chimpanzees remained very much a mystery to humans, with very little factual scientific information available.


+--------+
|  TAGS  |
+--------+
Dublin Ireland Zoo "Dublin Zoo" Chimp Chimps Chimpanzee "Canon EOS 5D" Canon EOS "EOS 5D" "William Murphy" Infomatique mapireland www.infomatique.org "Tourist Guide" "Dublin City" "Dublin Guide" Photographs "Visit Dublin" "Guide To Dublin" "Magical Murphy The Virtual Tourist Guide" "Magical Murphy" 