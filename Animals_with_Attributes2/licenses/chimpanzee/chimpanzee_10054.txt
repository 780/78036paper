+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kitty Terwolbeck
Photo URL    : https://www.flickr.com/photos/kittysfotos/11114728094/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Nov 26 15:52:11 GMT+0100 2013
Upload Date  : Fri Nov 29 12:32:44 GMT+0100 2013
Geotag Info  : Latitude:52.366595, Longitude:4.914493
Views        : 1,015
Comments     : 0


+---------+
|  TITLE  |
+---------+
Baby chimpanzee Ajani with mother Amber


+---------------+
|  DESCRIPTION  |
+---------------+
Artis Royal Zoo - Amsterdam - The Netherlands


+--------+
|  TAGS  |
+--------+
Artis zoo dierentuin animal dier monkey aap Ajani Amber chimpanzee chimpansee "Pan troglodytes" zoogdier 