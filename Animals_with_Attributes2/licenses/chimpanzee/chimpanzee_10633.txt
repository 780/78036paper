+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pelican
Photo URL    : https://www.flickr.com/photos/pelican/8620813893/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 31 07:50:08 GMT+0200 2013
Upload Date  : Fri Apr 5 13:10:50 GMT+0200 2013
Views        : 543
Comments     : 0


+---------+
|  TITLE  |
+---------+
Taronga zoo, Sydney, Australia


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Sigma 70-300mm F4-5.6 Macro" K-5 "taronga zoo" sydney chimpanzee 