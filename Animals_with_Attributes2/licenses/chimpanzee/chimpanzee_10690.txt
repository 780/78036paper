+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/6969209556/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 24 14:20:33 GMT+0100 2012
Upload Date  : Thu Apr 26 12:38:28 GMT+0200 2012
Geotag Info  : Latitude:47.420073, Longitude:9.285378
Views        : 9,947
Comments     : 4


+---------+
|  TITLE  |
+---------+
Young chimp near her mother


+---------------+
|  DESCRIPTION  |
+---------------+
A cute scene: one of the many young chimpanzee sitting near hear mother. The chimp enclosure of the zoo is really great for photographers!


+--------+
|  TAGS  |
+--------+
chimp chimpanzee primage ape monkey black young female mother sitting calm grass "walter zoo" zoo gossau switzerland nikon d700 