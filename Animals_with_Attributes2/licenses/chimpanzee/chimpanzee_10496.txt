+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BigMikeSndTech
Photo URL    : https://www.flickr.com/photos/bigmikesndtech/3324202406/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 27 17:17:21 GMT+0100 2009
Upload Date  : Mon Mar 2 23:39:40 GMT+0100 2009
Geotag Info  : Latitude:8.419333, Longitude:-13.206334
Views        : 229
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_0153


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Freetown Sierra Leone chimpanzee "chimpanzee sanctuary" 