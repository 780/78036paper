+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jordi Payà Canals
Photo URL    : https://www.flickr.com/photos/arg0s/14221726637/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 18 16:01:41 GMT+0200 2014
Upload Date  : Thu Jun 12 23:12:00 GMT+0200 2014
Geotag Info  : Latitude:41.387075, Longitude:2.191417
Views        : 1,699
Comments     : 0


+---------+
|  TITLE  |
+---------+
common chimpanzee


+---------------+
|  DESCRIPTION  |
+---------------+
Pan troglodytes


+--------+
|  TAGS  |
+--------+
Jordi Payà Canals Canon EOS 450D EF 70-300mm IS USM Catalonia Barcelona zoo zoological park primate chimpanzee human portrait look ears eyes mouth nose animal fauna biology black fur ape Pan troglodytes 