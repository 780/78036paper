+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/9612813784/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 24 13:40:52 GMT+0200 2013
Upload Date  : Wed Aug 28 04:08:53 GMT+0200 2013
Geotag Info  : Latitude:42.064268, Longitude:-71.586366
Views        : 1,071
Comments     : 0


+---------+
|  TITLE  |
+---------+
Showing off Coconut Halves


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
soutchwicks zoo ape chimp chimpanzee coconut halves tree 