+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : -JvL-
Photo URL    : https://www.flickr.com/photos/-jvl-/15172897297/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 21 11:49:52 GMT+0200 2014
Upload Date  : Fri Sep 26 15:00:06 GMT+0200 2014
Geotag Info  : Latitude:51.520813, Longitude:5.111733
Views        : 585
Comments     : 0


+---------+
|  TITLE  |
+---------+
A group of chimpanzees


+---------------+
|  DESCRIPTION  |
+---------------+
A group of chimpanzees sitting in Safari park Beekse Bergen


+--------+
|  TAGS  |
+--------+
"Beekse Bergen" Chimpansee Chimpanzee "Safari Park" Hilvarenbeek Noord-Brabant Nederland 