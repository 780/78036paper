+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lutramania
Photo URL    : https://www.flickr.com/photos/lutramania/5326615988/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 5 15:49:02 GMT+0100 2011
Upload Date  : Wed Jan 5 08:53:29 GMT+0100 2011
Views        : 95
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC03480e


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
otter "orana wildlife park" "asian small-clawed river otter" lutrinae zoo christchurch 