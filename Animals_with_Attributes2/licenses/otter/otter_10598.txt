+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andy Morffew
Photo URL    : https://www.flickr.com/photos/andymorffew/8416678035/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 26 15:46:27 GMT+0100 2013
Upload Date  : Sat Jan 26 19:25:38 GMT+0100 2013
Views        : 2,804
Comments     : 74


+---------+
|  TITLE  |
+---------+
Pond wars!


+---------------+
|  DESCRIPTION  |
+---------------+
A small alligator went for this otter as he was fishing. The otter turned around and went back at the alligator.  They snarled and hissed at each other for several minutes but finally the otter realised the alligator had no vulnerable area he could attack and he left for another pond. 


Thank you for visiting, I always appreciate your views, favs, invites and comments.

My images are available free for any use but must be attributed and not made part of another image without permission. My website link is <a href="http://www.andymorffew.com" rel="nofollow">www.andymorffew.com</a>


+--------+
|  TAGS  |
+--------+
otter alligator fight pond "Freedom Park" Naples Florida "Andy Morffew" Morffew "Nature through the Lens" blinkagain 