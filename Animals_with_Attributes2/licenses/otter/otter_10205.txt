+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DoNotLick
Photo URL    : https://www.flickr.com/photos/donotlick/5930355145/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 11 16:33:30 GMT+0200 2011
Upload Date  : Tue Jul 12 19:16:38 GMT+0200 2011
Geotag Info  : Latitude:36.617893, Longitude:-121.901916
Views        : 474
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter at the Monterey Bay Aquarium


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"firefox offsite" "firefox workweek" monterey "monterey bay aquarium" california "sea life" organisms 