+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lutramania
Photo URL    : https://www.flickr.com/photos/lutramania/3940014007/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 21 21:30:02 GMT+0200 2009
Upload Date  : Mon Sep 21 11:30:02 GMT+0200 2009
Geotag Info  : Latitude:-41.319422, Longitude:174.784455
Views        : 109
Comments     : 0


+---------+
|  TITLE  |
+---------+
webDSC00999


+---------------+
|  DESCRIPTION  |
+---------------+
Hi, I'm an otter, I'm ridiculously cute and I can bite your finger clean off with my carnivore teeth.


+--------+
|  TAGS  |
+--------+
otters lutrinae "aonyx cinerea" 