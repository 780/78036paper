+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SnapAdik
Photo URL    : https://www.flickr.com/photos/hubertyu/14658354842/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 3 09:05:00 GMT+0200 2014
Upload Date  : Tue Jul 15 07:15:57 GMT+0200 2014
Geotag Info  : Latitude:47.668790, Longitude:-122.350155
Views        : 221
Comments     : 0


+---------+
|  TITLE  |
+---------+
Asian Small-Clawed Otters


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Woodland Park Zoo" Seattle Washington zoo otters 