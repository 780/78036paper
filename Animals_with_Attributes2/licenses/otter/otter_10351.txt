+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : The Lamb Family
Photo URL    : https://www.flickr.com/photos/lambfamilyphotos/6052784379/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 16 15:38:02 GMT+0200 2011
Upload Date  : Wed Aug 17 17:58:30 GMT+0200 2011
Views        : 5
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)