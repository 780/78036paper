+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : offwhitehouse
Photo URL    : https://www.flickr.com/photos/10844205@N00/935319977/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 28 13:41:10 GMT+0200 2007
Upload Date  : Sun Jul 29 07:52:01 GMT+0200 2007
Views        : 131
Comments     : 0


+---------+
|  TITLE  |
+---------+
Swimming Otters


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
hunstanton seaside otters 