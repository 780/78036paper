+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : VirtKitty
Photo URL    : https://www.flickr.com/photos/lalouque/3878739844/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 24 16:11:06 GMT+0200 2009
Upload Date  : Tue Sep 1 19:00:23 GMT+0200 2009
Views        : 41
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otters


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
AK Seward otter 