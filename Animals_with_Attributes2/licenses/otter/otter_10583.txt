+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michelle Lai Lai
Photo URL    : https://www.flickr.com/photos/milalai/20416928235/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 2 19:04:06 GMT+0200 2015
Upload Date  : Sun Aug 9 06:13:49 GMT+0200 2015
Views        : 48
Comments     : 0


+---------+
|  TITLE  |
+---------+
OTTERS!


+---------------+
|  DESCRIPTION  |
+---------------+
OMG! Otters are so cute! Why did I not know about this before?

Osaka Aquarium, Japan


+--------+
|  TAGS  |
+--------+
"Osaka Aquarium" Japan Otters 