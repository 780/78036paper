+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : robotpolisher
Photo URL    : https://www.flickr.com/photos/lintbrush/182887029/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 2 18:33:52 GMT+0200 2006
Upload Date  : Thu Jul 6 02:28:37 GMT+0200 2006
Views        : 60
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
I've come to the conclusion that were I to come back as any particular animal, it'd have to be a sea otter... Looks like a nice life.


+--------+
|  TAGS  |
+--------+
newyorkaquarium aquarium coneyisland brooklyn summer friends 