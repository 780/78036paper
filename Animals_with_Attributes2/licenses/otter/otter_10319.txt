+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Der Stefan
Photo URL    : https://www.flickr.com/photos/derstefan/4578827443/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 1 13:17:51 GMT+0200 2010
Upload Date  : Tue May 4 22:15:19 GMT+0200 2010
Views        : 717
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo osnabrück animals otter 