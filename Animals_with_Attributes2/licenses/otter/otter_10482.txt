+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Harlequeen
Photo URL    : https://www.flickr.com/photos/harlequeen/955749284/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 22 12:09:35 GMT+0200 2007
Upload Date  : Mon Jul 30 23:37:48 GMT+0200 2007
Geotag Info  : Latitude:51.847231, Longitude:-0.541419
Views        : 981
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otters on a log


+---------------+
|  DESCRIPTION  |
+---------------+
Lots of otters on a log


+--------+
|  TAGS  |
+--------+
otter 