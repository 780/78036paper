+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Joanne Goldby
Photo URL    : https://www.flickr.com/photos/jovamp/8546918958/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 9 20:00:47 GMT+0100 2013
Upload Date  : Mon Mar 11 00:31:41 GMT+0100 2013
Views        : 212
Comments     : 0


+---------+
|  TITLE  |
+---------+
Birmingham Nature Centre (13 of 14).jpg


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Aonyx cinerea" "Birmingham Nature Centre" "Cannon Hill Park" "Cannon Hill Park Nature Centre" "Nature Centre" "Oriental small claw otter" Otter "short-clawed otter" 