+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bunnygoth
Photo URL    : https://www.flickr.com/photos/bunnygoth/7825439938/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 12 02:16:25 GMT+0200 2012
Upload Date  : Mon Aug 20 20:46:51 GMT+0200 2012
Views        : 116
Comments     : 0


+---------+
|  TITLE  |
+---------+
Beardsley Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
North American River Otter, Beardsley Zoo, Bridgeport, CT


+--------+
|  TAGS  |
+--------+
river otter beardsley zoo bridgeport ct connecticut 