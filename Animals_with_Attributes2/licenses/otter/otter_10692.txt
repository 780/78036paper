+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neil DeMaster
Photo URL    : https://www.flickr.com/photos/84169650@N07/15999119783/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 19 14:30:23 GMT+0200 2014
Upload Date  : Mon Feb 23 02:58:46 GMT+0100 2015
Views        : 295
Comments     : 0


+---------+
|  TITLE  |
+---------+
Southern Sea Otter (8)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"marine mammal" mammal "southern sea otter" "sea otter" otter 