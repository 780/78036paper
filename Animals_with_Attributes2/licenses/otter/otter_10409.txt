+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SmokingDonkey & ZumiWeb
Photo URL    : https://www.flickr.com/photos/sable/13537571605/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 29 13:07:26 GMT+0100 2014
Upload Date  : Mon Mar 31 15:49:56 GMT+0200 2014
Views        : 82
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
Wingham Wildlife Park march 29th 2014


+--------+
|  TAGS  |
+--------+
Wingham Wildlife Park 