+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : biologyfishman
Photo URL    : https://www.flickr.com/photos/43021596@N00/145236629/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 29 06:48:21 GMT+0200 2005
Upload Date  : Fri May 12 22:43:33 GMT+0200 2006
Views        : 68
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
Otter at Chincoteague Wildlife Refuge 2005


+--------+
|  TAGS  |
+--------+
Mammal 