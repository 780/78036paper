+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Roland Klose
Photo URL    : https://www.flickr.com/photos/rwklose/6183967176/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 26 11:20:11 GMT+0200 2011
Upload Date  : Mon Sep 26 04:51:05 GMT+0200 2011
Views        : 21
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otters


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)