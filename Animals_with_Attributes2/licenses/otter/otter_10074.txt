+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Opt1mus76
Photo URL    : https://www.flickr.com/photos/opt1mus76/8626870831/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 3 13:25:16 GMT+0200 2013
Upload Date  : Sun Apr 7 15:33:13 GMT+0200 2013
Views        : 631
Comments     : 0


+---------+
|  TITLE  |
+---------+
European Otter (Lutra Lutra)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Banham "Banham Zoo" Otter Lutra "European Otter" 