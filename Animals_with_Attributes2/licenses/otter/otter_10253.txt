+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Snake3yes
Photo URL    : https://www.flickr.com/photos/snake3yes/200541600/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 21 11:56:12 GMT+0200 2006
Upload Date  : Fri Jul 28 23:28:22 GMT+0200 2006
Views        : 108
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
otter wildlife animal 