+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : D Felstead at butterfly
Photo URL    : https://www.flickr.com/photos/d_m_felstead_66/16142393537/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 17 13:26:13 GMT+0100 2015
Upload Date  : Tue Jan 20 22:04:34 GMT+0100 2015
Views        : 534
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)