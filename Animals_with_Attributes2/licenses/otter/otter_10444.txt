+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jonathan Stonehouse
Photo URL    : https://www.flickr.com/photos/gizmo_bunny/13319232063/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 17 13:39:38 GMT+0100 2014
Upload Date  : Sat Mar 22 02:15:55 GMT+0100 2014
Views        : 491
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter, Dudley Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Otter Dudley Zoo fuji finepix s6500 