+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : zemistor
Photo URL    : https://www.flickr.com/photos/zemistor/2530494204/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 26 14:08:49 GMT+0200 2008
Upload Date  : Wed May 28 07:13:42 GMT+0200 2008
Geotag Info  : Latitude:36.618083, Longitude:-121.901461
Views        : 84
Comments     : 0


+---------+
|  TITLE  |
+---------+
Memorial Day Weekend010


+---------------+
|  DESCRIPTION  |
+---------------+
Otter at the Monterey Bay Aquarium


+--------+
|  TAGS  |
+--------+
California Monterey "Monterey Bay Aquarium" otter 