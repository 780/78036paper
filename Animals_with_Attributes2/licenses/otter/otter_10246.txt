+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mark Gstohl
Photo URL    : https://www.flickr.com/photos/howieluvzus/67248693/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 26 14:36:35 GMT+0100 2005
Upload Date  : Sat Nov 26 23:32:56 GMT+0100 2005
Views        : 132
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Audubon Zoo" "New Orleans" Thanksgiving Zoo Otter Post-Katrina 