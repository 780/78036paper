+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Steve Slater (used to be Wildlife Encounters)
Photo URL    : https://www.flickr.com/photos/wildlife_encounters/15649045626/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 27 12:37:36 GMT+0100 2014
Upload Date  : Fri Oct 31 12:40:21 GMT+0100 2014
Views        : 540
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter close up artistic


+---------------+
|  DESCRIPTION  |
+---------------+
Otter close up artistic


+--------+
|  TAGS  |
+--------+
abstract artistic "British Wildlife Centre" England "European Otter" "Fine art" HDR Mammals Wildlife "Wildlife Encounters" Eastbourne UK 