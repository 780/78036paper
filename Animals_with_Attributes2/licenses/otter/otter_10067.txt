+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lutramania
Photo URL    : https://www.flickr.com/photos/lutramania/5326008735/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 5 15:43:14 GMT+0100 2011
Upload Date  : Wed Jan 5 08:53:23 GMT+0100 2011
Views        : 126
Comments     : 1


+---------+
|  TITLE  |
+---------+
DSC03445e


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
otter "orana wildlife park" "asian small-clawed river otter" lutrinae zoo christchurch 