+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dr Stephen Dann
Photo URL    : https://www.flickr.com/photos/stephendann/4531007778/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 20 15:08:26 GMT+0100 2010
Upload Date  : Sun Apr 18 13:52:32 GMT+0200 2010
Views        : 38
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
canberra zoo animals canberrazoo otter 