+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Schristia
Photo URL    : https://www.flickr.com/photos/schristia/3986809722/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 5 13:44:29 GMT+0200 2009
Upload Date  : Tue Oct 6 10:33:51 GMT+0200 2009
Views        : 234
Comments     : 11


+---------+
|  TITLE  |
+---------+
Otter  (IMG_6508R)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
otter zoo NaturesFinest 