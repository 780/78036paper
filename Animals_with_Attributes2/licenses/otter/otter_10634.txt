+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : donjd2
Photo URL    : https://www.flickr.com/photos/ddebold/24889163950/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 20 12:47:10 GMT+0100 2016
Upload Date  : Mon Feb 22 05:19:44 GMT+0100 2016
Geotag Info  : Latitude:36.812702, Longitude:-121.771334
Views        : 87
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sea Otter Looking at Us at Moss Landing


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"elkhorn slough safari" "moss landing" "sea otter" California "United States" US 