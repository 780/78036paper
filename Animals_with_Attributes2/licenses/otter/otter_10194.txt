+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Feral78
Photo URL    : https://www.flickr.com/photos/emmettgrrrl/5025914759/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 18 16:22:50 GMT+0200 2010
Upload Date  : Sun Sep 26 17:41:25 GMT+0200 2010
Views        : 89
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter 2


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"New Forest" otter wildlife 