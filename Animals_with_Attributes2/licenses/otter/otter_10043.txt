+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Yortw
Photo URL    : https://www.flickr.com/photos/yortw/5083043011/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 21 11:38:15 GMT+0200 2010
Upload Date  : Fri Oct 15 13:02:54 GMT+0200 2010
Geotag Info  : Latitude:52.506752, Longitude:13.341243
Views        : 271
Comments     : 0


+---------+
|  TITLE  |
+---------+
BZ - Otters


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at Berlin Zoo.


+--------+
|  TAGS  |
+--------+
"Berlin Zoo" Berlin Animals Otter Europe DMC-G10 Lumix Panasonic MicroFourThirds 2010 microfourthirdsmicro43 Zoo 