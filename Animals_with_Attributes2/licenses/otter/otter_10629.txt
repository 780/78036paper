+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : CaliforniaDFW
Photo URL    : https://www.flickr.com/photos/californiadfg/22506533917/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Nov 10 09:55:58 GMT+0100 2015
Upload Date  : Tue Nov 10 19:44:01 GMT+0100 2015
Views        : 149
Comments     : 0


+---------+
|  TITLE  |
+---------+
sea otter (enhydra lutris)


+---------------+
|  DESCRIPTION  |
+---------------+
File photo.


+--------+
|  TAGS  |
+--------+
(no tags)