+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : claumoho
Photo URL    : https://www.flickr.com/photos/claudiah/3589947500/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 31 18:47:01 GMT+0200 2009
Upload Date  : Tue Jun 2 20:02:43 GMT+0200 2009
Views        : 129
Comments     : 0


+---------+
|  TITLE  |
+---------+
First glimpse of sea otters


+---------------+
|  DESCRIPTION  |
+---------------+
Holding their little paws out of the water...


+--------+
|  TAGS  |
+--------+
"Elkhorn Slough" otters 