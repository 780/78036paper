+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Spider.Dog
Photo URL    : https://www.flickr.com/photos/spiderdog/22553195858/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 18 13:09:43 GMT+0200 2015
Upload Date  : Thu Nov 12 22:49:15 GMT+0100 2015
Views        : 86
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kirkudbright Wildlife Park


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Kirkudbright Dumfries Galloway Wildlife Otter 