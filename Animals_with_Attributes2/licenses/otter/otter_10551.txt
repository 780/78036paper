+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Peter G Trimming
Photo URL    : https://www.flickr.com/photos/peter-trimming/5587972301/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 2 13:35:10 GMT+0200 2011
Upload Date  : Mon Apr 4 12:00:47 GMT+0200 2011
Views        : 690
Comments     : 2


+---------+
|  TITLE  |
+---------+
Otters


+---------------+
|  DESCRIPTION  |
+---------------+
At the British Wildlife Centre, Newchapel, Surrey; the otter cubs eye up some photographers.


+--------+
|  TAGS  |
+--------+
British Wildlife Centre Newchapel Surrey 2011 Otter Lutra Peter Trimming 