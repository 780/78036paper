+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : thethrillstheyyield
Photo URL    : https://www.flickr.com/photos/thethrillstheyyield/2942944538/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 11 12:02:55 GMT+0200 2008
Upload Date  : Tue Oct 14 23:52:01 GMT+0200 2008
Geotag Info  : Latitude:33.733052, Longitude:-84.371169
Views        : 32
Comments     : 0


+---------+
|  TITLE  |
+---------+
otters


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Zoo Atlanta" otters 