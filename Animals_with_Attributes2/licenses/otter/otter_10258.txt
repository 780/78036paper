+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : geraintandkim
Photo URL    : https://www.flickr.com/photos/geraintandkim/314782571/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 3 00:48:32 GMT+0100 2006
Upload Date  : Tue Dec 5 11:03:39 GMT+0100 2006
Views        : 39
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Sydney " Australia" " NSW" " Taronga Zoo" 