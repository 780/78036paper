+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ben Sutherland
Photo URL    : https://www.flickr.com/photos/bensutherland/5588647560/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 3 14:49:43 GMT+0200 2011
Upload Date  : Mon Apr 4 12:49:23 GMT+0200 2011
Views        : 373
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter and chick on the riverbank


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"birmingham nature centre" "nature centre" birmingham "west midlands" animals nature wildlife otter otters "otters eating" feeding "feeding time" chick chicks "dead chicks" "otters eating dead chicks" 