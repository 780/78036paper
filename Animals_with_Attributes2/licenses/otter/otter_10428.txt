+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rob Oo
Photo URL    : https://www.flickr.com/photos/105105658@N03/15755507093/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 2 16:55:30 GMT+0200 2014
Upload Date  : Mon Jan 26 22:03:07 GMT+0100 2015
Geotag Info  : Latitude:12.298786, Longitude:76.669736
Views        : 177
Comments     : 0


+---------+
|  TITLE  |
+---------+
Otter


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Zoo Animals ro016b 