+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jkirkhart35
Photo URL    : https://www.flickr.com/photos/jkirkhart35/3006817235/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Nov 4 14:42:02 GMT+0100 2008
Upload Date  : Thu Nov 6 08:10:53 GMT+0100 2008
Views        : 489
Comments     : 14


+---------+
|  TITLE  |
+---------+
Sea Otter Pair


+---------------+
|  DESCRIPTION  |
+---------------+
Now is the time to see the mother and its young at Morro Rock.  They play for a long time. The youngster is almost as big as the mother.  I believe the pup is behind her because of its darker face.


+--------+
|  TAGS  |
+--------+
(no tags)