+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Accretion Disc
Photo URL    : https://www.flickr.com/photos/befuddledsenses/8734092834/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 12 14:50:59 GMT+0200 2013
Upload Date  : Mon May 13 03:45:57 GMT+0200 2013
Geotag Info  : Latitude:61.124713, Longitude:-149.792232
Views        : 378
Comments     : 0


+---------+
|  TITLE  |
+---------+
River Otter


+---------------+
|  DESCRIPTION  |
+---------------+
A river otter at the Alaska Zoo in Anchorage.


+--------+
|  TAGS  |
+--------+
anchorage alaska zoo riverotter otter 