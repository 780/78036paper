+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : z2amiller
Photo URL    : https://www.flickr.com/photos/z2amiller/278074752/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 7 16:05:10 GMT+0200 2006
Upload Date  : Tue Oct 24 09:21:08 GMT+0200 2006
Views        : 167
Comments     : 0


+---------+
|  TITLE  |
+---------+
River otter


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
river otter sanfrancisco zoo sfzoo 