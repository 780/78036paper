+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : commorancy
Photo URL    : https://www.flickr.com/photos/commorancy/2527636210/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 25 16:23:25 GMT+0200 2008
Upload Date  : Tue May 27 10:30:06 GMT+0200 2008
Views        : 472
Comments     : 0


+---------+
|  TITLE  |
+---------+
Monterey Aquarium - Sleeping Otters


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Monterey Carmel "Pacific Grove" Sea Ocean Aquarium Flowers Wildlife california "pacific ocean" coast 