+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Aug 23, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Maggie Osterberg
Photo URL    : https://www.flickr.com/photos/mediawench/27338273864/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 27 14:39:04 GMT+0200 2016
Upload Date  : Tue Jun 28 04:51:19 GMT+0200 2016
Views        : 476
Comments     : 2


+---------+
|  TITLE  |
+---------+
Jemma And Michael At Play, June 27, 2016


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Fujifilm X100 maggieo Lincoln Nebraska dog "Rough Collie" "Jemma Simmons" "Color Efex Pro 4" 