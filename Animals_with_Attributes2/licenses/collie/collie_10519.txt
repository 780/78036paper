+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/5140792464/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 12 09:52:07 GMT+0200 2010
Upload Date  : Tue Nov 2 20:57:55 GMT+0100 2010
Geotag Info  : Latitude:56.800737, Longitude:8.236227
Views        : 3,501
Comments     : 2


+---------+
|  TITLE  |
+---------+
Rocky on the beach


+---------------+
|  DESCRIPTION  |
+---------------+
Rocky is the female border collie of my German friend, having fun with the waves on the beach. 

She is a kind of old lady but doesn't look like it.

Taken during the holidays in Denmark, in Agger / Versterwig.


+--------+
|  TAGS  |
+--------+
border collie black white dog canine female waves sand beach fun denmark holidays nikon d300 