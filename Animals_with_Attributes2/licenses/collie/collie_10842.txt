+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Karen Arnold
Photo URL    : http://www.publicdomainpictures.net/view-image.php?image=34045&picture=bearded-collie-dog
License      : Public Domain (https://creativecommons.org/publicdomain/zero/1.0/)