+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daf73r
Photo URL    : https://www.flickr.com/photos/daf73/4416768718/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 21 14:02:30 GMT+0100 2010
Upload Date  : Mon Mar 8 10:50:14 GMT+0100 2010
Geotag Info  : Latitude:43.149516, Longitude:11.154513
Views        : 120
Comments     : 0


+---------+
|  TITLE  |
+---------+
San Galgano


+---------------+
|  DESCRIPTION  |
+---------------+
Pazze corse nell'Abbazia di San Galgano, provincia di Siena.
Reckless inside the San Galgano Church, near Siena - Italy.


+--------+
|  TAGS  |
+--------+
Collie 