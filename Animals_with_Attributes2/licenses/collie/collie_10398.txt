+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dave Peake
Photo URL    : https://www.flickr.com/photos/davepeake/146151760/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 21 13:32:03 GMT+0200 2005
Upload Date  : Sun May 14 16:37:12 GMT+0200 2006
Geotag Info  : Latitude:-38.208106, Longitude:146.158676
Views        : 911
Comments     : 0


+---------+
|  TITLE  |
+---------+
Learning By Osmosis


+---------------+
|  DESCRIPTION  |
+---------------+
Jetson contemplating my lecture material.


+--------+
|  TAGS  |
+--------+
Jetson "Border Collie" 