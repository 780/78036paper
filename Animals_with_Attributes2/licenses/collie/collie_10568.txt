+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daf73r
Photo URL    : https://www.flickr.com/photos/daf73/4416814400/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 7 15:27:52 GMT+0100 2010
Upload Date  : Mon Mar 8 11:23:50 GMT+0100 2010
Geotag Info  : Latitude:42.891435, Longitude:11.628341
Views        : 348
Comments     : 2


+---------+
|  TITLE  |
+---------+
snow running


+---------------+
|  DESCRIPTION  |
+---------------+
Sulla neve del Monte Amiata. On the snow of Amiata Mountain, Italy.


+--------+
|  TAGS  |
+--------+
Collie snow 