+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : scottfeldstein
Photo URL    : https://www.flickr.com/photos/scottfeldstein/87502992/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 16 09:46:07 GMT+0100 2005
Upload Date  : Mon Jan 16 22:05:15 GMT+0100 2006
Views        : 824
Comments     : 0


+---------+
|  TITLE  |
+---------+
logan looking up the trail


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dog newfoundland collie wisconsin 