+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jinxmcc
Photo URL    : https://www.flickr.com/photos/64443083@N00/14163484158/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 4 13:16:34 GMT+0200 2014
Upload Date  : Thu Jun 5 07:55:53 GMT+0200 2014
Views        : 413
Comments     : 7


+---------+
|  TITLE  |
+---------+
Dinah meeting Roo


+---------------+
|  DESCRIPTION  |
+---------------+
We've adopted Roo --- a young border collie.  Today we brought her from her foster home in the South Bay, with a stop in San Francisco to show her to Dinah, and on home to Edgewood.  
Roo is quite young, and I'm sure there will be challenges as well as pleasures --- but once we saw her we had no doubt!


+--------+
|  TAGS  |
+--------+
Dinah Roo Rue BorderCollie BorderCollieRescueofNorthernCalifornia 