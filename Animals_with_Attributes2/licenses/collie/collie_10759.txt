+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : bogitw
Photo URL    : https://pixabay.com/en/dog-collie-animal-fur-pet-meadow-733778/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : May 28, 2011
Uploaded     : April 22, 2015

+--------+
|  TAGS  |
+--------+
Dog, Collie, Animal, Fur, Pet, Meadow, View