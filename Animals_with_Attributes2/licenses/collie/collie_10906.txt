+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Ron Armstrong
Photo URL    : https://commons.wikimedia.org/wiki/File:Border_Collie_agility_dogwalk.jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution 2.0 Generic (//creativecommons.org/licenses/by/2.0/deed.en)
Date         : 2007-04-27 16:32
