+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Laurel L. Ruswwurm
Photo URL    : https://commons.wikimedia.org/wiki/File:Rough_collie_on_the_porch.jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution 2.0 Generic (//creativecommons.org/licenses/by/2.0/deed.en)
Date         : 2011-02-14 01:43:41
