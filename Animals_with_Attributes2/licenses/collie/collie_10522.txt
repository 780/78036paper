+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Petful.com
Photo URL    : https://www.flickr.com/photos/petsadviser-pix/15965021373/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Feb 16 09:42:00 GMT+0100 2015
Upload Date  : Fri Feb 20 00:40:29 GMT+0100 2015
Views        : 354
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rough Collie-8-Topstar Blue Sanctuary 01


+---------------+
|  DESCRIPTION  |
+---------------+
Terms of Use: Please consider linking directly to our website, <a href="http://www.petful.com" rel="nofollow">www.petful.com</a>, rather than Flickr if you use this photo. Thanks for your support. 2015 Westminster Kennel Club Dog Show, New York City.


+--------+
|  TAGS  |
+--------+
"dog show" dogs Westminster dog "dog breed" "dog shows" conformation grooming best show breed 