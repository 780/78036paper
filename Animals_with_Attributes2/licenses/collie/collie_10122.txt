+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sonstroem
Photo URL    : https://www.flickr.com/photos/sonstroem/25028394800/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Feb 25 18:30:46 GMT+0100 2016
Upload Date  : Sun Feb 28 03:42:41 GMT+0100 2016
Views        : 147
Comments     : 0


+---------+
|  TITLE  |
+---------+
Border Collie


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"border collie" collie dog "dog behavior" happy mix playing 