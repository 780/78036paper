+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Karen Arnold
Photo URL    : http://www.publicdomainpictures.net/view-image.php?image=52641&picture=hund-laufen
License      : Public Domain (https://creativecommons.org/publicdomain/zero/1.0/)