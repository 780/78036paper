+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Karen Arnold
Photo URL    : http://www.publicdomainpictures.net/view-image.php?image=33182&picture=rough-collie-dog
License      : Public Domain (https://creativecommons.org/publicdomain/zero/1.0/)