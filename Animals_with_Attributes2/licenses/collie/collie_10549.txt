+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SheltieBoy
Photo URL    : https://www.flickr.com/photos/montanapets/7299909444/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 25 13:40:31 GMT+0200 2012
Upload Date  : Wed May 30 07:08:29 GMT+0200 2012
Views        : 450
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheepdog Trials in California


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
sheep sheepdog dog trial california herding border collie 