+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Aug 23, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : dogsbylori
Photo URL    : https://www.flickr.com/photos/dogsbylori/25830262643/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 19 08:50:25 GMT+0100 2016
Upload Date  : Fri Apr 15 00:21:38 GMT+0200 2016
Views        : 24
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_9603


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
photo by dogsbylori "Blue Merle Collie" "LaFortune Park in Tulsa, OK" LOGAN "Photographed by Lori Abrams Rauchwerger" "Robbie Bleu" "Rough Coat Collie" 