+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : adrusi
Photo URL    : https://www.flickr.com/photos/adrusi/5540776730/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 19 13:33:28 GMT+0100 2010
Upload Date  : Sat Mar 19 20:24:58 GMT+0100 2011
Views        : 532
Comments     : 0


+---------+
|  TITLE  |
+---------+
Collie


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"adrusi adrian sinclair lakes water wilde lake Columbia Howard County Maryland MD" animals animal wild wildlife life nature dog dogs collie 