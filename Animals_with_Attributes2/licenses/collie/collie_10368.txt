+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dave Peake
Photo URL    : https://www.flickr.com/photos/davepeake/134795770/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 18 13:18:29 GMT+0200 2005
Upload Date  : Tue Apr 25 14:54:31 GMT+0200 2006
Geotag Info  : Latitude:-38.208106, Longitude:146.158676
Views        : 585
Comments     : 0


+---------+
|  TITLE  |
+---------+
Jetson in the park


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Jetson border collie 