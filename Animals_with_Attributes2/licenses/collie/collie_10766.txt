+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : bogitw
Photo URL    : https://pixabay.com/en/dog-collie-fur-animal-pet-818063/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : May 17, 2012
Uploaded     : June 23, 2015

+--------+
|  TAGS  |
+--------+
Dog, Collie, Fur, Animal, Pet, Animal Portrait, View