+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jimbomack66
Photo URL    : https://www.flickr.com/photos/doggybytes/3670969996/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 10 16:06:31 GMT+0200 2009
Upload Date  : Mon Jun 29 06:51:33 GMT+0200 2009
Views        : 2,365
Comments     : 0


+---------+
|  TITLE  |
+---------+
Border Collie (Smooth Coat) - "Sweety"


+---------------+
|  DESCRIPTION  |
+---------------+
Sweety playing hide and seek at Beacon Hill Park along Dallas Rd.


+--------+
|  TAGS  |
+--------+
"border collie" dog 