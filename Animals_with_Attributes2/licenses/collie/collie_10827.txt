+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Colliefreund
Photo URL    : https://pixabay.com/en/collies-dogs-race-play-hunt-fur-388923/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : March 9, 2014
Uploaded     : July 12, 2014

+--------+
|  TAGS  |
+--------+
Collies, Dogs, Race, Play, Hunt, Fur, Great, Meadow