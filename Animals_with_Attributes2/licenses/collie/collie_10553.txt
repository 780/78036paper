+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SheltieBoy
Photo URL    : https://www.flickr.com/photos/montanapets/7310391490/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 27 08:45:19 GMT+0200 2012
Upload Date  : Thu May 31 22:48:40 GMT+0200 2012
Views        : 394
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheepdog Trials in California


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Sheepdog Trials dog sheep border collie California 