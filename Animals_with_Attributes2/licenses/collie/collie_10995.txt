+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Martin Steiger
Photo URL    : https://commons.wikimedia.org/wiki/File:Border_Collie.jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) CC0 1.0 Universal Public Domain Dedication (//creativecommons.org/publicdomain/zero/1.0/deed.en)
Date         : 2004-06-13
