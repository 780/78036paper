+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ian Blacker
Photo URL    : https://www.flickr.com/photos/ianblacker/8686179811/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 27 13:35:26 GMT+0200 2013
Upload Date  : Sat Apr 27 23:09:01 GMT+0200 2013
Views        : 104
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lenny sitting on a post


+---------------+
|  DESCRIPTION  |
+---------------+
SONY DSC


+--------+
|  TAGS  |
+--------+
border collie riverside park lenny handsome 