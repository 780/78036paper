+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mandydale
Photo URL    : https://www.flickr.com/photos/mandydale/367514495/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 13 13:16:56 GMT+0100 2006
Upload Date  : Wed Jan 24 02:30:13 GMT+0100 2007
Views        : 286
Comments     : 0


+---------+
|  TITLE  |
+---------+
rainy day with the dog


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
pilot "border collie" 