+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Highlander Collies
Photo URL    : https://commons.wikimedia.org/wiki/File:Blue_Merle_Rough_Collie.jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution 2.0 Generic (//creativecommons.org/licenses/by/2.0/deed.en)
Date         : 2010-03-04
