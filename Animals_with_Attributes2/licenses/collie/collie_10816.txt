+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : 825545
Photo URL    : https://pixabay.com/en/soap-bubbles-dog-672659/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : Aug. 28, 2014
Uploaded     : March 14, 2015

+--------+
|  TAGS  |
+--------+
Soap Bubbles, Dog, Dog Hunting Soap Bubbles, Playful