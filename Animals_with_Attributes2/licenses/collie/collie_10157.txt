+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Chris_Parfitt
Photo URL    : https://www.flickr.com/photos/chr1sp/4780510865/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 8 10:47:54 GMT+0200 2010
Upload Date  : Sat Jul 10 23:18:20 GMT+0200 2010
Views        : 704
Comments     : 0


+---------+
|  TITLE  |
+---------+
Collie Cross - Llansteffan Beach


+---------------+
|  DESCRIPTION  |
+---------------+
Llansteffan Beach


+--------+
|  TAGS  |
+--------+
"llansteffan beach" llansteffan carmarthenshire beach sea wales collie cross "collie cross" dog black stick sand 