+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Aug 23, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Maggie Osterberg
Photo URL    : https://www.flickr.com/photos/mediawench/28770167005/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 4 15:21:52 GMT+0200 2016
Upload Date  : Thu Aug 4 23:47:31 GMT+0200 2016
Views        : 202
Comments     : 1


+---------+
|  TITLE  |
+---------+
Amelia Pond And Jemma Simmons, June 04, 2016


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Olympus OM-D E-M5II Summilux25/1.4 maggieo Lincoln Nebraska cat "Amelia Pond" dog "Rough Collie" "Jemma Simmons" "Color Efex Pro 4" "LEICA DG SUMMILUX 25/F1.4" 