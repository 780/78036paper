+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Heis
Photo URL    : https://pixabay.com/en/dog-collie-play-beast-793243/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : July 13, 2010
Uploaded     : June 2, 2015

+--------+
|  TAGS  |
+--------+
Dog, Collie, Play, Beast