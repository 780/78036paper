+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : 825545
Photo URL    : https://pixabay.com/en/border-collie-dog-trick-662711/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : July 26, 2011
Uploaded     : March 7, 2015

+--------+
|  TAGS  |
+--------+
Border Collie, Dog Trick, Dog Show Trick, Beach