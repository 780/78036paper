+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Patrick Hoesly
Photo URL    : https://www.flickr.com/photos/zooboing/4234817346/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 1 14:23:46 GMT+0100 2010
Upload Date  : Fri Jan 1 21:23:50 GMT+0100 2010
Geotag Info  : Latitude:39.299420, Longitude:-94.588669
Views        : 3,798
Comments     : 0


+---------+
|  TITLE  |
+---------+
Shalla the Energetic Border Collie


+---------------+
|  DESCRIPTION  |
+---------------+
This is a photo I took on my iPhone 3G and processed with the Photo fx app.  Both this image and the previous are grainy due to the poor iPhone camera, however this image used the Smoque filter (which bleaches and blurs an image).


+--------+
|  TAGS  |
+--------+
Shalla border collie energetic run snow Patrick Hoesly Photo-A-Day Daily Photo Journal ZooBoing cold ice white water frozen nature snowfall powder slush sleet crystal drift flake frost freez chill zero bitter cool frigid frosty icy iced nippy polar shiver winter wintery "Patrick Hoesly" 