+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SheltieBoy
Photo URL    : https://www.flickr.com/photos/montanapets/7298175208/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 25 10:41:05 GMT+0200 2012
Upload Date  : Wed May 30 01:05:37 GMT+0200 2012
Views        : 650
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheepdog Trials in California


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
sheep sheepdog dog trial california herding border collie 