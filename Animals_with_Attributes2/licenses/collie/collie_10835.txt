+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Karen Arnold
Photo URL    : http://www.publicdomainpictures.net/view-image.php?image=34258&picture=border-collie-hund
License      : Public Domain (https://creativecommons.org/publicdomain/zero/1.0/)