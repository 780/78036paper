+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : 825545
Photo URL    : https://pixabay.com/en/border-collie-running-dog-field-672572/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : Sept. 16, 2014
Uploaded     : March 15, 2015

+--------+
|  TAGS  |
+--------+
Border Collie, Running Dog, Field, Summer