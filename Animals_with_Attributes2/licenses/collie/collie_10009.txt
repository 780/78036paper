+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Pam loves pie
Photo URL    : https://www.flickr.com/photos/pamlovespie/6867293676/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 17 15:47:47 GMT+0100 2012
Upload Date  : Sat Mar 17 16:47:47 GMT+0100 2012
Views        : 1,183
Comments     : 1


+---------+
|  TITLE  |
+---------+
077 of 366


+---------------+
|  DESCRIPTION  |
+---------------+
Visit to the Lakes means a visit to see Megan - always happy to pose - thank you!


+--------+
|  TAGS  |
+--------+
"Border Collie" Megan 