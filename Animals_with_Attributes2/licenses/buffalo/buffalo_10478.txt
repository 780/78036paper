+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mmmavocado
Photo URL    : https://www.flickr.com/photos/mmmavocado/2493799438/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 5 13:33:41 GMT+0200 2008
Upload Date  : Thu May 15 03:05:41 GMT+0200 2008
Views        : 163
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison, relaxing


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Yellowstone animals nature "national park" wildlife animal 