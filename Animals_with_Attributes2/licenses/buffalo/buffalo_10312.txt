+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Luis in Denver
Photo URL    : https://www.flickr.com/photos/11263821@N05/2239715460/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 4 10:26:36 GMT+0200 2005
Upload Date  : Sun Feb 3 18:26:36 GMT+0100 2008
Geotag Info  : Latitude:39.711148, Longitude:-105.318975
Views        : 896
Comments     : 1


+---------+
|  TITLE  |
+---------+
Bison


+---------------+
|  DESCRIPTION  |
+---------------+
Bison as viewed from the overlook on I-70 in Jefferson County, Colorado


+--------+
|  TAGS  |
+--------+
bison buffalo Colorado 