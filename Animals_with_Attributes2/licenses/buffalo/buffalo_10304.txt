+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pacificpelican
Photo URL    : https://www.flickr.com/photos/dinosaurcountry/528336376/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 26 11:01:37 GMT+0200 2007
Upload Date  : Sun Jun 3 22:23:52 GMT+0200 2007
Views        : 413
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison


+---------------+
|  DESCRIPTION  |
+---------------+
by Daniel J. McKeown, May 2007. [IMG_2741]


+--------+
|  TAGS  |
+--------+
YellowstoneNationalPark buffalo bison 