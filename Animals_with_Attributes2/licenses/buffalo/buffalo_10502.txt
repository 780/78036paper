+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hjjanisch
Photo URL    : https://www.flickr.com/photos/hjjanisch/3485488947/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 26 10:37:57 GMT+0200 2009
Upload Date  : Wed Apr 29 16:24:51 GMT+0200 2009
Geotag Info  : Latitude:47.233032, Longitude:15.814132
Views        : 407
Comments     : 0


+---------+
|  TITLE  |
+---------+
bisons


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
austria styria "tierpark herberstein" hdr sony bisons 