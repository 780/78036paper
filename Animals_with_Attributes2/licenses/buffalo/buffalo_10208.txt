+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : LandBetweenTheLakesKYTN
Photo URL    : https://www.flickr.com/photos/lblkytn/15745716649/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 13 17:04:23 GMT+0200 2013
Upload Date  : Tue Dec 2 18:23:31 GMT+0100 2014
Views        : 355
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison in EBP


+---------------+
|  DESCRIPTION  |
+---------------+
Bison enjoy grazing in the Prairie. Photo by Kelly Best Bennett


+--------+
|  TAGS  |
+--------+
"LBL Photos" LandBetweenTheLakesKYTN "Land Between The Lakes" "US Forest Service" nature "environmental education" "Kentucky Lake" "Lake Barkley" history camping birding biking hiking hunting Kentucky Tennessee "public lands" "scenic driving" "wildlife watching" 