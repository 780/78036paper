+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : djtansey
Photo URL    : https://www.flickr.com/photos/djtansey/47364257/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 19 07:22:58 GMT+0200 2005
Upload Date  : Wed Sep 28 09:21:11 GMT+0200 2005
Views        : 309
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_1191.JPG


+---------------+
|  DESCRIPTION  |
+---------------+
Baby!


+--------+
|  TAGS  |
+--------+
Yellowstone bison buffalo baby 