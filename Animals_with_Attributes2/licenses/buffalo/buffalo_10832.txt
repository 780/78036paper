+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Larry Smith2010
Photo URL    : https://www.flickr.com/photos/lsmith2010/15186337298/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 27 08:02:08 GMT+0200 2014
Upload Date  : Sat Sep 27 23:06:52 GMT+0200 2014
Geotag Info  : Latitude:34.724994, Longitude:-98.680375
Views        : 2,833
Comments     : 2


+---------+
|  TITLE  |
+---------+
Wichita Mountain Sunrise 9-27-14


+---------------+
|  DESCRIPTION  |
+---------------+
Wichita Mountains Wildlife Refuge
SW Oklahoma


+--------+
|  TAGS  |
+--------+
Superior "Wichita Mountains" "Larry Smith" Oklahoma bison sunrise "Wichita Mountains Wildlife Refuge" 