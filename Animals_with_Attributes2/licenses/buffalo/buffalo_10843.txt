+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : TuckerH586
Photo URL    : https://www.flickr.com/photos/60314725@N00/5484574252/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Feb 23 08:36:25 GMT+0100 2011
Upload Date  : Mon Feb 28 04:21:00 GMT+0100 2011
Views        : 487
Comments     : 2


+---------+
|  TITLE  |
+---------+
Winter Grazing


+---------------+
|  DESCRIPTION  |
+---------------+
Bison scratching away to find anything to eat under the snow.


+--------+
|  TAGS  |
+--------+
Yellowstone "Bison Yellowstone Winter" 