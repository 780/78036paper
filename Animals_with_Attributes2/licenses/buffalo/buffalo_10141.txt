+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Wade_Lehmann
Photo URL    : https://www.flickr.com/photos/68956031@N08/6960137536/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 17 17:53:52 GMT+0200 2011
Upload Date  : Mon Apr 23 16:40:44 GMT+0200 2012
Geotag Info  : Latitude:41.088149, Longitude:-112.172470
Views        : 68
Comments     : 0


+---------+
|  TITLE  |
+---------+
bison antelope island utah


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)