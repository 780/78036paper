+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : egvvnd
Photo URL    : https://www.flickr.com/photos/ericvv/3597239646/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 4 11:43:45 GMT+0200 2009
Upload Date  : Fri Jun 5 05:37:59 GMT+0200 2009
Views        : 93
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Brookfield Zoo Chicago Illinois 