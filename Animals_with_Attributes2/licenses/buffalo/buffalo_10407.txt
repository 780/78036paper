+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Watson Lake
Photo URL    : https://www.flickr.com/photos/yukonlife/6744219247/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 14 11:12:35 GMT+0100 2010
Upload Date  : Sun Jan 22 21:17:00 GMT+0100 2012
Geotag Info  : Latitude:59.927151, Longitude:-127.436599
Views        : 4,953
Comments     : 16


+---------+
|  TITLE  |
+---------+
Bison


+---------------+
|  DESCRIPTION  |
+---------------+
Individuals can be spotted along the Alaska Highway east of Watson Lake, pushing aside the snow to get to the grass below - usually on the sunny side of the highway.


+--------+
|  TAGS  |
+--------+
bison wildlife snow winter nature 