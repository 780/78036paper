+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Oregon State University
Photo URL    : https://www.flickr.com/photos/oregonstateuniversity/15210024320/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 2 16:00:58 GMT+0200 2012
Upload Date  : Tue Sep 30 02:15:34 GMT+0200 2014
Views        : 445
Comments     : 0


+---------+
|  TITLE  |
+---------+
BisonAndAspen_byLukePainter


+---------------+
|  DESCRIPTION  |
+---------------+
In Yellowstone National Park in 2012, bison had increased as elk had decreased, but these trends had not prevented a general aspen recovery beginning in the eastern part of the northern range. Aspen in this photo show the historical gap (saplings and mature trees) in recruitment and also the new young aspen that are now above the reach of elk and bison.


+--------+
|  TAGS  |
+--------+
(no tags)