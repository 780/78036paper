+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : U.S. Fish and Wildlife Service - Midwest Region
Photo URL    : https://www.flickr.com/photos/usfwsmidwest/17286345386/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 29 10:11:10 GMT+0200 2015
Upload Date  : Wed Apr 29 17:12:42 GMT+0200 2015
Views        : 2,048
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison with calf at Neal Smith National Wildlife Refuge


+---------------+
|  DESCRIPTION  |
+---------------+
Neal Smith National Wildlife Refuge in Iowa now has six bison calves!

Photo by Doreen Van Ryswyk/USFWS.


+--------+
|  TAGS  |
+--------+
Bison Buffalo Calf Young Baby Calves Mammal Iowa Spring "Neal Smith" NWR Refuge "National Wildlife Refuge" IA "Creative Commons" USFWS "U.S. Fish and Wildlife Service" Midwest 