+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rachaelvoorhees
Photo URL    : https://www.flickr.com/photos/rachaelvoorhees/2543444566/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 30 23:10:45 GMT+0200 2008
Upload Date  : Mon Jun 2 02:07:43 GMT+0200 2008
Views        : 78
Comments     : 0


+---------+
|  TITLE  |
+---------+
A lil bison friend?


+---------------+
|  DESCRIPTION  |
+---------------+
Yellowstone National Park, Wyoming


+--------+
|  TAGS  |
+--------+
wyoming bison yellowstone 