+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pixel0908
Photo URL    : https://www.flickr.com/photos/pixelpoint/2770703021/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 17 15:11:44 GMT+0200 2008
Upload Date  : Sun Aug 17 18:46:51 GMT+0200 2008
Views        : 193
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison


+---------------+
|  DESCRIPTION  |
+---------------+
Aufgenommen im Tierpark Rosegg in Kärnten.


+--------+
|  TAGS  |
+--------+
bison büffel rosegg tierpark 