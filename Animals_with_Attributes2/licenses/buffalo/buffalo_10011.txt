+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Raymond Hitchcock
Photo URL    : https://www.flickr.com/photos/spendadaytouring/3625940855/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 16 18:24:59 GMT+0200 2002
Upload Date  : Sun Jun 14 23:40:58 GMT+0200 2009
Views        : 459
Comments     : 0


+---------+
|  TITLE  |
+---------+
33218 Fort Peck Bison


+---------------+
|  DESCRIPTION  |
+---------------+
Springtime at Fort Peck.


+--------+
|  TAGS  |
+--------+
Fort Peck Bison Montana Missouri River Country SeattleRay spendadaytouring' 