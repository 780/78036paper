+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brian.gratwicke
Photo URL    : https://www.flickr.com/photos/briangratwicke/7848000152/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 17 00:02:58 GMT+0200 2012
Upload Date  : Fri Aug 24 03:20:29 GMT+0200 2012
Geotag Info  : Latitude:44.679464, Longitude:-110.486783
Views        : 448
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison at sunset


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Yellowstone Wyoming USA National Park #greatnatureshots 