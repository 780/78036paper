+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lansstyrelsenjonkoping
Photo URL    : https://www.flickr.com/photos/jonkopings_lan/16401481915/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 30 02:05:41 GMT+0100 2015
Upload Date  : Fri Jan 30 11:05:41 GMT+0100 2015
Views        : 160
Comments     : 0


+---------+
|  TITLE  |
+---------+
Inte varje dag man träffar en amerikansk bison😃. Vi hade turen att göra detta i samband med tillsyn av vilthägn. #bison #vilthägn #jönköpingslän


+---------------+
|  DESCRIPTION  |
+---------------+
via Instagram <a href="http://ift.tt/1A5JoLA" rel="nofollow">ift.tt/1A5JoLA</a>


+--------+
|  TAGS  |
+--------+
IFTTT Instagram 