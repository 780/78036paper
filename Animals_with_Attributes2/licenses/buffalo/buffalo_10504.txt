+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : michael pollak
Photo URL    : https://www.flickr.com/photos/michaelpollak/5388330609/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 16 16:18:50 GMT+0100 2011
Upload Date  : Wed Jan 26 00:54:45 GMT+0100 2011
Views        : 511
Comments     : 0


+---------+
|  TITLE  |
+---------+
bisons


+---------------+
|  DESCRIPTION  |
+---------------+
those are big, beautiful creatures


+--------+
|  TAGS  |
+--------+
czech_canada1 ausflug wandern bisons cz 