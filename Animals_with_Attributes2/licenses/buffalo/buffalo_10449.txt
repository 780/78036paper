+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : André-Pierre
Photo URL    : https://www.flickr.com/photos/andrepierre/14842916459/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 9 16:58:43 GMT+0200 2014
Upload Date  : Mon Aug 25 12:07:51 GMT+0200 2014
Views        : 128
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)