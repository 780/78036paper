+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : magnus.johansson10
Photo URL    : https://www.flickr.com/photos/120374925@N06/15005722562/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 22 16:27:44 GMT+0200 2014
Upload Date  : Sat Aug 23 09:11:17 GMT+0200 2014
Views        : 2,184
Comments     : 1


+---------+
|  TITLE  |
+---------+
bison-1924


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo Sweden Stockholm skansen Animals bisonoxe bison oxe 