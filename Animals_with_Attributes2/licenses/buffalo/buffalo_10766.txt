+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : YellowstoneNPS
Photo URL    : https://www.flickr.com/photos/yellowstonenps/14284883905/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 10 12:41:58 GMT+0200 2014
Upload Date  : Tue May 27 20:31:23 GMT+0200 2014
Views        : 7,555
Comments     : 0


+---------+
|  TITLE  |
+---------+
Please drive carefully


+---------------+
|  DESCRIPTION  |
+---------------+
Bison cow nursing her calf in the middle of the road in Lamar Valley;
Neal Herbert;
May 2014;
Catalog #19424d;
Original #7369


+--------+
|  TAGS  |
+--------+
bison calf "Yellowstone National Park" 