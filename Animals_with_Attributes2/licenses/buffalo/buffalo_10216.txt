+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : YellowstoneNPS
Photo URL    : https://www.flickr.com/photos/yellowstonenps/15534019716/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 6 08:27:02 GMT+0200 2014
Upload Date  : Fri Oct 17 20:04:50 GMT+0200 2014
Views        : 3,428
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison in rut


+---------------+
|  DESCRIPTION  |
+---------------+
Bison bull in rut and cow;
Neal Herbert;
August 2014;
Catalog #19684d;
Original #3156


+--------+
|  TAGS  |
+--------+
Yellowstone "Yellowstone National Park" "national park" 