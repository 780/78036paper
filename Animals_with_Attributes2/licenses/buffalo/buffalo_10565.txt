+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USFWS Mountain Prairie
Photo URL    : https://www.flickr.com/photos/usfwsmtnprairie/15113406033/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 20 07:47:18 GMT+0200 2007
Upload Date  : Fri Nov 7 17:49:28 GMT+0100 2014
Views        : 1,420
Comments     : 0


+---------+
|  TITLE  |
+---------+
Denver's Bison


+---------------+
|  DESCRIPTION  |
+---------------+
A group of bison walk through the prairie with the Denver skyline in the background at Rocky Mountain Arsenal National Wildlife Refuge.

Photo Credit: Rich Keen / DPRA


+--------+
|  TAGS  |
+--------+
USFWS FWS "Rocky Mountain Arsenal National Wildlife Refuge" Refuge NATURE CONSERVATION WILDLIFE "Wildlife Refuge" Bison "U.S. FISH AND WILDLIFE SERVICE" "U.S. FISH & WILDLIFE SERVICE" "National Wildlife Refuge System" NWRS "National Wildlife Refuge" 