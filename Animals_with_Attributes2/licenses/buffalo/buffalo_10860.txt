+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dan'l  Burton
Photo URL    : https://www.flickr.com/photos/danielburton/2084497200/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 15 11:18:02 GMT+0200 2007
Upload Date  : Mon Dec 3 18:39:47 GMT+0100 2007
Views        : 321
Comments     : 0


+---------+
|  TITLE  |
+---------+
Yellowstone Buffalo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Yellowstone Bison 