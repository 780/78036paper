+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : paleololigo
Photo URL    : https://www.flickr.com/photos/paleololigo/3748450906/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 22 16:33:32 GMT+0200 2009
Upload Date  : Thu Jul 23 05:47:10 GMT+0200 2009
Views        : 185
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Yellowstone "Mud Volcano" 