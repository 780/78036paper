+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : geff47
Photo URL    : https://www.flickr.com/photos/geff47/6363644381/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 12 09:17:26 GMT+0200 2011
Upload Date  : Sat Nov 19 17:11:15 GMT+0100 2011
Views        : 92
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_3191


+---------------+
|  DESCRIPTION  |
+---------------+
Plains Bison in Grand Teton National Park


+--------+
|  TAGS  |
+--------+
Grand Teton N.P. 