+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MSVG
Photo URL    : https://www.flickr.com/photos/msvg/5503402493/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 6 14:53:47 GMT+0100 2011
Upload Date  : Sun Mar 6 22:32:03 GMT+0100 2011
Geotag Info  : Latitude:43.644029, Longitude:-79.463875
Views        : 711
Comments     : 13


+---------+
|  TITLE  |
+---------+
Running into Bison... Literally


+---------------+
|  DESCRIPTION  |
+---------------+
I went for a run at High Park only to be greated by these huge creatures!


+--------+
|  TAGS  |
+--------+
Bison High Park Zoo Toronto Ontario Canada 