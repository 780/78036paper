+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : wfvanvalkenburg
Photo URL    : https://www.flickr.com/photos/10013573@N05/17118657467/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 29 16:18:03 GMT+0200 2015
Upload Date  : Fri May 1 00:42:33 GMT+0200 2015
Views        : 190
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_4916.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
alberta animal bison canada "elk island national park" 