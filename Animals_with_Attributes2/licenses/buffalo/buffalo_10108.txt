+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : iamsch
Photo URL    : https://www.flickr.com/photos/iamsch/14221736970/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 3 10:56:37 GMT+0200 2014
Upload Date  : Thu Jun 12 23:41:26 GMT+0200 2014
Views        : 186
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison 1


+---------------+
|  DESCRIPTION  |
+---------------+
A European Bison at a bison breeding center in Niepołomice


+--------+
|  TAGS  |
+--------+
(no tags)