+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gvgoebel
Photo URL    : https://www.flickr.com/photos/37467370@N08/7612046426/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 20 15:43:36 GMT+0200 2012
Upload Date  : Sat Jul 21 00:48:11 GMT+0200 2012
Views        : 611
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ybbuf_2b


+---------------+
|  DESCRIPTION  |
+---------------+
buffalo (bison), Denver Zoo, Colorado / 2006


+--------+
|  TAGS  |
+--------+
animals mammals buffalo bison 