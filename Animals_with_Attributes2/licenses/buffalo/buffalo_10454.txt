+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : YellowstoneNPS
Photo URL    : https://www.flickr.com/photos/yellowstonenps/15430968045/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 9 18:36:12 GMT+0100 2014
Upload Date  : Fri Oct 3 21:37:20 GMT+0200 2014
Views        : 2,578
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison


+---------------+
|  DESCRIPTION  |
+---------------+
Wet bison;
Neal Herbert;
February 2014;
Catalog #19441d;
Original #2951


+--------+
|  TAGS  |
+--------+
(no tags)