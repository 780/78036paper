+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Derek White
Photo URL    : https://www.flickr.com/photos/39983106@N04/15348063606/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 23 10:23:21 GMT+0200 2014
Upload Date  : Sat Sep 27 19:56:05 GMT+0200 2014
Views        : 59
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lone Bull


+---------------+
|  DESCRIPTION  |
+---------------+
Photo by Derek White


+--------+
|  TAGS  |
+--------+
bison bull wild Tetons bokeh 