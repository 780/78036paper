+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blmurch
Photo URL    : https://www.flickr.com/photos/blmurch/270524088/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 14 16:04:30 GMT+0200 2006
Upload Date  : Sun Oct 15 22:36:06 GMT+0200 2006
Geotag Info  : Latitude:10.497939, Longitude:-66.928024
Views        : 235
Comments     : 0


+---------+
|  TITLE  |
+---------+
Buffalo


+---------------+
|  DESCRIPTION  |
+---------------+
Caracas Zoo


+--------+
|  TAGS  |
+--------+
Caracas Venezuela Zoo Bison 