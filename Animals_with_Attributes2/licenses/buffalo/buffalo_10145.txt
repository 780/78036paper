+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ginger Beered
Photo URL    : https://www.flickr.com/photos/tbennett/264079568/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 7 20:13:39 GMT+0200 2006
Upload Date  : Sun Oct 8 20:11:15 GMT+0200 2006
Views        : 101
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison at Golden Gate Park - 2


+---------------+
|  DESCRIPTION  |
+---------------+
Taken while attending the Hardly Strictly Bluegrass festival at Golden Gate Park


+--------+
|  TAGS  |
+--------+
"San Francisco" 