+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : miketnorton
Photo URL    : https://www.flickr.com/photos/mtnorton/14986134332/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 6 12:31:32 GMT+0200 2014
Upload Date  : Wed Aug 6 19:31:32 GMT+0200 2014
Geotag Info  : Latitude:44.909082, Longitude:-110.355777
Views        : 211
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lone Bison


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
travel "american west" west bison buffalo wyoming montana yellowstone "yellowstone national park" "national park" 