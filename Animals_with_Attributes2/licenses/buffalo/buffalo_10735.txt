+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rachaelvoorhees
Photo URL    : https://www.flickr.com/photos/rachaelvoorhees/2537099741/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 30 06:28:55 GMT+0200 2008
Upload Date  : Sat May 31 04:44:50 GMT+0200 2008
Views        : 216
Comments     : 0


+---------+
|  TITLE  |
+---------+
*munch munch munch*


+---------------+
|  DESCRIPTION  |
+---------------+
Yellowstone National Park, Wyoming


+--------+
|  TAGS  |
+--------+
yellowstone "yellowstone national park" bison 