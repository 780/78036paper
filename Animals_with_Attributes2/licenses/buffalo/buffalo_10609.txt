+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : _e.t
Photo URL    : https://www.flickr.com/photos/45688285@N00/10820904/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 23 10:51:26 GMT+0200 2005
Upload Date  : Mon Apr 25 09:40:27 GMT+0200 2005
Views        : 373
Comments     : 0


+---------+
|  TITLE  |
+---------+
Earth Day @ Golden Gate Park.


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
earthday goldengatepark buffalo bison 