+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : AndyMcLemore
Photo URL    : https://www.flickr.com/photos/andy_emcee/3778889902/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 1 10:21:40 GMT+0200 2009
Upload Date  : Sat Aug 1 21:17:03 GMT+0200 2009
Views        : 160
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC00518


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
LBL "Land Between the Lakes" bison "American Bison" "Bison bison" 