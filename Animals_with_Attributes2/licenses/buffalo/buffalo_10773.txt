+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DaffyDuke
Photo URL    : https://www.flickr.com/photos/daffyduke/14652113580/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 17 17:50:37 GMT+0200 2014
Upload Date  : Tue Aug 5 22:32:19 GMT+0200 2014
Geotag Info  : Latitude:49.774114, Longitude:1.184291
Views        : 204
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rêve de Bisons


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
vacances normandie bisons 