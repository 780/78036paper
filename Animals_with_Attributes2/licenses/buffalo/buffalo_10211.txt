+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mark.hogan
Photo URL    : https://www.flickr.com/photos/markhogan/14025926572/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 9 16:16:34 GMT+0100 2014
Upload Date  : Sun Apr 27 07:51:37 GMT+0200 2014
Views        : 136
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison in Golden Gate Park


+---------------+
|  DESCRIPTION  |
+---------------+
A weekend wander around Golden Gate Park.


+--------+
|  TAGS  |
+--------+
"San Francisco" California park 