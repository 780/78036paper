+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Photommo
Photo URL    : https://www.flickr.com/photos/photommo/14603600249/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 8 09:26:58 GMT+0200 2014
Upload Date  : Thu Jul 31 02:34:06 GMT+0200 2014
Geotag Info  : Latitude:43.900366, Longitude:-102.284774
Views        : 638
Comments     : 4


+---------+
|  TITLE  |
+---------+
Tatanka


+---------------+
|  DESCRIPTION  |
+---------------+
Badlands National Park, South Dakota USA


+--------+
|  TAGS  |
+--------+
Tatanka buffalo bison badlands national park usa america 2014 south dakota grasslands animal wildlife wild 