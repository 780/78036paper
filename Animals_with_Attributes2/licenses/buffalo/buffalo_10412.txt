+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tim Alamenciak
Photo URL    : https://www.flickr.com/photos/timalamenciak/7058168001/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 8 09:43:34 GMT+0200 2012
Upload Date  : Sun Apr 8 23:26:18 GMT+0200 2012
Views        : 127
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bison


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"high park" toronto zoo animals parks nature forest 