+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jsichitaro
Photo URL    : https://www.flickr.com/photos/60424013@N08/5525715010/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 14 00:42:05 GMT+0100 2011
Upload Date  : Mon Mar 14 08:42:05 GMT+0100 2011
Views        : 316
Comments     : 1


+---------+
|  TITLE  |
+---------+
dolphin


+---------------+
|  DESCRIPTION  |
+---------------+
The photo of the dolphin which I took in a aquarium


+--------+
|  TAGS  |
+--------+
dolphin 