+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aaron_anderer
Photo URL    : https://www.flickr.com/photos/aaron_anderer/15478325113/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 25 03:00:53 GMT+0200 2014
Upload Date  : Wed Dec 24 23:39:02 GMT+0100 2014
Views        : 56
Comments     : 0


+---------+
|  TITLE  |
+---------+
CHT_9436


+---------------+
|  DESCRIPTION  |
+---------------+
Swimming with Dolphins in the Cayman Islands.


+--------+
|  TAGS  |
+--------+
dolphin swimming cruise cayman islands 