+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : John Biehler
Photo URL    : https://www.flickr.com/photos/retrocactus/5929473980/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 5 14:00:18 GMT+0200 2011
Upload Date  : Tue Jul 12 08:44:07 GMT+0200 2011
Geotag Info  : Latitude:59.884165, Longitude:-149.540691
Views        : 140
Comments     : 0


+---------+
|  TITLE  |
+---------+
Alaskan Adventure


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://johnbiehler.com" rel="nofollow">johnbiehler.com</a>


+--------+
|  TAGS  |
+--------+
voltalaska trip travel alaska dolphins 