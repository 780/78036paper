+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tkcrash123
Photo URL    : https://www.flickr.com/photos/9744413@N07/1960199682/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 7 13:07:37 GMT+0100 2007
Upload Date  : Sun Nov 11 06:27:39 GMT+0100 2007
Views        : 4,310
Comments     : 3


+---------+
|  TITLE  |
+---------+
Dolphin


+---------------+
|  DESCRIPTION  |
+---------------+
So peaceful to watch !


+--------+
|  TAGS  |
+--------+
dolphin water peacefull n.s.w Coffs-Harbour nature 