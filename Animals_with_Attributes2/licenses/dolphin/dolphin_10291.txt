+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Steve's stills
Photo URL    : https://www.flickr.com/photos/96510847@N06/10467358943/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 16 22:47:45 GMT+0200 2012
Upload Date  : Fri Oct 25 02:50:43 GMT+0200 2013
Geotag Info  : Latitude:16.330160, Longitude:-86.519371
Views        : 5,407
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dolphin


+---------------+
|  DESCRIPTION  |
+---------------+
Roatan, Honduras
Anthony's Key Resort
Roatan Institute for Marine Sciences


+--------+
|  TAGS  |
+--------+
Roatan "Roatan Honduras" Swim Dolphin Carribean "Anthony's Key Resort" "Roatan Institute for Marine Sciences" "Bailey's Key" Kenny 