+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Vegard Haugland
Photo URL    : https://www.flickr.com/photos/v3gard/8385186956/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 4 15:17:19 GMT+0100 2013
Upload Date  : Tue Jan 15 23:28:34 GMT+0100 2013
Views        : 329
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dolphin Dance


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dolphin "dolphin show" "gran canaria" holiday "palmitos park" 