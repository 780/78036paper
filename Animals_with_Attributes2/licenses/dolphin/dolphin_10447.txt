+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nicklas Lundqvist
Photo URL    : https://www.flickr.com/photos/nicklaslundqvist/25238259043/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 27 22:50:16 GMT+0200 2014
Upload Date  : Thu Mar 17 20:12:50 GMT+0100 2016
Views        : 372
Comments     : 4


+---------+
|  TITLE  |
+---------+
Dolphins


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dolphin 