+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Seba Sofariu
Photo URL    : https://www.flickr.com/photos/seba_sofariu/2616029406/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 27 14:41:03 GMT+0200 2008
Upload Date  : Fri Jun 27 15:41:03 GMT+0200 2008
Geotag Info  : Latitude:41.619035, Longitude:12.461929
Views        : 403
Comments     : 6


+---------+
|  TITLE  |
+---------+
... my friend


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dolphins blue 