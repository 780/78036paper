+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : WIlly Volk
Photo URL    : https://www.flickr.com/photos/volk/1113664739/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 30 09:23:27 GMT+0200 2007
Upload Date  : Tue Aug 14 12:47:43 GMT+0200 2007
Geotag Info  : Latitude:26.694551, Longitude:-79.005646
Views        : 7,234
Comments     : 3


+---------+
|  TITLE  |
+---------+
Dolphins


+---------------+
|  DESCRIPTION  |
+---------------+
They were very friendly and curious.


+--------+
|  TAGS  |
+--------+
dolphin bahamas dolphins "atlantic spotted dolphins" scuba dive diver diving 