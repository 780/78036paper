+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lakewentworth
Photo URL    : https://www.flickr.com/photos/roome/3564868715/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 25 23:33:31 GMT+0200 2009
Upload Date  : Tue May 26 05:33:31 GMT+0200 2009
Geotag Info  : Latitude:25.018884, Longitude:-77.548284
Views        : 353
Comments     : 10


+---------+
|  TITLE  |
+---------+
Dolphin Pastel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dolphin bahamas ocean blue 