+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eoghann Irving
Photo URL    : https://www.flickr.com/photos/eoghann/4023335802/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 27 11:40:39 GMT+0200 2009
Upload Date  : Sun Oct 18 20:40:45 GMT+0200 2009
Views        : 34
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dolphins!


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://posterous.com" rel="nofollow">Posted via email</a>  from <a href="http://eoghann.posterous.com/dolphins-22" rel="nofollow">eoghann's posterous</a>


+--------+
|  TAGS  |
+--------+
baltimore aquarium sealife dolphins 