+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : San Diego Shooter
Photo URL    : https://www.flickr.com/photos/nathaninsandiego/5371396442/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 15 12:07:31 GMT+0100 2011
Upload Date  : Thu Jan 20 00:14:16 GMT+0100 2011
Geotag Info  : Latitude:32.765751, Longitude:-117.227168
Views        : 1,839
Comments     : 3


+---------+
|  TITLE  |
+---------+
6 dolphins jumping


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"san diego" dolphin dolphins "sea world" "sea world san diego" "dolphins jumping" 