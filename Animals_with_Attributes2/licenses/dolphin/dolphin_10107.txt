+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : krzy4rc
Photo URL    : https://www.flickr.com/photos/whitlow/27961998695/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 27 11:17:05 GMT+0200 2016
Upload Date  : Tue Jun 28 18:34:16 GMT+0200 2016
Views        : 73
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cruise2016-8913


+---------------+
|  DESCRIPTION  |
+---------------+
2016 Cruise
Amber Cove
Dominican Republic


+--------+
|  TAGS  |
+--------+
(no tags)