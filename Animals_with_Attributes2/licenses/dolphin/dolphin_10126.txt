+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gigi_h
Photo URL    : https://www.flickr.com/photos/gigi-h/3365327694/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 14 23:50:34 GMT+0100 2009
Upload Date  : Wed Mar 18 11:57:10 GMT+0100 2009
Views        : 255
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC01988


+---------------+
|  DESCRIPTION  |
+---------------+
All the dolphins we swam with...


+--------+
|  TAGS  |
+--------+
africa mozambique underwater dolphin 