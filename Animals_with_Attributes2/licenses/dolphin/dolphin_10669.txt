+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Swamibu
Photo URL    : https://www.flickr.com/photos/swamibu/1582168787/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 29 15:12:14 GMT+0200 2005
Upload Date  : Tue Oct 16 03:19:04 GMT+0200 2007
Views        : 71,149
Comments     : 54


+---------+
|  TITLE  |
+---------+
Jumping free


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://bighugelabs.com/flickr/onblack.php?id=1582168787&amp;size=large" rel="nofollow">View large</a>
Dolphins playing games in Indonesia.

With more than 40 species, dolphins are mammals closely related to whales. They vary in size from 1.2 metres (4 ft) and 40 kilograms (88 lb) (Maui's Dolphin), up to 9.5 m (30 ft) and ten tonnes (the Orca or Killer Whale).

Most dolphins have acute eyesight, both in and out of the water, and their sense of hearing is superior to that of humans. Dolphins often leap above the water surface, sometimes performing acrobatic figures (e.g. the spinner dolphin).


+--------+
|  TAGS  |
+--------+
MyWinners ImpressedBeauty ABigFave WowieKazowie flickrdiamond DiamondClassPhotographer BlueRibbonWinner 