+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Graham Green10
Photo URL    : https://www.flickr.com/photos/grahamskatie/3783892839/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 10 06:49:39 GMT+0200 2009
Upload Date  : Mon Aug 3 11:28:33 GMT+0200 2009
Geotag Info  : Latitude:38.548903, Longitude:-0.090379
Views        : 10
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dolphins


+---------------+
|  DESCRIPTION  |
+---------------+
Mundar Spain


+--------+
|  TAGS  |
+--------+
Spain Benidorm 