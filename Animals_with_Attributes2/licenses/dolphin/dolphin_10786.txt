+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SparkyLeigh
Photo URL    : https://www.flickr.com/photos/sparkyleigh/8065882358/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 10 09:10:24 GMT+0200 2012
Upload Date  : Mon Oct 8 08:29:02 GMT+0200 2012
Views        : 12,028
Comments     : 18


+---------+
|  TITLE  |
+---------+
Spinner Dolphins offshore of the island of Hawaii


+---------------+
|  DESCRIPTION  |
+---------------+
A fun little video of these same spinner dolphins can be viewed on my YouTube site here (also see new link below): <a href="http://youtu.be/nq77rpm12KI" rel="nofollow">youtu.be/nq77rpm12KI</a> -Oh, and if you do take the time to watch the video be sure to turn your audio up and listen to all the communicating these dolphins do; especially when playing. 
<b>New dolphin link with music is <a href="http://youtu.be/o1U80eVSvMg" rel="nofollow">HERE</a></b>

Being in the open ocean with wild spinner dolphins is quite special. I've been doing it for decades and never tire of the thrill of it.

Below are some more still shots taken while recording the movie footage in the video linked above.

All shot with a little point &amp; shoot underwater camera.


+--------+
|  TAGS  |
+--------+
"wild dolphins Hawaii" "swimming with wild dolphins" 