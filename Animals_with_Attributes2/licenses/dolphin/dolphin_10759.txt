+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sata1706
Photo URL    : https://www.flickr.com/photos/44514402@N03/4099515830/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 7 12:39:05 GMT+0200 2009
Upload Date  : Fri Nov 13 00:49:08 GMT+0100 2009
Geotag Info  : Latitude:35.746512, Longitude:-5.976562
Views        : 621
Comments     : 0


+---------+
|  TITLE  |
+---------+
Salto Delfín


+---------------+
|  DESCRIPTION  |
+---------------+
Hecha en el zoo de Madrid


+--------+
|  TAGS  |
+--------+
Delfín 