+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ian Chapin
Photo URL    : https://www.flickr.com/photos/tagg_art_photography/10659973023/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 3 16:59:47 GMT+0100 2013
Upload Date  : Mon Nov 4 02:03:24 GMT+0100 2013
Views        : 693
Comments     : 1


+---------+
|  TITLE  |
+---------+
Playing with my Food


+---------------+
|  DESCRIPTION  |
+---------------+
Pacific Whitesided Dolphin playing with his treat by throwing it up and catching it over and over again in its mouth. Taken at Vancouver Aquarium, Vancouver, BC.


+--------+
|  TAGS  |
+--------+
"Pacific Whitesided Dolphin" "Vancouver Aquarium" Vancouver BC dolphin "marine mammal" "Ian Chapin" "copyright Ian Chapin" 