+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : GrahamAndDairne
Photo URL    : https://www.flickr.com/photos/r36ariadne/2302311619/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 1 21:18:43 GMT+0100 2008
Upload Date  : Sat Mar 1 22:18:43 GMT+0100 2008
Views        : 19
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dolphin


+---------------+
|  DESCRIPTION  |
+---------------+
Dolphin seen in Bristol Channel


+--------+
|  TAGS  |
+--------+
Wildlife 