+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aresauburn™
Photo URL    : https://www.flickr.com/photos/aresauburnphotos/2623431395/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 27 10:47:29 GMT+0200 2008
Upload Date  : Mon Jun 30 09:43:18 GMT+0200 2008
Views        : 7,873
Comments     : 1


+---------+
|  TITLE  |
+---------+
Dolphins


+---------------+
|  DESCRIPTION  |
+---------------+
Dolphins at Six Flags Discovery Kingdom


+--------+
|  TAGS  |
+--------+
"six flags" "marine world" "six flags discovery kingdom" marine park ca california lumix fz7 panasonic aresauburn animal bottlenose dolphins 