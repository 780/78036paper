+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : AlexandreRoux01
Photo URL    : https://www.flickr.com/photos/30142279@N07/16377473325/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jan 26 07:34:04 GMT+0100 2015
Upload Date  : Tue Jan 27 03:53:17 GMT+0100 2015
Views        : 313
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lagenorhynchus obscurus


+---------------+
|  DESCRIPTION  |
+---------------+
Dusky Dolphin in New Zealand


+--------+
|  TAGS  |
+--------+
dusky dolphin lagenorhynque obscur Lagenorhynchus obscurus 