+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : WhaleRiot
Photo URL    : https://www.flickr.com/photos/whaleriot/8650021599/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 14 13:52:56 GMT+0200 2013
Upload Date  : Mon Apr 15 05:25:53 GMT+0200 2013
Views        : 839
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dolphins


+---------------+
|  DESCRIPTION  |
+---------------+
Dolphins swimming with the Dana Wharf Whale Watching


+--------+
|  TAGS  |
+--------+
"Dana Wharf Whale Watching" "Dana Point" Dolphins 