+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jual
Photo URL    : https://www.flickr.com/photos/jual/14299769513/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 20 14:01:18 GMT+0200 2014
Upload Date  : Tue May 27 04:32:06 GMT+0200 2014
Views        : 596
Comments     : 3


+---------+
|  TITLE  |
+---------+
So long, and thanks for all the fish


+---------------+
|  DESCRIPTION  |
+---------------+
Tursiops truncatus. Mississippi Sound


+--------+
|  TAGS  |
+--------+
dolphin "bottlenose dolphin" cetacea sea blue water ocean mississippi "tursiops truncatus" tursiops delfin mar azul océano cetáceo 