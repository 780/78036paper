+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michele Dorsey Walfred
Photo URL    : https://www.flickr.com/photos/dorseymw/14951794670/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 3 06:35:39 GMT+0200 2014
Upload Date  : Thu Sep 4 17:08:05 GMT+0200 2014
Views        : 110
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dolphin treat! Sailing on the Kalmar Nyckel Tall Ship


+---------------+
|  DESCRIPTION  |
+---------------+
Lewes, Delaware


+--------+
|  TAGS  |
+--------+
dolphins 