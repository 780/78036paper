+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ZeMoufette
Photo URL    : https://www.flickr.com/photos/zemoufette/3975408041/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 2 19:47:14 GMT+0200 2009
Upload Date  : Sat Oct 3 04:47:14 GMT+0200 2009
Views        : 768
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dolphin


+---------------+
|  DESCRIPTION  |
+---------------+
A pacific white-sided dolphin.


+--------+
|  TAGS  |
+--------+
dolphin pacific white sided splash water zemoufette 