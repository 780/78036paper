+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lauren Tucker Photography
Photo URL    : https://www.flickr.com/photos/photographygal123/4779750561/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 5 16:09:20 GMT+0200 2010
Upload Date  : Sat Jul 10 18:12:06 GMT+0200 2010
Geotag Info  : Latitude:36.541488, Longitude:-4.623980
Views        : 10,796
Comments     : 5


+---------+
|  TITLE  |
+---------+
Dolphins Jumping Over Keeper


+---------------+
|  DESCRIPTION  |
+---------------+
Dolphins Jumping Over Keeper In Dolphin Show, Benalmadena


+--------+
|  TAGS  |
+--------+
Spain Fuengirola Costa Del Sol Espanol Selwo Marina Sea World Benalmadena Blue Dolphins Water Grey White Jump 