+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pmarkham
Photo URL    : https://www.flickr.com/photos/pmarkham/5753799474/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 18 02:56:31 GMT+0200 2011
Upload Date  : Tue May 24 06:37:09 GMT+0200 2011
Views        : 4,941
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dolphins in Pine Island Sound


+---------------+
|  DESCRIPTION  |
+---------------+
We went on a dolphin cruise with Captiva Cruises and saw several pods of dolphins.  Whenever we saw a group, the captain would speed up the boat and we were encouraged to make LOTS of noise to attract the dolphins.  Inevitably, one or two would come over and play in the wake of the boat.  They were so close to us and these were wild Atlantic Bottlenose Dolphins.  Very neat indeed!


+--------+
|  TAGS  |
+--------+
dolphins "Pine Island Sound" Captiva Sanibel FL USA 