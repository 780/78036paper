+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Expectmohr
Photo URL    : https://www.flickr.com/photos/julien-mohr/7187608510/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 7 10:17:22 GMT+0200 2012
Upload Date  : Sun May 13 13:12:07 GMT+0200 2012
Views        : 56
Comments     : 2


+---------+
|  TITLE  |
+---------+
Dolphins!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Zonnigzeilen.nl Turkije Turkey Griekenland Greece Hellas Dolfijn Dolphin 