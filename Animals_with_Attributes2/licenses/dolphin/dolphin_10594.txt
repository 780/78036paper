+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : loic.v
Photo URL    : https://www.flickr.com/photos/loicv/14358526031/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 27 13:06:28 GMT+0200 2014
Upload Date  : Fri Jun 6 23:53:31 GMT+0200 2014
Geotag Info  : Latitude:43.614275, Longitude:7.125009
Views        : 729
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_1731


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Marineland Antibes France "Côte d'Azur" parc spectacle show mammifère marin marine mammal Dauphin Dolphin 