+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SkyWhisperer
Photo URL    : https://www.flickr.com/photos/skywhisperer/3286647499/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Feb 5 16:54:09 GMT+0100 2009
Upload Date  : Tue Feb 17 09:34:28 GMT+0100 2009
Geotag Info  : Latitude:36.112682, Longitude:-115.177756
Views        : 89
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sigfried and Roy's Secret Garden


+---------------+
|  DESCRIPTION  |
+---------------+
Las Vegas Trip


+--------+
|  TAGS  |
+--------+
vegas dolphins 