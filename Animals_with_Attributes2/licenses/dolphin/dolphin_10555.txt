+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Gert-Jan Hanekamp
Photo URL    : https://www.flickr.com/photos/gjhanekamp/98765536/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 11 14:00:08 GMT+0200 2005
Upload Date  : Sun Feb 12 18:00:27 GMT+0100 2006
Views        : 412
Comments     : 2


+---------+
|  TITLE  |
+---------+
Greece, Dolphins


+---------------+
|  DESCRIPTION  |
+---------------+
Dolphins passing by while snorkling.
2005


+--------+
|  TAGS  |
+--------+
Greece Dolphins 