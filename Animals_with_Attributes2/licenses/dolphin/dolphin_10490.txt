+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nathangibbs
Photo URL    : https://www.flickr.com/photos/nathangibbs/6812470863/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 31 10:25:36 GMT+0100 2011
Upload Date  : Fri Feb 3 17:30:15 GMT+0100 2012
Geotag Info  : Latitude:26.035548, Longitude:-111.329715
Views        : 405
Comments     : 2


+---------+
|  TITLE  |
+---------+
Dolphins


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Canon EOS 7D" "Tamron 17-50mm f/2.8 VC" Loreto "Baja California Sur" México ocean bay dolphin Coronados Isla Coronado 