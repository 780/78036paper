+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/4965255586/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 4 15:10:40 GMT+0200 2010
Upload Date  : Mon Sep 6 22:32:54 GMT+0200 2010
Geotag Info  : Latitude:53.228196, Longitude:-2.889146
Views        : 869
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Lions


+--------+
|  TAGS  |
+--------+
"Chester Zoo" Lions 