+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : richardrichard
Photo URL    : https://www.flickr.com/photos/richardtoller/9487364187/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 24 09:16:20 GMT+0200 2013
Upload Date  : Sun Aug 11 23:33:44 GMT+0200 2013
Views        : 631
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_5421


+---------------+
|  DESCRIPTION  |
+---------------+
Lion cub, Serengeti


+--------+
|  TAGS  |
+--------+
"Lion cub" Serengeti 