+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : isfleming
Photo URL    : https://www.flickr.com/photos/fleming007/4835618709/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 28 13:00:06 GMT+0100 2009
Upload Date  : Wed Jul 28 02:09:56 GMT+0200 2010
Views        : 12,283
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion Portrait


+---------------+
|  DESCRIPTION  |
+---------------+
Lion at the Toronto Zoo


+--------+
|  TAGS  |
+--------+
Animal Lion Zoo Toronto 