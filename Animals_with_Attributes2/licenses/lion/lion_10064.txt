+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jdsmith1021
Photo URL    : https://www.flickr.com/photos/jdsmith1021/8022392301/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 22 10:33:28 GMT+0200 2012
Upload Date  : Tue Sep 25 08:28:16 GMT+0200 2012
Views        : 98
Comments     : 0


+---------+
|  TITLE  |
+---------+
African Lion


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
portland pdx oregon zoo animals washington park 2012 September 