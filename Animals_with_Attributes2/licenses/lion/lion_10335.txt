+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : m01229
Photo URL    : https://www.flickr.com/photos/39908901@N06/7578341248/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 4 14:09:47 GMT+0200 2012
Upload Date  : Mon Jul 16 00:53:14 GMT+0200 2012
Geotag Info  : Latitude:38.926293, Longitude:-77.048723
Views        : 2,486
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion at the National Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
lion "national zoo" dc "zoo animals" 