+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : leroy_regis
Photo URL    : https://www.flickr.com/photos/regilero/8727492851/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 11 13:23:52 GMT+0200 2013
Upload Date  : Sat May 11 13:25:04 GMT+0200 2013
Geotag Info  : Latitude:47.247629, Longitude:-1.198024
Views        : 382
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lionne


+---------------+
|  DESCRIPTION  |
+---------------+
Zoo boissière du dorée, France


+--------+
|  TAGS  |
+--------+
animal zoo free Lion Lionne 