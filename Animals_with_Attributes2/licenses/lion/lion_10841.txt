+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : theglobalpanorama
Photo URL    : https://www.flickr.com/photos/121483302@N02/13912139567/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 3 13:28:21 GMT+0200 2014
Upload Date  : Sat May 3 22:31:33 GMT+0200 2014
Views        : 1,207
Comments     : 0


+---------+
|  TITLE  |
+---------+
Male Lion


+---------------+
|  DESCRIPTION  |
+---------------+
Image Courtesy: Ltshears - Trisha M Shears, Released into the public domain | Wikimedia Commons


+--------+
|  TAGS  |
+--------+
globalpanorama tgp lion male leo lions Skull Malformations Captive Science 