+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Josh Friedman Luxury Travel
Photo URL    : https://www.flickr.com/photos/joshfriedmantravel/3273113709/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 7 00:29:39 GMT+0200 2008
Upload Date  : Thu Feb 12 07:34:45 GMT+0100 2009
Views        : 403
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Lion Botswana "stanley's camp" "chief's camp" "okavango delta" "abercrombie and kent" "sanctuary lodges" 