+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : abhinaba
Photo URL    : https://www.flickr.com/photos/abhinaba/4831853669/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 24 15:52:24 GMT+0200 2010
Upload Date  : Tue Jul 27 00:29:54 GMT+0200 2010
Geotag Info  : Latitude:47.669072, Longitude:-122.349736
Views        : 156
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)