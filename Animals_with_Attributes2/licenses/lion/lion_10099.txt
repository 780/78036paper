+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flowcomm
Photo URL    : https://www.flickr.com/photos/flowcomm/4871282144/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 30 16:11:46 GMT+0200 2010
Upload Date  : Sun Aug 8 09:55:30 GMT+0200 2010
Views        : 1,996
Comments     : 0


+---------+
|  TITLE  |
+---------+
Beautiful lion, Madikwe


+---------------+
|  DESCRIPTION  |
+---------------+
Madikwe Game Reserve, South Africa


+--------+
|  TAGS  |
+--------+
Madikwe "South Africa" Africa wildlife lion animal wild 