+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jsogo
Photo URL    : https://www.flickr.com/photos/jsogo/3853942390/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 20 09:20:05 GMT+0200 2009
Upload Date  : Mon Aug 24 23:25:41 GMT+0200 2009
Views        : 1,644
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion


+---------------+
|  DESCRIPTION  |
+---------------+
Ingala!


+--------+
|  TAGS  |
+--------+
"South Africa" Kruger safari lion nikon d5000 