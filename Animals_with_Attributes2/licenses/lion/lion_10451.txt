+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flowcomm
Photo URL    : https://www.flickr.com/photos/flowcomm/8951427369/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 1 08:29:19 GMT+0200 2013
Upload Date  : Tue Jun 4 22:21:26 GMT+0200 2013
Views        : 519
Comments     : 4


+---------+
|  TITLE  |
+---------+
Lion yawning


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wildlife Africa Madikwe "Madikwe Game Reserve" "South Africa" 