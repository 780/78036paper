+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : charlieishere@btinternet.com
Photo URL    : https://www.flickr.com/photos/100915417@N07/18639823176/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 8 09:30:13 GMT+0200 2015
Upload Date  : Wed Jun 10 09:00:45 GMT+0200 2015
Views        : 6,805
Comments     : 2


+---------+
|  TITLE  |
+---------+
You Can't Hide Your Lion Eyes


+---------------+
|  DESCRIPTION  |
+---------------+
One of the two Bristol lions deciding which of the small children banging on the glass would make the tastiest snack.


+--------+
|  TAGS  |
+--------+
"Bristol Zoo" lion eyes 