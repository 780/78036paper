+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Åsa Berndtsson
Photo URL    : https://www.flickr.com/photos/93892629@N07/12120967764/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 9 17:45:41 GMT+0100 2013
Upload Date  : Fri Jan 24 18:54:11 GMT+0100 2014
Views        : 516
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lejon / Lion


+---------------+
|  DESCRIPTION  |
+---------------+
Original file: IMG_2291.TIF-single-new


+--------+
|  TAGS  |
+--------+
Lejon Lion "Panthera leo" Sydafrika Entabeni 