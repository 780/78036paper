+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : indeliblemistakes
Photo URL    : https://www.flickr.com/photos/kriis__xx/7478183752/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 14 14:11:43 GMT+0200 2012
Upload Date  : Sun Jul 1 13:05:11 GMT+0200 2012
Views        : 204
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion


+---------------+
|  DESCRIPTION  |
+---------------+
@ Melbourne Zoo


+--------+
|  TAGS  |
+--------+
lion mane zoo "melbourne zoo" 