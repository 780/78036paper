+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : fortherock
Photo URL    : https://www.flickr.com/photos/fortherock/3897905893/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 2 09:47:12 GMT+0100 2008
Upload Date  : Tue Sep 8 02:20:59 GMT+0200 2009
Views        : 16,424
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lions & Lion Cubs


+---------------+
|  DESCRIPTION  |
+---------------+
Lion Camp; Lion Cubs San Diego Wild Animal Park Safari Park March 2008


+--------+
|  TAGS  |
+--------+
Baby Lions Cubs Wild Animal Park San Diego Safari 