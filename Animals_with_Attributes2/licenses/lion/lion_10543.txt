+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Photos_by_Angela
Photo URL    : https://www.flickr.com/photos/angelabethell/4036976980/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 23 14:16:21 GMT+0200 2009
Upload Date  : Fri Oct 23 10:58:18 GMT+0200 2009
Views        : 228
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion


+---------------+
|  DESCRIPTION  |
+---------------+
Paradise Valley Wildlife Park - Rotorua, NZ


+--------+
|  TAGS  |
+--------+
(no tags)