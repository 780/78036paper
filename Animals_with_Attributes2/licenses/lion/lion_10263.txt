+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tama Leaver
Photo URL    : https://www.flickr.com/photos/tamaleaver/6855165989/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 11 09:59:09 GMT+0100 2012
Upload Date  : Sat Feb 11 07:35:12 GMT+0100 2012
Views        : 520
Comments     : 0


+---------+
|  TITLE  |
+---------+
King of the Jungle


+---------------+
|  DESCRIPTION  |
+---------------+
Stirring in the morning sun ...


+--------+
|  TAGS  |
+--------+
perth zoo southperth 2012 lion 