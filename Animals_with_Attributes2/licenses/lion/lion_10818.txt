+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stevehdc
Photo URL    : https://www.flickr.com/photos/sherseydc/479094548/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 30 16:59:28 GMT+0200 2007
Upload Date  : Tue May 1 01:40:15 GMT+0200 2007
Views        : 16,026
Comments     : 100


+---------+
|  TITLE  |
+---------+
lounging lion


+---------------+
|  DESCRIPTION  |
+---------------+
soaking up the late afternoon sun at the national zoo in washington, dc.  i was debating whether or not i liked the stalks of grass on the right side of the image...the more i look at this the more i like it.  not sure why though...maybe for me it helps fill the frame or give balance to the right side.  thoughts?


+--------+
|  TAGS  |
+--------+
sherseydc lion national zoo Zoology AnAwesomeShot superhearts SuperbMasterpiece Specanimal 10+faves APlusPhoto favemegroup-4 hbw bokeh 