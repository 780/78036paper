+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : heath.windcliff
Photo URL    : https://www.flickr.com/photos/heath_windcliff/3434855239/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 28 11:23:46 GMT+0100 2007
Upload Date  : Sun Apr 12 21:40:07 GMT+0200 2009
Views        : 1,037
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sister lion cubs at play.


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Zimbabwe. lions" Africa 