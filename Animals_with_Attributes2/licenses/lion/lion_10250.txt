+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : RobBixbyPhotography
Photo URL    : https://www.flickr.com/photos/scubabix/4145236547/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 29 11:15:59 GMT+0100 2009
Upload Date  : Mon Nov 30 03:24:11 GMT+0100 2009
Geotag Info  : Latitude:30.405285, Longitude:-81.644210
Views        : 157
Comments     : 0


+---------+
|  TITLE  |
+---------+
JFT-Zoo_11-29-09-4184


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo jaxfieldtrips Jacksonville florida lion lioness 