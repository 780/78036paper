+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ScriS - www.scris.it
Photo URL    : https://www.flickr.com/photos/scris/2467558140/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 4 17:00:43 GMT+0200 2008
Upload Date  : Mon May 5 12:02:43 GMT+0200 2008
Views        : 525
Comments     : 2


+---------+
|  TITLE  |
+---------+
The Lion King


+---------------+
|  DESCRIPTION  |
+---------------+
Royalty free at <a href="http://www.shutterstock.com/gallery.mhtml?id=169495" rel="nofollow">Shutterstock</a>


+--------+
|  TAGS  |
+--------+
leone lion animal nature wild portrait 