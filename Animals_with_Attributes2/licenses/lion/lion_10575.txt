+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Heather van der Ven
Photo URL    : https://www.flickr.com/photos/heathervanderven/5861494038/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 20 13:55:52 GMT+0100 2006
Upload Date  : Wed Jun 22 23:28:41 GMT+0200 2011
Views        : 100
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion


+---------------+
|  DESCRIPTION  |
+---------------+
San Francisco zoo


+--------+
|  TAGS  |
+--------+
(no tags)