+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bobosh_t
Photo URL    : https://www.flickr.com/photos/frted/7095291459/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 19 14:59:18 GMT+0200 2012
Upload Date  : Fri Apr 20 06:09:21 GMT+0200 2012
Geotag Info  : Latitude:38.929736, Longitude:-77.051067
Views        : 97
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_0025


+---------------+
|  DESCRIPTION  |
+---------------+
Lion


+--------+
|  TAGS  |
+--------+
Washington DC zoo "National Zoo" "Smithsonian National Zoological Park" 