+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : foxrosser
Photo URL    : https://www.flickr.com/photos/foxrosser/908213874/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 2 13:36:14 GMT+0100 2007
Upload Date  : Thu Jul 26 21:48:19 GMT+0200 2007
Views        : 555
Comments     : 0


+---------+
|  TITLE  |
+---------+
hungry lion


+---------------+
|  DESCRIPTION  |
+---------------+
The collection of animal photos were taken back in March with my Nikon CoolPix 4300 point-n-shoot with a teleconverter lens (before I got my DSLR.)


+--------+
|  TAGS  |
+--------+
"ft worth" zoo lion tongue lick FlickrChallengeGroup 