+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ahisgett
Photo URL    : https://www.flickr.com/photos/hisgett/4872121421/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 5 13:58:39 GMT+0200 2010
Upload Date  : Sun Aug 8 20:14:18 GMT+0200 2010
Geotag Info  : Latitude:51.533929, Longitude:-0.152746
Views        : 1,073
Comments     : 1


+---------+
|  TITLE  |
+---------+
Asian Lion


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
London Zoo ZSL Big Cat Lion 