+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/9555518281/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 16 11:26:19 GMT+0200 2013
Upload Date  : Tue Aug 20 21:47:44 GMT+0200 2013
Geotag Info  : Latitude:51.772434, Longitude:-1.653571
Views        : 11,061
Comments     : 3


+---------+
|  TITLE  |
+---------+
Asiatic lion profile


+---------------+
|  DESCRIPTION  |
+---------------+
Profile portrait of the young male Asiatic lion of the Cotswold Wildlife Park.


+--------+
|  TAGS  |
+--------+
lion asiatic big wild cat young male.portrait face lying platform wood sunny profile cotswold "wildlife park" zoo england uk "great britain" d4 