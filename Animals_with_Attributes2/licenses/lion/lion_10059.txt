+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KWDesigns
Photo URL    : https://www.flickr.com/photos/kwdesigns/6854631683/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Feb 6 09:10:32 GMT+0100 2012
Upload Date  : Sat Feb 11 04:34:14 GMT+0100 2012
Views        : 737
Comments     : 0


+---------+
|  TITLE  |
+---------+
African Lion


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"African Lion" "Lion Country Safari" Florida "West Palm Beach" "Loxahatchee, Florida" www.kristinwallphotography.com 