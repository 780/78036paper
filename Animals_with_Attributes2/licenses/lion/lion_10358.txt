+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike's Birds
Photo URL    : https://www.flickr.com/photos/pazzani/6614426585/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 11 23:12:32 GMT+0100 2011
Upload Date  : Sun Jan 1 20:58:03 GMT+0100 2012
Geotag Info  : Latitude:-24.796187, Longitude:31.498266
Views        : 457
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion Cub


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)