+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Christoph Strässler
Photo URL    : https://www.flickr.com/photos/christoph_straessler/16867095690/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 17 07:06:03 GMT+0100 2009
Upload Date  : Mon Apr 6 15:50:57 GMT+0200 2015
Views        : 528
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion cub, Serengeti


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
16-9 Löwe Serengeti Tansania "Tansania 2009" 