+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : matt512
Photo URL    : https://www.flickr.com/photos/matt512/25294195300/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 5 17:48:25 GMT+0100 2016
Upload Date  : Mon Mar 7 17:30:33 GMT+0100 2016
Views        : 185
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lions


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
ZSL London Zoo Animals 