+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : magnus.johansson10
Photo URL    : https://www.flickr.com/photos/120374925@N06/14598306199/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 29 17:25:44 GMT+0200 2014
Upload Date  : Wed Jul 30 12:07:29 GMT+0200 2014
Views        : 3,749
Comments     : 0


+---------+
|  TITLE  |
+---------+
Female Lion at Parken Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animals lions nature tamron nikon 