+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : appenz
Photo URL    : https://www.flickr.com/photos/appenz/2434019643/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 9 01:44:25 GMT+0200 2005
Upload Date  : Tue Apr 22 21:23:21 GMT+0200 2008
Views        : 6,831
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lions in Serengeti


+---------------+
|  DESCRIPTION  |
+---------------+
Two Lions in the Serengeti National park, Tanzania.


+--------+
|  TAGS  |
+--------+
Lion Serengeti Tanzania Africa 