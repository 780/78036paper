+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nearsjasmine
Photo URL    : https://www.flickr.com/photos/133530809@N08/23718003353/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 1 23:53:11 GMT+0100 2016
Upload Date  : Wed Jan 13 02:28:36 GMT+0100 2016
Views        : 794
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion Yawning/ Roaring


+---------------+
|  DESCRIPTION  |
+---------------+
We watched the lion sleep for awhile. Suddenly he rose and you could see his strength. With scars over his body, He yawned, which looks like roar. It was beautiful.  


<a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="nofollow"></a>This work by Jasmine Nears is licensed under a <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="nofollow">Creative Commons Attribution-ShareAlike 4.0 International License</a>.


+--------+
|  TAGS  |
+--------+
"South Africa" "South African Safari" andBeyond "Andbeyond Exeter River Lodge" "Big Five Game" Animals "Sabi Sands" "Private Game Reserve" "Kruger National Park" "near Kruger National Park" "Safari Drive" Art "Big Game" "Little 5" "Big 5" "Beautiful Animal" "Creative Commons" Sharealike Lion "Lion Male" "Male Lion" "Lion Roar" "Lion Yawn" "Lion sitting" "Lion sitting up" "Lion with mouth open" "Focused Lion" "Lion in Bush" "Bush Lion" "Lion Sleep" "Safari Lion" "Big Game Lion" "Brown Lion" "Yellow Lion" 