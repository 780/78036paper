+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SnapAdik
Photo URL    : https://www.flickr.com/photos/hubertyu/192422384/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 13 14:17:05 GMT+0200 2006
Upload Date  : Tue Jul 18 09:16:57 GMT+0200 2006
Geotag Info  : Latitude:36.102957, Longitude:-115.170809
Views        : 119
Comments     : 0


+---------+
|  TITLE  |
+---------+
li•on•ess


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
lion lioness lions cats bigcats 