+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ColinsCamera
Photo URL    : https://www.flickr.com/photos/colinscamera/5795052568/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 31 13:48:48 GMT+0200 2011
Upload Date  : Sat Jun 4 00:57:31 GMT+0200 2011
Views        : 85
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dozing Lion


+---------------+
|  DESCRIPTION  |
+---------------+
Jungle Park, Tenerife


+--------+
|  TAGS  |
+--------+
(no tags)