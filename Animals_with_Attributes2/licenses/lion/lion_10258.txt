+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michel Ethève
Photo URL    : https://www.flickr.com/photos/cocowood-productions/3003341282/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 14 10:16:15 GMT+0200 2008
Upload Date  : Tue Nov 4 18:38:15 GMT+0100 2008
Views        : 952
Comments     : 0


+---------+
|  TITLE  |
+---------+
King Leo


+---------------+
|  DESCRIPTION  |
+---------------+
Lion de l'Atlas, parc de la Tête d'Or, Lyon.


+--------+
|  TAGS  |
+--------+
Lion félin Lyon 