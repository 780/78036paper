+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael Gwyther-Jones
Photo URL    : https://www.flickr.com/photos/12587661@N06/8455886545/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 5 13:36:01 GMT+0100 2012
Upload Date  : Fri Feb 8 21:37:44 GMT+0100 2013
Views        : 1,104
Comments     : 0


+---------+
|  TITLE  |
+---------+
Singapore, Zoo, & Bird Park December 2012 499


+---------------+
|  DESCRIPTION  |
+---------------+
Singapore Zoo


+--------+
|  TAGS  |
+--------+
Lion 