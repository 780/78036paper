+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flowcomm
Photo URL    : https://www.flickr.com/photos/flowcomm/12592486863/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 25 07:32:33 GMT+0100 2014
Upload Date  : Mon Feb 17 17:45:18 GMT+0100 2014
Views        : 152
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Madikwe nature safari "Madikwe Game Reserve" 