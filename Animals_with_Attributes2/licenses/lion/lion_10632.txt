+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : leroy_regis
Photo URL    : https://www.flickr.com/photos/regilero/8728612810/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 11 13:24:05 GMT+0200 2013
Upload Date  : Sat May 11 13:25:03 GMT+0200 2013
Geotag Info  : Latitude:47.247629, Longitude:-1.198024
Views        : 1,674
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion


+---------------+
|  DESCRIPTION  |
+---------------+
Zoo boissière du dorée, France


+--------+
|  TAGS  |
+--------+
animal zoo free lion 