+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Anup Shah
Photo URL    : https://www.flickr.com/photos/anupshah/18651171250/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 14 10:48:17 GMT+0200 2015
Upload Date  : Mon Jun 15 21:23:26 GMT+0200 2015
Views        : 676
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lion


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Big Cat" Lion "Whipsnade Zoo" Wildlife 