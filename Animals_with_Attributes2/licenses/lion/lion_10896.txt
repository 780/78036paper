+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blmurch
Photo URL    : https://www.flickr.com/photos/blmurch/146001072/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 13 07:42:31 GMT+0200 2006
Upload Date  : Sun May 14 09:35:32 GMT+0200 2006
Views        : 1,362
Comments     : 0


+---------+
|  TITLE  |
+---------+
Proud lions


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Big Cat" "US Road Trip" "Exotic Feline Resuce Center" Indiana Brazil "Scavenger Hunt" Lion Lioness 