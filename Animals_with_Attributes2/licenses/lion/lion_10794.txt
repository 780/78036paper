+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 20, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jagermo
Photo URL    : https://www.flickr.com/photos/jagermo/11066862386/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Nov 21 16:51:51 GMT+0100 2013
Upload Date  : Tue Nov 26 14:12:46 GMT+0100 2013
Views        : 243
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lions


+---------------+
|  DESCRIPTION  |
+---------------+
They are just big cats. Really.


+--------+
|  TAGS  |
+--------+
"South Africa" "Big Five Game" 