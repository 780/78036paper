+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KaseyEriksen
Photo URL    : https://www.flickr.com/photos/kaseyeriksen/16987274357/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 15 12:42:06 GMT+0200 2001
Upload Date  : Sun Apr 19 05:43:53 GMT+0200 2015
Views        : 1,651
Comments     : 0


+---------+
|  TITLE  |
+---------+
Napping hamster


+---------------+
|  DESCRIPTION  |
+---------------+
...and a happy little mommy.


+--------+
|  TAGS  |
+--------+
hammy hamster girl pet animal friend happy fun silly play playful child sleep Lana Lanabanana Lanabananafitness 