+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Infomastern
Photo URL    : https://www.flickr.com/photos/infomastern/22481438530/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 12 10:56:18 GMT+0200 2015
Upload Date  : Sun Nov 1 12:01:54 GMT+0100 2015
Views        : 165
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hamster


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Danmark Denmark Fugleparken Graested animal djur hamster "exif:model=canon eos 760d" geo:country= "exif:focal_length=300 mm" "exif:lens=ef70-300mm f/4-5.6l is usm" geo:city= "camera:model=canon eos 760d" geo:state= geo:location= exif:iso_speed=1600 "exif:aperture=ƒ / 5,6" camera:make=canon exif:make=canon 