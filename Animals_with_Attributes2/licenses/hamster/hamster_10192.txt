+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cliff1066™
Photo URL    : https://www.flickr.com/photos/nostri-imago/2919844325/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 19 01:10:00 GMT+0100 2004
Upload Date  : Tue Oct 7 04:03:23 GMT+0200 2008
Geotag Info  : Latitude:38.846487, Longitude:-77.075036
Views        : 536
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dwarf Siberian Hamster - Percy


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Dwarf Siberian" Hamster Cricetinae Pet Rodent 