+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Haundreis
Photo URL    : https://www.flickr.com/photos/haundreis/2760350708/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 7 09:42:33 GMT+0200 2008
Upload Date  : Wed Aug 13 18:05:14 GMT+0200 2008
Geotag Info  : Latitude:14.392263, Longitude:121.037771
Views        : 21,435
Comments     : 9


+---------+
|  TITLE  |
+---------+
Hamster Fun


+---------------+
|  DESCRIPTION  |
+---------------+
Cute hamster resting after a try at the wheel


+--------+
|  TAGS  |
+--------+
hamster philippines animals animal pinoy pinay interesting filipino funny asia cute manila 