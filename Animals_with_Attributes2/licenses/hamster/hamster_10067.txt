+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Maarten Dirkse
Photo URL    : https://www.flickr.com/photos/superchango/4376877445/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 21 01:36:33 GMT+0100 2010
Upload Date  : Mon Feb 22 02:27:15 GMT+0100 2010
Views        : 1,520
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bebop de hamster


+---------------+
|  DESCRIPTION  |
+---------------+
Bebop, de hamster.


+--------+
|  TAGS  |
+--------+
hamster bebop "jazz pet" 