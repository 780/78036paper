+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : vincentchow*
Photo URL    : https://www.flickr.com/photos/_vincent/2777041707/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 19 16:58:21 GMT+0200 2008
Upload Date  : Tue Aug 19 12:12:34 GMT+0200 2008
Geotag Info  : Latitude:3.087897, Longitude:101.737602
Views        : 3,227
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sleeping Hamsters


+---------------+
|  DESCRIPTION  |
+---------------+
Sleeping together in a fine afternoon.


+--------+
|  TAGS  |
+--------+
hamsters sleep 