+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : schnappischnap
Photo URL    : https://www.flickr.com/photos/schnappischnap/8978095584/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 14 12:45:57 GMT+0100 2012
Upload Date  : Fri Jun 7 15:20:14 GMT+0200 2013
Geotag Info  : Latitude:52.612012, Longitude:-1.112655
Views        : 641
Comments     : 0


+---------+
|  TITLE  |
+---------+
Jeremiah Peabody II


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Hamster Hamsters Dwarf Cute "Dwarf Hamsters" "Dwarf Hamster" Pets Pet 