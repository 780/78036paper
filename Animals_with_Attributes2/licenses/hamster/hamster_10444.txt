+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jpockele
Photo URL    : https://www.flickr.com/photos/jpockele/280848425/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 27 23:17:38 GMT+0200 2006
Upload Date  : Fri Oct 27 23:17:38 GMT+0200 2006
Geotag Info  : Latitude:51.279555, Longitude:4.442338
Views        : 644
Comments     : 2


+---------+
|  TITLE  |
+---------+
Look, no hands!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
knibbel russian dwarf hamster cute 