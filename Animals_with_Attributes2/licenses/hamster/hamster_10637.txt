+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : -JvL-
Photo URL    : https://www.flickr.com/photos/-jvl-/3141901558/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 8 23:23:40 GMT+0200 2004
Upload Date  : Sat Dec 27 19:19:06 GMT+0100 2008
Views        : 90
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sjimmie


+---------------+
|  DESCRIPTION  |
+---------------+
Sjimmie feasting on a leaf of lettuce


+--------+
|  TAGS  |
+--------+
Hamster Pets 