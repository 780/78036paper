+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rkimpeljr
Photo URL    : https://www.flickr.com/photos/rkimpeljr/319191758/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 25 10:46:21 GMT+0100 2006
Upload Date  : Mon Dec 11 04:17:20 GMT+0100 2006
Views        : 147
Comments     : 0


+---------+
|  TITLE  |
+---------+
P1080645


+---------------+
|  DESCRIPTION  |
+---------------+
Harvey likes cantaloupe.  YUM!


+--------+
|  TAGS  |
+--------+
Harvey hamster cantaloupe 