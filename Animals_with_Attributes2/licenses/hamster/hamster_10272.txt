+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lyn Lomasi
Photo URL    : https://www.flickr.com/photos/lynlomasi/7782739602/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 28 07:05:22 GMT+0200 2011
Upload Date  : Tue Aug 14 20:23:36 GMT+0200 2012
Views        : 409
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hamster running on her wheel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)