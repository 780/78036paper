+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cliff1066™
Photo URL    : https://www.flickr.com/photos/nostri-imago/2919963523/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 19 14:17:33 GMT+0100 2008
Upload Date  : Tue Oct 7 04:48:33 GMT+0200 2008
Geotag Info  : Latitude:38.846462, Longitude:-77.075036
Views        : 336
Comments     : 0


+---------+
|  TITLE  |
+---------+
Nikita


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Winter White Russian Dwarf" Hamster "Phodopus Sungorus" Cricetinae Pet Rodent 