+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : captainmcdan
Photo URL    : https://www.flickr.com/photos/captainmcdan/4264105010/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 10 14:43:30 GMT+0100 2010
Upload Date  : Sun Jan 10 23:23:22 GMT+0100 2010
Views        : 868
Comments     : 1


+---------+
|  TITLE  |
+---------+
Hamsters


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2010 Hamsters "dwarf hamsters" "campbells russian dwarf hamsters" "Sigma 17-70mm f2.8-4.5 DC MACRO" "Canon Speedlite 430EXII" January 