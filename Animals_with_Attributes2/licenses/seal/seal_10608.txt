+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Chulabush
Photo URL    : https://www.flickr.com/photos/119460142@N04/13501950584/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 24 14:40:13 GMT+0100 2014
Upload Date  : Sun Mar 30 05:27:54 GMT+0200 2014
Views        : 92
Comments     : 0


+---------+
|  TITLE  |
+---------+
Northern Elephant Seal (Mirounga angustirostris)


+---------------+
|  DESCRIPTION  |
+---------------+
At Año Nuevo State Park, California, USA.


+--------+
|  TAGS  |
+--------+
"taxonomy:binomial=Mirounga angustirostris" 