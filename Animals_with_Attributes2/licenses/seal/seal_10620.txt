+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ruth and Dave
Photo URL    : https://www.flickr.com/photos/ruthanddave/2821689842/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 1 14:43:24 GMT+0200 2008
Upload Date  : Tue Sep 2 15:48:06 GMT+0200 2008
Views        : 245
Comments     : 0


+---------+
|  TITLE  |
+---------+
Nose in the air


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
seal "harbour seal" snooty aquarium "vancouver aquarium" whiskers msh1108-3 msh1108 msh1009-14 msh1009 