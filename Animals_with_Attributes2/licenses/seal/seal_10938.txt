+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rusty Clark - On the Air M-F 8am-noon
Photo URL    : https://www.flickr.com/photos/rusty_clark/6046352009/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 10 11:50:04 GMT+0200 2011
Upload Date  : Mon Aug 15 21:16:21 GMT+0200 2011
Views        : 61
Comments     : 0


+---------+
|  TITLE  |
+---------+
Thirty Year Old Harbor Seal - Mystic Aquarium - Mystic CT


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)