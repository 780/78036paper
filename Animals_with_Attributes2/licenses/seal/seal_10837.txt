+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MollySVH
Photo URL    : https://www.flickr.com/photos/18472724@N00/23306976652/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 29 09:08:47 GMT+0100 2015
Upload Date  : Mon Nov 30 07:59:45 GMT+0100 2015
Views        : 32
Comments     : 0


+---------+
|  TITLE  |
+---------+
Seals at Poipu Beach


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)