+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Robin Wendler
Photo URL    : https://www.flickr.com/photos/jprvw/8812322606/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 16 09:41:58 GMT+0200 2013
Upload Date  : Fri May 24 14:03:20 GMT+0200 2013
Views        : 306
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_5539-sharp


+---------------+
|  DESCRIPTION  |
+---------------+
Hawaiian Monk Seal at Po'ipu Beach, Kauai


+--------+
|  TAGS  |
+--------+
Kauai 2013 "taxonomy:binomial=Monachus schauinslandi" 