+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ed Townend
Photo URL    : https://www.flickr.com/photos/townendphotography/3011522956/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 19 10:37:43 GMT+0200 2008
Upload Date  : Fri Nov 7 23:18:35 GMT+0100 2008
Views        : 160
Comments     : 0


+---------+
|  TITLE  |
+---------+
Seal


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Scarborough seal seals water nature animals sealife 