+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : csakkarin
Photo URL    : https://www.flickr.com/photos/csakkarin/16431093057/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 20 11:51:50 GMT+0100 2015
Upload Date  : Tue Feb 24 23:30:24 GMT+0100 2015
Views        : 219
Comments     : 0


+---------+
|  TITLE  |
+---------+
Posing Seal


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Seal Scottish SEALIFE Sanctuary Samsung NX1 Oban 