+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mark Waters
Photo URL    : https://www.flickr.com/photos/markwaters/520257038/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 27 12:28:15 GMT+0200 2007
Upload Date  : Tue May 29 20:21:16 GMT+0200 2007
Views        : 2,897
Comments     : 0


+---------+
|  TITLE  |
+---------+
Seal, Howth, Dublin, Ireland


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
seal howth dublin ireland 