+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : James Patrick Casey
Photo URL    : https://www.flickr.com/photos/jamespcasey/11421894215/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 12 18:25:22 GMT+0100 2013
Upload Date  : Tue Dec 17 18:15:20 GMT+0100 2013
Views        : 318
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Seals "Sea Lions" "San Francisco" California 