+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ScriS - www.scris.it
Photo URL    : https://www.flickr.com/photos/scris/15637196259/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 15 12:16:49 GMT+0100 2014
Upload Date  : Wed Nov 19 00:55:47 GMT+0100 2014
Geotag Info  : Latitude:53.471393, Longitude:0.158786
Views        : 1,855
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gray Seal at Donna Nook


+---------------+
|  DESCRIPTION  |
+---------------+
Gray Seal with pup at Donna Nook


+--------+
|  TAGS  |
+--------+
seal gray "donna nook" pup animal wildlife uk england sea mammals newborn 