+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Five Furlongs
Photo URL    : https://www.flickr.com/photos/fivefurlongs/7821017792/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 15 00:10:55 GMT+0200 2012
Upload Date  : Mon Aug 20 06:37:47 GMT+0200 2012
Views        : 591
Comments     : 0


+---------+
|  TITLE  |
+---------+
Seal


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
bermuda zoo animal seal 