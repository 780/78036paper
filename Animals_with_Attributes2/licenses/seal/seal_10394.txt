+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : JKD Atlanta
Photo URL    : https://www.flickr.com/photos/jkdatlanta/5165104211/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 7 16:06:22 GMT+0100 2010
Upload Date  : Thu Nov 11 02:24:53 GMT+0100 2010
Views        : 177
Comments     : 0


+---------+
|  TITLE  |
+---------+
grey seal sitting up


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Chicago November 2010 