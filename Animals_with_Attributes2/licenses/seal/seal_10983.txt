+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : 1000zen
Photo URL    : https://www.flickr.com/photos/1000zen/6568132945/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 24 13:53:18 GMT+0100 2011
Upload Date  : Sun Dec 25 10:08:27 GMT+0100 2011
Geotag Info  : Latitude:53.300928, Longitude:5.098085
Views        : 5,766
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zeehond baby


+---------------+
|  DESCRIPTION  |
+---------------+
New born grey seal on the beach of Vlieland, the Netherlands. Day before Christmas 2011
<a href="http://en.wikipedia.org/wiki/Grey_seal" rel="nofollow">en.wikipedia.org/wiki/Grey_seal</a>
The babies are born with a white fur coat, The live on the beach for the first two weeks, fed by extremely fat mother milk. In the third week they are abandoned and will have to go into the water to hunt for fish.


+--------+
|  TAGS  |
+--------+
"grey seal" baby seal 