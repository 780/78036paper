+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sebastien.faillon
Photo URL    : https://www.flickr.com/photos/128090976@N08/15745214808/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 16 13:35:34 GMT+0200 2014
Upload Date  : Tue Dec 2 20:58:56 GMT+0100 2014
Geotag Info  : Latitude:50.584926, Longitude:3.884267
Views        : 200
Comments     : 0


+---------+
|  TITLE  |
+---------+
Phoque


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Pairi Daiza Nature Animaux Belgique Phoque Seal 