+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : xeno_sapien
Photo URL    : https://www.flickr.com/photos/gilad_rom/7339356800/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 4 15:04:43 GMT+0200 2012
Upload Date  : Tue Jun 5 01:02:51 GMT+0200 2012
Views        : 1,274
Comments     : 0


+---------+
|  TITLE  |
+---------+
Just Chillin


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
leopard seal resting iceberg antarctica mammal cute sleep 