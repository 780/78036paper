+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ingrid Taylar
Photo URL    : https://www.flickr.com/photos/taylar/12609321115/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 8 14:42:40 GMT+0100 2014
Upload Date  : Tue Feb 18 10:48:33 GMT+0100 2014
Views        : 3,001
Comments     : 3


+---------+
|  TITLE  |
+---------+
Masked


+---------------+
|  DESCRIPTION  |
+---------------+
Harbor Seal hauled out at Pacific Grove, California.

<i>Photographed at a distance, from the shoreline bluff trail. No Marine Mammal Protection Act violations. :-) </i>


+--------+
|  TAGS  |
+--------+
"harbor seal" "pacific grove" monterey bay pacific ocean seals pinnipeds "marine mammals" mask "face markings" california february 2014 "northern california" coast "hauled out" "haul out" rocks kelp resting olympus om-d e-m1 zuiko 50-200mm 