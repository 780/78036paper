+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SD Dirk
Photo URL    : https://www.flickr.com/photos/dirkhansen/6104655825/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 1 14:19:11 GMT+0200 2011
Upload Date  : Fri Sep 2 05:23:30 GMT+0200 2011
Views        : 40
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant Seals near San Simeon


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)