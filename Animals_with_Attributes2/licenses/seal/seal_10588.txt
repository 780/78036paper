+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stuart001uk
Photo URL    : https://www.flickr.com/photos/stuart001uk/451183974/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 20 12:09:39 GMT+0200 2004
Upload Date  : Sun Apr 8 22:22:25 GMT+0200 2007
Geotag Info  : Latitude:50.730221, Longitude:1.593918
Views        : 111
Comments     : 0


+---------+
|  TITLE  |
+---------+
Nausicaa Seal


+---------------+
|  DESCRIPTION  |
+---------------+
Seal at the Nausicaa national aquarium in Boulogne, France.

<a href="http://www.nausicaa.fr/Anglais/accueil.htm" rel="nofollow">www.nausicaa.fr/Anglais/accueil.htm</a>


+--------+
|  TAGS  |
+--------+
2004 france seaworld Animals 