+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ethan Ableman
Photo URL    : https://www.flickr.com/photos/ethansphotosarecool/5816087277/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 6 18:35:33 GMT+0200 2010
Upload Date  : Fri Jun 10 00:51:23 GMT+0200 2011
Views        : 84
Comments     : 0


+---------+
|  TITLE  |
+---------+
Harbor Seal


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)