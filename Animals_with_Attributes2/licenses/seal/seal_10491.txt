+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jamie_riden
Photo URL    : https://www.flickr.com/photos/jamie_riden/287966181/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 6 15:48:15 GMT+0100 2006
Upload Date  : Fri Nov 3 22:26:28 GMT+0100 2006
Views        : 204
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_6838


+---------------+
|  DESCRIPTION  |
+---------------+
Seal, Caitlins, NZ


+--------+
|  TAGS  |
+--------+
seal caitlins "new zealand" 