+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : dbrooksNY
Photo URL    : https://www.flickr.com/photos/dpriddy/212676519/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 2 17:11:23 GMT+0200 2006
Upload Date  : Fri Aug 11 21:20:08 GMT+0200 2006
Views        : 118
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_3145


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
elephant seal california coast molting sand sunning huge ocean summer august 2006 nikon d50 vacation "bigger than you think" 