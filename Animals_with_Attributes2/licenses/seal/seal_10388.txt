+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : spencer77
Photo URL    : https://www.flickr.com/photos/spencer77/4957096148/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Feb 23 14:49:03 GMT+0100 2009
Upload Date  : Sat Sep 4 15:45:02 GMT+0200 2010
Geotag Info  : Latitude:52.746580, Longitude:1.669578
Views        : 748
Comments     : 1


+---------+
|  TITLE  |
+---------+
Grey Seal (Halichoeurus Grypus), Horsey Beach


+---------------+
|  DESCRIPTION  |
+---------------+
This picture is available to use for free, under the creative commons licence. All I ask is that I'm given a photo credit &amp; a courtesy email to let me know how it's being used.


+--------+
|  TAGS  |
+--------+
Horsey Seal norfolk beach mammal animal Seals "halichoerus grypus" sony A350 alpha 