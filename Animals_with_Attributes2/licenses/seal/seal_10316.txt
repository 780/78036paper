+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : donjd2
Photo URL    : https://www.flickr.com/photos/ddebold/8656803265/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 13 14:33:53 GMT+0200 2013
Upload Date  : Wed Apr 17 12:51:54 GMT+0200 2013
Views        : 491
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant Seals


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"En Plein Air" "california central coast" "elephant seal" "photo workshop" 