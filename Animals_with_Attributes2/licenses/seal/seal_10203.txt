+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : GanMed64
Photo URL    : https://www.flickr.com/photos/ganmed64/15424072722/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 2 21:01:23 GMT+0200 2014
Upload Date  : Fri Oct 3 03:43:35 GMT+0200 2014
Views        : 175
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant Seal pup, Años Nuevos State Park, California


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)