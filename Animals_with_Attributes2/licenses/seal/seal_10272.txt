+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ed Bierman
Photo URL    : https://www.flickr.com/photos/edbierman/5561653774/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 13 09:07:56 GMT+0100 2011
Upload Date  : Sat Mar 26 17:53:02 GMT+0100 2011
Views        : 86
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant Seals on Beach


+---------------+
|  DESCRIPTION  |
+---------------+
Northern Elephant Seal (M. angustirostris)


+--------+
|  TAGS  |
+--------+
"Ed Bierman" "San Simeon" california cambria history ocean 