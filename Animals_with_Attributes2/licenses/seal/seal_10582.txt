+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : _e.t
Photo URL    : https://www.flickr.com/photos/45688285@N00/146711874/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 14 10:00:22 GMT+0200 2006
Upload Date  : Mon May 15 07:24:43 GMT+0200 2006
Views        : 78
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mother's Day: Pescadero/Hwy. 1


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Pescadero "Highway One" "elephant seals" "Año Nuevo" 