+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : wewerlacy
Photo URL    : https://www.flickr.com/photos/jimlacy/6406507757/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 25 10:48:01 GMT+0200 2007
Upload Date  : Sat Nov 26 19:38:10 GMT+0100 2011
Views        : 150
Comments     : 0


+---------+
|  TITLE  |
+---------+
harbor seal 2, Monterey Bay


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Jim Lacy Travel 