+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kevincole
Photo URL    : https://www.flickr.com/photos/kevcole/3190240418/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 10 08:05:57 GMT+0100 2009
Upload Date  : Mon Jan 12 03:52:29 GMT+0100 2009
Views        : 2,080
Comments     : 3


+---------+
|  TITLE  |
+---------+
January 10, 2009 AWN DPW at Piedras Blancas Elephant Seal Rookery


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Elephant Seal" Female Weaner Pup kevincole kevinlcole "Piedras Blancas Rookery" California 209-33361 USA 