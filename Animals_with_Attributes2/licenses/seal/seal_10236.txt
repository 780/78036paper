+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jphilipg
Photo URL    : https://www.flickr.com/photos/15708236@N07/16234257349/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 20 18:44:28 GMT+0200 2014
Upload Date  : Sun Feb 1 19:10:45 GMT+0100 2015
Views        : 203
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant Seal


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Elephant Seal" "Éléphant de mer" Californie California 