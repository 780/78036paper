+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mark Koester
Photo URL    : https://www.flickr.com/photos/markwkoester/15255971812/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 17 14:58:21 GMT+0200 2014
Upload Date  : Tue Sep 16 11:49:43 GMT+0200 2014
Views        : 33
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wild seal at Discovery Park, Seattle, WA - 11


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)