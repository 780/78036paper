+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Christina Spicuzza
Photo URL    : https://www.flickr.com/photos/spicuzza/4706102475/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 4 12:20:35 GMT+0200 2010
Upload Date  : Wed Jun 16 18:28:53 GMT+0200 2010
Geotag Info  : Latitude:35.662051, Longitude:-121.256001
Views        : 159
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_4957_1


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"elephant seal" coast coastal fight Pacific ocean wildlife seal California PCH 