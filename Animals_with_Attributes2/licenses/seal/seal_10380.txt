+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael R Perry
Photo URL    : https://www.flickr.com/photos/michaelrperry/4244294619/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 26 09:41:28 GMT+0100 2009
Upload Date  : Mon Jan 4 16:53:36 GMT+0100 2010
Views        : 3,097
Comments     : 0


+---------+
|  TITLE  |
+---------+
Galapagos Islands Sea Lion Pup


+---------------+
|  DESCRIPTION  |
+---------------+
Bahia Fe is a spot known to lobster fishermen, rarely visited by tourists - except that other boat that arrived for lunch just after us...  More Marine Iguanas, Galapgos Sea Lions and schools of exotic fish to swim with.  A mangrove stand to explore and lunch of fish and peanut butter tamlales.  Great day!


+--------+
|  TAGS  |
+--------+
Galapagos Iguana Equador wildlife seal tortoise giant "sea lion" volcano lava "sierra negra" "San Cristobal" "Puerto Ayara" "Michael Perry" "Cynthia Perry" "ROW Adventures" Ecuador Darwin "Charles Darwin" evolution "sea lion pup" "baby sea lion" 