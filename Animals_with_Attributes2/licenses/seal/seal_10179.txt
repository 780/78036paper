+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Photographing Travis
Photo URL    : https://www.flickr.com/photos/photographingtravis/24528504911/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 23 07:37:04 GMT+0100 2016
Upload Date  : Mon Jan 25 21:23:11 GMT+0100 2016
Geotag Info  : Latitude:37.114488, Longitude:-122.326492
Views        : 1,244
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant Seal Adventure


+---------------+
|  DESCRIPTION  |
+---------------+
Ano Nuevo State Park, Pescadero, California


+--------+
|  TAGS  |
+--------+
animals "ano nuevo" beach creatures "elephant seal" "half moon bay" nature park pescadero 