+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : U. S. Fish and Wildlife Service - Northeast Region
Photo URL    : https://www.flickr.com/photos/usfwsnortheast/5961874532/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 21 14:34:46 GMT+0200 2011
Upload Date  : Thu Jul 21 20:34:46 GMT+0200 2011
Geotag Info  : Latitude:41.385978, Longitude:-70.039858
Views        : 1,520
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gray seal


+---------------+
|  DESCRIPTION  |
+---------------+
Gray seal at Nantucket National Wildlife Refuge in Massachusetts.

Credit: Amanda Boyd / USFWS


+--------+
|  TAGS  |
+--------+
nantucket "national wildlife refuge" refuge national usfws "fish and wildlife service" mammal seal seals "gray seal" "harbor seal" island ocean beach massachusetts ma conservation habitat 