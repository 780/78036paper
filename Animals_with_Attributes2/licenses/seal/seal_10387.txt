+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Charles & Clint
Photo URL    : https://www.flickr.com/photos/dad_and_clint/6397598587/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 23 09:55:25 GMT+0100 2011
Upload Date  : Fri Nov 25 03:06:24 GMT+0100 2011
Geotag Info  : Latitude:52.969577, Longitude:0.955166
Views        : 2,305
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gray Seals Mating @ Blakeney Point, England 23 Nov 11


+---------------+
|  DESCRIPTION  |
+---------------+
The grey seal (Halichoerus grypus, meaning &quot;hooked-nosed sea pig&quot;) is found on both shores of the North Atlantic Ocean. It is a large seal of the family Phocidae or &quot;true seals&quot;.  It is also known as Atlantic grey seal and the horsehead seal.  It is a large seal, with the bulls reaching 2.5–3.3 m (8.2–11 ft) long and weighing 170–310 kg (370–680 lb); the cows are much smaller, typically 1.6–2.0 m (5.2–6.6 ft) long and 100–190 kg (220–420 lb) in weight. The grey seal feeds on a wide variety of fish, mostly benthic or demersal species, taken at depths down to 70 m (230 ft) or more. Sand eels (Ammodytes spp) are important in its diet in many localities. Cod and other gadids, flatfish, herring[6] and skates[7] are also important locally. However, it is clear that the grey seal will eat whatever is available, including octopus[8] and lobsters.[9] The average daily food requirement is estimated to be 5 kg (11 lb), though the seal does not feed every day and it fasts during the breeding season.


+--------+
|  TAGS  |
+--------+
"Seal Watch" 