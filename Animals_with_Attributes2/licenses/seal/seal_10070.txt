+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : wolfpix
Photo URL    : https://www.flickr.com/photos/wolfraven/5212873654/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 26 13:03:15 GMT+0100 2010
Upload Date  : Sat Nov 27 23:45:03 GMT+0100 2010
Geotag Info  : Latitude:37.116919, Longitude:-122.316563
Views        : 741
Comments     : 12


+---------+
|  TITLE  |
+---------+
Bandit-faced fighter


+---------------+
|  DESCRIPTION  |
+---------------+
Young male elephant seal
<i>Mirounga augustirostris</i>
Año Nuevo State Reserve
San Mateo, California


+--------+
|  TAGS  |
+--------+
seal "elephant seal" "Mirounga augustirostris" mammal "marine mammal" phoque 密封 těsnění zeehond selyo seehund חותם segel focas "海 豹" foca 