+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stuart001uk
Photo URL    : https://www.flickr.com/photos/stuart001uk/1288093208/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 22 11:36:10 GMT+0200 2007
Upload Date  : Fri Aug 31 21:38:18 GMT+0200 2007
Geotag Info  : Latitude:50.625549, Longitude:-2.444404
Views        : 45
Comments     : 0


+---------+
|  TITLE  |
+---------+
Common Seal 2


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://www.sealifeeurope.com/local/index.php?loc=weymouth" rel="nofollow">www.sealifeeurope.com/local/index.php?loc=weymouth</a>

<a href="http://www.uksafari.com/commonseals.htm" rel="nofollow">www.uksafari.com/commonseals.htm</a>

<a href="http://en.wikipedia.org/wiki/Common_Seal" rel="nofollow">en.wikipedia.org/wiki/Common_Seal</a>


+--------+
|  TAGS  |
+--------+
2007 Vacation dorset seaworld Animals 