+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : thirteenthbat
Photo URL    : https://www.flickr.com/photos/awarmplace/5063243664/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Oct 5 14:29:27 GMT+0200 2010
Upload Date  : Fri Oct 8 21:29:14 GMT+0200 2010
Views        : 89
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dead Seal @ Heceta Head


+---------------+
|  DESCRIPTION  |
+---------------+
Found on the beach, might be Devil's Elbow.


+--------+
|  TAGS  |
+--------+
(no tags)