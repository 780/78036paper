+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mjambon
Photo URL    : https://www.flickr.com/photos/mjambon/6854815939/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 10 15:17:24 GMT+0100 2012
Upload Date  : Sat Feb 11 05:32:28 GMT+0100 2012
Views        : 55
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant seals


+---------------+
|  DESCRIPTION  |
+---------------+
Año Nuevo State Park, California.


+--------+
|  TAGS  |
+--------+
(no tags)