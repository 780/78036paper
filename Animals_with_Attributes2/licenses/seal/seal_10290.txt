+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sryffel
Photo URL    : https://www.flickr.com/photos/sryffel/3425809843/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 14 16:49:00 GMT+0100 2009
Upload Date  : Thu Apr 9 15:20:38 GMT+0200 2009
Views        : 335
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant Seals


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
USA "Elephant Seals" Favorites 