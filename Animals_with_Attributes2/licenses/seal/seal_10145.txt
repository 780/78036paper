+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Darren J Glanville
Photo URL    : https://www.flickr.com/photos/darren_j_glanville/6577283261/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 26 13:08:21 GMT+0100 2011
Upload Date  : Mon Dec 26 22:28:04 GMT+0100 2011
Geotag Info  : Latitude:52.719762, Longitude:1.699233
Views        : 126
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_2812


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Sea Beach Norfolk Animal Seal Wildlife Young Pup 