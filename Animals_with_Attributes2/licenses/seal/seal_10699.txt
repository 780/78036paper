+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : alberth2
Photo URL    : https://www.flickr.com/photos/alberth2/2236408519/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Feb 28 14:08:48 GMT+0100 2007
Upload Date  : Sat Feb 2 18:25:57 GMT+0100 2008
Geotag Info  : Latitude:-45.862998, Longitude:170.741271
Views        : 197
Comments     : 0


+---------+
|  TITLE  |
+---------+
Seal @ Otago Peninsula


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Otago Peninsula" Seal 