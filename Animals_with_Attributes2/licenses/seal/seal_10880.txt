+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cordyph
Photo URL    : https://www.flickr.com/photos/cordyph/1356398507/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 9 12:39:02 GMT+0200 2007
Upload Date  : Tue Sep 11 01:30:50 GMT+0200 2007
Views        : 290
Comments     : 0


+---------+
|  TITLE  |
+---------+
Seehunde vor der Brandung


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Helgoländer Düne" Seehunde Robben "common seals" "taxonomy:binomial=Phoca vitulina" taxonomy:genus=Phoca "taxonomy:common=Harbour Seal" 