+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nickboos
Photo URL    : https://www.flickr.com/photos/subpop77/1024609361/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 30 16:59:51 GMT+0200 2007
Upload Date  : Mon Aug 6 07:33:14 GMT+0200 2007
Views        : 123
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant Seal


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Big Sur" California 2007 "Elephant Seal" 