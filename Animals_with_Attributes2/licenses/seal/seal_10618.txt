+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : vonlohmann
Photo URL    : https://www.flickr.com/photos/vonlohmann/11635361284/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 28 10:35:31 GMT+0100 2013
Upload Date  : Mon Dec 30 00:59:51 GMT+0100 2013
Geotag Info  : Latitude:37.114328, Longitude:-122.328828
Views        : 288
Comments     : 0


+---------+
|  TITLE  |
+---------+
Northern elephant seal, Año Nuevo State Park, California


+---------------+
|  DESCRIPTION  |
+---------------+
female and pup


+--------+
|  TAGS  |
+--------+
(no tags)