+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rdeetz
Photo URL    : https://www.flickr.com/photos/rdeetz/8478544/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 20 14:14:39 GMT+0100 2004
Upload Date  : Tue Apr 5 04:00:40 GMT+0200 2005
Views        : 97
Comments     : 0


+---------+
|  TITLE  |
+---------+
Seal


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
california travel animals 