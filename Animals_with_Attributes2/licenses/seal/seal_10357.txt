+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Martin Cooper Ipswich
Photo URL    : https://www.flickr.com/photos/m-a-r-t-i-n/16433918658/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 14 12:11:06 GMT+0100 2015
Upload Date  : Mon Feb 23 08:22:40 GMT+0100 2015
Geotag Info  : Latitude:52.752455, Longitude:1.662150
Views        : 3,688
Comments     : 2


+---------+
|  TITLE  |
+---------+
Four seals


+---------------+
|  DESCRIPTION  |
+---------------+
Four of the hundreds of seals resting on Horsey Beach on Valentine's Day this year.


+--------+
|  TAGS  |
+--------+
Seals mammals Halichoerus grypus Phocidae British Norfolk Horsey beach gap 