+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tess Milligan
Photo URL    : https://www.flickr.com/photos/tessmilligan/7195932442/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 14 14:30:43 GMT+0200 2012
Upload Date  : Mon May 14 15:31:46 GMT+0200 2012
Views        : 278
Comments     : 0


+---------+
|  TITLE  |
+---------+
Canada Seals


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Canada seals 