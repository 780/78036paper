+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neil DeMaster
Photo URL    : https://www.flickr.com/photos/84169650@N07/16411562547/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 15 15:07:39 GMT+0200 2014
Upload Date  : Mon Feb 23 02:59:24 GMT+0100 2015
Views        : 88
Comments     : 0


+---------+
|  TITLE  |
+---------+
Northern Elephant Seal (1)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"marine mammal" mammal pinniped "northern elephant seal" "elephant seal" seal 