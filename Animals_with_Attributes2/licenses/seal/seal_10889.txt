+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : minkymonkeymoo
Photo URL    : https://www.flickr.com/photos/minkymonkeymoo/6642326595/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 26 16:47:10 GMT+0100 2011
Upload Date  : Thu Jan 5 19:10:38 GMT+0100 2012
Views        : 75
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sleeping calf seal, Donna Nook


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)