+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Allie_Caulfield
Photo URL    : https://www.flickr.com/photos/wm_archiv/2718337102/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 29 13:58:35 GMT+0200 2007
Upload Date  : Thu Jul 31 01:22:16 GMT+0200 2008
Geotag Info  : Latitude:53.544521, Longitude:8.570322
Views        : 279
Comments     : 0


+---------+
|  TITLE  |
+---------+
2007-03-29 Bremerhaven 061


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Bremerhaven Bremerhafen Bremen März 2007 Frühling Hafen "Neuer Hafen" Weser Zoo "Zoo am Meer" Themenzoo Unterweser Tierpark seal Robbe Seehund "Phoca vitulinaKegelrobbe" Foto photo image picture Bild "creative commons" flickr "high resolution" stockphoto free cc 