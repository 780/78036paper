+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mamamusings
Photo URL    : https://www.flickr.com/photos/liz/2961105544/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 20 19:01:41 GMT+0200 2008
Upload Date  : Tue Oct 21 08:58:53 GMT+0200 2008
Views        : 948
Comments     : 0


+---------+
|  TITLE  |
+---------+
Harbor Seals Arguing


+---------------+
|  DESCRIPTION  |
+---------------+
They were very loud, and very aggressive. Quite intimidating.


+--------+
|  TAGS  |
+--------+
monterey california october 2008 il08 seal seals harbor water bay 