+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eliduke
Photo URL    : https://www.flickr.com/photos/elisfanclub/3402779328/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 11 15:26:03 GMT+0100 2009
Upload Date  : Wed Mar 11 22:26:03 GMT+0100 2009
Views        : 236
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kauai: Shipwrecks Beach


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
hawaii kauai shipwrecks monk seals 