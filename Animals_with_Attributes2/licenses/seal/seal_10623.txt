+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : madprime
Photo URL    : https://www.flickr.com/photos/madprime/6314863796/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 31 10:05:44 GMT+0100 2011
Upload Date  : Sat Nov 5 13:04:13 GMT+0100 2011
Views        : 600
Comments     : 0


+---------+
|  TITLE  |
+---------+
Nursing seal


+---------------+
|  DESCRIPTION  |
+---------------+
I think these are California sea lions. Their barking was a bit of nostalgia, like the sea lions we've seen in downtown La Jolla.


+--------+
|  TAGS  |
+--------+
nursing seal berlin zoo 