+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Samuel Mann
Photo URL    : https://www.flickr.com/photos/21218849@N03/5651641449/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 23 10:49:22 GMT+0200 2011
Upload Date  : Mon Apr 25 04:38:02 GMT+0200 2011
Geotag Info  : Latitude:-45.392663, Longitude:170.866584
Views        : 78
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moeraki seal


+---------------+
|  DESCRIPTION  |
+---------------+
Moeraki Easter 2011


+--------+
|  TAGS  |
+--------+
(no tags)