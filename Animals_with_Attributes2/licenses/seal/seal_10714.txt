+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jonlclark
Photo URL    : https://www.flickr.com/photos/jon-clark/6269439902/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 29 02:54:43 GMT+0200 2011
Upload Date  : Sat Oct 22 16:53:48 GMT+0200 2011
Views        : 732
Comments     : 0


+---------+
|  TITLE  |
+---------+
Seal Beach, La Jolla


+---------------+
|  DESCRIPTION  |
+---------------+
The seals at the La Jolla Children's Pool - swim at your own risk ;)


+--------+
|  TAGS  |
+--------+
"la jolla" seal seals "seal beach" beach "childrens pool" california ca 