+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : fabian.kron
Photo URL    : https://www.flickr.com/photos/mrdoctor/24212226131/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 14 20:07:42 GMT+0200 2015
Upload Date  : Mon Jan 11 14:33:27 GMT+0100 2016
Views        : 9
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elefantes Marinhos


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"San Simeon" Elefantes Marinhos "Elephant Seals" "Pacific Coast" "Highway 1" 