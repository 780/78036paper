+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USFWS Pacific Southwest Region
Photo URL    : https://www.flickr.com/photos/usfws_pacificsw/14534371298/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 11 14:50:33 GMT+0100 2012
Upload Date  : Wed Jul 23 01:25:51 GMT+0200 2014
Views        : 397
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant seal at San Nicolas Island


+---------------+
|  DESCRIPTION  |
+---------------+
Photo by Robert McMorran/USFWS.


+--------+
|  TAGS  |
+--------+
California "Marine Mammals" "San Nicolas Island" "Ventura County" USFWS conservation "wildlife viewing" "wildlife biologist" "southern California" 