+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : polandeze
Photo URL    : https://www.flickr.com/photos/polandeze/463285395/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 11 06:55:58 GMT+0200 2007
Upload Date  : Tue Apr 17 23:31:04 GMT+0200 2007
Views        : 1,026
Comments     : 0


+---------+
|  TITLE  |
+---------+
young seal


+---------------+
|  DESCRIPTION  |
+---------------+
This seal is about 8 months old. It seemed unwell.


+--------+
|  TAGS  |
+--------+
seal "seal pup" injured Penwith Cornwall rescue 