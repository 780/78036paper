+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pmarkham
Photo URL    : https://www.flickr.com/photos/pmarkham/6729776643/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 15 03:38:56 GMT+0100 2012
Upload Date  : Fri Jan 20 09:04:43 GMT+0100 2012
Views        : 1,312
Comments     : 1


+---------+
|  TITLE  |
+---------+
Endangered Hawaiian Monk Seal


+---------------+
|  DESCRIPTION  |
+---------------+
Not far from our hotel is a beach called &quot;Secret Beach&quot; in the community of Ko Olina on the island of Oahu.  Three days now when we've walked that beach during the day we've found a 300+ pound Hawaiian Monk Seal sleeping either on the rocks or on the sand of the beach.  I learned from a naturalist that was there and from the signs that this species of seal is endangered and is believed to number less than 1000.  They only exist in the the Hawaiian Islands.  Very cool animal.  It reminded me of one of my dogs sleeping on the beach.


+--------+
|  TAGS  |
+--------+
"Hawaiian Monk Seal" Seal "Endangered Species" "Ko Olina" HI USA 