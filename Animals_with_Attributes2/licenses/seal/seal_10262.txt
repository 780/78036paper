+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USFWS Pacific Southwest Region
Photo URL    : https://www.flickr.com/photos/usfws_pacificsw/15339997556/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Sep 25 16:07:34 GMT+0200 2014
Upload Date  : Fri Sep 26 23:02:57 GMT+0200 2014
Views        : 349
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant seals in San Luis Obispo County


+---------------+
|  DESCRIPTION  |
+---------------+
Photo by Ashley Spratt/USFWS


+--------+
|  TAGS  |
+--------+
(no tags)