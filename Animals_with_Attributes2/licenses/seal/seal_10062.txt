+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : CJ Isherwood
Photo URL    : https://www.flickr.com/photos/isherwoodchris/3251493186/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 15 20:03:05 GMT+0100 2009
Upload Date  : Tue Feb 3 21:09:55 GMT+0100 2009
Geotag Info  : Latitude:35.274774, Longitude:-120.877075
Views        : 1,070
Comments     : 0


+---------+
|  TITLE  |
+---------+
Baby Elephant Seals


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
baby elephant seals seal mom mum wild sand beach pacific ocean 