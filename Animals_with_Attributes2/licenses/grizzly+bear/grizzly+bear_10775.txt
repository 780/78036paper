+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Efraimstochter
Photo URL    : https://pixabay.com/en/bear-brown-bear-grizzly-424382/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : Aug. 21, 2014
Uploaded     : Aug. 23, 2014

+--------+
|  TAGS  |
+--------+
Bear, Brown Bear, Grizzly, Grizzly Bear, Animal, Zoo