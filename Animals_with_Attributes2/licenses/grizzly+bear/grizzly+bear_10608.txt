+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sky#walker
Photo URL    : https://www.flickr.com/photos/59683764@N00/4056860276/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 19 16:18:10 GMT+0200 2009
Upload Date  : Sat Nov 7 13:19:09 GMT+0100 2009
Geotag Info  : Latitude:52.167456, Longitude:9.589004
Views        : 708
Comments     : 1


+---------+
|  TITLE  |
+---------+
No stress, I rather take a seat


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://darckr.com/photo?photoid=4056860276" rel="nofollow">large on black</a>, <a href="http://darckr.com/photo?photoid=4056860276&amp;css=white" rel="nofollow">white</a>, <a href="http://darckr.com/username?username=59683764@N00" rel="nofollow">stream on black</a>, <a href="http://darckr.com/username?username=59683764@N00&amp;css=white" rel="nofollow">white</a>, <a href="http://darckr.com/username?username=59683764@N00&amp;sort=interestingness-desc" rel="nofollow">interestingness</a>, <a href="http://darckr.com/username?username=59683764@N00&amp;css=white&amp;sort=interestingness-desc" rel="nofollow">white</a> (generated by <a href="http://darckr.com/" rel="nofollow">[darckr]</a>)


+--------+
|  TAGS  |
+--------+
2009 Saupark Springe Wisentgehege "Ursus arctos" "brown bear" Braunbär dateposted:1256853412 