+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lwpkommunikacio
Photo URL    : https://www.flickr.com/photos/lwpkommunikacio/17774280380/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 13 10:49:31 GMT+0200 2014
Upload Date  : Fri May 22 11:24:48 GMT+0200 2015
Views        : 2,031
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zoltán, a Farkasember


+---------------+
|  DESCRIPTION  |
+---------------+
Brumi, a mix of Kamchatka grizzly and the brown bear.


+--------+
|  TAGS  |
+--------+
zoltan_a_farkasember 