+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michelle Simpson Photos
Photo URL    : https://www.flickr.com/photos/shelfie/9452794268/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 13 18:01:00 GMT+0200 2013
Upload Date  : Tue Aug 6 17:17:13 GMT+0200 2013
Views        : 479
Comments     : 1


+---------+
|  TITLE  |
+---------+
Bear Portrait


+---------------+
|  DESCRIPTION  |
+---------------+
Another from my trip to Katmai.


+--------+
|  TAGS  |
+--------+
alaska bear katmai national park grizzly brown water 