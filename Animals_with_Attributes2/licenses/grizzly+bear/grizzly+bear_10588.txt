+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cletch
Photo URL    : https://www.flickr.com/photos/cletch/7984162693/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 9 16:52:11 GMT+0200 2012
Upload Date  : Fri Sep 14 03:10:53 GMT+0200 2012
Views        : 231
Comments     : 0


+---------+
|  TITLE  |
+---------+
Montana Grizzly Encounter


+---------------+
|  DESCRIPTION  |
+---------------+
Jake &amp; Maggi


+--------+
|  TAGS  |
+--------+
Montana "Montana Grizzly Encounter" "grizzly bear" grizzlies "bear rescue center" 