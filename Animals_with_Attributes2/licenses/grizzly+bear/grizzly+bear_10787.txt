+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Loryn
Photo URL    : https://pixabay.com/en/brown-bear-bear-animal-forest-fur-545777/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : Aug. 30, 2013
Uploaded     : Nov. 26, 2014

+--------+
|  TAGS  |
+--------+
Brown Bear, Bear, Animal, Forest, Fur, Grizzly