+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : brandnewday
Photo URL    : https://pixabay.com/en/grizzly-bear-brown-bear-zoo-animal-719873/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Uploaded     : April 13, 2015

+--------+
|  TAGS  |
+--------+
Grizzly Bear, Brown Bear, Zoo, Animal, Wildlife, Mammal