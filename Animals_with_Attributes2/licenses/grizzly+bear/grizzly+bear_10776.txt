+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : madisonbuening
Photo URL    : https://pixabay.com/en/zoo-grizzly-bear-grizzly-bear-751652/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : April 9, 2015
Uploaded     : May 4, 2015

+--------+
|  TAGS  |
+--------+
Zoo, Grizzly Bear, Grizzly, Bear, Animal, Brown