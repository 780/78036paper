+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : justinbaeder
Photo URL    : https://www.flickr.com/photos/justinbaeder/187836486/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 11 14:43:20 GMT+0200 2006
Upload Date  : Wed Jul 12 06:52:04 GMT+0200 2006
Views        : 561
Comments     : 1


+---------+
|  TITLE  |
+---------+
Grizzly


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo woodlandparkzoo woodlandpark seattle grizzly bear 