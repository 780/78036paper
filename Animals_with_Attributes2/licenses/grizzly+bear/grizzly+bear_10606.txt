+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Becker1999
Photo URL    : https://www.flickr.com/photos/becker271/5082605408/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 14 19:52:09 GMT+0200 2010
Upload Date  : Fri Oct 15 01:52:09 GMT+0200 2010
Views        : 68
Comments     : 0


+---------+
|  TITLE  |
+---------+
new digs


+---------------+
|  DESCRIPTION  |
+---------------+
Columbus Zoo (Fall 2010)


+--------+
|  TAGS  |
+--------+
"paul's pic" 2010 columbus zoo animals ohio brown bear 