+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike's Birds
Photo URL    : https://www.flickr.com/photos/pazzani/20173010170/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 6 16:59:18 GMT+0200 2015
Upload Date  : Fri Aug 7 05:45:11 GMT+0200 2015
Geotag Info  : Latitude:60.351324, Longitude:-152.903555
Views        : 163
Comments     : 0


+---------+
|  TITLE  |
+---------+
Brown Bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)