+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : calestyo
Photo URL    : https://www.flickr.com/photos/calestyo/6022772981/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 6 13:28:31 GMT+0200 2011
Upload Date  : Mon Aug 8 22:09:02 GMT+0200 2011
Geotag Info  : Latitude:48.098194, Longitude:11.552494
Views        : 1,484
Comments     : 0


+---------+
|  TITLE  |
+---------+
P8068687


+---------------+
|  DESCRIPTION  |
+---------------+
An <a href="http://en.wikipedia.org/wiki/Eurasian_Brown_Bear" rel="nofollow">Eurasian brown bear</a> (<a href="http://species.wikimedia.org/wiki/Ursus_arctos_arctos" rel="nofollow">Ursus arctos arctos</a>; also known as European brown bear or common brown bear).


+--------+
|  TAGS  |
+--------+
Animalia "Ursus arctos arctos" "Eurasian brown bear" "European brown bear" "common brown bear" 