+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike's Birds
Photo URL    : https://www.flickr.com/photos/pazzani/20366632901/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 6 16:52:56 GMT+0200 2015
Upload Date  : Fri Aug 7 05:16:13 GMT+0200 2015
Geotag Info  : Latitude:60.351324, Longitude:-152.903555
Views        : 202
Comments     : 0


+---------+
|  TITLE  |
+---------+
Brown Bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)