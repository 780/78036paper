+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : LoveToTakePhotos
Photo URL    : https://pixabay.com/en/alaska-fishing-bear-unaware-water-561160/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Uploaded     : Dec. 9, 2014

+--------+
|  TAGS  |
+--------+
Alaska, Fishing, Bear, Unaware, Water, Wild, Wildlife