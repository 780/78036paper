+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : dancingnomad3
Photo URL    : https://www.flickr.com/photos/dancingnomad3/7322176796/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 22 10:55:45 GMT+0200 2012
Upload Date  : Sat Jun 2 21:13:13 GMT+0200 2012
Views        : 372
Comments     : 0


+---------+
|  TITLE  |
+---------+
Denali National Park


+---------------+
|  DESCRIPTION  |
+---------------+
Grizzly and Springers (cubs that were born this past winter)


+--------+
|  TAGS  |
+--------+
Alaska Denali "National Park" 