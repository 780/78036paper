+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sach1tb
Photo URL    : https://www.flickr.com/photos/sach1tb/5918575448/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 3 13:43:26 GMT+0200 2011
Upload Date  : Sat Jul 9 15:04:50 GMT+0200 2011
Geotag Info  : Latitude:60.821986, Longitude:-148.982423
Views        : 274
Comments     : 0


+---------+
|  TITLE  |
+---------+
brown bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animals 