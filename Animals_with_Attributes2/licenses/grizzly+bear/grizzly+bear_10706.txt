+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Joynt Inspirations
Photo URL    : https://www.flickr.com/photos/musicalgenius/9207791655/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 4 11:18:12 GMT+0200 2013
Upload Date  : Thu Jul 4 19:18:19 GMT+0200 2013
Views        : 1,240
Comments     : 1


+---------+
|  TITLE  |
+---------+
Ursus Arctos Horribilis


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2013 Canon 50D Adobe Lightroom5 Lightroom Calgary Zoo "Calgary Zoo" Grizzly Bear "Grizzly Bear" Alberta Canada Nature Wildlife Green Brown Contrast Rock Rocks Beautiful Natural Wet "Wet Fur" "Ursus Arctos" "Ursus Arctos Horribilis" Animal 70-200 70-200mm "Canon 70-200" 200mm 