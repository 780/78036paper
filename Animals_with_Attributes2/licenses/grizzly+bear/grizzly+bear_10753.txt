+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : wschwisow
Photo URL    : https://pixabay.com/en/grizzly-bear-mammal-yellowstone-386340/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : May 3, 2013
Uploaded     : July 9, 2014

+--------+
|  TAGS  |
+--------+
Grizzly, Bear, Mammal, Yellowstone, Animal