+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/17205802399/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 14 14:34:40 GMT+0100 2015
Upload Date  : Wed May 6 18:00:08 GMT+0200 2015
Geotag Info  : Latitude:46.690347, Longitude:6.340430
Views        : 2,555
Comments     : 0


+---------+
|  TITLE  |
+---------+
Brown bear walking in the snow


+---------------+
|  DESCRIPTION  |
+---------------+
Last picture: one of the bears (I don't know his name), walking in the quite high snow...


+--------+
|  TAGS  |
+--------+
walking portrait profile careful bear "brown bear" "european bear" brown snow winter cold juraparc vallorbe switzerland nikon d4 