+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rob Hurson
Photo URL    : https://www.flickr.com/photos/robhurson/18694935258/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 7 00:13:25 GMT+0200 2015
Upload Date  : Wed Jun 17 09:03:09 GMT+0200 2015
Geotag Info  : Latitude:49.382177, Longitude:-123.079170
Views        : 3,279
Comments     : 4


+---------+
|  TITLE  |
+---------+
Just a little yawn (Explored -just a little- 20150617)


+---------------+
|  DESCRIPTION  |
+---------------+
This is either Coola or Grinder, one of the two grizzlies up on Grouse Mountain.


+--------+
|  TAGS  |
+--------+
Vancouver Canada summer mountain sunny "Pentax K-30" "Pentax 50-300mm f/4-5.8" "Grouse Mountain" "British Columbia" Grinder Coola "North Shore mountains" teeth "grizzly bear" 