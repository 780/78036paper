+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mliu92
Photo URL    : https://www.flickr.com/photos/mliu92/3320104367/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 1 11:22:35 GMT+0100 2009
Upload Date  : Mon Mar 2 00:23:48 GMT+0100 2009
Geotag Info  : Latitude:32.736885, Longitude:-117.148933
Views        : 113
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lip Chew 0891


+---------------+
|  DESCRIPTION  |
+---------------+
I'm not sure if he was mugging for the camera or just slack-jawed.


+--------+
|  TAGS  |
+--------+
"san diego" zoo blackie mongolian brown bear 