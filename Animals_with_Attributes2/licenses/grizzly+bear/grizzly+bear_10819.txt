+--------------------------------------+
| PublicDomainPictures Photo Metadata  |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Lilla Frerichs
Photo URL    : http://www.publicdomainpictures.net/view-image.php?image=81299
License      : public domain (https://creativecommons.org/publicdomain/zero/1.0/)
