+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jerseygal2009
Photo URL    : https://www.flickr.com/photos/jerseygal2009/14630312285/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 1 16:11:08 GMT+0200 2014
Upload Date  : Fri Jul 11 23:04:04 GMT+0200 2014
Geotag Info  : Latitude:45.666065, Longitude:-110.834970
Views        : 389
Comments     : 0


+---------+
|  TITLE  |
+---------+
Montana Grizzly Encounter 14


+---------------+
|  DESCRIPTION  |
+---------------+
Grizzly Bear Rescue &amp; Education Sanctuary in Bozeman, Montana


+--------+
|  TAGS  |
+--------+
"Grizzly bear" Montana Bozeman "Casey Anderson" Brutus Jake Maggie Sheena "brown bear" 