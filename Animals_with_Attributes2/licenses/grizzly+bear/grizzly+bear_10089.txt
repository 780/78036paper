+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Allie_Caulfield
Photo URL    : https://www.flickr.com/photos/wm_archiv/2730563859/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 8 14:06:09 GMT+0100 2003
Upload Date  : Mon Aug 4 08:50:19 GMT+0200 2008
Geotag Info  : Latitude:48.099120, Longitude:11.552896
Views        : 120
Comments     : 0


+---------+
|  TITLE  |
+---------+
2003-03 München 006


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
München Bayern Munich 2003 März Zoo Tierpark Hellabrunn Braunbär Bär brown bear Foto photo image picture Bild "creative commons" flickr "high resolution" stockphoto free cc 