+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/7719280108/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 26 18:04:36 GMT+0200 2012
Upload Date  : Sun Aug 5 21:00:42 GMT+0200 2012
Geotag Info  : Latitude:46.934821, Longitude:7.449455
Views        : 2,603
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bear lying and licking his paw


+---------------+
|  DESCRIPTION  |
+---------------+
I think he had honey oder something yummy, at least he was licking his paw and forearm for a certain time...


+--------+
|  TAGS  |
+--------+
bear brown lying relaxed licking paw forearm dählhölzli zoo bern switzerland nikon d700 