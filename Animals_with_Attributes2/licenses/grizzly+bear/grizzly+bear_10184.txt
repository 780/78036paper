+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : spencer77
Photo URL    : https://www.flickr.com/photos/spencer77/7074482311/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 12 16:51:18 GMT+0200 2012
Upload Date  : Fri Apr 13 21:22:41 GMT+0200 2012
Geotag Info  : Latitude:51.859531, Longitude:-0.530605
Views        : 832
Comments     : 0


+---------+
|  TITLE  |
+---------+
Brown Bear (Ursos arctos), ZSL Whipsnade Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
This picture is available to use for free, under the creative commons licence. All I ask is that I'm given a photo credit &amp; a courtesy email to let me know how it's being used.


+--------+
|  TAGS  |
+--------+
Whipsnade zoo ZSL Bedfordshire Dunstable animal mammal bear grizzly brown omnivore 