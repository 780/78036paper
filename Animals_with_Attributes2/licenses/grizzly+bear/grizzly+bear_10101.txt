+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MSFP
Photo URL    : https://www.flickr.com/photos/13907555@N04/2046325092/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 15 11:33:59 GMT+0200 2007
Upload Date  : Mon Nov 19 06:33:44 GMT+0100 2007
Views        : 407
Comments     : 0


+---------+
|  TITLE  |
+---------+
070815-4504


+---------------+
|  DESCRIPTION  |
+---------------+
Grizzly bear, Fish Creek, Alaska


+--------+
|  TAGS  |
+--------+
www.murrayfeist.com "Murray Feist" "Moose Jaw" wildlife nature natural environment Saskatchewan Canada western fauna avian landscape Grizzly Salmon Hyder Alaska "Fish Creek" bear 