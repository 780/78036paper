+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Carl Chapman
Photo URL    : https://commons.wikimedia.org/wiki/File:Bear_Alaska_(3).jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution 2.0 Generic (//creativecommons.org/licenses/by/2.0/deed.en)
Date         : 2006-06-24 06:55
