+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : skeeze
Photo URL    : https://pixabay.com/en/grizzly-bear-wildlife-nature-wild-869223/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : June 7, 2005
Uploaded     : 11 months ago

+--------+
|  TAGS  |
+--------+
Grizzly Bear, Wildlife, Nature, Wild, Carnivore