+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/7462281032/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 29 14:40:36 GMT+0200 2012
Upload Date  : Thu Jun 28 21:00:05 GMT+0200 2012
Geotag Info  : Latitude:52.702030, Longitude:10.262775
Views        : 6,229
Comments     : 4


+---------+
|  TITLE  |
+---------+
Bear in the tire


+---------------+
|  DESCRIPTION  |
+---------------+
This brown bear of the Filmtierpark was really comfortable in this big hanging tire!


+--------+
|  TAGS  |
+--------+
brown bear mammal sitting posing tire big filmtierpark park eschede germany nikon d700 