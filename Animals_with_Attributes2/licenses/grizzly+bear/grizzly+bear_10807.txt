+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Steve Hillebrand, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/bears/grizzly-bear-cubs
License      : public domain (CC0)
Uploaded     : 2015-01-05 10:42:49

+--------+
|  TAGS  |
+--------+
grizzly, bear, cubs