+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : solyanka
Photo URL    : https://www.flickr.com/photos/solyanka/2846202704/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 7 15:14:36 GMT+0200 2008
Upload Date  : Wed Sep 10 17:55:18 GMT+0200 2008
Views        : 242
Comments     : 0


+---------+
|  TITLE  |
+---------+
Grizzly Bear


+---------------+
|  DESCRIPTION  |
+---------------+
San Francisco ZOO, 2008


+--------+
|  TAGS  |
+--------+
sfzoo s5200 s5600 fuji zoo 