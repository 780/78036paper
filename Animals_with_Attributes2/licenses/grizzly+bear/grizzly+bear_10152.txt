+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8544273237/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 3 14:58:22 GMT+0100 2012
Upload Date  : Sun Mar 10 16:01:02 GMT+0100 2013
Geotag Info  : Latitude:47.105828, Longitude:6.822745
Views        : 9,377
Comments     : 111


+---------+
|  TITLE  |
+---------+
Bear with melancholic expression


+---------------+
|  DESCRIPTION  |
+---------------+
The male bear with a quite nice and special expression on his face...


+--------+
|  TAGS  |
+--------+
portrait face melancholic special expression brown bear big male mammal "bois du petit château" "animal park" zoo la-chaux-de-fonds neuchâtel switzerland nikon d4 Blinkagain 