+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : generalising
Photo URL    : https://www.flickr.com/photos/shimgray/2796825629/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 24 16:59:09 GMT+0200 2008
Upload Date  : Mon Aug 25 21:57:00 GMT+0200 2008
Views        : 112
Comments     : 0


+---------+
|  TITLE  |
+---------+
p1210877


+---------------+
|  DESCRIPTION  |
+---------------+
Eurasian Brown Bear (Ursos arctos arctos) at Whipsnade Zoo


+--------+
|  TAGS  |
+--------+
bear "brown bear" "eurasian brown bear" "ursos arctos" "ursos arctos arctos" 