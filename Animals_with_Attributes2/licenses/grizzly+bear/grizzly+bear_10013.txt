+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Max Goldberg
Photo URL    : https://www.flickr.com/photos/max-goldberg/19718763825/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 27 21:43:21 GMT+0200 2015
Upload Date  : Wed Jul 15 16:17:10 GMT+0200 2015
Views        : 421
Comments     : 1


+---------+
|  TITLE  |
+---------+
Brooks Lodge


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Alaska "Brooks Lodge" Bears Salmon Waterfall Water "Katmai National Park & Preserve" Katmai Brooks Camp Brown Grizzly "Natural Habitat" 