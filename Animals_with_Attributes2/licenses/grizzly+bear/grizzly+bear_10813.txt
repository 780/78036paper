+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Hillebrand Steve, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/bears/two-grizzly-bear-cubs
License      : public domain (CC0)
Uploaded     : 2015-01-05 11:03:51

+--------+
|  TAGS  |
+--------+
two, grizzly, bear, cubs
