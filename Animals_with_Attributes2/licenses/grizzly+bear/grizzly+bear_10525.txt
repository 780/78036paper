+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Becker1999
Photo URL    : https://www.flickr.com/photos/becker271/2303514742/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 2 10:04:06 GMT+0100 2008
Upload Date  : Sun Mar 2 01:54:15 GMT+0100 2008
Views        : 279
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_0423


+---------------+
|  DESCRIPTION  |
+---------------+
Fighting Brown Bears


+--------+
|  TAGS  |
+--------+
becker.271 "columbus zoo" animals Fighting Bears brown "Canon Digital Rebel XTi" canon rebel "rebel xti" 