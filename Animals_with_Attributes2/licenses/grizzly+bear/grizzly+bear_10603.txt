+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Arend Vermazeren
Photo URL    : https://www.flickr.com/photos/vermazeren/14316604273/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 25 17:52:42 GMT+0200 2014
Upload Date  : Thu May 29 09:48:14 GMT+0200 2014
Views        : 10,566
Comments     : 1


+---------+
|  TITLE  |
+---------+
Mother bear and 3 cubs of a few months old


+---------------+
|  DESCRIPTION  |
+---------------+
She is very alert and chases anyone getting too close - especially boars (male bears) - away. Also the cubs quickly climb a tree in case of danger.


+--------+
|  TAGS  |
+--------+
"Finland beren" bear beer "brown bear" "bruine beer" bears beren "brown bears" "bruine beren" "Ursus arctos" Finland Martinselkosen Kainu Suomussalmi Pirttivaara cubs 