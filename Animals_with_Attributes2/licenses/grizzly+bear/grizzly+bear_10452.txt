+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jockrutherford
Photo URL    : https://www.flickr.com/photos/jockrutherford/15392677226/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Oct 1 21:11:01 GMT+0200 2014
Upload Date  : Thu Oct 2 03:11:49 GMT+0200 2014
Views        : 286
Comments     : 0


+---------+
|  TITLE  |
+---------+
Grizzly Bear


+---------------+
|  DESCRIPTION  |
+---------------+
Mosquito Creek, Alberta (1995)


+--------+
|  TAGS  |
+--------+
(no tags)