+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Beverly & Pack
Photo URL    : https://www.flickr.com/photos/walkadog/3683171688/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 2 17:27:57 GMT+0200 2009
Upload Date  : Fri Jul 3 01:28:23 GMT+0200 2009
Views        : 36,747
Comments     : 11


+---------+
|  TITLE  |
+---------+
Brown Bear having fun, rolling in the grass on his back with paws up


+---------------+
|  DESCRIPTION  |
+---------------+
The brown bear (Ursus arctos) is a large bear distributed across much of northern Eurasia and North America. It weighs 100 to 680 kilograms (220–1,500 lb) and one of its larger subspecies, the Kodiak bear, matches the polar bear as the largest member of the bear family, and as the largest land predator.

While the brown bear's range has shrunk, and it has faced local extinctions, it remains listed as a least concern species, with a total population of approximately 200,000. Its principal range countries are Russia, the United States (especially Alaska), Canada,the Carpathian area (especially Romania) and Finland where it is the national animal.

The species primarily feeds on vegetable matter, including roots, and fungi. Fish are a primary source of meat. It also eats small land mammals and occasionally larger mammals, such as deer. Adult brown bears can match wolf packs and large felines, often driving them off their kills.

The fractalius filter was added to this image to fine tune it, both for saturation and detail.


+--------+
|  TAGS  |
+--------+
brown bear grizzly grizzley bears Kodiak national wildlife wild paws air play fun rolling lying grass nature portrait closeup "ursus arctos" picture image pictures "north america" predator extinction "united states" canada finland "fractalius filter" redfield claw feet foot pads mammals free download wallpaper background "public domain" "creative commons" waving hello hi greeting barefoot 