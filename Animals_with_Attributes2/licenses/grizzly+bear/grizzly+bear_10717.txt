+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Matt Brittaine
Photo URL    : https://www.flickr.com/photos/mattbrittaine/11690605463/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 15 12:01:26 GMT+0100 2013
Upload Date  : Wed Jan 1 19:06:37 GMT+0100 2014
Views        : 100
Comments     : 0


+---------+
|  TITLE  |
+---------+
Whipsnade 1


+---------------+
|  DESCRIPTION  |
+---------------+
SONY DSC


+--------+
|  TAGS  |
+--------+
"Matt Brittaine" Whipsnade zoo "Whipsnade Zoo" animals wildlife park "wildlife park" brown bear "brown bear" lunchtime 