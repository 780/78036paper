+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike's Birds
Photo URL    : https://www.flickr.com/photos/pazzani/19751785754/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 6 16:53:27 GMT+0200 2015
Upload Date  : Fri Aug 7 17:45:43 GMT+0200 2015
Geotag Info  : Latitude:60.351324, Longitude:-152.903555
Views        : 273
Comments     : 1


+---------+
|  TITLE  |
+---------+
Brown Bear Cubs


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)