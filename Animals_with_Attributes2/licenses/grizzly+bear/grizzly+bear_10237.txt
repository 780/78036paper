+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : __Wichid__
Photo URL    : https://www.flickr.com/photos/-wichid/4738193686/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 26 16:18:33 GMT+0200 2010
Upload Date  : Sun Jun 27 11:02:20 GMT+0200 2010
Geotag Info  : Latitude:-37.786521, Longitude:144.949836
Views        : 180
Comments     : 0


+---------+
|  TITLE  |
+---------+
Brown Bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Melbourne Zoo" 