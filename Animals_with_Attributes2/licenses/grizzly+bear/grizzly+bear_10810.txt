+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Hillebrand Steve, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/bears/two-grizzly-brown-bear-cubs-playing
License      : public domain (CC0)
Uploaded     : 2015-01-05 11:05:30

+--------+
|  TAGS  |
+--------+
two, grizzly, brown, bear, cubs, playing