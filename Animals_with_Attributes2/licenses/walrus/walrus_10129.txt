+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Aug 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tim ellis
Photo URL    : https://www.flickr.com/photos/tim_ellis/28825062321/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 24 11:34:34 GMT+0200 2016
Upload Date  : Thu Aug 11 02:31:31 GMT+0200 2016
Geotag Info  : Latitude:79.732330, Longitude:11.004563
Views        : 25
Comments     : 0


+---------+
|  TITLE  |
+---------+
Walrus


+---------------+
|  DESCRIPTION  |
+---------------+
Smeerenberg  on Amsterdamoya was an old Dutch whaling station (the name means &quot;Blubber Town&quot;) from the early 17th Century  It is opposite Virgohamna on Danskoya was the starting point for may expeditions to the North Pole


+--------+
|  TAGS  |
+--------+
Holiday Spitzbergen Svalbard Arctic Serenissima "MS Serenissima" Smeerenburg Virgohamna Animal Walrus Pinniped "Odobenus rosmarus" Norway 