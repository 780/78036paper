+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Aug 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rwoan
Photo URL    : https://www.flickr.com/photos/rwoan/28509774626/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 10 15:56:46 GMT+0200 2016
Upload Date  : Mon Jul 25 17:46:40 GMT+0200 2016
Geotag Info  : Latitude:80.691569, Longitude:20.859513
Views        : 3
Comments     : 0


+---------+
|  TITLE  |
+---------+
Walrus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Svalbard "Svalbard and Jan Mayen" SJ 