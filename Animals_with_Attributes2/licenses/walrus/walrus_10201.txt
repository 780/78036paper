+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : misterfarmer
Photo URL    : https://pixabay.com/en/walrus-bart-zoo-rock-water-fins-1473278/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : June 16, 2016
Uploaded     : 1 month ago

+--------+
|  TAGS  |
+--------+
walrus, bart, zoo, rock, water, fins, thick, weight, free photos, free images, royalty free
