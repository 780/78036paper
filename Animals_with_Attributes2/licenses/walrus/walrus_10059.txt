+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : garybembridge
Photo URL    : https://www.flickr.com/photos/tipsfortravellers/20276229762/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 26 18:20:14 GMT+0200 2015
Upload Date  : Tue Aug 4 13:31:50 GMT+0200 2015
Views        : 845
Comments     : 0


+---------+
|  TITLE  |
+---------+
Walrus at Poolepynten, Svalbard, Arctic


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
walrus svalbard norway arctic silversea silver explorer 