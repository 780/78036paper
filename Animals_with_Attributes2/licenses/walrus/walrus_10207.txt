+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : nightowl
Photo URL    : https://pixabay.com/en/walrus-sea-world-san-diego-polar-74080/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : June 22, 2012
Uploaded     : Jan. 7, 2013

+--------+
|  TAGS  |
+--------+
Walrus, Sea World, San Diego, Polar, Ocean, California