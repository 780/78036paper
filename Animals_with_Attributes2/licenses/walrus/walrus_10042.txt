+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Martijn Nijenhuis
Photo URL    : https://www.flickr.com/photos/martijnnijenhuis/17263771645/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 19 11:25:35 GMT+0200 2015
Upload Date  : Sat Apr 25 13:16:41 GMT+0200 2015
Geotag Info  : Latitude:52.353306, Longitude:5.616620
Views        : 667
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pacific Walrus


+---------------+
|  DESCRIPTION  |
+---------------+
Showing off at 'the Snor(rrr)show' at the Dolfinarium Harderwijk.


+--------+
|  TAGS  |
+--------+
Dolfinarium Harderwijk Martijn Nijenhuis Nikon D750 "AF-S NIKKOR 70-200mm f/2.8G ED VR II" water Pacific walrus Snor(rrr)show 