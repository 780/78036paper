+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Aug 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rwoan
Photo URL    : https://www.flickr.com/photos/rwoan/27926421103/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 10 15:55:35 GMT+0200 2016
Upload Date  : Mon Jul 25 17:45:47 GMT+0200 2016
Geotag Info  : Latitude:80.691836, Longitude:20.861152
Views        : 2
Comments     : 0


+---------+
|  TITLE  |
+---------+
Walrus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Svalbard "Svalbard and Jan Mayen" SJ 