+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Christopher.Michel
Photo URL    : https://www.flickr.com/photos/cmichel67/19217139594/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 29 03:48:52 GMT+0200 2015
Upload Date  : Mon Jul 20 00:14:30 GMT+0200 2015
Geotag Info  : Latitude:79.944522, Longitude:49.849894
Views        : 367
Comments     : 0


+---------+
|  TITLE  |
+---------+
Walrus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
polarbears 50letpebody nuclearicebreaker "north pole" quark 