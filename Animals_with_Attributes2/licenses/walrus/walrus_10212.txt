+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Hickey Bill, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/walrus/walruses-in-water-close-up
License      : public domain (CC0)
Uploaded     : 2015-01-03 12:52:04

+--------+
|  TAGS  |
+--------+
walruses, water, close