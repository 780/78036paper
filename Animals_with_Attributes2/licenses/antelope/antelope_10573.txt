+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mister-E
Photo URL    : https://www.flickr.com/photos/mister-e/2271524057/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 26 16:32:25 GMT+0100 2008
Upload Date  : Sun Feb 17 20:32:12 GMT+0100 2008
Geotag Info  : Latitude:-24.942907, Longitude:31.711726
Views        : 2,593
Comments     : 6


+---------+
|  TITLE  |
+---------+
Impala


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Kruger "Kruger National Park" "Redbilled Oxpecker" "South Africa" "game reserve" impala NaturesFinest "Show Me Your Quality Pixels" IncredibleNature 