+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Scott Loarie
Photo URL    : https://www.flickr.com/photos/55368994@N06/7512721980/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 3 23:38:26 GMT+0200 2012
Upload Date  : Fri Jul 6 07:09:39 GMT+0200 2012
Views        : 328
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_5787


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
taxonomy:kingdom=Animalia Animalia taxonomy:phylum=Chordata Chordata taxonomy:class=Mammalia Mammalia taxonomy:order=Cetartiodactyla Cetartiodactyla taxonomy:family=Cervidae Cervidae taxonomy:genus=Dama Dama taxonomy:species=dama "taxonomy:binomial=Dama dama" "Dama dama" "Fallow Deer" "Mesopotamian Fallow Deer" "Persian Fallow Deer" "Daim Europ" Gamo Daino "taxonomy:common=Fallow Deer" "taxonomy:common=Mesopotamian Fallow Deer" "taxonomy:common=Persian Fallow Deer" "taxonomy:common=Daim Europ" taxonomy:common=Gamo taxonomy:common=Daino inaturalist:observation=98728 