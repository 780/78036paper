+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bobosh_t
Photo URL    : https://www.flickr.com/photos/frted/4036275282/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Oct 22 14:28:47 GMT+0200 2008
Upload Date  : Fri Oct 23 03:25:44 GMT+0200 2009
Views        : 220
Comments     : 0


+---------+
|  TITLE  |
+---------+
Slender Horned Gazelle


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Cincinnati Zoo 