+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tim Muttoo
Photo URL    : https://www.flickr.com/photos/timmuttoo/416761417/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 1 10:45:14 GMT+0100 2007
Upload Date  : Sat Mar 10 22:23:05 GMT+0100 2007
Geotag Info  : Latitude:2.278204, Longitude:31.683025
Views        : 42
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_1032


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Uganda February 2007 Murchison Falls National Park. 