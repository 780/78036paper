+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike's Birds
Photo URL    : https://www.flickr.com/photos/pazzani/6593630205/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 9 23:13:03 GMT+0100 2011
Upload Date  : Thu Dec 29 13:16:01 GMT+0100 2011
Geotag Info  : Latitude:-24.796187, Longitude:31.498266
Views        : 166
Comments     : 0


+---------+
|  TITLE  |
+---------+
Impala


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)