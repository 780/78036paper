+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : GOGO Visual
Photo URL    : https://www.flickr.com/photos/gogovisual/9498679088/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 9 13:56:31 GMT+0200 2013
Upload Date  : Tue Aug 13 00:08:16 GMT+0200 2013
Geotag Info  : Latitude:51.070682, Longitude:1.013059
Views        : 214
Comments     : 0


+---------+
|  TITLE  |
+---------+
P8094332


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)