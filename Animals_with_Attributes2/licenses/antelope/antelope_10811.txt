+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : z2amiller
Photo URL    : https://www.flickr.com/photos/z2amiller/420752334/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 2 15:58:20 GMT+0100 2007
Upload Date  : Wed Mar 14 05:40:10 GMT+0100 2007
Geotag Info  : Latitude:38.550422, Longitude:-122.713760
Views        : 246
Comments     : 3


+---------+
|  TITLE  |
+---------+
Resting Gazelles


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
safariwest santarosa antelope safari gazelle fb 