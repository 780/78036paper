+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Bird Brian
Photo URL    : https://www.flickr.com/photos/birdbrian/3564664233/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 18 00:00:13 GMT+0100 2009
Upload Date  : Tue May 26 04:28:33 GMT+0200 2009
Views        : 90
Comments     : 0


+---------+
|  TITLE  |
+---------+
2009-03-18 Kalahara Grootkolk 005


+---------------+
|  DESCRIPTION  |
+---------------+
Red Hartebeest


+--------+
|  TAGS  |
+--------+
Red Hartebeest 