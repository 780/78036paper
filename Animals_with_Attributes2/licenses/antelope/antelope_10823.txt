+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andre Kotze
Photo URL    : https://www.flickr.com/photos/andrekotze/7816320366/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 19 12:54:11 GMT+0200 2012
Upload Date  : Sun Aug 19 19:11:58 GMT+0200 2012
Views        : 72
Comments     : 0


+---------+
|  TITLE  |
+---------+
SA 2012-98


+---------------+
|  DESCRIPTION  |
+---------------+
Overland through Namibia and Botswana


+--------+
|  TAGS  |
+--------+
(no tags)