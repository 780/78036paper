+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : berniedup
Photo URL    : https://www.flickr.com/photos/berniedup/6001250973/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 3 13:00:57 GMT+0100 2010
Upload Date  : Tue Aug 2 14:29:29 GMT+0200 2011
Views        : 357
Comments     : 0


+---------+
|  TITLE  |
+---------+
Steenbok (Raphicerus campestris) male


+---------------+
|  DESCRIPTION  |
+---------------+
H5 South of Skukuza, Kruger NP, SOUTH AFRICA


+--------+
|  TAGS  |
+--------+
Steenbok "Raphicerus campestris" "taxonomy:binomial=Raphicerus campestris" 