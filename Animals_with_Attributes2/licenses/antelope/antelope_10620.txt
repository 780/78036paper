+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jthetzel
Photo URL    : https://www.flickr.com/photos/jthetzel/5989225428/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 23 17:46:26 GMT+0200 2011
Upload Date  : Sat Jul 30 01:55:53 GMT+0200 2011
Views        : 71
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kenya and Tanzania


+---------------+
|  DESCRIPTION  |
+---------------+
Scanned Image 00162

Christmas 1996


+--------+
|  TAGS  |
+--------+
East Africa 