+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : VSmithUK
Photo URL    : https://www.flickr.com/photos/vsmithuk/3076040642/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 9 09:56:28 GMT+0100 2008
Upload Date  : Tue Dec 2 01:18:38 GMT+0100 2008
Geotag Info  : Latitude:-1.752046, Longitude:35.046386
Views        : 272
Comments     : 0


+---------+
|  TITLE  |
+---------+
Thomson's Gazelle


+---------------+
|  DESCRIPTION  |
+---------------+
<i>Eudorcas thomsoni</i>, Northern Serengeti, Tanzania.


+--------+
|  TAGS  |
+--------+
_Favorite_ Tanzania Serengeti Mammal Life 