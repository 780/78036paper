+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : djpmapleferryman
Photo URL    : https://www.flickr.com/photos/63319497@N00/3006794381/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 5 22:53:04 GMT+0100 2008
Upload Date  : Thu Nov 6 07:53:04 GMT+0100 2008
Views        : 185
Comments     : 0


+---------+
|  TITLE  |
+---------+
081104_GRANTSsgazelle86


+---------------+
|  DESCRIPTION  |
+---------------+
Ngorongoro crater, Grant's gazelle.


+--------+
|  TAGS  |
+--------+
"taxonomy:binomial=Nanger granti" 