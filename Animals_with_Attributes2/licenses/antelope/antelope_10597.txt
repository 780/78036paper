+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : regexman
Photo URL    : https://www.flickr.com/photos/regexman/15383132355/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 20 08:18:43 GMT+0200 2014
Upload Date  : Sun Sep 28 21:06:33 GMT+0200 2014
Geotag Info  : Latitude:29.705448, Longitude:-98.351902
Views        : 26
Comments     : 0


+---------+
|  TITLE  |
+---------+
Impala


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)