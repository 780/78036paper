+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : LGO'Brien
Photo URL    : https://www.flickr.com/photos/lgobrien/7882335284/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 21 03:04:37 GMT+0200 2012
Upload Date  : Tue Aug 28 21:32:50 GMT+0200 2012
Views        : 59
Comments     : 0


+---------+
|  TITLE  |
+---------+
dsc_0527081412Ae


+---------------+
|  DESCRIPTION  |
+---------------+
Impala on the runway


+--------+
|  TAGS  |
+--------+
Africa July 2012 Kamalu Camp 