+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Base Camp Baker
Photo URL    : https://www.flickr.com/photos/basecampbaker/9008610917/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 23 23:00:10 GMT+0200 2013
Upload Date  : Mon Jun 10 22:18:19 GMT+0200 2013
Views        : 783
Comments     : 0


+---------+
|  TITLE  |
+---------+
Baker County Tourism – basecampbaker.com 7353


+---------------+
|  DESCRIPTION  |
+---------------+
Watching wildlife near Farewell Bend State Park near Huntington Oregon 

Baker County’s diverse geography and habitat are ideal for wildlife watching including River Otters like these.  In addition to great year round birding , visitors can see deer, bighorn sheep, elk, and antelope,  along the three scenic byways that connect in Baker City.  
Farewell Bend State Park and historic hwy 30 near the small town of Huntington is one of our favorite places for spotting local wildlife and we almost always encounter some great birds and a few Antelope whenever we visit. 

For more information about Farewell Bend State Park, or other watchable wildlife viewing opportunities in Baker County, visit the Baker County Tourism Website at <a href="http://www.basecampbaker.com" rel="nofollow">www.basecampbaker.com</a>


+--------+
|  TAGS  |
+--------+
Wildlife "“Watchable Wildlife”" "Wildlife viewing”" "“Hwy 30”" "“Farewell Bend State Park”" Huntington "“Baker County”" Oregon "“Eastern Oregon”" "“Baker County tourism”" basecampbaker Antelope 