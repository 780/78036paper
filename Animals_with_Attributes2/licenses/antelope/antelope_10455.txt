+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ladylinoleum
Photo URL    : https://www.flickr.com/photos/87739302@N00/4825317646/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 24 13:16:22 GMT+0200 2010
Upload Date  : Sun Jul 25 01:33:21 GMT+0200 2010
Views        : 88
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ibex


+---------------+
|  DESCRIPTION  |
+---------------+
LA Zoo, July, 2010.


+--------+
|  TAGS  |
+--------+
"5.0-60.0 mm" 