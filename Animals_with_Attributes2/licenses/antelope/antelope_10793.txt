+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Larry1732
Photo URL    : https://www.flickr.com/photos/larry1732/14595558218/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 26 06:21:40 GMT+0200 2014
Upload Date  : Wed Jul 30 04:51:02 GMT+0200 2014
Views        : 453
Comments     : 1


+---------+
|  TITLE  |
+---------+
Pronghorn, aka antelope


+---------------+
|  DESCRIPTION  |
+---------------+
La Veta, Colorado


+--------+
|  TAGS  |
+--------+
"la veta" colorado lamsa CO pronghorn antelope 