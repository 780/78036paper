+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Frontierofficial
Photo URL    : https://www.flickr.com/photos/frontierofficial/9553930023/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 20 16:48:52 GMT+0200 2013
Upload Date  : Tue Aug 20 17:50:27 GMT+0200 2013
Views        : 134
Comments     : 0


+---------+
|  TITLE  |
+---------+
983610_10200765466717742_886719656_n


+---------------+
|  DESCRIPTION  |
+---------------+
Thanks to Luke Holdsworth for his photos from South Africa Cricket Coaching. Find out more at <a href="http://www.frontiergap.com" rel="nofollow">www.frontiergap.com</a>


+--------+
|  TAGS  |
+--------+
cricket "South Africa" 