+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Alberto Ziveri
Photo URL    : https://www.flickr.com/photos/albyzzolo/6217638261/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 19 06:50:01 GMT+0200 2011
Upload Date  : Thu Oct 6 20:42:49 GMT+0200 2011
Geotag Info  : Latitude:-29.801773, Longitude:30.634517
Views        : 238
Comments     : 0


+---------+
|  TITLE  |
+---------+
A common South African antilope


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Africa antilope South 