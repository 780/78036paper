+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : williamjohnson1232
Photo URL    : https://www.flickr.com/photos/125207380@N08/15709334236/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 26 16:11:42 GMT+0100 2014
Upload Date  : Fri Nov 7 18:23:23 GMT+0100 2014
Geotag Info  : Latitude:40.157098, Longitude:-83.116550
Views        : 63
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_3724


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Columbus Zoo" Halloween Columbus OH 