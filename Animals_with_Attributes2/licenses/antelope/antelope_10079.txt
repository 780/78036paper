+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Archit Ratan Photography
Photo URL    : https://www.flickr.com/photos/architratan/7301481938/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 27 07:55:31 GMT+0200 2012
Upload Date  : Wed May 30 14:55:18 GMT+0200 2012
Geotag Info  : Latitude:27.836647, Longitude:74.423446
Views        : 9,796
Comments     : 67


+---------+
|  TITLE  |
+---------+
a pair of blackbuck at tal chhapar


+---------------+
|  DESCRIPTION  |
+---------------+
Tal Chhapar Sanctuary is a sanctuary located in the Churu district of Northwestern Rajasthan in the Bikaner division of India. It is 210 km from Jaipur and situated on road from Ratangarh to Sujangarh. The Tal Chhapar sanctuary lies in the Sujangarh Tehsil of Churu District. It lies on Nokha- Sujangarh state Highway and is situated at a distance of 85 km from Churu &amp; about 132 km from Bikaner. The nearest Railway station is Chappar which lies on Degana – Churu – Rewari metre gauge line of North Western Railways. The nearest Airport is Sanganer (Jaipur) which is at a distance of 215 km from Chappar. It is known for black bucks and is also home to a variety of birds.

The Tal Chhapar sanctuary is located on the fringe of the Great Indian Desert. Tal Chhapar nestles a unique refuge of the most elegant Antelope encountered in India, &quot;the Black buck&quot;. Tal Chhaper sanctuary, with almost flat tract and interspersed shallow low lying areas, has open grassland with scattered Acacia and prosopis trees which give it an appearance of a typical Savanna. The word &quot;Tal&quot; means plane land. The rain water flows through shallow low lying areas and collect in the small seasonal water ponds.

The Geology of the zone is obscured by the wind blown over-burden. Some small hillocks and exposed rocks of slate and quartzite are found in the western side of the sanctuary. Area between hillocks and the sanctuary constitutes the watershed area of the sanctuary. The whole sanctuary used to be flooded by water during the heavy rains but with salt mining going on in the watershed. Hardly any rain water falling on the hillocks reach the Sanctuary


+--------+
|  TAGS  |
+--------+
antilope india rajasthan blackbuck deer "tal chhapar" bikaner churu 