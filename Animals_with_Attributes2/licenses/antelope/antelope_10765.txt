+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Robert Nyman
Photo URL    : https://www.flickr.com/photos/robertnyman/6595383771/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 31 07:56:32 GMT+0100 2011
Upload Date  : Thu Dec 29 19:29:06 GMT+0100 2011
Views        : 29
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pilanesberg Game Reserve, South Africa


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)