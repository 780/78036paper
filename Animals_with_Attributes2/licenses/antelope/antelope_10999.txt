+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : romanboed
Photo URL    : https://www.flickr.com/photos/romanboed/8368870726/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 22 05:57:34 GMT+0100 2012
Upload Date  : Thu Jan 10 22:44:54 GMT+0100 2013
Geotag Info  : Latitude:-2.278032, Longitude:34.931545
Views        : 231
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Africa landscape safari Serengeti Tanzania travel wildlife 