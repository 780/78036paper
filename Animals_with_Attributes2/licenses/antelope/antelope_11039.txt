+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : berniedup
Photo URL    : https://www.flickr.com/photos/berniedup/6511478493/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 9 09:49:09 GMT+0100 2011
Upload Date  : Wed Dec 14 18:21:28 GMT+0100 2011
Geotag Info  : Latitude:-25.822144, Longitude:20.053138
Views        : 256
Comments     : 0


+---------+
|  TITLE  |
+---------+
Young Springbok (Antidorcas marsupialis)


+---------------+
|  DESCRIPTION  |
+---------------+
Auob Riverbed, Kgalagadi Transfrontier Park, SOUTH AFRICA


+--------+
|  TAGS  |
+--------+
Antidorcas marsupialis "taxonomy:binomial=Antidorcas marsupialis" 