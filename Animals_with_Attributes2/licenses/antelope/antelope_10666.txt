+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nao Iizuka
Photo URL    : https://www.flickr.com/photos/iizukanao/2293386435/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 9 23:35:05 GMT+0100 2007
Upload Date  : Tue Feb 26 16:38:47 GMT+0100 2008
Views        : 31
Comments     : 0


+---------+
|  TITLE  |
+---------+
Masai Mara


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)