+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : girolame
Photo URL    : https://www.flickr.com/photos/girolame/3053489911/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 16 17:03:50 GMT+0100 2008
Upload Date  : Sun Nov 23 23:19:29 GMT+0100 2008
Geotag Info  : Latitude:-24.661575, Longitude:30.316257
Views        : 287
Comments     : 0


+---------+
|  TITLE  |
+---------+
Blesbok


+---------------+
|  DESCRIPTION  |
+---------------+
Réserve privée de Hannah Lodge 
Burgersfort, Afrique du Sud


+--------+
|  TAGS  |
+--------+
Burgersfort "afrique du sud" "south africa" IXUS50 blesbok "Hannah Lodge" 