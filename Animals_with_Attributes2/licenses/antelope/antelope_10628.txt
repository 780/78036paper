+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brad.coy
Photo URL    : https://www.flickr.com/photos/bradfordcoy/3143805950/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 26 15:29:14 GMT+0100 2007
Upload Date  : Sun Dec 28 10:02:49 GMT+0100 2008
Geotag Info  : Latitude:35.148777, Longitude:-89.985966
Views        : 224
Comments     : 1


+---------+
|  TITLE  |
+---------+
Klipspringer Antelope


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Klipspringer Antelope zoo "memphis zoo" memphis Tennessee animals panasonic lumix tz5 