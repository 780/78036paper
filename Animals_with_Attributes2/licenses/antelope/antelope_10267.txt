+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mind on Fire Photography
Photo URL    : https://www.flickr.com/photos/sandybrownjensen/17036328101/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 4 20:48:27 GMT+0200 2015
Upload Date  : Sun Apr 5 05:48:27 GMT+0200 2015
Views        : 27
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dino Quarry & Red Ledges, Utah


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)