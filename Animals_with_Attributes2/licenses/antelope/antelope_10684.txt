+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Charlesjsharp
Photo URL    : https://www.flickr.com/photos/93882360@N07/12801059065/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 15 07:45:18 GMT+0100 2014
Upload Date  : Wed Feb 26 23:11:10 GMT+0100 2014
Views        : 294
Comments     : 0


+---------+
|  TITLE  |
+---------+
mountain gazelle (gazella gazella)


+---------------+
|  DESCRIPTION  |
+---------------+
mountain gazelle (gazella gazella), Dubai Desert Conservation Reserve, UAE


+--------+
|  TAGS  |
+--------+
(no tags)