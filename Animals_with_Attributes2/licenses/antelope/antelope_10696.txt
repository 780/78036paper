+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SqueakyMarmot
Photo URL    : https://www.flickr.com/photos/squeakymarmot/14714283443/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 14 11:30:15 GMT+0200 2014
Upload Date  : Sun Jul 20 01:16:18 GMT+0200 2014
Views        : 388
Comments     : 0


+---------+
|  TITLE  |
+---------+
Nairobi National Park 2


+---------------+
|  DESCRIPTION  |
+---------------+
At times it was surrealistic to be tracking wild animals at the outskirts of Nairobi with the skyline lurking above the horizon.


+--------+
|  TAGS  |
+--------+
2014 travel Africa Kenya Nairobi "Nairobi National Park" 