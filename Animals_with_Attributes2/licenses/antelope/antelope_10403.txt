+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : edenpictures
Photo URL    : https://www.flickr.com/photos/edenpictures/4690667264/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 10 12:13:13 GMT+0200 2010
Upload Date  : Fri Jun 11 13:40:48 GMT+0200 2010
Geotag Info  : Latitude:40.847135, Longitude:-73.877446
Views        : 159
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gazelle


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Bronx Zoo" gazelle Picnik 