+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pink_pixie21
Photo URL    : https://www.flickr.com/photos/11172034@N08/4435307507/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 15 14:17:44 GMT+0100 2010
Upload Date  : Mon Mar 15 19:15:41 GMT+0100 2010
Views        : 236
Comments     : 0


+---------+
|  TITLE  |
+---------+
Red Lechwe Antelope


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"red lechwe antelope" antelope deer animal "port lympne" "port lympne wild animal park" zoo 