+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : David W. Siu
Photo URL    : https://www.flickr.com/photos/dwysiu/5386943394/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 30 16:15:19 GMT+0200 2010
Upload Date  : Tue Jan 25 07:25:55 GMT+0100 2011
Views        : 390
Comments     : 0


+---------+
|  TITLE  |
+---------+
Impala


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Kruger National Park august 2010" Kruger NAtion Krugernationalpark Wildlife South Africa African Safari 