+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Abeeeer
Photo URL    : https://www.flickr.com/photos/abir82/4852829925/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 1 17:19:37 GMT+0200 2010
Upload Date  : Mon Aug 2 15:59:38 GMT+0200 2010
Views        : 411
Comments     : 0


+---------+
|  TITLE  |
+---------+
Serengeti 85


+---------------+
|  DESCRIPTION  |
+---------------+
A Thompson's gazelle stares back at me.


+--------+
|  TAGS  |
+--------+
Serengeti Tanzania Safari Thompson's gazelle 