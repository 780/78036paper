+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Arno Kleine Schaars
Photo URL    : https://www.flickr.com/photos/97388720@N00/9466283578/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 24 12:06:35 GMT+0200 2013
Upload Date  : Thu Aug 8 15:35:18 GMT+0200 2013
Views        : 96
Comments     : 0


+---------+
|  TITLE  |
+---------+
_MG_4502.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Afrika_2013 