+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sara&Joachim
Photo URL    : https://www.flickr.com/photos/sara_joachim/2181503064/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 6 13:46:16 GMT+0100 2007
Upload Date  : Wed Jan 9 21:38:20 GMT+0100 2008
Views        : 89
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_1428


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Hells Gate National Park" Africa Wildlife Kenya 