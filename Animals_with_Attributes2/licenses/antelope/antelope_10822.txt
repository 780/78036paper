+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/12003914456/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 11 15:55:54 GMT+0200 2013
Upload Date  : Sat Jan 18 01:00:48 GMT+0100 2014
Geotag Info  : Latitude:-25.994984, Longitude:27.933028
Views        : 20,350
Comments     : 0


+---------+
|  TITLE  |
+---------+
Running bontebok


+---------------+
|  DESCRIPTION  |
+---------------+
One of these gracious animals running in the savanna of the game drive of the park...


+--------+
|  TAGS  |
+--------+
bontebok blesbok antelope running fast savanna bush "game drive" "lion park" johannesburg "south africa" nikon d4 