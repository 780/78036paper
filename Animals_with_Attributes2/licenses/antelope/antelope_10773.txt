+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ahisgett
Photo URL    : https://www.flickr.com/photos/hisgett/15528045846/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 28 15:25:38 GMT+0200 2014
Upload Date  : Thu Oct 16 22:35:37 GMT+0200 2014
Geotag Info  : Latitude:37.568543, Longitude:-112.236329
Views        : 190
Comments     : 1


+---------+
|  TITLE  |
+---------+
Pronghorn 1


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Utah Bryce Canyon 