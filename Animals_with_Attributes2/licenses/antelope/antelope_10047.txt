+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Giåm
Photo URL    : https://www.flickr.com/photos/84554176@N00/1675802425/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 17 13:34:19 GMT+0200 2007
Upload Date  : Sun Oct 21 20:43:49 GMT+0200 2007
Geotag Info  : Latitude:-3.190136, Longitude:35.586433
Views        : 55
Comments     : 0


+---------+
|  TITLE  |
+---------+
2007-08-17


+---------------+
|  DESCRIPTION  |
+---------------+
Tanzanie, 
Safari dans le Ngorongoro


+--------+
|  TAGS  |
+--------+
Tanzania Tanzanie Ngorongoro Safari 