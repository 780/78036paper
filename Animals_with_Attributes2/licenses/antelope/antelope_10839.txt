+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jorn Eriksson
Photo URL    : https://www.flickr.com/photos/jorneriksson/8518718068/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 31 19:34:52 GMT+0100 2013
Upload Date  : Fri Mar 1 14:06:01 GMT+0100 2013
Views        : 153
Comments     : 0


+---------+
|  TITLE  |
+---------+
Safari in Uganda


+---------------+
|  DESCRIPTION  |
+---------------+
Queen Elisabeth National Park


+--------+
|  TAGS  |
+--------+
Uganda Safari 