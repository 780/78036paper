+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : AindriúH
Photo URL    : https://www.flickr.com/photos/97568737@N06/9851516886/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 22 06:12:15 GMT+0200 2013
Upload Date  : Sat Sep 21 12:17:37 GMT+0200 2013
Views        : 94
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gazelle


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Africa Botswana Chobe 