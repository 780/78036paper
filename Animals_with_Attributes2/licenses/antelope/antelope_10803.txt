+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Steve Slater (used to be Wildlife Encounters)
Photo URL    : https://www.flickr.com/photos/wildlife_encounters/13869143715/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 1 09:34:37 GMT+0200 2011
Upload Date  : Tue Apr 15 13:20:26 GMT+0200 2014
Views        : 618
Comments     : 0


+---------+
|  TITLE  |
+---------+
red duiker an endangered antelope taken in igwalagwala forest


+---------------+
|  DESCRIPTION  |
+---------------+
red duiker an endangered antelope taken in igwalagwala forest


+--------+
|  TAGS  |
+--------+
Isimangaliso "South Africa" antelope endangered kwazulunatal nature rare "red duiker" "st lucia" wildlife 