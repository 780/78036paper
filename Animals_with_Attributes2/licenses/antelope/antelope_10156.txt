+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : zigazou76
Photo URL    : https://www.flickr.com/photos/zigazou76/17467145944/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 25 13:56:28 GMT+0200 2015
Upload Date  : Mon May 25 20:24:52 GMT+0200 2015
Geotag Info  : Latitude:49.597005, Longitude:1.105141
Views        : 150
Comments     : 0


+---------+
|  TITLE  |
+---------+
Antilope cervicapre


+---------------+
|  DESCRIPTION  |
+---------------+
Antilope cervicapre dans le parc de Clères


+--------+
|  TAGS  |
+--------+
antilope clères parc zoo 