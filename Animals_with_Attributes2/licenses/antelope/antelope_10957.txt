+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Stig Nygaard
Photo URL    : https://www.flickr.com/photos/stignygaard/2459098871/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 12 13:01:58 GMT+0100 2007
Upload Date  : Fri May 2 19:30:10 GMT+0200 2008
Geotag Info  : Latitude:-2.455636, Longitude:34.774990
Views        : 4,195
Comments     : 2


+---------+
|  TITLE  |
+---------+
Thomson's Gazelles


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2007 Africa antelope "Canon EF 70-300mm f/4-5.6 IS USM" "East Africa" "Eastern Africa" "Gazella thomsoni" Gazelle nature Safari Serengeti Seronera Tanzania "Thomson's Gazelle" wildlife running action "Photo by Stig Nygaard" "Creative Commons" 400D "Canon EOS 400D" "Serengeti National Park" "UNESCO World Heritage Site" "World Heritage Site" 