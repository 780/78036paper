+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MalcolmMacgregor
Photo URL    : https://www.flickr.com/photos/maccattack2/6189846971/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 2 05:48:02 GMT+0200 2011
Upload Date  : Wed Sep 28 00:06:20 GMT+0200 2011
Views        : 159
Comments     : 0


+---------+
|  TITLE  |
+---------+
Red Lechwe


+---------------+
|  DESCRIPTION  |
+---------------+
Specially adapted antelope in the marshes of the Okavango


+--------+
|  TAGS  |
+--------+
Botswana 