+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USFWS Mountain Prairie
Photo URL    : https://www.flickr.com/photos/usfwsmtnprairie/17423783908/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 10 12:57:16 GMT+0200 2015
Upload Date  : Wed May 13 22:08:18 GMT+0200 2015
Views        : 1,087
Comments     : 0


+---------+
|  TITLE  |
+---------+
Boys Club on Seedskadee NWR


+---------------+
|  DESCRIPTION  |
+---------------+
A group of male pronghorns on Seedskadee NWR.  Patches of shed hair and bare skin are visible while the fresh summer coat fills in.  Photo: Tom Koerner/USFWS


+--------+
|  TAGS  |
+--------+
Pronghorn "Seedskadee NWR" NWRS USFWS Nature conservation Wildlife 