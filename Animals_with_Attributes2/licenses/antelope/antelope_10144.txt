+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Larry1732
Photo URL    : https://www.flickr.com/photos/larry1732/16607118844/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 14 07:05:30 GMT+0200 2015
Upload Date  : Wed Apr 22 04:06:58 GMT+0200 2015
Views        : 176
Comments     : 0


+---------+
|  TITLE  |
+---------+
Antelope, aka Pronghorn


+---------------+
|  DESCRIPTION  |
+---------------+
Near La Veta, Colorado


+--------+
|  TAGS  |
+--------+
antelope pronghorn lamsa colorado co "la veta" 