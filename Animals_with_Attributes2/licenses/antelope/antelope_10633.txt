+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : oddsock
Photo URL    : https://www.flickr.com/photos/oddsock/264868235/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 12 15:46:55 GMT+0200 2006
Upload Date  : Mon Oct 9 13:07:37 GMT+0200 2006
Views        : 6,007
Comments     : 3


+---------+
|  TITLE  |
+---------+
Kudu Antelope  - Botswana


+---------------+
|  DESCRIPTION  |
+---------------+
Kudu Antelope  - Botswana
the raw image for week 12 of the Weekly Photoshop Competition group
<a href="http://www.flickr.com/groups/pscomp/discuss/72157594320046373/">www.flickr.com/groups/pscomp/discuss/72157594320046373/</a>

This photograph of a female Kudu antelope in Botswana is this weeks raw image, I like the way she blends in to the bush and her fantastic ears ! a beautiful face and shape  : )

Their cryptic coloring and markings protect kudus by camouflaging them. If alarmed they usually stand still and are very difficult to spot.

Habitat - Lesser kudus are found in acacia and commiphora thornbush in arid savannas; they rely on thickets for security and are rarely found in open or scattered bush. Greater kudus are found in woodlands and bushlands.

Predators - leopards, hunting dogs, spotted hyenas, humans


+--------+
|  TAGS  |
+--------+
Kudu Antelope Botswana Africa "week 12" "Weekly Photoshop Competition" safari "Tragelaphus strepsiceros" 