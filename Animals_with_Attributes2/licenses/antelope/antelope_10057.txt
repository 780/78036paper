+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MaxH42
Photo URL    : https://www.flickr.com/photos/maxh42/9592544136/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 13 09:25:19 GMT+0200 2013
Upload Date  : Sun Aug 25 18:58:40 GMT+0200 2013
Views        : 219
Comments     : 0


+---------+
|  TITLE  |
+---------+
2013-08-13 09.25.19


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animal "Thompson's gazelle" 