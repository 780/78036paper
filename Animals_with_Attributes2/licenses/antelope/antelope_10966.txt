+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : HBarrison
Photo URL    : https://www.flickr.com/photos/hbarrison/7407631614/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 20 09:12:58 GMT+0200 2012
Upload Date  : Wed Jun 20 15:07:17 GMT+0200 2012
Geotag Info  : Latitude:-25.035423, Longitude:31.721420
Views        : 186
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tinga_2012 05 20_1022


+---------------+
|  DESCRIPTION  |
+---------------+
On the Morning Game Drive  -  impala (Aepyceros melampus), a medium-sized African antelope


+--------+
|  TAGS  |
+--------+
Africa HBarrison "Harvey Barrison" Tauck Tinga "Kruger National Park" "South Africa" impala "Taxonomy:binomial=Aepyceros melampus" Taxonomy:binomial=Aepyceros melampus 