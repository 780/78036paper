+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Upsilon Andromedae
Photo URL    : https://www.flickr.com/photos/upsand/6876686378/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 7 15:43:43 GMT+0200 2011
Upload Date  : Wed Mar 28 05:02:34 GMT+0200 2012
Views        : 314
Comments     : 2


+---------+
|  TITLE  |
+---------+
Pronghorn antelope


+---------------+
|  DESCRIPTION  |
+---------------+
Lamar Valley, Yellowstone National Park.


+--------+
|  TAGS  |
+--------+
(no tags)