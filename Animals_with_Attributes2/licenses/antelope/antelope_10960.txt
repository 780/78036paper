+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Martijn.Munneke
Photo URL    : https://www.flickr.com/photos/martijnmunneke/6693807257/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 11 08:00:05 GMT+0100 2012
Upload Date  : Sat Jan 14 09:14:30 GMT+0100 2012
Views        : 365
Comments     : 1


+---------+
|  TITLE  |
+---------+
Thomsongazelle


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
tanzania ngorongoro crater thomson's gazelle 