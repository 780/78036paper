+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Abspires40
Photo URL    : https://www.flickr.com/photos/abster35/13312526005/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 22 14:45:01 GMT+0100 2014
Upload Date  : Fri Mar 21 19:05:49 GMT+0100 2014
Views        : 536
Comments     : 0


+---------+
|  TITLE  |
+---------+
Best of Kruger 2014 beautiful


+---------------+
|  DESCRIPTION  |
+---------------+
Kruger National Park, South Africa


+--------+
|  TAGS  |
+--------+
"Kruger Park" KNP "Kruger National Park" "South Africa" Wildlife Animals Scenery Nature 