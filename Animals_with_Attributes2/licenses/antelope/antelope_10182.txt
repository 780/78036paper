+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Galli Luca
Photo URL    : https://www.flickr.com/photos/lucagalli/20389039003/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 30 14:13:29 GMT+0200 2015
Upload Date  : Sun Aug 30 20:44:22 GMT+0200 2015
Views        : 35
Comments     : 0


+---------+
|  TITLE  |
+---------+
Antilope


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Bioparco Cumiana Zoom 