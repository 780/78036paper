+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 02, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eska2203_Sil
Photo URL    : https://www.flickr.com/photos/8930168@N06/5922292438/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 17 13:37:13 GMT+0200 2011
Upload Date  : Sun Jul 10 16:28:49 GMT+0200 2011
Geotag Info  : Latitude:51.543666, Longitude:7.109355
Views        : 319
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rappenantilope - Sable Antelope


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Rappenantilope "Sable Antelope" Antilope antelope "Hippotragus niger" mammal Säugetier Zoo "zoom Erlebniswelt" "zoom Gelsenkirchen" Gelsenkirchen Afrika Africa Afrikawelt „Africa world“ animals nature fauna Tiere Wildtiere animals“ „wild 