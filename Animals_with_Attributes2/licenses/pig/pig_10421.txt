+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : StripeyAnne
Photo URL    : https://www.flickr.com/photos/stripeyanne/2750172167/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 8 15:39:17 GMT+0200 2008
Upload Date  : Sun Aug 10 22:10:06 GMT+0200 2008
Views        : 12,331
Comments     : 2


+---------+
|  TITLE  |
+---------+
Pigs that look like Poirot


+---------------+
|  DESCRIPTION  |
+---------------+
You've heard of cats that look like Hitler? Here's the Poirot pigs. Clever too.....


+--------+
|  TAGS  |
+--------+
pigs; stripeyanne; Poirot 