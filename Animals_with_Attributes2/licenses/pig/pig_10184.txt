+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mihaelhercog
Photo URL    : https://www.flickr.com/photos/114179746@N08/11889699773/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 11 12:17:12 GMT+0100 2014
Upload Date  : Sat Jan 11 17:41:31 GMT+0100 2014
Views        : 4,834
Comments     : 1


+---------+
|  TITLE  |
+---------+
Little Pig


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at Zoo Ljubljana


+--------+
|  TAGS  |
+--------+
Pig 