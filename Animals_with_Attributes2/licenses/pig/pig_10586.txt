+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : me'nthedogs
Photo URL    : https://www.flickr.com/photos/66176388@N00/6006253242/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 3 06:34:27 GMT+0200 2011
Upload Date  : Wed Aug 3 20:50:06 GMT+0200 2011
Geotag Info  : Latitude:51.140317, Longitude:-3.598022
Views        : 989
Comments     : 8


+---------+
|  TITLE  |
+---------+
Wandering Piglets


+---------------+
|  DESCRIPTION  |
+---------------+
I came across these, wandering down a main road on Exmoor early this morning. Luckily they went into a gateway and I was able to follow them and alert the owner.


+--------+
|  TAGS  |
+--------+
pigs piglets 