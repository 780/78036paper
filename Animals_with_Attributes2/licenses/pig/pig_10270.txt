+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jbdodane
Photo URL    : https://www.flickr.com/photos/jbdodane/9735162122/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 6 08:49:59 GMT+0200 2013
Upload Date  : Fri Sep 6 10:49:59 GMT+0200 2013
Geotag Info  : Latitude:6.776944, Longitude:-1.778333
Views        : 2,680
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pig farm


+---------------+
|  DESCRIPTION  |
+---------------+
Taken on 06 September 2013 in Ghana near Ntensere Kumasi (DSC_1742)

<a href="http://freewheely.com" rel="nofollow">freewheely.com</a>: Cycling Africa beyond mountains and deserts until Cape Town


+--------+
|  TAGS  |
+--------+
africa ashanti bicycle day307 farm ghana piggery pigs freewheely.com "cycle touring" cyclotourisme velo cycling jbcyclingafrica 