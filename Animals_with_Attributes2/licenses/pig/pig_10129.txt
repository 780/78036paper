+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Malik_Braun
Photo URL    : https://www.flickr.com/photos/malik_braun/3739359610/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 5 11:14:26 GMT+0200 2009
Upload Date  : Mon Jul 20 16:41:05 GMT+0200 2009
Views        : 1,025
Comments     : 6


+---------+
|  TITLE  |
+---------+
Glücksschwein zum Montag - Fortune piggy for Monday


+---------------+
|  DESCRIPTION  |
+---------------+
This little wild piglet was directly in front of my lens :)

Das kleine Wildschwein-Ferkel stand direkt vor meiner Linse.


+--------+
|  TAGS  |
+--------+
schwein pig ferkel piglet wildschwein jung tamronaf55200mmf456diiild 