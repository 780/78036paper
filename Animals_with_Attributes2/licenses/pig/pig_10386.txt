+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jennicatpink
Photo URL    : https://www.flickr.com/photos/jennicatpink/7956977364/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 8 13:41:30 GMT+0200 2012
Upload Date  : Sat Sep 8 20:44:30 GMT+0200 2012
Views        : 6,384
Comments     : 0


+---------+
|  TITLE  |
+---------+
Piglet pile


+---------------+
|  DESCRIPTION  |
+---------------+
Piglet pile


+--------+
|  TAGS  |
+--------+
pig piglets "farm animals" 