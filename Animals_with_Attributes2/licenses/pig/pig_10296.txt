+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andy Hay
Photo URL    : https://www.flickr.com/photos/andyhay/425847886/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 30 15:01:00 GMT+0200 2000
Upload Date  : Sun Mar 18 23:38:37 GMT+0100 2007
Geotag Info  : Latitude:47.865919, Longitude:-2.460650
Views        : 790
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pig racing


+---------------+
|  DESCRIPTION  |
+---------------+
Pig racing at the Roc St Andre village pardon.


+--------+
|  TAGS  |
+--------+
2000 35mm brittany festival france holidays "le roc st andre" negatives pardon pig "pig racing" racing "roc st andre" scanned summer 