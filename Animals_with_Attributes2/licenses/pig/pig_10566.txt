+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8740309971/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 3 14:27:48 GMT+0100 2013
Upload Date  : Wed May 15 15:00:15 GMT+0200 2013
Geotag Info  : Latitude:47.387367, Longitude:8.576009
Views        : 6,605
Comments     : 4


+---------+
|  TITLE  |
+---------+
Two cute dalmatian piglets


+---------------+
|  DESCRIPTION  |
+---------------+
We went to the zoolino to take pictures of these cute dalmatian piglets, there were about 10 and they were running and playing a lot! :)


+--------+
|  TAGS  |
+--------+
dalmatian pig piglet black white running playing fun cute zürich zoo switzerland nikon d4 