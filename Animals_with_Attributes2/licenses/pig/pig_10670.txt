+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : r.nial.bradshaw
Photo URL    : https://www.flickr.com/photos/zionfiction/8540864796/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 8 13:01:10 GMT+0100 2013
Upload Date  : Sat Mar 9 00:34:19 GMT+0100 2013
Views        : 979
Comments     : 0


+---------+
|  TITLE  |
+---------+
pigs-fenced-livestock.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
<b>Nikon D40</b>
ISO400
Aperture f/8
Exposure 1/500
55-200mm AF-S nikkor 1:4-5.6G ED
Lightroom 3


+--------+
|  TAGS  |
+--------+
pig farm fences caged animal "chain link" snout "stock photo" "stock photography" "royalty free" "attribution license" "free image" 