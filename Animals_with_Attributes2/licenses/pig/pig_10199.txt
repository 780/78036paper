+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Vjeran Pavic
Photo URL    : https://www.flickr.com/photos/vjeran_pavic/8719320902/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 16 16:53:52 GMT+0100 2013
Upload Date  : Wed May 8 02:15:33 GMT+0200 2013
Views        : 8,162
Comments     : 3


+---------+
|  TITLE  |
+---------+
Meet Puddle - only teacup pig I ever met


+---------------+
|  DESCRIPTION  |
+---------------+
My friends from college, Emily and Renee decided to get a tea cup pig as their pet after they moved off campus! Puddle is now around 6 months old and you can follow this adorable creature on instagram @puddlelh (he is having a bit of problems using his trotters on a touchscreen, be patient :))


<a rel="nofollow"><b>__________________________________________________________________________

Be sure to connect find me through 

<a href="https://instagram.com/vjeranpavic"><b><u>- - - Instagram - - -</u></b></a> 

<a href="https://twitter.com/vjeranpavic" rel="nofollow"><b><u>- - - Twitter - - -</u>

<a href="https://www.vjeranpavic.com/"><b><u>- - - Personal Webpage - - -</u></b></a>

__________________________________________________________________________</b></a></b></a>


+--------+
|  TAGS  |
+--------+
adorable animals cup cute "indoor pets" pavic pets photographer pig puddle tea teacup "teacup pig" vjeran vjeranpavic 