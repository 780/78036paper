+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Helgi Halldórsson/Freddi
Photo URL    : https://www.flickr.com/photos/8058853@N06/6783121620/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 24 14:32:05 GMT+0100 2012
Upload Date  : Sat Feb 25 21:01:19 GMT+0100 2012
Views        : 4,095
Comments     : 7


+---------+
|  TITLE  |
+---------+
Sweet little pig


+---------------+
|  DESCRIPTION  |
+---------------+
The night of 7th February Gauthild sows Gilitrutt pigs in a pig sty in the Reykjavik Zoo.The birth went well but this is the first litter sow prepared to take their pigs unfortunately not too well and they get milk to drink from a bottle on a regular basis around the clock. Dry milk is not the same as for infants, but specific pig milk.


+--------+
|  TAGS  |
+--------+
"Reykjavik Zoo & Family Park" Iceland Reykjavík 