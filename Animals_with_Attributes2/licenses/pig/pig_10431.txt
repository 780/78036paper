+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eamoncurry123
Photo URL    : https://www.flickr.com/photos/eamoncurry/14446202242/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 16 14:54:53 GMT+0200 2014
Upload Date  : Wed Jun 18 00:57:22 GMT+0200 2014
Views        : 618
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pigs/Piglets near Wardlow, Derbyshire


+---------------+
|  DESCRIPTION  |
+---------------+
Piglets and Pigs on a path coming from the village of Wardlow in Derbyshire. Photo Taken: 16/06/2014


+--------+
|  TAGS  |
+--------+
DERBYSHIRE WARDLOW PUBLIC FOOTPATH "PUBLIC FOOTPATH" PIGS PIGLETS ANIMALS 