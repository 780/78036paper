+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Travis S.
Photo URL    : https://www.flickr.com/photos/baggis/4345495636/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 19 12:36:43 GMT+0200 2009
Upload Date  : Wed Feb 10 05:38:34 GMT+0100 2010
Geotag Info  : Latitude:32.736046, Longitude:-117.152935
Views        : 656
Comments     : 0


+---------+
|  TITLE  |
+---------+
Visayan Warty Pig


+---------------+
|  DESCRIPTION  |
+---------------+
This warty pig comes from the neighboring Philippine Islands of <a href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=negros,+philippines&amp;sll=37.0625,-95.677068&amp;sspn=34.038806,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Negros,+Enrique+B.+Magalona,+Negros+Occidental,+Western+Visayas,+Philippines&amp;ll=11.566144,122.871094&amp;spn=20.938687,43.286133&amp;t=h&amp;z=5" rel="nofollow">Negros</a> and <a href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Panay,+philippines&amp;sll=11.566144,122.871094&amp;sspn=20.938687,43.286133&amp;ie=UTF8&amp;hq=&amp;hnear=Panay,+Provincial+Rd,+Calinog,+Iloilo,+Western+Visayas,+Philippines&amp;ll=11.307708,122.563477&amp;spn=20.957414,43.286133&amp;t=h&amp;z=5" rel="nofollow">Panay</a>. They live in a tropical grassland and look rather tasty.

They are critically endangered due to their being excessively hunted and the fact that they are losing their habitat. It must be difficult being endemic to two small islands.


+--------+
|  TAGS  |
+--------+
California "San Diego" "San Diego Zoo" zoo "Sus cebifrons" "Visayan Warty Pig" pig grazing 