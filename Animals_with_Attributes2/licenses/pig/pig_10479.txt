+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daniel_sh
Photo URL    : https://www.flickr.com/photos/photodaniel/5525093535/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 13 14:47:20 GMT+0100 2011
Upload Date  : Mon Mar 14 08:23:02 GMT+0100 2011
Geotag Info  : Latitude:53.442225, Longitude:9.887738
Views        : 2,082
Comments     : 12


+---------+
|  TITLE  |
+---------+
Pot-bellied pig


+---------------+
|  DESCRIPTION  |
+---------------+
Wildpark Schwarze Berge near Hamburg. There were quite a handful of these pigs, and a lot of visitors eager to spend their money to feed them.


+--------+
|  TAGS  |
+--------+
ds "Wildpark Schwarze Berge" pig zoo "Pot-bellied pig" schwein hängebauchschwein portrait porträt face gesicht 