+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Alex Kehr
Photo URL    : https://www.flickr.com/photos/alexkehr/196911964/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 17 13:19:37 GMT+0200 2006
Upload Date  : Mon Jul 24 09:53:53 GMT+0200 2006
Geotag Info  : Latitude:-17.644022, Longitude:177.758789
Views        : 1,142
Comments     : 2


+---------+
|  TITLE  |
+---------+
Flying Pig


+---------------+
|  DESCRIPTION  |
+---------------+
A flying pig!


+--------+
|  TAGS  |
+--------+
Fiji "Global Works Inc." "Community Service" "Flying Pig" 