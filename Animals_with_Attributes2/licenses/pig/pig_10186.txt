+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jim Mullhaupt
Photo URL    : https://www.flickr.com/photos/jimpic/10461564674/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 6 12:50:27 GMT+0200 2013
Upload Date  : Thu Oct 24 19:44:41 GMT+0200 2013
Views        : 1,947
Comments     : 0


+---------+
|  TITLE  |
+---------+
Little Pigs


+---------------+
|  DESCRIPTION  |
+---------------+
Next years bacon for my Daughter and family.  Warren, Pa.


+--------+
|  TAGS  |
+--------+
Pig swine Suidae hog farm animal Warren Pennsylvania mullhaupt "Jim Mullhaupt" flickr 