+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : International Livestock Research Institute
Photo URL    : https://www.flickr.com/photos/ilri/4729702674/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 15 10:41:00 GMT+0200 2007
Upload Date  : Thu Jun 24 07:53:00 GMT+0200 2010
Geotag Info  : Latitude:39.906009, Longitude:116.387908
Views        : 2,576
Comments     : 0


+---------+
|  TITLE  |
+---------+
Small-scale pig farming outside Beijing


+---------------+
|  DESCRIPTION  |
+---------------+
Liu Guiyang, of Shunyi County, a farming area near Beijing, runs a small-scale pig farm, the profits of which have educated her three daughters, who now live and work in white-colalr jobs in Beijing. Here Liu is cleaning out the piglet stalls (photo credit: ILRI/Mann).


+--------+
|  TAGS  |
+--------+
ILRI China Pigs 