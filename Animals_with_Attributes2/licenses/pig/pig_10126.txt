+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Turkinator
Photo URL    : https://www.flickr.com/photos/ianturk/14724003400/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 8 11:20:04 GMT+0200 2014
Upload Date  : Wed Aug 13 23:45:11 GMT+0200 2014
Views        : 794
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ginger Saddleback


+---------------+
|  DESCRIPTION  |
+---------------+
Piglet - The South of England Rare Breeds Centre


+--------+
|  TAGS  |
+--------+
Pig "The South of England Rare Breeds Centre" ginger saddleback 