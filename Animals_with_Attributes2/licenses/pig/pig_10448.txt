+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : angavallen
Photo URL    : https://www.flickr.com/photos/angavallen/3408728865/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 7 14:11:03 GMT+0200 2007
Upload Date  : Fri Apr 3 15:53:20 GMT+0200 2009
Geotag Info  : Latitude:55.441089, Longitude:13.031330
Views        : 1,888
Comments     : 1


+---------+
|  TITLE  |
+---------+
Pigs


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Pigs Grisar Ängavallen Angavallen Vellinge Skåne Scania Sweden Sverige malmotown:is=gourmet 