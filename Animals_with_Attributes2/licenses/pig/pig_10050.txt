+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mirrorlessview
Photo URL    : https://www.flickr.com/photos/hayzphotos/14880468233/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 29 18:17:14 GMT+0200 2014
Upload Date  : Fri Aug 8 18:59:28 GMT+0200 2014
Views        : 746
Comments     : 0


+---------+
|  TITLE  |
+---------+
As happy as pigs in Mud!


+---------------+
|  DESCRIPTION  |
+---------------+
This photo was taken on 29th July 2014 with an olympus OM-D E-M10 at Jacob's Inn, Wolvercote


+--------+
|  TAGS  |
+--------+
pig Olympus Oxford wolvercote "jacob's inn" omd E-M10 