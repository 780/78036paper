+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Elagro
Photo URL    : https://www.flickr.com/photos/homestyle/108040912/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 5 12:18:36 GMT+0100 2006
Upload Date  : Sun Mar 5 12:18:36 GMT+0100 2006
Views        : 5,890
Comments     : 13


+---------+
|  TITLE  |
+---------+
pig


+---------------+
|  DESCRIPTION  |
+---------------+
I took this shot in Krakow, Poland in 1989. It wasn't a train but a truck going by. Was very sad for me too.


+--------+
|  TAGS  |
+--------+
pig transport 