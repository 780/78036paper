+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kelmon
Photo URL    : https://www.flickr.com/photos/kelmon/10135309583/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 6 15:30:26 GMT+0200 2013
Upload Date  : Mon Oct 7 12:17:24 GMT+0200 2013
Views        : 1,013
Comments     : 1


+---------+
|  TITLE  |
+---------+
Day 279 - Piggin Marvellous


+---------------+
|  DESCRIPTION  |
+---------------+
So this week I'm down in London helping my sister move house and have now seen the new place.  It's in Colliers Wood (South Wimbledon, if you can get away with that) and it's got some nice little places around it in which to enjoy the lovely weather that appeared today.  One of those places was Deen City Farm, which seems to be a great place to take the kids with various examples of farm animals, including a cracking selection of chicken breeds.  While my preference would have been to take shots of the chickens (note: I think chickens are fab) the cage wire meant that the shots wouldn't have looked good.  So in the end this shot was taken of their pigs as they were sleeping in their enclosure as they perked up a bit.  I had tried for this shot a bit earlier, for which I was changing my lens to the 70-200mm f/2.8 but someone decided to get the pigs moving by throwing some banana into the enclosure, which was quite annoying.

Post-processing here has been entirely done in Lightroom, which has mostly revolved around pulling back the foreground exposure and adding some clarity to bring out the hair on the pigs.


+--------+
|  TAGS  |
+--------+
Agriculture Animals Attention Color Colour Pair Pig Pigs "one a day" "photo a day" "picture a day" "project 365" project365-06oct13 project365-100613 project365-279 "Colliers Wood" "Deen City Farm" London 