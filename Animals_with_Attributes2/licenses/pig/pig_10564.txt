+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mooglove
Photo URL    : https://www.flickr.com/photos/mooglove/3300282578/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Feb 19 15:32:32 GMT+0100 2009
Upload Date  : Sun Feb 22 14:02:47 GMT+0100 2009
Views        : 109
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiny Piglets


+---------------+
|  DESCRIPTION  |
+---------------+
The weird lighting is due to the heat lamp.  These little piglets were only about a foot long.  Subway sized ;)


+--------+
|  TAGS  |
+--------+
pigs S5700 