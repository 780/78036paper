+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : CAFNR
Photo URL    : https://www.flickr.com/photos/cafnr/14205198487/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 8 18:43:05 GMT+0200 2014
Upload Date  : Tue Jun 10 17:59:54 GMT+0200 2014
Views        : 366
Comments     : 0


+---------+
|  TITLE  |
+---------+
Swine Photos_06092014_0001


+---------------+
|  DESCRIPTION  |
+---------------+
6 week-old swine and hogs at MU South Farm's Swine Teaching Center.              

Photo by Morgan Lieberman | © 2014 - Curators of the University of Missouri


+--------+
|  TAGS  |
+--------+
swine farm mizzou cafnr hogs pigs newborn research 