+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Farm Sanctuary
Photo URL    : https://www.flickr.com/photos/farmsanctuary1/2689348365/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 11 08:10:12 GMT+0200 2008
Upload Date  : Mon Jul 21 20:48:05 GMT+0200 2008
Views        : 848
Comments     : 0


+---------+
|  TITLE  |
+---------+
Brave survivors of the Midwest floods.


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Farm Sanctuary" pigs midwest flood rescue vegan vegetarian "pork production" "gestation crates" 