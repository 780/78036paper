+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Peter O'Connor aka anemoneprojectors
Photo URL    : https://www.flickr.com/photos/anemoneprojectors/8746143629/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 11 14:13:01 GMT+0200 2013
Upload Date  : Fri May 17 09:51:01 GMT+0200 2013
Geotag Info  : Latitude:51.913300, Longitude:-0.258428
Views        : 1,209
Comments     : 0


+---------+
|  TITLE  |
+---------+
GOC St Paul's Walden 043: Piglet


+---------------+
|  DESCRIPTION  |
+---------------+
Pigs are like a tube on legs.

Hertfordshire GOC's 11 May 2013 walk, which was a circular walk starting in st Paul's Walden and visiting Whitwell, Rusling End, Langley, Chapelfoot and Hitch Wood. Please check out the other photos from the walk <a href="http://www.flickr.com/photos/anemoneprojectors/sets/72157633461190969">here</a>.


+--------+
|  TAGS  |
+--------+
2013 Animal Animalia Artiodactyl Artiodactyla Chapelfoot Chordata Chordate Domestic "Domestic pig" Domesticated England Farm "Gay Outdoor Club" GOC "GOC Hertfordshire" "GOC St Paul's Walden" Hertfordshire "Hertfordshire GOC" Hog Kodak Mammal Mammalia Oink Pig Piglet Suidae Sus "Sus domesticus" "Sus scrofa" "Sus scrofa domesticus" Swine Z981 "Kodak Easyshare Z981" 