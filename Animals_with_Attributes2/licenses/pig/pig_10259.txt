+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Bobcatnorth
Photo URL    : https://www.flickr.com/photos/bobcatnorth/3014959006/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 8 10:10:44 GMT+0100 2008
Upload Date  : Sun Nov 9 05:48:50 GMT+0100 2008
Views        : 1,240
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pig at Royal Winter Fair


+---------------+
|  DESCRIPTION  |
+---------------+
The Royal Winter Fair is the largest agricultural fair in Canada.  According to Wikipedia the 2007 fair featured &quot;over 3,000 head of cattle, 1,300 horses, 1,600 birds, 500 sheep, 300 goats and 300 pigs&quot;.  <a href="http://www.royalfair.org/" rel="nofollow">Royal Winter Fair website</a>  
<a href="http://www.youtube.com/watch?v=cXE-rvJvjTU" rel="nofollow">Youtube video of Royal Winter Fair Highlights</a>


+--------+
|  TAGS  |
+--------+
"royal winter fair" toronto ontario 2008 pig pink 