+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : International Livestock Research Institute
Photo URL    : https://www.flickr.com/photos/ilri/4577585310/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 4 08:34:17 GMT+0200 2010
Upload Date  : Tue May 4 07:34:17 GMT+0200 2010
Geotag Info  : Latitude:26.116899, Longitude:94.284217
Views        : 4,112
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pig breeding in India's Nagaland


+---------------+
|  DESCRIPTION  |
+---------------+
Pig breeding on a farm in Dimapur, Nagaland, India (photo credit: ILRI/Mann).


+--------+
|  TAGS  |
+--------+
India Nagaland ILRI Pigs 