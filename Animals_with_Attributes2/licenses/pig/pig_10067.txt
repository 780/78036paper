+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike Prince
Photo URL    : https://www.flickr.com/photos/mikeprince/14134089736/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 15 12:18:59 GMT+0100 2014
Upload Date  : Sun May 11 07:02:57 GMT+0200 2014
Views        : 1,830
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bornean Bearded Pig


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Bornean Bearded Pig" Borneo "Kinabatangan Jungle Camp" "Kinabatangan Wildlife Sanctuary" Malaysia Mammalia Mammals Pigs Sabah "Sus barbatus" 