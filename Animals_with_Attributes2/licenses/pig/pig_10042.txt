+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BecA-ILRI Hub
Photo URL    : https://www.flickr.com/photos/beca-hub/9612383109/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 22 08:55:43 GMT+0200 2013
Upload Date  : Wed Aug 28 15:08:08 GMT+0200 2013
Views        : 1,114
Comments     : 0


+---------+
|  TITLE  |
+---------+
Agricultural Research Connections (ARC) 2013 - Kariuki Farm, Kiambu County, Kenya


+---------------+
|  DESCRIPTION  |
+---------------+
Pig farming at Kariuki
Photo Credit: BecA-ILRI/Tim Hall


+--------+
|  TAGS  |
+--------+
(no tags)