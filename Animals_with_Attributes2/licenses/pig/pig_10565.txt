+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bgolub
Photo URL    : https://www.flickr.com/photos/benjamingolub/1855416319/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 3 15:15:10 GMT+0100 2007
Upload Date  : Sun Nov 4 14:11:57 GMT+0100 2007
Geotag Info  : Latitude:43.155781, Longitude:-77.615908
Views        : 34
Comments     : 0


+---------+
|  TITLE  |
+---------+
Trotting Pig


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"lollypop farm" pig 