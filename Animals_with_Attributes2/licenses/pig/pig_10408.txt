+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Farm Sanctuary
Photo URL    : https://www.flickr.com/photos/farmsanctuary1/2162688813/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 3 10:28:31 GMT+0100 2008
Upload Date  : Thu Jan 3 19:07:32 GMT+0100 2008
Views        : 4,408
Comments     : 1


+---------+
|  TITLE  |
+---------+
Pigs behind bars


+---------------+
|  DESCRIPTION  |
+---------------+
Factory farm pigs are typically raised in small pens with slatted or concrete floors and metal bars.

(Feel free to distribute freely for not-for-profit use, but please credit Farm Sanctuary.  If you are media and are in need of a high-resolution version of this image, please contact us at media@farmsanctuary.org and request the file &quot;pens5_300_1&quot;.)


+--------+
|  TAGS  |
+--------+
"animal rights" "animal welfare" animals environmentalism "factory farming" sustainability vegan veganism vegetarian vegetarianism pork pigs "gestation crates" "farrowing crates" 