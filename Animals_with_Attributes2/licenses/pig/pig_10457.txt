+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mercyforanimals
Photo URL    : https://www.flickr.com/photos/mercyforanimals/5655183892/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 25 15:22:49 GMT+0200 2011
Upload Date  : Mon Apr 25 22:22:49 GMT+0200 2011
Views        : 4,689
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pigs


+---------------+
|  DESCRIPTION  |
+---------------+
Pigs are social animals and prefer to live in family groups or herds of up to 10 individuals.


+--------+
|  TAGS  |
+--------+
(no tags)