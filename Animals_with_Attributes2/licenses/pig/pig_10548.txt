+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : UnitedSoybeanBoard
Photo URL    : https://www.flickr.com/photos/unitedsoybean/9621247493/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 24 13:28:40 GMT+0200 2013
Upload Date  : Thu Aug 29 19:43:39 GMT+0200 2013
Views        : 875
Comments     : 0


+---------+
|  TITLE  |
+---------+
Swine


+---------------+
|  DESCRIPTION  |
+---------------+
If used, credit must be given to the United Soybean Board or the Soybean Checkoff.


+--------+
|  TAGS  |
+--------+
ag agriculture animal pigs swine hog hogs horizontal 