+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nicholas Smale
Photo URL    : https://www.flickr.com/photos/nicholassmale/4055314000/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Oct 28 07:30:56 GMT+0100 2009
Upload Date  : Thu Oct 29 09:26:35 GMT+0100 2009
Views        : 36
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pig


+---------------+
|  DESCRIPTION  |
+---------------+
Photo by Emma Cleverly


+--------+
|  TAGS  |
+--------+
(no tags)