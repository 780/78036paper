+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : minds-eye
Photo URL    : https://www.flickr.com/photos/craigoneal/6576501171/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 23 21:19:19 GMT+0100 2011
Upload Date  : Mon Dec 26 20:11:36 GMT+0100 2011
Views        : 6,457
Comments     : 9


+---------+
|  TITLE  |
+---------+
Feral Pig and Piglets


+---------------+
|  DESCRIPTION  |
+---------------+
Photographed in the GTMNERR Wildlife Mgmt Area. A mother boar (150lbs est) with a couple of piglets. Photo by: Craig O'Neal


+--------+
|  TAGS  |
+--------+
pig piglets boar "feral pigs" wildlife hog florida swamps pork hunting gtmnerr guana 