+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brotherM
Photo URL    : https://www.flickr.com/photos/michaelcr/5797087585/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 4 13:32:30 GMT+0200 2011
Upload Date  : Sat Jun 4 21:38:12 GMT+0200 2011
Views        : 1,742
Comments     : 2


+---------+
|  TITLE  |
+---------+
June 4, 2011


+---------------+
|  DESCRIPTION  |
+---------------+
Happy pigs at Weber Farm, Lamoine, Maine.


+--------+
|  TAGS  |
+--------+
pig happy farm sty "Weber Farm" Maine Lamoine project365 cute animal "farm animal" 