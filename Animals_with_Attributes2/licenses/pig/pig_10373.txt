+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Oct 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tim Green aka atoach
Photo URL    : https://www.flickr.com/photos/atoach/9187263276/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 1 18:30:09 GMT+0200 2013
Upload Date  : Mon Jul 1 21:54:47 GMT+0200 2013
Geotag Info  : Latitude:53.776793, Longitude:-1.855616
Views        : 4,838
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pig


+---------------+
|  DESCRIPTION  |
+---------------+
Over 16,000 pictures and this is my first pig shot.


+--------+
|  TAGS  |
+--------+
pig cochon schwein boar queensbury pork bradford yorkshire 