+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ruth and Dave
Photo URL    : https://www.flickr.com/photos/ruthanddave/53089290/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 16 15:09:46 GMT+0200 2005
Upload Date  : Sun Oct 16 21:36:38 GMT+0200 2005
Geotag Info  : Latitude:51.853488, Longitude:-0.546398
Views        : 341
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pygmy hippo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
hippos hippopotamus whipsnade zoos pygmy 