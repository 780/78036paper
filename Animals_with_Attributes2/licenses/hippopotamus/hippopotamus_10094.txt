+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brainstorm1984
Photo URL    : https://www.flickr.com/photos/brainstorm1984/11025862346/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 16 08:11:02 GMT+0100 2013
Upload Date  : Sun Nov 24 12:12:29 GMT+0100 2013
Geotag Info  : Latitude:-19.194095, Longitude:23.363069
Views        : 1,724
Comments     : 5


+---------+
|  TITLE  |
+---------+
Flusspferd / Hippopotamus


+---------------+
|  DESCRIPTION  |
+---------------+
Ein Flusspferd im Moremi-Wildreservat (Okavangodelta, Botswana).

A Hippo in the Moremi Game Reserve (Okavango Delta, Botswana).


+--------+
|  TAGS  |
+--------+
"Camp Moremi" "Dead Tree Island" Flusspferd Hippo Hippopotamus "Hippopotamus amphibius" "Moremi Game Reserve" Moremi-Wildreservat Nilpferd "Okavango Delta" Safari Wildlife Großflusspferd Botswana Nordwest Botsuana "Desert & Delta Safaris" "Elangeni African Adventures" 