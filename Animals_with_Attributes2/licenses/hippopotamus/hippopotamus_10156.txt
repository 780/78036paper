+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : oliver.dodd
Photo URL    : https://www.flickr.com/photos/oliverdodd/5146429184/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 29 02:42:45 GMT+0200 2010
Upload Date  : Thu Nov 4 18:22:23 GMT+0100 2010
Views        : 224
Comments     : 0


+---------+
|  TITLE  |
+---------+
hippo face


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
africa tanzania safari serengeti hippo hippopotamus 