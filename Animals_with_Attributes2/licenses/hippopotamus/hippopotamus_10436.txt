+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : berniedup
Photo URL    : https://www.flickr.com/photos/berniedup/16467433986/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 13 10:56:08 GMT+0100 2015
Upload Date  : Tue Feb 10 18:09:11 GMT+0100 2015
Geotag Info  : Latitude:-24.981312, Longitude:31.566252
Views        : 650
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippos (Hippopotamus amphibius)


+---------------+
|  DESCRIPTION  |
+---------------+
Lake Panic, Skukuza, Kruger NP, SOUTH AFRICA


+--------+
|  TAGS  |
+--------+
Hippo "Hippopotamus amphibius" Kruger "Lake Panic" Skukuza "South Africa" "taxonomy:binomial=Hippopotamus amphibius" 