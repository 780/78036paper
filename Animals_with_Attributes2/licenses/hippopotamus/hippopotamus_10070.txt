+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mobikerbear
Photo URL    : https://www.flickr.com/photos/mobikerbear/14482943415/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 22 13:29:57 GMT+0200 2014
Upload Date  : Sun Jun 22 21:24:24 GMT+0200 2014
Views        : 69
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC00521


+---------------+
|  DESCRIPTION  |
+---------------+
Flusspferd - Hippopotamus amphibius - Hippopotamus


+--------+
|  TAGS  |
+--------+
2014 Köln "Kölner Zoo" 