+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jess & Kate
Photo URL    : https://www.flickr.com/photos/jessicapope/3155316520/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 19 14:40:33 GMT+0100 2008
Upload Date  : Thu Jan 1 03:22:56 GMT+0100 2009
Views        : 29
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippopotamus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Masai Mara" 