+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mobikerbear
Photo URL    : https://www.flickr.com/photos/mobikerbear/13826257764/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 12 16:02:49 GMT+0200 2014
Upload Date  : Sun Apr 13 19:52:49 GMT+0200 2014
Views        : 62
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC00301


+---------------+
|  DESCRIPTION  |
+---------------+
Flusspferd - Hippopotamus amphibius - Hippopotamus


+--------+
|  TAGS  |
+--------+
2014 Cologne "Kölner Zoo" 