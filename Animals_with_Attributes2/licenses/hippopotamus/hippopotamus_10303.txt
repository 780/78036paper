+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Hyougushi
Photo URL    : https://www.flickr.com/photos/hyougushi/3457432109/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 12 13:57:17 GMT+0200 2009
Upload Date  : Mon Apr 20 04:57:02 GMT+0200 2009
Views        : 180
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippopotamus @ Kobe City Oji Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(2009/04/12 Kobe, Hyogo, JAPAN)


+--------+
|  TAGS  |
+--------+
桜 Hippopotamus カバ mammal animal zoo 動物園 "Kobe City Oji Zoo" 神戸市立王子動物園 王子動物園 Kobe 神戸 Hyogo 兵庫 Japan 日本 