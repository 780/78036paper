+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Snake3yes
Photo URL    : https://www.flickr.com/photos/snake3yes/229911887/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 19 07:56:24 GMT+0200 2006
Upload Date  : Thu Aug 31 11:16:50 GMT+0200 2006
Views        : 186
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippo


+---------------+
|  DESCRIPTION  |
+---------------+
In the Mara River


+--------+
|  TAGS  |
+--------+
hippo africa kenya "masai mara" wildlife 