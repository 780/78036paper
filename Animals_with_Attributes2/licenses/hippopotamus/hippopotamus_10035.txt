+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jeri gloege
Photo URL    : https://www.flickr.com/photos/fractalmind/2241758208/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Nov 6 00:47:19 GMT+0100 2007
Upload Date  : Mon Feb 4 11:10:06 GMT+0100 2008
Views        : 47
Comments     : 0


+---------+
|  TITLE  |
+---------+
And finally a breath...


+---------------+
|  DESCRIPTION  |
+---------------+
After a long wait, the hippo finally gave me the shot I wanted when he came up to take a breath.

I love the two peeping teeth!
National Zoo
November 2007


+--------+
|  TAGS  |
+--------+
"Washington DC" "National Zoo" hippopotamus 