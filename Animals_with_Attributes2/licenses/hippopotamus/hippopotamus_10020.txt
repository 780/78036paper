+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : 2sirius
Photo URL    : https://www.flickr.com/photos/peterv/6074979287/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 23 13:33:09 GMT+0200 2011
Upload Date  : Wed Aug 24 05:29:32 GMT+0200 2011
Views        : 166
Comments     : 0


+---------+
|  TITLE  |
+---------+
20110823_1182.CR2.500


+---------------+
|  DESCRIPTION  |
+---------------+
Hippopotamus.

More dangerous than it looks...


+--------+
|  TAGS  |
+--------+
Toronto Ontario Canada Meadowvale Zoo hippo hippopotamus 