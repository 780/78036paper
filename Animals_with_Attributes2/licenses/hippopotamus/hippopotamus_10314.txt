+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rameshng
Photo URL    : https://www.flickr.com/photos/rameshng/5560149673/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 29 17:27:20 GMT+0100 2011
Upload Date  : Sat Mar 26 08:54:37 GMT+0100 2011
Views        : 1,083
Comments     : 1


+---------+
|  TITLE  |
+---------+
Hippopotamus at mysore zoo


+---------------+
|  DESCRIPTION  |
+---------------+
The hippopotamus (Hippopotamus amphibius), or hippo, from the ancient Greek for &quot;river horse&quot; , is a large, mostly herbivorous mammal in sub-Saharan Africa, and one of only two extant species in the family Hippopotamidae (the other is the Pygmy Hippopotamus.) The hippopotamus is the third largest land animal (after the elephant and the white rhinoceros) and the heaviest extant artiodactyl, despite being considerably shorter than the giraffe.


+--------+
|  TAGS  |
+--------+
Mysore Zoo Karnataka hippopotamus 