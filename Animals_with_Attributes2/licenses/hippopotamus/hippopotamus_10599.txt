+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kabacchi
Photo URL    : https://www.flickr.com/photos/kabacchi/5447329870/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 15 13:43:26 GMT+0100 2011
Upload Date  : Tue Feb 15 05:43:26 GMT+0100 2011
Views        : 466
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pygmy Hippopotamus - 09


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Pygmy Hippopotamus" コビトカバ Hippopotamus カバ 上野動物園 Animal 動物 Mammalia 哺乳類 "~Pygmy Hippopotamus~" 