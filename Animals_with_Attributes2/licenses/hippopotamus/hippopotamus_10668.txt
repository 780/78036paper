+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/15530479469/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 23 16:47:24 GMT+0200 2014
Upload Date  : Wed Nov 5 13:00:08 GMT+0100 2014
Geotag Info  : Latitude:47.385711, Longitude:8.573412
Views        : 20,791
Comments     : 5


+---------+
|  TITLE  |
+---------+
Walking baby pigmy hippo


+---------------+
|  DESCRIPTION  |
+---------------+
The same baby pigmy hippo walking on the hay...


+--------+
|  TAGS  |
+--------+
walking hay wet shiny profile young baby cute small "pigmy hippopotamus" hippo pigmy hippopotamus zürich zoo switzerland nikon d4 