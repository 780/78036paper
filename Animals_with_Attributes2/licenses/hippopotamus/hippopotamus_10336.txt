+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Numinosity (Gary J Wood)
Photo URL    : https://www.flickr.com/photos/garyjwood/871199927/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 22 13:50:53 GMT+0200 2007
Upload Date  : Sun Jul 22 22:13:16 GMT+0200 2007
Geotag Info  : Latitude:43.817352, Longitude:-79.184163
Views        : 111
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippopotamus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
canada ontario toronto zoos 