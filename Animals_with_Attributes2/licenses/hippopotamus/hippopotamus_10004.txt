+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Allie_Caulfield
Photo URL    : https://www.flickr.com/photos/wm_archiv/2695224716/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Mar 27 13:05:50 GMT+0200 2007
Upload Date  : Wed Jul 23 07:54:51 GMT+0200 2008
Views        : 197
Comments     : 0


+---------+
|  TITLE  |
+---------+
2007-03-24 04-01 Cloppenburg 034 Tierpark Thüle


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Cloppenburg 2007 März Frühling Niedersachsen Tierpark Zoo Thüle Worberg Friesoythe "Freizeitpark Thüle" Talsperre Thülsfelde Thülsfelder Zwergflusspferd Nilpferd hippo mini baby Foto photo image picture Bild "creative commons" flickr "high resolution" stockphoto free cc 