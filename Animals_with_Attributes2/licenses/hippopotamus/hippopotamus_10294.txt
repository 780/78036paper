+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sussexbirder
Photo URL    : https://www.flickr.com/photos/sussexbirder/8605545933/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 20 00:00:00 GMT+0200 1997
Upload Date  : Sun Mar 31 16:57:00 GMT+0200 2013
Views        : 23
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippopotamus (Hippopotamus amphibious)


+---------------+
|  DESCRIPTION  |
+---------------+
St Lucia, Natal


+--------+
|  TAGS  |
+--------+
(no tags)