+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : NatalieMaynor
Photo URL    : https://www.flickr.com/photos/nataliemaynor/2936177570/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 12 14:47:29 GMT+0200 2008
Upload Date  : Mon Oct 13 00:12:33 GMT+0200 2008
Views        : 317
Comments     : 1


+---------+
|  TITLE  |
+---------+
At the Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Jackson Zoo" "Jackson Zoological Park" zoo Jackson Mississippi hippo hippopotamus "zoos of the South" 