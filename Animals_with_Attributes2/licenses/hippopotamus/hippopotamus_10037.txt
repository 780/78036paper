+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Thomas Rousing Photography
Photo URL    : https://www.flickr.com/photos/thomasrousing/16556712724/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 11 15:19:25 GMT+0200 2015
Upload Date  : Fri Apr 17 18:50:59 GMT+0200 2015
Views        : 2,845
Comments     : 0


+---------+
|  TITLE  |
+---------+
Animals in the ZOO -  Hippos


+---------------+
|  DESCRIPTION  |
+---------------+
Please visit my other sites, for more photography.

<a href="http://www.instagram.com/thomasrousing.dk" rel="nofollow">My squares at Instagram</a>
<a href="http://www.facebook.com/thomasrousing" rel="nofollow">My page at Facebook</a>
<a href="http://www.thomasrousing.dk" rel="nofollow">My cool website</a>

<a href="mailto:photography@thomasrousing.dk" rel="nofollow">Contact me here</a>.

Copenhagen based photographer Thomas Rousing, captures the beauty of everyday life in the city. He seeks to explore the interestingness of making images filled with endless details and beautiful colors.


+--------+
|  TAGS  |
+--------+
Animals Baby Capital Child City Copenaghen Copenhagen Copenhague Danish Danmark Denmark Eating Frederiksberg "Frederiksberg park" Kaupmannahöfn Kopengagen Kopenhaga Koppenhága Köpenhamn Kööpenhamina København "Thomas Rousing" ZOO baboon bavian dansk dinamarca dyr dänemark flodhest giraf giraffes have hippo hippopotamus hovedstad kopenhagen lama llama parents scandinavia seal sealion skandinavien søløve visitcopenhagen visitdenmark www.thomasrousing.dk "zoologisk have" billede fotos fotografi kunst reklame editorial Danimarka 