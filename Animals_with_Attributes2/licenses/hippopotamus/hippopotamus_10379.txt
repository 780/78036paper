+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : russellstreet
Photo URL    : https://www.flickr.com/photos/russellstreet/4065821918/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 9 14:18:53 GMT+0100 2008
Upload Date  : Sun Nov 1 21:49:43 GMT+0100 2009
Views        : 117
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippopotamus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Animal Auckland "Auckland Zoo" Hippopotamus "New Zealand" NZL 