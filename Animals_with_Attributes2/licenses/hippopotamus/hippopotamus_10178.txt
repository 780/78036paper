+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : marfis75
Photo URL    : https://www.flickr.com/photos/marfis75/2909578253/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 3 15:15:19 GMT+0200 2008
Upload Date  : Fri Oct 3 20:20:09 GMT+0200 2008
Views        : 4,651
Comments     : 33


+---------+
|  TITLE  |
+---------+
hippo portrait


+---------------+
|  DESCRIPTION  |
+---------------+
Flusspferd im Opelzoo


+--------+
|  TAGS  |
+--------+
flusspferd hipo nilpferd portrait opelzoo zoo zootier hippo marfis75 animal tier gesicht face hippotamus riverhorse APlusPhoto creativecommons "marfis75 on flickr" cc #CC-BY-SA 