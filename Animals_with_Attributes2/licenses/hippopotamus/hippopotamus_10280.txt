+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lyndi&Jason
Photo URL    : https://www.flickr.com/photos/citnaj/1704987546/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Oct 23 03:26:03 GMT+0200 2007
Upload Date  : Tue Oct 23 09:26:03 GMT+0200 2007
Views        : 619
Comments     : 0


+---------+
|  TITLE  |
+---------+
hippo.JPG


+---------------+
|  DESCRIPTION  |
+---------------+
Hippo  at the San Diego Zoo

<a href="http://www.wegotocoolplaces.com" rel="nofollow">www.wegotocoolplaces.com</a>


+--------+
|  TAGS  |
+--------+
2007 san diego zoo hippo hippopotamus 