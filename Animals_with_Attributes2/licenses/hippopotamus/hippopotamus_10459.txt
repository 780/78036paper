+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Gabriel White
Photo URL    : https://www.flickr.com/photos/zoomzoom/4941498095/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 6 02:22:50 GMT+0200 2010
Upload Date  : Mon Aug 30 17:19:39 GMT+0200 2010
Views        : 491
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippos


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
masai mara kenya animals safari migration hippo 