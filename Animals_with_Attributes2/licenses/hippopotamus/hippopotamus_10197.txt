+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : The Mind's Eye Photography
Photo URL    : https://www.flickr.com/photos/kristinpia/223229987/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 20 09:57:29 GMT+0200 2005
Upload Date  : Thu Aug 24 01:01:20 GMT+0200 2006
Views        : 81
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Philadelphia Zoo" animals zoo hippo 