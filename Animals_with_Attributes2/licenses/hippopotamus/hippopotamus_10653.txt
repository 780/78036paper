+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : HBarrison
Photo URL    : https://www.flickr.com/photos/hbarrison/7407642666/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 20 11:25:33 GMT+0200 2012
Upload Date  : Wed Jun 20 15:09:45 GMT+0200 2012
Geotag Info  : Latitude:-25.035423, Longitude:31.721420
Views        : 464
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tinga_2012 05 20_1080


+---------------+
|  DESCRIPTION  |
+---------------+
The hippopotamus (Hippopotamus amphibius), from the ancient Greek for &quot;river horse&quot;, is a large, mostly herbivorous mammal in sub-Saharan Africa, and one of only two extant species in the family Hippopotamidae (the other is the Pygmy Hippopotamus.)


+--------+
|  TAGS  |
+--------+
Africa HBarrison "Harvey Barrison" Tauck Tinga "Kruger National Park" "South Africa" Hippopotamus "Taxonomy:binomial=Hippopotamus amphibius" 