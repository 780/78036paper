+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Chobist
Photo URL    : https://www.flickr.com/photos/chobis/8154613140/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 28 12:14:09 GMT+0100 2012
Upload Date  : Sun Nov 4 18:45:12 GMT+0100 2012
Views        : 569
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bull Hippopotamus


+---------------+
|  DESCRIPTION  |
+---------------+
I clicked this picture at the City Zoo.. the Bull Hippopotamus having a bite at the iron door as if practising the male toothing.. as naturally in the wild within the bulls .. though a little dissapointed as i could not zoom in with the lens as that gave a lot of noise .. :)


+--------+
|  TAGS  |
+--------+
Hippopotamus "Bull Hippopotamus" Animals "Herbivorous Animal" 