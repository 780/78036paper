+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : César Rincón
Photo URL    : https://www.flickr.com/photos/crincon/971002728/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 22 15:39:46 GMT+0100 2005
Upload Date  : Wed Aug 1 07:58:50 GMT+0200 2007
Geotag Info  : Latitude:20.725346, Longitude:-103.308112
Views        : 262
Comments     : 0


+---------+
|  TITLE  |
+---------+
20050122153946-0


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
hippopotamus hippo zoo 