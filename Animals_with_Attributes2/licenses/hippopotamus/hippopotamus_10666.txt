+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : HBarrison
Photo URL    : https://www.flickr.com/photos/hbarrison/7421834180/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 23 16:38:05 GMT+0200 2012
Upload Date  : Fri Jun 22 22:41:18 GMT+0200 2012
Geotag Info  : Latitude:-17.929960, Longitude:25.842876
Views        : 1,517
Comments     : 0


+---------+
|  TITLE  |
+---------+
Victoria Falls_2012 05 23_1419


+---------------+
|  DESCRIPTION  |
+---------------+
Sunset Cruise on the Zambezi River  /  meeting the Hippos


+--------+
|  TAGS  |
+--------+
Africa HBarrison "Harvey Barrison" Tauck "Victoria Falls" Zimbabwe "Zambezi River" Mosi-oa-Tunya hippopotamus Hippo "river horse" "Taxonomy:binomial=Hippopotamus amphibius" 