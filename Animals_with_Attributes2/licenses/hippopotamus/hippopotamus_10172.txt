+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : amanderson2
Photo URL    : https://www.flickr.com/photos/amanderson/4685750405/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 29 01:16:35 GMT+0200 2010
Upload Date  : Wed Jun 9 22:44:40 GMT+0200 2010
Views        : 442
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippo Pool Serengeti 9


+---------------+
|  DESCRIPTION  |
+---------------+
Closeup hippo head.


+--------+
|  TAGS  |
+--------+
Tanzania safari hippo serengeti 