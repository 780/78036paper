+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hobgadlng
Photo URL    : https://www.flickr.com/photos/hobgadlng/7766585332/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 19 17:17:17 GMT+0200 2012
Upload Date  : Sun Aug 12 19:31:09 GMT+0200 2012
Geotag Info  : Latitude:-17.789064, Longitude:25.192165
Views        : 149
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Botswana Chobe animal hippo hippopotamus 