+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : collinj
Photo URL    : https://www.flickr.com/photos/collinj/28193772/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jan 6 11:35:32 GMT+0100 2003
Upload Date  : Sun Jul 24 17:09:12 GMT+0200 2005
Views        : 383
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hungry hungry hippos


+---------------+
|  DESCRIPTION  |
+---------------+
It's not entirely clear what they're eating though.


+--------+
|  TAGS  |
+--------+
kenya safari wildlife africa hippo 