+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : QuakeUp!
Photo URL    : https://www.flickr.com/photos/quakeup/8872559609/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 8 22:18:17 GMT+0100 2013
Upload Date  : Wed May 29 02:43:18 GMT+0200 2013
Geotag Info  : Latitude:-1.839134, Longitude:34.942474
Views        : 493
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tanzania-Serengeti-MbuziMaweSafariDrive-46


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Serengeti National Park" Tanzania hippopotamus safari "Lake Masek" "lesser flamingo" d7000 Nikon 