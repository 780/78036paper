+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Leandro's World Tour
Photo URL    : https://www.flickr.com/photos/leandrociuffo/10952713505/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 16 05:03:15 GMT+0100 2013
Upload Date  : Wed Nov 20 01:05:43 GMT+0100 2013
Geotag Info  : Latitude:-1.551308, Longitude:30.680694
Views        : 1,165
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippopotamus


+---------------+
|  DESCRIPTION  |
+---------------+
Akagera Park, Rwanda


+--------+
|  TAGS  |
+--------+
Akagera animal Africa Ruanda Rwanda Hipo hipopótamo 