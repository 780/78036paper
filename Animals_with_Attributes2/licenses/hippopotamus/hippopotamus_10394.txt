+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike's Birds
Photo URL    : https://www.flickr.com/photos/pazzani/6593659839/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 9 22:42:11 GMT+0100 2011
Upload Date  : Thu Dec 29 13:25:12 GMT+0100 2011
Geotag Info  : Latitude:-24.796187, Longitude:31.498266
Views        : 159
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippopotamus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)