+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Stig Nygaard
Photo URL    : https://www.flickr.com/photos/stignygaard/2455968367/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 12 09:12:20 GMT+0100 2007
Upload Date  : Thu May 1 13:39:25 GMT+0200 2008
Geotag Info  : Latitude:-2.296643, Longitude:34.808464
Views        : 3,662
Comments     : 1


+---------+
|  TITLE  |
+---------+
Hippo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2007 Africa "Canon EF 70-300mm f/4-5.6 IS USM" "East Africa" "Eastern Africa" hippo hippopotamus "Hippopotamus amphibius" nature "Retima Pool" Safari Serengeti Seronera Tanzania wildlife "Photo by Stig Nygaard" "Creative Commons" 400D "Canon EOS 400D" "Serengeti National Park" "UNESCO World Heritage Site" "World Heritage Site" 