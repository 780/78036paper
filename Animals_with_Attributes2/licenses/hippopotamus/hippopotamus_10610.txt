+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Maria Keays
Photo URL    : https://www.flickr.com/photos/maria_keays/3072618024/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 28 15:33:24 GMT+0100 2008
Upload Date  : Mon Dec 1 00:05:25 GMT+0100 2008
Views        : 320
Comments     : 2


+---------+
|  TITLE  |
+---------+
Pygmy Hippopotamus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"edinburgh zoo" "pygmy hippopotamus" hippo 