+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : addedentry
Photo URL    : https://www.flickr.com/photos/addedentry/2970216790/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 24 13:28:50 GMT+0200 2008
Upload Date  : Fri Oct 24 22:04:42 GMT+0200 2008
Geotag Info  : Latitude:52.509210, Longitude:13.336404
Views        : 119
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hippopotamus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
hippos "Berlin Zoo" Berlin Germany 