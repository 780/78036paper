+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : marfis75
Photo URL    : https://www.flickr.com/photos/marfis75/548716296/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 7 15:30:20 GMT+0200 2007
Upload Date  : Thu Jun 14 21:19:14 GMT+0200 2007
Geotag Info  : Latitude:50.116175, Longitude:8.702030
Views        : 298
Comments     : 0


+---------+
|  TITLE  |
+---------+
Flusspferd Frankfurt


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
flusspferd nilpferd hipoZoo "zoologischer garten" Frankfurt tiere animal animals hessen germany deutschland tier innenstadt city marfis75 creativecommons "marfis75 on flickr" cc 