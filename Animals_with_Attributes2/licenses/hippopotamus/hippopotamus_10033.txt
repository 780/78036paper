+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 04, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Corvair Owner
Photo URL    : https://www.flickr.com/photos/joeross/665484038/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 4 04:19:54 GMT+0100 2007
Upload Date  : Sat Jun 30 02:52:09 GMT+0200 2007
Views        : 547
Comments     : 0


+---------+
|  TITLE  |
+---------+
AHI Treasures of Southern Africa 3-07 0887 N


+---------------+
|  DESCRIPTION  |
+---------------+
A face only a mother could love.  Hippos in the Chobe River, Chobe National Park, Botswana, Africa.


+--------+
|  TAGS  |
+--------+
africa botswana hippo hippopotamus chobe "chobe national park" animals boat tour 2007 ahi african 