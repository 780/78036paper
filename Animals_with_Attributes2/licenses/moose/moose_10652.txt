+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Efraimstochter
Photo URL    : https://pixabay.com/en/elk-park-moose-enclosure-sweden-905570/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : Aug. 16, 2015
Uploaded     : 11 months ago

+--------+
|  TAGS  |
+--------+
Elk Park, Moose, Enclosure, Sweden, Mammal, Animal