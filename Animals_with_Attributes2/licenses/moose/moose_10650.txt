+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : NadinLisa
Photo URL    : https://pixabay.com/en/moose-nature-park-scotland-animal-1520790/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : July 10, 2016
Uploaded     : 6 days ago

+--------+
|  TAGS  |
+--------+
Moose, Nature, Park, Scotland, Animal, Wildlife Park