+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Boylan Mike, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/deers/moose-and-elk/grazing-bull-moose
License      : public domain (CC0)
Uploaded     : 2015-01-06 07:18:21

+--------+
|  TAGS  |
+--------+
grazing, bull, moose