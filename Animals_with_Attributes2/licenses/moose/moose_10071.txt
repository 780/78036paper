+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ajburcar
Photo URL    : https://www.flickr.com/photos/ajburcarannis/4646133861/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 27 16:56:14 GMT+0200 2010
Upload Date  : Fri May 28 05:45:29 GMT+0200 2010
Views        : 131
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chocolate Moose-little posturing


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Moose Minnesota Nature 