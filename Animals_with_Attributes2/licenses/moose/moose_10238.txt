+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : TuckerH586
Photo URL    : https://www.flickr.com/photos/60314725@N00/5484573814/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 20 17:19:03 GMT+0100 2011
Upload Date  : Mon Feb 28 04:20:52 GMT+0100 2011
Views        : 695
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose Family


+---------------+
|  DESCRIPTION  |
+---------------+
Moose take the easy way - through the river. The snow banks here along the Galaitin River were 4-6 feet deep.


+--------+
|  TAGS  |
+--------+
Yellowstone "Moose Yellowstone Winter" 