+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jhoc
Photo URL    : https://www.flickr.com/photos/jhoc/2823664310/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 1 14:29:26 GMT+0200 2008
Upload Date  : Wed Sep 3 03:36:56 GMT+0200 2008
Views        : 7,017
Comments     : 5


+---------+
|  TITLE  |
+---------+
moose crossing


+---------------+
|  DESCRIPTION  |
+---------------+
Young moose crossing the road in Cape Breton Highlands National Park, Canada.

Explore September 2, 2008.


+--------+
|  TAGS  |
+--------+
moose road mountain crossing animal wildlife nature canada "nova scotia" park "national park" "cape breton" highlands jeremyhockin.com 