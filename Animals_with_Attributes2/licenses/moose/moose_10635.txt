+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Ruediger_Jung
Photo URL    : https://pixabay.com/en/moose-calf-moose-moose-child-373135/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : July 31, 2013
Uploaded     : June 20, 2014

+--------+
|  TAGS  |
+--------+
Moose Calf, Moose, Moose Child, Young Animal, Young