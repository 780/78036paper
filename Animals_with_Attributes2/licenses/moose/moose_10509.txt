+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Anthony J
Photo URL    : https://www.flickr.com/photos/ajoch/250913588/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 12 16:12:16 GMT+0200 2006
Upload Date  : Sun Sep 24 03:36:52 GMT+0200 2006
Views        : 474
Comments     : 0


+---------+
|  TITLE  |
+---------+
MOOSE!!!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Cape Breton" "Nova Scotia" "Cabot Trail" moose 