+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Joopey
Photo URL    : https://www.flickr.com/photos/joopey/128310377/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 31 18:17:15 GMT+0200 2006
Upload Date  : Fri Apr 14 12:15:28 GMT+0200 2006
Geotag Info  : Latitude:61.395506, Longitude:13.068709
Views        : 3,642
Comments     : 6


+---------+
|  TITLE  |
+---------+
Moose Warning


+---------------+
|  DESCRIPTION  |
+---------------+
These fellows turned up when we were busy sipping beer in the sauna. Dalarna, Sweden.


+--------+
|  TAGS  |
+--------+
moose elk sweden snow dalarna hut tree winter nikon d70 d70s freezing cold frostbites 