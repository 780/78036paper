+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Efraimstochter
Photo URL    : https://pixabay.com/en/moose-animal-portrait-head-901921/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : Aug. 16, 2015
Uploaded     : 11 months ago

+--------+
|  TAGS  |
+--------+
Moose, Animal Portrait, Head, Moose Cow, Sweden, Female