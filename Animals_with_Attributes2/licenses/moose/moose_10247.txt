+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tuchodi
Photo URL    : https://www.flickr.com/photos/tuchodi/5075616850/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Oct 12 07:57:21 GMT+0200 2010
Upload Date  : Tue Oct 12 17:33:24 GMT+0200 2010
Views        : 834
Comments     : 3


+---------+
|  TITLE  |
+---------+
Moose Morning


+---------------+
|  DESCRIPTION  |
+---------------+
This trio has been around most of the late summer and fall.


+--------+
|  TAGS  |
+--------+
"cow moose" "calf moose" moose 18-270mm 