+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sambouchard418
Photo URL    : https://www.flickr.com/photos/samuelbouchard/3212139817/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 11 15:50:53 GMT+0100 2008
Upload Date  : Tue Jan 20 15:52:49 GMT+0100 2009
Views        : 615
Comments     : 0


+---------+
|  TITLE  |
+---------+
Albino moose


+---------------+
|  DESCRIPTION  |
+---------------+
Taken in Beauce, Québec, Canada, by a friend.


+--------+
|  TAGS  |
+--------+
albino moose quebec "canada albino" 