+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Laubenstein Karen, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/deers/moose-and-elk/young-bull-moose-alces-alces-lies-in-grassy-area-near-water
License      : public domain (CC0)
Uploaded     : 2015-01-06 07:38:21

+--------+
|  TAGS  |
+--------+
young, bull, moose, alces, alces, lies, grassy, area, near, water