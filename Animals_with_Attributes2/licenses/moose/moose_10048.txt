+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Seven Months of Snow
Photo URL    : https://www.flickr.com/photos/47475932@N08/13753066335/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 27 10:54:05 GMT+0200 2013
Upload Date  : Thu Apr 10 07:59:27 GMT+0200 2014
Views        : 90
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bull moose 2013


+---------------+
|  DESCRIPTION  |
+---------------+
Bull moose 2013


+--------+
|  TAGS  |
+--------+
Animal Moose Alaska 