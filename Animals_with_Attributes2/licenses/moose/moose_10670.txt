+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Bauer, Erwin and Peggy, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/deers/moose-and-elk/bull-moose-browsing-in-an-alaskan-tundra-pond
License      : public domain (CC0)
Uploaded     : 2015-01-06 07:07:22

+--------+
|  TAGS  |
+--------+
bull, moose, browsing, Alaskan, tundra, pond