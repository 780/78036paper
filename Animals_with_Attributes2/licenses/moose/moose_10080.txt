+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : VirtKitty
Photo URL    : https://www.flickr.com/photos/lalouque/3881474178/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 30 14:17:26 GMT+0200 2009
Upload Date  : Wed Sep 2 15:42:34 GMT+0200 2009
Views        : 133
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow moose with twins


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
AK Denali moose 