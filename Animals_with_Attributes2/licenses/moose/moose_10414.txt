+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KimonBerlin
Photo URL    : https://www.flickr.com/photos/kimon/6174368868/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 12 04:18:23 GMT+0200 2011
Upload Date  : Fri Sep 23 06:31:28 GMT+0200 2011
Views        : 22
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
rmnp colorado spring 