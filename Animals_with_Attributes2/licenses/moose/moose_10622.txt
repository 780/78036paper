+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : come_to_sin
Photo URL    : https://pixabay.com/en/animals-moose-wildlife-elk-1173690/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : Aug. 16, 2015
Uploaded     : 5 months ago

+--------+
|  TAGS  |
+--------+
Animals, Moose, Wildlife, Elk