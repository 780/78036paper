+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Thomas Minnich
Photo URL    : https://www.flickr.com/photos/rawhide872/24586759985/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jan 18 15:21:32 GMT+0100 2016
Upload Date  : Sun Jan 24 19:37:10 GMT+0100 2016
Views        : 7
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elch


+---------------+
|  DESCRIPTION  |
+---------------+
18.01.16, Wildpark Klein-Auheim


+--------+
|  TAGS  |
+--------+
2016 "Wildpark Alte Fasanerie" "Wildpark Klein-Auheim" Wildpark "Januar 2016" Tiere Zoo 