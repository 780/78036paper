+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : twicepix
Photo URL    : https://www.flickr.com/photos/twicepix/4248637262/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 2 14:28:09 GMT+0100 2010
Upload Date  : Tue Jan 5 18:32:26 GMT+0100 2010
Views        : 233
Comments     : 0


+---------+
|  TITLE  |
+---------+
elk


+---------------+
|  DESCRIPTION  |
+---------------+
elch


+--------+
|  TAGS  |
+--------+
zoo tierpark dählhölzli bern elch elk 