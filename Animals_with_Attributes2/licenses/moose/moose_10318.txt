+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SurprisePally
Photo URL    : https://www.flickr.com/photos/31417716@N00/2657038962/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 29 16:31:58 GMT+0200 2008
Upload Date  : Thu Jul 10 23:54:49 GMT+0200 2008
Views        : 37
Comments     : 0


+---------+
|  TITLE  |
+---------+
moose


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Glacier National Park 