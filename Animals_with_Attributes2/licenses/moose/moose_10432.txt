+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : azule
Photo URL    : https://www.flickr.com/photos/azule/8086622418/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Sep 13 08:58:32 GMT+0200 2012
Upload Date  : Sun Oct 14 17:59:29 GMT+0200 2012
Views        : 572
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
yellowstone moose wild animal 