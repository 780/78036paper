+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : VisitGrosMorne
Photo URL    : https://www.flickr.com/photos/grosmornecoop/9242359570/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 8 01:36:39 GMT+0200 2005
Upload Date  : Mon Jul 8 21:55:09 GMT+0200 2013
Views        : 51
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)