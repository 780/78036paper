+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lauris Rubenis
Photo URL    : https://www.flickr.com/photos/beardforgood/9350259768/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 22 22:17:49 GMT+0200 2013
Upload Date  : Tue Jul 23 09:54:24 GMT+0200 2013
Views        : 854
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)