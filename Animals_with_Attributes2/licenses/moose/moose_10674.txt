+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Karen Laubenstein, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/deers/moose-and-elk/a-bull-moose-stands-under-birch-tree
License      : public domain (CC0)
Uploaded     : 2015-01-06 07:01:01

+--------+
|  TAGS  |
+--------+
bull, moose, stands, birch, tree