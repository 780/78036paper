+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : 
Photo URL    : https://commons.wikimedia.org/wiki/File:Lonesome-Lake-Moose.jpg
License      : Creative Commons (//en.wikipedia.org/wiki/en:Creative_Commons) Attribution-Share Alike 2.5 Generic (//creativecommons.org/licenses/by-sa/2.5/deed.en)
Date         : 
