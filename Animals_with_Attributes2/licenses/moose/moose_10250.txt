+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lee Edwin Coursey
Photo URL    : https://www.flickr.com/photos/leeco/2729263078/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 27 06:51:11 GMT+0200 2008
Upload Date  : Sun Aug 3 20:49:39 GMT+0200 2008
Views        : 1,016
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose number two


+---------------+
|  DESCRIPTION  |
+---------------+
From our trip to Maine in 2008 - taken on our first moose safari, with Ed Mathieu, on West Branch Pond north of Greenville.


+--------+
|  TAGS  |
+--------+
Maine travel summer 2008 safari moose animal wildlife 