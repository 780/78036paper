+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : shammond42
Photo URL    : https://www.flickr.com/photos/shammond42/1253497144/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 27 20:10:52 GMT+0200 2007
Upload Date  : Tue Aug 28 02:10:52 GMT+0200 2007
Views        : 60
Comments     : 0


+---------+
|  TITLE  |
+---------+
8880114


+---------------+
|  DESCRIPTION  |
+---------------+
Old Pictures of a Moose Following My Dad and I down Basin Trail in the White Mountains of NH


+--------+
|  TAGS  |
+--------+
moose NH 