+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Paul Resh
Photo URL    : https://www.flickr.com/photos/paulresh/198911580/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 26 09:47:59 GMT+0200 2006
Upload Date  : Wed Jul 26 18:47:59 GMT+0200 2006
Views        : 1,650
Comments     : 4


+---------+
|  TITLE  |
+---------+
Moose, AK


+---------------+
|  DESCRIPTION  |
+---------------+
I wish I could have gotten a better picture, but I didn't think it would be a good idea to get too close. The zoom on my minolta just doesn't cut it. Either way it was an awsome experience to be within 50 yards of such a beautiful mom and baby.


+--------+
|  TAGS  |
+--------+
Alaska Vacation Nature Wildlife Moose 