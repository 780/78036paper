+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael Johansson
Photo URL    : https://www.flickr.com/photos/agatefilm/6844247444/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 17 18:32:11 GMT+0100 2012
Upload Date  : Sat Mar 17 18:55:49 GMT+0100 2012
Geotag Info  : Latitude:58.377778, Longitude:12.831001
Views        : 822
Comments     : 0


+---------+
|  TITLE  |
+---------+
Älgar


+---------------+
|  DESCRIPTION  |
+---------------+
Min andra bild på älgar.


+--------+
|  TAGS  |
+--------+
älg håle Sweden Sverige Agatefilm Ågatefilm Michael Johansson "Michael Johansson" Grästorp Moose åker land country elk hirvi wapiti άλκες गोज़न ヘラジカ ಜಿಂಕೆ 麋鹿 고라니 alces alnis briedis elg łoś tos alce лось กวางชนิดใหญ่ Elch elledning skog träd färg colour oskärpa suddig sigma 18-50 18-55 