+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : weslowik
Photo URL    : https://www.flickr.com/photos/7702946@N02/6017847918/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 21 21:01:07 GMT+0200 2011
Upload Date  : Sun Aug 7 14:47:26 GMT+0200 2011
Views        : 109
Comments     : 0


+---------+
|  TITLE  |
+---------+
Canadian Moose in field w/ calf 3


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2011 Moose 