+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : U.S. Fish and Wildlife Service - Midwest Region
Photo URL    : https://www.flickr.com/photos/usfwsmidwest/15591774324/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 6 14:03:21 GMT+0200 2011
Upload Date  : Tue Jan 6 14:51:09 GMT+0100 2015
Views        : 1,329
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose


+---------------+
|  DESCRIPTION  |
+---------------+
Photo by USFWS.


+--------+
|  TAGS  |
+--------+
Agassiz "National Wildlife Refuge" Refuge NWR Minnesota MN Moose Mammal Mammals "Creative Commons" USFWS "U.S. Fish and Wildlife Service" Midwest 