+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : peupleloup
Photo URL    : https://www.flickr.com/photos/peupleloup/3885922912/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 1 09:52:37 GMT+0200 2009
Upload Date  : Fri Sep 4 02:15:57 GMT+0200 2009
Views        : 904
Comments     : 0


+---------+
|  TITLE  |
+---------+
Orignal


+---------------+
|  DESCRIPTION  |
+---------------+
Tranquille, mais regarde les environs régulièrement.


+--------+
|  TAGS  |
+--------+
orignal moose 