+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Niko Vermeer
Photo URL    : https://www.flickr.com/photos/tibbles/4951862317/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 9 19:21:47 GMT+0200 2010
Upload Date  : Thu Sep 2 22:50:31 GMT+0200 2010
Views        : 26
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose!


+---------------+
|  DESCRIPTION  |
+---------------+
Kangamacus Highway


+--------+
|  TAGS  |
+--------+
(no tags)