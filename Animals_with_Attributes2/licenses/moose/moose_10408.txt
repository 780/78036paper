+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KimonBerlin
Photo URL    : https://www.flickr.com/photos/kimon/6174365084/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 12 04:14:23 GMT+0200 2011
Upload Date  : Fri Sep 23 06:29:23 GMT+0200 2011
Views        : 20
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
rmnp colorado spring 