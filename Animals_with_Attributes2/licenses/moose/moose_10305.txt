+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : schmidmsu1
Photo URL    : https://www.flickr.com/photos/travelmemoirs/1321489808/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 26 15:41:55 GMT+0200 2007
Upload Date  : Tue Sep 4 15:32:12 GMT+0200 2007
Views        : 26
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moose


+---------------+
|  DESCRIPTION  |
+---------------+
Moose at rest, Denali


+--------+
|  TAGS  |
+--------+
(no tags)