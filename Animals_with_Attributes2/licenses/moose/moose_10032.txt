+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael Johansson
Photo URL    : https://www.flickr.com/photos/agatefilm/6593453225/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 29 11:15:18 GMT+0100 2011
Upload Date  : Thu Dec 29 12:18:49 GMT+0100 2011
Geotag Info  : Latitude:58.381918, Longitude:12.760448
Views        : 1,469
Comments     : 0


+---------+
|  TITLE  |
+---------+
Älg


+---------------+
|  DESCRIPTION  |
+---------------+
Fick syn på en älg och som tur så var kameran med idag.


+--------+
|  TAGS  |
+--------+
Älg Sweden Sverige Grästorp Moose åker land country Agatefilm Ågatefilm Michael Johansson "Michael Johansson" elk hirvi wapiti άλκες गोज़न ヘラジカ ಜಿಂಕೆ 麋鹿 고라니 alces alnis briedis elg łoś tos alce лось กวางชนิดใหญ่ Elch elledning skog träd färg colour oskärpa suddig sigma 18-50 18-55 