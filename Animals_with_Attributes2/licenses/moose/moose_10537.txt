+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kuhnmi
Photo URL    : https://www.flickr.com/photos/31176607@N05/13674015265/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 23 15:20:58 GMT+0100 2014
Upload Date  : Sun Apr 6 21:06:05 GMT+0200 2014
Views        : 280
Comments     : 0


+---------+
|  TITLE  |
+---------+
p-7250


+---------------+
|  DESCRIPTION  |
+---------------+
Moose drinking in the wildlife park Langenberg near Zurich, Switzerland


+--------+
|  TAGS  |
+--------+
Wildnispark Langenberg "Wildnispark Langenberg" wildlife park "wilidlife park" moose elk "Eurasian elk" Eurasischer Elch "eurasischer Elch" nature animal animals drinking water 