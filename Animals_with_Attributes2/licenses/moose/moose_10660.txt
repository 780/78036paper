+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Bowman Tim, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/deers/moose-and-elk/moose-and-calf
License      : public domain (CC0)
Uploaded     : 2015-01-06 07:19:35

+--------+
|  TAGS  |
+--------+
moose, calf