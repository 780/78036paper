+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : tadah
Photo URL    : https://pixabay.com/en/moose-animal-wild-wildlife-mammal-142133/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : May 22, 2011
Uploaded     : June 30, 2013

+--------+
|  TAGS  |
+--------+
Moose, Animal, Wild, Wildlife, Mammal, Forest, Deer