+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : priittammets
Photo URL    : https://www.flickr.com/photos/tammets/5252409970/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 10 21:50:09 GMT+0100 2010
Upload Date  : Sat Dec 11 21:31:40 GMT+0100 2010
Geotag Info  : Latitude:59.518341, Longitude:25.555019
Views        : 1,092
Comments     : 2


+---------+
|  TITLE  |
+---------+
moose


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
põder moose pudisoo detsember lumi 