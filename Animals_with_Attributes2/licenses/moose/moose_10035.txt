+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : NOAA Photo Library
Photo URL    : https://www.flickr.com/photos/noaaphotolib/5408277621/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 1 16:54:55 GMT+0100 2011
Upload Date  : Tue Feb 1 22:54:55 GMT+0100 2011
Views        : 561
Comments     : 0


+---------+
|  TITLE  |
+---------+
anim0663


+---------------+
|  DESCRIPTION  |
+---------------+
Bull moose still having velvet on antlers. This guy could cause some serious damage.  2006 September 8.

Photographer: Dan Petersen, NOAA/NWSFO Anchorage.

Credit: NOAA 200th Photo Contest.


+--------+
|  TAGS  |
+--------+
NOAA moose 