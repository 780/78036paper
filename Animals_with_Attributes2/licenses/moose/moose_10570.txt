+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jinxmcc
Photo URL    : https://www.flickr.com/photos/64443083@N00/5016534743/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 20 04:43:54 GMT+0200 2010
Upload Date  : Thu Sep 23 09:10:05 GMT+0200 2010
Views        : 158
Comments     : 5


+---------+
|  TITLE  |
+---------+
Talking Moose?


+---------------+
|  DESCRIPTION  |
+---------------+
This was the best view I ever got of his face.


+--------+
|  TAGS  |
+--------+
moose GrandTetons MooseJunction 