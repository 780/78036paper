+--------------------------------------+
|  PublicDomainImages Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Jerry, Danielle G, U.S. Fish and Wildlife Service
Photo URL    : http://www.public-domain-image.com/free-images/fauna-animals/deers/moose-and-elk/female-moose-and-calf-by-water
License      : public domain (CC0)
Uploaded     : 2015-01-06 07:17:35

+--------+
|  TAGS  |
+--------+
female, moose, calf, water