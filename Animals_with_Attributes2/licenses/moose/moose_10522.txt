+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : WordRidden
Photo URL    : https://www.flickr.com/photos/wordridden/2591957535/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 27 15:21:40 GMT+0200 2008
Upload Date  : Thu Jun 19 14:10:09 GMT+0200 2008
Views        : 146
Comments     : 0


+---------+
|  TITLE  |
+---------+
Moosie!


+---------------+
|  DESCRIPTION  |
+---------------+
Mooses. Moosen. Meese.


+--------+
|  TAGS  |
+--------+
alaska moose 