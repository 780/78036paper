+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : jazapp
Photo URL    : https://pixabay.com/en/moose-wildlife-nature-animal-male-1014123/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : July 17, 2015
Uploaded     : 8 months ago

+--------+
|  TAGS  |
+--------+
Moose, Wildlife, Nature, Animal, Male, Antler, Bull