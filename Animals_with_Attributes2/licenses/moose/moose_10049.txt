+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : RTD Photography
Photo URL    : https://www.flickr.com/photos/rtdphotography/3057763168/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 30 15:51:48 GMT+0200 2008
Upload Date  : Tue Nov 25 02:34:54 GMT+0100 2008
Geotag Info  : Latitude:48.110611, Longitude:-88.668379
Views        : 1,439
Comments     : 1


+---------+
|  TITLE  |
+---------+
Bull Moose in Brady Cove


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
duck ducks duckling birds wildlife animal animals "birds in flight" "cute animals" "black duck" chicks chick chicklets "isle royale" "national Park" "isle royale national park" "up north michigan" "upper pennnsula" "michigan u.p." outdoors moose bull "bull moose" "rtd photography" "rolf & Candy Peterson" "wolf/moose research" 