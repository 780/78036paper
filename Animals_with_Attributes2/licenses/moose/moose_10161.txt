+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DenaliNPS
Photo URL    : https://www.flickr.com/photos/denalinps/5728727966/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 28 02:13:52 GMT+0200 2010
Upload Date  : Tue May 17 03:23:53 GMT+0200 2011
Views        : 1,341
Comments     : 1


+---------+
|  TITLE  |
+---------+
I Spy ...


+---------------+
|  DESCRIPTION  |
+---------------+
(NPS Photo / Ken Conger) 

Check out the official Denali Facebook, Twitter and YouTube pages:

Like us on Facebook: <a href="http://www.facebook.com/DenaliNPS" rel="nofollow">www.facebook.com/DenaliNPS</a>

Follow us on Twitter: <a href="http://www.twitter.com/DenaliNPS" rel="nofollow">www.twitter.com/DenaliNPS</a>

Denali YouTube Channel: <a href="http://www.youtube.com/denalinps" rel="nofollow">www.youtube.com/denalinps</a>


+--------+
|  TAGS  |
+--------+
wildlife moose grizzlies bears prey 