+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cfaobam
Photo URL    : https://www.flickr.com/photos/cfaobam/8614368300/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 13 17:38:11 GMT+0100 2013
Upload Date  : Tue Apr 2 19:55:51 GMT+0200 2013
Views        : 458
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polarlicht-Reise 2013 - Tag08 - 031


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
nature winter finnland cfaobam canon eos 650d Lyngenfjord Lyngen Lyngsfjord nordsamisch Ivgovuotna Elch Alces Hirsche europe europa norwegen Nordweg Polarkreises Polarregion 