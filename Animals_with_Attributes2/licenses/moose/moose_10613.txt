+--------------------------------------+
|       Pixabay Photo Metadata         |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Hans
Photo URL    : https://pixabay.com/en/moose-alces-alces-forest-wild-zoo-60274/
License      : CC0 Public Domain (https://creativecommons.org/publicdomain/zero/1.0/deed.en)
Created      : Sept. 13, 2012
Uploaded     : Oct. 15, 2012

+--------+
|  TAGS  |
+--------+
Moose, Alces Alces, Forest, Wild, Zoo, Wildlife Park