+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : moon angel
Photo URL    : https://www.flickr.com/photos/angelune/2660902579/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 5 10:58:43 GMT+0200 2008
Upload Date  : Sat Jul 12 19:29:03 GMT+0200 2008
Views        : 206
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mr. Chubbs


+---------------+
|  DESCRIPTION  |
+---------------+
Becky's bunny.  She's had him for 10 years!


+--------+
|  TAGS  |
+--------+
"chris and becky" animals pets rabbit bunny 