+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Last Hero
Photo URL    : https://www.flickr.com/photos/uwe_schubert/4462190729/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 25 15:32:37 GMT+0100 2010
Upload Date  : Thu Mar 25 18:33:45 GMT+0100 2010
Views        : 635
Comments     : 0


+---------+
|  TITLE  |
+---------+
Frühling 2010


+---------------+
|  DESCRIPTION  |
+---------------+
Unser letztes Kaninchen.


+--------+
|  TAGS  |
+--------+
Kaninchen hase rabbit frühling spring franken märz march 2010 s5800 