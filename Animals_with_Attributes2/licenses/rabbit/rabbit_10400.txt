+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dru Bloomfield - At Home in Scottsdale
Photo URL    : https://www.flickr.com/photos/athomeinscottsdale/2520927955/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 22 07:11:27 GMT+0100 2008
Upload Date  : Sun May 25 18:35:46 GMT+0200 2008
Views        : 4,079
Comments     : 0


+---------+
|  TITLE  |
+---------+
Easter Bunny 2008


+---------------+
|  DESCRIPTION  |
+---------------+
This little guy was hanging outside my office door for weeks.  This photo was taken right around Easter.


+--------+
|  TAGS  |
+--------+
Scottdale rabbit bunny dru bloomfield drubloomfield facebook 