+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rolandojones
Photo URL    : https://www.flickr.com/photos/rolandojones/5671358236/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 24 08:30:15 GMT+0200 2011
Upload Date  : Sat Apr 30 03:39:17 GMT+0200 2011
Views        : 4,523
Comments     : 0


+---------+
|  TITLE  |
+---------+
Easter Bunny


+---------------+
|  DESCRIPTION  |
+---------------+
Captured this at church on Easter Sunday


+--------+
|  TAGS  |
+--------+
Easter Bunny "Rancho Capistrano" 