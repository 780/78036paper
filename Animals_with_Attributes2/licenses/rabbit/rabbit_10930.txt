+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nikchick
Photo URL    : https://www.flickr.com/photos/nikchick/6647940301/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 3 22:51:47 GMT+0100 2012
Upload Date  : Fri Jan 6 18:20:25 GMT+0100 2012
Geotag Info  : Latitude:47.542638, Longitude:-122.295781
Views        : 2,380
Comments     : 2


+---------+
|  TITLE  |
+---------+
Scout - 10 weeks


+---------------+
|  DESCRIPTION  |
+---------------+
Looking for homes for the Tribblets, this is what I've shared about Scout: Scout needs a home! Oh Scout, what to say about this one. The opposite of his sister Sophie in every way, he's the only mostly-white baby in our litter. He's the biggest, strongest, spunkiest of the boys. He's a tricky one, the first to open his eyes, the first to learn to jump to the next level of the cage, the first to make an escape. He loves to rattle his bowls, dig and toss, and steal his brother's blanket through the bars of his cage. If you think bunnies are pets without personality, Scout is out to show you what you're missing. Can you give Scout a home?


+--------+
|  TAGS  |
+--------+
Bunnies Tribblets 