+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : markbyzewski
Photo URL    : https://www.flickr.com/photos/markbyzewski/4703937973/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 3 06:27:03 GMT+0200 2006
Upload Date  : Tue Jun 15 23:43:50 GMT+0200 2010
Views        : 140
Comments     : 0


+---------+
|  TITLE  |
+---------+
2006-06-03 134


+---------------+
|  DESCRIPTION  |
+---------------+
A morning at Garden of the Gods with deer. Colorado Springs.


+--------+
|  TAGS  |
+--------+
"Garden of the Gods" "Colorado Springs" Rabbit 