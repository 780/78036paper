+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Marc Lagneau
Photo URL    : https://www.flickr.com/photos/marc-lagneau/4251702152/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 5 14:49:20 GMT+0100 2010
Upload Date  : Wed Jan 6 18:38:19 GMT+0100 2010
Views        : 3,030
Comments     : 14


+---------+
|  TITLE  |
+---------+
Lapin de garenne / Rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
Base de loisirs de saint Quentin en Yvelines - Trappes - 05/01/10


+--------+
|  TAGS  |
+--------+
Nikon D300s Trappes "Base de loisirs de saint Quentin en Yvelines" france "Lapin de garenne" Rabbit "Top Seven" 