+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gurdonark
Photo URL    : https://www.flickr.com/photos/46183897@N00/14108308077/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 5 10:59:28 GMT+0200 2013
Upload Date  : Thu May 29 03:54:03 GMT+0200 2014
Views        : 1,782
Comments     : 1


+---------+
|  TITLE  |
+---------+
cottontail rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
Shawnee Park, Plano, Texas, May 28 2014


+--------+
|  TAGS  |
+--------+
rabbit bunny cottontail shawnee park plano texas 