+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : buitenzorger
Photo URL    : https://www.flickr.com/photos/buitenzorger/2100791934/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 2 16:53:44 GMT+0200 2007
Upload Date  : Mon Dec 10 13:30:28 GMT+0100 2007
Geotag Info  : Latitude:-6.586511, Longitude:106.787796
Views        : 256
Comments     : 0


+---------+
|  TITLE  |
+---------+
Little Rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)