+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tomi Tapio
Photo URL    : https://www.flickr.com/photos/tomitapio/6141339944/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 8 19:52:26 GMT+0200 2009
Upload Date  : Mon Sep 12 20:26:54 GMT+0200 2011
Geotag Info  : Latitude:60.206067, Longitude:24.897315
Views        : 2,563
Comments     : 8


+---------+
|  TITLE  |
+---------+
Bushes are for hiding


+---------------+
|  DESCRIPTION  |
+---------------+
2009-07-08 in my neighbourhood. Id 20090708_4560.


+--------+
|  TAGS  |
+--------+
rabbit bunny villikani sx100 "Canon PowerShot SX100 IS" Helsinki Pikku-Huopalahti young ears safety 