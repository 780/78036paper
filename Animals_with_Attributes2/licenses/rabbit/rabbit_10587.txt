+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cibomahto
Photo URL    : https://www.flickr.com/photos/cibomahto/2402036539/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 5 17:32:58 GMT+0200 2008
Upload Date  : Thu Apr 10 07:48:52 GMT+0200 2008
Views        : 1,327
Comments     : 0


+---------+
|  TITLE  |
+---------+
Jack rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
Lots of wildlife around here


+--------+
|  TAGS  |
+--------+
"Gilbert Riparian Preserve" rabbit 