+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Derek N Winterburn
Photo URL    : https://www.flickr.com/photos/dnwinterburn/18250079763/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 14 16:16:42 GMT+0200 2015
Upload Date  : Tue Jun 16 23:03:45 GMT+0200 2015
Geotag Info  : Latitude:51.413213, Longitude:-0.347431
Views        : 146
Comments     : 0


+---------+
|  TITLE  |
+---------+
WIld rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Bushy Park" Rabbit "Richmond Upon Thames" Hampton England "United Kingdom" GB Flowers "Lilium lancifolium" Tigerlily 