+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : fauxto_digit
Photo URL    : https://www.flickr.com/photos/fauxto_dkp/5738176547/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 18 19:36:53 GMT+0200 2011
Upload Date  : Fri May 20 03:55:00 GMT+0200 2011
Views        : 920
Comments     : 9


+---------+
|  TITLE  |
+---------+
Big Jumper


+---------------+
|  DESCRIPTION  |
+---------------+
Sitting around in Evergreen, this bunny was really big, but so cute.  You can see the reflection of trees in his eye.  

I'm not seeing as many this year as I did last year.


+--------+
|  TAGS  |
+--------+
Bunny Furry Fauna Jumper Rabbit "Jack Rabbit" Hop 