+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sonstroem
Photo URL    : https://www.flickr.com/photos/sonstroem/14336856853/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 31 16:03:00 GMT+0200 2014
Upload Date  : Sun Jun 1 04:05:31 GMT+0200 2014
Views        : 839
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Cosumnes River Preserve" cottontail bunny rabbit 