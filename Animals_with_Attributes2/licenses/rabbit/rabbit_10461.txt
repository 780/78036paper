+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike Procario
Photo URL    : https://www.flickr.com/photos/procario/125176596/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 8 10:56:47 GMT+0200 2006
Upload Date  : Sat Apr 8 18:13:56 GMT+0200 2006
Views        : 2,076
Comments     : 2


+---------+
|  TITLE  |
+---------+
Ginger as the Easter Bunny


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
rabbit easter easterbunny bunnies 