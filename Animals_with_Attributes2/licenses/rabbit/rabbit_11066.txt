+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gvgoebel
Photo URL    : https://www.flickr.com/photos/37467370@N08/7632564662/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 23 14:08:35 GMT+0200 2012
Upload Date  : Mon Jul 23 23:10:26 GMT+0200 2012
Views        : 235
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ybbun_1b


+---------------+
|  DESCRIPTION  |
+---------------+
rabbit, Denver Zoo, Colorado / 2006


+--------+
|  TAGS  |
+--------+
animals mammals rabbit 