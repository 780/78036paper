+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : audreyjm529
Photo URL    : https://www.flickr.com/photos/audreyjm529/185563879/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 9 07:57:04 GMT+0200 2006
Upload Date  : Sun Jul 9 17:44:14 GMT+0200 2006
Views        : 2,977
Comments     : 2


+---------+
|  TITLE  |
+---------+
Breakfast In The Marigolds


+---------------+
|  DESCRIPTION  |
+---------------+
This baby bunny was eating the marigold leaves, so I didn't want to disturb it.


+--------+
|  TAGS  |
+--------+
baby bunny marigolds eating breakfast gray brown bricks yellow cute green morning ears zoom 