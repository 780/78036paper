+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nblumhardt
Photo URL    : https://www.flickr.com/photos/nblumhardt/4371462570/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 2 18:01:46 GMT+0200 2009
Upload Date  : Fri Feb 19 22:45:11 GMT+0100 2010
Views        : 519
Comments     : 2


+---------+
|  TITLE  |
+---------+
One more honey bunny


+---------------+
|  DESCRIPTION  |
+---------------+
In a bunny paradise, green grass as far as the eye can see.


+--------+
|  TAGS  |
+--------+
(no tags)