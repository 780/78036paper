+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Airwolfhound
Photo URL    : https://www.flickr.com/photos/24874528@N04/3316168543/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 28 15:27:43 GMT+0100 2009
Upload Date  : Sat Feb 28 19:17:14 GMT+0100 2009
Views        : 1,073
Comments     : 3


+---------+
|  TITLE  |
+---------+
Wild rabbit - February 2009


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Rabbit "Stanborough Lakes" 