+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : markoknuutila
Photo URL    : https://www.flickr.com/photos/markoknuutila/14859372739/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 21 19:19:47 GMT+0200 2014
Upload Date  : Tue Aug 26 21:40:23 GMT+0200 2014
Views        : 1,896
Comments     : 2


+---------+
|  TITLE  |
+---------+
Crazy bunny, part I


+---------------+
|  DESCRIPTION  |
+---------------+
A Finnish mountain hare, these are supposed to be wild


+--------+
|  TAGS  |
+--------+
hare rabbit bunny metsäjänis jänis 