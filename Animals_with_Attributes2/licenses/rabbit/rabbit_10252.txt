+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : synspectrum
Photo URL    : https://www.flickr.com/photos/epector/20260714829/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 9 17:50:09 GMT+0200 2015
Upload Date  : Mon Aug 10 06:39:17 GMT+0200 2015
Views        : 362
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bunny


+---------------+
|  DESCRIPTION  |
+---------------+
Springbrook prairie, DuPage Co


+--------+
|  TAGS  |
+--------+
(no tags)