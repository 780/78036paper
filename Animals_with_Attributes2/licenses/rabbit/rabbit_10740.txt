+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Christopher Ba
Photo URL    : https://www.flickr.com/photos/10487993@N02/6510238005/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 12 12:41:11 GMT+0100 2011
Upload Date  : Wed Dec 14 13:09:06 GMT+0100 2011
Views        : 1,056
Comments     : 0


+---------+
|  TITLE  |
+---------+
Place of rabbits


+---------------+
|  DESCRIPTION  |
+---------------+
(C) by Christopher Bachner


+--------+
|  TAGS  |
+--------+
rabbit animal pet hay grey cute buny 