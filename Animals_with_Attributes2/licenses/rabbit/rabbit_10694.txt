+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Carly & Art
Photo URL    : https://www.flickr.com/photos/wiredwitch/7415168198/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 17 18:31:10 GMT+0200 2012
Upload Date  : Thu Jun 21 19:42:38 GMT+0200 2012
Geotag Info  : Latitude:38.922691, Longitude:-76.974785
Views        : 1,584
Comments     : 17


+---------+
|  TITLE  |
+---------+
Olaf at rest


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
bunny rabbit "house rabbit" pet rex minirex soft white brown cute olaf 