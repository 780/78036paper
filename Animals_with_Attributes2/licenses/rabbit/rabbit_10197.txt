+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jim Larrison
Photo URL    : https://www.flickr.com/photos/larrison/14592808875/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 4 18:24:04 GMT+0200 2014
Upload Date  : Mon Jul 7 05:00:59 GMT+0200 2014
Views        : 775
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bunny Rabbit in the Neighborhood


+---------------+
|  DESCRIPTION  |
+---------------+
The neighborhood bunny rabbit hopping around keeping Putter at bay


+--------+
|  TAGS  |
+--------+
Bunny Rabbit BunnyRabbit Animal Cute Wildlife 