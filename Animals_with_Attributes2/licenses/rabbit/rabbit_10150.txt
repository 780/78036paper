+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Furryscaly
Photo URL    : https://www.flickr.com/photos/furryscalyman/3638785011/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 11 20:33:18 GMT+0200 2009
Upload Date  : Thu Jun 18 22:07:24 GMT+0200 2009
Geotag Info  : Latitude:46.820032, Longitude:-100.786342
Views        : 38,488
Comments     : 16


+---------+
|  TITLE  |
+---------+
Bun-Bun


+---------------+
|  DESCRIPTION  |
+---------------+
...One of many nicknames I had for the four eastern cottontails, <i>Sylvilagus floridanus</i>, that I raised.  That and &quot;Buns&quot; &quot;Skipitty&quot;, &quot;Speedy&quot; &quot;Spaz&quot;, and who knows what else I called them.

So long story not-so-short, my neighbor came up to me a couple months ago with a bucket full of week-old rabbits.  She had uncovered their nest in her yard and was going to dispose of them so they won't grow up and eat her garden.

Instead, I took them in under the condition that I release them elsewhere.  I hand-fed them formula until they were old enough to wean to grass.  Finally, when they got big and rambunctious enough I released them.  All four survived thus far.  They are still quite wild.

And just to stick it to the neighbors, I released them in my backyard anyway :P  There may be too many cottontails around, but I'm pretty sure there's too many people too.  Learn to share.  They probably have more right to be here than us anyway.  I still see some of them from time to time, though I can no longer tell them apart.


+--------+
|  TAGS  |
+--------+
"Sylvilagus floridanus" "eastern cottontail" Sylvilagus "eastern cottontail rabbit" "eastern cotton-tailed rabbit" "cotton-tailed rabbit" "cottontail rabbit" cottontail rabbit bunny "bunny rabbit" "NOT A RODENT" Mammalia mammal Lagomorpha lagomorph Leporidae "New World rabbit" wildlife "wild animal" ND "North Dakota" "Burleigh County" captive hand-raised hand-fed orphan orphaned abandoned rescue rescued "animal rescue" "animal shelter" "urban wildlife" whiskers ears nose eyes white "white background" whiteground high-key hi-key young juvenile immature sub-adult cute cuddly "easter bunny" fuzzy furry shaggy scruffy Animalia animal hairy "animal rehab" "animal rehabilitation" taxonomy:kingdom=Animalia taxonomy:phylum=Chordata taxonomy:class=Mammalia taxonomy:order=Lagomorpha taxonomy:family=Leporidae taxonomy:genus=Sylvilagus "taxonomy:binomial=Sylvilagus floridanus" "taxonomy:common=eastern cottontail" baby 