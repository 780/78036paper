+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Justin Beckley
Photo URL    : https://www.flickr.com/photos/beckleybisset/1774062672/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 27 12:43:48 GMT+0200 2007
Upload Date  : Mon Oct 9 07:38:08 GMT+0200 2006
Views        : 30
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_6751


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Justin Beckley "Justin Beckley" "United Kingdom" England www.beckleybisset.com "Claire Bisset" Photographer 