+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/2772071107/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 17 19:37:16 GMT+0200 2008
Upload Date  : Mon Aug 18 02:02:51 GMT+0200 2008
Views        : 985
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hungry Rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
in the woods by the Mystic River


+--------+
|  TAGS  |
+--------+
rabbit bunny woods hungry eat 