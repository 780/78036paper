+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : etee
Photo URL    : https://www.flickr.com/photos/etee/2768344662/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 9 17:50:02 GMT+0200 2008
Upload Date  : Sat Aug 16 17:35:53 GMT+0200 2008
Geotag Info  : Latitude:33.161147, Longitude:-96.681744
Views        : 401
Comments     : 0


+---------+
|  TITLE  |
+---------+
20080809_7507


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
McKinney rabbit vacation 