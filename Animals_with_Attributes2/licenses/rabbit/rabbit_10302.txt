+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gurdonark
Photo URL    : https://www.flickr.com/photos/46183897@N00/13947735519/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 15 13:06:05 GMT+0200 2013
Upload Date  : Thu May 8 04:01:43 GMT+0200 2014
Views        : 584
Comments     : 0


+---------+
|  TITLE  |
+---------+
cottontail rabbit in my side yard


+---------------+
|  DESCRIPTION  |
+---------------+
May 7, 2014, 8 p.m.


+--------+
|  TAGS  |
+--------+
cottontail rabbit allen texas 