+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dakiny
Photo URL    : https://www.flickr.com/photos/dakiny/4771347201/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 8 02:20:07 GMT+0200 2010
Upload Date  : Wed Jul 7 19:20:07 GMT+0200 2010
Geotag Info  : Latitude:35.363519, Longitude:136.623229
Views        : 580
Comments     : 0


+---------+
|  TITLE  |
+---------+
ウサギ


+---------------+
|  DESCRIPTION  |
+---------------+
三女飼っているウサギ


+--------+
|  TAGS  |
+--------+
rabbit ウサギ 兎 ネザーランドワーフ "Netherland Dwarf" 