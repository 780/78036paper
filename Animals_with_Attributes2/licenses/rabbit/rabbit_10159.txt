+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andy Atzert
Photo URL    : https://www.flickr.com/photos/andyatzert/6019397201/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 28 09:02:33 GMT+0100 2010
Upload Date  : Mon Aug 8 01:01:22 GMT+0200 2011
Views        : 228
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bunnies


+---------------+
|  DESCRIPTION  |
+---------------+
Baby rabbits, Mesa, AZ


+--------+
|  TAGS  |
+--------+
rabbits "baby rabbits" bunnies 