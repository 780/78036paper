+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Carly & Art
Photo URL    : https://www.flickr.com/photos/wiredwitch/3992157433/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 4 00:00:07 GMT+0200 2009
Upload Date  : Thu Oct 8 14:09:27 GMT+0200 2009
Geotag Info  : Latitude:38.922691, Longitude:-76.974785
Views        : 797
Comments     : 6


+---------+
|  TITLE  |
+---------+
Derby ponders his next move


+---------------+
|  DESCRIPTION  |
+---------------+
Clover or grass? squash or mint?


+--------+
|  TAGS  |
+--------+
derby bunny rabbit "house rabbit" "on the loose" pet outside brown soft rex minirex yard garden 