+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : audreyjm529
Photo URL    : https://www.flickr.com/photos/audreyjm529/4843550526/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 26 06:09:53 GMT+0200 2010
Upload Date  : Fri Jul 30 13:40:51 GMT+0200 2010
Views        : 16,884
Comments     : 3


+---------+
|  TITLE  |
+---------+
Rehab Success


+---------------+
|  DESCRIPTION  |
+---------------+
This is the same bunny that I had a picture of when he was a baby. There isn't much information about these rabbits since they usually don't live very long. This beautiful guy (Wink) is sure proving them all wrong! Thanks to our friend and fellow rehabber, he's free on her farm and even has a girlfriend. Thanks Sue!
To see him as a baby, look here-
<a href="http://www.flickr.com/photos/audreyjm529/4519068753/">www.flickr.com/photos/audreyjm529/4519068753/</a>


+--------+
|  TAGS  |
+--------+
Rehab Success rehabilitation wildlife rabbit bunny blonde albino canon zoom eating 