+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Marie Hale
Photo URL    : https://www.flickr.com/photos/15016964@N02/18113810800/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 16 11:28:44 GMT+0200 2015
Upload Date  : Sun May 31 13:32:41 GMT+0200 2015
Views        : 1,330
Comments     : 0


+---------+
|  TITLE  |
+---------+
European rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
European rabbit - taken at Farlington Marshes, Langstone Harbour, Hampshire on 16th May 2015


+--------+
|  TAGS  |
+--------+
rabbit 