+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ozmafan
Photo URL    : https://www.flickr.com/photos/ozmafan/7193957572/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 13 22:01:12 GMT+0200 2012
Upload Date  : Mon May 14 07:04:19 GMT+0200 2012
Views        : 1,936
Comments     : 0


+---------+
|  TITLE  |
+---------+
rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
at Orcas Landing


+--------+
|  TAGS  |
+--------+
rabbit "orcas island" "washington state" grass 