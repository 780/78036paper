+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Giorgos Michalogiorgakis
Photo URL    : https://www.flickr.com/photos/michalogiorgakis/5742686345/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 19 02:54:39 GMT+0200 2011
Upload Date  : Sat May 21 16:51:36 GMT+0200 2011
Geotag Info  : Latitude:40.455700, Longitude:-86.933312
Views        : 288
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
φύση indiana "west lafayette" "celery bog" λαγός usa 