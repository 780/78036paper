+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mori Claudia
Photo URL    : https://www.flickr.com/photos/morclaud/175098980/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 25 04:41:30 GMT+0200 2006
Upload Date  : Mon Jun 26 05:00:15 GMT+0200 2006
Views        : 171
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
I was finally able to take a picture of this rabbit after it ran all over the place.


+--------+
|  TAGS  |
+--------+
(no tags)