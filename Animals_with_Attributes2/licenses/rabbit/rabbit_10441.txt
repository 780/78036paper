+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dougtone
Photo URL    : https://www.flickr.com/photos/dougtone/14322707348/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 31 18:25:47 GMT+0200 2014
Upload Date  : Thu Jun 26 03:45:25 GMT+0200 2014
Views        : 199
Comments     : 0


+---------+
|  TITLE  |
+---------+
Fort Laramie - Wyoming


+---------------+
|  DESCRIPTION  |
+---------------+
Fort Laramie - Wyoming


+--------+
|  TAGS  |
+--------+
"Fort Laramie" fort frontier pioneer Wyoming "Oregon Trail" barracks house "parade ground" 