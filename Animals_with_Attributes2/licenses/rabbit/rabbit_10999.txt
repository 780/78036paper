+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daveoratox
Photo URL    : https://www.flickr.com/photos/atoxinsocks/3609376662/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 27 11:21:08 GMT+0200 2009
Upload Date  : Tue Jun 9 02:34:45 GMT+0200 2009
Views        : 947
Comments     : 5


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
The young'uns are out. This one let me get pretty close.


+--------+
|  TAGS  |
+--------+
rabbit "baby rabbit" clover "eastern cottontail" 