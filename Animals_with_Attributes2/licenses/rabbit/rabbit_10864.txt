+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bobosh_t
Photo URL    : https://www.flickr.com/photos/frted/7679527424/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 30 12:32:41 GMT+0200 2012
Upload Date  : Mon Jul 30 23:24:23 GMT+0200 2012
Geotag Info  : Latitude:39.654799, Longitude:-84.224313
Views        : 192
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rabbit


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Cox Arboretum" "Five Rivers Metroparks" 