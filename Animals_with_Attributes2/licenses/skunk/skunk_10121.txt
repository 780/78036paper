+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Marie Hale
Photo URL    : https://www.flickr.com/photos/15016964@N02/5695878311/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 18 10:53:23 GMT+0200 2010
Upload Date  : Sat May 7 17:09:17 GMT+0200 2011
Views        : 2,286
Comments     : 1


+---------+
|  TITLE  |
+---------+
Striped skunk


+---------------+
|  DESCRIPTION  |
+---------------+
Striped skunk - taken at Camperdown Wildlife Centre on 18th May 2010


+--------+
|  TAGS  |
+--------+
skunk 