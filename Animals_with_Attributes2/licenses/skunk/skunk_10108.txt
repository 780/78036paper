+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : GregTheBusker
Photo URL    : https://www.flickr.com/photos/gregthebusker/5889825633/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 23 08:35:46 GMT+0200 2011
Upload Date  : Fri Jul 1 09:45:53 GMT+0200 2011
Geotag Info  : Latitude:37.590346, Longitude:-122.323107
Views        : 1,168
Comments     : 0


+---------+
|  TITLE  |
+---------+
Skunks


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
California June 2011 skunk coyote point 