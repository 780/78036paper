+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cobaltfish
Photo URL    : https://www.flickr.com/photos/cobaltfish/13565636344/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 27 11:50:06 GMT+0100 2014
Upload Date  : Tue Apr 1 20:23:16 GMT+0200 2014
Geotag Info  : Latitude:53.239372, Longitude:10.050500
Views        : 423
Comments     : 0


+---------+
|  TITLE  |
+---------+
Skunk


+---------------+
|  DESCRIPTION  |
+---------------+
Popping out of the house?


+--------+
|  TAGS  |
+--------+
Skunk 