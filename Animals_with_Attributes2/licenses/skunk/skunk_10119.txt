+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Wisconsin Department of Natural Resources
Photo URL    : https://www.flickr.com/photos/widnr/6511403945/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 11 07:04:18 GMT+0100 2010
Upload Date  : Wed Dec 14 18:05:07 GMT+0100 2011
Views        : 919
Comments     : 0


+---------+
|  TITLE  |
+---------+
Striped skunk litter


+---------------+
|  DESCRIPTION  |
+---------------+
Litters of skunks usually number four to six.  Youngsters don't venture far from their underground den until they are six to seven weeks old.


+--------+
|  TAGS  |
+--------+
Mustelidae "Mephitis mephitis" litter young 