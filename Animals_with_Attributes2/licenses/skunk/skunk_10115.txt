+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : NDomer73
Photo URL    : https://www.flickr.com/photos/ndomer73/4548287441/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 24 12:15:19 GMT+0200 2010
Upload Date  : Sat Apr 24 21:15:19 GMT+0200 2010
Geotag Info  : Latitude:44.585301, Longitude:-111.336473
Views        : 9,887
Comments     : 6


+---------+
|  TITLE  |
+---------+
Striped Skunk (Mephitis mephitis)  DSC_0030


+---------------+
|  DESCRIPTION  |
+---------------+
Lin spotted this Striped Skunk from our Honda CR-V at highway speed while we were southbound on ID Highway 20 from West Yellowstone MT to Moose WY in mid-April 2010. The more direct route from Yellowstone National Park to Grand Teton National Park via Yellowstone's South Entrance was still closed by snow. Given our avocation as wildlife photographers devoid of species bias, we quickly pulled over and grabbed our cameras. The skunk was foraging in snow along a stream that crossed the highway, where a concrete bridge abutment provided some measure of safety. It seemed to pay us no mind when, after taking a few photos while crouching behind the abutment, we ventured onto the snow in cautious pursuit. We followed at a distance of 50 feet until the skunk made a meandering U-turn and began closing the distance between us at an uncomfortable pace. Always the thoughtful one, Lin pointed out that the skunk seemed more agile than I in knee-deep snow, which triggered a tactical retreat on my part. But not before getting this shot and even a little bit of video from about 20 feet. That prompted some concern from Lin over the safety of her Nikon D-90, which I was using at the time, so I bid farewell to the skunk and returned to the car. In hindsight, we must have provided quite a spectacle for passersby as we trailed the skunk across the snow, cameras and tripod in hand. Things might have ended differently if the skunk had objected to our curiosity, but as things turned out the encounter yielded a fun and memorable addition to our species list.


+--------+
|  TAGS  |
+--------+
"14 april 2010" april 2010 mammal "striped skunk" skunk snow better best PhotoContest-TNC11 calendar "calendar 2011" 