+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BCNH09
Photo URL    : https://www.flickr.com/photos/38976602@N05/6348791771/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Nov 10 13:55:11 GMT+0100 2011
Upload Date  : Wed Nov 16 04:26:06 GMT+0100 2011
Views        : 180
Comments     : 2


+---------+
|  TITLE  |
+---------+
Striped Skunk


+---------------+
|  DESCRIPTION  |
+---------------+
Wicomico County,  Maryland.


+--------+
|  TAGS  |
+--------+
Maryland mammals 