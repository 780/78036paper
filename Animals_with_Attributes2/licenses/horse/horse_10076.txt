+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eXtensionHorses
Photo URL    : https://www.flickr.com/photos/64081615@N06/5842674117/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 24 17:44:13 GMT+0100 2006
Upload Date  : Fri Jun 17 20:41:04 GMT+0200 2011
Views        : 921
Comments     : 0


+---------+
|  TITLE  |
+---------+
avoidance behavior in horses


+---------------+
|  DESCRIPTION  |
+---------------+
Avoidance behavior in horses.


+--------+
|  TAGS  |
+--------+
horses eXtensionhorses horsebehavior riding 