+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BANAMINE
Photo URL    : https://www.flickr.com/photos/banamine/469255344/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 22 18:46:39 GMT+0200 2007
Upload Date  : Mon Apr 23 03:46:39 GMT+0200 2007
Views        : 3,335
Comments     : 1


+---------+
|  TITLE  |
+---------+
Circular-Quay


+---------------+
|  DESCRIPTION  |
+---------------+
Circular Quay was unable to give a definitive answer to those in search of a viable Kentucky Derby (G1) contender if one merely considers that he finished fifth in the Risen Star Stakes (G2) on February 10 at Fair Grounds.

But when one considers that the colt was forced to alter course to avoid a fallen jockey at the top of the stretch and then still came home while charging wide, then the question was certainly answered in the affirmative.

&quot;To hit that other horse [Slew's Tizzy] head on and be stopped the way he was and still finish like he did really impressed me,&quot; jockey John Velazquez said. &quot;He accelerated again from almost a dead stop and if nothing else the race really had to school him, because if he wasn't afraid of coming through horses after what happened, he never will be.&quot;

With a good trip in the Louisiana Derby (G2) Circular Quay answered any doubts about his tenacity as he stormed home from sixth to a 2 1/4-length win.

&quot;He really ran well,&quot; winning jockey John Velazquez said. &quot;&quot;The last time, when he got going, everything happened in front of him. You can't get stopped at the quarter pole and start running again. But today, he did it without any trouble.&quot;Adam Coglianese/NYRA photo


+--------+
|  TAGS  |
+--------+
"horse racing" 