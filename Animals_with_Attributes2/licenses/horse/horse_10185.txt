+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : J.harwood
Photo URL    : https://www.flickr.com/photos/harwood-images/5992090806/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 29 14:41:12 GMT+0200 2011
Upload Date  : Sun Jul 31 00:26:43 GMT+0200 2011
Geotag Info  : Latitude:53.056072, Longitude:-2.691135
Views        : 1,694
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cholmondeley Horse Trials 2011 (72).JPG


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Cholmondeley Horse Trials" 