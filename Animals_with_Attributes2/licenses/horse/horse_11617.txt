+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Margaux-Marguerite Duquesnoy
Photo URL    : https://www.flickr.com/photos/124559226@N08/15611068887/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 15 20:28:48 GMT+0100 2014
Upload Date  : Sat Nov 15 20:29:42 GMT+0100 2014
Views        : 2,428
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wilde horses


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wild horses greenery lake france crotoy cheveaux cheval lac sauvage 