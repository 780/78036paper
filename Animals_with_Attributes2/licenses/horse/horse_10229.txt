+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Last Hero
Photo URL    : https://www.flickr.com/photos/uwe_schubert/4530563003/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 18 15:02:29 GMT+0200 2010
Upload Date  : Sun Apr 18 15:25:37 GMT+0200 2010
Views        : 443
Comments     : 0


+---------+
|  TITLE  |
+---------+
Das dunkle Pferd


+---------------+
|  DESCRIPTION  |
+---------------+
In Raitschin.


+--------+
|  TAGS  |
+--------+
pferd horse Frühling spring wolken clouds franken april s1000fd 