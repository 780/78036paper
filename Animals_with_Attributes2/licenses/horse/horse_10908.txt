+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MarilynJane
Photo URL    : https://www.flickr.com/photos/marilynjane/3474364090/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 25 11:00:19 GMT+0200 2009
Upload Date  : Sat Apr 25 22:06:08 GMT+0200 2009
Views        : 462
Comments     : 1


+---------+
|  TITLE  |
+---------+
Horses


+---------------+
|  DESCRIPTION  |
+---------------+
The last time I took this picture it was very cold with snow on the ground!


+--------+
|  TAGS  |
+--------+
Horses Animals "Okeford Hill" 