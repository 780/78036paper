+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Chez Eskay
Photo URL    : https://www.flickr.com/photos/29222579@N05/7668672664/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 7 13:53:40 GMT+0200 2012
Upload Date  : Sun Jul 29 16:02:26 GMT+0200 2012
Geotag Info  : Latitude:46.915110, Longitude:-2.058938
Views        : 180
Comments     : 0


+---------+
|  TITLE  |
+---------+
La maison de l'âne


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
France Frankreich Farm Holiday Urlaub Vendée Horse Pferd 