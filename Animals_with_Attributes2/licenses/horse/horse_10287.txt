+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : deischi
Photo URL    : https://www.flickr.com/photos/deischi/14477121411/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 21 13:48:20 GMT+0200 2014
Upload Date  : Sun Jun 22 16:05:32 GMT+0200 2014
Geotag Info  : Latitude:48.486936, Longitude:13.978627
Views        : 129
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC08026.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Altenfelden Kutschen Marathon Pferd Langhalsen "Upper Austria" Austria 