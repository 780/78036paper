+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rhea C.
Photo URL    : https://www.flickr.com/photos/orchid9/3711949926/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 11 09:53:46 GMT+0200 2009
Upload Date  : Sun Jul 12 05:06:07 GMT+0200 2009
Views        : 98
Comments     : 0


+---------+
|  TITLE  |
+---------+
Horses


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
family Jacksonville "St Augustine" horses 