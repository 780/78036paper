+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bambe1964
Photo URL    : https://www.flickr.com/photos/bambe1964/7122878843/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 28 14:04:22 GMT+0200 2012
Upload Date  : Sun Apr 29 02:24:42 GMT+0200 2012
Views        : 1,004
Comments     : 0


+---------+
|  TITLE  |
+---------+
glamour shot


+---------------+
|  DESCRIPTION  |
+---------------+
after many months of looking I have purchased a PRE Andalusian yearling. His name is 'Acero' which is Spanish for 'steele'. He arrived today. None of these shots are art they are snapshots but I don't care. :)


+--------+
|  TAGS  |
+--------+
yearling horse andalusian spanish 