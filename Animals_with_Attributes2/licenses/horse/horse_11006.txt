+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Moyan_Brenn
Photo URL    : https://www.flickr.com/photos/aigle_dore/14110669388/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 29 18:21:15 GMT+0100 2014
Upload Date  : Fri Jul 18 06:19:28 GMT+0200 2014
Geotag Info  : Latitude:64.129652, Longitude:-21.941885
Views        : 21,546
Comments     : 1


+---------+
|  TITLE  |
+---------+
Horses


+---------------+
|  DESCRIPTION  |
+---------------+
Beautiful Icelandic horses at sunet in the heart of Iceland, near Geyser

Add me on <a href="http://www.facebook.com/MoyanBrenn" rel="nofollow"><b>FACEBOOK</b> </a> and <a href="http://twitter.com/moyan_brenn" rel="nofollow"><b>TWITTER</b></a>! 

My travel blog: <a href="http://www.earthincolors.com" rel="nofollow">www.earthincolors.com</a> 

My official website: <a href="http://www.moyanbrenn.com" rel="nofollow">www.moyanbrenn.com</a> 

<b>My Portfolio</b>: Adobe, CNN, Lonely Planet, Yahoo, Alitalia, SkyTG24, Aeroflot, , Adobe, Huffington Post, The Guardian, and more ( <a href="http://moyanbrenn.com/moyan-brenn-portfolio/" rel="nofollow"><u>here</u></a> the complete list)

<b><u>************** COPYRIGHTS TERMS AND CONDITIONS **************</u></b> 

<i>This image is property of Moyan Brenn and registered to the U.S. Copyright Office. The basic license offered for this image is called C.C. Attr. 2.0 Gen. (CC BY 2.0) or Type-1. 
A more extended license for this image called Royalty Free Non Exclus. or Type-2 which permits the &quot;uncredited usage&quot; is available from €39 upon request (price depends by the use and company size). The purchase of the Type-2 license will also entitle to obtain a Max quality version of the same image shipped by email (quality of Flickr version is limited), more suitable for large printing. The same Type 2 license is available also in &quot;Exclusive&quot; form, called Type 3. More details about all licenses inside the official Moyan Brenn legal terms statement linked below. 
Any ABUSE and VIOLATIONS will be legally PURSUED for statutory damage. Before and during the use of the present picture, you are obliged to adhere to the Moyan Brenn Copyrights Legal Terms reported here --&gt; <a href="https://www.flickr.com/people/aigle_dore/"><u>click here</u></a></i>


+--------+
|  TAGS  |
+--------+
Horses 