+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kusabi
Photo URL    : https://www.flickr.com/photos/kusabi/6697452811/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 30 14:16:54 GMT+0100 2011
Upload Date  : Sat Jan 14 23:56:15 GMT+0100 2012
Views        : 144
Comments     : 0


+---------+
|  TITLE  |
+---------+
The Happy Horses of Norderney


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
horse horses Pferd Pferde Norderney 