+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : TCM1003
Photo URL    : https://www.flickr.com/photos/tcm1003/15794314660/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 23 13:31:43 GMT+0200 2014
Upload Date  : Tue Dec 9 09:03:12 GMT+0100 2014
Views        : 110
Comments     : 0


+---------+
|  TITLE  |
+---------+
Alle Viere von sich


+---------------+
|  DESCRIPTION  |
+---------------+
Sich wälzendes Pferd bei Bad Lauterberg


+--------+
|  TAGS  |
+--------+
Harz Pferd "Bad Lauterberg" 