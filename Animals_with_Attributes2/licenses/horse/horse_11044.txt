+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sunfox
Photo URL    : https://www.flickr.com/photos/sunfox/5915693545/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 21 13:14:09 GMT+0200 2011
Upload Date  : Fri Jul 8 20:04:31 GMT+0200 2011
Views        : 1,699
Comments     : 0


+---------+
|  TITLE  |
+---------+
Icelandic horses


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Icelandic horses" Iceland horses 