+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Peter O'Connor aka anemoneprojectors
Photo URL    : https://www.flickr.com/photos/anemoneprojectors/5339132239/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 9 14:54:00 GMT+0100 2011
Upload Date  : Sun Jan 9 17:58:44 GMT+0100 2011
Geotag Info  : Latitude:51.916609, Longitude:-0.215047
Views        : 4,905
Comments     : 0


+---------+
|  TITLE  |
+---------+
Horse


+---------------+
|  DESCRIPTION  |
+---------------+
This might have made a better 365 project photo but I already did a horse. This is the field where last spring, a notice went up saying that building work would start soon and the horses should all be vacated. No work started so this winter the horses are back. 

These two horses were not friendly, they were scared.

9 January 2011


+--------+
|  TAGS  |
+--------+
Horse Equus "Equus ferus caballus" "Equus ferus" Equidae Mammal Animal Stevenage Hertfordshire 