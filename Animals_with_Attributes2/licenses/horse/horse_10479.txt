+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : George Donnelly
Photo URL    : https://www.flickr.com/photos/cyklo/2276866497/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 27 15:13:02 GMT+0100 2008
Upload Date  : Tue Feb 19 17:55:39 GMT+0100 2008
Views        : 230
Comments     : 0


+---------+
|  TITLE  |
+---------+
Horse in La Mota, Medellin


+---------------+
|  DESCRIPTION  |
+---------------+
These poor horses work pulling carts during the week but get Saturday night and Sunday off. They spend their free time wandering the neighborhood eating grass.


+--------+
|  TAGS  |
+--------+
horse medellin colombia 