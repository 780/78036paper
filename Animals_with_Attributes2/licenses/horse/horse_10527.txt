+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Patricia (aka look lovely)
Photo URL    : https://www.flickr.com/photos/22308653@N07/4580370671/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 14 19:13:32 GMT+0200 2009
Upload Date  : Wed May 5 11:08:56 GMT+0200 2010
Views        : 6,289
Comments     : 4


+---------+
|  TITLE  |
+---------+
Horse whispers?


+---------------+
|  DESCRIPTION  |
+---------------+
The college has just acquired two shire horse, and two students from the equine department brought them out to show the governors at the end of their meeting.
Meeting the horses was a lot more fun than sitting through the meeting.


+--------+
|  TAGS  |
+--------+
horses "shire horses" 