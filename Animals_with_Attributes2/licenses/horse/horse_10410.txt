+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Charles D P Miller
Photo URL    : https://www.flickr.com/photos/cdpm/4576422402/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 3 15:03:46 GMT+0200 2010
Upload Date  : Mon May 3 23:49:35 GMT+0200 2010
Views        : 654
Comments     : 0


+---------+
|  TITLE  |
+---------+
Heavy Horses at Southsea


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Shire Horses 