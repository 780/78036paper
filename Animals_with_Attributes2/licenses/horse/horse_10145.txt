+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : My Buffo
Photo URL    : https://www.flickr.com/photos/mybuffo/16364912371/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 18 12:35:28 GMT+0100 2015
Upload Date  : Sun Jan 25 22:33:18 GMT+0100 2015
Views        : 250
Comments     : 0


+---------+
|  TITLE  |
+---------+
Caballo en contexto


+---------------+
|  DESCRIPTION  |
+---------------+
Pasando frío.


+--------+
|  TAGS  |
+--------+
león españa spain spanien invierno winter caballo horse pferd nieve snow schnee 