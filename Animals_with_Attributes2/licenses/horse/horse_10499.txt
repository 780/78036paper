+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Adrian Parnham
Photo URL    : https://www.flickr.com/photos/adrian_parnham/3207284009/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 22 11:57:35 GMT+0100 2008
Upload Date  : Mon Jan 19 00:29:56 GMT+0100 2009
Views        : 87,665
Comments     : 9


+---------+
|  TITLE  |
+---------+
Horse Portrait


+---------------+
|  DESCRIPTION  |
+---------------+
A horse near Petts Wood, Kent, UK.


+--------+
|  TAGS  |
+--------+
Horses 