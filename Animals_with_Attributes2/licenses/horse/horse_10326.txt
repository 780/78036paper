+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Chris Kantos
Photo URL    : https://www.flickr.com/photos/chriskantos/2173564066/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 6 15:03:42 GMT+0100 2008
Upload Date  : Mon Jan 7 00:21:34 GMT+0100 2008
Views        : 182
Comments     : 0


+---------+
|  TITLE  |
+---------+
Feeding the Horses!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
horses winter friends 