+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Renate Dodell
Photo URL    : https://www.flickr.com/photos/dorena-wm/5235255958/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 4 13:08:00 GMT+0100 2010
Upload Date  : Sun Dec 5 19:01:35 GMT+0100 2010
Geotag Info  : Latitude:47.800506, Longitude:11.208183
Views        : 1,019
Comments     : 50


+---------+
|  TITLE  |
+---------+
Pleasure in the snow


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://bighugelabs.com/onblack.php?id=5235255958&amp;size=large" rel="nofollow">View On Black</a>

Gesehen auf einer Weide bei Eberfing im Oberland.
Viewed on a pasture at Eberfing in the Upper Country / Bavaria.

My pictures are published under &quot;All rights reserved&quot;. If you want to use an image - either for commercial or non-profit purposes, feel free to contact me. I'm sure we'll find an agreement ...


+--------+
|  TAGS  |
+--------+
dorena-wm pferd horse winter schnee snow licht light sonne sun baum tree zaun fence weiss white 