+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Qrodo Photos
Photo URL    : https://www.flickr.com/photos/qrodoflickr/3776634542/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 10 16:30:19 GMT+0100 2007
Upload Date  : Sat Aug 1 01:51:38 GMT+0200 2009
Views        : 6,253
Comments     : 2


+---------+
|  TITLE  |
+---------+
Sport action


+---------------+
|  DESCRIPTION  |
+---------------+
Horse racing


+--------+
|  TAGS  |
+--------+
Horse racing 