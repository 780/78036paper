+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lorenkerns
Photo URL    : https://www.flickr.com/photos/lorenkerns/9581032545/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 4 13:13:43 GMT+0200 2013
Upload Date  : Sat Aug 24 16:27:14 GMT+0200 2013
Views        : 5,407
Comments     : 0


+---------+
|  TITLE  |
+---------+
Horses in Crow Country


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Montana horses 