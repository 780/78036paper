+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Frank de Kleine
Photo URL    : https://www.flickr.com/photos/frankdekleine/2629355642/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 16 12:37:51 GMT+0100 2007
Upload Date  : Tue Jul 1 23:21:11 GMT+0200 2008
Views        : 508
Comments     : 0


+---------+
|  TITLE  |
+---------+
Horses eating hay


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
peize paarden horses netherlands holland 