+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : WonderfulSnaps.com
Photo URL    : https://www.flickr.com/photos/wonderfulsnaps/15457297837/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 27 10:06:07 GMT+0100 2014
Upload Date  : Mon Oct 27 18:08:00 GMT+0100 2014
Views        : 285
Comments     : 0


+---------+
|  TITLE  |
+---------+
WS-Animals-Rosice-28


+---------------+
|  DESCRIPTION  |
+---------------+
Animals in action


+--------+
|  TAGS  |
+--------+
horse "horse feeding" 