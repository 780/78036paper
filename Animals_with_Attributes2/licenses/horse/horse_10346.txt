+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Patrick Doheny
Photo URL    : https://www.flickr.com/photos/14132971@N05/2322207989/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 10 12:55:36 GMT+0100 2008
Upload Date  : Mon Mar 10 03:03:12 GMT+0100 2008
Views        : 10,878
Comments     : 1


+---------+
|  TITLE  |
+---------+
For J.M. Dressage, Southlands Riding Club


+---------------+
|  DESCRIPTION  |
+---------------+
What an achievement!  

 A slim young girl  
Powerful horse moving
 In perfect symmetry 

Equestrian Haiku


+--------+
|  TAGS  |
+--------+
horses girls "equestrian dressage" 