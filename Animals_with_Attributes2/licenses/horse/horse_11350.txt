+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Wolfgang Staudt
Photo URL    : https://www.flickr.com/photos/wolfgangstaudt/2439657298/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 20 09:17:11 GMT+0100 2008
Upload Date  : Thu Apr 24 22:52:30 GMT+0200 2008
Views        : 7,391
Comments     : 51


+---------+
|  TITLE  |
+---------+
Portrait


+---------------+
|  DESCRIPTION  |
+---------------+
Get<a href="http://flickr.com/photos/wolfgangstaudt/2439657298/sizes/l/">  here  </a>  a large view!

Das <b>Camargue-Pferd</b> ist ein kleines robustes Wildpferd, das in der Camargue - dem Rhône-Delta Südfrankreichs - lebt.

Das Camargue-Pferd ist kräftig gebaut, mit kurzem Hals und steiler Schulter. Meistens ist er ramsnasig und hat kräftigen Behang. Bei ihm ist auch Geißbartbildung möglich. Die Fohlen sind bei der Geburt dunkelbraun bis schwarz,hellen jedoch mit zunehmendem Alter auf. Mit etwa 10 Jahren sind sie vollkommen weiß. Das Camarguepferd gilt als spätwüchsig und ist erst mit 7-8 Jahren ausgewachsen. Seine Hufe sind überdurchschnittlich groß um ein Einsinken im Sumpf zu verhindern, aber trotzdem sehr hart. Es hat flach im Kopf liegende Augen und kleine Ohren. Es erreicht mit durchschnittlich mehr als 25 Jahren ein für Wildpferde sehr hohes Alter.

Das Camargue-Pferd gilt als sehr widerstandsfähig und genügsam. Diese Eigenschaft geht auf seine harten Lebensbedingungen im Rhone-Delta zurück, wo es im Sommer großer Hitze, und in den übrigen Jahreszeiten ständig kaltem feuchten Boden ausgesetzt ist dazu kommt noch der Mistral, der im Jahresdurchschnitt 63 Prozent weht. Es kann mit geschlossenen Nüstern unter Wasser fressen.

From <a href="http://wikipedia.org/" rel="nofollow"> Wikipedia</a>, the free encyclopedia


+--------+
|  TAGS  |
+--------+
"Camargue horse" Horse Camargue "River Rhône delta" "Parc Régional de Camargue" "Regional park" Flamingo Flammant Rhone カマルグ Камарг Camarga France "South of France" "Mediterrian Sea" Mittelmeer "Wolfgang Staudt" Nikon Sigma "Nikon D300" Aube Saintes-Maries-de-la-Mer Provence tourist Holidays Travel "Travel Photographie" Reisefotografie Bouches-du-Rhône "côte méditerranéenne" Spring ABigFave Flickrcolour 