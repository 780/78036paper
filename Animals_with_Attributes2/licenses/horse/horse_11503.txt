+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cbr_case
Photo URL    : https://www.flickr.com/photos/ralf_nolte/2515170275/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 22 14:47:42 GMT+0200 2008
Upload Date  : Fri May 23 11:01:38 GMT+0200 2008
Views        : 169
Comments     : 0


+---------+
|  TITLE  |
+---------+
Three lucky ones


+---------------+
|  DESCRIPTION  |
+---------------+
This is Casjen on El Capitan after doing one of their first tournaments. Even though they were not ranked in this run, both were happy to have solved the whole parcours. For the photo: this is NOT cropped, straightened or altered in any way. just converted from RAW with PS, imported into iPhoto and uploaded. I think the G2 is not a bad camera at all! So i consider myself as the third lucky one, pushing the knob just the right second. (they were still moving). We had a lot of light there, so i could drop ISO to 50, while still getting a short exposure.


+--------+
|  TAGS  |
+--------+
casjen "el capitan" pferd turnier lage 