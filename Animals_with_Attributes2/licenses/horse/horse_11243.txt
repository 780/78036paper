+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eXtensionHorses
Photo URL    : https://www.flickr.com/photos/64081615@N06/5887978733/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 23 18:46:44 GMT+0200 2009
Upload Date  : Thu Jun 30 20:05:46 GMT+0200 2011
Views        : 1,750
Comments     : 0


+---------+
|  TITLE  |
+---------+
Oregon2_neglected horse


+---------------+
|  DESCRIPTION  |
+---------------+
Horse in poor condition. Photo source: Patricia Evans, Utah State University. 

More info: <a href="http://www.extension.org/pages/20162/unwanted-horses" rel="nofollow">www.extension.org/pages/20162/unwanted-horses</a>


+--------+
|  TAGS  |
+--------+
horse extensionhorses 