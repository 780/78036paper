+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : andrew_j_w
Photo URL    : https://www.flickr.com/photos/andrew_j_w/2635656651/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 24 10:58:14 GMT+0200 2008
Upload Date  : Fri Jul 4 14:53:38 GMT+0200 2008
Geotag Info  : Latitude:50.491425, Longitude:-3.970527
Views        : 731
Comments     : 0


+---------+
|  TITLE  |
+---------+
Horse and Foal


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dartmoor animal horse foal 