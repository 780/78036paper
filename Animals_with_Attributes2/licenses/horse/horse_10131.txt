+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jans canon
Photo URL    : https://www.flickr.com/photos/43158397@N02/4929596165/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 24 10:04:44 GMT+0200 2010
Upload Date  : Thu Aug 26 21:01:36 GMT+0200 2010
Views        : 293
Comments     : 1


+---------+
|  TITLE  |
+---------+
Brugge (Bruges) horses


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Brugge (Bruges) horses 