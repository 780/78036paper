+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : peggydavis66
Photo URL    : https://www.flickr.com/photos/11441121@N04/2493024778/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 6 12:19:43 GMT+0200 2008
Upload Date  : Wed May 14 20:43:08 GMT+0200 2008
Views        : 1,630
Comments     : 0


+---------+
|  TITLE  |
+---------+
P4060022


+---------------+
|  DESCRIPTION  |
+---------------+
Young and old, Arab &amp; Part Arab, horses


+--------+
|  TAGS  |
+--------+
Arabian horses gray 