+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : muensterland
Photo URL    : https://www.flickr.com/photos/muensterland/4720536312/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 21 01:53:23 GMT+0200 2010
Upload Date  : Mon Jun 21 10:53:23 GMT+0200 2010
Geotag Info  : Latitude:52.134620, Longitude:7.730684
Views        : 710
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ladbergen Pferdeweise


+---------------+
|  DESCRIPTION  |
+---------------+
Ruhig und ländlich ist die Umgebung von Ladbergen.
Mehr Infos auf <a href="http://www.muensterland-tourismus.de" rel="nofollow">www.muensterland-tourismus.de</a>


+--------+
|  TAGS  |
+--------+
Ladbergen Münsterland Pferd Weide Muensterland Dortmund-Ems-Kanal Fachwerk Westfalen Ort Stadt "Ort im Münsterland" river horse 