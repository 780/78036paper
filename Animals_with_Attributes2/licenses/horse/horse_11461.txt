+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BANAMINE
Photo URL    : https://www.flickr.com/photos/banamine/458534997/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 14 01:57:36 GMT+0200 2007
Upload Date  : Sat Apr 14 10:57:36 GMT+0200 2007
Views        : 3,625
Comments     : 0


+---------+
|  TITLE  |
+---------+
Street-Sounds{inside}


+---------------+
|  DESCRIPTION  |
+---------------+
Photo by Z
by Jeff Apel

Street Sounds turned back a determined stretch bid from runner-up Forever Together and earned her first graded stakes with a neck victory in the $250,000 Stonerside Beaumont Stakes (G2) on Thursday at Keeneland Race Course.

Breaking sharp, Street Sounds was positioned five wide at the start and raced in third through a quarter-mile in :23.26, approximately a half-length behind Palace Pier. The Street Cry (Ire) filly, with Edgar Prado aboard, dueled three wide on the turn with Palace Pier and opened a one-length lead in early stretch. After putting away Palace Pier, Street Sounds launched a stiff drive to the wire, finishing the seven-furlong race in 1:24.93 on a fast track.

The victory was Prado's third stakes win during the first four days of the Lexington track’s spring meet.

“She showed a lot of courage,” Prado said. “I saw the horse on the outside [Forever Together] coming, and she gave me an extra effort.”

“We weren’t sure we were making the right move because she did so well on the turf,” winning trainer Michael Matz said. “But she has trained well on the Polytrack and it worked out well.”

Grade 2 winner Forever Together, the 3-to-2 favorite who was unbeaten in three previous starts, finished second, 1 3/4 lengths in front of stakes winner Palace Pier.

Hidden Creek Farm’s Street Sounds won the Selima Stakes on November 25 at Laurel Park in her previous start. The dark bay or brown filly also finished third in the 2006 JPMorgan Chase Jessamine Stakes at Keeneland.

Bred in Ontario, Street Sounds is out of Rare Opportunity, by Danzig Connection.


+--------+
|  TAGS  |
+--------+
"horse racing" 