+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : shioshvili
Photo URL    : https://www.flickr.com/photos/vshioshvili/181008387/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 2 12:04:14 GMT+0200 2006
Upload Date  : Mon Jul 3 23:02:27 GMT+0200 2006
Geotag Info  : Latitude:41.749799, Longitude:43.520278
Views        : 558
Comments     : 5


+---------+
|  TITLE  |
+---------+
A Horse


+---------------+
|  DESCRIPTION  |
+---------------+
Now that Google has added high res images for some parts of Georgia, and luckily Bakuriani is covered, here's the <a href="http://www.flagr.com/flags/vshioshvili/view/11283" rel="nofollow">Exact location</a>, within a few meters...


+--------+
|  TAGS  |
+--------+
horse brown grass bakuriani georgia 