+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : zak mc
Photo URL    : https://www.flickr.com/photos/zakmc/3580621703/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 23 19:29:21 GMT+0200 2009
Upload Date  : Sun May 31 12:58:34 GMT+0200 2009
Views        : 302
Comments     : 2


+---------+
|  TITLE  |
+---------+
Cavalli selvaggi


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://www.youtube.com/watch?v=EhVLiHPUOIM" rel="nofollow">Wild Horses</a>


+--------+
|  TAGS  |
+--------+
cavalli horses Pferden abruzzo 