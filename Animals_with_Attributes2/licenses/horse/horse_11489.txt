+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neillwphoto
Photo URL    : https://www.flickr.com/photos/neillwphoto/14768801132/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 23 14:52:31 GMT+0200 2014
Upload Date  : Mon Jul 28 20:04:05 GMT+0200 2014
Views        : 990
Comments     : 0


+---------+
|  TITLE  |
+---------+
The Four Horses


+---------------+
|  DESCRIPTION  |
+---------------+
Whilst taking our detour through the fields we came across this group of beautiful horses.


+--------+
|  TAGS  |
+--------+
horses field white 