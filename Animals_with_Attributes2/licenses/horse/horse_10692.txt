+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : s1ng0
Photo URL    : https://www.flickr.com/photos/s1ng0/6620478127/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jan 2 15:30:14 GMT+0100 2012
Upload Date  : Mon Jan 2 16:30:14 GMT+0100 2012
Geotag Info  : Latitude:53.397739, Longitude:-2.701940
Views        : 931
Comments     : 0


+---------+
|  TITLE  |
+---------+
Horse


+---------------+
|  DESCRIPTION  |
+---------------+
Horse grazing in Bold Heath


+--------+
|  TAGS  |
+--------+
horse grass farm 