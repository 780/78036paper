+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brittanylynae
Photo URL    : https://www.flickr.com/photos/brittanylynae/2251876116/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 20 16:50:46 GMT+0200 2005
Upload Date  : Sat Feb 9 03:26:59 GMT+0100 2008
Views        : 843
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hannah and Spirit


+---------------+
|  DESCRIPTION  |
+---------------+
Quarter Horse and Foal


+--------+
|  TAGS  |
+--------+
"Quarter Horse" "Quarter Horse Foal" 