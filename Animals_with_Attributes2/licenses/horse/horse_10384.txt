+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Adam J Skowronski
Photo URL    : https://www.flickr.com/photos/adam_skowronski/14995531831/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 24 22:09:39 GMT+0200 2004
Upload Date  : Fri Aug 22 14:50:23 GMT+0200 2014
Views        : 555
Comments     : 0


+---------+
|  TITLE  |
+---------+
Grazing Horse


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Horse 