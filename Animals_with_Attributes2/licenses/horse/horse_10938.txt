+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : David Feltkamp
Photo URL    : https://www.flickr.com/photos/darwinist/5783672957/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 31 19:04:13 GMT+0200 2011
Upload Date  : Wed Jun 1 02:34:56 GMT+0200 2011
Geotag Info  : Latitude:52.418544, Longitude:4.987478
Views        : 49,971
Comments     : 5


+---------+
|  TITLE  |
+---------+
Horses


+---------------+
|  DESCRIPTION  |
+---------------+
And on the way back to Amsterdam, I ran into these horses I just HAD to photograph.


+--------+
|  TAGS  |
+--------+
waterland horses paarden 