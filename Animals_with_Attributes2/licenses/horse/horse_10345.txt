+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Photommo
Photo URL    : https://www.flickr.com/photos/photommo/15218702022/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 10 12:12:11 GMT+0200 2014
Upload Date  : Fri Sep 12 22:57:41 GMT+0200 2014
Geotag Info  : Latitude:43.314187, Longitude:-103.605194
Views        : 607
Comments     : 0


+---------+
|  TITLE  |
+---------+
Follow The Leader


+---------------+
|  DESCRIPTION  |
+---------------+
Black Hills Wild Horse Sanctuary, Hot Springs, SD


+--------+
|  TAGS  |
+--------+
horses IRAM 