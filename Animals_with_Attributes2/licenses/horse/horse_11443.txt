+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 10, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : GaryRHess
Photo URL    : https://www.flickr.com/photos/pofq/379368739/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 4 08:40:39 GMT+0100 2007
Upload Date  : Sun Feb 4 17:40:39 GMT+0100 2007
Views        : 1,125
Comments     : 3


+---------+
|  TITLE  |
+---------+
Snow Horse


+---------------+
|  DESCRIPTION  |
+---------------+
Sometimes I like to stare at cameras and look as cute as possible! Oh... is it snowing too?


+--------+
|  TAGS  |
+--------+
horse winter snow mustang face 