+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : star5112
Photo URL    : https://www.flickr.com/photos/johnjoh/717417221/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 30 15:10:15 GMT+0200 2007
Upload Date  : Wed Jul 4 20:43:26 GMT+0200 2007
Geotag Info  : Latitude:37.786190, Longitude:-122.433410
Views        : 173
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chihuahua


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2007 "San Francisco" "Fillmore Street" Jazz Festival food walking street fair drinks snacks dogs pets pet dog 