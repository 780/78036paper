+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Fido Factor
Photo URL    : https://www.flickr.com/photos/fidofactor/4172931975/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 9 13:37:50 GMT+0100 2009
Upload Date  : Thu Dec 10 05:48:00 GMT+0100 2009
Geotag Info  : Latitude:37.766957, Longitude:-122.413015
Views        : 107
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chihuahuas looking for a home


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dog Chihuahua Shelter 