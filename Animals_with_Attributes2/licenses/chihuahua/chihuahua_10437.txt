+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Valerie Everett
Photo URL    : https://www.flickr.com/photos/valeriebb/3991227360/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Oct 7 14:59:34 GMT+0200 2009
Upload Date  : Wed Oct 7 22:39:49 GMT+0200 2009
Views        : 126
Comments     : 1


+---------+
|  TITLE  |
+---------+
Pup


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Chihuahua puppy female 