+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sellers Patton
Photo URL    : https://www.flickr.com/photos/sellerspatton/1348374245/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 3 10:59:05 GMT+0200 2007
Upload Date  : Sun Sep 9 06:10:24 GMT+0200 2007
Views        : 694
Comments     : 0


+---------+
|  TITLE  |
+---------+
This Dog is Wearing Cowboy Boots.


+---------------+
|  DESCRIPTION  |
+---------------+
Louisville Flea Market


+--------+
|  TAGS  |
+--------+
cowboyboots chihuahua "flea market" 