+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Alt-Ctrl-Tom
Photo URL    : https://www.flickr.com/photos/46585418@N00/1922810453/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 17 00:18:26 GMT+0100 2007
Upload Date  : Thu Nov 8 22:28:27 GMT+0100 2007
Geotag Info  : Latitude:47.614990, Longitude:-122.326025
Views        : 1,104
Comments     : 0


+---------+
|  TITLE  |
+---------+
Choose Me, Pick Me, Love Me


+---------------+
|  DESCRIPTION  |
+---------------+
this is the shot of Vida from the rescue agency - blown up in size a bit, and touched up a little to try to make her stand out some more against the background.  I fell in love with her instantly.  Apparently  there is such a thing as love at first sight :)

If Meredith Gray had Vida's exponential cute factor, McDreamy would be even more love-struck :)


+--------+
|  TAGS  |
+--------+
Vida chihuahua "dachshund mix" sweet cute adorable dog puppy "rescue dog" 