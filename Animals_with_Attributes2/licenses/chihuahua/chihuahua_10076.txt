+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rikkis_refuge
Photo URL    : https://www.flickr.com/photos/rikkis_refuge/2696308177/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 23 16:43:27 GMT+0200 2008
Upload Date  : Thu Jul 24 00:09:51 GMT+0200 2008
Views        : 1,618
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chico aka Frank


+---------------+
|  DESCRIPTION  |
+---------------+
This little old guy came to Rikki's after he was literally drop kicked out of a car into a parking lot in Orange, VA. He was a bit ornery -- but who wouldn't be after that experience? 

He was adopted to a family in Richmond, VA who had another Chihuahua and was renamed Frank.


+--------+
|  TAGS  |
+--------+
chihuahua dog rikkis chico frank 