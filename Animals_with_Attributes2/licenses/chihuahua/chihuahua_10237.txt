+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : joe.ross
Photo URL    : https://www.flickr.com/photos/joeybones/5670503033/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 29 18:57:56 GMT+0200 2011
Upload Date  : Sat Apr 30 01:04:21 GMT+0200 2011
Views        : 908
Comments     : 0


+---------+
|  TITLE  |
+---------+
Image: Esteban the  Paraplegic Chihuahua Needs a Home!


+---------------+
|  DESCRIPTION  |
+---------------+
Originally posted via email to <a href="http://joeross.me/image-esteban-the-paraplegic-chihuahua-needs" rel="nofollow">joeross.me</a>.


+--------+
|  TAGS  |
+--------+
chihuahua dogs Image pets 