+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : freestock-by-melissasundman
Photo URL    : https://www.flickr.com/photos/105677693@N05/10420070075/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 3 11:24:36 GMT+0200 2012
Upload Date  : Tue Oct 22 13:23:38 GMT+0200 2013
Views        : 879
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chihuahua (freestock, read description)


+---------------+
|  DESCRIPTION  |
+---------------+
You can use the picture and edit it if you want, but &quot;MSPhotography&quot; needs to stay visible in the picture.

If there is no text in the picture you just have to write is visable in either the picture or under it.


+--------+
|  TAGS  |
+--------+
(no tags)