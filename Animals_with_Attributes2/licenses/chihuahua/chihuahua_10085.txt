+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : CoZy GloW PhotographY
Photo URL    : https://www.flickr.com/photos/125862484@N08/16974756080/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 13 19:23:23 GMT+0200 2015
Upload Date  : Thu Apr 16 02:26:23 GMT+0200 2015
Views        : 119
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chihuahua Pup Maisey


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Chihuahua. Dogs. Pets. Cute. Puppy. Northampton. NorthamptonPhototographers" "Cihuahua. Dogs. Pets. Cute. Puppy. Northampton. NorthamptonPhotographers. Northampton Images. Calvin Walters. Cozy Glow Photography." images TeacupChihuahua NorthamptonImages 