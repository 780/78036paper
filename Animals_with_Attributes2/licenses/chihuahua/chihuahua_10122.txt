+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ghost-Monkey
Photo URL    : https://www.flickr.com/photos/ghost-monkey/6675093737/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 10 00:45:57 GMT+0100 2012
Upload Date  : Tue Jan 10 22:13:06 GMT+0100 2012
Views        : 974
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chihuahua


+---------------+
|  DESCRIPTION  |
+---------------+
Chihuahua dog.


+--------+
|  TAGS  |
+--------+
Chihuahua 