+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/6898754582/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 3 17:50:19 GMT+0200 2012
Upload Date  : Wed Apr 4 15:08:30 GMT+0200 2012
Views        : 325
Comments     : 1


+---------+
|  TITLE  |
+---------+
Wo bleibst du?


+---------------+
|  DESCRIPTION  |
+---------------+
April 2012
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog dalmatiner female dalmatian Dalmatinac hund junghund young 