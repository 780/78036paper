+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/16063794012/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 6 14:41:25 GMT+0200 2014
Upload Date  : Sat Dec 20 18:16:59 GMT+0100 2014
Views        : 1,270
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cooles Fleckentier


+---------------+
|  DESCRIPTION  |
+---------------+
April 2014
Canon EOS 60D
EF 85mm f/1.8 USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hündin female dalmatiner dalmatian spaß fun 