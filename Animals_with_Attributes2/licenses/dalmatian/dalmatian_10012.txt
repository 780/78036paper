+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/5958447859/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 7 17:09:23 GMT+0200 2011
Upload Date  : Wed Jul 20 22:07:36 GMT+0200 2011
Views        : 572
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mein Loch!


+---------------+
|  DESCRIPTION  |
+---------------+
Juli 2011
Canon EOS 40D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog welpe puppy dalmatiner female Dalmatian Dalmatinac 