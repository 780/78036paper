+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/8467386983/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 10 14:57:56 GMT+0100 2013
Upload Date  : Tue Feb 12 16:17:49 GMT+0100 2013
Views        : 1,381
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dalmirundkopf


+---------------+
|  DESCRIPTION  |
+---------------+
Februar 2013
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac schnee snow 