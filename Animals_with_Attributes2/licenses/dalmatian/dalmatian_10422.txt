+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : H.A.S PhotoDesigns~Heart+Soul~
Photo URL    : https://www.flickr.com/photos/hasitha_tudugalle/8567501625/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 23 08:10:51 GMT+0200 2010
Upload Date  : Mon Mar 18 12:18:02 GMT+0100 2013
Geotag Info  : Latitude:6.927199, Longitude:79.872200
Views        : 878
Comments     : 2


+---------+
|  TITLE  |
+---------+
The Lone Dalmatian


+---------------+
|  DESCRIPTION  |
+---------------+
Colombo Sri Lanka


+--------+
|  TAGS  |
+--------+
Dog "Sri Lanka" Colombo Dalmatian Dogs Pet Pets Dalmatians Animal Animals Mammal 