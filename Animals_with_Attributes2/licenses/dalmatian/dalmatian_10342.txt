+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/14591026490/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 15 18:12:49 GMT+0200 2014
Upload Date  : Tue Jul 29 18:15:07 GMT+0200 2014
Views        : 892
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ohren kraulen


+---------------+
|  DESCRIPTION  |
+---------------+
Juli 2014
Canon EOS 60D
EF 85mm f/1.8 USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hündin female dalmatiner dalmatian spaß fun freilauf gassi 