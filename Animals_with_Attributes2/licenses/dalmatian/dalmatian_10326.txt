+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/14458977452/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 7 19:17:20 GMT+0200 2014
Upload Date  : Thu Jun 19 21:22:14 GMT+0200 2014
Views        : 685
Comments     : 0


+---------+
|  TITLE  |
+---------+
Plantschen mit Herrchen


+---------------+
|  DESCRIPTION  |
+---------------+
Juni 2014
Canon EOS 60D
EF 85mm f/1.8 USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hündin female dalmatiner dalmatian baden bathing schwimmen swimming wasser water 