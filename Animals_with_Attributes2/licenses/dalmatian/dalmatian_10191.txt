+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/9283911543/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 7 18:08:15 GMT+0200 2013
Upload Date  : Sun Jul 14 20:00:24 GMT+0200 2013
Views        : 1,204
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zergeln


+---------------+
|  DESCRIPTION  |
+---------------+
Juli 2013
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac fun playing game spiel zergeln 