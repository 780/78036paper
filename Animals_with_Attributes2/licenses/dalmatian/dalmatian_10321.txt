+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/14362046885/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 4 19:01:44 GMT+0200 2014
Upload Date  : Sat Jun 7 00:01:48 GMT+0200 2014
Views        : 1,208
Comments     : 0


+---------+
|  TITLE  |
+---------+
Angie


+---------------+
|  DESCRIPTION  |
+---------------+
Juni 2014
Canon EOS 60D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hündin female dalmatiner dalmatian spaß fun playing game ball booga flamingo fisch spielzeug toy 