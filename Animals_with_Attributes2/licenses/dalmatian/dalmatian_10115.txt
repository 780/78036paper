+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/7211116416/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 15 16:11:13 GMT+0200 2012
Upload Date  : Wed May 16 21:40:05 GMT+0200 2012
Views        : 412
Comments     : 0


+---------+
|  TITLE  |
+---------+
Heißgeliebtes Zergelspiel


+---------------+
|  DESCRIPTION  |
+---------------+
Mai 2012
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog dalmatiner female dalmatian Dalmatinac hund junghund young zergelspiel spiel ziehspiel playing 