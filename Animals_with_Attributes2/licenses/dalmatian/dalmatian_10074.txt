+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/6871483605/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 11 16:32:29 GMT+0100 2012
Upload Date  : Mon Feb 13 21:18:06 GMT+0100 2012
Views        : 827
Comments     : 1


+---------+
|  TITLE  |
+---------+
Fang mich doch...


+---------------+
|  DESCRIPTION  |
+---------------+
Februar 2012
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog dalmatiner female dalmatian Dalmatinac hund junghund young molosser dogge dogg bordeaux Dogue de 