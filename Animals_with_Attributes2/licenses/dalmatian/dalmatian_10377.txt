+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/15264648320/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 3 16:50:58 GMT+0200 2014
Upload Date  : Sun Oct 5 22:29:07 GMT+0200 2014
Views        : 1,098
Comments     : 1


+---------+
|  TITLE  |
+---------+
Dummytraining


+---------------+
|  DESCRIPTION  |
+---------------+
Oktober 2014
Canon EOS 60D
EF 85mm f/1.8 USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hündin female dalmatiner dalmatian spaß fun freilauf gassi dummy training futterbeutel dummytraining 