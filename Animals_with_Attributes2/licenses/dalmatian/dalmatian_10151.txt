+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/8291470729/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 16 16:11:20 GMT+0100 2012
Upload Date  : Thu Dec 20 21:59:08 GMT+0100 2012
Views        : 437
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ob ich eins nehme?


+---------------+
|  DESCRIPTION  |
+---------------+
Dezember 2012
Canon EOS 40D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac betteln bettelblick 