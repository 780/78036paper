+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/15303025298/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 27 16:21:37 GMT+0200 2014
Upload Date  : Thu Oct 9 22:22:32 GMT+0200 2014
Views        : 1,105
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wubba-Spaß


+---------------+
|  DESCRIPTION  |
+---------------+
September 2014
Canon EOS 60D
EF 85mm f/1.8 USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hündin female dalmatiner dalmatian spaß fun playing wubba kong game spielen 