+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/7044787187/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 3 16:18:02 GMT+0200 2012
Upload Date  : Wed Apr 4 14:38:04 GMT+0200 2012
Views        : 879
Comments     : 0


+---------+
|  TITLE  |
+---------+
Entspannt vor der Kamera


+---------------+
|  DESCRIPTION  |
+---------------+
April 2012
Canon EOS 40D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog dalmatiner female dalmatian Dalmatinac hund junghund young shooting photo fotoshooting 