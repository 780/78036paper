+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/8468241077/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 10 14:59:03 GMT+0100 2013
Upload Date  : Tue Feb 12 22:13:33 GMT+0100 2013
Views        : 569
Comments     : 0


+---------+
|  TITLE  |
+---------+
Dummysuche


+---------------+
|  DESCRIPTION  |
+---------------+
Februar 2013
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac schnee snow dummysuche suche dummy search 