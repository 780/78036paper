+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/14172009174/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 8 19:58:52 GMT+0200 2014
Upload Date  : Mon May 12 22:31:50 GMT+0200 2014
Views        : 2,738
Comments     : 0


+---------+
|  TITLE  |
+---------+
Angiemaus


+---------------+
|  DESCRIPTION  |
+---------------+
Mai 2014
Canon EOS 60D
EF 85mm f/1.8 USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hunde dogs hündin female dalmatian dalmatiner fun game playing spielen spiel hundespiel 