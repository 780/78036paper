+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/7141623713/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 2 16:57:55 GMT+0200 2012
Upload Date  : Fri May 4 13:26:04 GMT+0200 2012
Views        : 527
Comments     : 1


+---------+
|  TITLE  |
+---------+
Angie und der Knochen


+---------------+
|  DESCRIPTION  |
+---------------+
April 2012 - Angie und ihr Knochen: <a href="http://www.youtube.com/watch?v=5cn899jHKpg" rel="nofollow">www.youtube.com/watch?v=5cn899jHKpg</a> 

Canon EOS 40D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog dalmatiner female dalmatian Dalmatinac hund junghund young knochen bones rinderhüftknochen hüftknochen rinderknochen bone 