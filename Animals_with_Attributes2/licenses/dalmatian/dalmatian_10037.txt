+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/6188201583/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 25 14:27:17 GMT+0200 2011
Upload Date  : Tue Sep 27 13:55:07 GMT+0200 2011
Views        : 342
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hundische Lebensfreude


+---------------+
|  DESCRIPTION  |
+---------------+
September 2011
Canon EOS 40D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog welpe puppy dalmatiner female dalmatian Dalmatinac hund junghund young 