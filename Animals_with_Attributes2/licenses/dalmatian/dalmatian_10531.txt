+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Enrique Freire
Photo URL    : https://www.flickr.com/photos/enriquefreire/6330169606/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 6 15:18:58 GMT+0100 2011
Upload Date  : Wed Nov 9 22:27:01 GMT+0100 2011
Geotag Info  : Latitude:38.111081, Longitude:-1.868133
Views        : 167
Comments     : 2


+---------+
|  TITLE  |
+---------+
Mimetismo de un dálmata otoñal


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
FES Murcia España Spain 