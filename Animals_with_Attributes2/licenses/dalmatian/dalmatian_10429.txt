+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/21047310860/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 28 16:46:01 GMT+0200 2015
Upload Date  : Tue Sep 8 06:46:16 GMT+0200 2015
Views        : 869
Comments     : 0


+---------+
|  TITLE  |
+---------+
Angie und Bubbles


+---------------+
|  DESCRIPTION  |
+---------------+
August 2015

Canon EOS 60D
EF 24-105mm f/4L IS USM

Creative Commons Licence BY 2.0
<a href="https://creativecommons.org/licenses/by/2.0/" rel="nofollow">creativecommons.org/licenses/by/2.0/</a>

Quellenangabe / Credit:
Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hunde dogs hündin female dalmatiner dalmatian schwarz weiß black white running fun playing english springer spaniel 