+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/14228097330/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 12 19:33:03 GMT+0200 2014
Upload Date  : Fri Jun 13 22:26:06 GMT+0200 2014
Views        : 602
Comments     : 0


+---------+
|  TITLE  |
+---------+
Angsttraining auf Metallbrücke 1


+---------------+
|  DESCRIPTION  |
+---------------+
Juni 2014
Canon EOS 60D
EF 85mm f/1.8 USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hündin female dalmatiner dalmatian brücke metallbrücke bridge training angst fear 