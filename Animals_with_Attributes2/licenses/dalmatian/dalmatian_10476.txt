+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : asw909
Photo URL    : https://www.flickr.com/photos/asw909/8542418091/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 8 11:44:10 GMT+0100 2013
Upload Date  : Sun Mar 10 00:11:56 GMT+0100 2013
Geotag Info  : Latitude:52.451903, Longitude:-1.721849
Views        : 174
Comments     : 0


+---------+
|  TITLE  |
+---------+
Crufts 2013: Day Two (Toy and Utility): 08-March 2013


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Dalmatian Crufts "Crufts 2013" NEC "National Exhibition Centre" Birmingham 