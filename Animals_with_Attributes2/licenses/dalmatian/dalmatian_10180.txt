+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/8714318085/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 17 14:27:17 GMT+0100 2013
Upload Date  : Mon May 6 21:28:53 GMT+0200 2013
Views        : 645
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zergelspiel


+---------------+
|  DESCRIPTION  |
+---------------+
März 2013
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac schnee snow wubba kong 