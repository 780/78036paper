+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/9408416119/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 28 20:15:32 GMT+0200 2013
Upload Date  : Wed Jul 31 20:58:50 GMT+0200 2013
Views        : 960
Comments     : 0


+---------+
|  TITLE  |
+---------+
anny-x Geschirr protect


+---------------+
|  DESCRIPTION  |
+---------------+
Juli 2013
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac anny-x geschirr brustgeschirr führleine 