+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/6791302911/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jan 30 16:56:46 GMT+0100 2012
Upload Date  : Mon Jan 30 21:18:31 GMT+0100 2012
Views        : 463
Comments     : 0


+---------+
|  TITLE  |
+---------+
Jungspund Angie


+---------------+
|  DESCRIPTION  |
+---------------+
Januar 2012
Canon EOS 40D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog dalmatiner female dalmatian Dalmatinac hund junghund young 