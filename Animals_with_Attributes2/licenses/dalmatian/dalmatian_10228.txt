+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/10764359984/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 27 15:18:00 GMT+0100 2013
Upload Date  : Sat Nov 9 21:17:15 GMT+0100 2013
Views        : 833
Comments     : 0


+---------+
|  TITLE  |
+---------+
Angie


+---------------+
|  DESCRIPTION  |
+---------------+
Oktober 2013
Canon EOS 60D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac herbst autumn 