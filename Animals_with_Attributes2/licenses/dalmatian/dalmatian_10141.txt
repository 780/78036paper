+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/7982692417/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Sep 13 15:48:14 GMT+0200 2012
Upload Date  : Thu Sep 13 16:56:34 GMT+0200 2012
Views        : 1,221
Comments     : 0


+---------+
|  TITLE  |
+---------+
Angies Bettelblick


+---------------+
|  DESCRIPTION  |
+---------------+
September 2012
Canon EOS 40D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac betteln bettelblick 