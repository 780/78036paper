+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/6272212614/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 22 14:35:01 GMT+0200 2011
Upload Date  : Sun Oct 23 12:34:31 GMT+0200 2011
Views        : 370
Comments     : 0


+---------+
|  TITLE  |
+---------+
Angies Papa James


+---------------+
|  DESCRIPTION  |
+---------------+
Oktober  2011
Canon EOS 40D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - flickr.com


+--------+
|  TAGS  |
+--------+
hund hunde hündin dog dogs welpe welpen welpenspiel puppy puppies dalmatiner female dalmatian Dalmatinac rüde male 