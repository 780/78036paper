+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/15815142492/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 1 14:48:43 GMT+0100 2014
Upload Date  : Mon Nov 17 20:07:49 GMT+0100 2014
Views        : 924
Comments     : 0


+---------+
|  TITLE  |
+---------+
Futtertest Allergodog


+---------------+
|  DESCRIPTION  |
+---------------+
01.11.2014
Canon EOS 60D
EF 85mm f/1.8 USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hündin female dalmatiner dalmatian leckerchen futter food training 