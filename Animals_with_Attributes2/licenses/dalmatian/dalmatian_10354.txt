+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/14922618457/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 30 16:57:30 GMT+0200 2014
Upload Date  : Mon Sep 1 21:23:42 GMT+0200 2014
Views        : 919
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pippis Groupie


+---------------+
|  DESCRIPTION  |
+---------------+
August 2014
Canon EOS 60D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hündin female dalmatiner dalmatian spaß fun freilauf gassi male rüde pointer 