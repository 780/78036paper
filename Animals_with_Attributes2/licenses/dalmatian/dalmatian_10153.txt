+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/8378121552/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 13 15:39:56 GMT+0100 2013
Upload Date  : Sun Jan 13 21:48:42 GMT+0100 2013
Views        : 553
Comments     : 2


+---------+
|  TITLE  |
+---------+
Fliegeohren


+---------------+
|  DESCRIPTION  |
+---------------+
Januar 2013
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac schnee snow 