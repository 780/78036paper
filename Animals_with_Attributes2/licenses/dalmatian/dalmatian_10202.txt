+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/9311362901/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 14 16:33:30 GMT+0200 2013
Upload Date  : Thu Jul 18 12:41:01 GMT+0200 2013
Views        : 1,025
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pudelwohl fühlen


+---------------+
|  DESCRIPTION  |
+---------------+
Juli 2013
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund hündin dog dalmatiner female dalmatian Dalmatinac 