+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/22694673011/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 1 10:50:16 GMT+0100 2015
Upload Date  : Sun Nov 1 20:24:44 GMT+0100 2015
Views        : 576
Comments     : 0


+---------+
|  TITLE  |
+---------+
Fellwechsel


+---------------+
|  DESCRIPTION  |
+---------------+
November 2015

Canon EOS 60D
EF 24-105mm f/4L IS USM

Creative Commons Licence BY 2.0
<a href="https://creativecommons.org/licenses/by/2.0/" rel="nofollow">creativecommons.org/licenses/by/2.0/</a>

Quellenangabe / Credit:
Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hunde dogs hündin female dalmatiner dalmatian schwarz weiß black white fellwechsel fell moulting 