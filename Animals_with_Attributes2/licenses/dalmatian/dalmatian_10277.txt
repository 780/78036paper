+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/14007176473/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 19 15:26:07 GMT+0200 2014
Upload Date  : Wed Apr 23 22:31:37 GMT+0200 2014
Views        : 624
Comments     : 0


+---------+
|  TITLE  |
+---------+
Angie


+---------------+
|  DESCRIPTION  |
+---------------+
April 2014
Canon EOS 60D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hund dog hunde dogs hündin female dalmatian dalmatiner fun game playing spielen spiel hundespiel 