+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/6843281566/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 16 10:57:02 GMT+0100 2012
Upload Date  : Sat Mar 17 10:55:22 GMT+0100 2012
Views        : 452
Comments     : 0


+---------+
|  TITLE  |
+---------+
Aufmerksame Angie


+---------------+
|  DESCRIPTION  |
+---------------+
März 2012
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog dalmatiner female dalmatian Dalmatinac hund junghund young HighQualityDogs 