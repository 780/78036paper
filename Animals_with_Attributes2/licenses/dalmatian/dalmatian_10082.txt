+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/6815636772/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 7 13:29:44 GMT+0100 2012
Upload Date  : Wed Mar 7 15:41:19 GMT+0100 2012
Views        : 557
Comments     : 0


+---------+
|  TITLE  |
+---------+
Warme Mittagssonne


+---------------+
|  DESCRIPTION  |
+---------------+
März 2012
Canon EOS 40D
EF-S 55-250mm f/4-5.6 IS II

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog dalmatiner female dalmatian Dalmatinac hund junghund young frühling spring 