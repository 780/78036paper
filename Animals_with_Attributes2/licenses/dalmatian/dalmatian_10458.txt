+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Leo Hidalgo (@yompyz)
Photo URL    : https://www.flickr.com/photos/ileohidalgo/12704793513/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 22 09:47:02 GMT+0100 2014
Upload Date  : Sat Feb 22 22:43:19 GMT+0100 2014
Views        : 2,274
Comments     : 1


+---------+
|  TITLE  |
+---------+
¡Pitia deja el palo!


+---------------+
|  DESCRIPTION  |
+---------------+
| <b>Would you like to follow me on <a href="http://instagram.com/yompyz" rel="nofollow">Instagram</a>, <a href="https://twitter.com/yompyHERE" rel="nofollow">Twitter</a>, <a href="http://yompyz.tumblr.com" rel="nofollow">Tumblr</a> or <a href="http://www.youtube.com/user/XxYoMpYxX" rel="nofollow">YouTube</a></b>? |


+--------+
|  TAGS  |
+--------+
Canon eos 600d t3i ileohidalgo yompyz leo hidalgo random sunset bmx subrosa letum pitia dog dalmatian dalmata animal 50mm 1.8 larios calle malaga spain españa mijas fuengirola 