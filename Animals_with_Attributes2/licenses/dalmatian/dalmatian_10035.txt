+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blumenbiene
Photo URL    : https://www.flickr.com/photos/blumenbiene/6145888181/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 10 14:52:38 GMT+0200 2011
Upload Date  : Wed Sep 14 07:31:52 GMT+0200 2011
Views        : 509
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lebensfreude pur


+---------------+
|  DESCRIPTION  |
+---------------+
September 2011
Canon EOS 40D
EF-S 17-85mm f/4-5.6 IS USM

Creative Commons Licence BY 2.0

Quellenangabe / Credit:
Photo by Maja Dumat - CC BY 2.0


+--------+
|  TAGS  |
+--------+
hunde hündin dog welpe puppy dalmatiner female dalmatian Dalmatinac hund junghund young 