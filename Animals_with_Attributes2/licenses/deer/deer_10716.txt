+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Infomastern
Photo URL    : https://www.flickr.com/photos/infomastern/24108052306/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 13 12:06:09 GMT+0100 2015
Upload Date  : Sat Jan 2 23:06:22 GMT+0100 2016
Views        : 152
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Danmark Denmark Dyrehaven Jægersborg "Jægersborg Dyrehave" autumn deer forest park skog woods "exif:model=canon eos 760d" geo:country= "exif:focal_length=182 mm" geo:city= "camera:model=canon eos 760d" geo:state= "exif:aperture=ƒ / 5,0" geo:location= camera:make=canon "exif:lens=ef70-300mm f/4-5.6l is usm" exif:iso_speed=2000 exif:make=canon 