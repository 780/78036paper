+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ChristinaT
Photo URL    : https://www.flickr.com/photos/christina-t/312583943/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 18 13:31:34 GMT+0100 2006
Upload Date  : Sun Dec 3 06:27:00 GMT+0100 2006
Views        : 890
Comments     : 0


+---------+
|  TITLE  |
+---------+
Baby Deer on the path


+---------------+
|  DESCRIPTION  |
+---------------+
Her tongue is sticking out. Perhaps she is already anticipating the taste of sunflower seeds.


+--------+
|  TAGS  |
+--------+
deer "bird feeder" snow 