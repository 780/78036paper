+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : zimpenfish
Photo URL    : https://www.flickr.com/photos/zimpenfish/2041853118/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 17 13:37:33 GMT+0100 2007
Upload Date  : Sun Nov 18 00:19:56 GMT+0100 2007
Views        : 248
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"bushy park" deer 