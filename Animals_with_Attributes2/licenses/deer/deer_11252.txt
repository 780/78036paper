+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : peptic_ulcer
Photo URL    : https://www.flickr.com/photos/peptic_ulcer/6367361153/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 30 12:18:53 GMT+0100 2003
Upload Date  : Sun Nov 20 06:41:33 GMT+0100 2011
Views        : 161
Comments     : 3


+---------+
|  TITLE  |
+---------+
Three Fawns in My Backyard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Canon Deer fawn 