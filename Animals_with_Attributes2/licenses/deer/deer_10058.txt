+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/4844116714/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 17 14:06:37 GMT+0200 2010
Upload Date  : Fri Jul 30 18:21:08 GMT+0200 2010
Geotag Info  : Latitude:42.064268, Longitude:-71.586366
Views        : 696
Comments     : 0


+---------+
|  TITLE  |
+---------+
Antlers


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
southwick zoo deer forest antlers 