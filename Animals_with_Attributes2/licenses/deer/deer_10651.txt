+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kevinmklerks
Photo URL    : https://www.flickr.com/photos/ledicarus/14509723821/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 3 23:55:17 GMT+0200 2014
Upload Date  : Thu Jun 26 17:26:24 GMT+0200 2014
Geotag Info  : Latitude:52.480794, Longitude:-116.070064
Views        : 1,493
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
mountains rainstorm river clouds sunset deer bridge 