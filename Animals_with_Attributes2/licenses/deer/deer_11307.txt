+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Colby Stopa
Photo URL    : https://www.flickr.com/photos/photographybycolby/10114150245/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 5 17:33:09 GMT+0200 2013
Upload Date  : Sun Oct 6 11:49:17 GMT+0200 2013
Views        : 3,287
Comments     : 4


+---------+
|  TITLE  |
+---------+
White-tailed deer (Odocoileus virginianus)


+---------------+
|  DESCRIPTION  |
+---------------+
White-tailed deer in carburn park. Calgary, AB

Please support my photography efforts and like my page on facebook. facebook.com/photographybycolby


+--------+
|  TAGS  |
+--------+
Deer Whitetail #greatnature #animals wildlife mammals 