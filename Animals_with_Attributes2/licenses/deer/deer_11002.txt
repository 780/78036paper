+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pjsixft  ("PJ")
Photo URL    : https://www.flickr.com/photos/pjsixft/2963054368/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 29 11:38:04 GMT+0200 2008
Upload Date  : Wed Oct 22 01:39:42 GMT+0200 2008
Views        : 67
Comments     : 5


+---------+
|  TITLE  |
+---------+
Magnificence in my front yard


+---------------+
|  DESCRIPTION  |
+---------------+
In daylight... all day long, they come and entertain me, thrill me, bless me.


+--------+
|  TAGS  |
+--------+
deer 