+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : geraldbrazell
Photo URL    : https://www.flickr.com/photos/geraldbrazell/7817406652/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 19 00:41:19 GMT+0200 2012
Upload Date  : Sun Aug 19 21:22:36 GMT+0200 2012
Views        : 12,108
Comments     : 33


+---------+
|  TITLE  |
+---------+
Mother and Child Reunion - Oh, Deer!


+---------------+
|  DESCRIPTION  |
+---------------+
While I was preparing my lunch today, I saw this deer and her young fawn in my back yard. They lingered about 15 minutes munching on the greenery. There was a second fawn with them, but I was not able to get all three one the same frame. The fawn reminded me so much of Bambi from a Little Golden Book series I had as a child.

The quality is not so good as this was shot through three panes of glass.


+--------+
|  TAGS  |
+--------+
deer fawn mother child wildlife geraldbrazell 