+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : my_southborough
Photo URL    : https://www.flickr.com/photos/mysouthborough/3797426554/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 27 06:41:08 GMT+0200 2009
Upload Date  : Fri Aug 7 06:53:20 GMT+0200 2009
Views        : 217
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer in my backyard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
southborough deer fawns 