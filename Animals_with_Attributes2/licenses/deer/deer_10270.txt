+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jc.winkler
Photo URL    : https://www.flickr.com/photos/51653562@N00/6223952870/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 28 08:30:05 GMT+0200 2011
Upload Date  : Sat Oct 8 20:54:23 GMT+0200 2011
Views        : 145
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer in the yard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Pumpkins Sunflowers Deer 