+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cantanima
Photo URL    : https://www.flickr.com/photos/cantanima/13494686644/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 25 16:54:04 GMT+0100 2013
Upload Date  : Sat Mar 29 21:27:44 GMT+0100 2014
Views        : 1,163
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer near Tappahannock


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
dusk deer field 