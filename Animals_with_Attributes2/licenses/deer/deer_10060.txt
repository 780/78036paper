+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kilgarron
Photo URL    : https://www.flickr.com/photos/kilgarron/4340270442/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 6 15:38:00 GMT+0100 2010
Upload Date  : Mon Feb 8 08:25:11 GMT+0100 2010
Views        : 614
Comments     : 0


+---------+
|  TITLE  |
+---------+
Attingham Fallow Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Attingham Park" Deer "Fallow deer" 