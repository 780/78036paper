+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Joe Cleere
Photo URL    : https://www.flickr.com/photos/jcleere/9116659577/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 20 13:50:14 GMT+0200 2013
Upload Date  : Sun Jun 23 19:29:26 GMT+0200 2013
Views        : 2,493
Comments     : 2


+---------+
|  TITLE  |
+---------+
Red Deer Buck


+---------------+
|  DESCRIPTION  |
+---------------+
I went to Bushy Park in London on Thursday and had a great opportunity photographing the deer there. And I was lucky enough to have a buck close by, which had no interest in me what so ever (luckily!). I also managed to photograph a mother and it's youngster.


+--------+
|  TAGS  |
+--------+
deer bushy "bushy park" park grass horns "red deer" buck "male deer" "male red deer" 