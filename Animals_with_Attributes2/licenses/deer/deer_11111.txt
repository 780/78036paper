+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : caligula1995
Photo URL    : https://www.flickr.com/photos/pussreboots/15867087410/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 18 15:44:44 GMT+0100 2014
Upload Date  : Fri Dec 19 05:56:46 GMT+0100 2014
Geotag Info  : Latitude:37.677311, Longitude:-122.062959
Views        : 78
Comments     : 0


+---------+
|  TITLE  |
+---------+
PC187437


+---------------+
|  DESCRIPTION  |
+---------------+
Deer at Sulphur Creek


+--------+
|  TAGS  |
+--------+
"Sulphur Creek" 2014 deer 