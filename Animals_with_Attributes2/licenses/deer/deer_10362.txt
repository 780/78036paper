+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brokinhrt2
Photo URL    : https://www.flickr.com/photos/dq090702/2674155139/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 14 19:58:09 GMT+0200 2008
Upload Date  : Wed Jul 16 19:54:50 GMT+0200 2008
Views        : 557
Comments     : 1


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
Young buck on a field at Kickapoo State Park open field.


+--------+
|  TAGS  |
+--------+
Deer buck field park "state park" 