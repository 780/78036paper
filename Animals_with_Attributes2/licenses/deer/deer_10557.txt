+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mjambon
Photo URL    : https://www.flickr.com/photos/mjambon/6825253780/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 10 18:05:52 GMT+0100 2012
Upload Date  : Sun Mar 11 05:02:36 GMT+0100 2012
Views        : 85
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
Stanford Dish Trail, Stanford, California.


+--------+
|  TAGS  |
+--------+
(no tags)