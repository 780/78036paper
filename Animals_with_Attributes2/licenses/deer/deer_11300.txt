+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andrii Zymohliad
Photo URL    : https://www.flickr.com/photos/andrii_zymohliad/13204296015/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 15 13:11:05 GMT+0100 2014
Upload Date  : Mon Mar 17 00:07:42 GMT+0100 2014
Geotag Info  : Latitude:49.548059, Longitude:25.579540
Views        : 295
Comments     : 0


+---------+
|  TITLE  |
+---------+
White deer


+---------------+
|  DESCRIPTION  |
+---------------+
Free zoo in Ternopil


+--------+
|  TAGS  |
+--------+
Deer White Animal Animals Ternopil Zoo Nature 