+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flamesworddragon
Photo URL    : https://www.flickr.com/photos/flamesworddragon/8308296062/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 7 16:10:19 GMT+0200 2012
Upload Date  : Tue Dec 25 21:08:50 GMT+0100 2012
Views        : 437
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gazing Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
deer "Dyrham park" 