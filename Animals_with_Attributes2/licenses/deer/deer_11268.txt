+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael M. S.
Photo URL    : https://www.flickr.com/photos/39965300@N07/4995255216/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 15 16:58:56 GMT+0200 2010
Upload Date  : Thu Sep 16 07:07:13 GMT+0200 2010
Geotag Info  : Latitude:39.780928, Longitude:-75.121314
Views        : 76
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Gloucester County College" deer 