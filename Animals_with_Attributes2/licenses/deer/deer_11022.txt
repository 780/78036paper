+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : earien
Photo URL    : https://www.flickr.com/photos/earien/2175138936/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 6 16:01:35 GMT+0100 2008
Upload Date  : Mon Jan 7 12:17:45 GMT+0100 2008
Views        : 262
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mount Arcosu


+---------------+
|  DESCRIPTION  |
+---------------+
Mount Arcosu, Sardinia, Italy. 

A mountain area in southern Sardinia, 15 km south-west of the town of Cagliari. The largest evergreen broadleaved woodland of the region. 

Mount Arcosu reserve is managed by WWF and it's one of the most important and attractive italian natural reserves.

You can admire here the sardinian deers.


+--------+
|  TAGS  |
+--------+
deer animal sardinia italy 