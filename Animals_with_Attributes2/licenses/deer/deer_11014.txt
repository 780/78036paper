+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ElCapitanBSC
Photo URL    : https://www.flickr.com/photos/elcapitanbsc/3896255884/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 6 23:40:57 GMT+0200 2009
Upload Date  : Mon Sep 7 12:45:54 GMT+0200 2009
Views        : 876
Comments     : 0


+---------+
|  TITLE  |
+---------+
Miyajima Deer


+---------------+
|  DESCRIPTION  |
+---------------+
The deer at Miyajima are so accustomed to humans that you can get up close and pet them. They're everywhere and they don't care. I even stepped on one by accident and it didn't even start to move.

Uploaded through <a href="http://code.google.com/p/dfo" rel="nofollow">Desktop Flickr Organizer</a>.


+--------+
|  TAGS  |
+--------+
dfoupload japan miyajima deer 