+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Marie Hale
Photo URL    : https://www.flickr.com/photos/15016964@N02/5926621105/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 15 10:00:39 GMT+0200 2010
Upload Date  : Mon Jul 11 19:42:55 GMT+0200 2011
Views        : 982
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hog deer


+---------------+
|  DESCRIPTION  |
+---------------+
Hog deer - taken at Wingham Wildlife Park, Kent on 15th August 2010


+--------+
|  TAGS  |
+--------+
deer 