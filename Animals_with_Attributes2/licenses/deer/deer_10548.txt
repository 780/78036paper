+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Larry1732
Photo URL    : https://www.flickr.com/photos/larry1732/6238953975/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 8 08:49:49 GMT+0200 2011
Upload Date  : Thu Oct 13 03:18:52 GMT+0200 2011
Views        : 484
Comments     : 1


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
Lake City, Colorado


+--------+
|  TAGS  |
+--------+
deer lakecity lamsa co colorado 