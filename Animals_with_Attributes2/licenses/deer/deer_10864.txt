+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : George Donnelly
Photo URL    : https://www.flickr.com/photos/cyklo/74453097/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 17 16:22:33 GMT+0100 2001
Upload Date  : Sat Dec 17 18:48:27 GMT+0100 2005
Views        : 36
Comments     : 0


+---------+
|  TITLE  |
+---------+
Golden Gate Park


+---------------+
|  DESCRIPTION  |
+---------------+
While wandering around with a couple friends we came across this nice deer.


+--------+
|  TAGS  |
+--------+
california "san francisco" deer 