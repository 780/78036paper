+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nature'sSon
Photo URL    : https://www.flickr.com/photos/91782700@N08/8330896200/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 30 03:50:18 GMT+0100 2012
Upload Date  : Mon Dec 31 19:02:13 GMT+0100 2012
Geotag Info  : Latitude:40.904927, Longitude:-74.180328
Views        : 193
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_0317


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
deer winter woods 