+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nick Bramhall
Photo URL    : https://www.flickr.com/photos/black_friction/2754131643/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 10 11:43:42 GMT+0200 2008
Upload Date  : Mon Aug 11 23:50:45 GMT+0200 2008
Views        : 70
Comments     : 1


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
studley royal rain yorkshire north 