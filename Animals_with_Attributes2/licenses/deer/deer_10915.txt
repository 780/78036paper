+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : wiennat
Photo URL    : https://www.flickr.com/photos/wien/4785451560/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 11 11:50:49 GMT+0200 2010
Upload Date  : Mon Jul 12 05:09:57 GMT+0200 2010
Views        : 18
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_0743


+---------------+
|  DESCRIPTION  |
+---------------+
Nara Park, Todaiji
Japan

Jul 11 2010


+--------+
|  TAGS  |
+--------+
nara deer 