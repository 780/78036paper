+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rictor Norton & David Allen
Photo URL    : https://www.flickr.com/photos/rictor-and-david/3225304215/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 25 10:47:42 GMT+0100 2009
Upload Date  : Sun Jan 25 19:19:16 GMT+0100 2009
Geotag Info  : Latitude:51.442238, Longitude:-0.274143
Views        : 272
Comments     : 0


+---------+
|  TITLE  |
+---------+
Richmond Park


+---------------+
|  DESCRIPTION  |
+---------------+
Red deer (Cervus elaphus) stags in Richmond Park


+--------+
|  TAGS  |
+--------+
"Red Deer (Cervus elaphus)" 