+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Peter G Trimming
Photo URL    : https://www.flickr.com/photos/peter-trimming/5848753675/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 18 12:33:48 GMT+0200 2011
Upload Date  : Sun Jun 19 18:09:36 GMT+0200 2011
Views        : 1,272
Comments     : 0


+---------+
|  TITLE  |
+---------+
Red Deer


+---------------+
|  DESCRIPTION  |
+---------------+
Seen at the British Wildlife Centre, Newchapel, Surrey, on a wet and windy day. I was too busy, watching 'Eric' feeding on a piece of bread, to notice what the 'girls' were up to, in the background.


+--------+
|  TAGS  |
+--------+
British Wildlife Centre Newchapel Surrey 2011 Eric Red Deer Stag Cervus elaphus Peter Trimming 