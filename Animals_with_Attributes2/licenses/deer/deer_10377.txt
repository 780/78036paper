+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : andrew_j_w
Photo URL    : https://www.flickr.com/photos/andrew_j_w/2790443260/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 23 12:35:08 GMT+0200 2008
Upload Date  : Sat Aug 23 21:08:25 GMT+0200 2008
Geotag Info  : Latitude:51.412597, Longitude:-0.325384
Views        : 408
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"bushy park" deer 