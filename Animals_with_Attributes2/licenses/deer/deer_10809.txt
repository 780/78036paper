+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : C. P. Ewing
Photo URL    : https://www.flickr.com/photos/132033298@N04/17138420668/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 1 16:46:01 GMT+0100 2015
Upload Date  : Fri May 1 01:00:10 GMT+0200 2015
Views        : 7,886
Comments     : 23


+---------+
|  TITLE  |
+---------+
Eight Point White Tailed Buck


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
buck deer eight-point 