+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : CraigMoulding
Photo URL    : https://www.flickr.com/photos/craigmoulding/5878298719/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 28 10:11:46 GMT+0200 2011
Upload Date  : Tue Jun 28 00:00:16 GMT+0200 2011
Views        : 75
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chatsworth Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Chatsworth "Chatsworth Deer Park" Deer "Peak District" Stag 