+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Doug Hay
Photo URL    : https://www.flickr.com/photos/doughay/6239221020/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 26 11:22:27 GMT+0200 2011
Upload Date  : Thu Oct 13 01:22:13 GMT+0200 2011
Views        : 799
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Doug - July - Aug 13, 2001" "vancouver island" "amazing vancouver island" deer brown grass green 