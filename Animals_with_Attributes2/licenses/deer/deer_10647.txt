+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : synspectrum
Photo URL    : https://www.flickr.com/photos/epector/14383853317/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 3 17:53:59 GMT+0200 2014
Upload Date  : Fri Jul 4 08:51:20 GMT+0200 2014
Views        : 69
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
McDowell Grove Woods, DuPage County


+--------+
|  TAGS  |
+--------+
(no tags)