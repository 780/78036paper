+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : A Gude
Photo URL    : https://www.flickr.com/photos/agude/3711608151/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 6 08:42:52 GMT+0200 2009
Upload Date  : Sun Jul 12 09:02:27 GMT+0200 2009
Geotag Info  : Latitude:37.739744, Longitude:-119.573231
Views        : 353
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Canon EF-S 17-85mm f/4-5.6 IS USM Lens" Yosemite "Yosemite National Park" "Yosemite Valley" Deer 