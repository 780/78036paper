+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Becker1999
Photo URL    : https://www.flickr.com/photos/becker271/2916796249/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 4 09:11:30 GMT+0200 2008
Upload Date  : Mon Oct 6 05:12:15 GMT+0200 2008
Views        : 91
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wilds "cathy's pic" "animals and society animal and human interactions" 'animals "society animal" human interactions Ohio "ohio state" "ohio state university" OSU 