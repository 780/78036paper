+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : M&R Glasgow
Photo URL    : https://www.flickr.com/photos/glasgows/321853834/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 13 17:42:43 GMT+0100 2006
Upload Date  : Thu Dec 14 00:42:43 GMT+0100 2006
Geotag Info  : Latitude:30.171843, Longitude:-98.748035
Views        : 3,132
Comments     : 1


+---------+
|  TITLE  |
+---------+
deer hunting day2 (12)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
deer whitetail texas hunt 