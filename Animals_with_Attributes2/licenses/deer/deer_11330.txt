+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gvgoebel
Photo URL    : https://www.flickr.com/photos/37467370@N08/7612326424/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 20 16:57:02 GMT+0200 2012
Upload Date  : Sat Jul 21 02:01:41 GMT+0200 2012
Views        : 106
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ybdoe_1b


+---------------+
|  DESCRIPTION  |
+---------------+
deer &amp; pronghorn does, Rolling Hills Wildlife Adventure, Kansas / 2007


+--------+
|  TAGS  |
+--------+
animals mammals deer pronghorn doe 