+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ☺ Lee J Haywood
Photo URL    : https://www.flickr.com/photos/leehaywood/4237577344/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 2 16:25:10 GMT+0200 2009
Upload Date  : Sat Jan 2 17:13:47 GMT+0100 2010
Views        : 1,665
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer chewing


+---------------+
|  DESCRIPTION  |
+---------------+
A young deer chewing the grass.

This image has been used in an article...
<a href="http://www.benessereblog.it/post/6516/masticare-di-piu-per-mangiare-di-meno" rel="nofollow">www.benessereblog.it/post/6516/masticare-di-piu-per-mangi...</a>
(in Italian, <a href="http://translate.google.com/translate?sl=it&amp;tl=en&amp;js=n&amp;prev=_t&amp;hl=en&amp;ie=UTF-8&amp;layout=2&amp;eotf=1&amp;u=http://www.benessereblog.it/post/6516/masticare-di-piu-per-mangiare-di-meno&amp;act=url" rel="nofollow">English translation here</a>)


+--------+
|  TAGS  |
+--------+
deer chewing grass antlers "Wollaton Park" UsedBySomeone 