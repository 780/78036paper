+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : keithdixon1
Photo URL    : https://www.flickr.com/photos/129713698@N02/20063929825/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 25 13:08:42 GMT+0200 2015
Upload Date  : Mon Jul 27 22:07:30 GMT+0200 2015
Views        : 73
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer in the shade of a tree


+---------------+
|  DESCRIPTION  |
+---------------+
Deer at Burghley House, Stamford, Lincolnshire


+--------+
|  TAGS  |
+--------+
doe "deer, Burghley house" 