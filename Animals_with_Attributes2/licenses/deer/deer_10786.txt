+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mark F. Levisay
Photo URL    : https://www.flickr.com/photos/mlevisay/3019639797/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 7 11:27:22 GMT+0100 2008
Upload Date  : Mon Nov 10 22:20:43 GMT+0100 2008
Views        : 48
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_0269_edited


+---------------+
|  DESCRIPTION  |
+---------------+
Deer keeping pace with the hikers


+--------+
|  TAGS  |
+--------+
wildlife deer ShenandoahPark 