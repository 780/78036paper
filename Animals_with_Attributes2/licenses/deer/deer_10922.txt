+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Alan Light
Photo URL    : https://www.flickr.com/photos/alan-light/6076575732/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 16 00:34:10 GMT+0200 2010
Upload Date  : Wed Aug 24 15:17:32 GMT+0200 2011
Views        : 197
Comments     : 1


+---------+
|  TITLE  |
+---------+
IMG_2022


+---------------+
|  DESCRIPTION  |
+---------------+
August 21, 2011


+--------+
|  TAGS  |
+--------+
male deer buck rack 