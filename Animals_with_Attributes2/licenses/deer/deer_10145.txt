+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Airwolfhound
Photo URL    : https://www.flickr.com/photos/24874528@N04/3782988216/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 1 14:15:49 GMT+0200 2009
Upload Date  : Mon Aug 3 00:29:44 GMT+0200 2009
Views        : 1,216
Comments     : 2


+---------+
|  TITLE  |
+---------+
Deer - Woburn Deer Park August 2009


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Deer "Woburn Deer Park" 