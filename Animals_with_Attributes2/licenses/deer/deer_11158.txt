+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : dgoomany
Photo URL    : https://www.flickr.com/photos/dgoomany/7800794098/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 7 12:08:11 GMT+0200 2012
Upload Date  : Fri Aug 17 11:31:35 GMT+0200 2012
Views        : 238
Comments     : 0


+---------+
|  TITLE  |
+---------+
Red deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"richmond park" deer "red deer" london nature wildlife park 'royal green grass cloudy trees forest woodland 