+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Brian U
Photo URL    : https://www.flickr.com/photos/snype451/5403650832/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 8 13:50:38 GMT+0100 2011
Upload Date  : Mon Jan 31 05:19:51 GMT+0100 2011
Views        : 920
Comments     : 1


+---------+
|  TITLE  |
+---------+
A deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"big ears" cute deer forest herd "mule deer" snow snowing winter 