+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jkirkhart35
Photo URL    : https://www.flickr.com/photos/jkirkhart35/4927220346/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 18 09:00:15 GMT+0200 2010
Upload Date  : Wed Aug 25 19:55:03 GMT+0200 2010
Views        : 722
Comments     : 8


+---------+
|  TITLE  |
+---------+
Mule Deer  (Odocoileus heminous)


+---------------+
|  DESCRIPTION  |
+---------------+
Seen on the docent led bluff hike, who was certainly checking us out).


+--------+
|  TAGS  |
+--------+
Mule Deer 