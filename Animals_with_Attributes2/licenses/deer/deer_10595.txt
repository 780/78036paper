+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : One Day Closer
Photo URL    : https://www.flickr.com/photos/onedaycloser/8341166786/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 28 19:35:41 GMT+0200 2012
Upload Date  : Thu Jan 3 06:13:48 GMT+0100 2013
Views        : 82
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Kanopolis Lake Kansas summer nature" 