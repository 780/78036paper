+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Alan Light
Photo URL    : https://www.flickr.com/photos/alan-light/15631081215/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Oct 21 08:12:18 GMT+0200 2014
Upload Date  : Sun Oct 26 12:21:32 GMT+0100 2014
Views        : 1,196
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
October, 2014


+--------+
|  TAGS  |
+--------+
deer 