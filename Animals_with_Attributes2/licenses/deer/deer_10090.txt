+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike Tewkesbury
Photo URL    : https://www.flickr.com/photos/7687126@N06/3409055188/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 2 16:50:35 GMT+0200 2009
Upload Date  : Fri Apr 3 09:11:49 GMT+0200 2009
Views        : 379
Comments     : 2


+---------+
|  TITLE  |
+---------+
Brief encounter


+---------------+
|  DESCRIPTION  |
+---------------+
A group of deer grazing in a field off Sandhill Road outside of Marietta Ohio. I believe they would have almost ignored me but another driver in a hooptie pickup truck  came along and spooked the herd who ended up running back across the road and disappearing into the woods.


+--------+
|  TAGS  |
+--------+
"Ohio Deer" "Ohio wildlife" 