+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flamesworddragon
Photo URL    : https://www.flickr.com/photos/flamesworddragon/8069863896/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 29 16:48:12 GMT+0200 2012
Upload Date  : Tue Oct 9 08:41:25 GMT+0200 2012
Views        : 167
Comments     : 0


+---------+
|  TITLE  |
+---------+
Watchful Eye - Deer at Dyrham Park


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
deer "dyrham park" 