+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : minoir
Photo URL    : https://www.flickr.com/photos/minoir/14909290887/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 16 14:49:26 GMT+0100 2013
Upload Date  : Sun Aug 31 19:27:33 GMT+0200 2014
Views        : 324
Comments     : 0


+---------+
|  TITLE  |
+---------+
夕暮れと鹿


+---------------+
|  DESCRIPTION  |
+---------------+
なんだか神々しい。
＠東大寺 (Todaiji temple, Nara)


+--------+
|  TAGS  |
+--------+
nara todaiji temple deer 