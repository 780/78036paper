+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : vladeb
Photo URL    : https://www.flickr.com/photos/28122162@N04/9936169136/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 24 13:24:17 GMT+0200 2013
Upload Date  : Wed Sep 25 18:44:35 GMT+0200 2013
Views        : 1,945
Comments     : 1


+---------+
|  TITLE  |
+---------+
Alert Deer in STA-5


+---------------+
|  DESCRIPTION  |
+---------------+
I could have cropped this a little closer.   It is funny when legs dissappear behind something like grass.  I'm not sure if they should be cropped or left in the shot.


+--------+
|  TAGS  |
+--------+
deer "White-tailed Deer" STA-5 