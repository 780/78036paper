+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rumpleteaser
Photo URL    : https://www.flickr.com/photos/rumpleteaser/6088647610/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 11 13:39:46 GMT+0200 2010
Upload Date  : Sun Aug 28 12:44:18 GMT+0200 2011
Views        : 328
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
CC "Creative Commons" animal face eyes nature up-close fur tail ears 