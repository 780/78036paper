+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Daniel M. Hendricks
Photo URL    : https://www.flickr.com/photos/hendricksphotos/5256265038/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 12 12:43:51 GMT+0100 2010
Upload Date  : Mon Dec 13 02:01:05 GMT+0100 2010
Views        : 392
Comments     : 0


+---------+
|  TITLE  |
+---------+
Key Deer


+---------------+
|  DESCRIPTION  |
+---------------+
The endangered &quot;Key Deer&quot; of the Florida Keys are very docile and friendly.


+--------+
|  TAGS  |
+--------+
2010 deer florida "florida keys" 