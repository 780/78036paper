+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mountain Roamer
Photo URL    : https://www.flickr.com/photos/mountainroamerimages/6529202831/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 17 16:13:42 GMT+0100 2011
Upload Date  : Sun Dec 18 05:15:45 GMT+0100 2011
Geotag Info  : Latitude:40.516082, Longitude:-105.251255
Views        : 99
Comments     : 0


+---------+
|  TITLE  |
+---------+
184/365 - Oh Deer!


+---------------+
|  DESCRIPTION  |
+---------------+
(83)


+--------+
|  TAGS  |
+--------+
365Project "Watson Lake" deer 