+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ☺ Lee J Haywood
Photo URL    : https://www.flickr.com/photos/leehaywood/4313953820/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 2 10:49:29 GMT+0100 2009
Upload Date  : Fri Jan 29 15:36:34 GMT+0100 2010
Views        : 108
Comments     : 0


+---------+
|  TITLE  |
+---------+
Curious young deer


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at Wollaton Park.


+--------+
|  TAGS  |
+--------+
deer "Wollaton Park" 