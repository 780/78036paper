+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : xeno_sapien
Photo URL    : https://www.flickr.com/photos/gilad_rom/9595755459/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 24 17:39:08 GMT+0200 2013
Upload Date  : Mon Aug 26 10:34:32 GMT+0200 2013
Geotag Info  : Latitude:37.604241, Longitude:-119.013945
Views        : 10,657
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
antlers buck california canon closeup deer grazing horns mammoth zoom "Mammoth Lakes" "United States" 