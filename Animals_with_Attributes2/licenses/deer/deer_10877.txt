+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ☺ Lee J Haywood
Photo URL    : https://www.flickr.com/photos/leehaywood/4505004593/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 9 12:08:21 GMT+0200 2010
Upload Date  : Fri Apr 9 17:26:56 GMT+0200 2010
Views        : 393
Comments     : 0


+---------+
|  TITLE  |
+---------+
Heading to the lake


+---------------+
|  DESCRIPTION  |
+---------------+
A deer pauses, right before it heads out into the lake's waters for <a href="http://www.flickr.com/photos/leehaywood/4505025773/">a swim</a> at Wollaton Park.


+--------+
|  TAGS  |
+--------+
"Wollaton Park" deer 