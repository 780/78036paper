+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cobaltfish
Photo URL    : https://www.flickr.com/photos/cobaltfish/13564932365/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 27 10:58:33 GMT+0100 2014
Upload Date  : Tue Apr 1 20:03:50 GMT+0200 2014
Views        : 4,880
Comments     : 0


+---------+
|  TITLE  |
+---------+
Deer


+---------------+
|  DESCRIPTION  |
+---------------+
one of several herds of deer around the park,


+--------+
|  TAGS  |
+--------+
deer 