+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : atlai
Photo URL    : https://www.flickr.com/photos/atlai/4668119772/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 20 10:54:08 GMT+0200 2009
Upload Date  : Fri Jun 4 04:15:10 GMT+0200 2010
Views        : 87
Comments     : 0


+---------+
|  TITLE  |
+---------+
Jægersborg Dyrehave


+---------------+
|  DESCRIPTION  |
+---------------+
Deer park! Gorgeous. There were lots of these.


+--------+
|  TAGS  |
+--------+
denmark deer 