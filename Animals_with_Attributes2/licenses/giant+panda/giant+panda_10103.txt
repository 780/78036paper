+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sanchez jalapeno
Photo URL    : https://www.flickr.com/photos/sanchezjalapeno/6188250144/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 27 11:48:26 GMT+0200 2011
Upload Date  : Tue Sep 27 09:07:23 GMT+0200 2011
Views        : 837
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chengdu Panda Breeding Centre


+---------------+
|  DESCRIPTION  |
+---------------+
China
Pandas


+--------+
|  TAGS  |
+--------+
China Pandas 