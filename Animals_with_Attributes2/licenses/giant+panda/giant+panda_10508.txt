+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Keith Roper
Photo URL    : https://www.flickr.com/photos/keithroper/8131567727/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 12 07:33:04 GMT+0200 2012
Upload Date  : Sun Oct 28 18:00:34 GMT+0100 2012
Views        : 6,026
Comments     : 0


+---------+
|  TITLE  |
+---------+
Panda 2


+---------------+
|  DESCRIPTION  |
+---------------+
Pandas at Beijing Zoo. October 2012


+--------+
|  TAGS  |
+--------+
Pandas China 