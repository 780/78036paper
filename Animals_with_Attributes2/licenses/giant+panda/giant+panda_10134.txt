+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Frontierofficial
Photo URL    : https://www.flickr.com/photos/frontierofficial/8006339771/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Sep 20 16:08:11 GMT+0200 2012
Upload Date  : Thu Sep 20 17:08:11 GMT+0200 2012
Geotag Info  : Latitude:30.665675, Longitude:104.053230
Views        : 338
Comments     : 0


+---------+
|  TITLE  |
+---------+
China Breeding Programme


+---------------+
|  DESCRIPTION  |
+---------------+
Images from Frances McFadden. 

For more information please visit the <a href="http://bit.ly/Sawkhw" rel="nofollow">China Breeding Project</a> page of the Frontier website.


+--------+
|  TAGS  |
+--------+
Panda China 