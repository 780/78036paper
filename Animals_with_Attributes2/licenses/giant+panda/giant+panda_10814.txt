+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/14341786783/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 25 11:32:55 GMT+0200 2014
Upload Date  : Sun Jun 1 18:46:34 GMT+0200 2014
Views        : 686
Comments     : 0


+---------+
|  TITLE  |
+---------+
Singapore River Safari


+---------------+
|  DESCRIPTION  |
+---------------+
Giant Panda - Jia Jia


+--------+
|  TAGS  |
+--------+
Singapore "River Safari" "Giant Panda" 