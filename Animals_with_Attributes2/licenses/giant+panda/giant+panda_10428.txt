+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mulligan Stu
Photo URL    : https://www.flickr.com/photos/mulliganstu/8266260937/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 12 08:02:54 GMT+0200 2012
Upload Date  : Wed Dec 12 16:34:43 GMT+0100 2012
Views        : 3,555
Comments     : 9


+---------+
|  TITLE  |
+---------+
Laid Back Papa Panda


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Chengdu "Sichuan Giant Panda Sanctuaries" "Chengdu Panda Base" "Giant Panda" China "UNESCO World Natural Heritage Site" UNESCO 