+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andrew and Annemarie
Photo URL    : https://www.flickr.com/photos/andrew_annemarie/14301829793/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 27 09:55:10 GMT+0200 2014
Upload Date  : Tue May 27 03:55:10 GMT+0200 2014
Geotag Info  : Latitude:30.737805, Longitude:104.143191
Views        : 1,971
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giant Panda


+---------------+
|  DESCRIPTION  |
+---------------+
Follow our trip at <a href="http://www.trekkingasia.co.uk" rel="nofollow">Trekking Asia</a>


+--------+
|  TAGS  |
+--------+
file:md5sum=6f089ee716631c0c763aaeb4f1f1b53c file:sha1sig=3323b1f18577781ba4904a10a479fd76949b2344 panda Chengdu China Asia backpacking trekking sightseeing landscapes 