+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Chi King
Photo URL    : https://www.flickr.com/photos/davelau/2151389746/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 25 10:25:41 GMT+0100 2007
Upload Date  : Mon Dec 31 07:23:32 GMT+0100 2007
Geotag Info  : Latitude:30.774878, Longitude:103.610687
Views        : 5,552
Comments     : 3


+---------+
|  TITLE  |
+---------+
Pandas!! (GIANT PANDA/WOLONG/SICHUAN/CHINA)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Giant Panda" Panda Wolong China Interestingness 