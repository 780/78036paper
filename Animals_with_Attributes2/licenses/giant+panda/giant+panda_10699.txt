+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ray Bouknight
Photo URL    : https://www.flickr.com/photos/raybouk/8349012318/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 2 13:44:58 GMT+0100 2013
Upload Date  : Sat Jan 5 08:02:04 GMT+0100 2013
Views        : 1,943
Comments     : 0


+---------+
|  TITLE  |
+---------+
Panda, San Diego Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
panda zoo "san diego zoo" canon xsi january 2013 