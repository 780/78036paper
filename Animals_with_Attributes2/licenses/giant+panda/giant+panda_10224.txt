+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gill_penney
Photo URL    : https://www.flickr.com/photos/gillpenney/3018937124/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 6 09:19:22 GMT+0200 2007
Upload Date  : Mon Nov 10 10:06:36 GMT+0100 2008
Views        : 1,073
Comments     : 1


+---------+
|  TITLE  |
+---------+
Giant Panda Cubs 254


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
China Chengdu Chinese Sichuan Breeding zoo research "Giant panda" cubs panda red black white climbing 