+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : .curt.
Photo URL    : https://www.flickr.com/photos/curtsm/187876865/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 11 23:12:20 GMT+0200 2006
Upload Date  : Wed Jul 12 08:12:20 GMT+0200 2006
Views        : 476
Comments     : 0


+---------+
|  TITLE  |
+---------+
Panda at the National Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Canon EOS 5D" "Washington DC" panda 