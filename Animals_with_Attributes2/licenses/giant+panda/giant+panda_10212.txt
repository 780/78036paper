+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gill_penney
Photo URL    : https://www.flickr.com/photos/gillpenney/3423228755/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 5 02:25:43 GMT+0200 2009
Upload Date  : Wed Apr 8 15:02:06 GMT+0200 2009
Views        : 1,206
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giant Panda Cub, Chengdu, Sichuan april 2009 350D 2085


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"giant panda cub" chengdu sichuan China Chinese 