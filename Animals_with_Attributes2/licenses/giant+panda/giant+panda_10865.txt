+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/3780894712/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 13 13:20:33 GMT+0200 2009
Upload Date  : Sun Aug 2 13:11:32 GMT+0200 2009
Geotag Info  : Latitude:48.181886, Longitude:16.302005
Views        : 454
Comments     : 3


+---------+
|  TITLE  |
+---------+
Vienna Zoo, Austria


+---------------+
|  DESCRIPTION  |
+---------------+
pandas


+--------+
|  TAGS  |
+--------+
Vienna Austria Zoo Pandas 