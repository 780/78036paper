+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pelican
Photo URL    : https://www.flickr.com/photos/pelican/5754938/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 13 10:02:06 GMT+0200 2004
Upload Date  : Wed Mar 2 18:25:13 GMT+0100 2005
Views        : 6,767
Comments     : 13


+---------+
|  TITLE  |
+---------+
Adventure World, Shirahama, Japan


+---------------+
|  DESCRIPTION  |
+---------------+
Mother and her twin children are playing.


+--------+
|  TAGS  |
+--------+
zoo panda shirahama wakayama twins "adventure world" 