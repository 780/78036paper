+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sanchez jalapeno
Photo URL    : https://www.flickr.com/photos/sanchezjalapeno/6188313994/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 27 11:52:02 GMT+0200 2011
Upload Date  : Tue Sep 27 09:50:58 GMT+0200 2011
Views        : 518
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chengdu Panda Breeding Centre


+---------------+
|  DESCRIPTION  |
+---------------+
China
Pandas


+--------+
|  TAGS  |
+--------+
China Pandas 