+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : David Berkowitz
Photo URL    : https://www.flickr.com/photos/davidberkowitz/6883912388/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 25 11:40:34 GMT+0200 2012
Upload Date  : Fri Mar 30 20:22:22 GMT+0200 2012
Views        : 526
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pandas - Zoo Atlanta - Atlanta, Georgia


+---------------+
|  DESCRIPTION  |
+---------------+
A trip to Atlanta, Georgia - United States of America - March 2012

(cc) David Berkowitz - <a href="http://www.marketersstudio.com" rel="nofollow">www.marketersstudio.com</a> / <a href="http://www.about.me/dberkowitz" rel="nofollow">www.about.me/dberkowitz</a>


+--------+
|  TAGS  |
+--------+
Atlanta Georgia GA USA America travel South panda 