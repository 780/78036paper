+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : fortherock
Photo URL    : https://www.flickr.com/photos/fortherock/4306247976/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 24 11:59:06 GMT+0100 2010
Upload Date  : Tue Jan 26 09:36:22 GMT+0100 2010
Views        : 10,828
Comments     : 0


+---------+
|  TITLE  |
+---------+
Yun Zi - Baby Giant Panda - IMG_1696


+---------------+
|  DESCRIPTION  |
+---------------+
Yun Zi the new Giant Baby Panda in Action at the San Diego Zoo Jan 24th 2010


+--------+
|  TAGS  |
+--------+
Yun Zi Baby Giant Panda Cub San Diego Zoo 