+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tammylo
Photo URL    : https://www.flickr.com/photos/tammylo/8027732486/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 21 18:08:32 GMT+0200 2012
Upload Date  : Wed Sep 26 22:53:32 GMT+0200 2012
Geotag Info  : Latitude:32.729530, Longitude:-117.170562
Views        : 1,673
Comments     : 1


+---------+
|  TITLE  |
+---------+
Pandas @ San Diego Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"san diego zoo" animals panda pandasunlimited 