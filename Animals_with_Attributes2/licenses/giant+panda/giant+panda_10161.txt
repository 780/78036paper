+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : PauerKorde Photo
Photo URL    : https://www.flickr.com/photos/pauerkorde/3439200935/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 9 13:56:30 GMT+0200 2009
Upload Date  : Tue Apr 14 01:31:52 GMT+0200 2009
Views        : 862
Comments     : 5


+---------+
|  TITLE  |
+---------+
Dental Hygiene


+---------------+
|  DESCRIPTION  |
+---------------+
Giant Panda understands the benefits of a good tooth-brushing.. Or not..

<a href="http://farm4.static.flickr.com/3545/3439200935_a2e0e9f4c2_b.jpg" rel="nofollow">large</a>


+--------+
|  TAGS  |
+--------+
"giant panda" "national zoo" bear 