+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Soren Wolf
Photo URL    : https://www.flickr.com/photos/sorenwolf/15596874808/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 1 14:01:42 GMT+0200 2014
Upload Date  : Thu Nov 13 21:41:03 GMT+0100 2014
Views        : 180
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giant panda eating


+---------------+
|  DESCRIPTION  |
+---------------+
Panda from Vienna in Austria


+--------+
|  TAGS  |
+--------+
panda zoo animal giant mammal outdoor 