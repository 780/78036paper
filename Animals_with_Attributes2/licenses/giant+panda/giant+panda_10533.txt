+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : fortherock
Photo URL    : https://www.flickr.com/photos/fortherock/3899143794/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 2 09:27:58 GMT+0100 2008
Upload Date  : Tue Sep 8 04:37:52 GMT+0200 2009
Views        : 6,726
Comments     : 0


+---------+
|  TITLE  |
+---------+
Panda Bear - San Diego Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Panda Bear - San Diego Zoo


+--------+
|  TAGS  |
+--------+
Panda Bear San Diego Zoo 