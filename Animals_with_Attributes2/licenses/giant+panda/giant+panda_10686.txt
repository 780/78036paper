+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : emperornie
Photo URL    : https://www.flickr.com/photos/77326563@N06/14252290971/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 23 14:27:37 GMT+0200 2014
Upload Date  : Sat May 24 05:41:24 GMT+0200 2014
Geotag Info  : Latitude:31.054845, Longitude:121.719460
Views        : 291
Comments     : 0


+---------+
|  TITLE  |
+---------+
Panda


+---------------+
|  DESCRIPTION  |
+---------------+
Shanghai wildlife Park


+--------+
|  TAGS  |
+--------+
(no tags)