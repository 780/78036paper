+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : roland_aa
Photo URL    : https://www.flickr.com/photos/rolandaa/4293385894/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Nov 26 12:04:43 GMT+0100 2009
Upload Date  : Thu Jan 21 17:07:42 GMT+0100 2010
Views        : 410
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giant Panda


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"National Zoo" Zoo Animals Panda 