+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eileenmak
Photo URL    : https://www.flickr.com/photos/eileenmak/8247877459/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 17 23:51:29 GMT+0100 2012
Upload Date  : Thu Dec 6 02:49:35 GMT+0100 2012
Views        : 88
Comments     : 1


+---------+
|  TITLE  |
+---------+
Panda! 1


+---------------+
|  DESCRIPTION  |
+---------------+
Adelaide Zoo. It's quite a small zoo, smaller than Calgary Zoo, I think. But they have pandas, and that's all any zoo needs to attract visitors.


+--------+
|  TAGS  |
+--------+
Australia "Adelaide, SA" "Adelaide Zoo" 