+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Keith Roper
Photo URL    : https://www.flickr.com/photos/keithroper/8131585894/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 12 07:30:51 GMT+0200 2012
Upload Date  : Sun Oct 28 18:00:36 GMT+0100 2012
Views        : 2,086
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pandas 3


+---------------+
|  DESCRIPTION  |
+---------------+
Pandas at Beijing Zoo. October 2012


+--------+
|  TAGS  |
+--------+
Pandas China 