+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : LisaW123
Photo URL    : https://www.flickr.com/photos/pixellou/8662702688/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 23 14:18:45 GMT+0100 2012
Upload Date  : Fri Apr 19 08:38:23 GMT+0200 2013
Views        : 573
Comments     : 0


+---------+
|  TITLE  |
+---------+
Panda bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
california "san diego" zoo animal nature panda bear 