+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andrew and Annemarie
Photo URL    : https://www.flickr.com/photos/andrew_annemarie/14094703319/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 27 09:15:02 GMT+0200 2014
Upload Date  : Tue May 27 03:15:02 GMT+0200 2014
Geotag Info  : Latitude:30.737805, Longitude:104.143191
Views        : 1,508
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giant Panda


+---------------+
|  DESCRIPTION  |
+---------------+
Follow our trip at <a href="http://www.trekkingasia.co.uk" rel="nofollow">Trekking Asia</a>


+--------+
|  TAGS  |
+--------+
file:md5sum=b0306fd8d4b838361f37354f3cfb0ac7 file:sha1sig=39b9b19dbf413bdb8450a76b676f91bfa47d9877 panda Chengdu China Asia backpacking trekking sightseeing landscapes 