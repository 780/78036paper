+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gill_penney
Photo URL    : https://www.flickr.com/photos/gillpenney/3423954802/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 5 03:18:08 GMT+0200 2009
Upload Date  : Wed Apr 8 14:09:25 GMT+0200 2009
Views        : 2,671
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giant Panda, Chengdu, Sichuan april 2009 2224


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Giant Panda" Chengdu cubs bamboo China Chinese Sichuan 