+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 15, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : donjd2
Photo URL    : https://www.flickr.com/photos/ddebold/5567429710/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 26 10:17:52 GMT+0100 2011
Upload Date  : Mon Mar 28 09:24:58 GMT+0200 2011
Geotag Info  : Latitude:32.736333, Longitude:-117.150834
Views        : 3,903
Comments     : 0


+---------+
|  TITLE  |
+---------+
Giant Panda Splashing at San Diego Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"giant panda" "san diego" "san diego zoo" 