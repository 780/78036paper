+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Shayan (USA)
Photo URL    : https://www.flickr.com/photos/ssanyal/2854727655/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 13 12:21:39 GMT+0200 2008
Upload Date  : Sun Sep 14 08:42:16 GMT+0200 2008
Views        : 5,954
Comments     : 3


+---------+
|  TITLE  |
+---------+
Humpback Whale Fluke


+---------------+
|  DESCRIPTION  |
+---------------+
Monterey Bay - Whale Spotting - Am currently uploading additional pics related to this on <a href="http://www.shayansanyal.info" rel="nofollow">www.shayansanyal.info</a>


+--------+
|  TAGS  |
+--------+
whale "humpback whale" california monterey "whale watching" fluke usa "canon 300mm" "prime lense" "water spray" ocean pacific barnacles "sea gulls" "pacific ocean" boat "sea liions" "california sea lions" "300mm prime" 300mm flukemonterey Lifestyle Leisure 