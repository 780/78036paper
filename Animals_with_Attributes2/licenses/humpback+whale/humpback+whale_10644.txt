+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : spi516
Photo URL    : https://www.flickr.com/photos/spi/1352108903/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 8 10:26:24 GMT+0200 2007
Upload Date  : Mon Sep 10 02:01:26 GMT+0200 2007
Geotag Info  : Latitude:42.541445, Longitude:-70.616683
Views        : 22
Comments     : 0


+---------+
|  TITLE  |
+---------+
Signature wave


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
whale humpback 