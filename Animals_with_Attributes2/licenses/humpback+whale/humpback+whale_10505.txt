+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Christopher.Michel
Photo URL    : https://www.flickr.com/photos/cmichel67/12389514245/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Feb 5 11:55:37 GMT+0100 2014
Upload Date  : Sat Feb 8 19:15:16 GMT+0100 2014
Views        : 3,402
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whales


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Silver Bank" "Conscious Breath Adventures" whaleman "dominican republic" "In the Heart of the Sea" "Moby Dick" humpbacks ocean 