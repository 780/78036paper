+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : orca_bc
Photo URL    : https://www.flickr.com/photos/101951515@N07/10219061554/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 5 07:18:08 GMT+0200 2013
Upload Date  : Sat Oct 12 06:27:17 GMT+0200 2013
Views        : 255
Comments     : 0


+---------+
|  TITLE  |
+---------+
_MG_9180


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Blue Ocean Whale Watch" California "California Sea Lion" "California Sea Lions" "Humpback Whale" "Humpback Whales" "Marina State Beach" "Marine Life" "Marine Sanctuary" "Megaptera novaeangliae" Monterey "Monterey Bay" "Pacific Ocean" "Sea Lion" "Sea Lions" "United States of America" Whale "Zalophus californianus" baleen "baleen whale" cetacean cetaceans "in-close to shore" rainbow shore waves whales 