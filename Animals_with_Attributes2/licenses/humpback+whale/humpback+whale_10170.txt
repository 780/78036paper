+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Gregory "Slobirdr" Smith
Photo URL    : https://www.flickr.com/photos/slobirdr/14579123596/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 7 18:53:45 GMT+0200 2014
Upload Date  : Tue Jul 8 07:08:12 GMT+0200 2014
Geotag Info  : Latitude:35.309241, Longitude:-121.025047
Views        : 2,418
Comments     : 4


+---------+
|  TITLE  |
+---------+
Humpback Whale (Megaptera novaeangliae)


+---------------+
|  DESCRIPTION  |
+---------------+
A fun, late afternoon ride on the Sub Seas Whale Watching boat Dos Osos, took us through the flock of Sooty Shearwaters Mike Baird (thank you for the mail alert Mike!) wrote about.  That was on the way to watching the whales do at least thirty breaches (one that came up about ten metres from the boat) fifty tail slaps and innumerable pectoral waves.  Captain Kevin did a great job of finding the whales and putting what little sun there was at our backs...


+--------+
|  TAGS  |
+--------+
(no tags)