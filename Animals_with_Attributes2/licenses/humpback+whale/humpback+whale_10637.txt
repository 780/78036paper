+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : U. S. Fish and Wildlife Service - Northeast Region
Photo URL    : https://www.flickr.com/photos/usfwsnortheast/6959878878/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 23 09:03:32 GMT+0200 2012
Upload Date  : Mon Apr 23 15:09:27 GMT+0200 2012
Geotag Info  : Latitude:42.051418, Longitude:-70.187942
Views        : 4,327
Comments     : 2


+---------+
|  TITLE  |
+---------+
Photo of the Week - Humpback Whale (MA)


+---------------+
|  DESCRIPTION  |
+---------------+
Photo of the Week - 4/23/12

Humpback Whale photographed during a wale-watching tour out of Provincetown, MA.

Credit: Bill Thompson/USFWS


+--------+
|  TAGS  |
+--------+
"Humpback Whale" Provincetown MA "whale watching" "Atlantic Ocean" tail 