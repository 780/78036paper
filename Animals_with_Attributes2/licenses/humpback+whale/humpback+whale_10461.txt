+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daryl_mitchell
Photo URL    : https://www.flickr.com/photos/daryl_mitchell/23029947515/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Feb 11 11:47:52 GMT+0100 2015
Upload Date  : Sun Nov 15 09:30:39 GMT+0100 2015
Geotag Info  : Latitude:20.631970, Longitude:-105.421371
Views        : 41
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whales 39


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2015 winter "Puerto Vallarta" Mexico whale watching Banderas bay humpback 