+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sarah&Boston
Photo URL    : https://www.flickr.com/photos/pocheco/10803599863/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 28 07:40:06 GMT+0200 2013
Upload Date  : Mon Nov 11 19:07:33 GMT+0100 2013
Views        : 127
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whale


+---------------+
|  DESCRIPTION  |
+---------------+
Whale Watching in Gloucester


+--------+
|  TAGS  |
+--------+
(no tags)