+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Richard.Fisher
Photo URL    : https://www.flickr.com/photos/richardfisher/2987300999/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 3 12:31:37 GMT+0200 2008
Upload Date  : Fri Oct 31 01:41:38 GMT+0100 2008
Geotag Info  : Latitude:-25.048902, Longitude:153.050537
Views        : 1,983
Comments     : 0


+---------+
|  TITLE  |
+---------+
Southern Humpback Whale at Platypus Bay


+---------------+
|  DESCRIPTION  |
+---------------+
Mother.


+--------+
|  TAGS  |
+--------+
nikon d80 NIKKOR southern eastern humpback whale mother calf platypus hervey bay south east queensland australia great sandy marine park watching cetaceans 