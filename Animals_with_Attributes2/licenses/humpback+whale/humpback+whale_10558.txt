+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Polar Cruises
Photo URL    : https://www.flickr.com/photos/polarphotos/5962045211/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 11 11:30:49 GMT+0100 2011
Upload Date  : Fri Jul 22 01:01:02 GMT+0200 2011
Views        : 200
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback's in Wilhelmina Bay Antarctica-0791.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
Humpback


+--------+
|  TAGS  |
+--------+
whale 