+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : oliver.dodd
Photo URL    : https://www.flickr.com/photos/oliverdodd/8357137990/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 10 17:09:36 GMT+0100 2012
Upload Date  : Mon Jan 7 07:03:30 GMT+0100 2013
Views        : 325
Comments     : 0


+---------+
|  TITLE  |
+---------+
humpback dives


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
antarctica whale "humpback whale" humpback swim breach ocean sea "sea mammal" 