+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : natalielucier
Photo URL    : https://www.flickr.com/photos/natalielucier/3623113671/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 13 18:59:44 GMT+0200 2009
Upload Date  : Sun Jun 14 03:45:44 GMT+0200 2009
Views        : 1,066
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whale Tail


+---------------+
|  DESCRIPTION  |
+---------------+
humpback whale in Newfoundland, Canada.


+--------+
|  TAGS  |
+--------+
"humpback whale" tail 