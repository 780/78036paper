+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Javcon117*
Photo URL    : https://www.flickr.com/photos/javcon117/10440836403/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 25 16:18:54 GMT+0200 2013
Upload Date  : Wed Oct 23 16:32:35 GMT+0200 2013
Views        : 1,054
Comments     : 0


+---------+
|  TITLE  |
+---------+
Whale's Tail


+---------------+
|  DESCRIPTION  |
+---------------+
During my Maine vacation I went on a whale watching boat tour with Bar Harbor Whale Watching Company (see link below).  This is the tail of (I believe) a hump back whale.  After surfacing the whale dives back under the water.  This is about the most you will see of the tail when the whale dives.

<a href="http://www.barharborwhales.com/default.php" rel="nofollow">www.barharborwhales.com/default.php</a>


+--------+
|  TAGS  |
+--------+
Whale Tail Dive Maine "Bar Harbor" Watching Company Co "hump back" vacation boat cruise tour sightseeing water blue black Javcon117 "Frost Photos" July 2013 "New England" 