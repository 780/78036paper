+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : samurai_dave
Photo URL    : https://www.flickr.com/photos/88657298@N00/3394726674/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 23 11:54:41 GMT+0100 2009
Upload Date  : Sun Mar 29 10:14:45 GMT+0200 2009
Views        : 257
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_5383


+---------------+
|  DESCRIPTION  |
+---------------+
Mother and male &quot;friend&quot;.  Calf is just under the water.


+--------+
|  TAGS  |
+--------+
hawaii humpback whale-watching 