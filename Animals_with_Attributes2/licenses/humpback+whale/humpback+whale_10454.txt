+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : katevanheule
Photo URL    : https://www.flickr.com/photos/110320198@N03/11173016973/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 2 07:04:20 GMT+0100 2013
Upload Date  : Mon Dec 2 16:09:24 GMT+0100 2013
Views        : 36
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whales 2


+---------------+
|  DESCRIPTION  |
+---------------+
Wildlife


+--------+
|  TAGS  |
+--------+
(no tags)