+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Zippo S
Photo URL    : https://www.flickr.com/photos/briansummers/6047627743/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 24 14:20:54 GMT+0200 2010
Upload Date  : Tue Aug 16 04:22:05 GMT+0200 2011
Geotag Info  : Latitude:47.514000, Longitude:-52.619500
Views        : 39
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback whale


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)