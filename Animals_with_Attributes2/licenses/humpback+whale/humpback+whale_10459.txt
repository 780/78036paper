+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daryl_mitchell
Photo URL    : https://www.flickr.com/photos/daryl_mitchell/23003933556/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Feb 11 11:21:59 GMT+0100 2015
Upload Date  : Sun Nov 15 09:32:18 GMT+0100 2015
Geotag Info  : Latitude:20.631970, Longitude:-105.421371
Views        : 47
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whales 33


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2015 winter "Puerto Vallarta" Mexico whale watching Banderas bay humpback 