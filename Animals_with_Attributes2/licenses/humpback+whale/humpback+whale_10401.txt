+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rjshade
Photo URL    : https://www.flickr.com/photos/rjshade/15468783028/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 22 12:03:20 GMT+0200 2014
Upload Date  : Wed Oct 29 01:54:09 GMT+0100 2014
Views        : 5,770
Comments     : 1


+---------+
|  TITLE  |
+---------+
Humpback Whale


+---------------+
|  DESCRIPTION  |
+---------------+
Whale watching on a boat from Gloucester, MA back in the springtime. Whales were pretty excited and spent a lot of time breaching, waving, and generally showing off near the boat.


+--------+
|  TAGS  |
+--------+
massachusetts "whale watching" "humpback whale" humpback whale 