+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : markbyzewski
Photo URL    : https://www.flickr.com/photos/markbyzewski/11499562913/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 21 06:13:28 GMT+0200 2013
Upload Date  : Sun Dec 22 19:04:48 GMT+0100 2013
Views        : 117
Comments     : 0


+---------+
|  TITLE  |
+---------+
_MG_3168a


+---------------+
|  DESCRIPTION  |
+---------------+
Humpback Whales from the Matanuska on the Alaska Marine Highway.


+--------+
|  TAGS  |
+--------+
"Humpback Whale" Matanuska Alaska "Alaska Marine HIghway" 