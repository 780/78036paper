+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Pascal
Photo URL    : https://www.flickr.com/photos/walschots/7659900516/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 26 19:52:55 GMT+0200 2012
Upload Date  : Sat Jul 28 05:24:33 GMT+0200 2012
Geotag Info  : Latitude:48.106055, Longitude:-69.550323
Views        : 118
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback whales near zodiac


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)