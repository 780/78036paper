+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Percita
Photo URL    : https://www.flickr.com/photos/dittmars/1569267850/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 30 15:31:02 GMT+0200 2007
Upload Date  : Sun Oct 14 15:07:31 GMT+0200 2007
Views        : 666
Comments     : 3


+---------+
|  TITLE  |
+---------+
Merrimbula Humpback leaping out of the water  2


+---------------+
|  DESCRIPTION  |
+---------------+
This is the only jump we saw - I was on the other side of the boat - I climbed up on the seats and snapped over the heads of the others - I was lucky to catch it at all


+--------+
|  TAGS  |
+--------+
Whales "humpback whale" Merimbula 