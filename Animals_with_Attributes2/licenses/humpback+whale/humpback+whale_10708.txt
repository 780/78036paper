+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Indavar
Photo URL    : https://www.flickr.com/photos/indavar/15558925063/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 28 18:18:48 GMT+0200 2014
Upload Date  : Fri Jan 2 23:53:25 GMT+0100 2015
Views        : 73
Comments     : 0


+---------+
|  TITLE  |
+---------+
Whales


+---------------+
|  DESCRIPTION  |
+---------------+
Southern Alaska Adventures


+--------+
|  TAGS  |
+--------+
Ice Alaska "Southern Alaska" Whales Icebergs Glacier Glaciers "Humpback Whales" "Bubble net" "bubble net feeding" train "steam train" Skagway Juneau Sitka Ketchika "Glacier Bay" "Endicott Armn" Fjords 