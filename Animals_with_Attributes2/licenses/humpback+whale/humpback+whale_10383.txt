+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : regexman
Photo URL    : https://www.flickr.com/photos/regexman/10294152973/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 13 13:50:34 GMT+0200 2013
Upload Date  : Tue Oct 15 18:22:17 GMT+0200 2013
Geotag Info  : Latitude:37.616855, Longitude:-123.051428
Views        : 137
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback whale


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)