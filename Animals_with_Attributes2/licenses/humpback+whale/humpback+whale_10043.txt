+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : markbyzewski
Photo URL    : https://www.flickr.com/photos/markbyzewski/9654785449/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 14 21:30:59 GMT+0200 2013
Upload Date  : Mon Sep 2 21:15:14 GMT+0200 2013
Geotag Info  : Latitude:57.441838, Longitude:-133.710973
Views        : 449
Comments     : 0


+---------+
|  TITLE  |
+---------+
322HDRa


+---------------+
|  DESCRIPTION  |
+---------------+
Humpback Whales in Alaska.


+--------+
|  TAGS  |
+--------+
HDR "Humpback Whale" Alaska Ugly 