+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Richard.Fisher
Photo URL    : https://www.flickr.com/photos/richardfisher/2987266133/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 3 10:45:36 GMT+0200 2008
Upload Date  : Fri Oct 31 01:23:28 GMT+0100 2008
Geotag Info  : Latitude:-25.048902, Longitude:153.050537
Views        : 3,882
Comments     : 2


+---------+
|  TITLE  |
+---------+
Southern Humpback Whale at Platypus Bay


+---------------+
|  DESCRIPTION  |
+---------------+
Breaching calf.


+--------+
|  TAGS  |
+--------+
nikon d80 NIKKOR southern eastern humpback whale mother calf platypus hervey bay south east queensland australia great sandy marine park watching cetaceans 