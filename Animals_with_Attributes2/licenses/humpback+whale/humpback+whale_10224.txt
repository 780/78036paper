+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sylke Rohrlach
Photo URL    : https://www.flickr.com/photos/87895263@N06/16632349010/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 18 08:46:35 GMT+0200 2014
Upload Date  : Sun Mar 15 09:15:44 GMT+0100 2015
Geotag Info  : Latitude:-18.645719, Longitude:-174.111156
Views        : 1,431
Comments     : 1


+---------+
|  TITLE  |
+---------+
Young Male Humpback Whale-Megaptera novaeangliae


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Buckelwal Snorkeling Schnorcheln with Humpbacks big grey white Wal Cetacea Furchenwal Laurasiatheria Mysticeti Barten Bartenwal Balaenopteridae protected huge groß Riesen sanft Meer Meere friendly blubber fluke tail eye mammal Krill filter Seepocken barnacles mating season vulnerable rorqual Pacific Pazifik Whalewatching watching giant giants breaching breach breaches encounter 