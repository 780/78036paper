+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andrew Kalat
Photo URL    : https://www.flickr.com/photos/akalat/13588038063/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 22 13:29:37 GMT+0100 2014
Upload Date  : Wed Apr 2 22:00:52 GMT+0200 2014
Geotag Info  : Latitude:32.241898, Longitude:-64.865362
Views        : 426
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whale Tail during Dive off Bermuda


+---------------+
|  DESCRIPTION  |
+---------------+
Getting ready to dive deep after coming up to breath.


+--------+
|  TAGS  |
+--------+
Bermuda Humpback Whale Fin Slapping 