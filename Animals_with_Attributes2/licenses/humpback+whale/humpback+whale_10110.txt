+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Anup Shah
Photo URL    : https://www.flickr.com/photos/anupshah/15934571715/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 24 11:52:57 GMT+0100 2014
Upload Date  : Wed Dec 3 02:36:20 GMT+0100 2014
Views        : 685
Comments     : 0


+---------+
|  TITLE  |
+---------+
Breaching humpback whales


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Humpback Whales" Norway Tromso Tromsø "Whale watching" 