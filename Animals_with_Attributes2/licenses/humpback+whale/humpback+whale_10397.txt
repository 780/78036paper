+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mrmoorey
Photo URL    : https://www.flickr.com/photos/mrmoorey/14100897818/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 10 15:48:21 GMT+0200 2014
Upload Date  : Wed May 28 04:16:50 GMT+0200 2014
Geotag Info  : Latitude:20.909716, Longitude:-156.707938
Views        : 345
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whale


+---------------+
|  DESCRIPTION  |
+---------------+
Kaanapali, Maui. Taken during the Trilogy Excursions Kaanapali Snorkel trip


+--------+
|  TAGS  |
+--------+
whales "whale watching" mrmoorey "alan moore" alanmphoto www.alanmphoto.com "trilogy excursions" kaanapali maui "humpback whale" "Megaptera novaeangliae" 