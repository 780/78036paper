+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sylke Rohrlach
Photo URL    : https://www.flickr.com/photos/87895263@N06/16632589258/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 21 09:05:05 GMT+0200 2014
Upload Date  : Sun Mar 15 10:25:49 GMT+0100 2015
Geotag Info  : Latitude:-18.645719, Longitude:-174.111156
Views        : 1,688
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gropu of Humpback Whales-Megaptera novaeangliae


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Buckelwal Snorkeling Schnorcheln with Humpbacks big grey white Wal Cetacea Furchenwal Laurasiatheria Mysticeti Barten Bartenwal Balaenopteridae protected huge groß Riesen sanft Meer Meere friendly blubber fluke tail eye mammal Krill filter Seepocken barnacles mating season vulnerable rorqual Pacific Pazifik Whalewatching watching giant giants "taxonomy:binomial=Megaptera novaeangliae" 