+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kiskadee 3
Photo URL    : https://www.flickr.com/photos/kiskadee_3/8000309946/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 18 10:02:35 GMT+0200 2012
Upload Date  : Tue Sep 18 19:06:07 GMT+0200 2012
Geotag Info  : Latitude:59.793727, Longitude:-149.526673
Views        : 301
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whale 2012-06-18 (38)


+---------------+
|  DESCRIPTION  |
+---------------+
Humpback Whale tail fluke - 18 Jun 2012 - Resurrection Bay, Kenai Peninsula Borough, AK


+--------+
|  TAGS  |
+--------+
Alaska 