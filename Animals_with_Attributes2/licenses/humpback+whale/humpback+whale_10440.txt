+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : minicooper93402
Photo URL    : https://www.flickr.com/photos/minicooper93402/5920380793/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 9 18:40:11 GMT+0200 2011
Upload Date  : Sun Jul 10 05:58:40 GMT+0200 2011
Views        : 9,669
Comments     : 26


+---------+
|  TITLE  |
+---------+
humpback whale, morro rock and smokestacks


+---------------+
|  DESCRIPTION  |
+---------------+
the humpbacks put on an amazing show this afternoon.  the water was (very!) choppy, and a couple of people were seasick (over the side of the boat, thankfully) ... but the whales jumped out of the water for us ... an amazing experience!


+--------+
|  TAGS  |
+--------+
humpback whale "morro rock" smokestack "morro bay" "dos osos sub sea tours whale watching" breach 