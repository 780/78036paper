+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : markbyzewski
Photo URL    : https://www.flickr.com/photos/markbyzewski/9759886372/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 14 22:24:57 GMT+0200 2013
Upload Date  : Sun Sep 15 17:07:59 GMT+0200 2013
Views        : 220
Comments     : 0


+---------+
|  TITLE  |
+---------+
_MG_6413HDRa


+---------------+
|  DESCRIPTION  |
+---------------+
Humpback Whales in Alaska.


+--------+
|  TAGS  |
+--------+
HDR "Humpback Whale" Alaska 