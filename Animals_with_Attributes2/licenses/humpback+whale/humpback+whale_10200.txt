+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hgrimes
Photo URL    : https://www.flickr.com/photos/iamtheh/14997694858/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 26 06:32:05 GMT+0200 2014
Upload Date  : Tue Sep 9 02:58:22 GMT+0200 2014
Views        : 134
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback whale at Seacliff State Beach


+---------------+
|  DESCRIPTION  |
+---------------+
Aptos, CA


+--------+
|  TAGS  |
+--------+
whales humpbacks "humpback whales" "marine mammals" seacliff "seacliff state beach" "seacliff sb" california "monterey bay" aptos "santa cruz" awesome ocean 