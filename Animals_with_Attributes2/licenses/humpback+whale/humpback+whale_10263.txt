+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kris.layon
Photo URL    : https://www.flickr.com/photos/klayon/6029829711/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 10 16:37:54 GMT+0200 2011
Upload Date  : Wed Aug 10 22:41:37 GMT+0200 2011
Views        : 123
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback whale showing some fin


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2011 family 