+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Makuahine Pa'i Ki'i
Photo URL    : https://www.flickr.com/photos/hawaii-mcgraths/571235515/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 25 22:55:14 GMT+0100 2005
Upload Date  : Sat Mar 26 07:55:14 GMT+0100 2005
Views        : 71
Comments     : 0


+---------+
|  TITLE  |
+---------+
Blue Dolphin Cruise--Humpback Whale Tail--zoom


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)