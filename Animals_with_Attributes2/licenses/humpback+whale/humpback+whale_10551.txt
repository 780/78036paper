+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bazzat2003
Photo URL    : https://www.flickr.com/photos/bazzat/52896079/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 1 00:00:00 GMT+0100 2004
Upload Date  : Sun Oct 16 08:44:25 GMT+0200 2005
Views        : 358
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback


+---------------+
|  DESCRIPTION  |
+---------------+
Whale - near Vernadsky Station, Antarctic Peninsula


+--------+
|  TAGS  |
+--------+
keeper 