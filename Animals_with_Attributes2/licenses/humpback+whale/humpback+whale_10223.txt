+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andrew Kalat
Photo URL    : https://www.flickr.com/photos/akalat/13587995525/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 22 12:46:16 GMT+0100 2014
Upload Date  : Wed Apr 2 22:00:44 GMT+0200 2014
Geotag Info  : Latitude:32.241898, Longitude:-64.865362
Views        : 612
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whale Dorsal Fin off Bermuda


+---------------+
|  DESCRIPTION  |
+---------------+
A humpback whale coming up to breath.


+--------+
|  TAGS  |
+--------+
Bermuda Humpback Whale Fin Slapping 