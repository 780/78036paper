+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mack_L
Photo URL    : https://www.flickr.com/photos/mack46/14227876009/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 11 11:53:21 GMT+0200 2014
Upload Date  : Fri Jun 13 21:52:21 GMT+0200 2014
Views        : 214
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whales


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Alaska Un-Cruise "Inside Passage Western Coves" "humpback whales" "Wilderness Explorer" 