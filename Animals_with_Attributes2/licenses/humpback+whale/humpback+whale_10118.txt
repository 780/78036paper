+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stonehippo
Photo URL    : https://www.flickr.com/photos/stonehippo/13977825710/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 11 15:49:00 GMT+0200 2014
Upload Date  : Mon May 12 01:31:44 GMT+0200 2014
Views        : 283
Comments     : 0


+---------+
|  TITLE  |
+---------+
diving flukes


+---------------+
|  DESCRIPTION  |
+---------------+
Humpback whale diving, Stellwagoen Bank, Massachusetts


+--------+
|  TAGS  |
+--------+
whale animals wildlife 