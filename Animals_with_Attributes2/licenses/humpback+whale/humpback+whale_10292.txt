+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dominic Sherony
Photo URL    : https://www.flickr.com/photos/9765210@N03/5227045149/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 14 11:58:12 GMT+0200 2010
Upload Date  : Thu Dec 2 23:51:08 GMT+0100 2010
Views        : 244
Comments     : 1


+---------+
|  TITLE  |
+---------+
Humpback Whale with Herring Gull  Stellwagen Banks Marine Sanctuary


+---------------+
|  DESCRIPTION  |
+---------------+
Photo take at Stellwagen Banks Marine Sanctuary


+--------+
|  TAGS  |
+--------+
(no tags)