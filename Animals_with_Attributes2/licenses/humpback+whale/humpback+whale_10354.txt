+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Madeleine_H
Photo URL    : https://www.flickr.com/photos/madeleine_h/7592873152/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 14 13:25:01 GMT+0200 2012
Upload Date  : Tue Jul 17 22:58:45 GMT+0200 2012
Geotag Info  : Latitude:50.929439, Longitude:-127.641563
Views        : 416
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback whale


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
BC "British Columbia" Canada DS5000 Humpback RTW "Vancouver Island" whale 