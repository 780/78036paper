+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Percita
Photo URL    : https://www.flickr.com/photos/dittmars/8089688308/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 4 12:32:56 GMT+0200 2012
Upload Date  : Mon Oct 15 12:02:10 GMT+0200 2012
Geotag Info  : Latitude:-37.102882, Longitude:150.048677
Views        : 1,445
Comments     : 5


+---------+
|  TITLE  |
+---------+
Humpback whale - Eden NSW


+---------------+
|  DESCRIPTION  |
+---------------+
Taking a breath


+--------+
|  TAGS  |
+--------+
humpback Whales Eden 