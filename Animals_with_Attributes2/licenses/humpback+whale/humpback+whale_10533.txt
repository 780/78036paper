+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mack_L
Photo URL    : https://www.flickr.com/photos/mack46/14228153407/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 14 23:46:42 GMT+0200 2014
Upload Date  : Fri Jun 13 21:52:13 GMT+0200 2014
Views        : 323
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whales


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Alaska Un-Cruise "Inside Passage Western Coves" "humpback whales" "Wilderness Explorer" 