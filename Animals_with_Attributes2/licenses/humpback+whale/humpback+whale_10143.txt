+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : 3n
Photo URL    : https://www.flickr.com/photos/3n/3681415101/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 19 10:58:13 GMT+0200 2009
Upload Date  : Thu Jul 2 19:01:19 GMT+0200 2009
Geotag Info  : Latitude:59.890659, Longitude:-149.349517
Views        : 196
Comments     : 0


+---------+
|  TITLE  |
+---------+
how the humpback whale got its name


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Geotagged square "humpback whale" 