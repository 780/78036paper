+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jackoscage
Photo URL    : https://www.flickr.com/photos/jackoscage/14660656215/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 8 12:38:42 GMT+0200 2014
Upload Date  : Tue Jul 15 13:56:28 GMT+0200 2014
Views        : 39
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback Whales


+---------------+
|  DESCRIPTION  |
+---------------+
Whale watching in Kalbarri


+--------+
|  TAGS  |
+--------+
(no tags)