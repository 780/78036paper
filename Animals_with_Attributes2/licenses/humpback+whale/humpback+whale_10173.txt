+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Gregory "Slobirdr" Smith
Photo URL    : https://www.flickr.com/photos/slobirdr/14902935282/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 12 10:01:01 GMT+0200 2014
Upload Date  : Wed Aug 13 06:32:30 GMT+0200 2014
Views        : 11,880
Comments     : 6


+---------+
|  TITLE  |
+---------+
Humpback Whale (Megaptera novaeangliae)


+---------------+
|  DESCRIPTION  |
+---------------+
A really nice day out on the waters off of Morro Bay, CA. One of the ways to prepare for photographing whales is to watch the attendant sea lions in the area.  When they bunch up and gather near the surface, a whale will be surfacing shortly.  When this whale started to dive it just sort of stopped and started waving its tail in the air.  Pretty cool behaviour...


+--------+
|  TAGS  |
+--------+
"Humpback Whale (Megaptera novaeangliae)" "tail wave" "California Sea Lion" "Morro Bay" "marine mammal" "Gregory Slobirdr Smith" 