+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jennifer Turek
Photo URL    : https://www.flickr.com/photos/groovegrrrrrl/8440756244/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 24 17:06:34 GMT+0100 2013
Upload Date  : Sun Feb 3 10:17:53 GMT+0100 2013
Views        : 50
Comments     : 0


+---------+
|  TITLE  |
+---------+
Maui


+---------------+
|  DESCRIPTION  |
+---------------+
Hawaii


+--------+
|  TAGS  |
+--------+
Maui Hawaii "Whale Watching" "Humpback Whale" Water "Pacific Whale Foundation" 