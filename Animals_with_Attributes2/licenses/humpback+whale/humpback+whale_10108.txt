+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : speedphotos
Photo URL    : https://www.flickr.com/photos/dalelafollette/3213356696/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 14 19:27:50 GMT+0100 2009
Upload Date  : Tue Jan 20 19:00:42 GMT+0100 2009
Views        : 1,959
Comments     : 4


+---------+
|  TITLE  |
+---------+
Breaching humpback whale, Kauai


+---------------+
|  DESCRIPTION  |
+---------------+
Huge seas, long lens, many whales, many shots . . . . . of sky, boat deck, and a few of breaching whales.  This guy is spinning to make sure that he lands on his side as landing on his belly could be fatal. Why he is doing it is to show his strength to a female that is nearby.  There were three males courting her.  Can't wait to do it again!


+--------+
|  TAGS  |
+--------+
Kauai Hawaii "humpback whale" breaching 