+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hgrimes
Photo URL    : https://www.flickr.com/photos/iamtheh/15184277515/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 26 19:38:11 GMT+0200 2014
Upload Date  : Tue Sep 9 02:58:19 GMT+0200 2014
Views        : 142
Comments     : 0


+---------+
|  TITLE  |
+---------+
Humpback whale at Seacliff State Beach


+---------------+
|  DESCRIPTION  |
+---------------+
Aptos, CA


+--------+
|  TAGS  |
+--------+
whales humpbacks "humpback whales" "marine mammals" seacliff "seacliff state beach" "seacliff sb" california "monterey bay" aptos "santa cruz" awesome ocean 