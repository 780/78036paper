+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/7062801809/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 7 15:43:34 GMT+0200 2012
Upload Date  : Tue Apr 10 03:03:06 GMT+0200 2012
Geotag Info  : Latitude:42.462773, Longitude:-71.093666
Views        : 2,904
Comments     : 0


+---------+
|  TITLE  |
+---------+
Snow Leopard Teeth


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
stone zoo massachusetts cat snow leopard teeth profile mouth open 