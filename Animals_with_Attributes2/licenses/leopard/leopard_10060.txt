+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lazurite
Photo URL    : https://www.flickr.com/photos/lazurite/4042256828/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 23 15:10:29 GMT+0200 2009
Upload Date  : Sun Oct 25 09:41:33 GMT+0100 2009
Geotag Info  : Latitude:38.634520, Longitude:-90.291405
Views        : 275
Comments     : 1


+---------+
|  TITLE  |
+---------+
Amur Leopard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"St. Louis Zoo" zoo nature wildlife animals leopard "amur leopard" 