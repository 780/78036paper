+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Danny Nicholson
Photo URL    : https://www.flickr.com/photos/dannynic/5645526751/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 21 16:35:07 GMT+0200 2011
Upload Date  : Sat Apr 23 13:24:10 GMT+0200 2011
Views        : 819
Comments     : 2


+---------+
|  TITLE  |
+---------+
Xizi


+---------------+
|  DESCRIPTION  |
+---------------+
Wildlife photography day at Wildlife Heritage Foundation, Headcorn.

<a href="http://www.whf.org.uk" rel="nofollow">www.whf.org.uk</a>


+--------+
|  TAGS  |
+--------+
cat wild wildlife predator teeth nature conservation animal claw carnivore tiger cheetah lynx endangered breeding fur leopard xizi whf 5cardflickr feline whisker spot stripe protected 