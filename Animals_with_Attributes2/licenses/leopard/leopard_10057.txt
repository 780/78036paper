+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : wwarby
Photo URL    : https://www.flickr.com/photos/wwarby/3278223987/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 13 14:42:50 GMT+0100 2009
Upload Date  : Sat Feb 14 16:14:02 GMT+0100 2009
Geotag Info  : Latitude:50.994030, Longitude:-1.280160
Views        : 6,374
Comments     : 1


+---------+
|  TITLE  |
+---------+
Amur Leopard


+---------------+
|  DESCRIPTION  |
+---------------+
Portrait of an amur leopard at Marwell Zoo

PERMISSION TO USE: you are welcome to use this photo free of charge for any purpose including commercial. I am not concerned with how attribution is provided - a link to my flickr page or my name is fine. If the used in a context where attribution is impractical, that's fine too. I want my photography to be shared widely. I like hearing about where my photos have been used so please send me links, screenshots or photos where possible.


+--------+
|  TAGS  |
+--------+
70-300mm E-3 Marwell Olympus SLR Zuiko "Zuiko Digital" "amur leopard" animal captivity cat digital-camera digital-slr family leopard mammal outdoors portrait spots whiskers wildlife zoo 