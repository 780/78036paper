+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pelican
Photo URL    : https://www.flickr.com/photos/pelican/15612529918/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 14 16:53:14 GMT+0100 2014
Upload Date  : Sun Nov 16 01:03:18 GMT+0100 2014
Views        : 386
Comments     : 0


+---------+
|  TITLE  |
+---------+
Emirates Park Zoo, Abu Dhabi, UAE


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"smc PENTAX-DA 18-135mm F3.5-5.6 ED AL [IF] DC WR" K-5 "Abu Dhabi" "Emirates Park Zoo" leopard 