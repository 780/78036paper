+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mmmavocado
Photo URL    : https://www.flickr.com/photos/mmmavocado/9135539109/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 17 07:26:01 GMT+0200 2013
Upload Date  : Tue Jun 25 20:50:32 GMT+0200 2013
Geotag Info  : Latitude:-24.769961, Longitude:31.383991
Views        : 247
Comments     : 0


+---------+
|  TITLE  |
+---------+
leopard with cub


+---------------+
|  DESCRIPTION  |
+---------------+
Inyati Game Lodge, Sabi Sand area, Kruger


+--------+
|  TAGS  |
+--------+
"South Africa" 