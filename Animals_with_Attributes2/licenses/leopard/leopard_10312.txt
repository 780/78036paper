+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : H Sanchez
Photo URL    : https://www.flickr.com/photos/troubleshots/214335659/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 21 11:49:41 GMT+0200 2004
Upload Date  : Sun Aug 13 22:40:32 GMT+0200 2006
Views        : 176
Comments     : 0


+---------+
|  TITLE  |
+---------+
Leopard yawning


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Morelia Mexico 