+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hobgadlng
Photo URL    : https://www.flickr.com/photos/hobgadlng/7668111964/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 29 15:53:15 GMT+0200 2012
Upload Date  : Sun Jul 29 14:38:21 GMT+0200 2012
Geotag Info  : Latitude:-20.413982, Longitude:16.672439
Views        : 390
Comments     : 0


+---------+
|  TITLE  |
+---------+
Leopard looking a little Derp-y


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Namibia Okonjima leopard animal "Africat Foundation" derp 