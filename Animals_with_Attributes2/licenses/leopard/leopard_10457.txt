+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike's Birds
Photo URL    : https://www.flickr.com/photos/pazzani/6619581929/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 10 01:12:05 GMT+0100 2011
Upload Date  : Mon Jan 2 13:35:35 GMT+0100 2012
Geotag Info  : Latitude:-24.796187, Longitude:31.498266
Views        : 286
Comments     : 0


+---------+
|  TITLE  |
+---------+
Leopards of Londolozi


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)