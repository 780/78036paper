+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mats-ing
Photo URL    : https://www.flickr.com/photos/matsing/24153352845/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 3 19:55:23 GMT+0100 2016
Upload Date  : Sun Jan 3 20:00:16 GMT+0100 2016
Views        : 81
Comments     : 0


+---------+
|  TITLE  |
+---------+
Leopard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
kruger safari south-africa 