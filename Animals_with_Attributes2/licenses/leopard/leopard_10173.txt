+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : idnas71
Photo URL    : https://www.flickr.com/photos/28914290@N05/4273648358/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 6 19:10:29 GMT+0100 2009
Upload Date  : Thu Jan 14 07:22:05 GMT+0100 2010
Views        : 59
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_5294


+---------------+
|  DESCRIPTION  |
+---------------+
Leopard, Seronera Valley


+--------+
|  TAGS  |
+--------+
(no tags)