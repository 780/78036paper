+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : TonyParkin67
Photo URL    : https://www.flickr.com/photos/parkituk/6918607506/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 10 13:03:49 GMT+0200 2012
Upload Date  : Tue Apr 10 17:28:25 GMT+0200 2012
Geotag Info  : Latitude:53.495245, Longitude:-1.049966
Views        : 341
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_6672


+---------------+
|  DESCRIPTION  |
+---------------+
Leopard @ Yorkshire Wildlife Park


+--------+
|  TAGS  |
+--------+
"Yorkshire Wildlife Park" Doncaster leopard 