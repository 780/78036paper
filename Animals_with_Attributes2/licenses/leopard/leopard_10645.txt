+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ltshears
Photo URL    : https://www.flickr.com/photos/19598613@N00/2288765927/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 24 15:56:29 GMT+0100 2008
Upload Date  : Sun Feb 24 21:56:29 GMT+0100 2008
Views        : 4,827
Comments     : 8


+---------+
|  TITLE  |
+---------+
SnowLeopard


+---------------+
|  DESCRIPTION  |
+---------------+
Snow Leopard at Louisville Zoo in Kentucky.


+--------+
|  TAGS  |
+--------+
"Snow Leopard" Leopard Animal zoo Cat ltshears "Louisville Zoo" ZoosoftheSouth 