+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Bitterjug
Photo URL    : https://www.flickr.com/photos/bitterjug/10144114066/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 4 14:13:23 GMT+0200 2013
Upload Date  : Mon Oct 7 22:24:45 GMT+0200 2013
Views        : 282
Comments     : 0


+---------+
|  TITLE  |
+---------+
Snow leopard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)