+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lakpuratravels
Photo URL    : https://www.flickr.com/photos/lakpura/15848147296/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 24 22:17:29 GMT+0100 2014
Upload Date  : Tue Nov 25 07:19:19 GMT+0100 2014
Views        : 167
Comments     : 0


+---------+
|  TITLE  |
+---------+
Leopards Watching Sri Lanka


+---------------+
|  DESCRIPTION  |
+---------------+
Leopards Watching Sri Lanka


+--------+
|  TAGS  |
+--------+
(no tags)