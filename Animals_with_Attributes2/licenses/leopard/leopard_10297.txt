+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dunleavy Family
Photo URL    : https://www.flickr.com/photos/dunleavy_family/3496338151/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 2 12:32:11 GMT+0200 2009
Upload Date  : Sun May 3 14:56:29 GMT+0200 2009
Geotag Info  : Latitude:50.994056, Longitude:-1.279985
Views        : 792
Comments     : 0


+---------+
|  TITLE  |
+---------+
leopard sleeping


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Marwell Wildlife" Marwell Zoo leopard 