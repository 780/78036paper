+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pelican
Photo URL    : https://www.flickr.com/photos/pelican/2579091661/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 2 13:51:26 GMT+0200 2008
Upload Date  : Sun Jun 15 08:01:02 GMT+0200 2008
Views        : 531
Comments     : 0


+---------+
|  TITLE  |
+---------+
Oji zoo, Kobe, Japan


+---------------+
|  DESCRIPTION  |
+---------------+
Amur Leopard. It is always difficult to distinguish a leopard as being different from a jaguar.


+--------+
|  TAGS  |
+--------+
k100d "sigma apo 70-300mm F4-5.6" zoo oji kobe leopard "amur leopard" 