+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/22396353492/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 5 13:23:46 GMT+0200 2015
Upload Date  : Fri Oct 23 15:28:36 GMT+0200 2015
Geotag Info  : Latitude:41.787329, Longitude:-71.417430
Views        : 309
Comments     : 0


+---------+
|  TITLE  |
+---------+
Snow Leopards Playing


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"roger williams park" zoo animal big cat snow leopard 