+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Christopher.Michel
Photo URL    : https://www.flickr.com/photos/cmichel67/10028085914/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 8 00:55:59 GMT+0200 2013
Upload Date  : Tue Oct 1 00:56:37 GMT+0200 2013
Views        : 1,474
Comments     : 1


+---------+
|  TITLE  |
+---------+
Masai Mara


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Christopher Michel" Leopard "Masai Mara" "Kenya Christopher Michel" 