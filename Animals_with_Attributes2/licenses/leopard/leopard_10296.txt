+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael Bentley
Photo URL    : https://www.flickr.com/photos/donhomer/7638511946/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 22 11:51:22 GMT+0200 2012
Upload Date  : Tue Jul 24 20:27:07 GMT+0200 2012
Geotag Info  : Latitude:39.326596, Longitude:-76.644916
Views        : 1,045
Comments     : 0


+---------+
|  TITLE  |
+---------+
Leopard Sleeping in Tree


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo "baltimore zoo" leopard "Canon EF 135mm f/2L USM" 