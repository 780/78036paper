+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : David Davies
Photo URL    : https://www.flickr.com/photos/davies/14086587820/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 25 17:03:49 GMT+0200 2014
Upload Date  : Mon May 26 12:32:05 GMT+0200 2014
Views        : 403
Comments     : 0


+---------+
|  TITLE  |
+---------+
Amur leopard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)