+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/11061242225/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 9 15:57:11 GMT+0100 2013
Upload Date  : Tue Nov 26 05:19:11 GMT+0100 2013
Geotag Info  : Latitude:41.787329, Longitude:-71.417430
Views        : 1,433
Comments     : 0


+---------+
|  TITLE  |
+---------+
Snow Leopard Posed


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"roger williams" park zoo "rhode island" snow leopard big cat spots front teeth 