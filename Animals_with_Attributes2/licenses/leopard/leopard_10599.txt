+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike Miley
Photo URL    : https://www.flickr.com/photos/mike_miley/3868983777/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 29 13:44:21 GMT+0200 2009
Upload Date  : Sun Aug 30 06:01:52 GMT+0200 2009
Geotag Info  : Latitude:38.888803, Longitude:-77.026509
Views        : 432
Comments     : 3


+---------+
|  TITLE  |
+---------+
Snow Leopard


+---------------+
|  DESCRIPTION  |
+---------------+
Washington DC Day 1, Castle


+--------+
|  TAGS  |
+--------+
"Washington DC" DC Smithsonian "Museum of National History" 2009 "Snow Leopard" USA 