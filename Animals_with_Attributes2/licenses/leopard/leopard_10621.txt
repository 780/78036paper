+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flowcomm
Photo URL    : https://www.flickr.com/photos/flowcomm/8087710484/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 26 12:36:02 GMT+0200 2012
Upload Date  : Sun Oct 14 22:54:13 GMT+0200 2012
Geotag Info  : Latitude:27.037969, Longitude:88.263702
Views        : 517
Comments     : 0


+---------+
|  TITLE  |
+---------+
Snow leopard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
India Darjeeling "Himalayan Zoological Park" 