+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Arctic Wolf Pictures
Photo URL    : https://www.flickr.com/photos/arcticwoof/20476375520/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 17 22:34:20 GMT+0200 2015
Upload Date  : Mon Aug 17 22:55:52 GMT+0200 2015
Views        : 49
Comments     : 0


+---------+
|  TITLE  |
+---------+
Snowleopard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
feline snow leopard 