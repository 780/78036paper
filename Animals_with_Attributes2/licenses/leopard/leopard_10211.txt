+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : René Clausen Nielsen
Photo URL    : https://www.flickr.com/photos/shevy_dk/6062748151/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 29 10:37:33 GMT+0200 2011
Upload Date  : Sat Aug 20 22:51:34 GMT+0200 2011
Geotag Info  : Latitude:-24.515167, Longitude:30.903333
Views        : 666
Comments     : 0


+---------+
|  TITLE  |
+---------+
Leopard - Moholoholo Wildlife Rehab Centre


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"South Africa" Sydafrika "Panorama Route" Leopard 