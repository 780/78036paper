+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Thimindu
Photo URL    : https://www.flickr.com/photos/thimindu/5849452004/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 24 06:01:33 GMT+0200 2011
Upload Date  : Sun Jun 19 18:49:32 GMT+0200 2011
Views        : 928
Comments     : 4


+---------+
|  TITLE  |
+---------+
Yawning Leopard


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Leopard Mamals "National Parks" "Panthera pardus" "Panthera pardus kotiya" "Sri Lankan Leopard" Wildlife "Yala National Park" 