+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : michaeljohnbutton
Photo URL    : https://www.flickr.com/photos/michaeljohnbutton/14222864736/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 16 11:07:55 GMT+0200 2014
Upload Date  : Thu May 22 19:58:41 GMT+0200 2014
Views        : 217
Comments     : 0


+---------+
|  TITLE  |
+---------+
Copenhagen 2014 - Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Copenhagen 2014 - Zoo


+--------+
|  TAGS  |
+--------+
2014 Copenhagen leopard zoo Denmark 