+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pelican
Photo URL    : https://www.flickr.com/photos/pelican/8444006879/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 3 11:00:41 GMT+0100 2013
Upload Date  : Mon Feb 4 16:17:15 GMT+0100 2013
Views        : 1,066
Comments     : 1


+---------+
|  TITLE  |
+---------+
Oji zoo, Kobe, Japan


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Sigma 70-300mm F4-5.6 Macro" K-5 oji zoo kobe "Amur leopard" FlickrBigCats 