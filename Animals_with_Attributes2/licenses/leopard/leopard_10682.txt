+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/6894226523/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 11 14:35:42 GMT+0100 2012
Upload Date  : Sat Feb 18 02:49:26 GMT+0100 2012
Geotag Info  : Latitude:52.644677, Longitude:-1.509418
Views        : 475
Comments     : 1


+---------+
|  TITLE  |
+---------+
Twycross Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Snow Leopards


+--------+
|  TAGS  |
+--------+
"Twycross Zoo" "Snow Leopards" 