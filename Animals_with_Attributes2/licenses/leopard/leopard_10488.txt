+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ajari
Photo URL    : https://www.flickr.com/photos/ajari/3113632194/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 14 16:53:05 GMT+0100 2008
Upload Date  : Tue Dec 16 16:38:22 GMT+0100 2008
Views        : 2,797
Comments     : 34


+---------+
|  TITLE  |
+---------+
National Museum of Nature and Science,Tokyo_22


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
tokyo 東京 Japan 日本 Nikon D300 "National Museum" 博物館 ueno 上野 "AF-S Micro NIKKOR 60mm F2.8G ED" Leopard 豹 剥製 Stuffing 動物 Animal 