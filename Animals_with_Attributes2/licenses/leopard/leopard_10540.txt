+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : HBarrison
Photo URL    : https://www.flickr.com/photos/hbarrison/7375040348/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 18 16:43:58 GMT+0200 2012
Upload Date  : Fri Jun 15 17:45:56 GMT+0200 2012
Geotag Info  : Latitude:-24.873355, Longitude:31.643371
Views        : 650
Comments     : 1


+---------+
|  TITLE  |
+---------+
Sabi_2012 05 18_0537


+---------------+
|  DESCRIPTION  |
+---------------+
Evening Game Drive  /  The African Leopard (Panthera pardus pardus) is a leopard subspecies occurring across most of sub-Saharan Africa.  They are becoming increasingly rare outside protected areas.


+--------+
|  TAGS  |
+--------+
Africa HBarrison "Harvey Barrison" Tauck "Sabi Sabi" "Kruger National Park" "South Africa" "African Leopard" Leopard "Panthera pardus pardus" "Taxonomy:binomial=Panthera pardus" 