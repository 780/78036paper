+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tropicaLiving - Jessy Eykendorp
Photo URL    : https://www.flickr.com/photos/tropicaliving/3672347622/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 11 16:39:56 GMT+0100 2009
Upload Date  : Mon Jun 29 18:00:45 GMT+0200 2009
Geotag Info  : Latitude:-8.634400, Longitude:115.288295
Views        : 32,734
Comments     : 167


+---------+
|  TITLE  |
+---------+
Deadly Beauty


+---------------+
|  DESCRIPTION  |
+---------------+
The leopard (Panthera pardus) is the smallest of the four &quot;big cats&quot; in the genus Panthera; the other three are the tiger, lion and jaguar.
The leopard is an agile and stealthy predator. Although smaller than the other members of the Panthera genus, the leopard is still able to take large prey given a massive skull that well utilizes powerful jaw muscles...(<a href="http://en.wikipedia.org/wiki/Leopard" rel="nofollow"><b>Wikipedia</b></a>)
The image was taken in Bali Safary Park, Bali


<a href="http://animals.nationalgeographic.com/animals/mammals/leopard.html" rel="nofollow"><b>Leopard Profile</b></a>

Canon EOS 50D
EF 70-300mm f/4.0-5.6 IS USM
0.04sec • f/9 • 300 mm • ISO 200
shooting with JPEG
image recovery in Lightroom


+--------+
|  TAGS  |
+--------+
leopard "panthera pardus" "big cat" Endangered "Animal Planet" wikipedia water clouds sky rocks light 