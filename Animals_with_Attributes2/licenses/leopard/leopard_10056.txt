+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nick@
Photo URL    : https://www.flickr.com/photos/nic1/2555805191/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 31 13:56:58 GMT+0200 2008
Upload Date  : Fri Jun 6 19:11:29 GMT+0200 2008
Geotag Info  : Latitude:50.991582, Longitude:-1.272826
Views        : 8,320
Comments     : 8


+---------+
|  TITLE  |
+---------+
Amur leopard


+---------------+
|  DESCRIPTION  |
+---------------+
Amur leopard having a cat nap in the afternoon.


+--------+
|  TAGS  |
+--------+
"Amur leopard" "marwell zoo" sleeping FlickrBigCats "cat nap" 