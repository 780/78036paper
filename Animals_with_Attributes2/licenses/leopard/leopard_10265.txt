+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ciamabue
Photo URL    : https://www.flickr.com/photos/ciamabue/9535947069/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 28 16:07:20 GMT+0200 2013
Upload Date  : Sun Aug 18 15:49:00 GMT+0200 2013
Geotag Info  : Latitude:-24.150187, Longitude:30.643000
Views        : 437
Comments     : 0


+---------+
|  TITLE  |
+---------+
Leopard female drags kill to cub


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Animals Antelope Garonga Holidays Impala "Makalali Conservancy" Safari "South Africa" 