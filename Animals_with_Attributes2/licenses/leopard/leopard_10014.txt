+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/12785237385/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 22 13:58:43 GMT+0100 2014
Upload Date  : Wed Feb 26 05:10:42 GMT+0100 2014
Geotag Info  : Latitude:42.462697, Longitude:-71.093636
Views        : 1,633
Comments     : 0


+---------+
|  TITLE  |
+---------+
Snow Leopard by a Post


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
stone zoo massachusetts big cat snow leopard winter male 