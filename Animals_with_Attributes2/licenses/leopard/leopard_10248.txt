+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pedrol
Photo URL    : https://www.flickr.com/photos/9732968@N08/714923694/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 7 16:07:25 GMT+0200 2007
Upload Date  : Wed Jul 4 12:38:08 GMT+0200 2007
Views        : 1,896
Comments     : 0


+---------+
|  TITLE  |
+---------+
LEOPARD by peter laws


+---------------+
|  DESCRIPTION  |
+---------------+
Yes, it's a view in a zoo but a cracker nevertheless


+--------+
|  TAGS  |
+--------+
zoo "wild animals" cats spots leopard 