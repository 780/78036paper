+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nelson Minar
Photo URL    : https://www.flickr.com/photos/nelsonminar/53671255/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 16 14:37:49 GMT+0200 2005
Upload Date  : Tue Oct 18 10:30:49 GMT+0200 2005
Views        : 448
Comments     : 0


+---------+
|  TITLE  |
+---------+
Appenzeller cows


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
cow appenzell switzerland 