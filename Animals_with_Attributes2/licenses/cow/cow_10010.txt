+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : 2sirius
Photo URL    : https://www.flickr.com/photos/peterv/5157276590/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 7 05:17:29 GMT+0100 2010
Upload Date  : Mon Nov 8 05:57:50 GMT+0100 2010
Views        : 1,136
Comments     : 2


+---------+
|  TITLE  |
+---------+
05-SAM_1005


+---------------+
|  DESCRIPTION  |
+---------------+
Q: How do you get a cow to move quickly?
A: You look at it.

Apparently cows that are unaffected by trucks passing by on the highway will get spooked when a guy like me looks at them from the side of the highway.


+--------+
|  TAGS  |
+--------+
Canada Saskatchewan highway cow 