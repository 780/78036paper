+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bestbib&tucker
Photo URL    : https://www.flickr.com/photos/43992178@N00/1603403178/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Oct 2 12:03:55 GMT+0200 2007
Upload Date  : Wed Oct 17 21:21:24 GMT+0200 2007
Views        : 149
Comments     : 0


+---------+
|  TITLE  |
+---------+
Guardian Cows.


+---------------+
|  DESCRIPTION  |
+---------------+
There were about 6 cows a'chillin by the circle. My camera and I engaged these 2 in a staring contest.


+--------+
|  TAGS  |
+--------+
cows honeymoon ireland "county limerick" 