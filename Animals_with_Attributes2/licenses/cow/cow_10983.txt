+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stachelbeer
Photo URL    : https://www.flickr.com/photos/mario_storch/14746514386/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 28 12:44:59 GMT+0200 2014
Upload Date  : Mon Jul 28 20:19:28 GMT+0200 2014
Geotag Info  : Latitude:47.175922, Longitude:11.772516
Views        : 47
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kuh


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)