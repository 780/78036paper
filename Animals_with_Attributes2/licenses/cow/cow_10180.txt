+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : wattpublishing
Photo URL    : https://www.flickr.com/photos/wattagnet/6721749683/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 23 18:26:17 GMT+0200 2009
Upload Date  : Wed Jan 18 21:06:16 GMT+0100 2012
Views        : 1,230
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow in pasture


+---------------+
|  DESCRIPTION  |
+---------------+
A cow stands in a green pasture. Anyone can use this image freely if they link back to <a href="http://www.WATTAgNet.com" rel="nofollow">www.WATTAgNet.com</a>.


+--------+
|  TAGS  |
+--------+
cow cattle farm agriculture 