+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : David~
Photo URL    : https://www.flickr.com/photos/58026969@N05/5355957650/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 17 17:07:45 GMT+0200 2008
Upload Date  : Sat Jan 15 01:28:12 GMT+0100 2011
Views        : 868
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cows


+---------------+
|  DESCRIPTION  |
+---------------+
Often, cows jammed our way.


+--------+
|  TAGS  |
+--------+
venezuela hatopiñero national park savannah cows 