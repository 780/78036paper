+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tausend und eins, fotografie
Photo URL    : https://www.flickr.com/photos/74568665@N03/8958848591/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 18 17:25:43 GMT+0200 2012
Upload Date  : Wed Jun 5 16:48:48 GMT+0200 2013
Views        : 5,268
Comments     : 2


+---------+
|  TITLE  |
+---------+
Bulls Eye


+---------------+
|  DESCRIPTION  |
+---------------+
Bull standing near fence and looking into camera


+--------+
|  TAGS  |
+--------+
Rhön Bull Bulle Himmel Wiese Kuh Kühe Grün Braun weiss Nasenring Wolken Natur Milch Bauer farming 