+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : davedehetre
Photo URL    : https://www.flickr.com/photos/davedehetre/4613812159/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 25 12:21:08 GMT+0100 2010
Upload Date  : Mon May 17 06:17:55 GMT+0200 2010
Views        : 463
Comments     : 1


+---------+
|  TITLE  |
+---------+
cow down


+---------------+
|  DESCRIPTION  |
+---------------+
it's been gloomy here lately, so I cleaned out the virtual shoe box a bit.  uploading a few older shots that didn't get posted before.


+--------+
|  TAGS  |
+--------+
cows cow stare hill grass 