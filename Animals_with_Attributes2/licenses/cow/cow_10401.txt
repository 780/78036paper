+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Björn S...
Photo URL    : https://www.flickr.com/photos/40948266@N04/14128110910/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 31 14:35:11 GMT+0200 2014
Upload Date  : Sat May 31 22:47:41 GMT+0200 2014
Geotag Info  : Latitude:46.496713, Longitude:7.011158
Views        : 251
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
cow Kuh vache vacca 