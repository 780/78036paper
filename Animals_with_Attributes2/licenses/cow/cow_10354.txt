+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : windy_
Photo URL    : https://www.flickr.com/photos/acf_windy/5190760376/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 19 22:13:59 GMT+0100 2010
Upload Date  : Fri Nov 19 23:19:49 GMT+0100 2010
Views        : 236
Comments     : 0


+---------+
|  TITLE  |
+---------+
cow


+---------------+
|  DESCRIPTION  |
+---------------+
Canon 1000fn. A roll of slide film sent to the developers &gt;12 months ago - the processed slides only arrived back yesterday


+--------+
|  TAGS  |
+--------+
(no tags)