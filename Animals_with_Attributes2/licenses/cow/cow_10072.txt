+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SusanneK
Photo URL    : https://www.flickr.com/photos/susannek/19372608114/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 12 18:02:45 GMT+0200 2015
Upload Date  : Sat Jul 25 14:37:05 GMT+0200 2015
Views        : 74
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ausflug auf die Kechtalm bei Lofer


+---------------+
|  DESCRIPTION  |
+---------------+
Blick auf die Loferer Steinberge


+--------+
|  TAGS  |
+--------+
"Lofer Berge Mountains Alpen Alps Salzburg Pinzgau Alm Österreich Austria Autriche" Kuh Cow Buntvieh 