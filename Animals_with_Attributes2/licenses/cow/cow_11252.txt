+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jonas Birmé
Photo URL    : https://www.flickr.com/photos/birme/14498311362/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 20 14:21:29 GMT+0200 2014
Upload Date  : Tue Jun 24 21:26:53 GMT+0200 2014
Views        : 161
Comments     : 0


+---------+
|  TITLE  |
+---------+
The Cows!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
cows meadow countryside 