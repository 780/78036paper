+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Deus Figendi
Photo URL    : https://www.flickr.com/photos/25129529@N06/9776273774/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 2 12:22:15 GMT+0200 2013
Upload Date  : Mon Sep 16 20:52:25 GMT+0200 2013
Views        : 210
Comments     : 0


+---------+
|  TITLE  |
+---------+
PIC02230


+---------------+
|  DESCRIPTION  |
+---------------+
PIC02230.JPG


+--------+
|  TAGS  |
+--------+
Miltenberg Kuh Kühe Weide 