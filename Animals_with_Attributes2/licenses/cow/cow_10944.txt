+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Blogging Dagger
Photo URL    : https://www.flickr.com/photos/bloggingdagger/20276623913/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 5 10:30:27 GMT+0200 2015
Upload Date  : Wed Aug 26 14:43:52 GMT+0200 2015
Geotag Info  : Latitude:47.394158, Longitude:11.528571
Views        : 86
Comments     : 0


+---------+
|  TITLE  |
+---------+
Karwendel 2015 105


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Alm Almgaudi Berg Berge Ereignis Europa Karwendel Kuh Kühe Natur Ort Reise Tier Tiere Tirol Wandern animal cow cows europe event location mountain mountains nature oben travel tyrol up wanderlust Österreich 