+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : maltehempel_de
Photo URL    : https://www.flickr.com/photos/maltehempel_de/18385983512/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 5 16:43:15 GMT+0200 2013
Upload Date  : Tue Jun 2 19:36:00 GMT+0200 2015
Views        : 88
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kuh


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Tier Tiere Kuh Kühe 