+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lets Go Out Bournemouth and Poole
Photo URL    : https://www.flickr.com/photos/letsgoout-bournemouthandpoole/7195450232/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 17 17:28:44 GMT+0200 2012
Upload Date  : Mon May 14 13:47:33 GMT+0200 2012
Geotag Info  : Latitude:50.599855, Longitude:-2.031226
Views        : 1,201
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cows Grazing in the Purbecks


+---------------+
|  DESCRIPTION  |
+---------------+
Field near Worth Matravers, April 2012.


+--------+
|  TAGS  |
+--------+
dorset cows purbecks "worth matravers" 