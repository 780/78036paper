+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mdalmuld
Photo URL    : https://www.flickr.com/photos/mdalmuld/7180902804/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 12 13:26:14 GMT+0200 2012
Upload Date  : Sat May 12 11:33:44 GMT+0200 2012
Views        : 18,681
Comments     : 4


+---------+
|  TITLE  |
+---------+
Until the Cows Come Home


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animal cow australia "barrington tops" 