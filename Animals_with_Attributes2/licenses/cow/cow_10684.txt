+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Carles Tomás Martí
Photo URL    : https://www.flickr.com/photos/carles_tomas/3948353691/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 7 15:32:36 GMT+0200 2009
Upload Date  : Thu Sep 24 00:45:43 GMT+0200 2009
Geotag Info  : Latitude:42.828899, Longitude:0.181618
Views        : 132
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cows!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Pyrinées mountain lake cow 