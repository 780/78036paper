+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neillwphoto
Photo URL    : https://www.flickr.com/photos/neillwphoto/8728902040/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 31 14:03:20 GMT+0200 2012
Upload Date  : Sat May 11 16:00:28 GMT+0200 2013
Views        : 331
Comments     : 0


+---------+
|  TITLE  |
+---------+
Highland Cow


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Highland cow cattle 