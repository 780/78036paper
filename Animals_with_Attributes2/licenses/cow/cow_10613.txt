+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ImNotQuiteJack
Photo URL    : https://www.flickr.com/photos/imnotquitejack/5575004707/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 23 16:56:45 GMT+0100 2011
Upload Date  : Thu Mar 31 01:08:42 GMT+0200 2011
Geotag Info  : Latitude:36.309868, Longitude:-121.886272
Views        : 134
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cows


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
California adventure roadtrip cows 