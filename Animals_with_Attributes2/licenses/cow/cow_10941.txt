+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lilli2de
Photo URL    : https://www.flickr.com/photos/seven_of9/4843494549/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 17 10:51:14 GMT+0200 2010
Upload Date  : Fri Jul 30 18:18:46 GMT+0200 2010
Views        : 832
Comments     : 10


+---------+
|  TITLE  |
+---------+
Kakao-Kuh


+---------------+
|  DESCRIPTION  |
+---------------+
Schönes Wochenende! Happy weekend!


+--------+
|  TAGS  |
+--------+
2010 Juli july Germany "Land Brandenburg" Niederlausitz Finsterwalde "Wiese mit Kühen" "meadow with cows" Milchkühe "eine Kakaokuh" 