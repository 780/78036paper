+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : orangeaurochs
Photo URL    : https://www.flickr.com/photos/orangeaurochs/9486349067/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 10 10:57:56 GMT+0200 2013
Upload Date  : Sun Aug 11 20:34:08 GMT+0200 2013
Views        : 19,367
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow, Gibside, Tyne and Wear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
cows 