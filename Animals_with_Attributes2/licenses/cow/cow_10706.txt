+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cwasteson
Photo URL    : https://www.flickr.com/photos/wastes/4588709392/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 5 09:01:42 GMT+0200 2010
Upload Date  : Sat May 8 10:13:40 GMT+0200 2010
Views        : 1,019
Comments     : 0


+---------+
|  TITLE  |
+---------+
Curious II


+---------------+
|  DESCRIPTION  |
+---------------+
Simmentalkor och -kalvar på Bollerups Naturbruksinstitut.


+--------+
|  TAGS  |
+--------+
Bollerup simmental cow calf 