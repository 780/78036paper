+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : paisleyorguk
Photo URL    : https://www.flickr.com/photos/paisleyorguk/2561380715/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 8 17:22:57 GMT+0200 2008
Upload Date  : Sun Jun 8 21:28:16 GMT+0200 2008
Geotag Info  : Latitude:55.687262, Longitude:-4.802913
Views        : 528
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cows


+---------------+
|  DESCRIPTION  |
+---------------+
Windfarm behind West Kilbride taken for my friends website <a href="http://www.enterprisingenergy.com" rel="nofollow">www.enterprisingenergy.com</a> on behalf of <a href="http://www.paisley.org.uk" rel="nofollow">www.paisley.org.uk</a>


+--------+
|  TAGS  |
+--------+
winfarm coutryside renewable energy trees pond turbine "nature windfarm" windfarm 