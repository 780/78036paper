+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : faungg's photos
Photo URL    : https://www.flickr.com/photos/44534236@N00/15989989839/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 22 11:37:03 GMT+0100 2014
Upload Date  : Fri Jan 2 18:18:00 GMT+0100 2015
Geotag Info  : Latitude:30.274115, Longitude:-98.415462
Views        : 692
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
us usa tx texas nature animal fence tree "lyndon b. johnson national historical park" rural ranch travel 美国 德克萨斯州 旅游 乡村 风光 田园 