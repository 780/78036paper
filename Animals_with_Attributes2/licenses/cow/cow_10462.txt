+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Leszek.Leszczynski
Photo URL    : https://www.flickr.com/photos/leszekleszczynski/9325662885/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 17 15:25:02 GMT+0200 2013
Upload Date  : Sat Jul 20 16:30:58 GMT+0200 2013
Views        : 40,067
Comments     : 16


+---------+
|  TITLE  |
+---------+
Cows at ease - Explored :)


+---------------+
|  DESCRIPTION  |
+---------------+
Cows are not a rare view on the trekking paths in the Trento Dolomites. We had to make our way through a herd of these lovely animals.

This photo has been explored - thanks everybody!


+--------+
|  TAGS  |
+--------+
cow dolomites trento trekking 