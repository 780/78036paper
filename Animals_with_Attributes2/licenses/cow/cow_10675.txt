+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : artisrams
Photo URL    : https://www.flickr.com/photos/artisrams/15458567569/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 27 23:23:40 GMT+0100 2014
Upload Date  : Mon Oct 27 22:23:55 GMT+0100 2014
Views        : 612
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cows, Latvia, 2014


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
cows green grass nature 