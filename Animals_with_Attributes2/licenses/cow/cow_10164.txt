+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Alex Graves
Photo URL    : https://www.flickr.com/photos/87244355@N00/259244308/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 12 19:03:38 GMT+0200 2006
Upload Date  : Tue Oct 3 02:46:26 GMT+0200 2006
Views        : 323
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow face


+---------------+
|  DESCRIPTION  |
+---------------+
Mürren, Switzerland


+--------+
|  TAGS  |
+--------+
murren switzerland 