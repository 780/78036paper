+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Niclas Rhein
Photo URL    : https://www.flickr.com/photos/niclasrhein/20185778551/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 15 14:07:16 GMT+0200 2015
Upload Date  : Fri Jul 31 23:59:03 GMT+0200 2015
Views        : 450
Comments     : 3


+---------+
|  TITLE  |
+---------+
Fresh Water


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Animal Landscape Lake See Cow Kuh Sky Himmel Mountains Berge Landschaft Stones Steine Rock Fels Wald Trees Forest Bäume Green Grün Blue Blau Pragser Wildsee Clouds Wolken Tier Oil Öl Gemälde Painting Wood Holz Stick Stock Wasser Water Italy Italien Outdoor 