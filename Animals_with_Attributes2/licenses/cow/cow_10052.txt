+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : spline_splinson
Photo URL    : https://www.flickr.com/photos/splinson/21887935669/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 10 11:59:22 GMT+0200 2015
Upload Date  : Sat Oct 10 19:33:41 GMT+0200 2015
Views        : 50
Comments     : 0


+---------+
|  TITLE  |
+---------+
Alpabtrieb Seewis


+---------------+
|  DESCRIPTION  |
+---------------+
OLYMPUS DIGITAL CAMERA


+--------+
|  TAGS  |
+--------+
Switzerland alpabtrieb alpspektakel cow graubünden kuh schweiz seewis suisse "swiss alps" "swiss cow" "Seewis im Prättigau" CH 