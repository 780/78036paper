+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : yamada*
Photo URL    : https://www.flickr.com/photos/yamada/54057852/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 10 15:35:57 GMT+0200 2005
Upload Date  : Wed Oct 19 18:35:01 GMT+0200 2005
Views        : 164
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow


+---------------+
|  DESCRIPTION  |
+---------------+
こどもの国 (神奈川県横浜市青葉区)

AF Zoom 17-35mm F3.5G


+--------+
|  TAGS  |
+--------+
kodomonokuni cow 