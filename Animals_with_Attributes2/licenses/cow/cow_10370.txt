+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : StevenW.
Photo URL    : https://www.flickr.com/photos/helloeveryone123/5987749547/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 5 14:52:52 GMT+0200 2011
Upload Date  : Fri Jul 29 19:33:18 GMT+0200 2011
Views        : 769
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow


+---------------+
|  DESCRIPTION  |
+---------------+
Yet another cow.


+--------+
|  TAGS  |
+--------+
rural indiana farm black cow animal moo 