+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Karen V Bryan
Photo URL    : https://www.flickr.com/photos/europealacarte/5776455275/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 30 15:56:13 GMT+0200 2011
Upload Date  : Mon May 30 18:27:30 GMT+0200 2011
Views        : 1,263
Comments     : 0


+---------+
|  TITLE  |
+---------+
Friendly Cows of Tweedmouth


+---------------+
|  DESCRIPTION  |
+---------------+
There's a very curious, friendly herd of cows in a field beside the path along the River Tweed from Tweedmouth. When we walk past they amble over to see us.


+--------+
|  TAGS  |
+--------+
"Tweedmouth cows" cows 