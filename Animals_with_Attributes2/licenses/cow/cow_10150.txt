+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jannis_V
Photo URL    : https://www.flickr.com/photos/52336371@N07/26386494930/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 7 20:17:34 GMT+0200 2015
Upload Date  : Tue Apr 26 18:00:39 GMT+0200 2016
Views        : 111
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow at Sunset


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Cow Kuh Cattle Sunset Normande Pasture 