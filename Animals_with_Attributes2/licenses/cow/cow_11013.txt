+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Last Hero
Photo URL    : https://www.flickr.com/photos/uwe_schubert/4003542411/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 10 01:45:36 GMT+0200 2009
Upload Date  : Mon Oct 12 09:21:06 GMT+0200 2009
Views        : 178
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kurztrip nach Österreich


+---------------+
|  DESCRIPTION  |
+---------------+
Andere wollten sehen wer im Auto sitzt.


+--------+
|  TAGS  |
+--------+
Rattenberg tirol österreich Burg oktober 2009 s5800 kurztrip wochenende weekend Kramsau Regen münster Kuh Rindvieh 