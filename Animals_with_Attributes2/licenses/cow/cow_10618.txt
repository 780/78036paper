+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : William Hook
Photo URL    : https://www.flickr.com/photos/williamhook/5926369919/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 10 20:32:40 GMT+0200 2011
Upload Date  : Mon Jul 11 18:27:35 GMT+0200 2011
Views        : 2,439
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cows


+---------------+
|  DESCRIPTION  |
+---------------+
Moo!


+--------+
|  TAGS  |
+--------+
Wickers World Hot Air Balloon Ballooning Staffordshire Shugborough Hall Rugeley Aerial Cows 