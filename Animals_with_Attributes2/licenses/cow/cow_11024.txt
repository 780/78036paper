+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stephanemartin
Photo URL    : https://www.flickr.com/photos/stephanemartin/3889510505/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 11 16:37:03 GMT+0200 2009
Upload Date  : Sat Sep 5 18:40:41 GMT+0200 2009
Geotag Info  : Latitude:49.754686, Longitude:0.357508
Views        : 677
Comments     : 0


+---------+
|  TITLE  |
+---------+
Les vaches et la mer


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
normandie cow fécamp 