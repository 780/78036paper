+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : M. Bertulat
Photo URL    : https://www.flickr.com/photos/berti66/25768197194/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 22 10:53:20 GMT+0200 2015
Upload Date  : Mon Apr 11 19:24:11 GMT+0200 2016
Geotag Info  : Latitude:47.702563, Longitude:15.947620
Views        : 23
Comments     : 1


+---------+
|  TITLE  |
+---------+
Rind auf der Weide


+---------------+
|  DESCRIPTION  |
+---------------+
Oh, die Früchte hängen aber hoch


+--------+
|  TAGS  |
+--------+
alpen österreich austria gebirge prigglitz landschaft alps mountains landscape kuh rind weide cow "out at feed" alp "mountain pasture" 