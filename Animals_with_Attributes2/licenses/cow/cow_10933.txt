+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bethdixoncsi
Photo URL    : https://www.flickr.com/photos/elizadee/10332802393/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 16 17:49:13 GMT+0200 2013
Upload Date  : Thu Oct 17 20:45:21 GMT+0200 2013
Views        : 321
Comments     : 0


+---------+
|  TITLE  |
+---------+
Jersey Cow


+---------------+
|  DESCRIPTION  |
+---------------+
Curious Jersey cow.


+--------+
|  TAGS  |
+--------+
Jersey cow 