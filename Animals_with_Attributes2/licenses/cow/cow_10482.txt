+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : spikevicious
Photo URL    : https://www.flickr.com/photos/dkit/5096870047/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 26 15:28:13 GMT+0200 2010
Upload Date  : Tue Oct 19 20:09:48 GMT+0200 2010
Geotag Info  : Latitude:43.980525, Longitude:-73.311874
Views        : 1,140
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cows In Vermont


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Cows Dairy Cow Bridposrt Vermont 