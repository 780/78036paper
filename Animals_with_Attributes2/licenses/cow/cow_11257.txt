+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Akuppa
Photo URL    : https://www.flickr.com/photos/90664717@N00/250051412/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 12 17:48:05 GMT+0200 2006
Upload Date  : Sat Sep 23 00:47:55 GMT+0200 2006
Views        : 120
Comments     : 1


+---------+
|  TITLE  |
+---------+
Three Cows


+---------------+
|  DESCRIPTION  |
+---------------+
Somewhere in Northumberland.


+--------+
|  TAGS  |
+--------+
cow cattle northumberland 