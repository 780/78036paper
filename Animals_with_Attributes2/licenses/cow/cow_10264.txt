+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric F Savage
Photo URL    : https://www.flickr.com/photos/efsavage/1188523985/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 19 03:07:09 GMT+0200 2007
Upload Date  : Tue Aug 21 04:53:10 GMT+0200 2007
Geotag Info  : Latitude:42.063949, Longitude:-71.583335
Views        : 1,814
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cow


+---------------+
|  DESCRIPTION  |
+---------------+
Not sure what kind of cow this is.


+--------+
|  TAGS  |
+--------+
"southwick's zoo" zoo cow 