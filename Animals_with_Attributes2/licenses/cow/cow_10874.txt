+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : indi.ca
Photo URL    : https://www.flickr.com/photos/indi/4398623519/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Feb 24 11:48:35 GMT+0100 2010
Upload Date  : Mon Mar 1 21:16:20 GMT+0100 2010
Views        : 801
Comments     : 0


+---------+
|  TITLE  |
+---------+
Holy Cow


+---------------+
|  DESCRIPTION  |
+---------------+
In the morning the cows come to peoples doors and people seem to feed them and anoint their foreheads. There's a painted kolam in front of most stoops


+--------+
|  TAGS  |
+--------+
nasik brahmagirihill brahmagiri kolam cow 