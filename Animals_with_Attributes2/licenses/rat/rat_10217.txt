+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : at8eqeq3
Photo URL    : https://www.flickr.com/photos/at8eqeq3/1386920577/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 31 11:44:35 GMT+0200 2007
Upload Date  : Sat Sep 15 19:43:43 GMT+0200 2007
Geotag Info  : Latitude:61.310185, Longitude:50.137310
Views        : 155
Comments     : 0


+---------+
|  TITLE  |
+---------+
P7311298


+---------------+
|  DESCRIPTION  |
+---------------+
Businka the Rat @ Elbaza.Komi Republic


+--------+
|  TAGS  |
+--------+
rat rodent animal 