+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jean-Jacques Boujot
Photo URL    : https://www.flickr.com/photos/jean-jacquesboujot/14674711479/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 8 20:38:41 GMT+0200 2014
Upload Date  : Fri Aug 8 20:40:15 GMT+0200 2014
Geotag Info  : Latitude:47.302544, Longitude:-3.198469
Views        : 7,556
Comments     : 29


+---------+
|  TITLE  |
+---------+
Rat surmulot / Brown Rat


+---------------+
|  DESCRIPTION  |
+---------------+
Rattus norvegicus (Belle-Île -en-mer, Morbihan - France)


+--------+
|  TAGS  |
+--------+
rattus rat des belle île bretagne morbihan france nikon D7100 nikkor 200-400mm 200-400 Surmulot Brown 