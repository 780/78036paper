+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jack berry
Photo URL    : https://www.flickr.com/photos/jackberry/3657068619/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 16 16:35:55 GMT+0200 2009
Upload Date  : Wed Jun 24 20:17:04 GMT+0200 2009
Views        : 256
Comments     : 0


+---------+
|  TITLE  |
+---------+
ratty


+---------------+
|  DESCRIPTION  |
+---------------+
pet rat


+--------+
|  TAGS  |
+--------+
rat pet rodent 