+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stark23x
Photo URL    : https://www.flickr.com/photos/stark23x/56617847/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 4 06:10:37 GMT+0100 2004
Upload Date  : Thu Oct 27 18:41:20 GMT+0200 2005
Views        : 488
Comments     : 0


+---------+
|  TITLE  |
+---------+
The typing rat


+---------------+
|  DESCRIPTION  |
+---------------+
The Mins liked to type.  She once posted to my Livejournal account.  It read, and I quote:

kf23dddddddddggtnnnnnnnnnnnnnnnnnnnnnnn/g;;;;;;;;;;;;;;;;;;;;;;;,,IsdU?

, vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa|\\\ASSSSS
SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSYTG-UUUUU
UUUUGGGUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
UMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNN=[[[[[[[[[[[[[[[[[[[[[[[[[[[[E
444444444444444444444444444444444444444444444444444442

(line breaks added by me.)

If you look carefully, it kind of says &quot;assume&quot; in there.  She was a REALLY smart rat.  :)


+--------+
|  TAGS  |
+--------+
pet rat rats rodent minnie 