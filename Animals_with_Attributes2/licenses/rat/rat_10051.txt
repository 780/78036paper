+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mattymatt
Photo URL    : https://www.flickr.com/photos/mattymatt/3536910289/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 16 18:16:27 GMT+0200 2009
Upload Date  : Sun May 17 03:41:02 GMT+0200 2009
Geotag Info  : Latitude:37.775166, Longitude:-122.454500
Views        : 503
Comments     : 2


+---------+
|  TITLE  |
+---------+
Bathroom rats


+---------------+
|  DESCRIPTION  |
+---------------+
Uh oh, they're not happy. Christopher is puffing up and leaning down so Robin can't bite his nose. And Robin's baring his teeth and trying to puff up, despite lacking the bulk to really pull it off. They're still working out which one is in charge.


+--------+
|  TAGS  |
+--------+
rat conflict rats 