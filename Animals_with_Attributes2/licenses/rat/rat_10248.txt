+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nick Savchenko
Photo URL    : https://www.flickr.com/photos/nsavch/6495446227/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 10 23:09:34 GMT+0100 2011
Upload Date  : Mon Dec 12 00:00:16 GMT+0100 2011
Views        : 1,759
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rat


+---------------+
|  DESCRIPTION  |
+---------------+
Rat eating cheese


+--------+
|  TAGS  |
+--------+
darktable rat animal funny 