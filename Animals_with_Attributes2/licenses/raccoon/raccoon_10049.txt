+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stevehdc
Photo URL    : https://www.flickr.com/photos/sherseydc/561867045/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 17 15:23:43 GMT+0200 2007
Upload Date  : Sun Jun 17 23:09:02 GMT+0200 2007
Views        : 702
Comments     : 10


+---------+
|  TITLE  |
+---------+
Busted!!


+---------------+
|  DESCRIPTION  |
+---------------+
Caught this little guy coming out of a trash can.  All I saw at first were its two back feet (paws?) holding on to the edge of the can when it was face first inside.  Didn't seem to find any snacks inside.  

Not a technically fantastic shot, just a fun capture.  Cheers!


+--------+
|  TAGS  |
+--------+
raccoon sherseydc 