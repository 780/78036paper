+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : JUJUlianar
Photo URL    : https://www.flickr.com/photos/jujulianar/18808960394/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 20 12:00:29 GMT+0200 2015
Upload Date  : Sun Jul 5 12:19:51 GMT+0200 2015
Views        : 323
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
Taken in Malaysia, Lost World Of Tambun


+--------+
|  TAGS  |
+--------+
raccoon animals 