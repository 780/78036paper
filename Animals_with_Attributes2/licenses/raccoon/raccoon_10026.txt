+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Numinosity (Gary J Wood)
Photo URL    : https://www.flickr.com/photos/garyjwood/158997441/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 2 18:43:03 GMT+0200 2006
Upload Date  : Sat Jun 3 02:36:45 GMT+0200 2006
Views        : 212
Comments     : 0


+---------+
|  TITLE  |
+---------+
Baby Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
toronto ontario canada animals raccoons blogged 