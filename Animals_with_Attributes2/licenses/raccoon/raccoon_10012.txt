+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Canadian Starhawk
Photo URL    : https://www.flickr.com/photos/jameskidd/4660716092/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 27 16:57:51 GMT+0200 2010
Upload Date  : Tue Jun 1 19:31:38 GMT+0200 2010
Views        : 132
Comments     : 0


+---------+
|  TITLE  |
+---------+
100527-1620


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
raccoon "toronto zoo" animals caged 