+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : drewgstephens
Photo URL    : https://www.flickr.com/photos/dinomite/3105346847/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 13 01:11:35 GMT+0100 2008
Upload Date  : Sun Dec 14 01:36:25 GMT+0100 2008
Views        : 570
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
The raccoon saw us through the window and tried to stay very still thinking we wouldn't see him.


+--------+
|  TAGS  |
+--------+
Canon 40d 28135mm raccoon animal 