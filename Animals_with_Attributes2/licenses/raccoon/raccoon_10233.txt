+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nexuna
Photo URL    : https://www.flickr.com/photos/nexuna/2877856213/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 22 10:13:32 GMT+0200 2008
Upload Date  : Mon Sep 22 10:13:32 GMT+0200 2008
Geotag Info  : Latitude:51.423376, Longitude:6.801867
Views        : 120
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zoo Duisburg


+---------------+
|  DESCRIPTION  |
+---------------+
Die Henselers besuchen den Duisburger Zoo

Die Henselers besuchen den Duisburger Zoo


+--------+
|  TAGS  |
+--------+
Zoo-Duisburg Tiere Kinder "Henseler Zoo-Duisburg" Waschbär 