+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ducklover Bonnie
Photo URL    : https://www.flickr.com/photos/bonniesducks/4602862392/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 12 19:42:55 GMT+0200 2010
Upload Date  : Thu May 13 13:15:08 GMT+0200 2010
Views        : 504
Comments     : 17


+---------+
|  TITLE  |
+---------+
Raccoon in da hood


+---------------+
|  DESCRIPTION  |
+---------------+
In the lane behind my condo. Perhaps trying to get into that blue thing (a port-a-potty)!


+--------+
|  TAGS  |
+--------+
IMG6658 raccoon "and a port-a-potty" "last night" "skulking around" 