+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Arctic Wolf Pictures
Photo URL    : https://www.flickr.com/photos/arcticwoof/20477675559/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 17 22:35:38 GMT+0200 2015
Upload Date  : Mon Aug 17 22:55:49 GMT+0200 2015
Views        : 52
Comments     : 0


+---------+
|  TITLE  |
+---------+
Coon of the hill


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
raccoon 