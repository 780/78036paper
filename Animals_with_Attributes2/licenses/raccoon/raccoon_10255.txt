+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ArtBrom
Photo URL    : https://www.flickr.com/photos/art-sarah/2411936016/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 12 15:55:01 GMT+0200 2008
Upload Date  : Mon Apr 14 01:03:22 GMT+0200 2008
Views        : 54
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
Yep, I can never resist raccoon pictures.


+--------+
|  TAGS  |
+--------+
"zoo northwest trek" Northwest Trek Nikon D300 