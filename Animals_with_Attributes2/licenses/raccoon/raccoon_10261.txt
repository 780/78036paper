+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jmwests
Photo URL    : https://www.flickr.com/photos/jassen/2800369957/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 26 16:20:45 GMT+0200 2008
Upload Date  : Tue Aug 26 22:56:03 GMT+0200 2008
Views        : 1,855
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
An unexpected visitor.


+--------+
|  TAGS  |
+--------+
Raccoon 