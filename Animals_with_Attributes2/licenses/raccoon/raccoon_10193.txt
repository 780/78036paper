+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eichental
Photo URL    : https://www.flickr.com/photos/photo64/5529891457/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 12 12:08:10 GMT+0100 2011
Upload Date  : Tue Mar 15 22:49:52 GMT+0100 2011
Views        : 84
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zoom Erlebniswelt (Gelsenkirchen)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Erlebniswelt Gelsenkirchen Ruhrgebiet Tiere Waschbär Zoom 