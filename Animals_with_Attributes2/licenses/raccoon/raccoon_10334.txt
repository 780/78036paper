+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : LandBetweenTheLakesKYTN
Photo URL    : https://www.flickr.com/photos/lblkytn/14928062815/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 5 18:32:59 GMT+0200 2013
Upload Date  : Fri Aug 15 19:55:46 GMT+0200 2014
Views        : 136
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
You never know what kind of critter you may encounter when visiting Land Between The Lakes. This critter was spotted near the Nature Station. Photo by Kevin VanGorden


+--------+
|  TAGS  |
+--------+
LandBetweenTheLakesKYTN "Land Between The Lakes" "US Forest Service" nature "environmental education" "Kentucky Lake" "Lake Barkley" history camping birding biking hiking hunting Kentucky Tennessee "public lands" "scenic driving" "wildlife watching" 