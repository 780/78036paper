+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Carly & Art
Photo URL    : https://www.flickr.com/photos/wiredwitch/2353060653/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 22 13:09:19 GMT+0100 2008
Upload Date  : Sun Mar 23 05:19:56 GMT+0100 2008
Geotag Info  : Latitude:39.020884, Longitude:-77.598484
Views        : 125
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Banshee Reeks" "nature preserve" "loudoun county" virginia wildlife hiking piedmont 