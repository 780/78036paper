+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jill Clardy
Photo URL    : https://www.flickr.com/photos/jillclardy/3017338240/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 13 12:50:54 GMT+0200 2008
Upload Date  : Sun Nov 9 23:31:41 GMT+0100 2008
Views        : 479
Comments     : 1


+---------+
|  TITLE  |
+---------+
Up a Tree in Central Park, New York


+---------------+
|  DESCRIPTION  |
+---------------+
poor critter seem to be frozen in terror; couldn't figure out what to do with all the tourists gawking and snapping pictures. (If he were truly a native, I suppose he would have put out a hat and accepted contributions.)


+--------+
|  TAGS  |
+--------+
new york city raccoon central park 