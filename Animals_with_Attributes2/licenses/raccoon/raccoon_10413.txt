+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MyAngelG
Photo URL    : https://www.flickr.com/photos/aasg/468864931/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 22 16:03:58 GMT+0200 2007
Upload Date  : Sun Apr 22 22:31:15 GMT+0200 2007
Views        : 1,695
Comments     : 5


+---------+
|  TITLE  |
+---------+
Ricky Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
What a lazy Sunday afternoon for this fella!


+--------+
|  TAGS  |
+--------+
"World of Birds" Animals Nature Serene Raccoon Nap SOE 