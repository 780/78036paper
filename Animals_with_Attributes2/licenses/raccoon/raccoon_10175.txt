+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MSVG
Photo URL    : https://www.flickr.com/photos/msvg/4572036499/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 2 12:12:01 GMT+0200 2010
Upload Date  : Sun May 2 23:48:01 GMT+0200 2010
Geotag Info  : Latitude:43.727657, Longitude:-79.364923
Views        : 439
Comments     : 6


+---------+
|  TITLE  |
+---------+
Raccoon - The Canadian Pest


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Raccoon Toronto Ontario Canada 