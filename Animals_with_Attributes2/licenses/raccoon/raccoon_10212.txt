+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Madeleine_H
Photo URL    : https://www.flickr.com/photos/madeleine_h/7634010130/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 22 14:45:35 GMT+0200 2012
Upload Date  : Tue Jul 24 03:39:50 GMT+0200 2012
Geotag Info  : Latitude:49.296233, Longitude:-123.145666
Views        : 248
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon in Stanley Park


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
DS5000 raccoon Canada RTW BC "British Columbia" Vancouver "Stanley Park" 