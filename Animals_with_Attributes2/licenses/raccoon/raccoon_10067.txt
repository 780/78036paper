+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Paxson Woelber
Photo URL    : https://www.flickr.com/photos/paxson_woelber/9269401115/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 12 16:17:23 GMT+0200 2013
Upload Date  : Fri Jul 12 21:41:21 GMT+0200 2013
Views        : 3,070
Comments     : 0


+---------+
|  TITLE  |
+---------+
Curious Raccoon in the Everglades


+---------------+
|  DESCRIPTION  |
+---------------+
Photo by Paxson Woelber
<a href="http://www.paxsonwoelber.com" rel="nofollow">www.paxsonwoelber.com</a>


+--------+
|  TAGS  |
+--------+
raccoon Everglades Florida Paxson Woelber 