+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : synspectrum
Photo URL    : https://www.flickr.com/photos/epector/17334686753/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 21 19:25:57 GMT+0200 2015
Upload Date  : Fri May 22 06:10:48 GMT+0200 2015
Views        : 24
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
Naperville Riverwalk, DuPage Co


+--------+
|  TAGS  |
+--------+
(no tags)