+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Charles & Clint
Photo URL    : https://www.flickr.com/photos/dad_and_clint/179494277/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 30 19:27:58 GMT+0200 2006
Upload Date  : Sun Jul 2 05:58:44 GMT+0200 2006
Views        : 538
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon, Young


+---------------+
|  DESCRIPTION  |
+---------------+
Maybe half grown but appeared to be on his own already.  Was out and about in the early evening and I snapped this shot as he was scaling the cliff and stopped one last time to look back and see what we were doing following him!


+--------+
|  TAGS  |
+--------+
Raccoon mamal TX Texas "Val Verde" "Pecos River" Amistad 