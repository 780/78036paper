+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mobikerbear
Photo URL    : https://www.flickr.com/photos/mobikerbear/14039948924/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 27 12:29:54 GMT+0200 2014
Upload Date  : Sun Apr 27 22:35:55 GMT+0200 2014
Views        : 103
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC00173


+---------------+
|  DESCRIPTION  |
+---------------+
Waschbär - Procyon lotor - Racoon


+--------+
|  TAGS  |
+--------+
2014 Köln "Kölner Zoo" 