+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : dave and rose
Photo URL    : https://www.flickr.com/photos/davrozs/2918161895/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 23 18:51:19 GMT+0200 2008
Upload Date  : Mon Oct 6 17:04:20 GMT+0200 2008
Views        : 1,749
Comments     : 1


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
Oso Flaco Resident


+--------+
|  TAGS  |
+--------+
raccoon 