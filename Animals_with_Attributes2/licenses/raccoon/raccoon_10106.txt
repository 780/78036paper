+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bunnygoth
Photo URL    : https://www.flickr.com/photos/bunnygoth/7937132130/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 30 07:05:44 GMT+0200 2012
Upload Date  : Wed Sep 5 16:30:02 GMT+0200 2012
Views        : 67
Comments     : 0


+---------+
|  TITLE  |
+---------+
Green Cay


+---------------+
|  DESCRIPTION  |
+---------------+
Raccoon, Green Cay Wetlands, Boynton Beach, Florida


+--------+
|  TAGS  |
+--------+
raccoon green cay wetlands boynton beach florida wildlife 