+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cricketsblog
Photo URL    : https://www.flickr.com/photos/cricketsblog/24890278062/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 9 08:40:50 GMT+0100 2016
Upload Date  : Sun Feb 14 00:26:40 GMT+0100 2016
Views        : 41
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
taxonomy:kingdom=Animalia Animalia taxonomy:phylum=Chordata Chordata taxonomy:subphylum=Vertebrata Vertebrata taxonomy:class=Mammalia Mammalia taxonomy:order=Carnivora Carnivora taxonomy:family=Procyonidae Procyonidae taxonomy:genus=Procyon Procyon taxonomy:species=lotor "taxonomy:binomial=Procyon lotor" "Ós rentador" "Procyon lotor" Raccoon Mapachtli a’ka’bak batúi "Northern Raccoon" "Nordamerikanischer Waschbär" "Common Raccoon" "raton laveur" Mapache "Zorra Manglera" Waschbär pesukarhu "taxonomy:common=Ós rentador" taxonomy:common=Raccoon taxonomy:common=Mapachtli taxonomy:common=a’ka’bak taxonomy:common=batúi "taxonomy:common=Northern Raccoon" "taxonomy:common=Nordamerikanischer Waschbär" "taxonomy:common=Common Raccoon" "taxonomy:common=raton laveur" taxonomy:common=Mapache "taxonomy:common=Zorra Manglera" taxonomy:common=Waschbär taxonomy:common=pesukarhu 