+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : highlander411
Photo URL    : https://www.flickr.com/photos/highlander411/8264337740/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 11 08:11:07 GMT+0100 2012
Upload Date  : Tue Dec 11 14:06:27 GMT+0100 2012
Views        : 91
Comments     : 0


+---------+
|  TITLE  |
+---------+
Florida Raccoons


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Florida Raccoon animals wild 