+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tobyotter
Photo URL    : https://www.flickr.com/photos/78428166@N00/7411292098/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 20 20:33:17 GMT+0200 2012
Upload Date  : Thu Jun 21 03:03:41 GMT+0200 2012
Views        : 1,172
Comments     : 4


+---------+
|  TITLE  |
+---------+
Raccoon


+---------------+
|  DESCRIPTION  |
+---------------+
While walking the dogs this evening I heard some chattering.  I looked up just in time to see a couple of raccoon climbing in to a neighbors chimney.  I took a couple of photos and showed them to the home owner - he said he had been hearing noises but thought they were squirrels on the roof - he is calling animal control in the morning.


+--------+
|  TAGS  |
+--------+
raccoon NewportNews Virginia NorthAve 