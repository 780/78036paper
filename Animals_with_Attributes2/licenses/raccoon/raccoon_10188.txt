+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on May 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Pip R. Lagenta
Photo URL    : https://www.flickr.com/photos/pip_r_lagenta/2267815459/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 28 21:23:04 GMT+0200 2007
Upload Date  : Sat Feb 16 07:35:04 GMT+0100 2008
Views        : 367
Comments     : 0


+---------+
|  TITLE  |
+---------+
Raccoon at play


+---------------+
|  DESCRIPTION  |
+---------------+
The raccoon makes itself at home


+--------+
|  TAGS  |
+--------+
raccoon "cat toy" "cross-species colonization" 