+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : WordRidden
Photo URL    : https://www.flickr.com/photos/wordridden/13936295/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 4 18:16:00 GMT+0200 2005
Upload Date  : Sun May 15 09:44:25 GMT+0200 2005
Views        : 1,867
Comments     : 1


+---------+
|  TITLE  |
+---------+
Orca in British Columbia


+---------------+
|  DESCRIPTION  |
+---------------+
An orca pod swam around our boat for a while one evening.


+--------+
|  TAGS  |
+--------+
wildlife whale orca 