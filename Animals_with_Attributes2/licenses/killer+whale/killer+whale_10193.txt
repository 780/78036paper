+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : whistler1984
Photo URL    : https://www.flickr.com/photos/whistler1984/4983852355/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 2 03:19:44 GMT+0200 2010
Upload Date  : Mon Sep 13 00:02:29 GMT+0200 2010
Views        : 406
Comments     : 0


+---------+
|  TITLE  |
+---------+
Orca


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Orca Camp" orcas camp wild wilderness kayaking "johnstone straits" canada nature whales 