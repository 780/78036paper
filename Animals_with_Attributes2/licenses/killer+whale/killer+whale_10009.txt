+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rojer
Photo URL    : https://www.flickr.com/photos/rojer/4891908668/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 19 17:58:04 GMT+0200 2010
Upload Date  : Sat Aug 14 22:22:58 GMT+0200 2010
Geotag Info  : Latitude:32.764646, Longitude:-117.228654
Views        : 132
Comments     : 0


+---------+
|  TITLE  |
+---------+
100_6400


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Shamu Orca "Killer Whale" SeaWorld "San Diego" 