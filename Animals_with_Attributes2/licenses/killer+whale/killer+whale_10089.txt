+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eiolos
Photo URL    : https://www.flickr.com/photos/eiolos/11350763073/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 9 13:52:08 GMT+0200 2013
Upload Date  : Fri Dec 13 13:37:09 GMT+0100 2013
Geotag Info  : Latitude:43.614616, Longitude:7.125191
Views        : 475
Comments     : 0


+---------+
|  TITLE  |
+---------+
Jumping Orchas at Marineland, Antibes, France


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Marineland Orca Orcas Späckhuggare France Provence 