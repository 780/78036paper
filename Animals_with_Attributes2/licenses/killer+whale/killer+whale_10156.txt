+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tammylo
Photo URL    : https://www.flickr.com/photos/tammylo/8027930917/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 22 19:02:09 GMT+0200 2012
Upload Date  : Thu Sep 27 00:11:32 GMT+0200 2012
Geotag Info  : Latitude:32.729530, Longitude:-117.170562
Views        : 1,728
Comments     : 0


+---------+
|  TITLE  |
+---------+
One Ocean (Killer Whale Show) @ Sea World


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"sea world" "san diego" animals "killer whales" shamu orcas 