+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Skakerman
Photo URL    : https://www.flickr.com/photos/sterlic/6546792537/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 19 11:51:25 GMT+0100 2011
Upload Date  : Wed Dec 21 03:35:48 GMT+0100 2011
Views        : 630
Comments     : 0


+---------+
|  TITLE  |
+---------+
Killer Whales


+---------------+
|  DESCRIPTION  |
+---------------+
Another December, another week in Florida. Here's some shots from Seaworld (probably for the 2nd or 3rd time). I realized this year that the lighting at the performances is terrible but that's a true photographer-only complaint as to get decent lighting they'd need to remove the shades.


+--------+
|  TAGS  |
+--------+
Aquatic Florida Jump Orca Orlando Performance Seaworld Shamu "Shamu Stadium" Show "Theme Park" Tricks 