+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Deanna Keahey
Photo URL    : https://www.flickr.com/photos/advwench/257073051/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 14 16:18:14 GMT+0200 2006
Upload Date  : Sun Oct 1 09:19:56 GMT+0200 2006
Views        : 225
Comments     : 0


+---------+
|  TITLE  |
+---------+
Here he is!


+---------------+
|  DESCRIPTION  |
+---------------+
One of the orcas obligingly approached our boat - it's awesome to see them in action!


+--------+
|  TAGS  |
+--------+
"Adventurous Wench" "San Juan Islands" Washington tour trip islands women adventure vacation travel northwest orca whale orcas whales "killer whale" whalewatching 