+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KJGarbutt
Photo URL    : https://www.flickr.com/photos/kjgarbutt/4901608060/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 30 20:34:35 GMT+0200 2010
Upload Date  : Tue Aug 17 17:12:11 GMT+0200 2010
Geotag Info  : Latitude:32.765642, Longitude:-117.227425
Views        : 2,015
Comments     : 0


+---------+
|  TITLE  |
+---------+
Orca Show, Sea World


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
cybershot sony travel travelling usa america aroundtheworld water blue white animal black animals seaworld whale killer orca splash shamu sh California KJGarbutt Traveling "United States of America" "Sony Cybershot" "Kurtis Garbutt" Kurtis Garbutt Photography "KJGarbutt Photography" "Kurtis J Garbutt" 