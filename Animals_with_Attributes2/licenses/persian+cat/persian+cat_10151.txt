+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : clunkygirl
Photo URL    : https://www.flickr.com/photos/clunkygirl/2646303123/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 7 14:40:40 GMT+0200 2008
Upload Date  : Mon Jul 7 20:40:40 GMT+0200 2008
Views        : 1,682
Comments     : 5


+---------+
|  TITLE  |
+---------+
Dr. Benway


+---------------+
|  DESCRIPTION  |
+---------------+
Adopted July 5th 2008


+--------+
|  TAGS  |
+--------+
cat kitty persian odd-eyed "animal adoption" 