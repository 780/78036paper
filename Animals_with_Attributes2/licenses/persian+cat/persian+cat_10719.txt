+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gunman47
Photo URL    : https://www.flickr.com/photos/gunman47/8355385913/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 17 14:06:49 GMT+0100 2012
Upload Date  : Mon Jan 7 03:24:16 GMT+0100 2013
Geotag Info  : Latitude:25.086666, Longitude:121.826000
Views        : 1,499
Comments     : 0


+---------+
|  TITLE  |
+---------+
White Persian Cat (波斯貓)


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at the Houtong Cat Village on the Pingxi Line in Taiwan.


+--------+
|  TAGS  |
+--------+
Houtong Cat Cats Taiwan Pingxi Line White Persian 猴硐 侯硐 貓村 瑞芳 猫村 貓 猫 猫咪 貓咪 台灣 台湾 