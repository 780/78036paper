+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : uberculture
Photo URL    : https://www.flickr.com/photos/uberculture/2648805991/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 3 12:51:59 GMT+0200 2008
Upload Date  : Tue Jul 8 13:44:28 GMT+0200 2008
Views        : 1,709
Comments     : 4


+---------+
|  TITLE  |
+---------+
effed up cat


+---------------+
|  DESCRIPTION  |
+---------------+
This cat lives outside an antique store up north.  We think the shopowner gave this cat a haircut (an incredibly bad one, at that).


+--------+
|  TAGS  |
+--------+
cat orange fur feline persian pet k100d pentax Erskine 