+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Living in Monrovia
Photo URL    : https://www.flickr.com/photos/livinginmonrovia/2709193242/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 27 20:07:05 GMT+0200 2008
Upload Date  : Mon Jul 28 05:07:05 GMT+0200 2008
Views        : 2,097
Comments     : 6


+---------+
|  TITLE  |
+---------+
Mr. Maji on White


+---------------+
|  DESCRIPTION  |
+---------------+
I'm trying to improve my photography skills and somehow managed to get him to sit on a piece of white paper I had tacked up in my gazebo outside. He's such a good and smart kitty!


+--------+
|  TAGS  |
+--------+
orange cat kitty tabby sitting lying down looking 5bestcats persian 