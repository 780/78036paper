+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : blondinrikard
Photo URL    : https://www.flickr.com/photos/blondinrikard/14526483111/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 28 08:52:32 GMT+0200 2014
Upload Date  : Sun Jun 29 01:38:21 GMT+0200 2014
Views        : 396
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cute cat


+---------------+
|  DESCRIPTION  |
+---------------+
Third day in Lahijan

Gabeneh - the old town of Lahijan


+--------+
|  TAGS  |
+--------+
lahijan gilan gilaan "north iran" persia iran cat "street cat" "stray cat" "persian cat" 