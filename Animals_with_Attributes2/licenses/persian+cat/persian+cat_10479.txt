+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nickolas Titkov
Photo URL    : https://www.flickr.com/photos/titkov/15466384178/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 26 13:39:33 GMT+0100 2014
Upload Date  : Tue Oct 28 19:33:50 GMT+0100 2014
Geotag Info  : Latitude:55.828986, Longitude:37.642486
Views        : 381
Comments     : 0


+---------+
|  TITLE  |
+---------+
PER from „White Rose“


+---------------+
|  DESCRIPTION  |
+---------------+
Персидский кот из питомника „White Rose“. Кот. Окрас: белый с оранжевыми глазами (PER w 62). Владелец: Н.И.Насонова. Клуб «Сувенир», Москва


+--------+
|  TAGS  |
+--------+
"Golden Autumn" 2014 "cat show" Moscow cats exposition "Золотая Осень" "выставка кошек" Москва кошки PER Persian Персидская Россия "Russian Federation" 