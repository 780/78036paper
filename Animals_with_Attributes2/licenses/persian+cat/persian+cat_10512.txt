+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : semarr
Photo URL    : https://www.flickr.com/photos/semarr/270172582/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 14 13:26:24 GMT+0200 2006
Upload Date  : Sun Oct 15 16:25:29 GMT+0200 2006
Views        : 749
Comments     : 1


+---------+
|  TITLE  |
+---------+
Persian


+---------------+
|  DESCRIPTION  |
+---------------+
2006 CFA-Iams Cat Championship, Madison Square Garden


+--------+
|  TAGS  |
+--------+
cfa iams "cfa iams cat championship" "madison square garden" manhattan 2006 "cat show" cats persian nyc gothamist 