+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Peter Curbishley
Photo URL    : https://www.flickr.com/photos/peter_curb/3435239381/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 12 14:59:31 GMT+0200 2009
Upload Date  : Sun Apr 12 23:54:01 GMT+0200 2009
Views        : 475
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hector


+---------------+
|  DESCRIPTION  |
+---------------+
Unfortunately, I had to take this in an instant and did not get the lighting quite right.


+--------+
|  TAGS  |
+--------+
cat chat paws pattes whiskers pensive persian 