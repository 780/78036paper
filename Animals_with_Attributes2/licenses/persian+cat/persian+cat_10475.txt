+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nickolas Titkov
Photo URL    : https://www.flickr.com/photos/titkov/4195258664/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 6 15:26:21 GMT+0100 2009
Upload Date  : Fri Dec 18 15:30:24 GMT+0100 2009
Geotag Info  : Latitude:55.791468, Longitude:37.537955
Views        : 572
Comments     : 0


+---------+
|  TITLE  |
+---------+
PER Coco Chanel


+---------------+
|  DESCRIPTION  |
+---------------+
Персидская кошка Coco Chanel, №1018 по каталогу «Гран При Royal Canin-2009». Кошка. Окрас: чёрный мраморный (PER n 22). Дата рождения: 25/06/2009. Владелец: Ж.В.Войнова. Клуб «Фрея», Москва


+--------+
|  TAGS  |
+--------+
кошки cats выставка exposition "cat show" "Гран При Royal Canin-2009" Concord PER Persian Персидская Персидские 