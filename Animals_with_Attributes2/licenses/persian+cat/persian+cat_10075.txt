+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : D.Eickhoff
Photo URL    : https://www.flickr.com/photos/dweickhoff/5393341167/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 3 17:48:04 GMT+0200 2010
Upload Date  : Thu Jan 27 21:34:01 GMT+0100 2011
Views        : 405
Comments     : 0


+---------+
|  TITLE  |
+---------+
Boo-Boo Kitty


+---------------+
|  DESCRIPTION  |
+---------------+
Boo-Boo sitting in our window sill checking the occasional bird, lizard, bug or leaf that comes by--none of which she can get at. It's stimulus for her. Love her Persian profile.


+--------+
|  TAGS  |
+--------+
"Boo-Boo Kitty" Cat Felis Feline Cats Pets Hawaii kitty animal "pet cats" "house cats" 