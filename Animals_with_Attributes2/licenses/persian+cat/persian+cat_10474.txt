+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nickolas Titkov
Photo URL    : https://www.flickr.com/photos/titkov/4380779283/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 20 15:19:29 GMT+0100 2010
Upload Date  : Tue Feb 23 07:24:31 GMT+0100 2010
Geotag Info  : Latitude:55.822128, Longitude:37.389103
Views        : 308
Comments     : 0


+---------+
|  TITLE  |
+---------+
PER Barlandrus Domino


+---------------+
|  DESCRIPTION  |
+---------------+
Персидский кот Barlandrus Domino, №133 по каталогу TICA. Кот. Окрас: чёрный тэбби биколор (PER n 21 03). Дата рождения: 14/11/2007. Владелец: А.-М.Тихонова. Клуб, город не указаны


+--------+
|  TAGS  |
+--------+
кошки cats РосКош RosCosh выставка exposition "cat show" PER Persian Персидские 