+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Giorgio Montersino
Photo URL    : https://www.flickr.com/photos/novecentino/512689257/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 7 09:45:17 GMT+0200 2006
Upload Date  : Fri May 25 00:09:05 GMT+0200 2007
Geotag Info  : Latitude:38.914811, Longitude:45.598754
Views        : 537
Comments     : 0


+---------+
|  TITLE  |
+---------+
baby sphynx


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
iran azerbaijan 2006 travel cat gatto 