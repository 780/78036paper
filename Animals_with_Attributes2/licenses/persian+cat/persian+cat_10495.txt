+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : maticulous
Photo URL    : https://www.flickr.com/photos/maticulous/2401762033/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 10 15:06:50 GMT+0200 2008
Upload Date  : Thu Apr 10 05:06:50 GMT+0200 2008
Geotag Info  : Latitude:-36.870025, Longitude:174.766602
Views        : 726
Comments     : 1


+---------+
|  TITLE  |
+---------+
Persian Cat (R.I.P 1996-2008)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Persian Cat In Chair Lighting HDR Powershot G9 Canon 13 year old flat snub nose domestic RIP rest peace 