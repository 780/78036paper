+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nickolas Titkov
Photo URL    : https://www.flickr.com/photos/titkov/5289273275/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 4 17:23:34 GMT+0100 2010
Upload Date  : Sat Dec 25 08:31:15 GMT+0100 2010
Geotag Info  : Latitude:55.791468, Longitude:37.537955
Views        : 829
Comments     : 0


+---------+
|  TITLE  |
+---------+
Persian color-point • Персидский колор-пойнт


+---------------+
|  DESCRIPTION  |
+---------------+
Персидский колор-пойнт (с красными отметинами), рекламный класс (FIFe). Питомник „Silvery Princ*BY“. Владелец: Н.Кураченко, Гомель, Беларусь

Прошу владельца животного связаться со мной по 207493@gmail.com для уточнения клички и возраста


+--------+
|  TAGS  |
+--------+
кошки cats выставка exposition "cat show" "Гран При Royal Canin-2010" Concord PER Persian Персидские 