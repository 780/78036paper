+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Derek Keats
Photo URL    : https://www.flickr.com/photos/dkeats/16717369024/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 27 18:04:21 GMT+0100 2014
Upload Date  : Sat May 2 08:56:34 GMT+0200 2015
Views        : 207
Comments     : 0


+---------+
|  TITLE  |
+---------+
White rhinoceros or square-lipped rhinoceros, Ceratotherium simum. Note that in some of these photos there are a female with a calf, and a male that seems to have been challenging the calf.


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"endagered animals" "White rhinoceros" nature "Ceratotherium simum" "Square-lipped rhinoceros" "taxonomy:binomial=Ceratotherium simum" wildlife "threatened species" 