+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Derek Keats
Photo URL    : https://www.flickr.com/photos/dkeats/7775058848/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 5 14:29:12 GMT+0200 2012
Upload Date  : Mon Aug 13 19:32:06 GMT+0200 2012
Views        : 282
Comments     : 0


+---------+
|  TITLE  |
+---------+
One more rhino in the bush


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"White rhinoceros" "Square-lipped rhinoceros" "Ceratotherium simum" "taxonomy:binomial=Ceratotherium simum" 