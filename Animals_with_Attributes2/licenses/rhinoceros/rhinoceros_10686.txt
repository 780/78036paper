+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gvgoebel
Photo URL    : https://www.flickr.com/photos/37467370@N08/7611593690/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 20 14:03:51 GMT+0200 2012
Upload Date  : Fri Jul 20 23:06:08 GMT+0200 2012
Views        : 189
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ybrhi_1b


+---------------+
|  DESCRIPTION  |
+---------------+
rhinoceros, Denver Zoo, Colorado / 2006


+--------+
|  TAGS  |
+--------+
animals mammals rhinoceros 