+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : TrishaLyn
Photo URL    : https://www.flickr.com/photos/trishalyn/5127548593/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 30 00:53:51 GMT+0200 2010
Upload Date  : Sat Oct 30 09:53:51 GMT+0200 2010
Views        : 255
Comments     : 0


+---------+
|  TITLE  |
+---------+
SouthCarolina-244


+---------------+
|  DESCRIPTION  |
+---------------+
Tank the Rhino is the only working Rhino in the USA.  He's been featured in Blue Cross/Blue Shield commercials, a national ad campaign for Land Rover, and a Zicam national commercial campaign.


+--------+
|  TAGS  |
+--------+
Inman "South Carolina" "Hollywild Animal Park" animals Tank "white rhinoceros" "Ceratotherium simum" rhinos "white-tailed deer" "Odocoileus virginianus" Wellford "Land Rover" Zicam "Blue Cross/Blue Shield" 