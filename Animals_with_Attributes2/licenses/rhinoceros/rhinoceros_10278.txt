+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kthypryn
Photo URL    : https://www.flickr.com/photos/44603071@N00/1155300043/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 18 00:05:05 GMT+0200 2007
Upload Date  : Sat Aug 18 07:05:05 GMT+0200 2007
Views        : 440
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lowry Park Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
White rhinoceros


+--------+
|  TAGS  |
+--------+
Florida Tampa "Lowry Park Zoo" zoo animals "white rhinoceros" rhino wild_animals 