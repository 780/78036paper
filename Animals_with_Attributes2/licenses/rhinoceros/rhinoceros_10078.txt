+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/18618188553/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 23 10:26:47 GMT+0200 2015
Upload Date  : Sun Jun 28 19:03:40 GMT+0200 2015
Views        : 47
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Greater One- Horned Rhino


+--------+
|  TAGS  |
+--------+
"Chester Zoo" "Greater One- Horned Rhinoceros" 