+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mike feist
Photo URL    : https://www.flickr.com/photos/mikefeist/15691157071/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 30 11:57:26 GMT+0200 2013
Upload Date  : Sun Nov 2 19:35:16 GMT+0100 2014
Views        : 43
Comments     : 0


+---------+
|  TITLE  |
+---------+
Indian Rhinoceros


+---------------+
|  DESCRIPTION  |
+---------------+
<i>Rhinoceros unicornis</i>


+--------+
|  TAGS  |
+--------+
(no tags)