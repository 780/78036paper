+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Adam Jones, Ph.D. - Global Photo Archive
Photo URL    : https://www.flickr.com/photos/adam_jones/13907428086/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 18 01:39:33 GMT+0200 2014
Upload Date  : Sat Apr 19 04:52:33 GMT+0200 2014
Views        : 215
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rhinoceros - Chitwan National Park - Nepal - 02


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)