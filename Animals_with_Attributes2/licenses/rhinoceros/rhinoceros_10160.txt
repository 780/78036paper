+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andrew Callow
Photo URL    : https://www.flickr.com/photos/56380499@N06/14120934174/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 24 10:56:59 GMT+0200 2014
Upload Date  : Tue May 6 10:22:49 GMT+0200 2014
Views        : 56
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester 2014-232


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
28-300 FX "chester 2014" "chester zoo" d600 england europe "full frame" lightroom nikon "perfect photo suite" raw rhinoceros uk 