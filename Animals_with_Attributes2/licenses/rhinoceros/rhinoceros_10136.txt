+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USFWS Headquarters
Photo URL    : https://www.flickr.com/photos/usfwshq/6880954781/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 5 08:22:52 GMT+0200 2002
Upload Date  : Wed Feb 15 15:12:54 GMT+0100 2012
Views        : 685
Comments     : 0


+---------+
|  TITLE  |
+---------+
Black rhino


+---------------+
|  DESCRIPTION  |
+---------------+
Mkhuze, South Africa
Credit: Karl Stromayer/USFWS


+--------+
|  TAGS  |
+--------+
rhinoceros wildlifewithoutborders multinational species 