+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jérémy Couture
Photo URL    : https://www.flickr.com/photos/jeremy-couture/5659173376/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 23 15:31:05 GMT+0200 2011
Upload Date  : Tue Apr 26 22:59:07 GMT+0200 2011
Geotag Info  : Latitude:48.864735, Longitude:1.795535
Views        : 134
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rhinoceros


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Rhinoceros Zoo Thoiry 