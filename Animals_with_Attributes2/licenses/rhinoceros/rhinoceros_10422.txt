+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/20121577054/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 12 13:52:54 GMT+0200 2015
Upload Date  : Fri Aug 21 01:00:03 GMT+0200 2015
Geotag Info  : Latitude:47.547509, Longitude:7.578211
Views        : 2,763
Comments     : 1


+---------+
|  TITLE  |
+---------+
Rhinoceros in the water


+---------------+
|  DESCRIPTION  |
+---------------+
A nice portrait of one of the rhinoceros of the Basel zoo having a bath...


+--------+
|  TAGS  |
+--------+
water bath bathing surface profile portrait face rhino rhinoceros big basel zoo zolli switzerland nikon d4 