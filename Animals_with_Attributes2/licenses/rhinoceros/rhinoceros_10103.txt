+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : dionhinchcliffe
Photo URL    : https://www.flickr.com/photos/dionhinchcliffe/9583474288/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 24 09:09:18 GMT+0200 2013
Upload Date  : Sat Aug 24 15:11:35 GMT+0200 2013
Geotag Info  : Latitude:-25.287942, Longitude:27.002633
Views        : 152
Comments     : 0


+---------+
|  TITLE  |
+---------+
African Rhinoceros


+---------------+
|  DESCRIPTION  |
+---------------+
This amazing specimen stopped right in front of the truck. They said it was the best sighting they'd seen in 30 years of taking game tours. At Pilanesberg Game Reserve.


+--------+
|  TAGS  |
+--------+
(no tags)