+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brainstorm1984
Photo URL    : https://www.flickr.com/photos/brainstorm1984/8213646006/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Nov 15 07:40:45 GMT+0100 2012
Upload Date  : Sat Nov 24 10:56:36 GMT+0100 2012
Geotag Info  : Latitude:-28.109755, Longitude:32.099432
Views        : 562
Comments     : 3


+---------+
|  TITLE  |
+---------+
Breitmaulnashorn / White Rhinoceros


+---------------+
|  DESCRIPTION  |
+---------------+
Ein Breitmaulnashorn im Hluhluwe-iMfolozi-Park (Südafrika).

A White Rhino in the Hluhluwe–iMfolozi Park (South Africa).


+--------+
|  TAGS  |
+--------+
Südafrika "South Africa" Safari Hluhluwe-Umfolozi Breitmaulnashorn "White Rhinoceros" Rhino Wildlife Rhinocerotidae "Ceratotherium simum" Imfolozi-Hluhluwe Rhinoceros "Square-lipped Rhinoceros" KwaZulu-Natal Hluhluwe 