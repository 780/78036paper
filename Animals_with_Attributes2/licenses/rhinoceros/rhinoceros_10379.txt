+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Chester Zoo
Photo URL    : https://www.flickr.com/photos/chesterzoo/2395815860/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 7 04:26:58 GMT+0200 2008
Upload Date  : Mon Apr 7 13:26:58 GMT+0200 2008
Views        : 272
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rhino


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://www.chesterzoo.org" rel="nofollow">Visit Chester Zoo</a>


+--------+
|  TAGS  |
+--------+
Rhino rhinoceros 