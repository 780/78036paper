+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Amy Aletheia Cahill
Photo URL    : https://www.flickr.com/photos/themeowverlord/7338565318/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 4 09:44:38 GMT+0200 2012
Upload Date  : Mon Jun 4 23:01:22 GMT+0200 2012
Views        : 28
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rhinoceros


+---------------+
|  DESCRIPTION  |
+---------------+
At the Denver Zoo.


+--------+
|  TAGS  |
+--------+
animals zoo 