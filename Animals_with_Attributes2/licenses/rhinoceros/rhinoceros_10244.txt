+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : David Jones
Photo URL    : https://www.flickr.com/photos/davidcjones/6363281153/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 18 09:37:01 GMT+0200 2011
Upload Date  : Sat Nov 19 15:54:42 GMT+0100 2011
Geotag Info  : Latitude:48.181980, Longitude:16.306972
Views        : 973
Comments     : 0


+---------+
|  TITLE  |
+---------+
Indian Rhinoceros at Vienna Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Rhinoceros unicornis


+--------+
|  TAGS  |
+--------+
Vienna Wien Austria "Tiergarten Schönbrunn" zoo "Rhinoceros unicornis" "Indian Rhinoceros" 