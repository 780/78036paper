+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : asokolik
Photo URL    : https://www.flickr.com/photos/asokolik/14948043871/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 17 10:43:09 GMT+0200 2014
Upload Date  : Sun Aug 17 23:40:17 GMT+0200 2014
Views        : 100
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_7150


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Buffalo Zoo" "Buffalo Zoological Gardens" "Indian Rhino" rhinoceros baby 