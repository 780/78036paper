+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tobyotter
Photo URL    : https://www.flickr.com/photos/78428166@N00/14557344811/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 2 12:09:53 GMT+0200 2014
Upload Date  : Wed Jul 2 22:55:32 GMT+0200 2014
Views        : 580
Comments     : 1


+---------+
|  TITLE  |
+---------+
Indian Rhinoceros


+---------------+
|  DESCRIPTION  |
+---------------+
Not the most flattering view but I like all the &quot;Armor Plating&quot;.


+--------+
|  TAGS  |
+--------+
Ohio "Cincinnati Zoo" Vacation rhino "Indian rhinoceros" Rhinoceros 