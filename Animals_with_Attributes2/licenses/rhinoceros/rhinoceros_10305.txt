+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pelican
Photo URL    : https://www.flickr.com/photos/pelican/1504618159/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 7 14:34:09 GMT+0200 2007
Upload Date  : Sun Oct 7 14:06:44 GMT+0200 2007
Views        : 142
Comments     : 1


+---------+
|  TITLE  |
+---------+
Oji zoo, Kobe, Japan


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
k100d "sigma apo 70-300mm F4-5.6" oji kobe zoo japan rhino rhinoceros oct07_oji 