+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flickkerphotos
Photo URL    : https://www.flickr.com/photos/38504374@N02/3683592479/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 3 04:57:36 GMT+0200 2009
Upload Date  : Fri Jul 3 13:57:36 GMT+0200 2009
Views        : 767
Comments     : 0


+---------+
|  TITLE  |
+---------+
Indian Rhinoceros


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://www.flickr.com/photos/bunnygoth/3432560503/sizes/l/">Photo Credit:<b> bunnygoth</b></a>


+--------+
|  TAGS  |
+--------+
rhinoceros "indian rhinoceros" flickr bunnygoth "endangered animals" awake! 