+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ryan Kilpatrick
Photo URL    : https://www.flickr.com/photos/rkilpatrick21/19614357714/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 17 00:54:40 GMT+0200 2015
Upload Date  : Sun Aug 2 22:20:59 GMT+0200 2015
Views        : 86
Comments     : 0


+---------+
|  TITLE  |
+---------+
Baby Rhino


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Hluhluwe rhino rhinoceros 