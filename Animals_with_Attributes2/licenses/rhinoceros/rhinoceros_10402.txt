+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : *_*
Photo URL    : https://www.flickr.com/photos/o_0/15691746011/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 1 12:25:40 GMT+0100 2014
Upload Date  : Sun Nov 2 21:47:32 GMT+0100 2014
Views        : 129
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rhinoceros @ Lahore Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
lahore pakistan penjab punjab asia autumn 2014 november sunny zoo animal 