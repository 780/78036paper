+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : foqus
Photo URL    : https://www.flickr.com/photos/foqus/4062503633/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 31 12:06:43 GMT+0100 2009
Upload Date  : Sun Nov 1 05:47:40 GMT+0100 2009
Views        : 77
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rhinoceros


+---------------+
|  DESCRIPTION  |
+---------------+
San Antonio Zoo


+--------+
|  TAGS  |
+--------+
san antonio zoo 