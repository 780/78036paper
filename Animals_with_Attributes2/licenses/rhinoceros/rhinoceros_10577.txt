+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Crazy Creatures
Photo URL    : https://www.flickr.com/photos/47456200@N04/4422513852/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 20 10:39:08 GMT+0200 2008
Upload Date  : Wed Mar 10 14:26:19 GMT+0100 2010
Views        : 927
Comments     : 0


+---------+
|  TITLE  |
+---------+
Southern White Rhinoceros


+---------------+
|  DESCRIPTION  |
+---------------+
Also, and more accurately, know as the Square-lipped Rhinoceros. These guys are larger than their black counterpart but far less skittish!
There is a Northern White Rhinoceros, but these are sadly on the brink of extinction, with none known to be alive in the wild, although 4 individuals are being reintroduced into the wild in Kenya.
Dominant males will mark their territory, for example using dung middens, to which they will return repeatedly to top up!

Find out about many different animals at <a href="http://www.crazycreatures.org" rel="nofollow">www.crazycreatures.org</a>


+--------+
|  TAGS  |
+--------+
Rhinoceros Swaziland "White Rhinoceros" "Southern White Rhinoceros" "Square-lipped Rhinoceros" Ceratotherium "Ceratotherium simum" 