+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/7615593566/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 16 11:20:39 GMT+0200 2012
Upload Date  : Sat Jul 21 17:00:24 GMT+0200 2012
Geotag Info  : Latitude:51.340719, Longitude:6.602504
Views        : 4,747
Comments     : 0


+---------+
|  TITLE  |
+---------+
Portrait of a rhino


+---------------+
|  DESCRIPTION  |
+---------------+
Just a nice portrait of a rhino living at the zoo of Krefeld...


+--------+
|  TAGS  |
+--------+
rhino rhinoceros horns gray portrait face zoo krefeld germany nikon d700 