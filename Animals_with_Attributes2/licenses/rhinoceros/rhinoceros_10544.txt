+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : iRonInk
Photo URL    : https://www.flickr.com/photos/ironink/17394587101/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 24 14:56:09 GMT+0200 2014
Upload Date  : Wed May 6 23:30:10 GMT+0200 2015
Geotag Info  : Latitude:43.817807, Longitude:-79.184131
Views        : 113
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rocksteady


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at the Toronto Zoo


+--------+
|  TAGS  |
+--------+
"Toronto Zoo" Rhino Rhinoceros 