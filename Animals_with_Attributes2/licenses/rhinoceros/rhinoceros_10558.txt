+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : the grey sky morning
Photo URL    : https://www.flickr.com/photos/hashamalee/1934559459/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 29 17:23:10 GMT+0100 2006
Upload Date  : Fri Nov 9 17:33:22 GMT+0100 2007
Views        : 30,707
Comments     : 4


+---------+
|  TITLE  |
+---------+
Sleeping Rhinos


+---------------+
|  DESCRIPTION  |
+---------------+
these rhinos are sleeping so peacefully in the zoo..that place must be taking care of them with love..just look at how peaceful they look :)


+--------+
|  TAGS  |
+--------+
sleep rhinoceros rhino peace peaceful zoo sleeping 