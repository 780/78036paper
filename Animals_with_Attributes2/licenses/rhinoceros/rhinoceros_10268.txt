+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jockrutherford
Photo URL    : https://www.flickr.com/photos/jockrutherford/3893388132/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 6 03:32:22 GMT+0200 2009
Upload Date  : Sun Sep 6 18:02:20 GMT+0200 2009
Views        : 99
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kolmården Djurpark (Zoo)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
kolmården djurpark zoo rhinoceros 