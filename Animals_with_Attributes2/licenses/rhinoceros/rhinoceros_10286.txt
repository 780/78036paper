+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bunnygoth
Photo URL    : https://www.flickr.com/photos/bunnygoth/3433332240/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 11 21:29:53 GMT+0200 2009
Upload Date  : Sun Apr 12 03:29:53 GMT+0200 2009
Views        : 112
Comments     : 0


+---------+
|  TITLE  |
+---------+
MiamiMetroZoo.040809.216


+---------------+
|  DESCRIPTION  |
+---------------+
Indian rhinoceros at the Miami MetroZoo, Florida


+--------+
|  TAGS  |
+--------+
miami metrozoo zoo florida vacation 2009 animals indian rhinoceros rhino 