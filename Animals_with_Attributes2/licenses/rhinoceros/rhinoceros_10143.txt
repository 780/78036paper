+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ShadowDragon928
Photo URL    : https://www.flickr.com/photos/athazen/5657615483/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 18 09:28:57 GMT+0100 2010
Upload Date  : Tue Apr 26 18:02:35 GMT+0200 2011
Views        : 42
Comments     : 0


+---------+
|  TITLE  |
+---------+
Black Rhinoceros 2383


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at Animal Kingdom, Walt Disney World


+--------+
|  TAGS  |
+--------+
(no tags)