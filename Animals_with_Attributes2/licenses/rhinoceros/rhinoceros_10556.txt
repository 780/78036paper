+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MrGuilt
Photo URL    : https://www.flickr.com/photos/bontempscharly/13961391983/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 19 13:28:01 GMT+0200 2014
Upload Date  : Mon Apr 21 05:44:56 GMT+0200 2014
Views        : 193
Comments     : 0


+---------+
|  TITLE  |
+---------+
Seyia, the Black Rhino


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Cincinnati "Cincinnati Zoo" Animals "Zoos & Aquariums" Rhinoceros "AF-S DX Zoom-Nikkor 55-200mm f/4-5.6G ED" 