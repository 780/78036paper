+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jean & Nathalie
Photo URL    : https://www.flickr.com/photos/jries/4645971328/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 17 06:04:26 GMT+0200 2010
Upload Date  : Thu May 27 23:16:35 GMT+0200 2010
Views        : 1,371
Comments     : 2


+---------+
|  TITLE  |
+---------+
Rhinos


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Karongwe Limpopop "South Africa" Südafrika Rhino Rhinoceros Nashorn Oxpecker BigFive 