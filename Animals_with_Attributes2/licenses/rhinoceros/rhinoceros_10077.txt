+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/17086215298/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 14 15:37:16 GMT+0100 2015
Upload Date  : Sun Apr 26 10:50:08 GMT+0200 2015
Views        : 94
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Black Rhinoceros


+--------+
|  TAGS  |
+--------+
"Chester Zoo" "Black Rhino" Black Rhinoceros 