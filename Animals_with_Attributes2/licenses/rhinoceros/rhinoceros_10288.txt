+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 06, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ryan Kilpatrick
Photo URL    : https://www.flickr.com/photos/rkilpatrick21/7092895663/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 16 09:23:58 GMT+0200 2012
Upload Date  : Thu Apr 19 11:00:27 GMT+0200 2012
Geotag Info  : Latitude:-24.760471, Longitude:26.044635
Views        : 366
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mother rhino and calf (and red-billed oxpeckers)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Madikwe "rhino horn" rhinoceros rhinos Safari "South Africa" wildlife 