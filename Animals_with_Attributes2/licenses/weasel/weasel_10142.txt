+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : big-ashb
Photo URL    : https://www.flickr.com/photos/big-ashb/16236071529/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 1 16:52:53 GMT+0100 2015
Upload Date  : Sun Feb 1 22:50:14 GMT+0100 2015
Geotag Info  : Latitude:51.173855, Longitude:-0.048878
Views        : 497
Comments     : 0


+---------+
|  TITLE  |
+---------+
Weasel (Mustela nivalis)


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://www.britishwildlifecentre.co.uk/" rel="nofollow">www.britishwildlifecentre.co.uk/</a>

British Wildlife Centre
Eastbourne Road (A22),
Newchapel,
LINGFIELD
Surrey RH7 6LF


+--------+
|  TAGS  |
+--------+
Canon 600D British Wildlife Centre surrey 