+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Black, Tami S, U.S. Fish and Wildlife Service
Photo URL    : https://commons.wikimedia.org/wiki/File:Blackfooted_ferret_out_of_the_artificial_holes.jpg
License      : public domain
Date         : Not given
