+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Peter G Trimming
Photo URL    : https://www.flickr.com/photos/peter-trimming/6999931768/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 5 15:25:24 GMT+0200 2012
Upload Date  : Sat May 5 22:11:37 GMT+0200 2012
Geotag Info  : Latitude:51.166562, Longitude:-0.049223
Views        : 937
Comments     : 0


+---------+
|  TITLE  |
+---------+
Introducing 'Sybil'


+---------------+
|  DESCRIPTION  |
+---------------+
Seen at the British Wildlife Centre, Newchapel, Surrey. A young stoat orphan, thought to be about 7 weeks old. Being hand-reared by Senior Keeper Izzy.


+--------+
|  TAGS  |
+--------+
British Wildlife Centre Newchapel Surrey Stoat Mustela erminea 2012 Sybil Peter Trimming 