+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Aug 23, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kentish Plumber
Photo URL    : https://www.flickr.com/photos/plumberjohn/17540721986/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 10 13:49:00 GMT+0200 2015
Upload Date  : Tue May 12 20:03:30 GMT+0200 2015
Views        : 680
Comments     : 5


+---------+
|  TITLE  |
+---------+
Stoat


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Mustela erminea" Stoat "Short-tailed Weasel" UK Mammal "British Wildlife" Nature Wildlife NBW 