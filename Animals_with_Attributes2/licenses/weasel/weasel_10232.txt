+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Dibyendu Ash
Photo URL    : https://commons.wikimedia.org/wiki/File:Siberian_Weasel_Pangolakha_Wildlife_Sanctuary_East_Sikkim_India_14.02.2016.jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution-Share Alike 3.0 Unported (//creativecommons.org/licenses/by-sa/3.0/deed.en)
Date         : 2016-02-14
