+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Aug 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tbtalbottjr
Photo URL    : https://www.flickr.com/photos/tomtalbott/22759556769/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 8 08:18:44 GMT+0200 2015
Upload Date  : Thu Nov 19 22:09:52 GMT+0100 2015
Views        : 54
Comments     : 0


+---------+
|  TITLE  |
+---------+
Being Weaselly -- Long-tailed Weasel


+---------------+
|  DESCRIPTION  |
+---------------+
Being Weaselly -- Long-tailed Weasel, Discovery Park, Seattle, WA


+--------+
|  TAGS  |
+--------+
"discovery park" seattle washington "long-tailed weasel" 