+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Daderot
Photo URL    : https://commons.wikimedia.org/wiki/File:Mustela_frenata_-_Springfield_Science_Museum_-_Springfield,_MA_-_DSC03427.JPG
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) CC0 1.0 Universal Public Domain Dedication (//creativecommons.org/publicdomain/zero/1.0/deed.en)
Date         : 2012-12-19 11:41:17
