+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : USFWS Mountain-Prairie
Photo URL    : https://commons.wikimedia.org/wiki/File:Mama_Ferret_Counts_her_Kids_(7537056038).jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution 2.0 Generic (//creativecommons.org/licenses/by/2.0/deed.en)
Date         : 2012-07-09 10:52
