+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USFWS Mountain Prairie
Photo URL    : https://www.flickr.com/photos/usfwsmtnprairie/15543664645/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Oct 14 15:04:32 GMT+0200 2014
Upload Date  : Wed Oct 15 19:14:27 GMT+0200 2014
Views        : 1,657
Comments     : 0


+---------+
|  TITLE  |
+---------+
Black-footed Ferrets in Preconditioning Pens


+---------------+
|  DESCRIPTION  |
+---------------+
Photo taken at the National Black-footed Ferret Conservation Center in northern Colorado, where the U.S. Fish and Wildlife Service leads a captive breeding and recovery program where this endangered species is bred and preconditioned for release into the wild.

Photo Credit: Ryan Moehring / USFWS


+--------+
|  TAGS  |
+--------+
usfws "U.S. FISH AND WILDLIFE SERVICE" "Endangered Species" "Black-footed Ferret" "Black-footed Ferrets" ESA #ESA40 "Endangered Species Act" "species conservation" "species reintroduction" Mustelidae "mustela nigripes" ferrets Ferret Colorado WILDLIFE NATURE CONSERVATION 