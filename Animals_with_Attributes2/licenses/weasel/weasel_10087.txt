+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USFWS Mountain Prairie
Photo URL    : https://www.flickr.com/photos/usfwsmtnprairie/11208273936/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 4 08:44:36 GMT+0100 2013
Upload Date  : Wed Dec 4 17:46:38 GMT+0100 2013
Views        : 4,510
Comments     : 2


+---------+
|  TITLE  |
+---------+
Learning to Hunt


+---------------+
|  DESCRIPTION  |
+---------------+
This is yet another amazing capture from our National Black-footed Ferret Conservation Center.

The center breeds endangered black-footed ferrets and then prepares them for release into the wild. Here, a BFF is seen learning to catch live prey! Over 90% of a black-footed ferret's diet in the wild is prairie dog.

Credit: USFWS


+--------+
|  TAGS  |
+--------+
"Black-footed Ferret" Mustelidae "mustela nigripes" ferrets Ferret "Prairie dog" "prairie dogs" USFWS "U.S. FISH AND WILDLIFE SERVICE" "National Black-footed Ferret Conservation Center" "Endangered Species" WILDLIFE Colorado predation predator 