+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Anatolii Vlasov
Photo URL    : https://commons.wikimedia.org/wiki/File:Mustela_nivalis_-_2013%2704%2713-19h21m17s.jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution-Share Alike 3.0 Unported (//creativecommons.org/licenses/by-sa/3.0/deed.en)
Date         : 2013-04-13
