+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 13, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Peter G Trimming
Photo URL    : https://www.flickr.com/photos/peter-trimming/5476897151/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Feb 24 12:55:29 GMT+0100 2011
Upload Date  : Fri Feb 25 22:36:37 GMT+0100 2011
Views        : 807
Comments     : 0


+---------+
|  TITLE  |
+---------+
Singing Polecat?


+---------------+
|  DESCRIPTION  |
+---------------+
Is the female polecat, 'Velvet' giving a rendition of 'The Hills are Alive'? No; in fact she is just having a good yawn, at the British Wildlife Centre.


+--------+
|  TAGS  |
+--------+
British Wildlife Centre Newchapel Surrey Polecat Mustela putorius Peter Trimming Sigma 300mm EX DG 