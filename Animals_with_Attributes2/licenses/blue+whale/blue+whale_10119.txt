+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jamie McEwan
Photo URL    : https://www.flickr.com/photos/jamie_from_dunedin/2737614231/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 6 12:59:22 GMT+0200 2008
Upload Date  : Wed Aug 6 13:08:24 GMT+0200 2008
Views        : 1,165
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sperm Whale at Kaikoura


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Sperm Whale NZ Kaikoura Whalewatch "New Zealand" 