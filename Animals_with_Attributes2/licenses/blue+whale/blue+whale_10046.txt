+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Scratchdaddy
Photo URL    : https://www.flickr.com/photos/77746720@N00/3452098678/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 17 22:13:20 GMT+0200 2009
Upload Date  : Sat Apr 18 07:13:20 GMT+0200 2009
Views        : 126
Comments     : 0


+---------+
|  TITLE  |
+---------+
bluewhaletail


+---------------+
|  DESCRIPTION  |
+---------------+
The blue whale population on the West Coast is on the rise, according to researchers who photographed this one off Bodega Bay, Calif.  (Gretchen Steiger/Cascadia Research)


+--------+
|  TAGS  |
+--------+
blue whale tail 