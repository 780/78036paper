+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jkirkhart35
Photo URL    : https://www.flickr.com/photos/jkirkhart35/3900673125/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 7 10:38:52 GMT+0200 2009
Upload Date  : Tue Sep 8 21:22:15 GMT+0200 2009
Views        : 402
Comments     : 2


+---------+
|  TITLE  |
+---------+
Dual Blow Holes in Action (Balaenoptera musculus)


+---------------+
|  DESCRIPTION  |
+---------------+
You can easily seen that two blowholes of the Blue Whale.  Rouvaishyana arranged the great day for us to travel to the Channel Islands searching for whales.


+--------+
|  TAGS  |
+--------+
(no tags)