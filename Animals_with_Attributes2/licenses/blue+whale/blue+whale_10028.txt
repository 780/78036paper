+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Gregory "Slobirdr" Smith
Photo URL    : https://www.flickr.com/photos/slobirdr/16108163937/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 6 00:00:00 GMT+0200 2013
Upload Date  : Fri Jan 16 17:26:16 GMT+0100 2015
Geotag Info  : Latitude:37.436975, Longitude:-122.542877
Views        : 38,188
Comments     : 28


+---------+
|  TITLE  |
+---------+
Blue Whale (Balaenoptera musculus)


+---------------+
|  DESCRIPTION  |
+---------------+
On my California tour last year, we were fortunate enough to have a loose group of 25 blue whales off of Half Moon Bay, CA...


+--------+
|  TAGS  |
+--------+
flukes "marine mammal" "Gregory Slobirdr Smith" NaturalistJourneys.com "Blue Whale (Balaenoptera musculus)" 