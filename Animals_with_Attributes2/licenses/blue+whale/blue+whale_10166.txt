+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : indigo_iggy
Photo URL    : https://www.flickr.com/photos/44235769@N03/4842242461/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 29 14:21:10 GMT+0200 2010
Upload Date  : Fri Jul 30 05:51:53 GMT+0200 2010
Views        : 203
Comments     : 0


+---------+
|  TITLE  |
+---------+
Whales off Morro Bay Coastline


+---------------+
|  DESCRIPTION  |
+---------------+
We had a wonderful day on Sub-Sea tours, and lost count of the blue and humpback whales.  At one point, 5 were circling our boat!


+--------+
|  TAGS  |
+--------+
(no tags)