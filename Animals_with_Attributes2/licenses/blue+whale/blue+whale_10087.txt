+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Arian Zwegers
Photo URL    : https://www.flickr.com/photos/azwegers/6917912897/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 20 07:38:30 GMT+0100 2010
Upload Date  : Mon Nov 7 12:20:42 GMT+0100 2011
Geotag Info  : Latitude:5.903774, Longitude:80.425930
Views        : 7,065
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mirissa, whale watching, blue whale


+---------------+
|  DESCRIPTION  |
+---------------+
Mirissa, whale watching, blue whale

The blue whale (Balaenoptera musculus) is a marine mammal belonging to the baleen whales (Mysticeti). At 30 metres in length and 180 tonnes or more in weight, it is the largest existing animal and the heaviest that has ever existed.

Long and slender, the blue whale's body can be various shades of bluish-grey dorsally and somewhat lighter underneath. There are at least three distinct subspecies: B. m. musculus of the North Atlantic and North Pacific, B. m. intermedia of the Southern Ocean and B. m. brevicauda (also known as the pygmy blue whale) found in the Indian Ocean and South Pacific Ocean. B. m. indica, found in the Indian Ocean, may be another subspecies. As with other baleen whales, its diet consists almost exclusively of small crustaceans known as krill.

Blue whales were abundant in nearly all the oceans on Earth until the beginning of the twentieth century. For over a century, they were hunted almost to extinction by whalers until protected by the international community in 1966. A 2002 report estimated there were 5,000 to 12,000 blue whales worldwide, located in at least five groups.

(source: <a href="http://en.wikipedia.org/wiki/Blue_whale" rel="nofollow">en.wikipedia.org/wiki/Blue_whale</a>)


+--------+
|  TAGS  |
+--------+
Mirissa "whale watching" "blue whale" whale "Sri Lanka" "Balaenoptera musculus" "marine mammal" mammal "baleen whales" Mysticeti "Indian Ocean" 2010 