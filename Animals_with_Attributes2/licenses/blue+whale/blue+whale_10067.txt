+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Gemma Louise Lowe
Photo URL    : https://www.flickr.com/photos/gemmalouiselowe/4598641066/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 8 02:22:45 GMT+0200 2007
Upload Date  : Tue May 11 15:26:56 GMT+0200 2010
Views        : 472
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hump Back Whale, South Island, New Zealand.


+---------------+
|  DESCRIPTION  |
+---------------+
Hump Back Whale, South Island, New Zealand.


+--------+
|  TAGS  |
+--------+
blue Humpback Whale Sea "New Zealand" Dive 