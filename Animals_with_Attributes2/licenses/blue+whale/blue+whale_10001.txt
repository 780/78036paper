+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ptwo
Photo URL    : https://www.flickr.com/photos/ptwo/5002365811/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 18 22:30:41 GMT+0200 2010
Upload Date  : Sun Sep 19 02:57:43 GMT+0200 2010
Geotag Info  : Latitude:42.147957, Longitude:-70.155086
Views        : 740
Comments     : 0


+---------+
|  TITLE  |
+---------+
843


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
usa massachusetts boston "atlantic ocean" "gulf of maine" "stellwagen bank national marine sanctuary" "nora vittoria" "boston harbor cruises" "humpback whales" "Free Fall" "Spring Board" water blue 