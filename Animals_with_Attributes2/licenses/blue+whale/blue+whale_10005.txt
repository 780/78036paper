+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Allie_Caulfield
Photo URL    : https://www.flickr.com/photos/wm_archiv/2795093987/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 29 02:50:38 GMT+0100 2001
Upload Date  : Mon Aug 25 10:12:37 GMT+0200 2008
Geotag Info  : Latitude:-42.371227, Longitude:173.735561
Views        : 811
Comments     : 0


+---------+
|  TITLE  |
+---------+
2001-12-02 01-03 Neuseeland 396


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2001 Neuseeland "new zealand" Aotearoa Dezember Winter Sommer hiking backpacker Südinsel "south island" island Süden Natur nature beach Strand ocean Ozean Pacific Pazifik coast Küste Kaikoura "whale watching" whale Wal Pottwal "sperm whale" Flosse fin Foto photo image picture Bild "creative commons" flickr "high resolution" stockphoto free cc 