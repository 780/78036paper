+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Gemma Louise Lowe
Photo URL    : https://www.flickr.com/photos/gemmalouiselowe/4598641674/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 8 03:27:53 GMT+0200 2007
Upload Date  : Tue May 11 15:27:13 GMT+0200 2010
Views        : 1,179
Comments     : 2


+---------+
|  TITLE  |
+---------+
Hump Back Whale, South Island, New Zealand


+---------------+
|  DESCRIPTION  |
+---------------+
Hump Back Whale, South Island, New Zealand


+--------+
|  TAGS  |
+--------+
Humpback Whale Sea "New Zealand" clouds mountains dive blue animals 