+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Noveltyy
Photo URL    : https://www.flickr.com/photos/90872331@N05/8316352776/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 24 23:34:03 GMT+0200 2012
Upload Date  : Fri Dec 28 00:25:48 GMT+0100 2012
Views        : 2,283
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
One of the younger elephants in the exhibit. I can't remember which zoo.


+--------+
|  TAGS  |
+--------+
Elephant 