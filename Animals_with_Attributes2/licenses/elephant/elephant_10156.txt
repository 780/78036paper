+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : smerikal
Photo URL    : https://www.flickr.com/photos/smerikal/7634934552/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 19 09:46:03 GMT+0200 2012
Upload Date  : Tue Jul 24 07:12:01 GMT+0200 2012
Views        : 807
Comments     : 2


+---------+
|  TITLE  |
+---------+
Dubare elephant ride


+---------------+
|  DESCRIPTION  |
+---------------+
2012-Intia_1480z

Blogged: <a href="http://beautifulnow.is/bnow/the-wild-nature-of-beautiful-rides-now" rel="nofollow">beautifulnow.is/bnow/the-wild-nature-of-beautiful-rides-now</a>


+--------+
|  TAGS  |
+--------+
elephant norsu 