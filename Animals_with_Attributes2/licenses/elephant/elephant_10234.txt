+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Stefan Willoughby
Photo URL    : https://www.flickr.com/photos/stefwillo/3968665543/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 22 15:01:39 GMT+0200 2009
Upload Date  : Wed Sep 30 18:08:18 GMT+0200 2009
Views        : 406
Comments     : 1


+---------+
|  TITLE  |
+---------+
Elephant Family


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
chester zoo cheshire uk elephant family 