+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : The Mind's Eye Photography
Photo URL    : https://www.flickr.com/photos/kristinpia/215988127/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 20 09:54:06 GMT+0200 2005
Upload Date  : Tue Aug 15 16:28:12 GMT+0200 2006
Views        : 233
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
About to get run over...


+--------+
|  TAGS  |
+--------+
"Philadelphia Zoo" animals zoo Elephant 