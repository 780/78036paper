+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : James Temple
Photo URL    : https://www.flickr.com/photos/jamestemple/312324208/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 2 22:52:24 GMT+0100 2006
Upload Date  : Sat Dec 2 23:52:24 GMT+0100 2006
Geotag Info  : Latitude:-24.661994, Longitude:31.486816
Views        : 680
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Sabi Sands" "South Africa" Wildlife Elephant 