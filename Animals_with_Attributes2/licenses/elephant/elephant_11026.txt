+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Genista
Photo URL    : https://www.flickr.com/photos/genista/5089027455/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 10 07:46:39 GMT+0200 2010
Upload Date  : Sun Oct 17 16:08:12 GMT+0200 2010
Geotag Info  : Latitude:51.945641, Longitude:7.591552
Views        : 1,169
Comments     : 0


+---------+
|  TITLE  |
+---------+
what elephants also do


+---------------+
|  DESCRIPTION  |
+---------------+
I thought they slept standing up, like cows, and at night entire african fraternities run into the savannah and collectively tip one over.

Then again, maybe that's what happened here.


+--------+
|  TAGS  |
+--------+
allwetterzoo deutschland elefant elephant germany münster schlafen sleeping umgefallen westfalen 