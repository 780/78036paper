+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Drehatlas.de
Photo URL    : https://www.flickr.com/photos/89773578@N05/8164783660/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 7 11:38:57 GMT+0100 2012
Upload Date  : Wed Nov 7 20:40:16 GMT+0100 2012
Geotag Info  : Latitude:-1.959647, Longitude:30.675201
Views        : 215
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
A male elephant in Akagera Park, Rwanda 2011


+--------+
|  TAGS  |
+--------+
(no tags)