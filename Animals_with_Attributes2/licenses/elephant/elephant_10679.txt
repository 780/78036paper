+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : samuelrodgers752
Photo URL    : https://www.flickr.com/photos/130732751@N03/16486456144/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 1 14:03:06 GMT+0200 2015
Upload Date  : Sat Apr 11 15:52:14 GMT+0200 2015
Views        : 832
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephants holding trunks


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
elephants walking holding trunks 