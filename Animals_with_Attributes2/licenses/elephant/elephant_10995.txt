+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : little miss ladybug
Photo URL    : https://www.flickr.com/photos/61556972@N05/5726380624/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Nov 5 13:20:21 GMT+0100 2009
Upload Date  : Mon May 16 14:25:11 GMT+0200 2011
Views        : 806
Comments     : 0


+---------+
|  TITLE  |
+---------+
Three elephants


+---------------+
|  DESCRIPTION  |
+---------------+
Australia Zoo.


+--------+
|  TAGS  |
+--------+
elephants 