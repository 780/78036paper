+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : RichardBowen
Photo URL    : https://www.flickr.com/photos/rbowen/221846926/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 21 00:06:31 GMT+0200 2006
Upload Date  : Tue Aug 22 10:18:29 GMT+0200 2006
Geotag Info  : Latitude:6.714391, Longitude:80.681190
Views        : 318
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephants bathing at the river in Pinawalla


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
elephant pinawalla srilanka 