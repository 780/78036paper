+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MyAngelG
Photo URL    : https://www.flickr.com/photos/aasg/861208100/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 14 13:46:05 GMT+0200 2007
Upload Date  : Fri Jul 20 23:26:56 GMT+0200 2007
Views        : 1,419
Comments     : 1


+---------+
|  TITLE  |
+---------+
Elephant at Knysna


+---------------+
|  DESCRIPTION  |
+---------------+
At Knysna Elephant Park


+--------+
|  TAGS  |
+--------+
elephant Knysna south africa forest 