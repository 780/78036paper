+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ed Bierman
Photo URL    : https://www.flickr.com/photos/edbierman/8486504811/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 1 00:00:00 GMT+0100 2006
Upload Date  : Tue Feb 19 00:26:03 GMT+0100 2013
Views        : 60
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
Oakland Zoo, California


+--------+
|  TAGS  |
+--------+
animals mammals "nikon d80" "oakland zoo" 