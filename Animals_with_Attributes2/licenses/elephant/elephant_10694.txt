+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tobo
Photo URL    : https://www.flickr.com/photos/tobin/146581223/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 14 14:33:48 GMT+0200 2006
Upload Date  : Mon May 15 03:44:04 GMT+0200 2006
Views        : 980
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephants


+---------------+
|  DESCRIPTION  |
+---------------+
Driving home from a weekend trip to Syracuse, I was surprised to find the parking lot across from the Public Safety Building populated by elephants.


+--------+
|  TAGS  |
+--------+
circus rochester 