+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aj82
Photo URL    : https://www.flickr.com/photos/alexander_johmann/558391017/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 6 10:53:27 GMT+0200 2006
Upload Date  : Sun Jun 17 00:51:42 GMT+0200 2007
Views        : 241
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elefant


+---------------+
|  DESCRIPTION  |
+---------------+
Im Tarangire National Park, Tansania.


+--------+
|  TAGS  |
+--------+
afrika africa tansania tanzania tag12 tarangire nationalpark elefanten elephants 