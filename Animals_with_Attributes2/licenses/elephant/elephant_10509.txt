+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : linh.m.do
Photo URL    : https://www.flickr.com/photos/lmdo/5555972490/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 24 14:35:42 GMT+0100 2011
Upload Date  : Thu Mar 24 15:02:56 GMT+0100 2011
Geotag Info  : Latitude:-37.783475, Longitude:144.950759
Views        : 822
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"melbourne zoo" melbourne elephant 