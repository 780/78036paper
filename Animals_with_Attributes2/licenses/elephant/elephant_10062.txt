+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : robertpaulyoung
Photo URL    : https://www.flickr.com/photos/robertpaulyoung/2703485909/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 3 03:39:04 GMT+0100 2008
Upload Date  : Sat Jul 26 18:51:44 GMT+0200 2008
Geotag Info  : Latitude:-26.791743, Longitude:153.013286
Views        : 1,796
Comments     : 2


+---------+
|  TITLE  |
+---------+
Asian Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
australia brisbane queensland "australia zoo" asian elephant 