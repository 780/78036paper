+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : missbossy
Photo URL    : https://www.flickr.com/photos/missbossy/3290293581/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 14 14:39:45 GMT+0100 2009
Upload Date  : Wed Feb 18 19:36:36 GMT+0100 2009
Geotag Info  : Latitude:18.795696, Longitude:98.833351
Views        : 124
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephants


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
thailand chiangmai animals 