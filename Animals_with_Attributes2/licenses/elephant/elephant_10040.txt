+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BOMBMAN
Photo URL    : https://www.flickr.com/photos/ajay_g/5987647148/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 3 20:02:06 GMT+0100 2010
Upload Date  : Fri Jul 29 15:04:57 GMT+0200 2011
Geotag Info  : Latitude:-25.249509, Longitude:27.085590
Views        : 1,310
Comments     : 0


+---------+
|  TITLE  |
+---------+
African Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
elephant african 