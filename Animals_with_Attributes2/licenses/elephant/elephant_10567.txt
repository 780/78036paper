+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hobgadlng
Photo URL    : https://www.flickr.com/photos/hobgadlng/7761613758/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 16 12:00:25 GMT+0200 2012
Upload Date  : Sun Aug 12 01:10:18 GMT+0200 2012
Geotag Info  : Latitude:-19.864570, Longitude:23.431949
Views        : 222
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Namibia Guma animal elephant Botswana "Okavanga Delta" "Baboon Island" 