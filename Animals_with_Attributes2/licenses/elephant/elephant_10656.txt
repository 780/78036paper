+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : atlai
Photo URL    : https://www.flickr.com/photos/atlai/6069206022/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 20 21:22:21 GMT+0200 2011
Upload Date  : Mon Aug 22 12:57:29 GMT+0200 2011
Views        : 635
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant, Auckland Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
auckland zoo elephant 