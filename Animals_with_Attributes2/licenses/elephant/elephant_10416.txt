+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brian.gratwicke
Photo URL    : https://www.flickr.com/photos/briangratwicke/3594307555/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 16 04:16:48 GMT+0100 2009
Upload Date  : Thu Jun 4 12:43:18 GMT+0200 2009
Views        : 403
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
India June 2009 Asian elephant mammal 