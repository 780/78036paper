+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : icelight
Photo URL    : https://www.flickr.com/photos/icelight/48093988/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 25 18:10:34 GMT+0200 2003
Upload Date  : Fri Sep 30 23:40:36 GMT+0200 2005
Views        : 3,327
Comments     : 1


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
Decent head-on shot a a big elephant, taken in Kruger, SA this time. Ragged ears on this big guy (Or gal maybe, hard to tell with African elephants if they don't have young around.)


+--------+
|  TAGS  |
+--------+
"south africa" kruger elephant 