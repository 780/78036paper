+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike's Birds
Photo URL    : https://www.flickr.com/photos/pazzani/6593636009/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 10 23:10:09 GMT+0100 2011
Upload Date  : Thu Dec 29 13:17:47 GMT+0100 2011
Geotag Info  : Latitude:-24.796187, Longitude:31.498266
Views        : 91
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)