+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Maggie Hannan
Photo URL    : https://www.flickr.com/photos/walnutwhippet/3001658241/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 2 21:32:27 GMT+0100 2008
Upload Date  : Tue Nov 4 11:17:30 GMT+0100 2008
Views        : 158
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kenya


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
elephant kenya nairobi elephantorphanage davidsheldrickwildlifetrust 