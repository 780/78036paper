+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Leszek.Leszczynski
Photo URL    : https://www.flickr.com/photos/leszekleszczynski/4666046542/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 26 12:06:49 GMT+0200 2010
Upload Date  : Thu Jun 3 11:50:43 GMT+0200 2010
Views        : 1,063
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animal berlin zoo elephant 