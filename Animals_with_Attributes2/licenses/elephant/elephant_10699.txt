+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : elvissa
Photo URL    : https://www.flickr.com/photos/elvissa/200576909/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 23 14:29:04 GMT+0200 2006
Upload Date  : Sat Jul 29 00:33:20 GMT+0200 2006
Views        : 3,028
Comments     : 4


+---------+
|  TITLE  |
+---------+
elephants


+---------------+
|  DESCRIPTION  |
+---------------+
Elephant encounter.


+--------+
|  TAGS  |
+--------+
"central florida zoological park" sanford zoo elephant 