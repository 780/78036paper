+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gvgoebel
Photo URL    : https://www.flickr.com/photos/37467370@N08/16207950845/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 5 11:33:08 GMT+0100 2014
Upload Date  : Mon Jan 5 20:49:05 GMT+0100 2015
Views        : 423
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ybelf_6b


+---------------+
|  DESCRIPTION  |
+---------------+
asian elephant, Denver Zoo, Colorado / 2014


+--------+
|  TAGS  |
+--------+
animals mammals "asian elephant" 