+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ChaTo (Carlos Castillo)
Photo URL    : https://www.flickr.com/photos/chato/5595069660/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 5 12:41:39 GMT+0200 2011
Upload Date  : Wed Apr 6 13:30:05 GMT+0200 2011
Views        : 2,153
Comments     : 0


+---------+
|  TITLE  |
+---------+
Baby elephant eating


+---------------+
|  DESCRIPTION  |
+---------------+
A family of elephants in Nagarhole park at dusk. They are eating by first scratching the ground with their feet to loosen the grass, before picking it with their trunks. The baby elephant (bottom left) is also eating, well protected by the rest of the family.


+--------+
|  TAGS  |
+--------+
elephant india nagarhole 