+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mcoughlin
Photo URL    : https://www.flickr.com/photos/mcoughlin/11330864553/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Nov 21 00:00:04 GMT+0100 2013
Upload Date  : Thu Dec 12 00:32:17 GMT+0100 2013
Geotag Info  : Latitude:-3.965640, Longitude:35.971984
Views        : 7,736
Comments     : 2


+---------+
|  TITLE  |
+---------+
Elephants


+---------------+
|  DESCRIPTION  |
+---------------+
Elephants in Tarangire National Park, Tanzania.


+--------+
|  TAGS  |
+--------+
tanzania tarangire elephant "african elephant" 