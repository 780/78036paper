+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lorzs Photos
Photo URL    : https://www.flickr.com/photos/lorizs/762648504/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 30 11:14:29 GMT+0200 2007
Upload Date  : Mon Jul 9 19:29:32 GMT+0200 2007
Views        : 393
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephants


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"stl zoo" animals "st louis zoo" elephant baby 