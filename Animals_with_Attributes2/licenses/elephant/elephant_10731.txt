+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : phalinn
Photo URL    : https://www.flickr.com/photos/phalinn/2917177585/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 5 16:21:35 GMT+0200 2008
Upload Date  : Mon Oct 6 07:36:49 GMT+0200 2008
Geotag Info  : Latitude:3.207832, Longitude:101.756486
Views        : 530
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephants


+---------------+
|  DESCRIPTION  |
+---------------+
Zoo Negara
MRR2
Selangor Darul Ehsan
Malaysia


+--------+
|  TAGS  |
+--------+
Zoo negara kl "kuala lumpur" malaysia selangor wildlife nature photography canon eos 400d gajah elephants giraffe zirafah outdoor deer rusa antelope tiger harimau lion singa leopard panther unta camel mawas orang utan apes monyet capybara rhinoceros badak sumbu air hippopotamus seladang birds burung kambing goat crocodile baua buaya 