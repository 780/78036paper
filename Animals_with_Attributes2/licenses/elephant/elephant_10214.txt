+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ARTS
Photo URL    : https://www.flickr.com/photos/arts/6529732881/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 17 22:54:02 GMT+0100 2011
Upload Date  : Sun Dec 18 07:54:02 GMT+0100 2011
Views        : 257
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant at Woodland Park


+---------------+
|  DESCRIPTION  |
+---------------+
Taken October 5, 2011


+--------+
|  TAGS  |
+--------+
elephant instagram 