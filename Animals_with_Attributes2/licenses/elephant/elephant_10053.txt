+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : QuinnDombrowski
Photo URL    : https://www.flickr.com/photos/quinndombrowski/3967362269/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 12 10:28:30 GMT+0100 2007
Upload Date  : Wed Sep 30 05:31:14 GMT+0200 2009
Views        : 371
Comments     : 0


+---------+
|  TITLE  |
+---------+
An elephant's lunch


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
elephant lunch eating grass trunk 