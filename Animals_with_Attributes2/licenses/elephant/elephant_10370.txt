+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BBM Explorer
Photo URL    : https://www.flickr.com/photos/bbmexplorer/2560704538/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 4 06:51:55 GMT+0200 2008
Upload Date  : Sun Jun 8 09:53:47 GMT+0200 2008
Geotag Info  : Latitude:-25.023395, Longitude:31.522521
Views        : 343
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
South African Tourist Board Familiarisation Trip, June 2008.

You are welcome to use any of the photos in this set providing you leave a link back to our website: 

<a href="http://www.bbmexplorer.com" rel="nofollow">www.bbmexplorer.com</a>


+--------+
|  TAGS  |
+--------+
"South Africa" "Rainbow Nation" "africa elephant" elephant 