+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Alex Guibord
Photo URL    : https://www.flickr.com/photos/alexguibord/6842561950/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 28 21:06:25 GMT+0100 2012
Upload Date  : Sat Mar 17 02:52:54 GMT+0100 2012
Geotag Info  : Latitude:-26.838828, Longitude:152.959792
Views        : 290
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
#australia #zoo #queensland #elephant 