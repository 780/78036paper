+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MaxH42
Photo URL    : https://www.flickr.com/photos/maxh42/9595873798/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 8 16:36:21 GMT+0200 2013
Upload Date  : Mon Aug 26 03:04:13 GMT+0200 2013
Views        : 331
Comments     : 0


+---------+
|  TITLE  |
+---------+
2013-08-08 08.36.21


+---------------+
|  DESCRIPTION  |
+---------------+
Elephants eating the bark off of a baobab tree. Our guide said that they get kind of drunk off of it, and that's when you really have to stay away from them.


+--------+
|  TAGS  |
+--------+
animal elephant tree 