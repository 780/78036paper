+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lars Juhl Jensen
Photo URL    : https://www.flickr.com/photos/larsjuhljensen/5873492695/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 11 10:52:12 GMT+0200 2011
Upload Date  : Sun Jun 26 21:07:12 GMT+0200 2011
Views        : 527
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant in Copenhagen Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
copenhagen elephant zoo 