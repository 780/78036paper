+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : currybet
Photo URL    : https://www.flickr.com/photos/currybet/82188149/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 5 00:00:22 GMT+0100 2006
Upload Date  : Wed Jan 4 22:00:22 GMT+0100 2006
Geotag Info  : Latitude:25.254865, Longitude:51.487426
Views        : 566
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
doha qatar zoo "doha zoo" elephant 