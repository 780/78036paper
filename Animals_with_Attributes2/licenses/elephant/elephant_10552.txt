+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : PreVamp
Photo URL    : https://www.flickr.com/photos/prevamp/7131220563/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 30 12:59:00 GMT+0200 2012
Upload Date  : Tue May 1 11:20:17 GMT+0200 2012
Geotag Info  : Latitude:50.957886, Longitude:6.973164
Views        : 256
Comments     : 0


+---------+
|  TITLE  |
+---------+
elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
köln zoo 2012 tiere tiger giraffe elefant pinguin gepard vogel paradies ente natur tierpark 