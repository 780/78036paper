+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hobgadlng
Photo URL    : https://www.flickr.com/photos/hobgadlng/7766608576/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 19 17:04:39 GMT+0200 2012
Upload Date  : Sun Aug 12 19:30:53 GMT+0200 2012
Geotag Info  : Latitude:-17.789064, Longitude:25.192165
Views        : 280
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant Eating


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Botswana Chobe animal elephant 