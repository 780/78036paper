+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Aasen Ryan Family
Photo URL    : https://www.flickr.com/photos/vistadome/6161705950/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 18 15:14:16 GMT+0200 2011
Upload Date  : Mon Sep 19 06:30:52 GMT+0200 2011
Views        : 100
Comments     : 0


+---------+
|  TITLE  |
+---------+
Toronto Zoo - September 2011


+---------------+
|  DESCRIPTION  |
+---------------+
Gigi's first time at the Toronto Zoo.


+--------+
|  TAGS  |
+--------+
"toronto zoo" elephant 