+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Marco Zanferrari
Photo URL    : https://www.flickr.com/photos/tuttotutto/21999547088/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 8 15:31:43 GMT+0200 2014
Upload Date  : Thu Oct 15 11:06:45 GMT+0200 2015
Geotag Info  : Latitude:-22.656591, Longitude:18.202511
Views        : 50
Comments     : 0


+---------+
|  TITLE  |
+---------+
Filling the belly


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wildlife Namibia elephant 