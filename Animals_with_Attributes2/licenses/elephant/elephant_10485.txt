+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : oliver.dodd
Photo URL    : https://www.flickr.com/photos/oliverdodd/5079874650/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 24 06:06:27 GMT+0200 2010
Upload Date  : Thu Oct 14 01:59:15 GMT+0200 2010
Views        : 211
Comments     : 0


+---------+
|  TITLE  |
+---------+
elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
africa tanzania safari tarangire elephant 