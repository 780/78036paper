+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Danny Nicholson
Photo URL    : https://www.flickr.com/photos/dannynic/5895974613/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 1 14:24:10 GMT+0200 2011
Upload Date  : Sun Jul 3 09:27:01 GMT+0200 2011
Views        : 307
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
elephant trunk zoo colchester essex animal keeper zookeeper treat experience meerkat mammals 