+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Max Wolfe
Photo URL    : https://www.flickr.com/photos/leeadlaf/15594696274/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 7 10:34:40 GMT+0200 2014
Upload Date  : Tue Jan 6 22:38:54 GMT+0100 2015
Views        : 231
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephants


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Alabama Birmingham Elephant Nature Outdoors Summer "Birmingham Alabama Zoo" 