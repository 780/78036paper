+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hobgadlng
Photo URL    : https://www.flickr.com/photos/hobgadlng/7668912056/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 30 15:39:26 GMT+0200 2012
Upload Date  : Sun Jul 29 16:51:33 GMT+0200 2012
Geotag Info  : Latitude:-19.267396, Longitude:16.677417
Views        : 186
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Namibia animal elephant Etosha 