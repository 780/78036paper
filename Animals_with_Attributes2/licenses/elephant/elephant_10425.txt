+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Numinosity (Gary J Wood)
Photo URL    : https://www.flickr.com/photos/garyjwood/3792316505/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 31 12:58:02 GMT+0200 2009
Upload Date  : Wed Aug 5 21:30:10 GMT+0200 2009
Views        : 61
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
USA "audubon zoo" louisiana "new orleans" animals 