+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/7478501436/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 4 15:10:49 GMT+0200 2012
Upload Date  : Sun Jul 1 14:11:36 GMT+0200 2012
Views        : 101
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
elephants


+--------+
|  TAGS  |
+--------+
"Chester Zoo" Elephants 