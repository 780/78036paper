+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Voyages Lambert
Photo URL    : https://www.flickr.com/photos/voyages-lambert/12838067884/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 13 01:53:34 GMT+0100 2009
Upload Date  : Fri Feb 28 18:14:59 GMT+0100 2014
Geotag Info  : Latitude:-1.757536, Longitude:34.958496
Views        : 663
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elephant


+---------------+
|  DESCRIPTION  |
+---------------+
Kenya &amp; Tanzanie
Les plus beaux parcs animaliers de la planète

VOYAGES LAMBERT
Spécialiste des voyages culturels et d'aventures en Asie, Afrique, Océanie, Amérique du Sud et Europe !


+--------+
|  TAGS  |
+--------+
ngorongoro_crater cratere_ngorongoro lac_nakuru lac_victoria kenya nakuru_lake reserve_samburu masai masai-mara zebra tanzanie hipopotame phacochere pelican impala lion varan serengeti trip-kenya trip-tanzania voyage-tanzanie voyage-kenya safari-tanzania tanzania-tour tanzania-safari kenya-safari 