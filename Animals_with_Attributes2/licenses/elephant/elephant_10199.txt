+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lucia Maglione
Photo URL    : https://www.flickr.com/photos/luciamaglione/4453406811/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 16 14:57:39 GMT+0200 2009
Upload Date  : Mon Mar 22 13:21:47 GMT+0100 2010
Views        : 210
Comments     : 0


+---------+
|  TITLE  |
+---------+
Elefanti


+---------------+
|  DESCRIPTION  |
+---------------+
la foto originale era così: <a href="http://www.flickr.com/photos/luciamaglione/4446980709/in/set-72157623655845872/">www.flickr.com/photos/luciamaglione/4446980709/in/set-721...</a>


+--------+
|  TAGS  |
+--------+
elefante elephant wild animals 