+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jamie.lovelock
Photo URL    : https://www.flickr.com/photos/jamx/3152947991/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 30 14:52:19 GMT+0100 2008
Upload Date  : Wed Dec 31 15:08:51 GMT+0100 2008
Geotag Info  : Latitude:53.227464, Longitude:-2.879061
Views        : 31
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_4535.JPG


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Elephant "Chester Zoo" 