+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ForestWander.com
Photo URL    : https://www.flickr.com/photos/forestwander-nature-pictures/3626857355/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 21 17:05:44 GMT+0200 2009
Upload Date  : Mon Jun 15 04:40:57 GMT+0200 2009
Views        : 2,057
Comments     : 6


+---------+
|  TITLE  |
+---------+
wolf-wildlife_19


+---------------+
|  DESCRIPTION  |
+---------------+
ForestWander Nature Photography
<a href="http://www.ForestWander.com/wildlife" rel="nofollow">www.ForestWander.com/wildlife</a>


+--------+
|  TAGS  |
+--------+
(no tags)