+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : marcwitzel
Photo URL    : https://www.flickr.com/photos/28244776@N07/7655528068/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 26 00:00:32 GMT+0200 2012
Upload Date  : Fri Jul 27 12:39:19 GMT+0200 2012
Views        : 5,217
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wolf (Canis lupus)


+---------------+
|  DESCRIPTION  |
+---------------+
Wolf in der Zoom Erlebniswelt in Gelsenkirchen am 26.07.2012

<a href="http://de.wikipedia.org/wiki/Wolf" rel="nofollow">de.wikipedia.org/wiki/Wolf</a>


+--------+
|  TAGS  |
+--------+
Zoom Gelsenkirchen Zoo Animal Tier Tiere Erlebniswelt Wolf Canis Lupus 