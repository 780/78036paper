+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tom hartley
Photo URL    : https://www.flickr.com/photos/26096505@N02/4599009407/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 26 14:46:47 GMT+0100 2010
Upload Date  : Tue May 11 22:22:09 GMT+0200 2010
Geotag Info  : Latitude:51.862552, Longitude:0.828266
Views        : 389
Comments     : 1


+---------+
|  TITLE  |
+---------+
Grey wolf


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at Colchester Zoo


+--------+
|  TAGS  |
+--------+
(no tags)