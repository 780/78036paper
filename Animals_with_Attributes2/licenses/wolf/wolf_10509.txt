+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stachelbeer
Photo URL    : https://www.flickr.com/photos/mario_storch/6371681981/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 19 14:34:40 GMT+0100 2011
Upload Date  : Sun Nov 20 21:50:23 GMT+0100 2011
Geotag Info  : Latitude:52.921064, Longitude:13.548374
Views        : 193
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Wildpark Schorfheide" Wolf 