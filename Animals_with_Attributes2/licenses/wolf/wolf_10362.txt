+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Airwolfhound
Photo URL    : https://www.flickr.com/photos/24874528@N04/5543990547/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 19 12:26:20 GMT+0100 2011
Upload Date  : Sun Mar 20 22:39:17 GMT+0100 2011
Views        : 5,701
Comments     : 6


+---------+
|  TITLE  |
+---------+
Wolf - Whipsnade Zoo - March 2011


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Wolf Whipsnade Zoo 