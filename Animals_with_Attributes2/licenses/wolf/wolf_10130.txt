+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jonsson.nick
Photo URL    : https://www.flickr.com/photos/nickjonsson/16576763713/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 2 23:49:45 GMT+0200 2014
Upload Date  : Sun Apr 19 11:16:56 GMT+0200 2015
Views        : 691
Comments     : 0


+---------+
|  TITLE  |
+---------+
Eurasian wolf Canis lupus lupus Kingcraig 2014


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)