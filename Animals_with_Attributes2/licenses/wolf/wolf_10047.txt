+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Oregon Department of Fish & Wildlife
Photo URL    : https://www.flickr.com/photos/odfw/17294279475/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 10 09:27:26 GMT+0200 2012
Upload Date  : Tue Apr 28 00:52:06 GMT+0200 2015
Views        : 2,502
Comments     : 0


+---------+
|  TITLE  |
+---------+
0436_OR-13_odfw


+---------------+
|  DESCRIPTION  |
+---------------+
-Photo by Oregon Department of Fish and Wildlife-

On June 10, 2012, ODFW trapped OR-13, a two-year-old wolf of the Wenaha pack, and fitted it with a GPS radio-collar. The black female weighed 85 pounds and was captured in the Wenaha Wildlife Management Unit. <a href="http://dfw.state.or.us/news/2012/June/061212.asp" rel="nofollow">More information.</a> <a href="http://www.dfw.state.or.us/wolves/wenaha_wolf_pack.asp" rel="nofollow">More information on the Wenaha wolf pack</a>.


+--------+
|  TAGS  |
+--------+
mammal canine wolf ODFW Oregon "gray wolf" OR13 adult "radio collar" female "Wenaha Pack" 