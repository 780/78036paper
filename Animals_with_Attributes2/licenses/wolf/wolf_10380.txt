+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : fiskfisk
Photo URL    : https://www.flickr.com/photos/fiskfisk/2672913650/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 14 12:40:16 GMT+0200 2008
Upload Date  : Wed Jul 16 02:08:53 GMT+0200 2008
Views        : 3,100
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wolf Cubs


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
cubs "nordens ark" playing wolf 