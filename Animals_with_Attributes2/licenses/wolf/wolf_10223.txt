+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tswestendorp
Photo URL    : https://www.flickr.com/photos/tswestendorp/5931406282/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 4 11:54:43 GMT+0200 2011
Upload Date  : Tue Jul 12 21:54:39 GMT+0200 2011
Geotag Info  : Latitude:50.867278, Longitude:6.045055
Views        : 849
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_8814


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo building wolf tiger lion monkey chimpansee nature landscape ruin valkenburg maastricht kerkrade castle 