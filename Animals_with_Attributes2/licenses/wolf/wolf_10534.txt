+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DenaliNPS
Photo URL    : https://www.flickr.com/photos/denalinps/9593460321/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 24 20:39:18 GMT+0200 2013
Upload Date  : Mon Aug 26 02:46:41 GMT+0200 2013
Views        : 2,537
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
Just east of Stony Creek (NPS Photo by Jay Elhard)


+--------+
|  TAGS  |
+--------+
(no tags)