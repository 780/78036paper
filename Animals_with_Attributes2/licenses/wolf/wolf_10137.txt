+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rolf Brecher
Photo URL    : https://www.flickr.com/photos/104249543@N07/24354549863/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 12 13:51:17 GMT+0100 2016
Upload Date  : Fri Feb 12 18:39:23 GMT+0100 2016
Views        : 1,476
Comments     : 10


+---------+
|  TITLE  |
+---------+
eye to eye


+---------------+
|  DESCRIPTION  |
+---------------+
weißer Wolf - Auge in Auge - und er hat nicht gezuckt - irgendwie hatte ich das Gefühl er sagt, hier ist die Grenze - kommst du rüber, fress ich dich.


+--------+
|  TAGS  |
+--------+
Blick abschätzig abschätzend taxieren beobachten "Auge in Auge" Wolf "#Rolf Brecher Berlin" 