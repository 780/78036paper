+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : knitsteel
Photo URL    : https://www.flickr.com/photos/knitsteel/2774620403/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 17 02:35:56 GMT+0200 2008
Upload Date  : Mon Aug 18 19:50:16 GMT+0200 2008
Views        : 164
Comments     : 0


+---------+
|  TITLE  |
+---------+
wolf at MN zoo


+---------------+
|  DESCRIPTION  |
+---------------+
son took these photos at the Minnesota Zoo, Aug 2008


+--------+
|  TAGS  |
+--------+
minnesota zoo mykidsart wolf 