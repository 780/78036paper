+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/4334402735/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 30 11:55:42 GMT+0100 2010
Upload Date  : Sat Feb 6 17:37:15 GMT+0100 2010
Geotag Info  : Latitude:47.385711, Longitude:8.573412
Views        : 17,999
Comments     : 19


+---------+
|  TITLE  |
+---------+
Wolf with a piece of wood


+---------------+
|  DESCRIPTION  |
+---------------+
I don't know if they got this piece of wood as a toy, but this wolf seemed to have fun biting it and lifting it. All that in the snow!

Picture taken in the zoo of Zürich.


+--------+
|  TAGS  |
+--------+
wolf canine canid dog mongolian wood playing snow winter zoo zürich switzerland nikon d300 zurich 