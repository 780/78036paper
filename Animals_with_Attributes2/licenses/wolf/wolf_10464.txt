+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : George Self
Photo URL    : https://www.flickr.com/photos/georgeself/3600990231/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 5 12:29:05 GMT+0200 2009
Upload Date  : Sat Jun 6 23:10:50 GMT+0200 2009
Geotag Info  : Latitude:32.245401, Longitude:-111.167693
Views        : 10,221
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
This is a wolf at the Sonoran Desert Museum in Tucson, AZ.


+--------+
|  TAGS  |
+--------+
"Sonoran Desert Museum" Tucson Arizona wolf animal animals 