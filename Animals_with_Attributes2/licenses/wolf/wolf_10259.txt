+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Becker1999
Photo URL    : https://www.flickr.com/photos/becker271/4396306383/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 27 10:12:17 GMT+0100 2010
Upload Date  : Mon Mar 1 03:04:53 GMT+0100 2010
Views        : 474
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mexican Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
Feb. 2010 Flickr Meet-up at the Columbus Zoo


+--------+
|  TAGS  |
+--------+
"Paul's Pic" 2010 "columbus zoo" flickr meetup 