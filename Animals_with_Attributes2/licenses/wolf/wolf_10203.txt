+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aardwolf6886
Photo URL    : https://www.flickr.com/photos/132734449@N05/19126108855/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 20 13:26:28 GMT+0200 2014
Upload Date  : Wed Jun 24 22:24:23 GMT+0200 2015
Views        : 114
Comments     : 0


+---------+
|  TITLE  |
+---------+
Iberian Wolf - Canis lupus signatus


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
SLT-A77V 50-500mm 