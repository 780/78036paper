+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ahisgett
Photo URL    : https://www.flickr.com/photos/hisgett/5017626955/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 19 13:24:29 GMT+0200 2010
Upload Date  : Thu Sep 23 19:15:00 GMT+0200 2010
Geotag Info  : Latitude:52.379208, Longitude:-2.286293
Views        : 152
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Safari Park Animal 