+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Oregon Department of Fish & Wildlife
Photo URL    : https://www.flickr.com/photos/odfw/17292426402/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 11 00:00:00 GMT+0100 2011
Upload Date  : Tue Apr 28 00:52:49 GMT+0200 2015
Views        : 3,429
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wenaha_pup_2011_IMG_0370enh


+---------------+
|  DESCRIPTION  |
+---------------+
-Photo by the Oregon Department of Fish and Wildlife-

A pup from northeast Oregon’s Wenaha pack that was born in spring 2011. Image taken by remote camera on Dec. 11, 2011. <a href="http://www.dfw.state.or.us/news/2011/december/122311.asp" rel="nofollow">More information</a>.


+--------+
|  TAGS  |
+--------+
mammal canine wolf ODFW Oregon pup "gray wolf" "Wenaha Pack" 