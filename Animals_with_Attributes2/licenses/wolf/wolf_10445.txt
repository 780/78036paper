+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : arne.list
Photo URL    : https://www.flickr.com/photos/arne-list/2361156740/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 22 15:33:38 GMT+0100 2008
Upload Date  : Tue Mar 25 12:55:24 GMT+0100 2008
Views        : 4,313
Comments     : 3


+---------+
|  TITLE  |
+---------+
Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Eekholt "Tierpark Eekholt" Wolf 