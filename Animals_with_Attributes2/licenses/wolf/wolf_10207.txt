+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rob Gallop
Photo URL    : https://www.flickr.com/photos/robgallop/2223176883/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 26 11:45:37 GMT+0100 2008
Upload Date  : Sun Jan 27 20:24:11 GMT+0100 2008
Views        : 721
Comments     : 2


+---------+
|  TITLE  |
+---------+
IMG_0340


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wolf wolves beenham sanctuary "UK Wolf Conservation Trust" Reading berkshire 