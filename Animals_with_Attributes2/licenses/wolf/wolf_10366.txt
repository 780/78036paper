+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Becker1999
Photo URL    : https://www.flickr.com/photos/becker271/2251728730/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 9 10:12:45 GMT+0100 2008
Upload Date  : Sat Feb 9 01:39:44 GMT+0100 2008
Views        : 663
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wolf 02


+---------------+
|  DESCRIPTION  |
+---------------+
Columbus Zoo - Wolf


+--------+
|  TAGS  |
+--------+
becker.271 "columbus zoo" Februrary 2008 