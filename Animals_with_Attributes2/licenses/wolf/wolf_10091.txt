+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Red Wolf Recovery Program
Photo URL    : https://www.flickr.com/photos/trackthepack/7747761838/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 29 02:17:07 GMT+0100 2008
Upload Date  : Thu Aug 9 20:03:19 GMT+0200 2012
Views        : 2,086
Comments     : 0


+---------+
|  TITLE  |
+---------+
Captive male red wolf


+---------------+
|  DESCRIPTION  |
+---------------+
Photo Credit:  Ryan Nordsven/USFWS


+--------+
|  TAGS  |
+--------+
(no tags)