+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sam Foles
Photo URL    : https://www.flickr.com/photos/samfoles/8887897597/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 5 10:22:05 GMT+0200 2013
Upload Date  : Thu May 30 10:56:42 GMT+0200 2013
Views        : 1,242
Comments     : 0


+---------+
|  TITLE  |
+---------+
Grey Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
flickrandroidapp:filter=none 