+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dennis from Atlanta
Photo URL    : https://www.flickr.com/photos/dennis_matheson/4332300009/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Feb 5 18:40:45 GMT+0100 2009
Upload Date  : Fri Feb 5 18:49:58 GMT+0100 2010
Views        : 59,606
Comments     : 5


+---------+
|  TITLE  |
+---------+
Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
This is one of the wolves at the Grizzly and Wolf Discovery center in West Yellowstone.


+--------+
|  TAGS  |
+--------+
wolf wolves 