+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/10828420094/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 14 16:38:14 GMT+0200 2013
Upload Date  : Wed Nov 13 01:00:16 GMT+0100 2013
Geotag Info  : Latitude:47.547509, Longitude:7.578211
Views        : 9,001
Comments     : 0


+---------+
|  TITLE  |
+---------+
Wolf in light and shadow


+---------------+
|  DESCRIPTION  |
+---------------+
The white wolf of the Basel zoo walking on the rocks, between light and shadow...


+--------+
|  TAGS  |
+--------+
wolf canid dog male white walking pacing rocks light shadow portrait zolli zoo basel switzerland nikon d4 