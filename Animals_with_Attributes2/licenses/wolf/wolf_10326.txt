+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Lil Shepherd
Photo URL    : https://www.flickr.com/photos/lilshepherd/3362935375/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Mar 17 14:09:58 GMT+0100 2009
Upload Date  : Tue Mar 17 20:56:53 GMT+0100 2009
Views        : 6,459
Comments     : 0


+---------+
|  TITLE  |
+---------+
Timber Wolf Head


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wolf 