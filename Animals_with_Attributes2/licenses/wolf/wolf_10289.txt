+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : HyperLemon
Photo URL    : https://www.flickr.com/photos/hyperlemon/108124/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 16 13:31:26 GMT+0200 2004
Upload Date  : Sat Jul 24 16:51:33 GMT+0200 2004
Views        : 625
Comments     : 2


+---------+
|  TITLE  |
+---------+
pondering wolf


+---------------+
|  DESCRIPTION  |
+---------------+
Picture taken with a friends Nikon F60 with Sigma 28-105mm, lowest possible F-Number (which wasn't that low), ISO 400


+--------+
|  TAGS  |
+--------+
wolf 