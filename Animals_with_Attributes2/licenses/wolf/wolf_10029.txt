+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/16819297432/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 1 11:41:05 GMT+0100 2014
Upload Date  : Sun Mar 15 11:00:16 GMT+0100 2015
Geotag Info  : Latitude:47.734849, Longitude:7.350239
Views        : 16,666
Comments     : 3


+---------+
|  TITLE  |
+---------+
Profile of a gray wolf


+---------------+
|  DESCRIPTION  |
+---------------+
I like this profile portrait of a black (or gray) wolf!


+--------+
|  TAGS  |
+--------+
gray black portrait face profile "open mouth" pretty canadian wolf timberwolf dog canid canine mulhouse zoo france nikon d4 