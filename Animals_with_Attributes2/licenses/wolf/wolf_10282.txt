+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : generalising
Photo URL    : https://www.flickr.com/photos/shimgray/4823344335/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 13 15:48:51 GMT+0200 2010
Upload Date  : Sat Jul 24 16:26:16 GMT+0200 2010
Views        : 166
Comments     : 0


+---------+
|  TITLE  |
+---------+
P1290122


+---------------+
|  DESCRIPTION  |
+---------------+
Wolves sleeping


+--------+
|  TAGS  |
+--------+
copenhagen "copenhagen zoo" zoo wolf 