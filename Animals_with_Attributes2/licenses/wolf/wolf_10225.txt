+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : RolandStuehmer
Photo URL    : https://www.flickr.com/photos/rolandstuehmer/15530542992/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 3 01:24:13 GMT+0200 2009
Upload Date  : Tue Oct 14 00:02:17 GMT+0200 2014
Views        : 985
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_10129_lr


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
3 Animals Places "United States of America" snow vacation wolf 