+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Caninest
Photo URL    : https://www.flickr.com/photos/caninest/4394675343/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 26 14:27:10 GMT+0100 2005
Upload Date  : Sun Feb 28 17:26:30 GMT+0100 2010
Views        : 19,666
Comments     : 4


+---------+
|  TITLE  |
+---------+
Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
This image is in the public domain. Learn more about the different wolves of the world here: <a href="http://www.caninest.com/types-of-wolf/" rel="nofollow">Types of Wolf</a>


+--------+
|  TAGS  |
+--------+
wolf wild snow 