+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ed Coyle Photography
Photo URL    : https://www.flickr.com/photos/joxur223/7451670242/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 11 17:35:19 GMT+0200 2012
Upload Date  : Wed Jun 27 03:30:32 GMT+0200 2012
Geotag Info  : Latitude:44.662239, Longitude:-111.101112
Views        : 4,916
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gray Wolf - Yellowstone


+---------------+
|  DESCRIPTION  |
+---------------+
A Gray Wolf at the Grizzly and Wolf Discovery Center.


+--------+
|  TAGS  |
+--------+
gray wolf yellowstone "grizzly and wolf discovery center" national park nature animal wild saliva eyes 