+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jjandames
Photo URL    : https://www.flickr.com/photos/jjandames/6615115659/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 31 10:04:44 GMT+0100 2011
Upload Date  : Sun Jan 1 22:47:18 GMT+0100 2012
Views        : 451
Comments     : 0


+---------+
|  TITLE  |
+---------+
Red Maned Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"North Carolina" Asheboro zoo 