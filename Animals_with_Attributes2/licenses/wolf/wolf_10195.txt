+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daverooneyca
Photo URL    : https://www.flickr.com/photos/daverooney/5681593883/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 14 16:14:29 GMT+0100 2011
Upload Date  : Tue May 3 02:23:14 GMT+0200 2011
Views        : 181
Comments     : 0


+---------+
|  TITLE  |
+---------+
Haliburton Forest Wolf Centre - March 2011 - 127


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wolf haliburton forest 