+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Red Wolf Recovery Program
Photo URL    : https://www.flickr.com/photos/trackthepack/6189370229/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Oct 28 00:49:38 GMT+0100 2008
Upload Date  : Tue Sep 27 21:16:52 GMT+0200 2011
Views        : 1,917
Comments     : 0


+---------+
|  TITLE  |
+---------+
Red Wolf Feeding


+---------------+
|  DESCRIPTION  |
+---------------+
Photo credit: R. Nordsven/USFWS


+--------+
|  TAGS  |
+--------+
(no tags)