+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USFWS Headquarters
Photo URL    : https://www.flickr.com/photos/usfwshq/14706017133/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 5 21:31:40 GMT+0200 2012
Upload Date  : Fri Jul 18 23:19:29 GMT+0200 2014
Views        : 2,274
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gray wolf (Canis lupis) with radio collar and ear tags


+---------------+
|  DESCRIPTION  |
+---------------+
Image credit: Hilary Cooley


+--------+
|  TAGS  |
+--------+
wolf wolves "gray wolf" 