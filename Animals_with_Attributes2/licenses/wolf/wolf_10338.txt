+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Caninest
Photo URL    : https://www.flickr.com/photos/caninest/4394641125/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 28 08:13:55 GMT+0100 2010
Upload Date  : Sun Feb 28 17:13:55 GMT+0100 2010
Views        : 21,706
Comments     : 1


+---------+
|  TITLE  |
+---------+
Two Grey Wolves


+---------------+
|  DESCRIPTION  |
+---------------+
This image is in the public domain. Learn more about the different wolves of the world here: <a href="http://www.caninest.com/types-of-wolf/" rel="nofollow">Types of Wolf</a>


+--------+
|  TAGS  |
+--------+
wolf wolves "grey wolf" canine 