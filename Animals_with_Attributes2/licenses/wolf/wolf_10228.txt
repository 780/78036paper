+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sharon Mollerus
Photo URL    : https://www.flickr.com/photos/clairity/18350523893/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 19 13:29:21 GMT+0200 2015
Upload Date  : Sat Jun 20 03:17:48 GMT+0200 2015
Geotag Info  : Latitude:47.905719, Longitude:-91.827764
Views        : 657
Comments     : 1


+---------+
|  TITLE  |
+---------+
International Wolf Center, Ely


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Ely Minnesota "United States" 