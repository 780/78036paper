+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Arne_von_Brill
Photo URL    : https://www.flickr.com/photos/countrybob69/6648170211/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 6 14:22:18 GMT+0100 2012
Upload Date  : Fri Jan 6 19:08:06 GMT+0100 2012
Views        : 1,740
Comments     : 0


+---------+
|  TITLE  |
+---------+
Grey Wolf


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wolf grey 