+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Arctic Wolf Pictures
Photo URL    : https://www.flickr.com/photos/arcticwoof/20476258458/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 17 22:28:44 GMT+0200 2015
Upload Date  : Mon Aug 17 22:32:10 GMT+0200 2015
Views        : 291
Comments     : 0


+---------+
|  TITLE  |
+---------+
p[l]u[m]p


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wolf 