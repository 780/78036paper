+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/13510155894/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 30 14:54:38 GMT+0100 2013
Upload Date  : Sun Mar 30 15:00:06 GMT+0200 2014
Geotag Info  : Latitude:47.050651, Longitude:8.554229
Views        : 25,713
Comments     : 7


+---------+
|  TITLE  |
+---------+
Jumping wolf


+---------------+
|  DESCRIPTION  |
+---------------+
The wolves playing together!


+--------+
|  TAGS  |
+--------+
playing jumping fun two together wolf canine canid brown dog winter "tierpark goldau" tierpark zoo arth switzerland nikon d4 