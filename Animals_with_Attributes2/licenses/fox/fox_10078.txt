+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jfeuchter
Photo URL    : https://www.flickr.com/photos/jennerosity/17547272436/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 2 22:46:56 GMT+0200 2015
Upload Date  : Tue May 12 23:27:32 GMT+0200 2015
Views        : 341
Comments     : 0


+---------+
|  TITLE  |
+---------+
Arctic fox running (2)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo arctic fox yvr 