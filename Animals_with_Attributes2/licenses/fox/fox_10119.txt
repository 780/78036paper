+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pipilongstockings
Photo URL    : https://www.flickr.com/photos/68732830@N06/8582742211/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 28 19:59:36 GMT+0200 2012
Upload Date  : Sat Mar 23 20:24:49 GMT+0100 2013
Views        : 687
Comments     : 29


+---------+
|  TITLE  |
+---------+
Compare the Wiley.com Juvenile Wiley


+---------------+
|  DESCRIPTION  |
+---------------+
I put this up to show the difference in age of the facial features of Wiley, even foxes grow old  and get that worried brow lol
<a href="http://www.flickr.com/photos/68732830@N06/8566228944/"></a>


+--------+
|  TAGS  |
+--------+
(no tags)