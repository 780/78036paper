+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : B A Bowen Photography
Photo URL    : https://www.flickr.com/photos/riverbk/4208140412/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 21 00:39:33 GMT+0100 2009
Upload Date  : Wed Dec 23 05:59:33 GMT+0100 2009
Views        : 284
Comments     : 0


+---------+
|  TITLE  |
+---------+
Red Fox


+---------------+
|  DESCRIPTION  |
+---------------+
Homossassa Springs State Park
Homossassa, Florida


+--------+
|  TAGS  |
+--------+
"Homossassa Christmas" "Homossassa Springs State Park" Wildlife "Citrus County" Manatee "Florida Panther" "Florida State Park" 