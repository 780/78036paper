+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Marie Hale
Photo URL    : https://www.flickr.com/photos/15016964@N02/5662097343/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 30 14:37:04 GMT+0200 2010
Upload Date  : Wed Apr 27 23:44:44 GMT+0200 2011
Views        : 3,783
Comments     : 0


+---------+
|  TITLE  |
+---------+
European red fox


+---------------+
|  DESCRIPTION  |
+---------------+
European red fox - taken at British Wildlife Centre on 30th May 2010


+--------+
|  TAGS  |
+--------+
fox 