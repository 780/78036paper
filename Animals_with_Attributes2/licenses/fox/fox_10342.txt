+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Henrik Moltke
Photo URL    : https://www.flickr.com/photos/henrikmoltke/2741194266/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 3 07:45:47 GMT+0200 2008
Upload Date  : Thu Aug 7 11:07:26 GMT+0200 2008
Views        : 377
Comments     : 1


+---------+
|  TITLE  |
+---------+
friendly fox


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Hokkaido Japan cycling 