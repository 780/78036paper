+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eekim
Photo URL    : https://www.flickr.com/photos/eekim/19660649050/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 19 17:32:57 GMT+0200 2015
Upload Date  : Mon Jul 20 07:18:02 GMT+0200 2015
Geotag Info  : Latitude:37.553986, Longitude:-122.091767
Views        : 661
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gray Fox


+---------------+
|  DESCRIPTION  |
+---------------+
This little guy was hanging out with a friend on the trail at Coyote Hills Regional Park. He didn't seem to mind me much. I got pretty close to get this picture.


+--------+
|  TAGS  |
+--------+
"Coyote Hills Regional Park" fox "gray fox" marsh Fremont California "United States" US 