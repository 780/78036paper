+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DenaliNPS
Photo URL    : https://www.flickr.com/photos/denalinps/5302090111/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 2 02:02:09 GMT+0200 2009
Upload Date  : Wed Dec 29 07:20:23 GMT+0100 2010
Views        : 8,044
Comments     : 0


+---------+
|  TITLE  |
+---------+
Fox


+---------------+
|  DESCRIPTION  |
+---------------+
(NPS Photo/Kent Miller)

Check out the official Denali Facebook, Twitter and YouTube pages:

Like us on Facebook: <a href="http://www.facebook.com/DenaliNPS" rel="nofollow">www.facebook.com/DenaliNPS</a>

Follow us on Twitter: <a href="http://www.twitter.com/DenaliNPS" rel="nofollow">www.twitter.com/DenaliNPS</a>

Denali YouTube Channel: <a href="http://www.youtube.com/denalinps" rel="nofollow">www.youtube.com/denalinps</a>


+--------+
|  TAGS  |
+--------+
"Denali back country" fox 