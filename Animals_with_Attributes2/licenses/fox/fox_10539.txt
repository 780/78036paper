+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Intrinsic3141
Photo URL    : https://www.flickr.com/photos/93843039@N02/8880069595/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 29 11:15:39 GMT+0200 2013
Upload Date  : Wed May 29 19:03:19 GMT+0200 2013
Geotag Info  : Latitude:40.695682, Longitude:-73.268661
Views        : 1,638
Comments     : 0


+---------+
|  TITLE  |
+---------+
Red Fox


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
woodland creature red fox cute new york suffolk parks county wildlife nature mammal kit 