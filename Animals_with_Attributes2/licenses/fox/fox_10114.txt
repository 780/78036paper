+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : andyrusch
Photo URL    : https://www.flickr.com/photos/asrusch/9403248317/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 27 12:37:20 GMT+0200 2013
Upload Date  : Wed Jul 31 03:24:07 GMT+0200 2013
Views        : 329
Comments     : 0


+---------+
|  TITLE  |
+---------+
Channel Islands National Park


+---------------+
|  DESCRIPTION  |
+---------------+
Santa Cruz Island foxes


+--------+
|  TAGS  |
+--------+
channel islands national park Santa Cruz island fox 