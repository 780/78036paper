+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rusty Clark - On the Air M-F 8am-noon
Photo URL    : https://www.flickr.com/photos/rusty_clark/19185194954/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 9 12:27:06 GMT+0200 2015
Upload Date  : Sat Jul 18 22:19:27 GMT+0200 2015
Views        : 57
Comments     : 0


+---------+
|  TITLE  |
+---------+
Day 6: Provincetown


+---------------+
|  DESCRIPTION  |
+---------------+
red fox


+--------+
|  TAGS  |
+--------+
(no tags)