+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tehbieber
Photo URL    : https://www.flickr.com/photos/tehbieber/9106501930/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 21 12:58:38 GMT+0200 2013
Upload Date  : Sat Jun 22 08:53:55 GMT+0200 2013
Geotag Info  : Latitude:37.484564, Longitude:-122.148163
Views        : 2,326
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mirror Foxes I


+---------------+
|  DESCRIPTION  |
+---------------+
Two of the kits sitting on the deck together.


+--------+
|  TAGS  |
+--------+
fox foxes kit kits facebook 