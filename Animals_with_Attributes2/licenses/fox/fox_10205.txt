+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : dionhinchcliffe
Photo URL    : https://www.flickr.com/photos/dionhinchcliffe/18392030425/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 2 10:16:22 GMT+0200 2015
Upload Date  : Tue Jun 2 20:15:46 GMT+0200 2015
Views        : 107
Comments     : 0


+---------+
|  TITLE  |
+---------+
Fox in the front yard at extreme telephoto range


+---------------+
|  DESCRIPTION  |
+---------------+
Shot with a Sony NEX-6.


+--------+
|  TAGS  |
+--------+
(no tags)