+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : G. Frank Peterson
Photo URL    : https://www.flickr.com/photos/garpete/5993918869/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 13 18:31:42 GMT+0200 2011
Upload Date  : Sun Jul 31 18:01:29 GMT+0200 2011
Geotag Info  : Latitude:63.446058, Longitude:-150.717315
Views        : 888
Comments     : 0


+---------+
|  TITLE  |
+---------+
2011 Red Fox in Denali-1.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
2011 Alaska "Denali National Park" Mammal "Red Fox" 