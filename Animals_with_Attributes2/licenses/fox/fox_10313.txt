+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mrs Airwolfhound
Photo URL    : https://www.flickr.com/photos/amylloyd/17326824126/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 22 10:08:14 GMT+0200 2015
Upload Date  : Sun May 3 12:54:26 GMT+0200 2015
Views        : 884
Comments     : 5


+---------+
|  TITLE  |
+---------+
Fox


+---------------+
|  DESCRIPTION  |
+---------------+
British Wildlife centre


+--------+
|  TAGS  |
+--------+
"British Wildlife centre" wildlife british canon fox teeth furry bwc 