+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tsaiproject
Photo URL    : https://www.flickr.com/photos/tsaiproject/24288129953/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 9 07:26:01 GMT+0100 2016
Upload Date  : Tue Feb 9 13:27:18 GMT+0100 2016
Views        : 673
Comments     : 3


+---------+
|  TITLE  |
+---------+
Red Fox


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Canon 6D" "Canon 70-200mm f/2.8 II" 