+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neticola
Photo URL    : https://www.flickr.com/photos/torrelodones/4521108013/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 11 13:49:22 GMT+0200 2010
Upload Date  : Thu Apr 15 22:45:23 GMT+0200 2010
Geotag Info  : Latitude:40.537579, Longitude:-4.127683
Views        : 1,946
Comments     : 14


+---------+
|  TITLE  |
+---------+
Taking sun - Fox


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://en.wikipedia.org/wiki/Red_Fox" rel="nofollow">Wikipedia</a>
<i>Nikkor 70-300vr@300mm| ƒ8 | 1/1000s | ISO200</i>
Software: CNX2


+--------+
|  TAGS  |
+--------+
fox zorro vulpes iberian red rojo neticola photography "neticola photography" 