+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Suzanne McLeod
Photo URL    : https://www.flickr.com/photos/suzanne_mcleod/7807486476/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 11 09:15:26 GMT+0200 2012
Upload Date  : Sat Aug 18 13:47:21 GMT+0200 2012
Views        : 129
Comments     : 0


+---------+
|  TITLE  |
+---------+
P1000855


+---------------+
|  DESCRIPTION  |
+---------------+
Fox cubs


+--------+
|  TAGS  |
+--------+
(no tags)