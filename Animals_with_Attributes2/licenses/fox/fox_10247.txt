+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Svadilfari
Photo URL    : https://www.flickr.com/photos/22280677@N07/2922856209/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 28 02:23:52 GMT+0200 2008
Upload Date  : Wed Oct 8 05:31:53 GMT+0200 2008
Views        : 851
Comments     : 0


+---------+
|  TITLE  |
+---------+
Fox


+---------------+
|  DESCRIPTION  |
+---------------+
A taxidermy fox at the Bass Pro Shops in Foxboro, Massachusetts


+--------+
|  TAGS  |
+--------+
Foxboro MA "Foxboro Massachusetts" "Foxboro Mass" "Foxboro MA" Massachusetts "Foxborough Massachusetts" "Foxborough Mass" "Foxborough MA" "Bass Pro Shops" "Bass Pro" Store Shop Business Display Fox Taxidermy Stuffed Animal 