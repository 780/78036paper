+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Chabot
Photo URL    : https://www.flickr.com/photos/ericdelevis/5801971478/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 5 14:43:07 GMT+0200 2011
Upload Date  : Mon Jun 6 00:00:50 GMT+0200 2011
Geotag Info  : Latitude:46.856786, Longitude:-70.782358
Views        : 382
Comments     : 0


+---------+
|  TITLE  |
+---------+
2011-06-05 - Renards - 01


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
renard fox 