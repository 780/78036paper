+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/8736489065/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 5 16:24:57 GMT+0200 2013
Upload Date  : Tue May 14 05:34:20 GMT+0200 2013
Geotag Info  : Latitude:42.462697, Longitude:-71.093636
Views        : 1,819
Comments     : 0


+---------+
|  TITLE  |
+---------+
Arctic Fox with Yellow Eye


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
stone zoo arctic fox yellow eye 