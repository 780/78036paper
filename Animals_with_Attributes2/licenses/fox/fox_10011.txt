+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Peter G Trimming
Photo URL    : https://www.flickr.com/photos/peter-trimming/5615475797/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 12 13:27:31 GMT+0200 2011
Upload Date  : Wed Apr 13 12:37:55 GMT+0200 2011
Views        : 1,755
Comments     : 2


+---------+
|  TITLE  |
+---------+
A Surprised 'Pickles'


+---------------+
|  DESCRIPTION  |
+---------------+
At the British Wildlife Centre, Newchapel, Surrey; 'Pickles'  is surprised by a noise from outside her enclosure.


+--------+
|  TAGS  |
+--------+
British Wildlife Centre Newchapel Surrey 2011 Pickles Fox Vixen Vulpes Peter Trimming Sigma 300mm EX DG 