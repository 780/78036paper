+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/8191762822/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 29 14:15:49 GMT+0200 2012
Upload Date  : Fri Nov 16 21:00:11 GMT+0100 2012
Geotag Info  : Latitude:47.291907, Longitude:8.529338
Views        : 15,375
Comments     : 132


+---------+
|  TITLE  |
+---------+
Another fox portrait


+---------------+
|  DESCRIPTION  |
+---------------+
Next fox portrait, there will be many more!


+--------+
|  TAGS  |
+--------+
portait face close cute beautiful grass fox vulpine canid canine red wildpark wildnisspark langenberg park zürich switzerland nikon d4 