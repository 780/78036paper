+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Graham Ó Síodhacháin
Photo URL    : https://www.flickr.com/photos/funkdooby/13991265049/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 13 13:19:19 GMT+0200 2014
Upload Date  : Tue May 13 17:23:29 GMT+0200 2014
Views        : 531
Comments     : 0


+---------+
|  TITLE  |
+---------+
Siblings


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"fox cubs" "margate cemetery" wildlife nature 