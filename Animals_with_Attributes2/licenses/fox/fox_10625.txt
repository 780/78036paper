+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : CarolineG2011
Photo URL    : https://www.flickr.com/photos/cgranycome/7402835032/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 4 11:19:34 GMT+0200 2012
Upload Date  : Tue Jun 19 20:20:27 GMT+0200 2012
Views        : 2,952
Comments     : 2


+---------+
|  TITLE  |
+---------+
Urban Mother Fox


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Red Fox London British wildlife 