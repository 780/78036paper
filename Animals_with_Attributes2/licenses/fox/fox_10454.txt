+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Suzanne McLeod
Photo URL    : https://www.flickr.com/photos/suzanne_mcleod/7807481612/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 14 09:33:22 GMT+0200 2012
Upload Date  : Sat Aug 18 13:47:29 GMT+0200 2012
Views        : 136
Comments     : 0


+---------+
|  TITLE  |
+---------+
P1000863


+---------------+
|  DESCRIPTION  |
+---------------+
Fox cubs


+--------+
|  TAGS  |
+--------+
(no tags)