+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Arctic Wolf Pictures
Photo URL    : https://www.flickr.com/photos/arcticwoof/6959408442/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 28 15:26:55 GMT+0100 2008
Upload Date  : Mon Apr 23 11:04:53 GMT+0200 2012
Views        : 1,948
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar fox


+---------------+
|  DESCRIPTION  |
+---------------+
Cuteness overload


+--------+
|  TAGS  |
+--------+
animals fox white polar cute vulpes 