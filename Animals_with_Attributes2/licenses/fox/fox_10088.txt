+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Will_89
Photo URL    : https://www.flickr.com/photos/37428634@N04/5334161463/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 7 07:59:11 GMT+0100 2011
Upload Date  : Sat Jan 8 02:44:38 GMT+0100 2011
Views        : 2,983
Comments     : 9


+---------+
|  TITLE  |
+---------+
Arctic Fox


+---------------+
|  DESCRIPTION  |
+---------------+
Shot from Valley Zoo, Edmonton, AB


+--------+
|  TAGS  |
+--------+
(no tags)