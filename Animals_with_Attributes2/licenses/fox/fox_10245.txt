+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : byrion
Photo URL    : https://www.flickr.com/photos/byrion/2868469339/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 19 01:38:02 GMT+0200 2008
Upload Date  : Fri Sep 19 02:38:02 GMT+0200 2008
Views        : 151
Comments     : 0


+---------+
|  TITLE  |
+---------+
Fox


+---------------+
|  DESCRIPTION  |
+---------------+
He appeared right up to the window where I was, but I had to run and grab the camera - so didn't reach him till he was at the bottom of the garden. - It's majorly cropped.


+--------+
|  TAGS  |
+--------+
fox 