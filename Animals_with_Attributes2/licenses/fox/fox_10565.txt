+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : oldandsolo
Photo URL    : https://www.flickr.com/photos/shankaronline/16257742139/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 1 00:00:00 GMT+0100 2015
Upload Date  : Wed Feb 4 19:22:34 GMT+0100 2015
Views        : 2,011
Comments     : 0


+---------+
|  TITLE  |
+---------+
Rueppell's/ Sand Fox


+---------------+
|  DESCRIPTION  |
+---------------+
The Rueppell's Fox, or Sand Fox, stands up to take flight as a group of unruly kids then barged in. The Rüppell's/ Sand fox is a small fox, measuring 26 to 29&quot; in total length, which includes a 11 to 12&quot; tail. Males appear somewhat larger than females, but both sexes are reported to have an average weight of 1.7 kg. The coat is sandy in colour. The Rüppell's/ Sand Fox has fur on the pads on its feet that possibly helps distribute their weight and move easily on sand, and keeps the hot sand from burning their feet.Similar to other desert dwelling foxes, the Rüppell's fox has large ears to cool it off. Rüppell's/ Sand foxes are omnivores, and with a diet that varies considerably depending on what is locally available. In some regions, they are reported to be mainly insectivorous, especially feeding on beetles and orthopterans, while in others, small mammals, lizards, and birds form a larger part of their diet. Plants eaten include grass and desert succulents, along with fruit such as dates. They have also been known to scavenge from human garbage. (Emirates Park Zoo, Samha, Abu Dhabi, Jan. 2015)


+--------+
|  TAGS  |
+--------+
Fauna "Abu Dhabi" UAE "United Arab Emirates" "Samha Abu Dhabi" zoo "zoological gardens" "Emirates Park Zoo" "small animals" omnivore canine fox "Rüppell's Fox" "Vulpes rueppellii" "Rueppell's Fox" "Sand Fox" 