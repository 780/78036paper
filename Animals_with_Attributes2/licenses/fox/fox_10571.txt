+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SunWALKer - mainly street photography
Photo URL    : https://www.flickr.com/photos/sunwalk999/6873914430/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 24 08:38:05 GMT+0100 2012
Upload Date  : Tue Mar 27 07:34:45 GMT+0200 2012
Views        : 3,049
Comments     : 2


+---------+
|  TITLE  |
+---------+
Seaside Fox


+---------------+
|  DESCRIPTION  |
+---------------+
This urban-ised fox came early-ish one morning to laze around on the promenade wall.  He wouldn't look up at me as I hung out of the 4th floor window so I whistled him.  This was the laconic look he gave me!


+--------+
|  TAGS  |
+--------+
fox "urban fox" seaside animals promenade UK Sussex England Brighton fauna "flora & fauna" 