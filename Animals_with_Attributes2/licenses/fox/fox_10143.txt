+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Paul J Everett
Photo URL    : https://www.flickr.com/photos/paul_everett82/1857973991/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 3 10:26:22 GMT+0100 2007
Upload Date  : Sun Nov 4 17:45:49 GMT+0100 2007
Views        : 216
Comments     : 0


+---------+
|  TITLE  |
+---------+
eagle creek stuffed fox


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
eaglecreek indianapolis a700 "paul j everett" 