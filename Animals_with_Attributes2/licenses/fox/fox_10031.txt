+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Peter G Trimming
Photo URL    : https://www.flickr.com/photos/peter-trimming/7357679744/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 9 11:36:17 GMT+0200 2012
Upload Date  : Sun Jun 10 16:03:45 GMT+0200 2012
Views        : 2,187
Comments     : 5


+---------+
|  TITLE  |
+---------+
'Flo'


+---------------+
|  DESCRIPTION  |
+---------------+
Seen at the British Wildlife Centre, Newchapel, Surrey. 'Flo' relaxing, after the morning Keeper's talk.


+--------+
|  TAGS  |
+--------+
British Wildlife Centre Newchapel Surrey Fox Vixen Vulpes 2012 Flo Peter Trimming 