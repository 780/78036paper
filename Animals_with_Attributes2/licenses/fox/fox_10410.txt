+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bortescristian
Photo URL    : https://www.flickr.com/photos/bortescristian/2958753889/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 10 18:33:38 GMT+0200 2008
Upload Date  : Mon Oct 20 22:27:15 GMT+0200 2008
Geotag Info  : Latitude:51.474446, Longitude:-0.297735
Views        : 542
Comments     : 0


+---------+
|  TITLE  |
+---------+
Kew Gardens - London - September 2008


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
bortescristian cristianbortes canon s3 is powershot 2008 september septembrie autumn toamna Londres Großbritannien Anglaterra Regne ロンドン Grande-Bretagne Storbritannien لندن Лёндан Ло̀ндон Londýn Inglaterra Reino Unido Regno Unito Londen Verenigd Koninkrijk Anglicka 倫敦 伦敦 londra uk united kingdom great britain regatul al marii britanii marea britanie anglia england london kew gardens gradina botanica royal queen queen's fox vulpe 