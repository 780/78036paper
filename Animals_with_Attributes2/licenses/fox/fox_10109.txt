+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Joanne Goldby
Photo URL    : https://www.flickr.com/photos/jovamp/8697250676/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 28 18:14:21 GMT+0200 2013
Upload Date  : Tue Apr 30 23:29:54 GMT+0200 2013
Views        : 565
Comments     : 0


+---------+
|  TITLE  |
+---------+
BWC (22 of 64).jpg


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"British Wildlife Centre" Fox "Red Fox" 