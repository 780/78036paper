+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ejoui15
Photo URL    : https://www.flickr.com/photos/ejoui15/12050501263/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 19 16:42:11 GMT+0100 2014
Upload Date  : Mon Jan 20 14:55:38 GMT+0100 2014
Views        : 4,031
Comments     : 0


+---------+
|  TITLE  |
+---------+
red fox at Bearizona


+---------------+
|  DESCRIPTION  |
+---------------+
I rented a Nikon TC-20E teleconverter this weekend to try it out.  Here is a picture of a red fox taken near the Grand Canyon with a 70-200 and the teleconverter.


+--------+
|  TAGS  |
+--------+
fox Arizona teleconverter Nikon nature red animal sleep Bearizona "Arizona Passages" 