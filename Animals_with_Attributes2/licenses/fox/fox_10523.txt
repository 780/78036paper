+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Caninest
Photo URL    : https://www.flickr.com/photos/caninest/4395443980/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 17 10:20:37 GMT+0200 2009
Upload Date  : Sun Feb 28 17:26:51 GMT+0100 2010
Views        : 1,742
Comments     : 0


+---------+
|  TITLE  |
+---------+
Red Fox


+---------------+
|  DESCRIPTION  |
+---------------+
This image is in the public domain. Learn more about the different foxes of the world here: <a href="http://www.caninest.com/the-canidae-family-foxes-and-basal/" rel="nofollow">Canidae Family - Foxes &amp; Basal</a>


+--------+
|  TAGS  |
+--------+
"red fox" fox cute wild 