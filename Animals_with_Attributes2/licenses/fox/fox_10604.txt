+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/7908097800/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 3 11:06:28 GMT+0200 2012
Upload Date  : Sat Sep 1 21:00:06 GMT+0200 2012
Geotag Info  : Latitude:47.107873, Longitude:8.262104
Views        : 6,641
Comments     : 2


+---------+
|  TITLE  |
+---------+
The fennec just woke up!


+---------------+
|  DESCRIPTION  |
+---------------+
A sleepy cute fennec fox on the roof of the small house in the outside enclosure...


+--------+
|  TAGS  |
+--------+
fennec fox canid desert sleepy "waking up" cute ears sitting roof portrait "tonis zoo" zoo rothenburg lucerne luzern switzerland nikon d700 