+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nillamaria
Photo URL    : https://www.flickr.com/photos/nillanilzon/14458674516/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 20 17:58:11 GMT+0200 2014
Upload Date  : Sun Jun 22 18:59:14 GMT+0200 2014
Geotag Info  : Latitude:62.919609, Longitude:18.455915
Views        : 3,353
Comments     : 0


+---------+
|  TITLE  |
+---------+
The fox


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
räv fox 