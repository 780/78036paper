+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : caligula1995
Photo URL    : https://www.flickr.com/photos/pussreboots/14261348539/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 17 13:29:22 GMT+0200 2014
Upload Date  : Wed Jun 18 02:37:05 GMT+0200 2014
Geotag Info  : Latitude:37.676563, Longitude:-122.062981
Views        : 186
Comments     : 0


+---------+
|  TITLE  |
+---------+
P6170488


+---------------+
|  DESCRIPTION  |
+---------------+
OLYMPUS DIGITAL CAMERA


+--------+
|  TAGS  |
+--------+
"Sulphur Creek" 2014 fox 