+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brianfuller6385
Photo URL    : https://www.flickr.com/photos/birdwatcher63/5785025597/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 31 16:55:41 GMT+0200 2011
Upload Date  : Wed Jun 1 09:07:20 GMT+0200 2011
Views        : 441
Comments     : 1


+---------+
|  TITLE  |
+---------+
Young Fox


+---------------+
|  DESCRIPTION  |
+---------------+
There is some undisturbed land immediately behind our garden and I have been watching a family of foxes for about a week now. They are usually about 100 yards away but yesterday they were a bit closer. There are 3 quite large cubs and this is one of them.


+--------+
|  TAGS  |
+--------+
fox "fox cub" foxes "wild animals" mammals wildlife 