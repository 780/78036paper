+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/10683150496/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 14 14:56:03 GMT+0200 2013
Upload Date  : Tue Nov 5 04:27:28 GMT+0100 2013
Geotag Info  : Latitude:42.462697, Longitude:-71.093636
Views        : 3,724
Comments     : 0


+---------+
|  TITLE  |
+---------+
Arctic Fox - Paying Attn


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
stone zoo massachusetts arctic fox white 