+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : monkeywing
Photo URL    : https://www.flickr.com/photos/colinsite/4696718711/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 11 17:59:36 GMT+0200 2010
Upload Date  : Sun Jun 13 21:40:04 GMT+0200 2010
Views        : 132
Comments     : 1


+---------+
|  TITLE  |
+---------+
DSCF8992


+---------------+
|  DESCRIPTION  |
+---------------+
Looking after Blue, my neighbour's cat


+--------+
|  TAGS  |
+--------+
siamese cat Blue 