+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Molten Boron
Photo URL    : https://www.flickr.com/photos/16693270@N02/23136278212/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 8 10:57:33 GMT+0200 2015
Upload Date  : Thu Nov 19 20:52:04 GMT+0100 2015
Views        : 22
Comments     : 0


+---------+
|  TITLE  |
+---------+
Happy Cat


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Tortie Point" "Siamese cat" cat 