+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kyrre Gjerstad
Photo URL    : https://www.flickr.com/photos/kyrre_gjerstad/8283624427/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 18 16:42:58 GMT+0100 2012
Upload Date  : Tue Dec 18 16:47:02 GMT+0100 2012
Views        : 3,094
Comments     : 4


+---------+
|  TITLE  |
+---------+
Those eyes


+---------------+
|  DESCRIPTION  |
+---------------+
My aunts Siamese cat.


+--------+
|  TAGS  |
+--------+
cat pet cute siamese eyes intense gato 