+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Trish Hamme
Photo URL    : https://www.flickr.com/photos/trishhamme/7765050482/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 10 09:33:04 GMT+0200 2012
Upload Date  : Sun Aug 12 13:39:11 GMT+0200 2012
Views        : 23,038
Comments     : 43


+---------+
|  TITLE  |
+---------+
"Cats have it all - admiration, an endless sleep, and company only when they want it." Rod McKuen


+---------------+
|  DESCRIPTION  |
+---------------+
Have a Peaceful and Blessed Sunday :)


+--------+
|  TAGS  |
+--------+
cat siamese "Miss Truffles" cute "blue eyes" flirting adorable 