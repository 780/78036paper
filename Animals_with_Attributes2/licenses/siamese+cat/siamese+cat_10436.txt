+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Karl Palutke
Photo URL    : https://www.flickr.com/photos/palutke/2209545624/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 17 13:04:04 GMT+0100 2007
Upload Date  : Mon Jan 21 15:46:28 GMT+0100 2008
Geotag Info  : Latitude:33.993930, Longitude:-83.966056
Views        : 453
Comments     : 1


+---------+
|  TITLE  |
+---------+
Siamese


+---------------+
|  DESCRIPTION  |
+---------------+
Local Siamese who likes to lounge on our patio


+--------+
|  TAGS  |
+--------+
cats lawrenceville ga georgia siamese 2007 