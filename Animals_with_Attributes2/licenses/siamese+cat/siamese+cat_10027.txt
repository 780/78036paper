+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Trish Hamme
Photo URL    : https://www.flickr.com/photos/trishhamme/6835722136/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 12 07:16:42 GMT+0100 2012
Upload Date  : Wed Mar 14 13:38:05 GMT+0100 2012
Views        : 2,406
Comments     : 37


+---------+
|  TITLE  |
+---------+
Miss Truffles


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Miss Truffles" Truffles cat siamese "Happy Whisker Wednesday" "Pretty Blue Eyes" "Fancy Ears" PicMonkey 