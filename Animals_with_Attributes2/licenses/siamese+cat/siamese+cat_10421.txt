+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Zillay Ali
Photo URL    : https://www.flickr.com/photos/zillay/6857178233/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 5 22:23:21 GMT+0100 2012
Upload Date  : Sat Feb 11 17:34:49 GMT+0100 2012
Views        : 530
Comments     : 0


+---------+
|  TITLE  |
+---------+
Siamese Cat aka Sami Cat


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Siamese Cat" "Sami Cat" Sami Siamese Cat Pakistan Lahore Animals Pets Beautiful Tiger 