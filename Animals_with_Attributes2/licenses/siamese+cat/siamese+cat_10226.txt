+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ibm4381
Photo URL    : https://www.flickr.com/photos/j_benson/4108358813/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 15 07:46:40 GMT+0100 2009
Upload Date  : Mon Nov 16 12:46:25 GMT+0100 2009
Views        : 6,011
Comments     : 12


+---------+
|  TITLE  |
+---------+
At play


+---------------+
|  DESCRIPTION  |
+---------------+
I'd often wondered why there aren't more pictures of cats playing.  They can be such active creatures when they're not being comatose.  But with my elderly Siamese it would have had to have been a two person job, one to run the camera and one to play with the cat.
But seven month old Buff Tabby breaks out into spontaneous play several time a day, so I figured all I had to do was have a camera waiting for him to enter full throttle boogie and it shouldn't be a problem.
Um, no.  It turned out to be quite hard.  I chose the 1D3 for its superior AF servo and 10 FPS and the 70-200/2.8 IS for speed and versatility and chased him around several times, trying to stay far enough away that the DOF wasn't too narrow, trying to stay out of his light, trying to find an unobstructed view.  But my light was cloudy daylight from a North-facing window.  Tons of light for a stationary subject but with this critter it meant high ISO noise in every image and most of them useless because of motion blur.  Sure a flash would have solved my lighting problem, but I'd lose my 10 frames per second, and the chance that I could find the one cute pose and pull the trigger before it morphed into something else is pretty unlikely.  Oh well.


+--------+
|  TAGS  |
+--------+
"buff tabby" "7 months old" playing "Canon EF 70-200mm F2.8L IS USM" 