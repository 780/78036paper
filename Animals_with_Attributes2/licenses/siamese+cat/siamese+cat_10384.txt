+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stark23x
Photo URL    : https://www.flickr.com/photos/stark23x/55575439/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 24 20:31:11 GMT+0100 2002
Upload Date  : Mon Oct 24 14:53:21 GMT+0200 2005
Views        : 568
Comments     : 0


+---------+
|  TITLE  |
+---------+
Pitsa in repose as a Queen


+---------------+
|  DESCRIPTION  |
+---------------+
Pitsa.  Queen.  In repose.  Worship accordingly.


+--------+
|  TAGS  |
+--------+
pet cat pitsa feral siamese cats 