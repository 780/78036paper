+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Roy Montgomery
Photo URL    : https://www.flickr.com/photos/roymontgomery/5004800121/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 12 09:04:26 GMT+0200 2010
Upload Date  : Sun Sep 19 20:34:15 GMT+0200 2010
Views        : 622
Comments     : 5


+---------+
|  TITLE  |
+---------+
ECHO_5293


+---------------+
|  DESCRIPTION  |
+---------------+
Echo poses in the foyer.


+--------+
|  TAGS  |
+--------+
Echo cat kitten kitty feline siamese "Austin Siamese Rescue" Lucinda Meezer 