+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Helena Jacoba
Photo URL    : https://www.flickr.com/photos/69302634@N02/9359450341/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 21 14:43:44 GMT+0200 2013
Upload Date  : Thu Jul 25 01:17:07 GMT+0200 2013
Views        : 216
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ezra Pound, a beautiful, friendly part-Himalayan with Siamese markings


+---------------+
|  DESCRIPTION  |
+---------------+
Bonded with brother TS Eliot. The pair can be adopted through <a href="http://www.abbeycats.org" rel="nofollow">www.abbeycats.org</a>


+--------+
|  TAGS  |
+--------+
Himalayan kittie cat Siamese markings cute 