+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : VintageAura
Photo URL    : https://www.flickr.com/photos/vintage-aura/8961785394/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 2 21:08:00 GMT+0200 2011
Upload Date  : Wed Jun 5 19:48:37 GMT+0200 2013
Views        : 534
Comments     : 0


+---------+
|  TITLE  |
+---------+
curious cat


+---------------+
|  DESCRIPTION  |
+---------------+
Aiden, with his adorable curious blue eyes.


+--------+
|  TAGS  |
+--------+
kitten "siamese kitten" '*~*Sweet*Kitties*Love*~*' 