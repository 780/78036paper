+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Roy Montgomery
Photo URL    : https://www.flickr.com/photos/roymontgomery/3416864930/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 4 08:26:03 GMT+0200 2009
Upload Date  : Mon Apr 6 04:11:30 GMT+0200 2009
Views        : 224
Comments     : 13


+---------+
|  TITLE  |
+---------+
Bentley 2374


+---------------+
|  DESCRIPTION  |
+---------------+
Bentely watching the morning doves fly in to the feeder. He likes to watch!


+--------+
|  TAGS  |
+--------+
cat feline gato siamese 