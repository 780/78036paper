+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Helga Weber
Photo URL    : https://www.flickr.com/photos/helga/2495590774/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 15 18:05:11 GMT+0200 2008
Upload Date  : Thu May 15 21:40:28 GMT+0200 2008
Views        : 1,312
Comments     : 2


+---------+
|  TITLE  |
+---------+
23/365 - Mommy Cat


+---------------+
|  DESCRIPTION  |
+---------------+
Another animal shot. Peachy (my Siamese baby) went missing the whole day and it was raining, so I couldn't take decent pictures.


+--------+
|  TAGS  |
+--------+
cats kittycat project365 