+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Zillay Ali
Photo URL    : https://www.flickr.com/photos/zillay/7595292352/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 17 22:46:14 GMT+0200 2012
Upload Date  : Wed Jul 18 07:30:20 GMT+0200 2012
Views        : 750
Comments     : 0


+---------+
|  TITLE  |
+---------+
Siamese Cat - Baby Kitten


+---------------+
|  DESCRIPTION  |
+---------------+
Siamese Cat - Baby Kitten


+--------+
|  TAGS  |
+--------+
(no tags)