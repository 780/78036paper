+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Anders.Bachmann
Photo URL    : https://www.flickr.com/photos/andersbachmann/6011940155/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 5 19:35:11 GMT+0200 2011
Upload Date  : Fri Aug 5 20:56:20 GMT+0200 2011
Views        : 815
Comments     : 8


+---------+
|  TITLE  |
+---------+
New mother!


+---------------+
|  DESCRIPTION  |
+---------------+
Sooo.. one of our youngest cats just got her very first children! ;)


+--------+
|  TAGS  |
+--------+
Cat Siamese cute kiting 