+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Trish Hamme
Photo URL    : https://www.flickr.com/photos/trishhamme/8030111657/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 25 11:39:50 GMT+0200 2012
Upload Date  : Thu Sep 27 18:37:36 GMT+0200 2012
Views        : 5,094
Comments     : 51


+---------+
|  TITLE  |
+---------+
In a blue funk


+---------------+
|  DESCRIPTION  |
+---------------+
<b>Me , not Truffles :)</b> 
Been enduring a sad and depressed mood , a family thing( over which I have no control) , time of year , sad anniversaries , and maybe just feeling old :(
Working My way up &amp; out of this &quot;funk&quot; , and I know all will right itself , with time ~~~ and PRAYER !
Thank You , Dear Friends , We Love You !!!!!!!!!!!!


+--------+
|  TAGS  |
+--------+
cat siamese "Miss Truffles" Me mood sadness depression "in a blue funk" "will be OK" bestofcats 