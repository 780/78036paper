+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Trish Hamme
Photo URL    : https://www.flickr.com/photos/trishhamme/11207070825/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 3 16:47:55 GMT+0100 2013
Upload Date  : Wed Dec 4 16:20:56 GMT+0100 2013
Views        : 6,998
Comments     : 18


+---------+
|  TITLE  |
+---------+
“Smile and the world smiles with you ~ ~ ~


+---------------+
|  DESCRIPTION  |
+---------------+
~ ~  cry and you cry alone.”     
Happy Whisker Wednesday :)


+--------+
|  TAGS  |
+--------+
cat siamese "Miss Truffles" smile "Happy Whisker Wednesday" 