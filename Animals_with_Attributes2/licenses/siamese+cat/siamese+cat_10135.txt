+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Helena Jacoba
Photo URL    : https://www.flickr.com/photos/69302634@N02/9544196296/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 18 13:31:54 GMT+0200 2013
Upload Date  : Mon Aug 19 04:46:13 GMT+0200 2013
Views        : 358
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ezra Pound in the cats' favourite basket


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
cat Siamese Himilayan 