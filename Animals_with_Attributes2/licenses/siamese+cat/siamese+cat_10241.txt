+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 12, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : C. Rushforth
Photo URL    : https://www.flickr.com/photos/sanguisvitae/8664126931/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 20 18:10:49 GMT+0200 2013
Upload Date  : Sat Apr 20 10:28:43 GMT+0200 2013
Views        : 389
Comments     : 0


+---------+
|  TITLE  |
+---------+
blue-eyed girl


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
asahipentaxspotmatic spotmatic iso400 pentaxspotmatic townsville home pets cat tonkinese siamese blue 