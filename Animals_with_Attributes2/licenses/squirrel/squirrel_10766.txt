+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Commodore Gandalf Cunningham
Photo URL    : https://www.flickr.com/photos/gandalfcunningham/4009194447/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 3 15:13:38 GMT+0200 2009
Upload Date  : Wed Oct 14 00:55:30 GMT+0200 2009
Geotag Info  : Latitude:43.650795, Longitude:-79.404931
Views        : 97
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Squirrel "urban wildlife" "alexandra park" 