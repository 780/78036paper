+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Genista
Photo URL    : https://www.flickr.com/photos/genista/2227291245/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 26 15:33:48 GMT+0100 2008
Upload Date  : Tue Jan 29 06:58:42 GMT+0100 2008
Geotag Info  : Latitude:40.727683, Longitude:-74.076916
Views        : 155
Comments     : 0


+---------+
|  TITLE  |
+---------+
faceoff


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
squirrels rodents animals 