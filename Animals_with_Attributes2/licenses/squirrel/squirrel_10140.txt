+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : darkripper
Photo URL    : https://www.flickr.com/photos/darkripper/2507669119/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 20 13:02:57 GMT+0200 2008
Upload Date  : Tue May 20 13:02:57 GMT+0200 2008
Views        : 97
Comments     : 0


+---------+
|  TITLE  |
+---------+
Damn squirrels


+---------------+
|  DESCRIPTION  |
+---------------+
They're too fast for my lenses.


+--------+
|  TAGS  |
+--------+
scoiattolo scoiattoli squirrels squirrel 