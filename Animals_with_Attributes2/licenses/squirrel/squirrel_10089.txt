+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Allan Hack
Photo URL    : https://www.flickr.com/photos/aehack/25001471942/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Feb 18 17:17:35 GMT+0100 2016
Upload Date  : Fri Feb 19 04:12:45 GMT+0100 2016
Views        : 50
Comments     : 2


+---------+
|  TITLE  |
+---------+
Black Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Squirrel "Black Squirrel" 