+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kennethkonica
Photo URL    : https://www.flickr.com/photos/littlebiglens/15066705302/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 28 16:58:31 GMT+0200 2014
Upload Date  : Thu Aug 28 23:58:31 GMT+0200 2014
Views        : 6,754
Comments     : 16


+---------+
|  TITLE  |
+---------+
Is the munching squirrel smiling?


+---------------+
|  DESCRIPTION  |
+---------------+
The hungry squirrel had such a tasty treat it didn't even notice me about fifteen feet away.


+--------+
|  TAGS  |
+--------+
"best of squirrels" squirrel 