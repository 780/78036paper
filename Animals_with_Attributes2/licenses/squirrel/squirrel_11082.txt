+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dave Williss
Photo URL    : https://www.flickr.com/photos/dwilliss/7762054676/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 11 10:15:17 GMT+0200 2012
Upload Date  : Sun Aug 12 01:35:52 GMT+0200 2012
Views        : 24
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)