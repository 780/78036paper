+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sheep purple
Photo URL    : https://www.flickr.com/photos/sheeppurple/2664116375/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 10 19:01:38 GMT+0200 2008
Upload Date  : Sun Jul 13 20:04:03 GMT+0200 2008
Geotag Info  : Latitude:55.870724, Longitude:-4.283638
Views        : 362
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel on a healthy diet


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
glasgow park squirrel scotland 