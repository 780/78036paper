+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : foqus
Photo URL    : https://www.flickr.com/photos/foqus/2327804532/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Mar 11 09:34:10 GMT+0100 2008
Upload Date  : Tue Mar 11 23:14:53 GMT+0100 2008
Views        : 1,299
Comments     : 1


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
A squirrel on the natural feeder that I built in my backyard.


+--------+
|  TAGS  |
+--------+
squirrel best squirrels "best of squirrels" 