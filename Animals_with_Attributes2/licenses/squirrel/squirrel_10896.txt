+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : James E. Petts
Photo URL    : https://www.flickr.com/photos/14730981@N08/6867504618/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 24 17:30:45 GMT+0100 2012
Upload Date  : Sun Mar 25 12:57:11 GMT+0200 2012
Geotag Info  : Latitude:51.465707, Longitude:-0.092340
Views        : 718
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
Ruskin Park, South London


+--------+
|  TAGS  |
+--------+
"Ruskin park" squirrel "Panasonic 25mm F1.4" 