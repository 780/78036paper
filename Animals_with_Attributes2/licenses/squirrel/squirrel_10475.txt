+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Chris Parker2012
Photo URL    : https://www.flickr.com/photos/chrisparker2012/14235953933/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 13 09:25:51 GMT+0200 2014
Upload Date  : Sun May 18 21:55:59 GMT+0200 2014
Views        : 611
Comments     : 2


+---------+
|  TITLE  |
+---------+
Red Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
squirrel "Red Squirrel" mammal wildlife nature "British Wildlife Centre" 