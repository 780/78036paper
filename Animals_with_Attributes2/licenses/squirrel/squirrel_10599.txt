+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : likeaduck
Photo URL    : https://www.flickr.com/photos/thartz00/4847218122/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 31 10:38:16 GMT+0200 2010
Upload Date  : Sat Jul 31 21:03:15 GMT+0200 2010
Geotag Info  : Latitude:39.820864, Longitude:-75.764293
Views        : 79
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel on the Prowl


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
squirrel branch limb 