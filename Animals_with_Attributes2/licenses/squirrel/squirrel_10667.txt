+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stanmarston
Photo URL    : https://www.flickr.com/photos/stanmarston/816802335/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 14 16:42:00 GMT+0200 2007
Upload Date  : Sun Jul 15 13:12:53 GMT+0200 2007
Views        : 84
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"grey squirrel" picnic wildlife "calke abbey" squirrel 