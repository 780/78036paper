+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jerry Downs
Photo URL    : https://www.flickr.com/photos/jdowns66/539700999/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 29 18:05:28 GMT+0200 2007
Upload Date  : Mon Jun 11 02:16:41 GMT+0200 2007
Views        : 61
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
Taken at feeders at home on 4/29/07


+--------+
|  TAGS  |
+--------+
Animal 