+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Genista
Photo URL    : https://www.flickr.com/photos/genista/6670723/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 11 13:57:19 GMT+0100 2005
Upload Date  : Wed Mar 16 18:08:24 GMT+0100 2005
Geotag Info  : Latitude:37.870957, Longitude:-122.264378
Views        : 178
Comments     : 1


+---------+
|  TITLE  |
+---------+
deep thought


+---------------+
|  DESCRIPTION  |
+---------------+
Clean your mouth, Mr Squirrel, you got dirt on your chin.


+--------+
|  TAGS  |
+--------+
berkeley california animals mammals squirrel 