+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kla4067
Photo URL    : https://www.flickr.com/photos/84263554@N00/5494472003/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 2 09:51:30 GMT+0100 2011
Upload Date  : Thu Mar 3 19:37:57 GMT+0100 2011
Views        : 1,721
Comments     : 2


+---------+
|  TITLE  |
+---------+
Squirrel, "Are you looking at me?"


+---------------+
|  DESCRIPTION  |
+---------------+
I don't have to look any farther than my backyard to find &quot;wildlife&quot; to photograph.


+--------+
|  TAGS  |
+--------+
Squirrel backyard 