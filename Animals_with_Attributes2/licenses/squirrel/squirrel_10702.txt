+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Loimere
Photo URL    : https://www.flickr.com/photos/loimere/2689386864/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 19 21:17:28 GMT+0200 2008
Upload Date  : Mon Jul 21 16:17:29 GMT+0200 2008
Geotag Info  : Latitude:48.647654, Longitude:-91.205406
Views        : 45
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Quetico Loimere canoeing camping 