+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dhinal Chheda
Photo URL    : https://www.flickr.com/photos/dhinalchheda/8575876711/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Mar 20 04:55:43 GMT+0100 2013
Upload Date  : Thu Mar 21 06:03:53 GMT+0100 2013
Geotag Info  : Latitude:37.390583, Longitude:-122.000598
Views        : 406
Comments     : 2


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
squirrel expression 