+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Bobolink
Photo URL    : https://www.flickr.com/photos/bobolink/14083337440/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 25 14:31:51 GMT+0200 2014
Upload Date  : Mon May 26 02:00:53 GMT+0200 2014
Geotag Info  : Latitude:44.295664, Longitude:-77.553337
Views        : 1,887
Comments     : 4


+---------+
|  TITLE  |
+---------+
Black Squirrel_9434


+---------------+
|  DESCRIPTION  |
+---------------+
Eating black oil sunflower seeds


+--------+
|  TAGS  |
+--------+
"Black Squirrel" Stirling Ontario 