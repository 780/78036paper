+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ScriS - www.scris.it
Photo URL    : https://www.flickr.com/photos/scris/6635364935/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 18 13:38:23 GMT+0200 2011
Upload Date  : Wed Jan 4 17:01:40 GMT+0100 2012
Views        : 556
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
Squirrel at St. James Park (London)


+--------+
|  TAGS  |
+--------+
Squirrel scoiattolo park london londra parco 