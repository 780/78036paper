+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : milan.boers
Photo URL    : https://www.flickr.com/photos/milanboers/3507420766/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 6 04:45:18 GMT+0200 2009
Upload Date  : Wed May 6 13:45:18 GMT+0200 2009
Views        : 98
Comments     : 0


+---------+
|  TITLE  |
+---------+
A Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
This is a Squirrel in Florida. He looks just like the ones in the Netherlands.


+--------+
|  TAGS  |
+--------+
florida "st. petersburg" squirrel 