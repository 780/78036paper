+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nick J Webb
Photo URL    : https://www.flickr.com/photos/nickwebb/3210257307/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 18 12:43:48 GMT+0100 2009
Upload Date  : Mon Jan 19 23:20:47 GMT+0100 2009
Views        : 139
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squizzel


+---------------+
|  DESCRIPTION  |
+---------------+
Fat Squirrel in Kelsey Park


+--------+
|  TAGS  |
+--------+
Squirrel "Kelsey park" 