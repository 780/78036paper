+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : inyucho
Photo URL    : https://www.flickr.com/photos/inyucho/7239775228/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 20 14:03:11 GMT+0200 2012
Upload Date  : Mon May 21 08:47:52 GMT+0200 2012
Geotag Info  : Latitude:59.981177, Longitude:30.265322
Views        : 124
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"St Petersburg" "Saint Petersburg" Russia Санкт-Петерб́ург Росси́я "Елагин остров" "Yelagin Island" Россия Санкт-Петербург 