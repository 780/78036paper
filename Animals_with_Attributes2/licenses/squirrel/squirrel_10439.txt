+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : guano
Photo URL    : https://www.flickr.com/photos/guano/309890918/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 29 19:20:29 GMT+0100 2006
Upload Date  : Thu Nov 30 02:20:29 GMT+0100 2006
Views        : 270
Comments     : 2


+---------+
|  TITLE  |
+---------+
rain-soaked squirrel chowing down on some tasty corn


+---------------+
|  DESCRIPTION  |
+---------------+
Now that our two wild cats are inside and safe, all our backyard squirrels are coming back to their back porch corn feeder.


+--------+
|  TAGS  |
+--------+
2006 Nov 28 squirrel 