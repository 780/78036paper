+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Steve Webel
Photo URL    : https://www.flickr.com/photos/webel/76674666/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 20 02:27:04 GMT+0100 2005
Upload Date  : Fri Dec 23 21:59:18 GMT+0100 2005
Geotag Info  : Latitude:27.714486, Longitude:-82.648767
Views        : 660
Comments     : 1


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
Looking for goodies.


+--------+
|  TAGS  |
+--------+
Florida Minolta dynax5d webelo 