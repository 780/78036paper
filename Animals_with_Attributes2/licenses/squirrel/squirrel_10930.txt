+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gurdonark
Photo URL    : https://www.flickr.com/photos/46183897@N00/8275838041/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Dec 16 01:50:35 GMT+0100 2012
Upload Date  : Sun Dec 16 05:12:10 GMT+0100 2012
Views        : 59
Comments     : 0


+---------+
|  TITLE  |
+---------+
squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
Texarkana, Arkansas, December 15 2012, Bobby Ferguson Park


+--------+
|  TAGS  |
+--------+
(no tags)