+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : chefranden
Photo URL    : https://www.flickr.com/photos/chefranden/7469517940/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Mar 1 14:17:10 GMT+0100 2011
Upload Date  : Sat Jun 30 01:34:55 GMT+0200 2012
Views        : 476
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gray Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
nature squirrel rodent animal fur furry tree 