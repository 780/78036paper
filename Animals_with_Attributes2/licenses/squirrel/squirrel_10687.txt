+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jeff Kubina
Photo URL    : https://www.flickr.com/photos/kubina/2401524797/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 23 14:40:57 GMT+0100 2008
Upload Date  : Thu Apr 10 03:05:15 GMT+0200 2008
Geotag Info  : Latitude:39.200017, Longitude:-76.828857
Views        : 1,395
Comments     : 9


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"united states" 200803 maryland columbia 2008 squirrel "taken by jeff kubina" public creative_commons 