+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daveoratox
Photo URL    : https://www.flickr.com/photos/atoxinsocks/8343156139/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 27 16:01:48 GMT+0200 2012
Upload Date  : Fri Jan 4 00:51:34 GMT+0100 2013
Views        : 279
Comments     : 0


+---------+
|  TITLE  |
+---------+
Snack


+---------------+
|  DESCRIPTION  |
+---------------+
I was hearing a gnawing sound every time I went to my car for days before I finally investigated and found this guy going to town on a walnut.


+--------+
|  TAGS  |
+--------+
squirrel nut walnut 