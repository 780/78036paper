+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BillDamon
Photo URL    : https://www.flickr.com/photos/billdamon/13311940763/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 21 08:44:09 GMT+0100 2014
Upload Date  : Fri Mar 21 18:15:37 GMT+0100 2014
Views        : 366
Comments     : 0


+---------+
|  TITLE  |
+---------+
2014-03-21 - Squirrel on the back deck


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
squirrel 