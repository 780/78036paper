+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Limbo Poet having a break for a while
Photo URL    : https://www.flickr.com/photos/44639455@N00/1213981365/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jun 10 00:00:00 GMT+0200 2007
Upload Date  : Thu Aug 23 17:14:47 GMT+0200 2007
Views        : 2,275
Comments     : 37


+---------+
|  TITLE  |
+---------+
Squirrel Posing


+---------------+
|  DESCRIPTION  |
+---------------+
Cropped

From my archive.


+--------+
|  TAGS  |
+--------+
Squirrel Posing Pose Nature Crop SuperbMasterpiece SOE SearchTheBest. NaturesFinest écureuil herbe pelouse Uk England Angleterre Harlow 