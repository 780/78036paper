+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : heolzo
Photo URL    : https://www.flickr.com/photos/heolzo/16779563442/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Mar 10 10:27:47 GMT+0100 2015
Upload Date  : Wed Mar 11 03:09:54 GMT+0100 2015
Views        : 1,486
Comments     : 0


+---------+
|  TITLE  |
+---------+
Écureuil gris - Gray squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
Écureuil gris - Sciurus carolinensis - Gray squirrel


+--------+
|  TAGS  |
+--------+
Écureuil squirrel wildlife faune 