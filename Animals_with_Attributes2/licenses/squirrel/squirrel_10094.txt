+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ReneS
Photo URL    : https://www.flickr.com/photos/rene-germany/45010983/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 20 15:50:34 GMT+0200 2005
Upload Date  : Tue Sep 20 15:50:34 GMT+0200 2005
Views        : 1,070
Comments     : 2


+---------+
|  TITLE  |
+---------+
Boston Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
Photo taken in Boston Downtown in one of the parks.


+--------+
|  TAGS  |
+--------+
boston park squirrel animal 