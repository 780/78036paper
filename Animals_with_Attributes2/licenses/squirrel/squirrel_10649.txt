+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tompagenet
Photo URL    : https://www.flickr.com/photos/tompagenet/173721895/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 23 18:25:33 GMT+0200 2006
Upload Date  : Sat Jun 24 12:36:19 GMT+0200 2006
Views        : 132
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"St James's Park" Park London squirrel 