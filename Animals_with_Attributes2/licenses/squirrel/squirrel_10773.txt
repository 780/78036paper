+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SkyFireXII
Photo URL    : https://www.flickr.com/photos/skyfire/4215012789/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 26 10:57:51 GMT+0100 2009
Upload Date  : Sat Dec 26 12:38:36 GMT+0100 2009
Views        : 176
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Raw squirrel winter 2009 