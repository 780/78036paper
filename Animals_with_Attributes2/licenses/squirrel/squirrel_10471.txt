+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : peter vogel.troll
Photo URL    : https://www.flickr.com/photos/peter-ist-troll/10875271966/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 20 08:08:37 GMT+0200 2013
Upload Date  : Fri Nov 15 22:05:51 GMT+0100 2013
Geotag Info  : Latitude:52.203825, Longitude:10.510308
Views        : 3,349
Comments     : 10


+---------+
|  TITLE  |
+---------+
Red Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Anna Karenina" "BS Leiferde" "Eichhörnchen (Sciurus vulgaris) Red squirrel" "Eichhörnchen {Eichhoernchen} (Sciurus)" "Europäisches Eichhörnchen {Europaeisches Eichhoernchen} (Sciur" "Hörnchen {Hoernchen} (Sciuridae) squirrel" "Landwirbeltiere (Tetrapoda) Tetrapod" "Nagetiere (Rodentia)" "Tierwelt animals world" "Wild lebende Tiere Wildlife" "Wirbeltiere (Vertebrata) Vertebrate" "سنجاب أحمر  Ardilla roja  Écureuil roux  Scoiattolo  r" "Europäisches Eichhörnchen {Europaeisches Eichhoernchen} (Sciurus vulgaris) Eurasian red squirrel" "سنجاب أحمر  Ardilla roja  Écureuil roux  Scoiattolo  rode eekhoorn  Wiewiórka pospolita  Ekorre" "Areal Germany (German Federal Republic)" "Areal Braunschweig (Brunswik)" "Säugetiere {Saeugetiere} (Mammalia) • Mammal" "Baumhörnchen {Baumhoernchen} (Sciurini) • Sciurini" "Baum- und Gleithörnchen {Gleithoernchen} (Sciurinae) • Sciurinae" "Areal Niedersachsen • Lower Saxony" 