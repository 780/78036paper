+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bunnygoth
Photo URL    : https://www.flickr.com/photos/bunnygoth/14021860510/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 21 02:24:20 GMT+0200 2014
Upload Date  : Sun May 18 00:58:34 GMT+0200 2014
Views        : 311
Comments     : 0


+---------+
|  TITLE  |
+---------+
212


+---------------+
|  DESCRIPTION  |
+---------------+
Young squirrel in our yard


+--------+
|  TAGS  |
+--------+
baby young squirrel 