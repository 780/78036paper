+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cliff1066™
Photo URL    : https://www.flickr.com/photos/nostri-imago/2880347447/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 25 16:27:12 GMT+0200 2008
Upload Date  : Tue Sep 23 04:15:29 GMT+0200 2008
Geotag Info  : Latitude:38.887947, Longitude:-77.026005
Views        : 374
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrels


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Animal Chordate Mammal squirrels "flying squirrel" "grey squirrel" "fox squirrel" "red squirrel" "cat squirrel" acorn Rodent Sciuridae Squirrel "genera Sciurus" "Bushy tail" "Black Squirrel" Wildlife Nature 