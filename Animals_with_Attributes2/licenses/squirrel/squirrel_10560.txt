+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ingrid Taylar
Photo URL    : https://www.flickr.com/photos/taylar/4118781766/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Nov 3 09:55:37 GMT+0100 2009
Upload Date  : Fri Nov 20 01:38:18 GMT+0100 2009
Views        : 975
Comments     : 1


+---------+
|  TITLE  |
+---------+
Squirrel Contemplation


+---------------+
|  DESCRIPTION  |
+---------------+
Fox squirrel (<i>Scurius Niger</i>)


+--------+
|  TAGS  |
+--------+
squirrels "tree squirrel" "fox squirrel" "scurius niger" california tree trunk standing sun 