+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Stevesworldofphotos
Photo URL    : https://www.flickr.com/photos/stevesworldofphotos/13718166315/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 22 11:22:26 GMT+0100 2014
Upload Date  : Tue Apr 8 17:11:00 GMT+0200 2014
Views        : 185
Comments     : 0


+---------+
|  TITLE  |
+---------+
Squirrel


+---------------+
|  DESCRIPTION  |
+---------------+
Quite simply, the best portrait of a squirrel I've ever taken. That's all.


+--------+
|  TAGS  |
+--------+
March Minneapolis Minnesota "Poho 365" "Powderhorn 365" "Powderhorn Park" city neighborhood urban winter 