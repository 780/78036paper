+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 11, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Airwolfhound
Photo URL    : https://www.flickr.com/photos/24874528@N04/15192910540/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 28 10:50:20 GMT+0200 2014
Upload Date  : Sun Sep 28 15:26:33 GMT+0200 2014
Views        : 4,366
Comments     : 7


+---------+
|  TITLE  |
+---------+
Squirrel - RSPB Sandy


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Squirrel "RSPB Sandy" 