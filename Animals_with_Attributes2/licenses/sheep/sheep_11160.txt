+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sean MacEntee
Photo URL    : https://www.flickr.com/photos/smemon/21926891432/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 25 18:34:06 GMT+0200 2015
Upload Date  : Sun Oct 4 15:33:29 GMT+0200 2015
Views        : 86
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
Kilmalkedar Church &amp; Graveyard, Kerry

Sheep


+--------+
|  TAGS  |
+--------+
"Kilmalkedar Church & Graveyard" Kerry Sheep 