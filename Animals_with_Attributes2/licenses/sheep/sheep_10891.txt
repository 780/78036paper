+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : katesheets
Photo URL    : https://www.flickr.com/photos/katesheets/2821537660/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 25 13:57:43 GMT+0200 2008
Upload Date  : Tue Sep 2 14:39:39 GMT+0200 2008
Geotag Info  : Latitude:48.860649, Longitude:23.437957
Views        : 65
Comments     : 1


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
The hotel had four pet sheep that were caged during the day, and roamed the grounds at night.  This one was sad... he had a limp.


+--------+
|  TAGS  |
+--------+
COS "Peace Corps" Ukraine Carpathians Slavske 