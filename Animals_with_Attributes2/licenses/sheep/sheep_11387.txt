+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : foxypar4
Photo URL    : https://www.flickr.com/photos/foxypar4/474250992/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 26 07:46:16 GMT+0200 2007
Upload Date  : Fri Apr 27 08:10:37 GMT+0200 2007
Geotag Info  : Latitude:57.877808, Longitude:-4.029150
Views        : 1,216
Comments     : 4


+---------+
|  TITLE  |
+---------+
Who are ewe looking at


+---------------+
|  DESCRIPTION  |
+---------------+
Part of a very nervous flock of sheep on the outskirts of town


+--------+
|  TAGS  |
+--------+
scotland dornoch sutherland sheep field 