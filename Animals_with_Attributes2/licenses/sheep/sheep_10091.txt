+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Powhusku
Photo URL    : https://www.flickr.com/photos/hubbardcoe/5654226074/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 23 14:33:12 GMT+0200 2011
Upload Date  : Mon Apr 25 17:54:59 GMT+0200 2011
Views        : 298
Comments     : 0


+---------+
|  TITLE  |
+---------+
Baby Big Horn Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Big Horn Sheep 