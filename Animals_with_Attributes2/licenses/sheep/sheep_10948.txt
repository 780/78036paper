+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tony Austin
Photo URL    : https://www.flickr.com/photos/tonyaustin/4008187859/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 10 13:43:17 GMT+0200 2009
Upload Date  : Tue Oct 13 18:28:12 GMT+0200 2009
Views        : 599
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Beale Park" sheep "Ovis aries" 