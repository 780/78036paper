+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KathrynW1
Photo URL    : https://www.flickr.com/photos/kathryn-wright/13911881938/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 3 08:17:10 GMT+0200 2014
Upload Date  : Sat May 3 21:41:49 GMT+0200 2014
Geotag Info  : Latitude:52.030554, Longitude:-1.845703
Views        : 83
Comments     : 0


+---------+
|  TITLE  |
+---------+
Walking to Broadway Tower


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
sheep lamb cotswolds animal 