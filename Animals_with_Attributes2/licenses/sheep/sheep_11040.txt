+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Effervescing Elephant
Photo URL    : https://www.flickr.com/photos/cr01/6908504943/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Feb 19 12:23:44 GMT+0100 2012
Upload Date  : Mon Feb 20 09:58:12 GMT+0100 2012
Views        : 513
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Sheep "Hebden Bridge" "Stoodley Pike" Hike walking moors 