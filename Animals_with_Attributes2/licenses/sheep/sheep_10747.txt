+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Martin Pettitt
Photo URL    : https://www.flickr.com/photos/mdpettitt/2681167650/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 17 10:54:18 GMT+0200 2008
Upload Date  : Sat Jul 19 00:56:02 GMT+0200 2008
Geotag Info  : Latitude:51.848365, Longitude:-0.538040
Views        : 1,453
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep Grazing


+---------------+
|  DESCRIPTION  |
+---------------+
Sheep at Whipsnade Zoo


+--------+
|  TAGS  |
+--------+
Sheep "Whipsnade Zoo" Sony DSLR Animals Zoo "Wildlife Park" 