+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Magic Madzik
Photo URL    : https://www.flickr.com/photos/cefeida/2463952779/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 4 14:05:13 GMT+0200 2008
Upload Date  : Sun May 4 17:48:15 GMT+0200 2008
Geotag Info  : Latitude:51.819127, Longitude:19.400782
Views        : 8,318
Comments     : 2


+---------+
|  TITLE  |
+---------+
125/366: Sheep on Lisciasta Street


+---------------+
|  DESCRIPTION  |
+---------------+
May 4th 125/366

I knew I was biking to the edges of the city but I really didn't expect to run into a flock of sheep!

(That's not my bike,  by the way, it's some kid's. :P Mine is a bit bigger than that.)


+--------+
|  TAGS  |
+--------+
lodz poland sheep flock "project 366 2008" 