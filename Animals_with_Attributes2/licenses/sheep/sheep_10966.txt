+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nickton
Photo URL    : https://www.flickr.com/photos/18203311@N08/4453297746/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 20 21:17:00 GMT+0100 2010
Upload Date  : Mon Mar 22 04:38:11 GMT+0100 2010
Geotag Info  : Latitude:38.005462, Longitude:-122.362117
Views        : 114
Comments     : 7


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
Point Pinole


+--------+
|  TAGS  |
+--------+
ebparksok 