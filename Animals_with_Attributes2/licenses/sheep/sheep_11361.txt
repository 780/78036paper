+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ramon_harkema
Photo URL    : https://www.flickr.com/photos/129515770@N07/24061564905/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 22 16:46:23 GMT+0200 2015
Upload Date  : Wed Dec 30 10:44:49 GMT+0100 2015
Views        : 331
Comments     : 1


+---------+
|  TITLE  |
+---------+
Wales_Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Wales England Engeland UK Nature Natuur Wildlife Wild Animals Dieren Beautiful prachtig mooi Schaap Sheep Cliff coast "Canon 60D" Canon "Sigma DG 150-500mm 1:5-6.3 APO HSM" Sigma 500mm tele zoom 