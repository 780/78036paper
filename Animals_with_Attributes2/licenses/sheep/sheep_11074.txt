+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : LandBetweenTheLakesKYTN
Photo URL    : https://www.flickr.com/photos/lblkytn/8476267747/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 20 10:11:33 GMT+0100 2011
Upload Date  : Fri Feb 15 22:14:11 GMT+0100 2013
Views        : 356
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
Life on the Farm
Landing Page


+--------+
|  TAGS  |
+--------+
LandBetweenTheLakesKYTN "Land Between The Lakes" "US Forest Service" nature "environmental education" "Kentucky Lake" "Lake Barkley" history camping birding biking hiking hunting Kentucky Tennessee "public lands" "scenic driving" "wildlife watching" 