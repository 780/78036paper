+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : SaraYeomans
Photo URL    : https://www.flickr.com/photos/yeomans/211318597/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 9 16:26:56 GMT+0200 2006
Upload Date  : Thu Aug 10 01:26:56 GMT+0200 2006
Views        : 363
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSCN3481


+---------------+
|  DESCRIPTION  |
+---------------+
Sheep.  They're everywhere in Iceland!


+--------+
|  TAGS  |
+--------+
Iceland sheep lambs 