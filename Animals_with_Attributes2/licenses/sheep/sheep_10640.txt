+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neil T
Photo URL    : https://www.flickr.com/photos/neilt/9275635781/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 11 14:08:42 GMT+0200 2013
Upload Date  : Sat Jul 13 19:22:07 GMT+0200 2013
Geotag Info  : Latitude:53.982742, Longitude:-1.504869
Views        : 622
Comments     : 0


+---------+
|  TITLE  |
+---------+
Scottish Blackface Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
This sheep isn't normally orange but its customary for farmers of Scottish Blackface sheep to dye them orange


+--------+
|  TAGS  |
+--------+
greatyorkshireshow orange scottishblackface sheep yorkshire yorkshireshow 