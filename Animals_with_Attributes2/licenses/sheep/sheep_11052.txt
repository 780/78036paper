+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : amandabhslater
Photo URL    : https://www.flickr.com/photos/pikerslanefarm/7297702648/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 29 14:32:24 GMT+0200 2012
Upload Date  : Tue May 29 23:39:22 GMT+0200 2012
Geotag Info  : Latitude:51.938021, Longitude:-1.834716
Views        : 305
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
Cotswold Farm Park


+--------+
|  TAGS  |
+--------+
"Cotswold Farm Park" Gloucestershire 