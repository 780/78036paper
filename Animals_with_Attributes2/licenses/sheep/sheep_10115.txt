+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tico_24
Photo URL    : https://www.flickr.com/photos/tico24/10134878514/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 25 11:36:01 GMT+0200 2013
Upload Date  : Mon Oct 7 12:05:11 GMT+0200 2013
Geotag Info  : Latitude:52.862782, Longitude:0.613356
Views        : 472
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bircham Windmill


+---------------+
|  DESCRIPTION  |
+---------------+
This is a sheep, not a windmill. The sheep is at the windmill.


+--------+
|  TAGS  |
+--------+
Grass Sheep Wool 