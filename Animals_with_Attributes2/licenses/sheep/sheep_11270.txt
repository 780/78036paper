+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mikecogh
Photo URL    : https://www.flickr.com/photos/mikecogh/1344887201/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 8 10:45:55 GMT+0200 2007
Upload Date  : Sat Sep 8 10:03:56 GMT+0200 2007
Geotag Info  : Latitude:-34.944980, Longitude:138.586692
Views        : 331
Comments     : 0


+---------+
|  TITLE  |
+---------+
Spiffy Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
royalshow sheep judging judges breeding 