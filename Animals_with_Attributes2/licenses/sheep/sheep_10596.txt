+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rawdonfox
Photo URL    : https://www.flickr.com/photos/34739556@N04/5580175965/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 28 16:13:47 GMT+0200 2011
Upload Date  : Sat Apr 2 00:11:21 GMT+0200 2011
Geotag Info  : Latitude:53.894021, Longitude:-1.632628
Views        : 1,614
Comments     : 2


+---------+
|  TITLE  |
+---------+
Rawdon Racing Sheep.


+---------------+
|  DESCRIPTION  |
+---------------+
A little known sport around here. Big money passes hands through the illegal book taking of sheep racing.
These three are both a bit young, but are being trained to follow the mechanical carrot to the end of the course. Jockeys are of course prohibited from the actual race, but the sheep which are trained under the saddle can move a lot quicker than their competitors without the extra burdon of a trainer.


+--------+
|  TAGS  |
+--------+
sheep racing rawdon book betting lamb aprilfool 