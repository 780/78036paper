+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : AlicePopkorn2
Photo URL    : https://www.flickr.com/photos/47283811@N06/7122269063/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 22 17:03:42 GMT+0200 2012
Upload Date  : Sat Apr 28 22:21:21 GMT+0200 2012
Views        : 3,167
Comments     : 8


+---------+
|  TITLE  |
+---------+
friendly sheep


+---------------+
|  DESCRIPTION  |
+---------------+
photo tour in the open countryside :-)


+--------+
|  TAGS  |
+--------+
sheep Schafe sunny afternoon spring friendly 