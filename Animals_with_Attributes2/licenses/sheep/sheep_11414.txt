+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jenny-bee
Photo URL    : https://www.flickr.com/photos/jennyellenbrown/2735760003/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 1 16:03:03 GMT+0200 2008
Upload Date  : Tue Aug 5 22:02:36 GMT+0200 2008
Views        : 1,538
Comments     : 1


+---------+
|  TITLE  |
+---------+
Yellow sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
yellow sheep pen isle lewis "Isle of Lewis" Scotland 