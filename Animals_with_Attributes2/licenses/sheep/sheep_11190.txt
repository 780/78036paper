+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jeffedoe
Photo URL    : https://www.flickr.com/photos/jeffedoe/2400233069/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 6 15:11:55 GMT+0200 2008
Upload Date  : Wed Apr 9 15:34:13 GMT+0200 2008
Views        : 94
Comments     : 1


+---------+
|  TITLE  |
+---------+
SHEEP!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Hardwick sheep lambs grass Chesterfield Statley home 'National Trust' 