+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Luc.T
Photo URL    : https://www.flickr.com/photos/luctnl/16411034704/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 4 14:44:24 GMT+0200 2015
Upload Date  : Sat Apr 4 21:49:11 GMT+0200 2015
Geotag Info  : Latitude:50.930638, Longitude:3.901244
Views        : 1,065
Comments     : 1


+---------+
|  TITLE  |
+---------+
Fat sheep


+---------------+
|  DESCRIPTION  |
+---------------+
In een weide tussen Vlierzele en Zonnegem stond deze zware jongen ons boos aan te kijken...

In a meadow somewhere between Vlierzele and Zonnegem, stood this heavy guy, looking at us angrily...


+--------+
|  TAGS  |
+--------+
Vlaanderen Sheep België 