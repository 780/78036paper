+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kumweni
Photo URL    : https://www.flickr.com/photos/76338186@N03/15824311440/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 6 09:32:48 GMT+0100 2014
Upload Date  : Sat Dec 13 15:37:08 GMT+0100 2014
Views        : 844
Comments     : 1


+---------+
|  TITLE  |
+---------+
Sheep, Cotswolds, Gloucestershire


+---------------+
|  DESCRIPTION  |
+---------------+
SONY DSC


+--------+
|  TAGS  |
+--------+
sheep dawn cotswolds gloucestershire sony a700 kumweni photograph photo photography picture art light exposure capture shot image snap world earth planet natural history outdoor national 