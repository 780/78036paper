+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : PhillipC
Photo URL    : https://www.flickr.com/photos/flissphil/14608740/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 19 20:24:44 GMT+0200 2005
Upload Date  : Thu May 19 10:27:15 GMT+0200 2005
Views        : 690
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep of the Cause Mejean, Ste. Pierre des Tripiers, Tarn, France 1993


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
sheep tarn france 1993 