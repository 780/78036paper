+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ozzy Delaney
Photo URL    : https://www.flickr.com/photos/24931020@N02/15099125406/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 2 19:09:35 GMT+0200 2014
Upload Date  : Wed Sep 3 00:09:11 GMT+0200 2014
Views        : 5,972
Comments     : 4


+---------+
|  TITLE  |
+---------+
Laughing sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
sheep wool smile baa fleece graving ovine agricultural animal livestock lamb ram ewe happy mammal "Ovis aries" 