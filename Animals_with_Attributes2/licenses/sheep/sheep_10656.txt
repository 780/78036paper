+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kat.dodd
Photo URL    : https://www.flickr.com/photos/katdodd/3449027442/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 13 17:26:28 GMT+0200 2009
Upload Date  : Fri Apr 17 02:16:12 GMT+0200 2009
Geotag Info  : Latitude:52.541702, Longitude:-3.076686
Views        : 8
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep (12)


+---------------+
|  DESCRIPTION  |
+---------------+
13/04/09


+--------+
|  TAGS  |
+--------+
(no tags)