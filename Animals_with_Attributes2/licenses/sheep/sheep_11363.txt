+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KathrynW1
Photo URL    : https://www.flickr.com/photos/kathryn-wright/14075414226/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 3 08:05:37 GMT+0200 2014
Upload Date  : Sat May 3 21:47:54 GMT+0200 2014
Geotag Info  : Latitude:52.030554, Longitude:-1.845703
Views        : 125
Comments     : 0


+---------+
|  TITLE  |
+---------+
Walking to Broadway Tower


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
sheep lamb cotswolds animal 