+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Martin Terber
Photo URL    : https://www.flickr.com/photos/jesuspresley/470978730/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 21 16:20:46 GMT+0200 2007
Upload Date  : Tue Apr 24 09:01:14 GMT+0200 2007
Views        : 186
Comments     : 1


+---------+
|  TITLE  |
+---------+
Rhine Sheep nibbling


+---------------+
|  DESCRIPTION  |
+---------------+
...and grazing


+--------+
|  TAGS  |
+--------+
schaf rhine sheep cologne bike rad radtour pollerwiesen rhodenkirchen poll rhein 2007 april 