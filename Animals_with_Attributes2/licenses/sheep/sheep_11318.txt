+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : grongar
Photo URL    : https://www.flickr.com/photos/grongar/5115142726/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 21 09:02:10 GMT+0200 2010
Upload Date  : Mon Oct 25 19:47:35 GMT+0200 2010
Views        : 159
Comments     : 0


+---------+
|  TITLE  |
+---------+
Three sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
pitlochry sheep scotland 