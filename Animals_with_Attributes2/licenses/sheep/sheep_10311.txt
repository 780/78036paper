+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : amandabhslater
Photo URL    : https://www.flickr.com/photos/pikerslanefarm/7297688522/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 29 14:32:30 GMT+0200 2012
Upload Date  : Tue May 29 23:36:51 GMT+0200 2012
Geotag Info  : Latitude:51.938021, Longitude:-1.834716
Views        : 365
Comments     : 0


+---------+
|  TITLE  |
+---------+
Herdwick Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
Cotswold Farm Park


+--------+
|  TAGS  |
+--------+
"Cotswold Farm Park" Gloucestershire 