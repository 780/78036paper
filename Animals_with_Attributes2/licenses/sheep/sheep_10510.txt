+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : virginsuicide photography
Photo URL    : https://www.flickr.com/photos/bneumann/4914691917/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 20 03:40:08 GMT+0200 2010
Upload Date  : Sun Aug 22 06:39:15 GMT+0200 2010
Views        : 373
Comments     : 0


+---------+
|  TITLE  |
+---------+
New Zealand Native Bush


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"New Zealand" bush sheep 