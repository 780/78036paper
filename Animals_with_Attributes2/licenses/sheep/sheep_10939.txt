+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MShades
Photo URL    : https://www.flickr.com/photos/mshades/3819954273/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 14 13:31:34 GMT+0200 2009
Upload Date  : Fri Aug 14 16:34:42 GMT+0200 2009
Views        : 487
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
One of many sheep hanging out in the pasture.


+--------+
|  TAGS  |
+--------+
Hyogo Kobe Rokko-san sheep 