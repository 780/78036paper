+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : franklin_hunting
Photo URL    : https://www.flickr.com/photos/franklin_hunting/2057775446/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 18 16:29:56 GMT+0100 2007
Upload Date  : Fri Nov 23 18:50:48 GMT+0100 2007
Views        : 110
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sir Edmund & the girls


+---------------+
|  DESCRIPTION  |
+---------------+
Otley, Suffolk


+--------+
|  TAGS  |
+--------+
Sheep ottley suffolk 