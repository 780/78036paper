+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : edenpictures
Photo URL    : https://www.flickr.com/photos/edenpictures/5215919699/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 27 09:44:23 GMT+0100 2010
Upload Date  : Mon Nov 29 02:17:26 GMT+0100 2010
Geotag Info  : Latitude:52.327085, Longitude:5.066409
Views        : 66
Comments     : 0


+---------+
|  TITLE  |
+---------+
Three Sheep I


+---------------+
|  DESCRIPTION  |
+---------------+
These sheep lived on top of a restaurant in Muiden--they seemed as though they were saying, are you the sort of passerby who gives us tasty treats?


+--------+
|  TAGS  |
+--------+
Muiden Netherlands Holland vacation trip Nederland 