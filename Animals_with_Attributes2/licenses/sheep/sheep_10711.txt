+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Janitors
Photo URL    : https://www.flickr.com/photos/janitors/13944975116/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 21 12:29:49 GMT+0200 2014
Upload Date  : Tue Apr 22 13:16:43 GMT+0200 2014
Views        : 539
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep at Riga Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"riga zoo" zoo sheep animal 