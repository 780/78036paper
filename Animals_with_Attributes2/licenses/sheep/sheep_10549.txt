+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aegidian
Photo URL    : https://www.flickr.com/photos/aegidian/4907569096/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 17 13:06:47 GMT+0200 2010
Upload Date  : Thu Aug 19 15:46:12 GMT+0200 2010
Geotag Info  : Latitude:51.152351, Longitude:-2.439458
Views        : 151
Comments     : 0


+---------+
|  TITLE  |
+---------+
P1010616


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Somerset sheep 