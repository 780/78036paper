+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Bev Trayner
Photo URL    : https://www.flickr.com/photos/btrayner/447629653/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 4 17:20:56 GMT+0200 2007
Upload Date  : Thu Apr 5 23:44:38 GMT+0200 2007
Views        : 59
Comments     : 0


+---------+
|  TITLE  |
+---------+
The Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Maison Florence" France 