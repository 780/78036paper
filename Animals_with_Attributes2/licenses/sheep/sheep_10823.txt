+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Todd Slagter
Photo URL    : https://www.flickr.com/photos/toddslagter/163214180/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 16 08:14:43 GMT+0200 2006
Upload Date  : Thu Jun 8 23:08:11 GMT+0200 2006
Views        : 103
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Ireland 2006 "Muckross House" "County Kerry" Travel 