+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BotheredByBees
Photo URL    : https://www.flickr.com/photos/botheredbybees/2745812164/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 3 10:58:58 GMT+0200 2008
Upload Date  : Sat Aug 9 04:21:45 GMT+0200 2008
Views        : 963
Comments     : 0


+---------+
|  TITLE  |
+---------+
marking lambs I


+---------------+
|  DESCRIPTION  |
+---------------+
Last Sunday our friend John invited us around to help mark up a few lambs. We arrived around 10am to find this mob moving slowly back towards the pens


+--------+
|  TAGS  |
+--------+
sheep lamb lambing docking marking farm 