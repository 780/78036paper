+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kitty Terwolbeck
Photo URL    : https://www.flickr.com/photos/kittysfotos/14151953964/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 9 17:27:51 GMT+0200 2014
Upload Date  : Sat May 10 16:13:01 GMT+0200 2014
Views        : 909
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep portrait


+---------------+
|  DESCRIPTION  |
+---------------+
A countryside walk between Amersfoort and Baarn (The Netherlands)

(Trying out my compact camera: Panasonic Lumix TZ40)


+--------+
|  TAGS  |
+--------+
sheep schaap portrait portret head kop ears oren animal dier white wit 