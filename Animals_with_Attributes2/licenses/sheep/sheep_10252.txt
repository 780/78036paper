+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Robb North
Photo URL    : https://www.flickr.com/photos/robbn1/4574299329/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 2 13:54:17 GMT+0200 2010
Upload Date  : Mon May 3 15:54:51 GMT+0200 2010
Geotag Info  : Latitude:42.822602, Longitude:-81.860504
Views        : 11,020
Comments     : 48


+---------+
|  TITLE  |
+---------+
family


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
sheep farmland rural vintage retro feel T4L "les brumes" "Aleeka's Dreams" Painterly 