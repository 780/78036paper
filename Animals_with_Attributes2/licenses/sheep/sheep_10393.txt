+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : A_Peach
Photo URL    : https://www.flickr.com/photos/a_peach/14232175199/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 10 14:23:58 GMT+0200 2014
Upload Date  : Sat Jun 14 14:19:17 GMT+0200 2014
Views        : 1,593
Comments     : 0


+---------+
|  TITLE  |
+---------+
Innocence


+---------------+
|  DESCRIPTION  |
+---------------+
Herdwick lamb


+--------+
|  TAGS  |
+--------+
2014 Sussex "Panasonic Lumix G5" "Panasonic 100-300mm" lamb herdwick England 