+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : merriehaskell
Photo URL    : https://www.flickr.com/photos/merriehaskell/2275754853/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 11 05:07:02 GMT+0200 2005
Upload Date  : Tue Feb 19 05:32:05 GMT+0100 2008
Geotag Info  : Latitude:53.832814, Longitude:-1.955652
Views        : 319
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Haworth England sheep vacations 