+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : grassrootsgroundswell
Photo URL    : https://www.flickr.com/photos/grassrootsgroundswell/13908624598/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 4 14:01:09 GMT+0200 2014
Upload Date  : Sat May 3 12:56:57 GMT+0200 2014
Views        : 512
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ewe with lambs


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
ewe lambs sheep 