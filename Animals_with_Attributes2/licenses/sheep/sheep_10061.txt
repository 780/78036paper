+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Massachusetts Office of Travel & Tourism
Photo URL    : https://www.flickr.com/photos/masstravel/11339092994/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Oct 12 10:23:56 GMT+0200 2011
Upload Date  : Thu Dec 12 15:28:42 GMT+0100 2013
Geotag Info  : Latitude:41.343051, Longitude:-70.744485
Views        : 2,754
Comments     : 0


+---------+
|  TITLE  |
+---------+
Allen Sheep Farm, Chilmark


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Allen Sheep Farm" Chilmark "Martha's Vineyard" "Cape Cod" Massachusetts MA sheep farm lamb outdoors island 