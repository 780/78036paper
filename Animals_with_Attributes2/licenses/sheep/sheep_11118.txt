+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DavidGeen
Photo URL    : https://www.flickr.com/photos/dgeen/14576293501/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 7 20:17:06 GMT+0200 2013
Upload Date  : Sat Jul 5 17:03:34 GMT+0200 2014
Views        : 268
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
sheep nature Bude Cornwall 