+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : WayShare
Photo URL    : https://www.flickr.com/photos/47042618@N06/5324961256/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 31 13:02:48 GMT+0200 2010
Upload Date  : Tue Jan 4 21:43:18 GMT+0100 2011
Views        : 301
Comments     : 3


+---------+
|  TITLE  |
+---------+
Close encounter with sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
derbyshire peak district baslow sheep 