+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Matt Brittaine
Photo URL    : https://www.flickr.com/photos/mattbrittaine/5589896248/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 3 12:02:51 GMT+0200 2011
Upload Date  : Mon Apr 4 20:56:52 GMT+0200 2011
Views        : 669
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
lamb sheep farm lambing mattbrittaine askett "princes risborough" 