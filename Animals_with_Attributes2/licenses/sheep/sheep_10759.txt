+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Fred Hsu
Photo URL    : https://www.flickr.com/photos/fhsu/2159984826/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 21 17:55:44 GMT+0100 2007
Upload Date  : Wed Jan 2 23:39:18 GMT+0100 2008
Views        : 325
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep in the field


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
newzealand nz travel outdoors nature mountains sheep 