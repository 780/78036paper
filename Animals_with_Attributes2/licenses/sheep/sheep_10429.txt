+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ☺ Lee J Haywood
Photo URL    : https://www.flickr.com/photos/leehaywood/4528493691/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 17 11:31:50 GMT+0200 2010
Upload Date  : Sat Apr 17 21:42:26 GMT+0200 2010
Views        : 526
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lambs and sheep


+---------------+
|  DESCRIPTION  |
+---------------+
Taken in the Peak District, around Monsal Dale.


+--------+
|  TAGS  |
+--------+
"Peak District" "Monsal Dale" sheep lambs field grass 