+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MICOLO J Thanx 4, 2.2 million+ views
Photo URL    : https://www.flickr.com/photos/robin1966/17300788605/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 28 20:27:15 GMT+0200 2015
Upload Date  : Tue Apr 28 15:04:03 GMT+0200 2015
Views        : 888
Comments     : 5


+---------+
|  TITLE  |
+---------+
Lamb investigating


+---------------+
|  DESCRIPTION  |
+---------------+
Curious young lamb having a look at the field machinery and trailers


+--------+
|  TAGS  |
+--------+
lambs sheep farmanimals 