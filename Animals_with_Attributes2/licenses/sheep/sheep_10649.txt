+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jennifer Whiting
Photo URL    : https://www.flickr.com/photos/jennywhitingnz/15199370156/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Oct 2 13:09:34 GMT+0200 2013
Upload Date  : Sat Sep 13 08:51:45 GMT+0200 2014
Views        : 1,637
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep - Ambury Regional Park - Mangere Bridge - Auckland - New Zealand


+---------------+
|  DESCRIPTION  |
+---------------+
Sheep -- Ambury Farm -- Ambury Regional Park -- Mangere Bridge -- New Zealand -- 2nd October 2013
#sheep #amburyfarm #amburyregionalpark #mangerebridge #auckland #newzealand
Picture taken by: Jennifer Whiting


+--------+
|  TAGS  |
+--------+
sheep amburfarm amburyregionalpark mangerebridge auckland newzealand 