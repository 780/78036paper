+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Posicionamiento WEB
Photo URL    : https://www.flickr.com/photos/62113742@N08/5676970165/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 29 11:28:48 GMT+0200 2011
Upload Date  : Sun May 1 23:34:14 GMT+0200 2011
Views        : 2,468
Comments     : 0


+---------+
|  TITLE  |
+---------+
Highland black head sheep with lamb


+---------------+
|  DESCRIPTION  |
+---------------+
Highland black head sheep with lamb


+--------+
|  TAGS  |
+--------+
lamb black head sheep scotland highland 