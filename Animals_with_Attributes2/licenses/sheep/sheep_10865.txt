+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : C Pirate
Photo URL    : https://www.flickr.com/photos/cpirate/2139978621/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 6 11:29:48 GMT+0200 2007
Upload Date  : Thu Dec 27 08:52:05 GMT+0100 2007
Views        : 44
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
A shaggy sheep.


+--------+
|  TAGS  |
+--------+
travel stansted britain 