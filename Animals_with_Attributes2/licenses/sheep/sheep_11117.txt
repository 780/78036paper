+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flowcomm
Photo URL    : https://www.flickr.com/photos/flowcomm/14515466563/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 25 13:49:17 GMT+0200 2014
Upload Date  : Tue Jun 24 09:07:54 GMT+0200 2014
Views        : 101
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Stowe "National Trust" Buckinghamshire England "United Kingdom" 