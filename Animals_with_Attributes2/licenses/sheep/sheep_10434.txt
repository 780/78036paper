+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : andyspictures
Photo URL    : https://www.flickr.com/photos/andyspicturesurl/3487423382/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 12 15:10:25 GMT+0200 2009
Upload Date  : Thu Apr 30 00:50:45 GMT+0200 2009
Views        : 260
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lambs near Skipton


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Lamb Sheep 