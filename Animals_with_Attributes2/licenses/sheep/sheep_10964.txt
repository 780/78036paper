+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MarilynJane
Photo URL    : https://www.flickr.com/photos/marilynjane/4370498327/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Feb 19 12:28:41 GMT+0100 2010
Upload Date  : Fri Feb 19 21:00:34 GMT+0100 2010
Views        : 3,354
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
Friendly sheep who came to say hello!


+--------+
|  TAGS  |
+--------+
Sheep "Farm Animals" Animals 