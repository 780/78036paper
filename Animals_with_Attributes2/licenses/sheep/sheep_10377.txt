+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stephenshellard
Photo URL    : https://www.flickr.com/photos/stephen_shellard/7018529119/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 25 14:35:07 GMT+0200 2012
Upload Date  : Mon Mar 26 21:19:37 GMT+0200 2012
Views        : 162
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_8169


+---------------+
|  DESCRIPTION  |
+---------------+
Location: Burnett, United Kingdom.


+--------+
|  TAGS  |
+--------+
field grass sheep white dirty 