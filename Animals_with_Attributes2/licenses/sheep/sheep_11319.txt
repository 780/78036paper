+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jronaldlee
Photo URL    : https://www.flickr.com/photos/jronaldlee/4539837347/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 17 16:10:45 GMT+0200 2010
Upload Date  : Wed Apr 21 11:52:33 GMT+0200 2010
Views        : 901
Comments     : 0


+---------+
|  TITLE  |
+---------+
three-sheep


+---------------+
|  DESCRIPTION  |
+---------------+
This photo free for use under a Creative Commons <a href="http://jronaldlee.com/copyright/" rel="nofollow">copyright</a> (see link for details). If you have time, I enjoy it when I am left a note saying where the picture was used.


+--------+
|  TAGS  |
+--------+
sheep wool shorn farm graze grazing grass fence 