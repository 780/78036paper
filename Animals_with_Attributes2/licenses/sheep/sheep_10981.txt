+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : lo_ise
Photo URL    : https://www.flickr.com/photos/lo_ise/4980991722/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Sep 9 04:50:47 GMT+0200 2010
Upload Date  : Sun Sep 12 00:48:07 GMT+0200 2010
Views        : 304
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sheep


+---------------+
|  DESCRIPTION  |
+---------------+
Lake District, UK


+--------+
|  TAGS  |
+--------+
Sheep "lake district" "wasdale head" cumbria nikon d300 