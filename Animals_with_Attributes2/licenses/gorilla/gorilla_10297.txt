+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : golgarth
Photo URL    : https://www.flickr.com/photos/golgarth/7843641512/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 20 13:52:25 GMT+0200 2012
Upload Date  : Thu Aug 23 12:00:07 GMT+0200 2012
Views        : 80
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gorilla Playfights


+---------------+
|  DESCRIPTION  |
+---------------+
Lucky capture so not a perfect shot, lovely to see &quot;Dad&quot; being so gentle with his little son (though it doesn't look like it)


+--------+
|  TAGS  |
+--------+
(no tags)