+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Joanne Goldby
Photo URL    : https://www.flickr.com/photos/jovamp/15156701537/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 16 13:36:59 GMT+0200 2014
Upload Date  : Wed Sep 24 19:30:20 GMT+0200 2014
Views        : 157
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_8369.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Durrell Wildlife Conservation Trust" "Durrell Wildlife Park" Gorilla Jersey 