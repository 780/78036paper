+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael Seeley
Photo URL    : https://www.flickr.com/photos/mseeley1/8199949235/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 11 11:04:50 GMT+0100 2012
Upload Date  : Mon Nov 19 20:51:09 GMT+0100 2012
Views        : 223
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
disneysanimalkingdom animalkingdom disneyworld disney florida zoo gorilla gorillas animal wildlife Michael Seeley "Michael Seeley" "Mike Seeley" Mike 