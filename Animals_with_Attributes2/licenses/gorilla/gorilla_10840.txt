+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mybulldog
Photo URL    : https://www.flickr.com/photos/1967chevrolet/4861368100/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 2 15:26:43 GMT+0200 2010
Upload Date  : Wed Aug 4 22:52:21 GMT+0200 2010
Views        : 1,700
Comments     : 32


+---------+
|  TITLE  |
+---------+
Western Lowland Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Animals Zoo Seattle "Washington State" D90 Nikon Sigma "Sigma 120 400" gorilla face "western lowland gorilla" "Woodland Park Zoo" mywinners 