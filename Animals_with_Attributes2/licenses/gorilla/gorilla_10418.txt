+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : USFWS Headquarters
Photo URL    : https://www.flickr.com/photos/usfwshq/6461814929/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 5 22:31:03 GMT+0200 2008
Upload Date  : Mon Dec 5 21:42:44 GMT+0100 2011
Views        : 360
Comments     : 1


+---------+
|  TITLE  |
+---------+
Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
Credit: USFWS


+--------+
|  TAGS  |
+--------+
(no tags)