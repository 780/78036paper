+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : antonikon
Photo URL    : https://www.flickr.com/photos/antonikon/14569079847/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 27 11:27:53 GMT+0200 2014
Upload Date  : Sun Jul 27 10:54:47 GMT+0200 2014
Geotag Info  : Latitude:47.246726, Longitude:1.349086
Views        : 569
Comments     : 0


+---------+
|  TITLE  |
+---------+
beauval_080714_075.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
gorille femelle
zoo de Beauval


+--------+
|  TAGS  |
+--------+
gorille "zoo de Beauval" beauval singe gorilla 