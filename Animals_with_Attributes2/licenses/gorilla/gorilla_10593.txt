+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : asokolik
Photo URL    : https://www.flickr.com/photos/asokolik/8004667111/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 5 13:32:29 GMT+0200 2012
Upload Date  : Thu Sep 20 02:25:06 GMT+0200 2012
Geotag Info  : Latitude:42.938218, Longitude:-78.852374
Views        : 126
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_9625


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Buffalo Zoological Gardens" "Buffalo Zoo" "Lowland Gorilla" "Gorilla gorilla gorilla" 