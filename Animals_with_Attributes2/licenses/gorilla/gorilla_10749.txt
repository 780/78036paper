+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Castles, Capes & Clones
Photo URL    : https://www.flickr.com/photos/lorenjavier/5016317143/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 21 19:31:50 GMT+0200 2010
Upload Date  : Thu Sep 23 06:46:49 GMT+0200 2010
Views        : 1,376
Comments     : 0


+---------+
|  TITLE  |
+---------+
Silverback Gorilla on the Pangani Forest Exploration Trail


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Florida "Lake Buena Vista" "Walt Disney World" "Walt Disney World Resort" "Animal Kingdom" "Disney's Animal Kingdom" Africa "Pangani Forest Exploration Trail" "Silverback Gorilla" 