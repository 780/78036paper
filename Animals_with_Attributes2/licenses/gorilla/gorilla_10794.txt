+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gudi&cris
Photo URL    : https://www.flickr.com/photos/gudi3101/4370874592/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 25 09:49:20 GMT+0100 2009
Upload Date  : Fri Feb 19 18:00:05 GMT+0100 2010
Views        : 485
Comments     : 0


+---------+
|  TITLE  |
+---------+
Uganda 2009


+---------------+
|  DESCRIPTION  |
+---------------+
Bwindi impenetrable forest, gorilla


+--------+
|  TAGS  |
+--------+
gorilla "bwindi forest" uganda 