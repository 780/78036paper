+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eric Kilby
Photo URL    : https://www.flickr.com/photos/ekilby/24009449601/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 24 11:46:31 GMT+0100 2015
Upload Date  : Thu Dec 31 23:10:48 GMT+0100 2015
Geotag Info  : Latitude:42.305272, Longitude:-71.088958
Views        : 303
Comments     : 0


+---------+
|  TITLE  |
+---------+
Four Gorillas Together


+---------------+
|  DESCRIPTION  |
+---------------+
look for the baby on mom's leg


+--------+
|  TAGS  |
+--------+
franklin park zoo boston primate ape gorilla group 