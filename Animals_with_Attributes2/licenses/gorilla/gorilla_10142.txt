+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : dvanhorn
Photo URL    : https://www.flickr.com/photos/dvanhorn/5178888620/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Nov 2 12:37:05 GMT+0100 2010
Upload Date  : Mon Nov 15 16:02:34 GMT+0100 2010
Geotag Info  : Latitude:42.302871, Longitude:-71.086303
Views        : 50
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_0136


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)