+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael Seeley
Photo URL    : https://www.flickr.com/photos/mseeley1/8199949921/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 11 11:09:10 GMT+0100 2012
Upload Date  : Mon Nov 19 20:51:26 GMT+0100 2012
Views        : 348
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animal animalkingdom disney disneysanimalkingdom disneyworld florida gorilla gorillas wildlife zoo Michael Seeley "Michael Seeley" "Mike Seeley" Mike 