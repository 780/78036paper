+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : justinbaeder
Photo URL    : https://www.flickr.com/photos/justinbaeder/39677695/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 2 17:29:45 GMT+0200 2005
Upload Date  : Sat Sep 3 02:29:45 GMT+0200 2005
Views        : 348
Comments     : 0


+---------+
|  TITLE  |
+---------+
Lowland Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
gorilla sandiegozoo zoo wap sandiegowap wildanimalpark sandiegowildanimalpark sandiegozoowildanimalpark 