+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/15368916487/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 5 14:26:30 GMT+0200 2014
Upload Date  : Fri Oct 17 11:00:07 GMT+0200 2014
Geotag Info  : Latitude:49.244640, Longitude:6.137795
Views        : 18,064
Comments     : 5


+---------+
|  TITLE  |
+---------+
Unhappy big gorilla eating


+---------------+
|  DESCRIPTION  |
+---------------+
The silverback was sitting and eating, and showing one shouldn't disturb him...


+--------+
|  TAGS  |
+--------+
silverback older angry "pissed off" eating sitting plants portrait face gorilla black primate monkey ape amnéville zoo france nikon d4 