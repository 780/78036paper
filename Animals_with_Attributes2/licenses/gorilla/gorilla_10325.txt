+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Barcelona.cat
Photo URL    : https://www.flickr.com/photos/barcelona_cat/6801010997/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Feb 1 13:46:25 GMT+0100 2012
Upload Date  : Wed Feb 1 13:46:25 GMT+0100 2012
Views        : 884
Comments     : 0


+---------+
|  TITLE  |
+---------+
GORIL·LA VIRUNGA


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"zoo de barcelona" zoo barcelona gorila goril·la virunga "floquet de neu" 