+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Pauls Imaging Photography
Photo URL    : https://www.flickr.com/photos/paulsimaging/8899996615/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 25 15:57:28 GMT+0200 2013
Upload Date  : Fri May 31 12:45:15 GMT+0200 2013
Views        : 184
Comments     : 1


+---------+
|  TITLE  |
+---------+
DSC_0116


+---------------+
|  DESCRIPTION  |
+---------------+
Various photos of animals at Blackpool zoo.


+--------+
|  TAGS  |
+--------+
wildlife "pauls imaging" lion gorilla portrait monkey seal otter 