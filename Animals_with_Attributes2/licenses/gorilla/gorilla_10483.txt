+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : macinate
Photo URL    : https://www.flickr.com/photos/macinate/2440911062/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 13 10:34:29 GMT+0200 2008
Upload Date  : Fri Apr 25 13:40:01 GMT+0200 2008
Views        : 951
Comments     : 0


+---------+
|  TITLE  |
+---------+
Grassy gorilla whisp


+---------------+
|  DESCRIPTION  |
+---------------+
Couldn't resist this one!


+--------+
|  TAGS  |
+--------+
"Royal Melbourne Zoo" lowland gorilla animal 