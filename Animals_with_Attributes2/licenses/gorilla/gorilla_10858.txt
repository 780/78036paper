+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Carine06
Photo URL    : https://www.flickr.com/photos/43555660@N00/8155788471/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 8 09:33:33 GMT+0200 2012
Upload Date  : Mon Nov 5 00:38:25 GMT+0100 2012
Views        : 1,048
Comments     : 0


+---------+
|  TITLE  |
+---------+
Young mountain gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
Sabyinyo group, Volcanoes National Park, Rwanda.


+--------+
|  TAGS  |
+--------+
gorilla "mountain gorilla" Rwanda Sabyinyo ape primate "great ape" Africa "Volcanoes National Park" "Parc National des Volcans" 