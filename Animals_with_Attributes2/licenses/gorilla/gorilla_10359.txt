+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : malfet_
Photo URL    : https://www.flickr.com/photos/malfet/2443108250/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 26 08:03:37 GMT+0200 2008
Upload Date  : Sat Apr 26 15:18:10 GMT+0200 2008
Geotag Info  : Latitude:35.648090, Longitude:140.085039
Views        : 2,697
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
gorilla monkey zoo Chiba 千葉市動物公園 