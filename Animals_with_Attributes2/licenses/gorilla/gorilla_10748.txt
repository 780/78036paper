+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Heather.Fischer6
Photo URL    : https://www.flickr.com/photos/28770851@N05/16727271598/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 1 15:59:18 GMT+0100 2015
Upload Date  : Tue Mar 24 11:06:39 GMT+0100 2015
Views        : 405
Comments     : 0


+---------+
|  TITLE  |
+---------+
Silverback Gorilla at Ragunan Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Ragunan Zoo in Jakarta has a really great primate center where the primates can roam freely.


+--------+
|  TAGS  |
+--------+
gorilla jakarta ragunan zoo 