+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : tiannaspicer
Photo URL    : https://www.flickr.com/photos/tiannaspicer/8618137787/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 18 15:16:02 GMT+0200 2012
Upload Date  : Thu Apr 4 14:41:15 GMT+0200 2013
Views        : 556
Comments     : 0


+---------+
|  TITLE  |
+---------+
Silverback Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
Captured in Loro Parque, Tenerife. Shot through glass


+--------+
|  TAGS  |
+--------+
image Silverback Gorilla Loro Parque Primate 