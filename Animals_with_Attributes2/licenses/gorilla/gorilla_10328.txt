+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Barcelona.cat
Photo URL    : https://www.flickr.com/photos/barcelona_cat/6801012467/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 10 20:06:42 GMT+0100 2006
Upload Date  : Wed Feb 1 13:46:51 GMT+0100 2012
Views        : 848
Comments     : 0


+---------+
|  TITLE  |
+---------+
GORIL·LA VIRUNGA


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"zoo de barcelona" zoo barcelona gorila goril·la virunga "floquet de neu" 