+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : girlzilla09
Photo URL    : https://www.flickr.com/photos/girlzilla09/6440500637/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 1 23:56:17 GMT+0100 2011
Upload Date  : Fri Dec 2 08:56:17 GMT+0100 2011
Views        : 752
Comments     : 11


+---------+
|  TITLE  |
+---------+
Kinyani (Foreground) and Ivan (Background)


+---------------+
|  DESCRIPTION  |
+---------------+
This is a picture that I snapped of Ivan and Kinyani when they were still together in April of last year. Kinyani now lives in Columbus, Ohio with a silverback named Oliver. Both Kinyani and Oliver are deaf.

Ivan was born in the wild in 1962 and has been at Zoo Atlanta since 1994. At nearly 50 years of age Ivan is one of four very special senior citizens that currenly reside at Zoo Atlanta. The other three are Shamba (Female, 52), Ozoum aka &quot;Ozzie&quot; (Male, 50), Choomba (Female, 48)


+--------+
|  TAGS  |
+--------+
ZooAtlanta 