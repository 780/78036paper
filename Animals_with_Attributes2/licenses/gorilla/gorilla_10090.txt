+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rich Bard
Photo URL    : https://www.flickr.com/photos/reech00/4589710232/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 24 18:02:53 GMT+0200 2010
Upload Date  : Sat May 8 19:48:07 GMT+0200 2010
Views        : 210
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bronx Zoo Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
I'm not sure who this is, but I love the contemplate look on her face. It was really great to see familiar faces at the gorilla exhibit after being gone from there for so many years.


+--------+
|  TAGS  |
+--------+
animal wildlife 