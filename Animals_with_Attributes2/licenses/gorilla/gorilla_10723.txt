+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Carodean Road Designs
Photo URL    : https://www.flickr.com/photos/carodeanroaddesigns/5773835237/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 11 23:10:20 GMT+0100 2011
Upload Date  : Mon May 30 04:13:25 GMT+0200 2011
Views        : 385
Comments     : 0


+---------+
|  TITLE  |
+---------+
San Diego Safari Park


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"safari park" "san diego" animals elephant girrafe gorilla parrot flamingo duck cheetah 