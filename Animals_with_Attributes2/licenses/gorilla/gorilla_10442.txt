+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ptc24
Photo URL    : https://www.flickr.com/photos/ptc24/14845774040/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Aug 19 12:09:12 GMT+0200 2014
Upload Date  : Mon Aug 25 17:09:29 GMT+0200 2014
Views        : 2,111
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
The Western Lowland Gorilla, Gorilla gorilla gorilla


+--------+
|  TAGS  |
+--------+
"Panasonic Lumix DMC-TZ60" "Port Lympne" animal zoo 