+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : RobBixbyPhotography
Photo URL    : https://www.flickr.com/photos/scubabix/16627307680/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 14 16:00:22 GMT+0100 2015
Upload Date  : Sat Mar 14 21:12:08 GMT+0100 2015
Geotag Info  : Latitude:30.399946, Longitude:-81.643763
Views        : 375
Comments     : 0


+---------+
|  TITLE  |
+---------+
JaxZoo_3-13-15-8796


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Florida Jacksonville Zoo animals 