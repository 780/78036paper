+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : charlieishere@btinternet.com
Photo URL    : https://www.flickr.com/photos/100915417@N07/17997840570/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 16 12:14:29 GMT+0200 2015
Upload Date  : Thu May 28 07:29:16 GMT+0200 2015
Views        : 3,540
Comments     : 0


+---------+
|  TITLE  |
+---------+
Gorilla Profile


+---------------+
|  DESCRIPTION  |
+---------------+
It was a very busy Saturday at Bristol Zoo and most of the gorillas were staying safely in their house away from all the noisy human beings. This one came out and posed for photographers for a few minutes.


+--------+
|  TAGS  |
+--------+
(no tags)