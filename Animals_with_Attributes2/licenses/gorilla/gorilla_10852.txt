+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gvgoebel
Photo URL    : https://www.flickr.com/photos/37467370@N08/7630603616/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 6 09:45:06 GMT+0200 2008
Upload Date  : Mon Jul 23 18:12:37 GMT+0200 2012
Views        : 471
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ybgor_2b


+---------------+
|  DESCRIPTION  |
+---------------+
gorilla with baby, Zoo Atlanta, Georgia / 2008


+--------+
|  TAGS  |
+--------+
animals mammals ape gorilla 