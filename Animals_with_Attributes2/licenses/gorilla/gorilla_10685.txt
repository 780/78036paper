+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : pelican
Photo URL    : https://www.flickr.com/photos/pelican/1505359832/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 7 13:36:21 GMT+0200 2007
Upload Date  : Sun Oct 7 13:47:15 GMT+0200 2007
Views        : 1,143
Comments     : 1


+---------+
|  TITLE  |
+---------+
Oji zoo, Kobe, Japan


+---------------+
|  DESCRIPTION  |
+---------------+
&quot;What did you say?&quot;


+--------+
|  TAGS  |
+--------+
k100d "sigma apo 70-300mm F4-5.6" oji zoo kobe japan gorilla ape oct07_oji 