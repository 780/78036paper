+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : alwright1
Photo URL    : https://www.flickr.com/photos/alwright1/3565665900/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 23 00:00:34 GMT+0200 2009
Upload Date  : Tue May 26 05:26:46 GMT+0200 2009
Geotag Info  : Latitude:29.921322, Longitude:-90.130269
Views        : 1,084
Comments     : 0


+---------+
|  TITLE  |
+---------+
Silverback Gorilla


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"New Orleans, LA" NOLA "Audubon Zoo" "silverback gorilla" 