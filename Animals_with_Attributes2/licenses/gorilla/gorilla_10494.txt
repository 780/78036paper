+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ettore Balocchi
Photo URL    : https://www.flickr.com/photos/29882791@N02/8299217043/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 12 12:27:15 GMT+0200 2008
Upload Date  : Sun Dec 23 13:31:05 GMT+0100 2012
Views        : 80
Comments     : 0


+---------+
|  TITLE  |
+---------+
Hominidae - Gorilla gorilla (Lowland Gorilla)


+---------------+
|  DESCRIPTION  |
+---------------+
Lowlands gorilla - Gorilla gorilla


+--------+
|  TAGS  |
+--------+
(no tags)