+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sara&Joachim
Photo URL    : https://www.flickr.com/photos/sara_joachim/2042417695/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 16 09:07:05 GMT+0100 2007
Upload Date  : Sun Nov 18 11:58:35 GMT+0100 2007
Geotag Info  : Latitude:-1.542019, Longitude:29.485759
Views        : 985
Comments     : 1


+---------+
|  TITLE  |
+---------+
DSC_0960


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Rwanda Africa Wildlife "Parc National des Volcans" Mountain Gorilla "mountain gorilla" ruhengeri 