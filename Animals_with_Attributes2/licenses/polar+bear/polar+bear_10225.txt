+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : daniel.wittberger
Photo URL    : https://www.flickr.com/photos/wittberger/329441543/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 27 13:43:11 GMT+0200 2006
Upload Date  : Thu Dec 21 22:38:46 GMT+0100 2006
Views        : 11
Comments     : 0


+---------+
|  TITLE  |
+---------+
Eisbär


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)