+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : beingmyself
Photo URL    : https://www.flickr.com/photos/20406121@N04/2222670744/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jan 26 15:23:55 GMT+0100 2007
Upload Date  : Sun Jan 27 08:05:58 GMT+0100 2008
Views        : 7,803
Comments     : 1


+---------+
|  TITLE  |
+---------+
polar bears cuddilng


+---------------+
|  DESCRIPTION  |
+---------------+
Apart from naps, most polar bears sleep for seven to eight hours at a stretch, about like humans do. In one study of their sleeping habits, the bears slept an average of seven hours and 45 minutes.

Sleepy bear On the ice in spring and summer, polar bears tend to sleep more during the day than at night. In the Arctic, however, the words &quot;day&quot; and &quot;night&quot; hold little meaning. Most polar bears live in a land with 24 hours of daylight in summer and 24 hours of darkness in winter.

Polar bears may sleep more during the day because seals are more active at night.

In addition to longer sleeps, polar bears like to nap. They will nap just about anywhere and at any time.

Napping helps the bears conserve energy. Canadian scientist Ian Stirling has noted that the polar bear's entire existence is centered around hunting and the conservation of energy.

Polar bears usually nap for an hour or so after feeding on a seal.

In winter, polar bears dig shallow pits in the snow in which to sleep. They prefer to sleep with their sides or backs to the wind.

When a blizzard hits, polar bears sleep through the storm in a day bed, frequently dug in the lee of a ridge. They often awake covered with a thick blanket of snow. The bears may remain curled up under the snow for several days until the storm passes.

In summer, polar bears will curl up to sleep on the tundra or on an ice floe, sometimes using a block of ice or an outstretched paw as a pillow. Along the shores of Hudson Bay in summer, landlocked bears dig sleeping pits in the sand or in gravel ridges along the beach.

During the fall congregation of polar bears at Cape Churchill, male bears who play together for hours may later sleep together in the same snow pit. Mothers and cubs also curl up together, but well away from the large male bears.

Pregnant female polar bears dig a maternity den in a snow bank in fall. They give birth to their cubs in early winter and emerge from the den in March or April. When they are in the den, the cubs and their mother spend much of their time sleeping.


+--------+
|  TAGS  |
+--------+
(no tags)