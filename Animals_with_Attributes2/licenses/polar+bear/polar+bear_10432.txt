+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DDohler
Photo URL    : https://www.flickr.com/photos/ddohler/15575242119/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 8 22:55:17 GMT+0100 2014
Upload Date  : Tue Nov 11 01:28:41 GMT+0100 2014
Views        : 739
Comments     : 0


+---------+
|  TITLE  |
+---------+
Philly Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animal "polar bear" 