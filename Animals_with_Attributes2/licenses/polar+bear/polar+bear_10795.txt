+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : em_j_bishop
Photo URL    : https://www.flickr.com/photos/emmabishop/6356518081/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Nov 9 11:43:52 GMT+0100 2011
Upload Date  : Fri Nov 18 04:34:26 GMT+0100 2011
Views        : 1,113
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Churchill Manitoba Canada "November 2011" "polar bears" "Wapusk National Park" "Churchill Wildlife Management Area" "Hudson Bay" tunda "tundra buggy tour" 