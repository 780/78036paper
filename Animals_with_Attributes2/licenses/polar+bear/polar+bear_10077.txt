+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ju2style
Photo URL    : https://www.flickr.com/photos/127017951@N02/15493930528/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 1 10:00:37 GMT+0100 2014
Upload Date  : Sat Nov 1 08:18:03 GMT+0100 2014
Views        : 752
Comments     : 4


+---------+
|  TITLE  |
+---------+
A little sleepy（polar bear003）


+---------------+
|  DESCRIPTION  |
+---------------+
so cute!/zoo/animal/japan/2014.11


+--------+
|  TAGS  |
+--------+
polarbear e-pl3 olympus microfourthirds maruyamazoo zoo japan hokkaido animal animals bear ホッキョクグマ 円山動物園 動物園 動物 オリンパス 日本 北海道 