+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : omninate
Photo URL    : https://www.flickr.com/photos/nate_kate/503256288/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 13 10:09:04 GMT+0200 2007
Upload Date  : Fri May 18 15:36:47 GMT+0200 2007
Views        : 1,716
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar Bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Seneca Park Zoo" Seneca Park Zoo "Monroe County Zoo" Monroe County "Rochester, NY" Rochester "Mothers Day" "Polar Bear" 