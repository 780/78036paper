+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : denn
Photo URL    : https://www.flickr.com/photos/denn/2361029496/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 22 14:56:43 GMT+0100 2008
Upload Date  : Tue Mar 25 11:25:02 GMT+0100 2008
Geotag Info  : Latitude:35.478704, Longitude:139.619407
Views        : 852
Comments     : 0


+---------+
|  TITLE  |
+---------+
polar bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
denn travel japan yokohama aquarium polar bear 