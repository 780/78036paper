+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Halvard O. Grønning H. Figenschoue Gai
Photo URL    : https://www.flickr.com/photos/jarld/14537656836/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jul 2 21:52:52 GMT+0200 2014
Upload Date  : Thu Jul 3 00:33:00 GMT+0200 2014
Geotag Info  : Latitude:63.317552, Longitude:10.195905
Views        : 17
Comments     : 0


+---------+
|  TITLE  |
+---------+
Isbjørn


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)