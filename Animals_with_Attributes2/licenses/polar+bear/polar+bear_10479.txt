+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : torbakhopper
Photo URL    : https://www.flickr.com/photos/gazeronly/8094209697/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 14 12:55:59 GMT+0200 2011
Upload Date  : Tue Oct 16 18:40:02 GMT+0200 2012
Views        : 2,837
Comments     : 2


+---------+
|  TITLE  |
+---------+
polar bear jaw extension


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
polar bear jaw width 