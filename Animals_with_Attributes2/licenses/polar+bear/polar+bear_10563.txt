+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kevin Burkett
Photo URL    : https://www.flickr.com/photos/kevinwburkett/2932369361/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 11 16:30:23 GMT+0200 2008
Upload Date  : Sun Oct 12 03:04:55 GMT+0200 2008
Geotag Info  : Latitude:39.972055, Longitude:-75.195794
Views        : 374
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar Bear


+---------------+
|  DESCRIPTION  |
+---------------+
The Philadelphia Zoo, Saturday, Oct. 11, 2008


+--------+
|  TAGS  |
+--------+
nature philadelphia pennsylvania zoo animals 