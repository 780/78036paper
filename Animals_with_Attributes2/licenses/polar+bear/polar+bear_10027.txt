+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : markbyzewski
Photo URL    : https://www.flickr.com/photos/markbyzewski/9768103454/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 11 08:45:04 GMT+0200 2013
Upload Date  : Mon Sep 16 06:44:25 GMT+0200 2013
Geotag Info  : Latitude:39.749630, Longitude:-104.947153
Views        : 159
Comments     : 0


+---------+
|  TITLE  |
+---------+
_MG_5462


+---------------+
|  DESCRIPTION  |
+---------------+
A day at the Denver Zoo.


+--------+
|  TAGS  |
+--------+
Denver Zoo Colorado Ugly 