+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sauri
Photo URL    : https://www.flickr.com/photos/sauri/101557195/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 28 00:00:15 GMT+0200 2005
Upload Date  : Sun Jul 31 22:00:15 GMT+0200 2005
Views        : 297
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar bear


+---------------+
|  DESCRIPTION  |
+---------------+
Moscow Zoo, 28 Jul 2005.


+--------+
|  TAGS  |
+--------+
moscow russia animals 