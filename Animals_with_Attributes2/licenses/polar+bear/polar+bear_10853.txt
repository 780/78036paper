+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Günter Hentschel
Photo URL    : https://www.flickr.com/photos/v230gh/8531963864/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 2 13:31:00 GMT+0100 2013
Upload Date  : Tue Mar 5 18:17:28 GMT+0100 2013
Views        : 208
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zoo Wuppertal (D)


+---------------+
|  DESCRIPTION  |
+---------------+
<a href="http://www.zoo-wuppertal.de/home.html" rel="nofollow">www.zoo-wuppertal.de/home.html</a>

<a href="http://www.zoo-wuppertal.de/zoo-wuppertal/besucherservice.html#c2182" rel="nofollow">www.zoo-wuppertal.de/zoo-wuppertal/besucherservice.html#c...</a>

Schade das der Zoo verlangt das die Bilder nur mit 72,dpi hier gezeigt werden dürfen.Das ist eigentlich der einzige Nachteil von sonst einem Wunderschönen Tiergarten.


+--------+
|  TAGS  |
+--------+
Zoo Deutschland Germany Europa Wuppertal.Tiergarten Löwe Eisbär Tiger Zebra Pinguin Wasser I-Phone Nikon "D 40" "Nikon D40" Tiere Raubtiere Raubkatzen Vögel 