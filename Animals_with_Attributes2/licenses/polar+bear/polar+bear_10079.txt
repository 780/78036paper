+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : young shanahan
Photo URL    : https://www.flickr.com/photos/youngshanahan/20036753581/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 26 19:16:11 GMT+0200 2015
Upload Date  : Sun Jul 26 20:46:35 GMT+0200 2015
Views        : 283
Comments     : 1


+---------+
|  TITLE  |
+---------+
A Polar Bear.


+---------------+
|  DESCRIPTION  |
+---------------+
He didn't seem like a particularly happy polar bear, guess a small enclosure in a zoo in central Prague in the middle of summer a bit depressing if you're a polar bear.

Prague Zoo, Prague.


+--------+
|  TAGS  |
+--------+
"czech republic" prague praha "prague zoo" "deltoids go to the zoo" 