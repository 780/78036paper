+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Darnea
Photo URL    : https://www.flickr.com/photos/andreasbestefotos/2321369513/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Oct 3 14:47:10 GMT+0200 2007
Upload Date  : Sun Mar 9 21:34:05 GMT+0100 2008
Geotag Info  : Latitude:52.508150, Longitude:13.337681
Views        : 315
Comments     : 0


+---------+
|  TITLE  |
+---------+
tired polar bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"tired polar bear" "Berlin Zoo" october Oktober 2007 Herbst autumn 