+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : zanthrax-dot-nl
Photo URL    : https://www.flickr.com/photos/zanthraxnl/13012911093/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 8 12:34:49 GMT+0100 2014
Upload Date  : Sat Mar 8 17:46:09 GMT+0100 2014
Views        : 215
Comments     : 0


+---------+
|  TITLE  |
+---------+
polar bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Diergaarde Blijdorp" Blijdorp Rotterdam Zoo Dierentuin 