+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Adrian R. Tan
Photo URL    : https://www.flickr.com/photos/redefinition_au/13002881373/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 3 15:23:49 GMT+0100 2014
Upload Date  : Sat Mar 8 05:43:22 GMT+0100 2014
Views        : 278
Comments     : 0


+---------+
|  TITLE  |
+---------+
000012


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
D90 photography "Gold Coast" Queensland Australia animals wildlife "Polar Bear" 