+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Christopher.Michel
Photo URL    : https://www.flickr.com/photos/cmichel67/19701158560/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 30 19:31:15 GMT+0200 2015
Upload Date  : Tue Jul 21 17:36:04 GMT+0200 2015
Geotag Info  : Latitude:85.106852, Longitude:61.772738
Views        : 609
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar Bears


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Christopher Michel" polarbears 50letpebody nuclearicebreaker "north pole" quark "polar bear" 