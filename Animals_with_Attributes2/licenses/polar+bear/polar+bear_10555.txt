+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sneakerdog
Photo URL    : https://www.flickr.com/photos/sneakerdog/2665898714/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 12 23:52:28 GMT+0200 2008
Upload Date  : Mon Jul 14 00:55:15 GMT+0200 2008
Views        : 948
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Baltimore "Maryland Zoo" "Druid Hill Park" "polar bear" "Sigma 70-300mm f/4-5.6" Maryland "Maryland, USA" "Mark Peters" 