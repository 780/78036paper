+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : JasonParis
Photo URL    : https://www.flickr.com/photos/jasonparis/5531359260/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 9 15:49:56 GMT+0100 2011
Upload Date  : Wed Mar 16 05:36:34 GMT+0100 2011
Geotag Info  : Latitude:43.822727, Longitude:-79.182774
Views        : 1,443
Comments     : 0


+---------+
|  TITLE  |
+---------+
Toronto Zoo (Scarborough - Rouge)


+---------------+
|  DESCRIPTION  |
+---------------+
@ Polar Bear display.


+--------+
|  TAGS  |
+--------+
Toronto Zoo "Toronto Zoo" Animals Zoological Scarborough Rouge "Rouge Valley" "Polar Bear" "Polar Bears" Arctic 