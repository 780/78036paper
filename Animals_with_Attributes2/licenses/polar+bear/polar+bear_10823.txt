+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : colink.
Photo URL    : https://www.flickr.com/photos/colink/4153838295/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Nov 23 15:06:47 GMT+0100 2009
Upload Date  : Thu Dec 3 03:09:23 GMT+0100 2009
Views        : 887
Comments     : 0


+---------+
|  TITLE  |
+---------+
Whazzat?


+---------------+
|  DESCRIPTION  |
+---------------+
Polar bear standing


+--------+
|  TAGS  |
+--------+
"Toronto Zoo" "polar bear" 