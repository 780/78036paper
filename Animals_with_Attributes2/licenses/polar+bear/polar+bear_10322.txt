+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : FrankieFiggs
Photo URL    : https://www.flickr.com/photos/secretagentpope/10030348703/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 26 14:37:03 GMT+0200 2013
Upload Date  : Tue Oct 1 04:00:58 GMT+0200 2013
Views        : 88
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_4055.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"San Diego Zoo and Safari Park" 