+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael Seeley
Photo URL    : https://www.flickr.com/photos/mseeley1/8201046690/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 5 13:04:58 GMT+0200 2012
Upload Date  : Mon Nov 19 20:52:56 GMT+0100 2012
Views        : 1,640
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar Bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
bears polar polarbear polarbears Michael Seeley "Michael Seeley" "Mike Seeley" Mike 