+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Infomastern
Photo URL    : https://www.flickr.com/photos/infomastern/12748279204/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Feb 5 11:22:45 GMT+0100 2014
Upload Date  : Mon Feb 24 16:04:50 GMT+0100 2014
Views        : 8,578
Comments     : 32


+---------+
|  TITLE  |
+---------+
Taking a swim


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Bear Copenhagen "Copenhagen Zoo" Danmark Denmark Köpenhamn "Köpenhamns Zoo" Køpenhavn "Køpenhavn Zoo" Zoo björn isbjörn "polar bear" 