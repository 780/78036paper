+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ber'Zophus
Photo URL    : https://www.flickr.com/photos/10574543@N08/957639909/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 17 15:55:16 GMT+0200 2007
Upload Date  : Tue Jul 31 06:02:02 GMT+0200 2007
Views        : 3,784
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sleeping Polar Bear


+---------------+
|  DESCRIPTION  |
+---------------+
A sleeping polar bear. Taken at the Toronto Zoo with a Canon EOS 350D &amp; 70-300mm f4-5.6 IS USM lens.


+--------+
|  TAGS  |
+--------+
"Polar Bear" Bear "Ursus Maritimus" "Toronto Zoo" Animals Zoo 