+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gripso_banana_prune
Photo URL    : https://www.flickr.com/photos/antonystanley/1185203007/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 19 11:30:33 GMT+0200 2007
Upload Date  : Mon Aug 20 22:11:24 GMT+0200 2007
Views        : 2,475
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar Bear Washing #2


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Edinburgh zoo" 2007 Edinburgh "polar bear" washing 