+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : asokolik
Photo URL    : https://www.flickr.com/photos/asokolik/7330541986/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jun 2 15:49:28 GMT+0200 2012
Upload Date  : Sun Jun 3 23:37:33 GMT+0200 2012
Geotag Info  : Latitude:42.938218, Longitude:-78.852374
Views        : 44
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_1414


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)