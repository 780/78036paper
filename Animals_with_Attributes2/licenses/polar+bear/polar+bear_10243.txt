+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jörg Weingrill
Photo URL    : https://www.flickr.com/photos/joerg73/10692827294/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 31 15:46:36 GMT+0100 2013
Upload Date  : Tue Nov 5 18:07:29 GMT+0100 2013
Views        : 187
Comments     : 0


+---------+
|  TITLE  |
+---------+
Eisbär


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)