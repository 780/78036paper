+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ST33VO
Photo URL    : https://www.flickr.com/photos/st33vo/6864120451/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jun 15 12:20:47 GMT+0200 2010
Upload Date  : Sun Feb 12 19:26:55 GMT+0100 2012
Geotag Info  : Latitude:52.508045, Longitude:13.338142
Views        : 3,272
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar Bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
polar bear "creative commons" 