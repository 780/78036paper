+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Dan Ruscoe
Photo URL    : https://www.flickr.com/photos/druscoe/513524374/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 11 18:52:16 GMT+0100 2007
Upload Date  : Fri May 25 17:44:48 GMT+0200 2007
Views        : 238
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar Bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)