+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tim Sträter
Photo URL    : https://www.flickr.com/photos/stuutje/7914649356/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 2 14:32:32 GMT+0200 2012
Upload Date  : Sun Sep 2 19:17:53 GMT+0200 2012
Geotag Info  : Latitude:51.927337, Longitude:4.441051
Views        : 1,723
Comments     : 0


+---------+
|  TITLE  |
+---------+
IJsbeer


+---------------+
|  DESCRIPTION  |
+---------------+
Deze foto is gemaakt in Diergaarde Blijdorp.

This picture was taken at Rotterdam Zoo.


+--------+
|  TAGS  |
+--------+
"Diergaarde Blijdorp" "Rotterdam Zoo" Dierentuin Zoo Rotterdam IJsbeer Poolbeer "Polar bear" "Ursus maritimus" 