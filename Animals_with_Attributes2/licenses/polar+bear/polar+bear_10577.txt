+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : marfis75
Photo URL    : https://www.flickr.com/photos/marfis75/3976886081/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Sep 24 17:28:29 GMT+0200 2009
Upload Date  : Sat Oct 3 19:12:06 GMT+0200 2009
Views        : 8,939
Comments     : 5


+---------+
|  TITLE  |
+---------+
polar bear


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
white ice icebear bear polar polarbear eisbär bär pelz schlafen marfis75 zoo zootier animal wilhelma wilhelmastuttgart stuttgart zoo-stuttgart zootiere canon tier tiere herbst 2009 0909 092009 "marfis75 on flickr" cc #CC-BY-SA 