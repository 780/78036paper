+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nakashi
Photo URL    : https://www.flickr.com/photos/nakashi/13643364333/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 30 12:04:02 GMT+0200 2014
Upload Date  : Sat Apr 5 15:37:42 GMT+0200 2014
Views        : 307
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar Bear


+---------------+
|  DESCRIPTION  |
+---------------+
at Oga Aquarium


+--------+
|  TAGS  |
+--------+
GAO oga ogaaquarium aquarium akita japan 