+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : www.traveljunction.com
Photo URL    : https://www.flickr.com/photos/128012202@N05/15960828674/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 30 09:27:35 GMT+0100 2014
Upload Date  : Thu Feb 19 20:37:03 GMT+0100 2015
Views        : 784
Comments     : 0


+---------+
|  TITLE  |
+---------+
Polar Bear in the water at Schönbrunn Zoo in Vienna, Austria.


+---------------+
|  DESCRIPTION  |
+---------------+
Follow us on Facebook &amp; Twitter to keep up to date with our latest images from around the world. Visit our website traveljunction.com/ to find great deals on hotels.
 
**** COPYRIGHT AND CC INFORMATION ****
 
If you like and want to use our photos you're welcome to but we require that you give us the required attribution on each image you use.
 
An example of the required attribution is: Image by <a href="http://www.traveljunction.com" rel="nofollow">www.traveljunction.com</a>


+--------+
|  TAGS  |
+--------+
"Schönbrunn Zoo" "Schönbrunn Tiergarten" "Vienna Zoo" "Polar Bear" "Polar Bear in the water" 