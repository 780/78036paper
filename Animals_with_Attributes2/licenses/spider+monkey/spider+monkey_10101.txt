+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sun dazed
Photo URL    : https://www.flickr.com/photos/sundazed/555338083/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 26 14:00:45 GMT+0200 2007
Upload Date  : Sat Jun 16 07:26:02 GMT+0200 2007
Geotag Info  : Latitude:28.826196, Longitude:-81.316165
Views        : 231
Comments     : 0


+---------+
|  TITLE  |
+---------+
monkeying around


+---------------+
|  DESCRIPTION  |
+---------------+
This black-handed spider monkey came to visit me in front of the viewing area.


+--------+
|  TAGS  |
+--------+
Sanford Florida "Central Florida Zoological Park" zoo animals monkey 