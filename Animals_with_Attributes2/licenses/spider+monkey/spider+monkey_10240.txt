+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : western4uk
Photo URL    : https://www.flickr.com/photos/western4uk/1092699351/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 3 19:20:43 GMT+0200 2007
Upload Date  : Sun Aug 12 16:23:55 GMT+0200 2007
Views        : 203
Comments     : 0


+---------+
|  TITLE  |
+---------+
Spider Monkeys


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Yucatan Peninsular" "Quintana Roo" "Jungle Jim's ATV" Cenote Wildlife Mexico 2007 Holiday Animals "Biker Chic Irony" 