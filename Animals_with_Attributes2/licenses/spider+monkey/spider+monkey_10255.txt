+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kurayba
Photo URL    : https://www.flickr.com/photos/kurt-b/5678107057/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 11 12:23:15 GMT+0200 2011
Upload Date  : Mon May 2 05:18:48 GMT+0200 2011
Geotag Info  : Latitude:20.529827, Longitude:-105.281767
Views        : 360
Comments     : 0


+---------+
|  TITLE  |
+---------+
Spider Monkeys


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
puerto vallarta mismaloya jalisco mexico zoologico de zoo captive pentax k-5 Potos flavus spider monkey monkeys sitting drinking water da 18-250 smcpda18250mmf3563edalif 