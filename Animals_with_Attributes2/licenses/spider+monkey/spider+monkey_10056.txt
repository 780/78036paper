+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Bird Brian
Photo URL    : https://www.flickr.com/photos/birdbrian/4429158300/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 31 20:51:36 GMT+0200 2009
Upload Date  : Sat Mar 13 11:00:57 GMT+0100 2010
Views        : 162
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_8930.NEF


+---------------+
|  DESCRIPTION  |
+---------------+
Spider Monkey


+--------+
|  TAGS  |
+--------+
Tambopata_Research_Centre Peru Amazon_Basin 