+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : fortes
Photo URL    : https://www.flickr.com/photos/fortes/3242764810/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jan 19 11:01:06 GMT+0100 2009
Upload Date  : Sun Feb 1 01:04:00 GMT+0100 2009
Geotag Info  : Latitude:8.556747, Longitude:-83.679428
Views        : 204
Comments     : 0


+---------+
|  TITLE  |
+---------+
Spider Monkey


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
costarica peninsulaosa osapeninsula corcovado parquenacionalcorcovada corcovadonationalpark spidermonkey monkey centralamerica 