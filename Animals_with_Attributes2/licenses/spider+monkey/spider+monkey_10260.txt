+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jordan klein
Photo URL    : https://www.flickr.com/photos/jordanklein/403185222/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 20 12:56:13 GMT+0100 2007
Upload Date  : Mon Feb 26 09:11:16 GMT+0100 2007
Views        : 53
Comments     : 0


+---------+
|  TITLE  |
+---------+
Spider Monkeys!


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Costa Rica" travel 