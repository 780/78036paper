+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neil T
Photo URL    : https://www.flickr.com/photos/neilt/4248764716/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 3 12:42:42 GMT+0100 2010
Upload Date  : Tue Jan 5 19:21:44 GMT+0100 2010
Geotag Info  : Latitude:53.815827, Longitude:-3.010436
Views        : 104
Comments     : 1


+---------+
|  TITLE  |
+---------+
Spider Monkeys


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
blackpoolzoo zoo monkey spidermonkey monkeys 