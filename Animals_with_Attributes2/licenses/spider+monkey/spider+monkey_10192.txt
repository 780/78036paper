+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neil Saunders
Photo URL    : https://www.flickr.com/photos/nsaunders/3940044513/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 20 04:13:40 GMT+0200 2009
Upload Date  : Mon Sep 21 11:48:38 GMT+0200 2009
Geotag Info  : Latitude:-33.843116, Longitude:151.241590
Views        : 494
Comments     : 3


+---------+
|  TITLE  |
+---------+
Spider monkey


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
sydney taronga zoo "spider monkey" "Ateles geoffroyi" 