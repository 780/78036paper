+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cliff1066™
Photo URL    : https://www.flickr.com/photos/nostri-imago/2854964652/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Nov 29 10:16:20 GMT+0100 2007
Upload Date  : Sun Sep 14 03:28:34 GMT+0200 2008
Geotag Info  : Latitude:21.271766, Longitude:-157.820999
Views        : 1,306
Comments     : 0


+---------+
|  TITLE  |
+---------+
Spider Monkey (Ateles geoffroyi)


+---------------+
|  DESCRIPTION  |
+---------------+
The black-handed spider monkey has a light to medium brown body and darker limbs with the hands and feet usually black in color. The tail is sometimes longer than the body. When the animal is on the lookout, it stands or walks on two feet, using the tail to hold on to a support. In the wild, the Spider monkey rarely comes down to the jungle floor. This arboreal monkey has a prehensile tail that is muscular and tactile and is used as an extra hand. Both the underside and tip of the tail are used for climbing and grasping. When swinging by the tail, the hands are free to gather food. Acrobatic and swift, Spider monkeys move through the trees, with one arm stride covering up to 40 feet.


+--------+
|  TAGS  |
+--------+
Mammals Reptiles Honolulu Zoo O'ahu Turtle Alligator "Spider Monkey" Monkey "Galapagos Tortoise" Tortoise 