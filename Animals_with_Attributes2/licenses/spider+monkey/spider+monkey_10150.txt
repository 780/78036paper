+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : donjd2
Photo URL    : https://www.flickr.com/photos/ddebold/6206495235/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 2 11:00:25 GMT+0200 2011
Upload Date  : Mon Oct 3 09:53:26 GMT+0200 2011
Geotag Info  : Latitude:37.326500, Longitude:-121.861834
Views        : 584
Comments     : 2


+---------+
|  TITLE  |
+---------+
Spider Monkey at Happy Hollow


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"happy hollow" "Kelley Park" "squirrel monkey" zoo "San Jose" CA USA 