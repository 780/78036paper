+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kabacchi
Photo URL    : https://www.flickr.com/photos/kabacchi/5106374873/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 23 16:14:22 GMT+0200 2010
Upload Date  : Sat Oct 23 09:14:22 GMT+0200 2010
Views        : 209
Comments     : 0


+---------+
|  TITLE  |
+---------+
Geoffroy's Spider Monkey - 03


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Geoffroy's Spider Monkey" ジェフロイクモザル Monkey 猿 上野動物園 Animal 動物 Mammalia 哺乳類 "~Geoffroys Spider Monkey~" 