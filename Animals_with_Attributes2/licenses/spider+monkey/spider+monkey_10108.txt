+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : HBarrison
Photo URL    : https://www.flickr.com/photos/hbarrison/21840220889/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 2 06:12:00 GMT+0200 2015
Upload Date  : Thu Oct 8 01:16:04 GMT+0200 2015
Geotag Info  : Latitude:-4.483059, Longitude:-73.523117
Views        : 126
Comments     : 0


+---------+
|  TITLE  |
+---------+
Monos_2015 08 02_1617


+---------------+
|  DESCRIPTION  |
+---------------+
An early morning skiff expedition to Monkey Island sanctuary  /  Black-headed spider monkey (Ateles fusciceps)


+--------+
|  TAGS  |
+--------+
Amazon Peru HBarrison "Harvey Barrison" "Black-headed spider monkey" "Taxonomy:binomial=Ateles fusciceps" 