+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : HBarrison
Photo URL    : https://www.flickr.com/photos/hbarrison/22000918366/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 2 06:14:46 GMT+0200 2015
Upload Date  : Thu Oct 8 01:16:07 GMT+0200 2015
Geotag Info  : Latitude:-4.483059, Longitude:-73.523117
Views        : 282
Comments     : 0


+---------+
|  TITLE  |
+---------+
Monos_2015 08 02_1623


+---------------+
|  DESCRIPTION  |
+---------------+
An early morning skiff expedition to Monkey Island sanctuary  /  Venezuelan red howler (Alouatta seniculus) with Black-headed Spider Monkey (Ateles fusciceps)


+--------+
|  TAGS  |
+--------+
Amazon Peru HBarrison "Harvey Barrison" "Venezuelan red howler" "Taxonomy:binomial=Alouatta seniculus" "Black-headed Spider Monkey" "Taxonomy:binomial=Ateles fusciceps" 