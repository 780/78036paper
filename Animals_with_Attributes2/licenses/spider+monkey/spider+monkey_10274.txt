+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : e³°°°
Photo URL    : https://www.flickr.com/photos/e3000/4885467301/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 6 15:41:24 GMT+0200 2010
Upload Date  : Thu Aug 12 20:38:33 GMT+0200 2010
Views        : 5,679
Comments     : 13


+---------+
|  TITLE  |
+---------+
the old man and the tree / Colombian Black Spider Monkey


+---------------+
|  DESCRIPTION  |
+---------------+
spectacular tree climber from Colombia or Panama, looking sad but I assume he 's happy


+--------+
|  TAGS  |
+--------+
"Ateles fusciceps robustus" "Colombian Black Spider Monkey" monkey ape Colombia Panama "Schwarzer Klammeraffe" Colombiaanse slingeraap natuur nature naturepix "taxonomy:trinomial=Ateles fusciceps robustus" "Colombiaanse slingeraap" Apenheul Apeldoorn 