+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : PeterQQ2009
Photo URL    : https://www.flickr.com/photos/peterschoen/5375816308/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Dec 17 12:47:34 GMT+0100 2010
Upload Date  : Fri Jan 21 17:45:49 GMT+0100 2011
Geotag Info  : Latitude:-19.874319, Longitude:-41.722179
Views        : 2,752
Comments     : 5


+---------+
|  TITLE  |
+---------+
Northern Muriqui (Brachyteles hypoxanthus) with young


+---------------+
|  DESCRIPTION  |
+---------------+
The Northern Muriqui is a muriqui (woolly spider monkey) species endemic to Brazil. It is found in the Atlantic Forest region of the Brazilian states of Rio de Janeiro, Espírito Santo, Minas Gerais and Bahia. Muriquis are the largest species of monkeys in the Americas. The Northern Muriqui is listed as ‘CRITICALLY ENDANGERED’ on the IUCN Red List of Threatened Species. Only about 500 individuals remain in the wild, and one third of those occur in a single forest, the RPPN (Private Reserve of Natural Patrimony) Feliciano Abdala, near Caratinga in the state of Minas Gerais.

O muriqui-do-norte (Brachyteles hipoxanthus), o maior mamífero endémico ao 
Brasil e uma espécie muito carismática, encontra-se na lista dos prirnatas mais ameaçados do planeta. O muriqui-do-norte tornou-se uma espécie-bandeira de grande importância para o Brasil. Somente cerca de 500 indivíduos sobrevivem na natureza e um terço dessa população existe em um único local: as matas da RPPN Feliciano Abdala, sede da Estação Biológica de Caratinga (EBC), no estado de Minas Gerais.

RPPN Feliciano Miguel Abdala, Caratinga, Minas Gerais, Brazil.


+--------+
|  TAGS  |
+--------+
Mammals brazil monkeys animals 