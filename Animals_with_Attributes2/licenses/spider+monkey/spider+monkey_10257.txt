+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mikes.space
Photo URL    : https://www.flickr.com/photos/mikesspace/9411416331/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Mar 17 13:17:07 GMT+0100 2007
Upload Date  : Thu Aug 1 07:08:46 GMT+0200 2013
Views        : 203
Comments     : 0


+---------+
|  TITLE  |
+---------+
Spider monkeys


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wildlife monkeys zoos 