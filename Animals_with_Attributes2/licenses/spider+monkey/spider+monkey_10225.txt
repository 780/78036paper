+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ed Clayton
Photo URL    : https://www.flickr.com/photos/edclayton/13611415935/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Mar 18 15:37:29 GMT+0100 2014
Upload Date  : Fri Apr 4 00:28:16 GMT+0200 2014
Geotag Info  : Latitude:20.492116, Longitude:-87.734209
Views        : 48
Comments     : 0


+---------+
|  TITLE  |
+---------+
Spider Monkey


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)