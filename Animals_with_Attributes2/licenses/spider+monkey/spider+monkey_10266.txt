+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : chasingwildlife
Photo URL    : https://www.flickr.com/photos/chasingwildlife/23211736125/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 19 16:07:48 GMT+0100 2005
Upload Date  : Sun Nov 22 16:44:43 GMT+0100 2015
Views        : 159
Comments     : 0


+---------+
|  TITLE  |
+---------+
Spider_Monkeys_Santa_Rosa,_Bolivia_(8_of_8).jpg


+---------------+
|  DESCRIPTION  |
+---------------+
Olympus digital camera


+--------+
|  TAGS  |
+--------+
Amazon Bolivia Monkeys Olympus Rosa Santa Wildlife jungle monkey riverside squirrel 