+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 07, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Matt Brittaine
Photo URL    : https://www.flickr.com/photos/mattbrittaine/8751041172/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 17 11:48:54 GMT+0200 2013
Upload Date  : Sat May 18 17:41:52 GMT+0200 2013
Views        : 75
Comments     : 0


+---------+
|  TITLE  |
+---------+
London Zoo 17


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"London Zoo" Zoo ZSL Spider monkey 