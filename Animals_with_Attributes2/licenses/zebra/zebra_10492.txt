+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gvgoebel
Photo URL    : https://www.flickr.com/photos/37467370@N08/7611632228/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 20 14:11:44 GMT+0200 2012
Upload Date  : Fri Jul 20 23:13:37 GMT+0200 2012
Views        : 163
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ybzeb_2b


+---------------+
|  DESCRIPTION  |
+---------------+
zebra colt, Idaho Falls Zoo, Idaho / 2006


+--------+
|  TAGS  |
+--------+
animals mammals zebra colt 