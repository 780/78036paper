+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : elPadawan
Photo URL    : https://www.flickr.com/photos/elpadawan/22573338039/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 7 17:02:37 GMT+0200 2008
Upload Date  : Thu Nov 12 13:26:59 GMT+0100 2015
Geotag Info  : Latitude:43.340136, Longitude:-80.181293
Views        : 217
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebras


+---------------+
|  DESCRIPTION  |
+---------------+
Zebras, African Lion Safari, Hamilton, ON, CA


+--------+
|  TAGS  |
+--------+
"African Lion Safari" Ontario Canada "0907 - African Lion Safari" 2008 Zebra mammals nature 