+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Michael Fraley
Photo URL    : https://www.flickr.com/photos/mrfraley/8546525616/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Mar 10 14:21:20 GMT+0100 2013
Upload Date  : Sun Mar 10 22:21:20 GMT+0100 2013
Views        : 180
Comments     : 0


+---------+
|  TITLE  |
+---------+
San Francisco Zoo 146


+---------------+
|  DESCRIPTION  |
+---------------+
Minolta X-700
Fuji Superia 200
Miida 300mm/f4.5
01/26/2013


+--------+
|  TAGS  |
+--------+
zebra 