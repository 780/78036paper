+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : christoph.ch
Photo URL    : https://www.flickr.com/photos/christoph_ch/26034824890/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 6 09:50:22 GMT+0200 2016
Upload Date  : Fri Apr 8 15:47:15 GMT+0200 2016
Views        : 28
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_7005


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Lake Manyara National Park" zebra 