+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : g-l-o-w
Photo URL    : https://www.flickr.com/photos/43595579@N06/5023230236/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 25 07:44:55 GMT+0200 2010
Upload Date  : Sat Sep 25 16:44:55 GMT+0200 2010
Views        : 112
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_5433


+---------------+
|  DESCRIPTION  |
+---------------+
Zebra at the Indianapolis Zoo.


+--------+
|  TAGS  |
+--------+
Zebra Animals Zoo 