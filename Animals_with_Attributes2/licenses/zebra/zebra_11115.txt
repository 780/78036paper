+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Mike McHolm
Photo URL    : https://www.flickr.com/photos/mikemcholm/6866408641/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 12 02:28:11 GMT+0100 2012
Upload Date  : Mon Feb 13 01:58:38 GMT+0100 2012
Geotag Info  : Latitude:-3.234612, Longitude:35.515022
Views        : 178
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebras


+---------------+
|  DESCRIPTION  |
+---------------+
Ngorongoro Crater Safari
Tanzania, Africa


+--------+
|  TAGS  |
+--------+
Africa Animals "Big Five" McHolm "Mike McHolm" "Ngorongoro Crater" Safari Tanzania Vancouver Ngorongoro 