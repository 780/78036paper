+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/22045226303/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 19 10:54:35 GMT+0200 2015
Upload Date  : Sun Nov 1 09:54:35 GMT+0100 2015
Views        : 100
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Zebra


+--------+
|  TAGS  |
+--------+
"Chester Zoo" Zebra 