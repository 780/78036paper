+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : donnierayjones
Photo URL    : https://www.flickr.com/photos/donnieray/7528520274/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 7 11:24:46 GMT+0200 2012
Upload Date  : Sun Jul 8 19:00:30 GMT+0200 2012
Views        : 467
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zoo fort-worth animal 