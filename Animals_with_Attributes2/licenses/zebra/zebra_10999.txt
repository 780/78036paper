+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : South African Tourism
Photo URL    : https://www.flickr.com/photos/south-african-tourism/6253262144/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 24 11:20:06 GMT+0200 2009
Upload Date  : Mon Oct 17 09:41:17 GMT+0200 2011
Views        : 1,260
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra's, Limpopo, South Africa


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"South Africa" "South African Tourism" MeetSouthAfrica Limpopo "Limpopo Province" 