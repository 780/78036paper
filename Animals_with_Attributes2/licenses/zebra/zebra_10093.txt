+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Bird Brian
Photo URL    : https://www.flickr.com/photos/birdbrian/5330943139/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Oct 4 07:41:59 GMT+0200 2010
Upload Date  : Thu Jan 6 23:35:49 GMT+0100 2011
Views        : 50
Comments     : 0


+---------+
|  TITLE  |
+---------+
Burchell's Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
Burchell's Zebra


+--------+
|  TAGS  |
+--------+
Addo National Park 