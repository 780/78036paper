+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : paulshaffner
Photo URL    : https://www.flickr.com/photos/paulshaffner/282419123/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 29 09:10:23 GMT+0100 2006
Upload Date  : Sun Oct 29 18:10:23 GMT+0100 2006
Views        : 520
Comments     : 1


+---------+
|  TITLE  |
+---------+
Twin Zebras


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Ruaha Tanzania 