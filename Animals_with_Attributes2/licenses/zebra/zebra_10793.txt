+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Clara S.
Photo URL    : https://www.flickr.com/photos/pictureclara/4105040966/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 4 10:01:09 GMT+0200 2009
Upload Date  : Sun Nov 15 05:42:18 GMT+0100 2009
Views        : 286
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"DC Zoo Summer 2009" black white zoo animal zebra 