+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jemartin03
Photo URL    : https://www.flickr.com/photos/jemartin03/412615459/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Feb 28 05:54:13 GMT+0100 2006
Upload Date  : Tue Mar 6 15:53:30 GMT+0100 2007
Views        : 186
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
safari zebra "lake nakuru" kenya 