+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/3578468294/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun May 10 12:39:29 GMT+0200 2009
Upload Date  : Sat May 30 13:22:33 GMT+0200 2009
Geotag Info  : Latitude:47.420073, Longitude:9.285378
Views        : 16,314
Comments     : 36


+---------+
|  TITLE  |
+---------+
Very young zebra


+---------------+
|  DESCRIPTION  |
+---------------+
This adorable zebra foal was lounging on the ground of his enclosure of the Walter zoo, and was really looking cute! :)

What is interesting is that some of his stripes are dark brown and not black, and I don't think it's an effect of the light...


+--------+
|  TAGS  |
+--------+
small zebra foal baby lying portrait cute stripes equine walter zoo gossau switzerland nikon d300 mammal equid horse SpecAnimal 