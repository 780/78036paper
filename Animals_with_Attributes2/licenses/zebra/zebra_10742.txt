+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ramanathan.Kathiresan
Photo URL    : https://www.flickr.com/photos/rampix/2883152395/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 4 12:42:27 GMT+0200 2008
Upload Date  : Wed Sep 24 03:24:23 GMT+0200 2008
Views        : 464
Comments     : 1


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
wildlife beauty beautiful inspiring tiger zebra birds cow buffalo 