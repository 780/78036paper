+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flowcomm
Photo URL    : https://www.flickr.com/photos/flowcomm/14965195236/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 16 08:04:00 GMT+0200 2014
Upload Date  : Thu Aug 21 11:47:34 GMT+0200 2014
Views        : 138
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Kruger National Park" "South Africa" 