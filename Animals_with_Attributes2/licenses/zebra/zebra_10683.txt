+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Vaiz Ha
Photo URL    : https://www.flickr.com/photos/vaizha/8524252550/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 16 16:07:24 GMT+0100 2013
Upload Date  : Sun Mar 3 11:28:01 GMT+0100 2013
Views        : 301
Comments     : 6


+---------+
|  TITLE  |
+---------+
zuidafrika-152


+---------------+
|  DESCRIPTION  |
+---------------+
Addo Elephant Park


+--------+
|  TAGS  |
+--------+
"South Africa" "Addo Elephant Park" zebra 