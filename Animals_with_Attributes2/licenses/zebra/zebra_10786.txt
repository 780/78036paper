+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ellesmere FNC
Photo URL    : https://www.flickr.com/photos/ellesmerefnc/3934861512/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 19 13:53:39 GMT+0200 2009
Upload Date  : Sat Sep 19 19:44:56 GMT+0200 2009
Geotag Info  : Latitude:53.225151, Longitude:-2.886292
Views        : 68
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"chester zoo" Zoo SX10is Canon "canon SX10is" "natural history" Nature FNC "Ellesmere FNC" 