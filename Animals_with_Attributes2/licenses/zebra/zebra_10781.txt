+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rhoftonphoto
Photo URL    : https://www.flickr.com/photos/rachels_photo_world/3877924992/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Sep 1 11:20:28 GMT+0200 2009
Upload Date  : Tue Sep 1 13:17:10 GMT+0200 2009
Views        : 705
Comments     : 0


+---------+
|  TITLE  |
+---------+
zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
face zebra stripes melbourne zoo animals spring sun outdoors nature "melbourne zoo" creatures outting 