+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : spalti
Photo URL    : https://www.flickr.com/photos/spalti/3959895208/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 26 13:49:11 GMT+0200 2009
Upload Date  : Sun Sep 27 21:00:47 GMT+0200 2009
Views        : 97
Comments     : 1


+---------+
|  TITLE  |
+---------+
IMG_7998


+---------------+
|  DESCRIPTION  |
+---------------+
Tierpark Hellabrunn


+--------+
|  TAGS  |
+--------+
"Tierpark Hellabrunn" Zebra 