+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : anthonydilaura
Photo URL    : https://www.flickr.com/photos/anthonydilaura/16478432294/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 7 14:17:08 GMT+0200 2015
Upload Date  : Fri Apr 10 21:12:57 GMT+0200 2015
Views        : 58
Comments     : 0


+---------+
|  TITLE  |
+---------+
zebra


+---------------+
|  DESCRIPTION  |
+---------------+
miami zoo &amp; everglades


+--------+
|  TAGS  |
+--------+
everglades wildlife miami zoo 