+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/12210809654/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 13 12:52:52 GMT+0200 2013
Upload Date  : Wed Jan 29 21:22:37 GMT+0100 2014
Views        : 95
Comments     : 0


+---------+
|  TITLE  |
+---------+
Antwerp Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Zebra


+--------+
|  TAGS  |
+--------+
"Antwerp Zoo" 