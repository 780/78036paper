+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jayt74
Photo URL    : https://www.flickr.com/photos/jayt74/5033657777/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Sep 27 12:39:13 GMT+0200 2010
Upload Date  : Tue Sep 28 21:51:00 GMT+0200 2010
Views        : 267
Comments     : 21


+---------+
|  TITLE  |
+---------+
Scratching


+---------------+
|  DESCRIPTION  |
+---------------+
Zebra at Longleat


+--------+
|  TAGS  |
+--------+
Zebra wildlife NikonD5000 Longleat 