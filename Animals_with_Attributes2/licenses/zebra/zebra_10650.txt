+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hicharice
Photo URL    : https://www.flickr.com/photos/25397558@N07/4889966455/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 14 06:17:08 GMT+0200 2010
Upload Date  : Sat Aug 14 12:17:08 GMT+0200 2010
Views        : 157
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra shot


+---------------+
|  DESCRIPTION  |
+---------------+
Roger Williams Park Zoo in Providence, RI.


+--------+
|  TAGS  |
+--------+
zebra zoo "roger williams park zoo" providence "rhode island" 