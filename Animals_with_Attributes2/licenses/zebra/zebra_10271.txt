+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ravend
Photo URL    : https://www.flickr.com/photos/ravendr78/6206232048/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 2 13:33:36 GMT+0200 2011
Upload Date  : Mon Oct 3 04:28:06 GMT+0200 2011
Views        : 129
Comments     : 0


+---------+
|  TITLE  |
+---------+
IMG_2387


+---------------+
|  DESCRIPTION  |
+---------------+
The Zebra, hiding partially behind the rock.


+--------+
|  TAGS  |
+--------+
"Knoxville Zoo" zebra 