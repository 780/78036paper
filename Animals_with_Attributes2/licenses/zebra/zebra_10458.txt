+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : leander.canaris
Photo URL    : https://www.flickr.com/photos/28299495@N04/3884363321/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 22 21:38:42 GMT+0200 2009
Upload Date  : Thu Sep 3 20:42:45 GMT+0200 2009
Views        : 376
Comments     : 0


+---------+
|  TITLE  |
+---------+
Toronto Zoo- Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
toronto zoo zebra 