+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : stephenyeargin
Photo URL    : https://www.flickr.com/photos/stephenyeargin/2850718961/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 12 10:42:46 GMT+0200 2008
Upload Date  : Fri Sep 12 20:30:09 GMT+0200 2008
Geotag Info  : Latitude:36.088991, Longitude:-86.741244
Views        : 53
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
Quite a few of them out there.


+--------+
|  TAGS  |
+--------+
"nashville zoo" grassmere 