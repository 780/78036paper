+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Marcello Testi
Photo URL    : https://www.flickr.com/photos/i-testi/3759258285/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 26 14:27:29 GMT+0200 2009
Upload Date  : Mon Jul 27 01:32:06 GMT+0200 2009
Geotag Info  : Latitude:44.472684, Longitude:8.022079
Views        : 90
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra (so close)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animali 