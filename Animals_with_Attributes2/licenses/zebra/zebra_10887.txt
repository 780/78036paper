+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : hobgadlng
Photo URL    : https://www.flickr.com/photos/hobgadlng/7671071932/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 1 16:59:53 GMT+0200 2012
Upload Date  : Sun Jul 29 22:46:23 GMT+0200 2012
Geotag Info  : Latitude:-18.930807, Longitude:14.791889
Views        : 894
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Namibia Etosha animal zebra 