+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Rob Enslin
Photo URL    : https://www.flickr.com/photos/doos/3808105672/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Aug 5 07:44:21 GMT+0200 2009
Upload Date  : Mon Aug 10 16:05:16 GMT+0200 2009
Views        : 932
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra at Amber Valley (Howick, SA)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zebra howick "amber valley" "nature reserve" animal 