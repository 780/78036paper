+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : JP0202
Photo URL    : https://www.flickr.com/photos/jp0202/2107868915/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 14 14:20:08 GMT+0200 2006
Upload Date  : Thu Dec 13 12:22:07 GMT+0100 2007
Views        : 39
Comments     : 0


+---------+
|  TITLE  |
+---------+
zebras in the Serengeti


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
East Africa Trip 