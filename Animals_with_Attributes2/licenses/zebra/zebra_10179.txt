+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Neil Tackaberry
Photo URL    : https://www.flickr.com/photos/23629083@N03/6960264023/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 1 00:00:00 GMT+0200 2009
Upload Date  : Wed Mar 7 00:43:27 GMT+0100 2012
Views        : 269
Comments     : 0


+---------+
|  TITLE  |
+---------+
Fota Island, Cork


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
zebra animal fota island "fota island" cork ireland irish neil_t 