+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jdeeringdavis
Photo URL    : https://www.flickr.com/photos/hayesandjenn/3893416546/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Sep 5 15:33:22 GMT+0200 2008
Upload Date  : Sun Sep 6 18:10:50 GMT+0200 2009
Views        : 424
Comments     : 0


+---------+
|  TITLE  |
+---------+
zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
texas wildlife "natural bridge wildlife ranch" zebra animals "hill country" 