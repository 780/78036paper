+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jsogo
Photo URL    : https://www.flickr.com/photos/jsogo/3910998586/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 21 08:38:34 GMT+0200 2009
Upload Date  : Sat Sep 12 00:47:19 GMT+0200 2009
Views        : 461
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"South Africa" kruger safari zebra "Lion Sands" nikon d5000 