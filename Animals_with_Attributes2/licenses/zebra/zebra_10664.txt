+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : icelight
Photo URL    : https://www.flickr.com/photos/icelight/48093990/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 17 19:44:44 GMT+0200 2003
Upload Date  : Fri Sep 30 23:40:36 GMT+0200 2005
Views        : 1,629
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
A nice little shot of a lone zebra (definately not the norm) staring at us from over his shoulder.


+--------+
|  TAGS  |
+--------+
namibia etosha zebra 