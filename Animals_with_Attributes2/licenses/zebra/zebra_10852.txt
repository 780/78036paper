+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andesine
Photo URL    : https://www.flickr.com/photos/andesine/6070076855/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 31 14:02:39 GMT+0200 2011
Upload Date  : Mon Aug 22 21:04:29 GMT+0200 2011
Views        : 3,541
Comments     : 14


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Zebra "African equids" "black and white stripes" "zebra crossing" RSA "Rietvleidam Nature Reserve" Flickr Andesine "fingerprint colours" stripes animals wildlife environment nature Pretoria Gauteng 