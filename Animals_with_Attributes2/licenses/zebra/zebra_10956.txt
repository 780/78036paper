+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : prelude2000
Photo URL    : https://www.flickr.com/photos/prelude-2000/18580179234/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 30 10:51:39 GMT+0100 2014
Upload Date  : Sat Jun 27 14:45:01 GMT+0200 2015
Views        : 135
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
OLYMPUS DIGITAL CAMERA


+--------+
|  TAGS  |
+--------+
Sydney M.ZUIKO DIGITAL ED 40-150mm F2.8 PRO Zebra Taronga Zoo 