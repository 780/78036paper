+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nagarjun
Photo URL    : https://www.flickr.com/photos/nagarjun/18623570279/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 12 15:39:13 GMT+0200 2015
Upload Date  : Sun Jun 14 21:10:09 GMT+0200 2015
Views        : 206
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
Lion Park, Johannesburg. Gauteng, South Africa. June 2015.


+--------+
|  TAGS  |
+--------+
"Lion Park" Johannesburg Gauteng "South Africa" animals 