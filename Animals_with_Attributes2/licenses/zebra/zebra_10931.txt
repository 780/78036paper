+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jpkeith77
Photo URL    : https://www.flickr.com/photos/jpkeith77/13296525024/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 14 16:59:43 GMT+0100 2014
Upload Date  : Thu Mar 20 23:06:04 GMT+0100 2014
Views        : 75
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Ft. Worth Zoo 2014 