+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jcarbaugh
Photo URL    : https://www.flickr.com/photos/jcarbaugh/37905242/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 21 17:48:02 GMT+0200 2005
Upload Date  : Sun Aug 28 17:11:57 GMT+0200 2005
Geotag Info  : Latitude:33.099813, Longitude:-117.001272
Views        : 366
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebras at San Diego Zoo Wild Animal Park


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
california "san diego" zoo zebra 