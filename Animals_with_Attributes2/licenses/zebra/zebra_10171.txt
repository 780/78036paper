+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Namibnat
Photo URL    : https://www.flickr.com/photos/namibnat/5007477431/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 8 10:48:14 GMT+0200 2010
Upload Date  : Mon Sep 20 13:10:46 GMT+0200 2010
Geotag Info  : Latitude:-18.858777, Longitude:16.925554
Views        : 3,095
Comments     : 1


+---------+
|  TITLE  |
+---------+
Etosha Waterhole Zebra Drinking


+---------------+
|  DESCRIPTION  |
+---------------+
A Burchell's or Plains Zebra <i>Equus quagga burchellii</i> drinking water at Chudop (meaning &quot;black mud&quot;) waterhole.  Like several other natural waterholes in the park, Chudop has a bunch of reeds in the middle of the waterhole, with open dusty banks free of any vegetation - so the waterholes would almost resemble a donut if you looked from above.

Chudop is one of the best waterholes in the park to spend time at - there are often 5 or six species of large mammals there at the same time, and it is frequented by giraffe.  It is also one of the few waterholes where Eland are often seen.


+--------+
|  TAGS  |
+--------+
Zebra Etosha Chudop "Burchell's Zebra" "Equus quagga burchellii" 