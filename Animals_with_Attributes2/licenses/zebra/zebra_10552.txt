+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : yuxi3200
Photo URL    : https://www.flickr.com/photos/yuxi3200/14368591443/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed May 28 14:13:04 GMT+0200 2014
Upload Date  : Thu Jun 5 01:54:01 GMT+0200 2014
Views        : 83
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra at Houston Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)