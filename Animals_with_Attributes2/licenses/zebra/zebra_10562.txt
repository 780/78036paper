+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DavidDennisPhotos.com
Photo URL    : https://www.flickr.com/photos/davidden/376775755/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Feb 1 22:32:49 GMT+0100 2007
Upload Date  : Thu Feb 1 21:32:49 GMT+0100 2007
Geotag Info  : Latitude:-3.666927, Longitude:34.892578
Views        : 3,201
Comments     : 1


+---------+
|  TITLE  |
+---------+
Zebra Close-up in Ngorongoro Crater


+---------------+
|  DESCRIPTION  |
+---------------+
I highly recommend the tour company we used on this trip: <a href="http://www.thomsonsafaris.com" rel="nofollow">www.thomsonsafaris.com</a> and the organization who planned it for us: <a href="http://www.familyadventures.com" rel="nofollow">www.familyadventures.com</a>.


+--------+
|  TAGS  |
+--------+
Tanzania Africa "East Africa" Thomson Ngorongoro zebra 