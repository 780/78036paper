+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : biggertree
Photo URL    : https://www.flickr.com/photos/biggertree/3793004956/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 24 13:25:59 GMT+0200 2009
Upload Date  : Wed Aug 5 20:43:29 GMT+0200 2009
Views        : 910
Comments     : 18


+---------+
|  TITLE  |
+---------+
Z Day at the Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
A study in framing.  Taken at the Memphis Zoo.


+--------+
|  TAGS  |
+--------+
Zebra Zoo "Memphis Zoo" Framing "MBG Shutterbuds" It'sAZooOutThere 