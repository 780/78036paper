+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kezee
Photo URL    : https://www.flickr.com/photos/kezee/5784352628/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 20 09:03:27 GMT+0200 2011
Upload Date  : Wed Jun 1 03:11:16 GMT+0200 2011
Views        : 82
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
serengeti "serengeti national park" tanzania wildlife zebra 