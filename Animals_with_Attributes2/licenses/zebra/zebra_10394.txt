+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Pius Mahimbi
Photo URL    : https://www.flickr.com/photos/pius_mahimbi/5245828025/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Nov 23 06:55:08 GMT+0100 2010
Upload Date  : Thu Dec 9 12:45:19 GMT+0100 2010
Views        : 9,844
Comments     : 56


+---------+
|  TITLE  |
+---------+
Plains Zebra: Equus quagga


+---------------+
|  DESCRIPTION  |
+---------------+
Ruaha National Park, Iringa, Tanzania


+--------+
|  TAGS  |
+--------+
"Plains Zebra" Animalia Chordata Mammalia Perissodactyla Equidae "Equus quagga" Wildlife "Ruaha National Park" Iringa Tanzania Nature Africa MusicToMyEyesLevel1 