+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Joyce Kaes
Photo URL    : https://www.flickr.com/photos/103189636@N05/24579819560/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 8 22:08:13 GMT+0200 2014
Upload Date  : Sun Feb 7 16:53:32 GMT+0100 2016
Views        : 39
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Zebra Funny Pattern Planckendael Zoo 