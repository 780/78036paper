+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Abspires40
Photo URL    : https://www.flickr.com/photos/abster35/13311445164/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Feb 20 10:23:12 GMT+0100 2014
Upload Date  : Fri Mar 21 19:06:19 GMT+0100 2014
Views        : 450
Comments     : 0


+---------+
|  TITLE  |
+---------+
Best of Kruger 2014 Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
Kruger National Park, South Africa


+--------+
|  TAGS  |
+--------+
"Kruger Park" KNP "Kruger National Park" "South Africa" Wildlife Animals Scenery Nature 