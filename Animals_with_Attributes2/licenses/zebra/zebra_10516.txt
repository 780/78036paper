+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Care_SMC
Photo URL    : https://www.flickr.com/photos/75491103@N00/12040153734/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jan 19 10:23:02 GMT+0100 2014
Upload Date  : Mon Jan 20 00:12:01 GMT+0100 2014
Views        : 244
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra (Omniplex in background)


+---------------+
|  DESCRIPTION  |
+---------------+
OKC Zoo


+--------+
|  TAGS  |
+--------+
"OKC Zoo" Zebra Omniplex Animals 