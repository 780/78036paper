+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Roberto Verzo
Photo URL    : https://www.flickr.com/photos/verzo/3166128383/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Dec 30 18:33:45 GMT+0100 2008
Upload Date  : Sun Jan 4 15:46:04 GMT+0100 2009
Views        : 230
Comments     : 0


+---------+
|  TITLE  |
+---------+
Damara-Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
Damara-Zebra (Equus burchellii antiquorum). Das Damarazebra oder Chapmanzebra ist eine Zebraart aus dem südlichen Afrika. Tiergarten Schönbrunn, Dezember 2008.


+--------+
|  TAGS  |
+--------+
zoo "zoologischer Garten" "zoological gardens" Zebra Damarazebra Schönbrunn Schoenbrunn Tiergarten animals garden Wien Vienna Austria emperor 