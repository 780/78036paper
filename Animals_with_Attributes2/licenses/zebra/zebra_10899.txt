+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brian.gratwicke
Photo URL    : https://www.flickr.com/photos/briangratwicke/8325772221/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Dec 27 08:03:10 GMT+0100 2012
Upload Date  : Sun Dec 30 19:07:44 GMT+0100 2012
Geotag Info  : Latitude:-17.837950, Longitude:31.088712
Views        : 3,032
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Mukuvisi Woodlands" Harare Zimbabwe taxonomy:kingdom=Animalia Animalia taxonomy:phylum=Chordata Chordata taxonomy:class=Mammalia Mammalia taxonomy:order=Perissodactyla Perissodactyla taxonomy:family=Equidae Equidae taxonomy:genus=Equus Equus taxonomy:species=quagga "taxonomy:binomial=Equus quagga" "Equus quagga" "Burchell's Zebra" "Plains Zebra" "Common Zebra" "Painted Zebra" "taxonomy:common=Burchell's Zebra" "taxonomy:common=Plains Zebra" "taxonomy:common=Common Zebra" "taxonomy:common=Painted Zebra" 