+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BBM Explorer
Photo URL    : https://www.flickr.com/photos/bbmexplorer/9531308111/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 28 14:42:55 GMT+0200 2013
Upload Date  : Sun Aug 18 00:09:07 GMT+0200 2013
Geotag Info  : Latitude:-24.154428, Longitude:28.196153
Views        : 893
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra (Equus quagga)


+---------------+
|  DESCRIPTION  |
+---------------+
You are free to use this image under a Creative Commons license on condition you include a credit link to <a href="http://www.bbmexplorer.com" rel="nofollow">www.bbmexplorer.com</a>


+--------+
|  TAGS  |
+--------+
"south africa" limpopo "limpopo province" safari waterberg zebra Equus quagga "Equus quagga" 