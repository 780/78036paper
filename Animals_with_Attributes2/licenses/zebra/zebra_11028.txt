+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : a-Reino
Photo URL    : https://www.flickr.com/photos/areinostudios/2714699873/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 28 22:54:05 GMT+0200 2008
Upload Date  : Wed Jul 30 02:02:26 GMT+0200 2008
Views        : 219
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebras Grazing 01


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Zebra Mammals Zoo Horse Stripes Alert feeding grazing 