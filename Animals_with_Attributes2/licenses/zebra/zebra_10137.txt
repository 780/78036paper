+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bobosh_t
Photo URL    : https://www.flickr.com/photos/frted/5719823982/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 14 13:13:46 GMT+0200 2011
Upload Date  : Sat May 14 21:26:15 GMT+0200 2011
Views        : 101
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_0029


+---------------+
|  DESCRIPTION  |
+---------------+
Grevy's Zebra


+--------+
|  TAGS  |
+--------+
"Cincinnati Zoo" Zoo "Zoo animals" Grevy's Zebra 