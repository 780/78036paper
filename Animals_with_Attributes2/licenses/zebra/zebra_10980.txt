+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sarahjadeonline
Photo URL    : https://www.flickr.com/photos/sarahjadeonline/16586779805/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 19 17:30:51 GMT+0200 2013
Upload Date  : Fri Feb 20 05:21:20 GMT+0100 2015
Views        : 11
Comments     : 0


+---------+
|  TITLE  |
+---------+
Zebra, Kruger National Park


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)