+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 14, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : goingslo
Photo URL    : https://www.flickr.com/photos/goingslo/5220661549/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Nov 28 11:15:13 GMT+0100 2010
Upload Date  : Tue Nov 30 16:59:55 GMT+0100 2010
Views        : 556
Comments     : 4


+---------+
|  TITLE  |
+---------+
Proud Male Zebra


+---------------+
|  DESCRIPTION  |
+---------------+
We're thinking this is the guy who sired the two young zebras we found in San Simeon.  
He accompanied the two females and their off-spring the whole time we watched.


+--------+
|  TAGS  |
+--------+
(no tags)