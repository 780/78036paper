+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Shawn McCready
Photo URL    : https://www.flickr.com/photos/shawnmccready/5910324430/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 2 17:28:32 GMT+0200 2011
Upload Date  : Wed Jul 6 23:57:02 GMT+0200 2011
Geotag Info  : Latitude:53.676406, Longitude:-112.823553
Views        : 1,151
Comments     : 14


+---------+
|  TITLE  |
+---------+
American Beaver


+---------------+
|  DESCRIPTION  |
+---------------+
The American beaver, Canada's national emblem, is the largest North American rodent. This particular one was found at Elk Island Park. 

If you look at his expression he does not look happy to have his picture being taken.


+--------+
|  TAGS  |
+--------+
Elk Island Park Nature Beaver Alberta Edmonton. 