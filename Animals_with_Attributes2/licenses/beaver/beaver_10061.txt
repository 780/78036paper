+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ArtBrom
Photo URL    : https://www.flickr.com/photos/art-sarah/2526437230/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 24 19:43:44 GMT+0200 2008
Upload Date  : Tue May 27 02:29:27 GMT+0200 2008
Views        : 110
Comments     : 0


+---------+
|  TITLE  |
+---------+
Beaver


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Oregon OR Nikon D300 hatchery "fish hatchery" beaver "marine mammal" river boat 