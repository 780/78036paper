+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : MSVG
Photo URL    : https://www.flickr.com/photos/msvg/8670625266/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 21 19:39:47 GMT+0200 2013
Upload Date  : Mon Apr 22 02:47:12 GMT+0200 2013
Geotag Info  : Latitude:43.620791, Longitude:-79.477243
Views        : 3,830
Comments     : 5


+---------+
|  TITLE  |
+---------+
The Humber Bay Beaver


+---------------+
|  DESCRIPTION  |
+---------------+
I was startled when this guy swam past me with wood. I did not really see him and thought a log was moving right past me. I took some shots as he was coming back and forth from his/her dam; he did not mind me there snapping at all! I love these guys! It's unfortunate that they are killed because they are considered pests. 

&quot;Beavers are known for their natural trait of building dams on rivers and streams, and building their homes (known as &quot;lodges&quot;) in the resulting pond. Beavers also build canals to float building materials that are difficult to haul over land. They use powerful front teeth to cut trees and other plants that they use both for building and for food. In the absence of existing ponds, beavers must construct dams before building their lodges. First they place vertical poles, then fill between the poles with a crisscross of horizontally placed branches. They fill in the gaps between the branches with a combination of weeds and mud until the dam impounds sufficient water to surround the lodge.&quot; <i>Wikipedia</i>


+--------+
|  TAGS  |
+--------+
Humber Bay Park East West Beaver swimming Lake Ontario Etobicoke Toronto Canada 