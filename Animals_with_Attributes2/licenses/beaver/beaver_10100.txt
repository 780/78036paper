+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sapienssolutions
Photo URL    : https://www.flickr.com/photos/leonardodasilva/4903118277/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 24 10:59:47 GMT+0200 2010
Upload Date  : Wed Aug 18 06:44:50 GMT+0200 2010
Geotag Info  : Latitude:41.629682, Longitude:-70.951799
Views        : 142
Comments     : 0


+---------+
|  TITLE  |
+---------+
Buttonwood Park Zoo - New Bedford MA


+---------------+
|  DESCRIPTION  |
+---------------+
zoologico zoo newbedfordma new bedford
animals


+--------+
|  TAGS  |
+--------+
castor beaver 