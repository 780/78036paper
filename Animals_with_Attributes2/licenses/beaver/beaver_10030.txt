+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kurayba
Photo URL    : https://www.flickr.com/photos/kurt-b/6094967867/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 28 20:21:51 GMT+0200 2011
Upload Date  : Tue Aug 30 05:25:00 GMT+0200 2011
Geotag Info  : Latitude:53.496086, Longitude:-113.559350
Views        : 418
Comments     : 1


+---------+
|  TITLE  |
+---------+
Beaver at Dusk


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
edmonton alberta canada whitemud ravine park creek nature preserve pentax k-5 tamron 300 f/2.8 adaptall sp 60b "Tamron Adaptall-2 SP 300mm f/2.8" beaver dusk water ripples evening low light 