+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Glyn Lowe Photoworks.
Photo URL    : https://www.flickr.com/photos/glynlowe/7420708952/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 22 11:19:33 GMT+0200 2012
Upload Date  : Fri Jun 22 18:51:05 GMT+0200 2012
Geotag Info  : Latitude:38.912465, Longitude:-76.944862
Views        : 6,350
Comments     : 80


+---------+
|  TITLE  |
+---------+
Woodchuck Up A Tree


+---------------+
|  DESCRIPTION  |
+---------------+
Woodchuck Up A Tree at Kenilworth Aquatic Gardens, Washington DC.
 
The groundhog (Marmota monax), also known as a woodchuck, or in some areas as a land-beaver, is a rodent of the family Sciuridae, belonging to the group of large ground squirrels known as marmots. Other marmots, such as the yellow-bellied and hoary marmots, live in rocky and mountainous areas, but the woodchuck is a lowland creature. It is widely distributed in North America and common in the northeastern and central United States. Groundhogs are found as far north as Alaska, with their habitat extending southeast to Alabama.

More Photos At:
<a href="http://www.glynlowe.com/kenilworth_aquatic_gardens/e3f912865#h3f912865" rel="nofollow">www.glynlowe.com/kenilworth_aquatic_gardens</a>


+--------+
|  TAGS  |
+--------+
Alabama Alaska America Aquatic DC Gardens Groundhogs Kenilworth "Kenilworth Aquatic Gardens" Marmota "Marmota monax" North Sciuridae States United Washington "Washington DC" creature distributed extending family ground groundhog group habitat hoary land-beaver lowland marmots monax mountainous northeastern rodent southeast squirrels woodchuck yellow-bellied 