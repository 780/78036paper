+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KMW2700
Photo URL    : https://www.flickr.com/photos/43934774@N04/22074022438/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 17 14:17:45 GMT+0200 2015
Upload Date  : Sun Oct 18 04:31:39 GMT+0200 2015
Views        : 92
Comments     : 0


+---------+
|  TITLE  |
+---------+
Beaver 3


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Edmonton Canada Kanada Alberta "River Valley" "North Saskatchewan River" Flußtal River Fluß Beaver Bieber 