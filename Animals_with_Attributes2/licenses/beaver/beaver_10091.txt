+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : halseike
Photo URL    : https://www.flickr.com/photos/99624358@N00/6035339693/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 30 05:56:18 GMT+0200 2011
Upload Date  : Fri Aug 12 18:41:38 GMT+0200 2011
Geotag Info  : Latitude:39.112164, Longitude:-104.939801
Views        : 365
Comments     : 0


+---------+
|  TITLE  |
+---------+
Beavers


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Beaver Colorado resivor lake brown green water beavers nature grass branches family 