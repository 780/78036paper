+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Klaudiusz Muchowski
Photo URL    : https://commons.wikimedia.org/wiki/File:Castor_fiber_vistulanus2.jpg
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) Attribution-Share Alike 3.0 Unported (//creativecommons.org/licenses/by-sa/3.0/deed.en)
Date         : 2012-08-24
