+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Feb 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Greencolander
Photo URL    : https://www.flickr.com/photos/greencolander/2447669710/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Apr 27 12:56:29 GMT+0200 2008
Upload Date  : Mon Apr 28 02:35:46 GMT+0200 2008
Views        : 582
Comments     : 2


+---------+
|  TITLE  |
+---------+
Dead beaver


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
ottawa beaver dead 