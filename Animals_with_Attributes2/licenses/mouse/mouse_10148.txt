+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : CJ Isherwood
Photo URL    : https://www.flickr.com/photos/isherwoodchris/5255316003/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Nov 30 19:37:55 GMT+0100 2010
Upload Date  : Sun Dec 12 23:59:10 GMT+0100 2010
Views        : 15,034
Comments     : 23


+---------+
|  TITLE  |
+---------+
Rescued Mouse


+---------------+
|  DESCRIPTION  |
+---------------+
So, the cat brought in a mouse, it ran away and disappeared in the kitchen, so we left it in there with the cat over night, the next day the cat wouldnt move from infront of the fridge so we knew it was there. Put the cat in another room and moved the fridge out. After about an hour of chasing it/luring it out/throwing seives on it, we got him. Instead of taking it out back in the garden straight away (in the snow) I decided to put him in a box and take some photos, also gave him a load of cheese to get his energy back after a traumatic night!


+--------+
|  TAGS  |
+--------+
cheese mouse cat 