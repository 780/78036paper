+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : cfiesler
Photo URL    : https://www.flickr.com/photos/cfiesler/5128644689/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 27 04:21:16 GMT+0100 2004
Upload Date  : Sat Oct 30 19:39:13 GMT+0200 2010
Views        : 4,371
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mouse Love


+---------------+
|  DESCRIPTION  |
+---------------+
Pet mice, Frankie and Sangria, whose personalities could roughly be mapped to Pinky and the Brain.


+--------+
|  TAGS  |
+--------+
mice pets 