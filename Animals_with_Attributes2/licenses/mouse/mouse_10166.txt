+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : brianfuller6385
Photo URL    : https://www.flickr.com/photos/birdwatcher63/16679779278/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 19 22:10:20 GMT+0100 2015
Upload Date  : Thu Mar 19 23:37:12 GMT+0100 2015
Views        : 1,966
Comments     : 0


+---------+
|  TITLE  |
+---------+
The Visitor - Field Mouse


+---------------+
|  DESCRIPTION  |
+---------------+
I know people don't usually like mice in the house, but from time to time we get a visiting field mouse (also known as a wood mouse) and I quite like them. I don't really see any difference between these little creatures and wild birds which we all like. When I spotted this one I put down a few biscuit crumbs and waited with my camera. I wasn't disappointed!


+--------+
|  TAGS  |
+--------+
(no tags)