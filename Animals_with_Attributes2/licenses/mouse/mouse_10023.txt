+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : KimCarpenter NJ
Photo URL    : https://www.flickr.com/photos/kim_carpenter_nj/4101387491/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Sep 20 00:53:45 GMT+0200 2003
Upload Date  : Sat Nov 14 03:15:20 GMT+0100 2009
Views        : 1,809
Comments     : 2


+---------+
|  TITLE  |
+---------+
Clive-BEW-English-satin male 9-03 pic1a


+---------------+
|  DESCRIPTION  |
+---------------+
Clive, a satiny BEW (black-eyed-white) mouse


+--------+
|  TAGS  |
+--------+
mouse mice 