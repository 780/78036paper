+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : emmajanehw
Photo URL    : https://www.flickr.com/photos/emmajane/196498168/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 22 22:58:12 GMT+0200 2006
Upload Date  : Sun Jul 23 23:58:37 GMT+0200 2006
Views        : 1,351
Comments     : 2


+---------+
|  TITLE  |
+---------+
gimpy the mouse


+---------------+
|  DESCRIPTION  |
+---------------+
I found him (or her) in the house. Back left leg appears to be broken. Even though my natural instinct is to kill mice in the house, I nearly drove over to the store to pick up my hamster cage and nurse it back to health. I compromised and put the mouse outside.


+--------+
|  TAGS  |
+--------+
mouse mice myhouse 