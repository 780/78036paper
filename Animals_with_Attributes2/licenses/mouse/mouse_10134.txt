+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Jeff Pang
Photo URL    : https://www.flickr.com/photos/jeffpang/2975755094/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Oct 17 19:22:08 GMT+0200 2008
Upload Date  : Sun Oct 26 20:53:02 GMT+0100 2008
Views        : 3,283
Comments     : 2


+---------+
|  TITLE  |
+---------+
Mousy slurping chocolate


+---------------+
|  DESCRIPTION  |
+---------------+
Mousy slurping chocolate.

Enchantment Lakes trip, October 17-19, 2008.


+--------+
|  TAGS  |
+--------+
mouse "hot chocolate" camping 