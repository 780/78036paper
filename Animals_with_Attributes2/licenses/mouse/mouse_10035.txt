+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Apr 22, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Canary Islands Photos - UNFILTERED
Photo URL    : https://www.flickr.com/photos/svoogle/268000958/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 12 20:48:00 GMT+0200 2006
Upload Date  : Thu Oct 12 21:52:29 GMT+0200 2006
Views        : 140
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC01386


+---------------+
|  DESCRIPTION  |
+---------------+
© <a href="http://www.svoogle.org" rel="nofollow">www.svoogle.org</a>


+--------+
|  TAGS  |
+--------+
maus gran canaria maspalomas 