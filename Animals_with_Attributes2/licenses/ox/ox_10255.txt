+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Derio Nekazaritza Eskola BHI
Photo URL    : https://www.flickr.com/photos/nekaderio/8571937642/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Mar 17 12:42:47 GMT+0100 2011
Upload Date  : Tue Mar 19 14:34:24 GMT+0100 2013
Geotag Info  : Latitude:41.628395, Longitude:-0.977847
Views        : 259
Comments     : 0


+---------+
|  TITLE  |
+---------+
Feria Zaragoza FIMA 2011


+---------------+
|  DESCRIPTION  |
+---------------+
Colección de los stands, animales y infraestructuras agroganaderas expuestos en la edición del 2011 de la Feria FIMA de Zaragoza.
Información adicional: <a href="http://www.feriazaragoza.com/fima_ganadera.aspx" rel="nofollow">www.feriazaragoza.com/fima_ganadera.aspx</a>
---
Zaragozako FIMA erakusketaren 2011ko edizioan harturiko argazki bilduma. Stand, animali hazienda eta abeltzain eta nekazaritza baliabideen erakusketa.
Informazio osagarria:
<a href="http://www.feriazaragoza.com/fima_ganadera.aspx" rel="nofollow">www.feriazaragoza.com/fima_ganadera.aspx</a>


+--------+
|  TAGS  |
+--------+
fima zaragoza expo fima2011 