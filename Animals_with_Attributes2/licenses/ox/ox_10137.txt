+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : joepyrek
Photo URL    : https://www.flickr.com/photos/joepyrek/8298699297/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 20 09:00:35 GMT+0200 2012
Upload Date  : Sun Dec 23 17:57:34 GMT+0100 2012
Geotag Info  : Latitude:-1.585894, Longitude:35.054787
Views        : 101
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cape Buffalo in the Serengeti (8)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Tanzania Serengeti Subaru Shoebaru Forester "Goodwood to Good Hope" 