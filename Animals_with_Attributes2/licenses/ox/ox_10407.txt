+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Patrick Denker
Photo URL    : https://www.flickr.com/photos/pdenker/9860719183/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 1 00:00:00 GMT+0100 1998
Upload Date  : Sat Sep 21 22:07:04 GMT+0200 2013
Views        : 50
Comments     : 0


+---------+
|  TITLE  |
+---------+
img20130908_14415955


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
tx longhorns meridian 