+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Spider.Dog
Photo URL    : https://www.flickr.com/photos/spiderdog/4983151883/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 12 09:03:40 GMT+0200 2010
Upload Date  : Sun Sep 12 20:40:28 GMT+0200 2010
Views        : 179
Comments     : 0


+---------+
|  TITLE  |
+---------+
Highland Cows


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Glasgow Pollok Country Park September Highland Cow 