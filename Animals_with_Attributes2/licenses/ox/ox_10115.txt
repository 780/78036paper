+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : flowcomm
Photo URL    : https://www.flickr.com/photos/flowcomm/14800791509/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Aug 15 17:59:56 GMT+0200 2014
Upload Date  : Thu Aug 21 09:35:25 GMT+0200 2014
Views        : 433
Comments     : 1


+---------+
|  TITLE  |
+---------+
Buffalo, Kruger National Park, South Africa


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Buffalo "Kruger National Park" "South Africa" wildlife nature safari "Big 5" 