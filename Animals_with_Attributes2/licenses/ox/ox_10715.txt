+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gvgoebel
Photo URL    : https://www.flickr.com/photos/37467370@N08/7612074866/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 20 15:51:00 GMT+0200 2012
Upload Date  : Sat Jul 21 00:53:32 GMT+0200 2012
Views        : 901
Comments     : 0


+---------+
|  TITLE  |
+---------+
Ybmox_1b


+---------------+
|  DESCRIPTION  |
+---------------+
musk ox, Point Defiance Zoo, Tacoma, Washington / 2006


+--------+
|  TAGS  |
+--------+
animals mammals "musk ox" 