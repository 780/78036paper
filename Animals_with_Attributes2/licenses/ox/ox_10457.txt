+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 09, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : gailhampshire
Photo URL    : https://www.flickr.com/photos/gails_pictures/5494175381/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 15 12:16:51 GMT+0200 2010
Upload Date  : Thu Mar 3 17:21:53 GMT+0100 2011
Views        : 121
Comments     : 0


+---------+
|  TITLE  |
+---------+
Longhorn cattle


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)