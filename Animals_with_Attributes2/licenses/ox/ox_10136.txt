+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : romanboed
Photo URL    : https://www.flickr.com/photos/romanboed/8368680472/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Dec 19 10:48:30 GMT+0100 2012
Upload Date  : Thu Jan 10 21:28:01 GMT+0100 2013
Geotag Info  : Latitude:-3.032697, Longitude:35.368595
Views        : 591
Comments     : 0


+---------+
|  TITLE  |
+---------+
Cape buffalo in Ngorongoro Crater


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Africa "Ngorongoro Crater" safari Tanzania travel wildlife 