+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jnissa
Photo URL    : https://www.flickr.com/photos/jnissa/1489440216/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jan 1 00:06:08 GMT+0100 1980
Upload Date  : Fri Oct 5 12:25:26 GMT+0200 2007
Views        : 184
Comments     : 0


+---------+
|  TITLE  |
+---------+
paraa_11


+---------------+
|  DESCRIPTION  |
+---------------+
The proud and majestic horns of a water buffalo.


+--------+
|  TAGS  |
+--------+
africa paraa uganda wildlife "water buffalo" 