+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : mobikerbear
Photo URL    : https://www.flickr.com/photos/mobikerbear/13826371964/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 12 15:31:05 GMT+0200 2014
Upload Date  : Sun Apr 13 19:52:35 GMT+0200 2014
Views        : 221
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC00281


+---------------+
|  DESCRIPTION  |
+---------------+
Moschusochse - Ovibus moschatus - Musk-ox


+--------+
|  TAGS  |
+--------+
2014 Cologne "Kölner Zoo" 