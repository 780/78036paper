+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : HBarrison
Photo URL    : https://www.flickr.com/photos/hbarrison/7407664514/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon May 21 07:58:09 GMT+0200 2012
Upload Date  : Wed Jun 20 15:14:25 GMT+0200 2012
Geotag Info  : Latitude:-25.035423, Longitude:31.721420
Views        : 519
Comments     : 1


+---------+
|  TITLE  |
+---------+
Tinga_2012 05 21_1563


+---------------+
|  DESCRIPTION  |
+---------------+
On the Morning Game Drive  /  The African buffalo, affalo, nyati, Mbogo or Cape buffalo (Syncerus caffer) is a large African bovine


+--------+
|  TAGS  |
+--------+
Africa HBarrison "Harvey Barrison" Tauck Tinga "Kruger National Park" "South Africa" "African buffalo" affalo nyati Mbogo "Cape buffalo" "Taxonomy:binomial=Syncerus caffer" 