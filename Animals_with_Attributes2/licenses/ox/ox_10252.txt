+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : EmuTakesPics
Photo URL    : https://www.flickr.com/photos/emutakespics/8212703560/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 2 17:33:59 GMT+0200 2012
Upload Date  : Sat Nov 24 01:29:28 GMT+0100 2012
Geotag Info  : Latitude:52.141805, Longitude:6.866672
Views        : 90
Comments     : 0


+---------+
|  TITLE  |
+---------+
EmuTakesPics-29.jpg


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Landschaft "Schottische Hochlandrinder" Sommer Tiere "Witte Venn" Haaksbergen Overijssel Niederlande 