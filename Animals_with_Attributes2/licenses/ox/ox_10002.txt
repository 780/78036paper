+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : kasio69
Photo URL    : https://www.flickr.com/photos/kasio69/19811475345/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jul 18 09:43:01 GMT+0200 2015
Upload Date  : Sun Jul 19 01:22:52 GMT+0200 2015
Geotag Info  : Latitude:43.341909, Longitude:-80.178394
Views        : 246
Comments     : 0


+---------+
|  TITLE  |
+---------+
_BRK5002


+---------------+
|  DESCRIPTION  |
+---------------+
African Lion Safari


+--------+
|  TAGS  |
+--------+
"African Lion Safari" Lion "Asian Elephant" Turtle Baboon Monkey "White Lion" Lama Rhino 