+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : FrooiOhnesorg
Photo URL    : https://www.flickr.com/photos/80802825@N04/8568341936/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 18 01:47:29 GMT+0100 2013
Upload Date  : Mon Mar 18 09:47:29 GMT+0100 2013
Views        : 61
Comments     : 0


+---------+
|  TITLE  |
+---------+
Was einem doch immer den Weg versperrt


+---------------+
|  DESCRIPTION  |
+---------------+
Auf einer Wanderung versperrten diese Rinder den Weg.


+--------+
|  TAGS  |
+--------+
Omerushaka Tansania Tanzania 