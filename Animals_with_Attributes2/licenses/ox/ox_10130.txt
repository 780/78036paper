+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Stiller Beobachter
Photo URL    : https://www.flickr.com/photos/a-herzog/15477955455/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 4 15:56:59 GMT+0200 2014
Upload Date  : Wed Oct 8 15:18:54 GMT+0200 2014
Geotag Info  : Latitude:49.446822, Longitude:11.144555
Views        : 1,081
Comments     : 0


+---------+
|  TITLE  |
+---------+
calf


+---------------+
|  DESCRIPTION  |
+---------------+
Tiergarten Nürnberg


+--------+
|  TAGS  |
+--------+
calf rind cattle cow 