+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Wilferd Duckitt
Photo URL    : https://www.flickr.com/photos/34480553@N06/22631921845/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 29 18:20:59 GMT+0100 2015
Upload Date  : Sat Oct 31 10:35:08 GMT+0100 2015
Views        : 17
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_1068


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)