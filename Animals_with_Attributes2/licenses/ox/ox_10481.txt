+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Zambog
Photo URL    : https://www.flickr.com/photos/zambog/14848959190/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 18 13:01:50 GMT+0200 2014
Upload Date  : Mon Aug 25 22:25:20 GMT+0200 2014
Views        : 269
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mull-32


+---------------+
|  DESCRIPTION  |
+---------------+
Highland Cow on beach at Laggan Bay


+--------+
|  TAGS  |
+--------+
2014 Mull 