+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ro431977
Photo URL    : https://www.flickr.com/photos/nahmenro/5218677565/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Nov 19 04:37:16 GMT+0100 2010
Upload Date  : Mon Nov 29 23:17:56 GMT+0100 2010
Views        : 13
Comments     : 0


+---------+
|  TITLE  |
+---------+
DSC_1027


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Annapurna Sanctuary" "Nov 2010" "GAP Adventures" 