+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Pluruk
Photo URL    : https://www.flickr.com/photos/pluruk/6137511138/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 11 13:59:02 GMT+0200 2011
Upload Date  : Sun Sep 11 21:07:04 GMT+0200 2011
Views        : 744
Comments     : 0


+---------+
|  TITLE  |
+---------+
Schotse hooglander


+---------------+
|  DESCRIPTION  |
+---------------+
Schotse hooglander in het Deelerwoud.


+--------+
|  TAGS  |
+--------+
Pluruk schotse hooglander Deelerwoud 