+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : bousole
Photo URL    : https://www.flickr.com/photos/29401656@N00/15290501066/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 16 15:27:45 GMT+0200 2014
Upload Date  : Sun Sep 21 23:06:34 GMT+0200 2014
Views        : 218
Comments     : 0


+---------+
|  TITLE  |
+---------+
Yukon Wildlife Preserve - best eye to eye with muskox


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)