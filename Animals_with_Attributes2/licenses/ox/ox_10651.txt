+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Phillip Cowell
Photo URL    : https://www.flickr.com/photos/30808846@N08/9605831186/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jun 28 15:50:24 GMT+0200 2013
Upload Date  : Tue Aug 27 06:25:50 GMT+0200 2013
Views        : 271
Comments     : 0


+---------+
|  TITLE  |
+---------+
(no title)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)