+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 08, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : oldandsolo
Photo URL    : https://www.flickr.com/photos/shankaronline/7348758076/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 1 00:00:00 GMT+0200 2009
Upload Date  : Thu Jun 7 19:18:50 GMT+0200 2012
Views        : 2,363
Comments     : 0


+---------+
|  TITLE  |
+---------+
Yaks and tourists at Tsomgo Lake


+---------------+
|  DESCRIPTION  |
+---------------+
The Tsongmo Lake is also known locally as Changu Lake. It is actually a glacial lake and is located at an altitude of 3,780 m (12,400 ft). The China border at Nathu La is a mere 5km from here as the crow flies, but is about 18km by road. The Yaks here are used to offer rides to tourists, but the yak herders have taken to charging atrocious amounts for the ride on grounds that the animals eat a lot and fodder is very expensive. This makes things worse for them as there are few takers for the ride. (May 2009)


+--------+
|  TAGS  |
+--------+
Sikkim "Gangtok. Tsomgo Lake" "Silk Road" "Silk Route" Yak 