+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Aug 18, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : aigledayres
Photo URL    : https://www.flickr.com/photos/aigledayres/28256455042/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Aug 30 12:23:34 GMT+0200 2010
Upload Date  : Sun Jul 17 10:03:14 GMT+0200 2016
Views        : 15
Comments     : 0


+---------+
|  TITLE  |
+---------+
Taupe d'Europe Talpa europaea European mole


+---------------+
|  DESCRIPTION  |
+---------------+
Taupe d'Europe Talpa europaea European mole


+--------+
|  TAGS  |
+--------+
"Taupe d'Europe" "Talpa europaea" "European mole" 