+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BSC Photography
Photo URL    : https://www.flickr.com/photos/bsc_photgraphy/6777273263/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Jan 28 12:22:16 GMT+0100 2012
Upload Date  : Sat Jan 28 19:40:46 GMT+0100 2012
Views        : 722
Comments     : 2


+---------+
|  TITLE  |
+---------+
Eastern Mole (Scalopus aquaticus)


+---------------+
|  DESCRIPTION  |
+---------------+
After 34 years I have finally found one alive!!


+--------+
|  TAGS  |
+--------+
Brown brunswick close eastern feet fuzzy georgia mammal mole up 