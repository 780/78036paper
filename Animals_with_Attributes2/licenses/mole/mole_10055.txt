+--------------------------------------+
|             Photo Metadata           |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : M J Richardson
Photo URL    : http://www.geograph.org.uk/photo/2369065
License      : Creative Commons Attribution-ShareAlike 2.0 Generic (https://creativecommons.org/licenses/by-sa/2.0/)
Date         : 16 April, 2011
