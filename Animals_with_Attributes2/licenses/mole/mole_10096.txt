+--------------------------------------+
|   commons.wikimedia Photo Metadata   |
+--------------------------------------+



+--------+
|  INFO  |
+--------+
Photographer : Hundehalter
Photo URL    : https://commons.wikimedia.org/wiki/File:Maulwurf01.JPG
License      : Creative Commons (https://en.wikipedia.org/wiki/en:Creative_Commons) 3.0 Unported (//creativecommons.org/licenses/by-sa/3.0/deed.en)
Date         : 2012-08-26
