+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : eamoncurry123
Photo URL    : https://www.flickr.com/photos/eamoncurry/14766483396/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 18 10:28:33 GMT+0200 2014
Upload Date  : Wed Jul 30 23:53:38 GMT+0200 2014
Views        : 92
Comments     : 0


+---------+
|  TITLE  |
+---------+
Mole (Deceased), Derbyshire


+---------------+
|  DESCRIPTION  |
+---------------+
Photo Taken: 18/07/14


+--------+
|  TAGS  |
+--------+
MOLE DERBYSHIRE ANIMAL 