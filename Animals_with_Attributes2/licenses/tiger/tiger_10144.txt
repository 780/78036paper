+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : orca_bc
Photo URL    : https://www.flickr.com/photos/101951515@N07/12733036835/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Feb 22 10:30:28 GMT+0100 2014
Upload Date  : Mon Feb 24 01:14:59 GMT+0100 2014
Views        : 1,159
Comments     : 0


+---------+
|  TITLE  |
+---------+
LA140222_14


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Big Cat" Cats Felidae "Siberian Tiger" "Siberian Tigers" Snow Tiger Tigers Winter "bengal tiger" "panthera tigris" 