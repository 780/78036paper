+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : ifmuth
Photo URL    : https://www.flickr.com/photos/ifmuth/6129531284/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Jul 10 15:17:43 GMT+0200 2011
Upload Date  : Fri Sep 9 08:11:39 GMT+0200 2011
Views        : 94
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
The Lincoln Park Zoo is a free and wonderful attraction in Chicago.  It's nice to take a shortcut through and see the animals....and it's right in the middle of the city.


+--------+
|  TAGS  |
+--------+
lincoln park zoo chicago il tiger 