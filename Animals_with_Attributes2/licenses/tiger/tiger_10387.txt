+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : richcollins77
Photo URL    : https://www.flickr.com/photos/nostalgia77/6932321083/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Feb 20 16:11:52 GMT+0100 2012
Upload Date  : Sun Feb 26 19:56:57 GMT+0100 2012
Views        : 4,769
Comments     : 2


+---------+
|  TITLE  |
+---------+
Tiger yawns at National Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Tiger yawns on warm February afternoon at Washington National Zoo


+--------+
|  TAGS  |
+--------+
tiger richcollins77 zoo washingtondc 