+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : amslerPIX
Photo URL    : https://www.flickr.com/photos/amslerpix/7099473169/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 13 12:18:33 GMT+0200 2012
Upload Date  : Sat Apr 21 19:52:14 GMT+0200 2012
Views        : 2,849
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
Tiger at the Louisville Zon


+--------+
|  TAGS  |
+--------+
Kentucky Louisville "Louisville Zoo" Siberian stripe tiger zoo 