+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Don Calisto
Photo URL    : https://www.flickr.com/photos/mr_leviathan/4834383555/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jun 16 11:54:31 GMT+0200 2010
Upload Date  : Tue Jul 27 18:49:20 GMT+0200 2010
Views        : 95
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
&quot;I am behind you, I always find you. I am the tiger&quot;..


+--------+
|  TAGS  |
+--------+
Tiger Animals Zoo Safari Tigre 