+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ger Dekker
Photo URL    : https://www.flickr.com/photos/ger_dekker/7586182424/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu May 31 15:56:18 GMT+0200 2012
Upload Date  : Tue Jul 17 00:24:51 GMT+0200 2012
Geotag Info  : Latitude:51.927172, Longitude:4.451093
Views        : 69
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sumatran Tiger (Panthera tigris sumatrae)


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Blijdorp Zoo Rotterdam 2012 