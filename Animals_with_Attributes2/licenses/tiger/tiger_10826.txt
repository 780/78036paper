+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : neusitas
Photo URL    : https://www.flickr.com/photos/neusitas/4486833517/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 1 10:44:33 GMT+0200 2010
Upload Date  : Sat Apr 3 20:13:34 GMT+0200 2010
Views        : 1,223
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tigre


+---------------+
|  DESCRIPTION  |
+---------------+
Tigre (Panthera Tigris).

Fotografía realizada en el parque de Paque Natural de Cabárceno.


+--------+
|  TAGS  |
+--------+
Cabarceno animales tigre tiger 