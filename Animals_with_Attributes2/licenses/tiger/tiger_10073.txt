+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : spencer77
Photo URL    : https://www.flickr.com/photos/spencer77/4901284143/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 3 15:24:45 GMT+0200 2009
Upload Date  : Tue Aug 17 18:47:13 GMT+0200 2010
Geotag Info  : Latitude:52.402994, Longitude:-0.337142
Views        : 4,079
Comments     : 1


+---------+
|  TITLE  |
+---------+
Bengal Tiger (Panthera Tigris Tigris), Hamerton Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Thanks to Photographers on Safari (<a href="http://www.photographersonsafari.com/" rel="nofollow">www.photographersonsafari.com/</a>)
This picture is available to use for free, under the creative commons licence. All I ask is that I'm given a photo credit &amp; a courtesy email to let me know how it's being used.


+--------+
|  TAGS  |
+--------+
Animal mammal cat feline tiger panthera tigris bengal stripes "Hamerton Zoo" Cats FlickrBigCats "panthera tigris" Sony A350 alpha photographersonsafari 