+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : DannyFord
Photo URL    : https://www.flickr.com/photos/dannyford/15406032884/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Dec 15 13:56:41 GMT+0100 2014
Upload Date  : Mon Dec 15 16:01:21 GMT+0100 2014
Views        : 130
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger II


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
animals wildlife howletts zoo tiger 