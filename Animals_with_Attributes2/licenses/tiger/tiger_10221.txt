+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : firecloak
Photo URL    : https://www.flickr.com/photos/firecloak/8180415016/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Nov 8 14:12:42 GMT+0100 2012
Upload Date  : Tue Nov 13 00:25:02 GMT+0100 2012
Views        : 671
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sleeping White Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
White tiger at Audubon Zoo trying to sleep


+--------+
|  TAGS  |
+--------+
white tiger "big cat" 