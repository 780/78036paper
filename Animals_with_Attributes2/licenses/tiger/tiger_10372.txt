+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : djst33l
Photo URL    : https://www.flickr.com/photos/silver_and_steel/3757413380/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jul 6 03:21:16 GMT+0200 2009
Upload Date  : Sun Jul 26 08:29:45 GMT+0200 2009
Views        : 157
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger temple


+---------------+
|  DESCRIPTION  |
+---------------+
The tiger temple in Kanchanaburri, Thailand.


+--------+
|  TAGS  |
+--------+
thailand tigers outdoors 