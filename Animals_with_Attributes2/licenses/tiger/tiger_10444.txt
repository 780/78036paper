+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : thebuffafamily
Photo URL    : https://www.flickr.com/photos/thebuffafamily/902655679/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 26 15:06:23 GMT+0200 2007
Upload Date  : Thu Jul 26 10:32:11 GMT+0200 2007
Views        : 483
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
Hard to catch a good shot


+--------+
|  TAGS  |
+--------+
"Taronga Zoo" tiger 