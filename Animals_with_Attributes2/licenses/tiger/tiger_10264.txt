+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Eoghann Irving
Photo URL    : https://www.flickr.com/photos/eoghann/8757860196/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri May 10 13:10:42 GMT+0200 2013
Upload Date  : Mon May 20 14:30:40 GMT+0200 2013
Geotag Info  : Latitude:38.929018, Longitude:-77.046582
Views        : 604
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sumatran Tiger, Smithsonian's National Zoo, DC


+---------------+
|  DESCRIPTION  |
+---------------+
The zoo trip turned out to be full of great photo ops, like this one of a Sumatran tiger sunning itself. Definitely a case where my telephoto lens paid off to bring the tiger closeup.

With a population estimated at less than 700 Sumatran tigers are classified as an endangered species. They exist in isolated groups across the island of  Sumatra.

Comparatively small for a tiger, males weigh between 220 and 310lbs with a length of 89&quot; while females weigh between 170 and 240lbs with a length of 91&quot;


+--------+
|  TAGS  |
+--------+
sumatrantiger tiger zoo nature animal 