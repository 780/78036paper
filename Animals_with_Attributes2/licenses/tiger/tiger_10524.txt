+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Guillermo Fdez
Photo URL    : https://www.flickr.com/photos/guillermofdez/3497834244/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Apr 30 10:59:25 GMT+0200 2009
Upload Date  : Sun May 3 18:54:44 GMT+0200 2009
Views        : 18,344
Comments     : 61


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
Explore: 2009-05-03  ( #20)

Nikon D300 | Nikon 70-200mm@340mm (1.7x) | ƒ4.8 | 1/80s | ISO200 | Handheld

This is my first photo done with the teleconverter 1.7x. The autofocus is a bit slower but the bokeh is still wonderful.
This photo is dedicated to all my friends in FC. I hope you like it :)

--
Esta es mi primera foto hecha con el mutiplicador 1.7x. Relentiza un poco el enfoque pero el bokeh sigue siendo igual de fantástico.
Dedico esta foto a todos mis compañeros y buenos amigos de FC. Espero que os guste :)


+--------+
|  TAGS  |
+--------+
Tiger Tigre "*nature* outpost" 