+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : rollirob
Photo URL    : https://www.flickr.com/photos/110282566@N08/18125253116/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 26 13:25:32 GMT+0200 2015
Upload Date  : Wed May 27 09:36:11 GMT+0200 2015
Views        : 620
Comments     : 1


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Kölner Zoo Tiere Nikon Digital D7100 Frühjahr Tierpark Aqurium Terraium Germany Deutschland D800 