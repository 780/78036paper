+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : catlovers
Photo URL    : https://www.flickr.com/photos/90389546@N00/3512034897/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Apr 15 12:13:29 GMT+0200 2009
Upload Date  : Fri May 8 13:24:22 GMT+0200 2009
Views        : 142,773
Comments     : 162


+---------+
|  TITLE  |
+---------+
tiger


+---------------+
|  DESCRIPTION  |
+---------------+
This tiger steps gently for catlick!


+--------+
|  TAGS  |
+--------+
catlovers moni.sertel tiger animal cat catlick Frankfurt zoo "cat of prey" nature VosPlusBellesPhotos GoldStarAward "The Perfect Photographer" SogniDreams ABigFave OneOfMyPics ArtOfAtmosphere InternationalFlickrAwards MultiMegaShot “flickrAward” APlusPhoto NaturesElegantShots "Nature Through The Lens" Pot-of-Gold Kartpostal "Today´s Best" mywinners 