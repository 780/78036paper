+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : jpstanley
Photo URL    : https://www.flickr.com/photos/jpstanley/2747986755/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Aug 2 12:09:34 GMT+0200 2008
Upload Date  : Sun Aug 10 05:56:41 GMT+0200 2008
Views        : 90
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"san diego" "san diego zoo" tiger california 