+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : deliciouscherry1988
Photo URL    : https://www.flickr.com/photos/123739726@N04/15507423968/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 1 16:54:28 GMT+0100 2014
Upload Date  : Sun Nov 2 19:06:51 GMT+0100 2014
Views        : 43
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
Tiger - Zoo Duisburg


+--------+
|  TAGS  |
+--------+
Tiger Zoo Duisburg Nature 