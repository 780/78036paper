+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/8063372596/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Jul 20 11:03:31 GMT+0200 2012
Upload Date  : Sun Oct 7 18:41:51 GMT+0200 2012
Views        : 124
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Tigers


+--------+
|  TAGS  |
+--------+
"Chester Zoo" tigers 