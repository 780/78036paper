+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Kurayba
Photo URL    : https://www.flickr.com/photos/kurt-b/5678693090/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Apr 11 13:03:53 GMT+0200 2011
Upload Date  : Mon May 2 05:27:58 GMT+0200 2011
Geotag Info  : Latitude:20.530028, Longitude:-105.280448
Views        : 862
Comments     : 0


+---------+
|  TITLE  |
+---------+
Bengal Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
puerto vallarta mismaloya jalisco mexico zoologico de zoo captive pentax k-5 Panthera tigris Royal Bengal Tiger water walking wet cat da 18-250 high iso smcpda18250mmf3563edalif 