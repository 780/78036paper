+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Tambako the Jaguar
Photo URL    : https://www.flickr.com/photos/tambako/4657281407/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat May 8 14:07:58 GMT+0200 2010
Upload Date  : Tue Jun 1 00:21:28 GMT+0200 2010
Geotag Info  : Latitude:47.385711, Longitude:8.573412
Views        : 6,417
Comments     : 16


+---------+
|  TITLE  |
+---------+
Coto eating the hanging meat


+---------------+
|  DESCRIPTION  |
+---------------+
This handsome Amur tiger was stretching his whole body in order to eat the big chunk of meat which was hanging from the cliff in his enclosure. That was quite impressive to watch.

Picture taken in the zoo of Zürich.


+--------+
|  TAGS  |
+--------+
tiger male amur siberian big wild cat eating food meat hanging paws impressive zoo zürich switzerland nikon d300 zurich "panthera tigris" kitty tigre feline 