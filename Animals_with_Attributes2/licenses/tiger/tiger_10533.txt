+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Albuquerque BioPark
Photo URL    : https://www.flickr.com/photos/abqbiopark/3596043448/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 4 11:31:43 GMT+0200 2009
Upload Date  : Thu Jun 4 20:31:43 GMT+0200 2009
Views        : 2,505
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
Top of the Food Chain. See the world through the eye of the tiger. Photo courtesy of ABQ BioPark. Read more or sign up for <a href="http://bit.ly/BioParkPOTW" rel="nofollow">Photo of the Week</a> by email.


+--------+
|  TAGS  |
+--------+
Albuquerque BioPark Zoo "Rio Grande Zoo" Tiger Catwalk 