+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : sjostromfoto.se
Photo URL    : https://www.flickr.com/photos/damienz/7817196670/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Oct 2 15:24:44 GMT+0200 2011
Upload Date  : Sun Aug 19 20:51:17 GMT+0200 2012
Views        : 1,441
Comments     : 5


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
Those eyes


+--------+
|  TAGS  |
+--------+
Animal Autumn "Canon 550D" Color "EF-S 55-250mm f/4.0-5" Nature Photo Season Tele Wildlife Sverige Sweden Daniel Sjöström Borås Zoo 