+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Sports Cars Fan
Photo URL    : https://www.flickr.com/photos/114082686@N08/21400419312/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jul 16 17:14:36 GMT+0200 2015
Upload Date  : Mon Sep 14 20:34:25 GMT+0200 2015
Geotag Info  : Latitude:-36.839824, Longitude:-73.008581
Views        : 7,228
Comments     : 2


+---------+
|  TITLE  |
+---------+
Starring back at you


+---------------+
|  DESCRIPTION  |
+---------------+
White tiger - Tigre blanco (Panthera tigris tigris)


+--------+
|  TAGS  |
+--------+
zoo "wild animal" captivity "animal salvaje" cautiverio animal mamífero "big cat" carnivore carnívoro feline felino mammal tigre tiger 