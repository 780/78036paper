+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : nestor galina
Photo URL    : https://www.flickr.com/photos/nestorgalina/2250914649/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Jan 2 15:19:15 GMT+0100 2008
Upload Date  : Sat Feb 9 01:25:29 GMT+0100 2008
Views        : 869
Comments     : 2


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
Tigre ZOO Cordoba


+--------+
|  TAGS  |
+--------+
tigre tiger zoo cordoba argentina LTyTR1 nestorgalina galina 