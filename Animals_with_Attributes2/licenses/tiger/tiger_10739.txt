+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Andreas Munter
Photo URL    : https://www.flickr.com/photos/munterphoto/9966613084/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Wed Sep 25 14:06:51 GMT+0200 2013
Upload Date  : Fri Sep 27 16:31:13 GMT+0200 2013
Geotag Info  : Latitude:55.671800, Longitude:12.523137
Views        : 183
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
From Copenhagen Zoo September 2013


+--------+
|  TAGS  |
+--------+
Copenhagen Tiger Zoo denmark 