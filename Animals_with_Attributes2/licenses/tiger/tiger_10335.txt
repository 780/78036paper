+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Begemot
Photo URL    : https://www.flickr.com/photos/begemot/89209333/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jan 12 00:23:27 GMT+0100 2006
Upload Date  : Sat Jan 21 10:58:43 GMT+0100 2006
Views        : 2,067
Comments     : 3


+---------+
|  TITLE  |
+---------+
Tiger cub, Kanha Park, January 2006


+---------------+
|  DESCRIPTION  |
+---------------+
Tiger cub in Kanha Park, India, January 2006.


+--------+
|  TAGS  |
+--------+
tiger kanha 