+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Derek A Young
Photo URL    : https://www.flickr.com/photos/derekarlanyoung/7032214831/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 30 13:34:09 GMT+0200 2012
Upload Date  : Sat Mar 31 17:33:38 GMT+0200 2012
Views        : 29
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Bronx Zoo" Bronx 