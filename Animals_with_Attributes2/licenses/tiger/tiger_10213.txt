+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Ber'Zophus
Photo URL    : https://www.flickr.com/photos/10574543@N08/900264564/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Jul 17 15:59:50 GMT+0200 2007
Upload Date  : Thu Jul 26 02:24:59 GMT+0200 2007
Views        : 6,717
Comments     : 2


+---------+
|  TITLE  |
+---------+
Siberian Tiger, Swimming


+---------------+
|  DESCRIPTION  |
+---------------+
A Siberian Tiger, taking a swim. Taken at the Toronto Zoo, with a Canon EOS 350D &amp; 70-300mm f4-5.6 IS USM lens


+--------+
|  TAGS  |
+--------+
"Siberian Tiger" Tiger "Panthera Tigris Altaica" "Toronto Zoo" Animals Zoo 