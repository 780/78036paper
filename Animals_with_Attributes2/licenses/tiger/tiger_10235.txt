+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : -=Kip=-
Photo URL    : https://www.flickr.com/photos/kipsoep/8600444493/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Mar 29 14:34:48 GMT+0100 2013
Upload Date  : Fri Mar 29 21:29:06 GMT+0100 2013
Views        : 165
Comments     : 0


+---------+
|  TITLE  |
+---------+
Sumatraanse tijger


+---------------+
|  DESCRIPTION  |
+---------------+
Burgers Zoo


+--------+
|  TAGS  |
+--------+
sumatraanse tijger Panthera tigris sumatrae burgers zoo 