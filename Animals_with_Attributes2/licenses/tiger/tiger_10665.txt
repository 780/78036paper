+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : My 365
Photo URL    : https://www.flickr.com/photos/tmb/6161450724/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Sep 18 16:19:43 GMT+0200 2011
Upload Date  : Mon Sep 19 05:01:17 GMT+0200 2011
Views        : 350
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"San Francisco Zoo" Tiger 