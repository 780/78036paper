+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Koshyk
Photo URL    : https://www.flickr.com/photos/kkoshy/3489363171/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue Apr 28 17:54:40 GMT+0200 2009
Upload Date  : Fri May 1 02:03:15 GMT+0200 2009
Views        : 9,127
Comments     : 15


+---------+
|  TITLE  |
+---------+
Bandhavgarh


+---------------+
|  DESCRIPTION  |
+---------------+
Captan Suresh Sharma and I just returned from three day trip to Bandhavgarh National Park, Madhya Pradesh. The National Park, formally established in 1968 was the hunting reserve of the erstwhile rulers of Rewa. Spread over an area of 108 Square KM in the core area and a buffer zone of nearly 500 Sq.KM, this tiger reserve is famous for its large population of tigers. Though the Park has a rich variety of fauna, tigers remain the main attraction. This Park has the greatest density of tigers in India. In summers, the visibility of tigers become more on account of the drying up of seasonal water sources. A few water holes available attract prey and the hunter and of course photographers willing to brave the sweltering summer heat of India.
The small village of Tala where the main gate is situated has a number of hotels and lodges catering to all tastes and purses. Our stay at Nature Heritage (aslo known as Agra Lodge) was very comfortable and lucky. In three days we had over 15 sighting of tigers. As many as 60 species of birds and 12 of wild animals were also seen


+--------+
|  TAGS  |
+--------+
tiger Bandhavgarh safari MadyaPradesh MP Forest jungle Mogli cat "Big Cat" wildlife "Operation Tiger" conservation NikonD300 Sigma300 "Natur Heritage" stripes "Royal Bengal Tiger" "Pathera Tigris" Bagh Kaduva Nature "Wildlife Safari" "Madhya Pradesh Wildlife Department" "Sigma 120-300 f/2.8" "Sigma 1.4X Converter" "Bandhavgarh travel" "Wildlife safari Bandhavgarh" "Nature Heritage Resort Bandhavgarh" tigress 