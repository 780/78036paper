+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Smudge 9000
Photo URL    : https://www.flickr.com/photos/smudge9000/457777858/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Mar 5 11:57:51 GMT+0100 2007
Upload Date  : Fri Apr 13 18:03:06 GMT+0200 2007
Geotag Info  : Latitude:51.268896, Longitude:1.153306
Views        : 2,425
Comments     : 1


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
Tiger at Port Lympne Wildlife Park, UK.


+--------+
|  TAGS  |
+--------+
"Aspinal Zoo Park" Zoo "Wildlife Park" Tiger FlickrBigCats 