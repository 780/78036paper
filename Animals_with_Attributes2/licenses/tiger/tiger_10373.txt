+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Doug Beckers
Photo URL    : https://www.flickr.com/photos/dougbeckers/4548288634/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Apr 3 17:05:58 GMT+0200 2010
Upload Date  : Sat Apr 24 16:39:07 GMT+0200 2010
Views        : 285
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger Temple


+---------------+
|  DESCRIPTION  |
+---------------+
Indochinese Tigers at the Tiger Temple - Wat Pa Luangta Bua Yannasampanno, 3 hours north of Bangkok Thailand


+--------+
|  TAGS  |
+--------+
"Panthera tigris corbetti" "Indochinese Tiger" Thailand 