+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Stv.
Photo URL    : https://www.flickr.com/photos/stv/477872867/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Fri Apr 20 10:38:57 GMT+0200 2007
Upload Date  : Mon Apr 30 05:55:09 GMT+0200 2007
Geotag Info  : Latitude:43.819418, Longitude:-79.184732
Views        : 31
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
ON Toronto Travel Vacation "Toronto Zoo" Tiger 