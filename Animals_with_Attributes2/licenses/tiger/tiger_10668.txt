+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Phil Nilsson - SunrayImages
Photo URL    : https://www.flickr.com/photos/pjnphotography/6283839391/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Oct 13 14:39:00 GMT+0200 2011
Upload Date  : Wed Oct 26 22:51:15 GMT+0200 2011
Views        : 20
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
(no tags)