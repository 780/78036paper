+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : big-ashb
Photo URL    : https://www.flickr.com/photos/big-ashb/14877718564/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Aug 7 17:01:03 GMT+0200 2014
Upload Date  : Sun Aug 10 23:29:50 GMT+0200 2014
Geotag Info  : Latitude:50.991313, Longitude:-1.283255
Views        : 1,240
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Canon 600D Sigma Marwell Zoo animal tiger laying 