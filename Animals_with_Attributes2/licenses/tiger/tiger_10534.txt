+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : L.Richarz
Photo URL    : https://www.flickr.com/photos/lricharz/3598906988/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Thu Jun 4 16:07:05 GMT+0200 2009
Upload Date  : Fri Jun 5 21:37:12 GMT+0200 2009
Views        : 20
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
Metro Toronto Zoo Tiger 