+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel's Europe & beyond 2
Photo URL    : https://www.flickr.com/photos/zooeurope/7480855344/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Mon Jun 4 11:02:38 GMT+0200 2012
Upload Date  : Sun Jul 1 20:11:59 GMT+0200 2012
Views        : 97
Comments     : 0


+---------+
|  TITLE  |
+---------+
Chester Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
Tigers


+--------+
|  TAGS  |
+--------+
"Chester Zoo" Tigers 