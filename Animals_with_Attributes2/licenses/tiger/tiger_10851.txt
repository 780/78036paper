+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : poplinre
Photo URL    : https://www.flickr.com/photos/poplinre/514831254/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Tue May 22 13:15:12 GMT+0200 2007
Upload Date  : Sat May 26 19:05:59 GMT+0200 2007
Geotag Info  : Latitude:42.304769, Longitude:-71.087712
Views        : 1,490
Comments     : 1


+---------+
|  TITLE  |
+---------+
White Tiger #1


+---------------+
|  DESCRIPTION  |
+---------------+
(no description)


+--------+
|  TAGS  |
+--------+
"Panthera tigris tigris" Mammalia Boston Panthera animals Felidae Earth "Bengal Tiger" "Canon EF 200mm f/2.8L USM" tiger "white morph" Carnivora 2007 Chordata "white tiger" "Franklin Park Zoo" handheld Massachusetts "Panthera tigris" zoo Animalia USA "United States of America" 