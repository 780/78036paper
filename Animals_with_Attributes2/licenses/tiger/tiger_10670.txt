+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Nigel Quest Photography
Photo URL    : https://www.flickr.com/photos/51782432@N07/6486701539/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Dec 10 11:32:10 GMT+0100 2011
Upload Date  : Sat Dec 10 15:06:28 GMT+0100 2011
Geotag Info  : Latitude:50.409666, Longitude:-3.993101
Views        : 3,487
Comments     : 6


+---------+
|  TITLE  |
+---------+
tiger


+---------------+
|  DESCRIPTION  |
+---------------+
This was taken this morning at sparkwell zooalogical park with Michelle really nice morning and some great shots . Hope you like this one


+--------+
|  TAGS  |
+--------+
Tiger FlickrBigCats 