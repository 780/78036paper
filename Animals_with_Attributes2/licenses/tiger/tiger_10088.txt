+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : Christopher Kray Visuals
Photo URL    : https://www.flickr.com/photos/85208536@N02/14740680525/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Nov 24 16:49:25 GMT+0100 2012
Upload Date  : Fri Jul 25 12:46:10 GMT+0200 2014
Views        : 28,015
Comments     : 18


+---------+
|  TITLE  |
+---------+
Blue Eyed Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
On my trip to india in 2012 we did not see many tigers but towards the end we did manage to see a couple, this young 4 year old with amazing blue eyes.

Also i need to add that when i seen this tiger in 2012 he had a slight injury from fighting with another mail tiger, when i returned in 2013 i was told that the tiger died from his injuries.


+--------+
|  TAGS  |
+--------+
tiger ranthambhore sony a99 