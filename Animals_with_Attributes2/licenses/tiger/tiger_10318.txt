+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : RichardTurnerPhotography
Photo URL    : https://www.flickr.com/photos/ymmv/3259827037/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sat Oct 22 12:19:04 GMT+0200 2005
Upload Date  : Sat Feb 7 16:15:32 GMT+0100 2009
Views        : 792
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger at Marwell Zoo


+---------------+
|  DESCRIPTION  |
+---------------+
The Tiger is watching my nephew very closely, and followed him along the path. Scary!


+--------+
|  TAGS  |
+--------+
tiger 