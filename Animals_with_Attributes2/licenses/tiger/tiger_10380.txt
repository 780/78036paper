+--------------------------------------+
|   Flickr Photo Metadata              |
|   Created by Bulkr on Mar 03, 2016   |
|   http://clipyourphotos.com/bulkr    |
+--------------------------------------+

+--------+
|  INFO  |
+--------+
Photographer : BlackburnMike_1
Photo URL    : https://www.flickr.com/photos/mikeblackburn/4849556711/
License      : Creative Commons (http://creativecommons.org/licenses)
Taken Date   : Sun Aug 1 12:47:33 GMT+0200 2010
Upload Date  : Sun Aug 1 19:22:39 GMT+0200 2010
Views        : 67
Comments     : 0


+---------+
|  TITLE  |
+---------+
Tiger Tiger


+---------------+
|  DESCRIPTION  |
+---------------+
From behind (thick) glass in the tiger enclosure, Jhb Zoo. Pretty sad actually.


+--------+
|  TAGS  |
+--------+
canon350D johannesburg tiger zoo 